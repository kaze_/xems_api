xems_api
========

A Symfony project created on May 2, 2017, 10:36 am.


## Redis Pub/Sub
先有一隻 Symfony Command 去訂閱 Redis Topic，並在背景 Run。
在程式中，可直接引用 Predis Pub 去觸發。

這其中必須有一張表，紀錄待處理的事件。


#### 作法：
1. 建立一個服務、一張紀錄表，並在 service 中完成 寫入、讀出處理
    1. 參考 ```src/EvBundle/Services/PushNotification.php```
1. 新增一隻 Command，實踐 ```src/EvBundle/Command/RedisSubCommand.php```
    1. 參考 其他 Command
    1. 可用 php bin/console xxxx:xxxx 123 來開發測試
1. 執行 ``` php bin/console xxxx:xxxx ``` 啟用 deamon
1. 在 service 寫入的下方，呼叫 ``` $redis->publish('mytopic', 123) ``` 觸發

#### Deploy：
1. 所有 Redis Deamon 都寫在 ```deploy/deamon.sh```
1. Crontab 固定一天重啟一次
> 0 * * * * sh /var/www/test_api/deploy/deamon.sh

#### 注意：
1. 建議在 Publish 上方加上「同步處理機制」，防止 deamon 掛掉時，沒人處理
1. mysql connect 幾分鐘沒動作，會自動斷線，這部分 RedisSubCommand 已處理

