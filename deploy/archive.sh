#!/bin/bash
BASEDIR=$(dirname "$0")
BRANCH=$1

cd $BASEDIR/../
mkdir working/
git archive $BRANCH | tar -x -C working/
