#!/bin/bash
BASEDIR=$(dirname "$0")

cd $BASEDIR/../../
php bin/console esp:monthlyfee --env=prod
php bin/console esp:dailyfee --env=prod
php bin/console ev:monthlycharge --env=prod
php bin/console ev:user:monthlycharge --env=prod
