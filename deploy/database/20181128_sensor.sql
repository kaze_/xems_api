create table ems_sensor (
  device_id         char(36) not null, 
  sensor_type_id    int(11) not null, 
  modbus_id         int(11), 
  modbus_gateway_id char(36) not null, 
  log_interval      int(11), 
  monitor_interval  int(11), 
  primary key (device_id)) CHARACTER SET utf8mb4;
create table ems_sensor_type (
  sensor_type_id    int(11) not null auto_increment, 
  sensor_name       varchar(30) not null, 
  sensor_model_name varchar(30) not null, 
  primary key (sensor_type_id)) CHARACTER SET utf8mb4 AUTO_INCREMENT=5;
create unique index esp_electricity_bill 
  on esp_electricity_bill (esp_customer_number, energy_package_id, billing_start_date);
alter table ems_sensor add constraint FKems_sensor647974 foreign key (device_id) references ems_device_master (device_id);
alter table ems_sensor add constraint FKems_sensor71755 foreign key (modbus_gateway_id) references ems_modbus_gateway (device_id);
alter table ems_sensor add constraint FKems_sensor494763 foreign key (sensor_type_id) references ems_sensor_type (sensor_type_id);

