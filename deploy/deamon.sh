#!/bin/bash
BASEDIR=$(dirname "$0")

cd $BASEDIR/../

T="`date +%Y%m%d`"
LOG=var/logs

SMS="/usr/bin/php bin/console sms:send --env=prod"
pkill -f "$SMS"
$SMS >> $LOG/sms.log.$T &


NOTI="/usr/bin/php bin/console push:notification --env=prod"
pkill -f "$NOTI"
$NOTI >> $LOG/noti.log.$T &


PAY="/usr/bin/php bin/console pay:trade --env=prod"
pkill -f "$PAY"
$PAY >> $LOG/pay.log.$T &

chown www-data:www-data $LOG/*
