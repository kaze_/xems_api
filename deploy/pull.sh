#!/bin/bash
BASEDIR=$(dirname "$0")

cd $BASEDIR/../
git pull
php bin/console cache:clear --env=prod
#php bin/console assetic:dump
chown -R www-data:www-data .