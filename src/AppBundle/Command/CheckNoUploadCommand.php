<?php
namespace AppBundle\Command;

// https://symfony.com/doc/2.6/cookbook/console/console_command.html
// https://symfony.com/doc/3.3/console.html
// https://ourcodeworld.com/articles/read/239/how-to-create-and-execute-a-custom-console-command-in-symfony-3

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Bundle\FrameworkBundle\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use PDO;

class CheckNoUploadCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:checknoupload')
            ->setDescription('check gateways upload no data')
            ->addArgument(
                'to',
                InputArgument::OPTIONAL,
                'to email address'
            )
            ->addArgument(
                'subject',
                InputArgument::OPTIONAL,
                'email subject'
            )
        ;
    }

    /**
     * Initializes the command just after the input has been validated.
     *
     * This is mainly useful when a lot of commands extends one main command
     * where some things need to be initialized based on the input arguments and options.
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->exclude_circuits = [
            'a80707eb-a461-4b99-983d-512a7da74386' => 1,
        ];
    }


    /**
     * Interacts with the user.
     *
     * This method is executed before the InputDefinition is validated.
     * This means that this is the only place where the command can
     * interactively ask for values of missing required arguments.
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
    }

    protected function getCircuits(OutputInterface $output)
    {
        $query = <<<EOT
        SELECT A.circuit_id, A.last_data_report, C.device_type_id, C.device_name
        FROM ems_circuit_master AS A, ems_meter_circuit_mapping AS B, ems_device_master AS C
        WHERE A.circuit_id = B.circuit_id
        AND ( B.device_id = C.device_id )
        AND ( C.device_type_id = 100 OR C.device_type_id = 200 OR C.device_type_id = 800) 
        AND ( A.last_data_report is NULL OR A.last_data_report < DATE_SUB(NOW(), INTERVAL 1 HOUR) )
        ORDER BY A.last_data_report DESC
        ;
EOT
        ;

        $result = null;
        try {
            $em = $this->getContainer()->get('doctrine')->getManager();
            $statement = $em->getConnection()->prepare($query);
            $statement->execute();
            $result = $statement->fetchAll(PDO::FETCH_ASSOC);

            // print_r($result);
            //$output->writeln( 'done' );
        } catch (PDOException $e) {
            $output->writeln( $ex->getMessage() );
        }

        return $result;
    }



    protected function foo(InputInterface $input, OutputInterface $output)
    {

        $query = '
        SELECT MAX(report_datetime) AS rtime
        FROM ems_power_report_log 
        WHERE circuit_id=:cid 
        AND report_datetime < DATE_SUB(NOW(), INTERVAL 1 HOUR)
        ;
        ';

        $query2 = <<<EOT3
        SELECT count(report_datetime) AS reports
        FROM ems_power_report_log 
        WHERE circuit_id=:cid
            AND report_datetime > DATE_SUB(NOW(), INTERVAL 24 HOUR)
        ;
EOT3
        ;

        $circuits = $this->getCircuits($output);
        $circuits_upload = [];

        if ( ! $circuits ) {
            // FIXME
            $output->writeln( 'no circuit' );
            return $circuits_upload;
        }

        try {
            $em = $this->getContainer()->get('doctrine')->getManager();
            $stmt = $em->getConnection()->prepare($query);
            $stmt2 = $em->getConnection()->prepare($query2);
        } catch (PDOException $e) {
            // FIXME
            $output->writeln( $ex->getMessage() );
            return ;
        }
        try {
            foreach ($circuits as $circuit) {
                $cid = $circuit['circuit_id'];
                //echo $cid,"\n";
                //echo $circuit['device_name'];

                // skip exclude circuits
                if ( isset($this->exclude_circuits[$cid]) ) 
                    continue;

                $stmt->bindParam(':cid',$cid,PDO::PARAM_STR);
                $stmt->execute();
                $result = $stmt ->fetch(PDO::FETCH_ASSOC);
                //print_r($result);

                //$stmt2->bindParam(':cid',$cid,PDO::PARAM_STR);
                //$stmt2->execute();
                //$result2 = $stmt2->fetch(PDO::FETCH_ASSOC);

                //$circuits_upload[$cid] = [ $cid, $circuit['device_name'], $result['rtime'], $result2['reports'] ] ;
                $circuits_upload[] = [ $cid, $circuit['device_name'], $result['rtime']] ;
            }
        } catch (PDOException $e) {
            $output->writeln( $ex->getMessage() );
        }

        print_r($circuits_upload);
        return $circuits_upload;
    }


    protected function sendMail($from, $to, $subject, $content)
    {

        //$this->getContainer()->get('app.user_manager');
        //$content = 'From: 12345'.PHP_EOL.PHP_EOL;

        $message = \Swift_Message::newInstance()
            ->setSubject('[test] '.$subject)
            ->setFrom($from)
            ->setTo($to)
            ->setBody($content)
        ;

        $message = \Swift_Message::newInstance()
            ->setSubject('[test] '.$subject)
            ->setFrom($from)
            ->setTo($to)
            ->setBody(
                 $content,
                'text/html'
            )
        ;
        try {
            // $this->get('mailer')->send($message);
            //$this->getContainer()->getParameter('mailer')->send($message);
            $mailer = $this->getContainer()->get('mailer');
            $mailer->send($message);
        } catch (Exception $ex) {

            $logger = $this->get('logger');
            $logger->error('[Email] Send failed.'.$ex->getMessage());

            //$output->writeln( '[Email] Send failed.'.$ex->getMessage() );
        }
        //$output->writeln( 'send done' );

    }


    function toTable($circuits)
    {


        $head = "<table boder=1> <thead> <td> circuit_id </td> <td> device_name </td> <td> report time </td>  </thead>";

        $arr = [];
        $arr[] = $head;
        foreach($circuits as $circuit) {
            $arr[] = sprintf("<tr> <td> %s </td>   <td>%s  </td> <td> %s </td> </tr>",
                $circuit['circuit_id'],
                $circuit['device_name'],
                $circuit['last_data_report']
            );
        }
        $arr[] = "</table>";

        return join("\n",$arr);

    }

    protected function foo2(InputInterface $input, OutputInterface $output)
    {
        $circuits = $this->getCircuits($output);
        $content = print_r($circuits,true);

        $content = $this->toTable( $circuits);

        $from = $this->getContainer()->getParameter('admin_email');
        $addr = $input->getArgument('to');
        if ($addr) {
            $to = $addr;
        } else {
            //$to = ['onyx.huang@xmight.com', 'chiehying@xmight.com' ];
            $to = ['onyx.huang@xmight.com' ];
        }

        $subject = $input->getArgument('subject');
        if ( ! $subject ) {
            $subject = '逾時一小時未上傳資料的設備' ;
        }
        //echo $content;

        $this->sendMail($from, $to, $subject, $content);
    }


    // $this->getParameter('admin_email')
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $r = $this->foo2($input, $output);

        echo "done", PHP_EOL;

    }

}


