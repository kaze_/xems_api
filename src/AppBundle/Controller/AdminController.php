<?php

namespace AppBundle\Controller;

use AppBundle\Entity\EmsGatewayMaster;
use AppBundle\Entity\EmsLocationGatewayMapping;
use AppBundle\Entity\EmsLocationMaster;
use AppBundle\Entity\EvChargingStationMaster;
use AppBundle\Entity\ZipCode;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Exception;
use Ramsey\Uuid\Uuid;
use AppBundle\Entity\EmsCustomerLocationMapping;

class AdminController extends Controller
{
    public function uuidAction(Request $request)
    {

        $n = $request->get('number');
        if ( $n == null ) {
            $n = 0;
        }
/*
        print_r ($uuid);
        $uuid->getBytes();
*/
        $response = new JsonResponse();
        $uuids = array();
        for($i = 0; $i < $n; $i++ ) {
            $uuid = Uuid::uuid4();
            $uuids[] = $uuid->toString();
        }

        $response->setData($uuids);
        return $response;

    }

    public function customerAction()
    {
        $uuid = Uuid::uuid4()->toString();
        $RAW_QUERY = 'INSERT INTO customer_master (customer_uuid) values (unhex(replace(\''.$uuid.'\', \'-\', \'\')));';
        $entityManager = $this->getDoctrine()->getManager();

        $statement = $entityManager->getConnection()->prepare($RAW_QUERY);
        $statement->execute();
        $response = new JsonResponse();
        $response->setData(array(
            "customer_uuid" => $uuid
        ));
        return $response;
    }

    public function locationAction()
    {
        $request = Request::createFromGlobals();
        $jsonData = $request->getContent();
        $data = json_decode($jsonData, true);
        $customer_uuid = isset($data['customer_uuid'])? $data['customer_uuid']: null;
        $location_name = isset($data['location_name']) ? $data['location_name'] : '';
        $location_type_id = isset($data['location_type_id']) ? $data['location_type_id'] : 0;
        $location_address = isset($data['location_address']) ? $data['location_address'] : '';

        $uuid = Uuid::uuid4();
        $em = $this->getDoctrine()->getManager();
        $em->getConnection()->beginTransaction();
        try {

            $location = new EmsLocationMaster();
            $location->setLocationId($uuid->getBytes())
                ->setLocationName($location_name)
                ->setLocationTypeId($location_type_id)
                ->setLocationAddress($location_address)
                ->setLocationCreateDateTime(new \DateTime("now"));

            $em->persist($location);
            $em->flush();

            if ( $customer_uuid != null ) {
                $customerLocationMapping = new EmsCustomerLocationMapping();
                $customerLocationMapping->setCustomerId(hex2bin($customer_uuid))
                    ->setLocationId($uuid->getBytes())
                    ->setStatusId(0)
                    ->setDate(new \DateTime('now'));
                $em->persist($customerLocationMapping);
                $em->flush();
            }

            $em->persist($location);
            $em->flush();
            $em->getConnection()->commit();
        } catch (Exception $e) {
            $em->getConnection()->rollBack();
            throw $e;
        }

        $response = new JsonResponse();
        $response->setData(array(
            "location_uid" => $uuid->toString()
        ));
        return $response;
    }

    public function gatewayAction()
    {
        $request = Request::createFromGlobals();
        $jsonData = $request->getContent();
        $data = json_decode($jsonData, true);

        $location_uuid = isset($data['location_uuid'])? $data['location_uuid']: null;
        $gateway_name = isset($data['gateway_name']) ? $data['gateway_name'] : '';
        $gateway_ethernet_mac = isset($data['gateway_ethernet_mac']) ? $data['gateway_ethernet_mac'] : '';
        $gateway_hw_id = isset($data['gateway_hw_id']) ? $data['gateway_hw_id'] : 0;
        $gateway_sw_version = isset($data['gateway_sw_version']) ? $data['gateway_sw_version'] : '';
        $gateway_meta = isset($data['gateway_meta']) ? $data['gateway_meta'] : '';
        $status_id = isset($data['status_id']) ? $data['status_id'] : 0;

        $uuid = Uuid::uuid4();
        $em = $this->getDoctrine()->getManager();
        $em->getConnection()->beginTransaction();
        try {
            $gateway = new EmsGatewayMaster();
            $gateway->setGatewayId($uuid->getBytes())
                ->setGatewayName($gateway_name)
                ->setGatewayEthernetMac($gateway_ethernet_mac)
                ->setGatewayHwId($gateway_hw_id)
                ->setGatewaySwVersion($gateway_sw_version)
                ->setGatewayMeta($gateway_meta)
                ->setStatusId($status_id);

            $em->persist($gateway);
            $em->flush();

            if ($location_uuid != null) {
                $locationGatewayMapping = new EmsLocationGatewayMapping();
                $locationGatewayMapping->setLocationId(hex2bin($location_uuid))
                    ->setGatewayId($uuid->getBytes())
                    ->setStatusId(0)
                    ->setDate(new \DateTime('now'));
                $em->persist($locationGatewayMapping);
                $em->flush();
            }
            $em->getConnection()->commit();
        } catch (Exception $e) {
            $em->getConnection()->rollBack();
            throw $e;
        }

        $response = new JsonResponse();
        $response->setData(array(
            "gateway_uid" => $uuid->toString()
        ));
        return $response;
    }

    public function testStationAction(EvChargingStationMaster $station)
    {
        $em = $this->get('doctrine')->getManager('iothub_lab');
        $em->getConnection()->connect();
        $connected = $em->getConnection()->isConnected();
        print_r($connected);
	return $connected;
/*
        $em = $this->get('doctrine')->getManager();
        $location = $em->getRepository('AppBundle:EmsLocationMaster')->findByChargingStation($station);
        $addr = $location->getLocationAddress();
        $name = $location->getLocationName();
        echo($addr);
        echo($name);
*/ 
   }
    
    public function syncZipCodeAction(Request $request)
    {
        $json = file_get_contents('https://quality.data.gov.tw/dq_download_json.php?nid=5948&md5_url=871400654c4091a72388c7587ce3c70f');
        $obj = json_decode($json);
        if(empty($obj)){
            return $this->returnJsonResponse(
                JsonResponse::HTTP_BAD_REQUEST,
                'zipcode api failed.',
                substr($json, 0, 200)
            );
        }
        
        $em = $this->getDoctrine()->getManager();
        
        $fromDB = $em->getRepository("AppBundle:ZipCode")->findAll();
        $old = [];
        if( ! empty($fromDB)){
            foreach ($fromDB as $row){
                $old[$row->getCode()] = $row->getCity().$row->getArea();    
            }
        }
        
        $listCode = [];
        $allData = [];
        $modify = [];
        $new = [];
        foreach ($obj as $row){
            $zip3 = substr($row->Zip5, 0, 3);
            if( ! in_array($zip3, $listCode)){
                $listCode[] = $zip3;
                
                //Diff
                if(isset($old[$zip3])){
                    if($old[$zip3] !== $row->City.$row->Area){
                        $modify[] = $zip3;
                    }
                }else{
                    $new[] = $zip3;
                }
                
                $allData[] = $row;
            }            
        }
        
        
        if(boolval($request->get('save'))){
            foreach ($allData as $row){
                $zip3 = substr($row->Zip5, 0, 3);

                $zipcode = null;
                if(in_array($zip3, $modify)){
                    $zipcode = $em->find('AppBundle:ZipCode', $zip3);   
                
                }elseif(in_array($zip3, $new)){
                    $zipcode = new ZipCode();
                }
                
                if(is_object($zipcode)){
                    $zipcode->setCode($zip3);
                    $zipcode->setCity($row->City);
                    $zipcode->setArea($row->Area);
                    
                    $em->persist($zipcode);
                    $em->flush();
                }
            }
        }


        return $this->returnJsonResponse(
            JsonResponse::HTTP_OK,
            boolval($request->get('save')) ? 'Save success.' : 'Debug',
            [
                'new' => $new,
                'modify' => $modify,
            ]
        );
    }


    public function syncCWBAction(Request $request)
    {
        $json = file_get_contents('https://quality.data.gov.tw/dq_download_json.php?nid=5948&md5_url=871400654c4091a72388c7587ce3c70f');
        $obj = json_decode($json);
        if(empty($obj)){
            return $this->returnJsonResponse(
                JsonResponse::HTTP_BAD_REQUEST,
                'zipcode api failed.',
                substr($json, 0, 200)
            );
        }

        $em = $this->getDoctrine()->getManager();

        $fromDB = $em->getRepository("AppBundle:ZipCode")->findAll();
        $old = [];
        if( ! empty($fromDB)){
            foreach ($fromDB as $row){
                $old[$row->getCode()] = $row->getCity().$row->getArea();
            }
        }

        $listCode = [];
        $allData = [];
        $modify = [];
        $new = [];
        foreach ($obj as $row){
            $zip3 = substr($row->Zip5, 0, 3);
            if( ! in_array($zip3, $listCode)){
                $listCode[] = $zip3;

                //Diff
                if(isset($old[$zip3])){
                    if($old[$zip3] !== $row->City.$row->Area){
                        $modify[] = $zip3;
                    }
                }else{
                    $new[] = $zip3;
                }

                $allData[] = $row;
            }
        }


        if(boolval($request->get('save'))){
            foreach ($allData as $row){
                $zip3 = substr($row->Zip5, 0, 3);

                $zipcode = null;
                if(in_array($zip3, $modify)){
                    $zipcode = $em->find('AppBundle:ZipCode', $zip3);

                }elseif(in_array($zip3, $new)){
                    $zipcode = new ZipCode();
                }

                if(is_object($zipcode)){
                    $zipcode->setCode($zip3);
                    $zipcode->setCity($row->City);
                    $zipcode->setArea($row->Area);

                    $em->persist($zipcode);
                    $em->flush();
                }
            }
        }


        return $this->returnJsonResponse(
            JsonResponse::HTTP_OK,
            boolval($request->get('save')) ? 'Save success.' : 'Debug',
            [
                'new' => $new,
                'modify' => $modify,
            ]
        );
    }
    
    
    protected function returnJsonResponse($code, $msg = null, $data = null)
    {
        $return = array();
        $return['code'] = $code;

        if($code === JsonResponse::HTTP_OK){
            $httpcode = $code;

            if($data !== null){
                $return['data'] = $data;
            }
            if($msg !== null){
                $return['msg'] = $msg;
            }
        }else{
            $httpcode = JsonResponse::HTTP_CREATED;

            if($data !== null){
                $return['data'] = $data;
            }
            if($msg !== null){
                $return['msg'] = $msg;
            }
        }

        return new JsonResponse($return, $httpcode);
    }
}
