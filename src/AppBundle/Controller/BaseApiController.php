<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
class BaseApiController extends Controller
{
    protected $jresp;
    protected $data;
    
    
    protected function getDefaultJsonResp(Request $request) {
        $this->data = array();
        $this->data['code'] = JsonResponse::HTTP_OK;
        $this->data['title'] = '';
        $this->data['message'] = '';
        $this->data['data'] = array();
        $this->jresp = new JsonResponse();
        $callback = $request->get('callback');
        if ($callback) {
            $this->jresp->setCallback($callback);
        }
        return $this->jresp;
    }

    protected function returnJsonResponse($request, $respData)
    {
        $response = new JsonResponse($respData, $respData['code']);

        $response->headers->set('Access-Control-Allow-Origin', '*');
        $callback = $request->get('callback');
        if ($callback) {
            $response->setCallback($callback);
        }
        return $response;
    }
}

