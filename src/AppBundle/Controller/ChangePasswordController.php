<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Controller;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\UserBundle\Controller\ChangePasswordController as BaseController;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controller managing the password change
 *
 * @author Thibault Duplessis <thibault.duplessis@gmail.com>
 * @author Christophe Coevoet <stof@notk.org>
 */
class ChangePasswordController extends BaseController
{
    /**
     * Change user password
     */
    public function changePasswordAction(Request $request)
    {
        $jsonp = new JsonResponse();
        $data = array();
        $data["title"] = $this->get('translator')->trans("label.title.changepassword");
        if ($request->get('callback')) {
            $jsonp->setCallback($request->get('callback'));
        }
        
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::CHANGE_PASSWORD_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->get('fos_user.change_password.form.factory');

        $form = $formFactory->createForm();
        $form->setData($user);

        $form->handleRequest($request);

        if ( $form->isSubmitted() && $form->isValid()) {
            /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
            $userManager = $this->get('fos_user.user_manager');

            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(FOSUserEvents::CHANGE_PASSWORD_SUCCESS, $event);

            $userManager->updateUser($user);

            if (null === $response = $event->getResponse()) {
                //$url = $this->generateUrl('fos_user_profile_show');
                $response = $jsonp;
            }

            $dispatcher->dispatch(FOSUserEvents::CHANGE_PASSWORD_COMPLETED, new FilterUserResponseEvent($user, $request, $response));
            $data["message"] = $this->get('translator')->trans("message.update.success");
            if ($request->isXmlHttpRequest()) {
                $jsonp->setData($data);
                return $jsonp;
            } /*else {
                return $response;
            }*/
        } else {
            $data['other'] = "form invalid";
//            $data['more'] = count($form->getErrors(true));
//            $data['form'] = $form->getErrors(true)[0]->getMessage();
        }

        if ($request->isXmlHttpRequest() && $request->getMethod() == "POST") {
            $data['message'] =  $this->get('translator')->trans('message.update.fail');
            $jsonp->setData($data);
            $jsonp->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
            return $jsonp;
        } else {
            if ( isset($data["message"])) $message = $data["message"];
            else if ( $form->getErrors(true) != "" ) {
                $message = $this->get('translator')->trans('message.update.fail');
            } else {
                $message = null;
            }
            return $this->render('@App\ChangePassword\change_password.html.twig', array(
                'form' => $form->createView(),
                'message' => $message
            ));
        }
    }
}
