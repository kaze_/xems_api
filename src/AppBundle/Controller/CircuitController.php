<?php

namespace AppBundle\Controller;

use AppBundle\Entity\EmsCircuitMaster;
use AppBundle\Entity\EmsCircuitStructure;
use AppBundle\Entity\EmsDeviceMaster;
use AppBundle\Entity\EmsSmartRelayTransactionLog;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\EmsMeterCircuitMapping;
use FOS\RestBundle\Controller\Annotations\QueryParam;

class CircuitController extends BaseApiController
{
    // demand interval = 15 minutes = 900
    const demandInterval = 900;

    public function allAction(Request $request)
    {
        $this->denyAccessUnlessGranted('view', new EmsCircuitMaster());
        $type = $request->get('type');
        $chargingStatus = $request->get('chargingStatus');
        $status = $request->get('status');
        // $vip = $request->get('vip');
//        $order = $request->get('order');
//        $orderType = $request->get('orderType');
        $view = View::create();
        $repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:EmsCircuitMaster');

        switch($type) {
            case 'report':
                $context = (new Context())->addGroup('report');
                $context->addGroup('status');
                // $circuits = $repository->findBy([], ['lastDataReport' => 'DESC']);
                $circuits = $repository->createQueryBuilder('c')
                    ->innerJoin('AppBundle:EmsMeterCircuitMapping', 'mc_mapping', 'WITH', 'c.circuitId = mc_mapping.circuit')
                    ->innerJoin('AppBundle:EmsGatewayDeviceMapping', 'gd_mapping', 'WITH', 'mc_mapping.device = gd_mapping.deviceId')
                    ->innerJoin('AppBundle:EmsLocationGatewayMapping', 'lg_mapping', 'WITH', 'gd_mapping.gatewayId = lg_mapping.gatewayId')
                    ->innerJoin('AppBundle:EmsLocationMaster', 'l', 'WITH', 'lg_mapping.locationId = l.locationId')
                    ->innerJoin('AppBundle:EmsLocationType', 'lt', 'WITH', 'l.locationType = lt.locationTypeId')
                    ->select('l.locationName, lt.locationTypeId, c.circuitId, c.circuitName, c.evChargingStation, c.lastDataReport, c.newestData')
                    ->where('l.locationStatus = 1')
                    ->addOrderBy('l.locationName', 'ASC')
                    ->addOrderBy('c.circuitName', 'ASC')
                    ->getQuery()->getResult(); 
            break;
            default:
                $context = (new Context())->addGroup('list'); 
                $circuits = $repository->findAll();
        }

        $deviceData = $this->get('app.device_data');
        $newCircuits = [];
        foreach ($circuits as $circuit) {
            if ( $chargingStatus == 'true' && $circuit['evChargingStation'] == true) {
                // get charging & relay status
                $data = $deviceData->getCircuitChargingStatus($circuit['circuitId']);
                if ( $data ){
                    $relayStatus = $data['relayStatus'];
                    $circuit['equipment'] = $data['equipment'];
                    $circuit['powerSwitch'] = $data['powerSwitch'];
                    if ( isset($relayStatus['logCurrent']) ) 
                        $circuit['aAverage'] = $relayStatus['logCurrent'];
                }
            } else {
            // get circuit current
                $logRepo = $this->getDoctrine()->getManager()->getRepository('AppBundle:EmsPowerReportLog');
                $log = $logRepo->createQueryBuilder('c')
                    ->select('c.aA, c.aB, c.aC, c.aAverage')
                    ->where('c.circuitId = :circuitId')
                    ->andWhere('c.reportDatetime > :datetime')
                    ->orderBy('c.reportDatetime', 'DESC')
                    ->setMaxResults(1)
                    ->setParameter('circuitId', $circuit['circuitId'])
                    ->setParameter('datetime', (new \Datetime())->modify('-1 days'))
                    ->getQuery()->getOneOrNullResult();
                if ( $log ) {
                    $circuit['aA'] = $log['aA'];
                    $circuit['aB'] = $log['aB'];
                    $circuit['aC'] = $log['aC'];
                }
            }

            $circuit['id'] = $circuit['circuitId']->toString();
            unset($circuit['circuitId']);
            $newCircuits[] = $circuit;
        }
        $view->setContext($context);
        $view->setStatusCode(200);
        $view->setData(array('code' => 200, 'data' => $newCircuits));
        return $view;
    }

    /**
     * Finds and displays a circuitMaster entity.
     *
     */
    public function getAction(Request $request, EmsCircuitMaster $circuit)
    {
        $this->denyAccessUnlessGranted('view', $circuit);
        $view = View::create();
        $context = (new Context())->addGroup('list');

        $status = $request->get('status');
        $detail = $request->get('detail');

        if ( $detail == 'true' ) {
            $context->addGroup('circuit');
        }
        if ( $status == 'true' ) {
            $context->addGroup('status');
            $repository = $this->getDoctrine()->getManager()
                ->getRepository('AppBundle:EmsPowerReportLog');
            $currentStatus = $repository->findCurrentStatusByCircuit($circuit->getCircuitId());
            if ($currentStatus != null) {
                $charge = $this->get('app.power_report')->getCircuitFee($circuit, new \DateTime('first day of this month'), null);
                if ( $charge ) {
                    $currentStatus['fee'] = $charge['amount'];
                }
                $circuit->setCurrentStatus($currentStatus);
            }
            //$this->queryCurrentStatus($circuit);
        }
        $view->setContext($context);
        $view->setStatusCode(200);
        $view->setData(array('code' => 200, 'data' => $circuit));
        return $view;
    }

    protected function queryCurrentStatus($circuit)
    {
        $repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:EmsPowerReportLog');
        $currentStatusObj = $repository->findNewestDataByCircuit(
            array("aA", "aB", "aC", "aAverage",
                "vA", "vB", "vC", "vAverage",
                "kwA", "kwB", "kwC", "kwTotal", "currDmdKw", "circuitId"),
            array("circuit" => $circuit->getCircuitId()),
            // array("circuit" => $circuit['circuitId'])
            true
        );
        $currentStatus = array();
        if ( $currentStatusObj != null ) {
            $currentStatus = array();
            $currentStatus['time'] = $currentStatusObj->getReportDateTime();
            $currentStatus['power'] = $currentStatusObj->getkwTotal();
            $currentStatus['current'] = $currentStatusObj->getAAverage();
            $currentStatus['voltage'] = $currentStatusObj->getVAverage();
            $currentStatus['demand'] = $currentStatusObj->getCurrDmdKw();
            $circuit->setCurrentStatus($currentStatus);
            // $circuit['currentStatus'] = $currentStatus;
        }

    }

    /**
     * Creates a new circuitMaster entity.
     */
    public function newAction(Request $request)
    {
        // permission set in security.yml
        $circuit = new EmsCircuitMaster();
        $this->denyAccessUnlessGranted('edit', $circuit);

        return $this->processForm($request, $circuit);
    }

    /**
     * Displays a form to edit an existing circuitMaster entity.
     */
    public function editAction(Request $request, EmsCircuitMaster $circuit)
    {
        $this->denyAccessUnlessGranted('edit', $circuit);
        return $this->processForm($request, $circuit);
    }

    private function processForm($request, $circuit)
    {
        $view = View::create();
        $context = (new Context())->addGroup('list');

        $em = $this->getDoctrine()->getManager();
        $statusCode = $em->contains($circuit) ? 200:201;

        $data = json_decode($request->getContent(), true);
        $form = $this->createForm('AppBundle\Form\EmsCircuitMasterType', $circuit );
        $clearMissing = ($request->getMethod() == 'PATCH') ? false : true;
        $form->submit($data, $clearMissing);

        if ( $statusCode == 201 ) {
            $required = [ 
                //'powerMeterDeviceId', 
                'evChargingStation', 
                'circuitName', 'circuitMaxCurrent', 'circuitDesc', 'circuitCategory', 'circuitStandbyCurrent'
                ];
            foreach( $required as $var_name ) {
                if ( ! isset( $data[$var_name] ) )  {
                    $view->setData(array('code' => Response::HTTP_BAD_REQUEST,
                        'message' =>  $var_name . ' cannot be null.'));
                    return $view;
                }
            }
        }

        $meterId = isset($data['powerMeterDeviceId']) ? $data['powerMeterDeviceId']: null;
        $evChargingStation = $data['evChargingStation'];
        
        $meter = null;
        if ( $meterId != null) {
            $meterCircuitMapping = $this->getDoctrine()->getRepository('AppBundle:EmsMeterCircuitMapping');
            if ( $statusCode == 200 ) {
                if ($meterCircuitMapping->find(array(
                    "circuitId" => $circuit->getCircuitId(),
                    "status" => 1
                ))) {
                    $view->setStatusCode(Response::HTTP_BAD_REQUEST);
                    $view->setData(array(
                        'code' => Response::HTTP_BAD_REQUEST,
                        'message' => 'this circuit is already link to another gateway, unlink first'
                    ));
                    return $view;
                }
            }
            $meter = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:EmsPowerMeter')
            ->find($meterId);
            if ( $meter == null) {
                $view->setStatusCode(Response::HTTP_BAD_REQUEST);
                $view->setData(array(
                    'code' => Response::HTTP_BAD_REQUEST,
                    'message' => 'device ID does not exist.'
                ));
                return $view;
            }
        }
        
        if ( $evChargingStation == 0 ) {
            $circuit->setEvChargingStation(false);
        } else {
            $circuit->setEvChargingStation(true);
        }
        
        if ( $form->isValid() ) {
            if ( $statusCode === 201 ) {
                $em->persist($circuit);
                $view->setLocation($this->generateUrl('xems_circuits_get', array('circuit' => $circuit->getCircuitId()->toString())));
            }
            $em->flush();
            $view->setContext($context);
            $view->setStatusCode($statusCode);
            $view->setData(array('code' => $statusCode, 'data' => $circuit));
            if ( $meterId != null ) {
                $mapping = new EmsMeterCircuitMapping();
                $mapping->setDevice($meter);
                $mapping->setCircuit($circuit);
                $mapping->setChannel(isset($data['channel'])? $data['channel']: 0);
                $mapping->setStatusId(1);
                $em->persist($mapping);
                $em->flush();
            }
            
        } else {
            $view->setStatusCode(400);
            $view->setData(array('code' => 400, 'data' => $form->getErrors()));
        }
        return $view;
    }

    /**
     * Deletes a circuitMaster entity.
     *
     */
    public function deleteAction(Request $request, EmsCircuitMaster $circuitMaster)
    {
        $form = $this->createDeleteForm($circuitMaster);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($circuitMaster);
            $em->flush();
        }

        return $this->redirectToRoute('circuit_index');
    }

    /**
     * Creates a form to delete a circuitMaster entity.
     *
     * @param EmsCircuitMaster $circuitMaster The gatewayMaster entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(EmsCircuitMaster $circuitMaster)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('circuit_delete', array('gatewayUuid' => $circuitMaster->getCircuitId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    // power report

    /**
     * @param Request $request
     * @param ParamFetcher $paramFetcher
     * @param EmsCircuitMaster $circuit
     * @return JsonResponse
     *
     * use paramFetcher in FOSRestBundle to check parameters.
     * @QueryParam(name="from", requirements="[A-Z\d\-: ]+", strict=true, nullable=false)
     * @QueryParam(name="to", requirements="[A-Z\d\-: ]+", strict=true, nullable=false)
     * @QueryParam(name="offset", requirements="\d+", default=0)
     * @QueryParam(name="limit", requirements="\d+", default=0)
     */
    public function powerAction(Request $request, ParamFetcher $paramFetcher, EmsCircuitMaster $circuit)
    {
        $this->denyAccessUnlessGranted('view', $circuit);

        $from = $paramFetcher->get('from');
        $to = $paramFetcher->get('to');
        $limit = $paramFetcher->get('limit');
        $offset = $paramFetcher->get('offset');

        $startDate = (new \DateTime())->setTimestamp(strtotime($from));
        $endDate =(new \DateTime())->setTimestamp(strtotime($to));
        if ( $startDate == null || $endDate == null ) {
            return $this->returnJsonResponse($request, array(
                'code' => JsonResponse::HTTP_BAD_REQUEST
            ));
        }

        $repository = $this->getDoctrine()->getManager()
            ->getRepository('AppBundle:EmsPowerReportLog');

        $phaseJson = $circuit->getCircuitPhase();
        $phase = json_decode($phaseJson);
        if ( $phase ) {
            $fields = ['kwTotal'];
            foreach( $phase as $p ) {
                $fields[]  = 'kw'.strtoupper($p);
            }
        } else {
            $fields = array('kwA', 'kwB', 'kwC', 'kwTotal');
        }
        $result = $repository->findDataByCircuit(
//            array('kwA', 'kwB', 'kwC', 'kwTotal'),
            $fields, array(
                'startDate' => $startDate,
                'endDate' => $endDate,
                'circuit' => $circuit,
                'interval' => $this::demandInterval),
            array('reportDatetime' => 'ASC'), $limit, $offset
        );

        $currData = array();
        foreach($result as $data) {
            //$data['time'] = $data['reportDatetime']->format('Y-m-d H:i:s');
            unset($data['reportDatetime']);
            // unset($data['timekey']);
            if ( $data['kwTotal'] == null ) {
                $a = ($data['kwA'] != null) ? $data['kwA'] : 0;
                $b = ($data['kwB'] != null) ? $data['kwB'] : 0;
                $c = ($data['kwC'] != null) ? $data['kwC'] : 0;
                $data['kwTotal'] = $a + $b + $c;
            }

            $currData[] = $data;
        }

        return $this->returnJsonResponse($request, array(
            "code" => JsonResponse::HTTP_OK,
            "data" => $currData
        ));
    }


    // power report

    /**
     * @param Request $request
     * @param ParamFetcher $paramFetcher
     * @param EmsCircuitMaster $circuit
     * @return JsonResponse
     *
     * * use paramFetcher in FOSRestBundle to check parameters.
     * @QueryParam(name="from", requirements="[A-Z\d\-: ]+", strict=true, nullable=false)
     * @QueryParam(name="to", requirements="[A-Z\d\-: ]+", strict=true, nullable=false)
     * @QueryParam(name="offset", requirements="\d+", default=0)
     * @QueryParam(name="limit", requirements="\d+", default=0)
     */
    public function currentAction(Request $request, ParamFetcher $paramFetcher, EmsCircuitMaster $circuit)
    {
        $this->denyAccessUnlessGranted('view', $circuit);

        $from = $paramFetcher->get('from');
        $to = $paramFetcher->get('to');
        $limit = $paramFetcher->get('limit');
        $offset = $paramFetcher->get('offset');

        $startDate = (new \DateTime())->setTimestamp(strtotime($from));
        $endDate =(new \DateTime())->setTimestamp(strtotime($to));
        if ( $startDate == null || $endDate == null ) {
            return $this->returnJsonResponse($request, array(
                'code' => JsonResponse::HTTP_BAD_REQUEST
            ));
        }

        $repository = $this->getDoctrine()->getManager()
            ->getRepository('AppBundle:EmsPowerReportLog');

        $phaseJson = $circuit->getCircuitPhase();
        $phase = json_decode($phaseJson);
        if ( $phase ) {
            $fields = ['aAverage'];
            foreach( $phase as $p ) {
                $fields[]  = 'a'.strtoupper($p);
            }
        } else {
            $fields = array('aA', 'aB', 'aC', 'aAverage');
        }

        $result = $repository->findDataByCircuit(
            // array("aA", "aB", "aC", "aAverage"),
            $fields, array(
                'startDate' => $startDate,
                'endDate' => $endDate,
                'circuit' => $circuit,
                'interval' => $this::demandInterval),
            array('reportDatetime' => 'ASC'), $limit, $offset
        );
        $currData = array();
        foreach($result as $data) {
            //$data['time'] = $data['reportDatetime']->format('Y-m-d H:i:s');
            unset($data['reportDatetime']);
            //unset($data['timekey']);
            if ( $data['aAverage'] === null ) {
                $total = 0;
                $sum = 0;
                if ( isset($data['aA'])) {
                    $total++;
                    $sum = $sum + ($data['aA'] != null) ?  $data['aA'] : 0; 
                }
                if ( isset($data['aB'])) {
                    $total++; 
                    $sum = $sum + ($data['aB'] != null) ?  $data['aB'] : 0; 
                }
                if ( isset($data['aC'])) {
                    $total++; 
                    $sum = $sum + ($data['aC'] != null) ?  $data['aC'] : 0; 
                }
                $data['aAverage'] = $sum / $total;    
            }
            $currData[] = $data;
        }

        return $this->returnJsonResponse($request, array(
            "code" => JsonResponse::HTTP_OK,
            "data" => $currData
        ));
    }

    public function latestCurrentAction(Request $request, EmsCircuitMaster $circuit)
    {
        $this->denyAccessUnlessGranted('view', $circuit);

        $repository = $this->getDoctrine()->getManager()
            ->getRepository('AppBundle:EmsPowerReportLog');

        $phaseJson = $circuit->getCircuitPhase();
        $parentPhaseJson = $circuit->getParentPhase();
        $phase = json_decode($phaseJson);
        $parentPhase = json_decode($parentPhaseJson, true);
        if ( $phase ) {
            $fields = ['aAverage'];
            foreach( $phase as $p ) {
                $fields[]  = 'a'.strtoupper($p);
            }
        } else {
            $fields = array('aA', 'aB', 'aC', 'aAverage');
        }

        $data = $repository->findNewestDataByCircuit(
            $fields, array('circuit' => $circuit)
        );

        // unset($data['reportDatetime']);
            if ( $data['aAverage'] === null ) {
                $total = 0;
                $sum = 0;
                if ( isset($data['aA'])) {
                    $total++;
                    $sum = $sum + ($data['aA'] != null) ?  $data['aA'] : 0; 
                }
                if ( isset($data['aB'])) {
                    $total++; 
                    $sum = $sum + ($data['aB'] != null) ?  $data['aB'] : 0; 
                }
                if ( isset($data['aC'])) {
                    $total++; 
                    $sum = $sum + ($data['aC'] != null) ?  $data['aC'] : 0; 
                }
                if ( $total != 0 )
                    $data['aAverage'] = $sum / $total;    
                else
                    $data['aAverage'] = 0;
            }
        
        $data['parentPhase'] = $parentPhase;
        return $this->returnJsonResponse($request, array(
            "code" => JsonResponse::HTTP_OK,
            "data" => $data,
        ));
    }

    public function currentCurrentAction(Request $request, EmsCircuitMaster $circuit)
    {
        $this->denyAccessUnlessGranted('view', $circuit);

        $repository = $this->getDoctrine()->getManager()
            ->getRepository('AppBundle:EmsPowerReportLog');

        $report = $repository->findNewestDataByCircuit(
            array("aA", "aB", "aC", "aAverage"),
            array("circuit" => $circuit));

        if ($report == null || count($report) == 0 ) {
            return $this->returnJsonResponse($request, array(
                'code' => JsonResponse::HTTP_NOT_FOUND,
                'message' => 'No Data'
            ));
        }
        $report["time"] = $report["reportDatetime"]->format('Y-m-d H:i:s');
        unset($report["reportDatetime"]);

        $a = ($report['aA'] != null) ? $report['aA'] : 0;
        $b = ($report['aB'] != null) ? $report['aB'] : 0;
        $c = ($report['aC'] != null) ? $report['aC'] : 0;
        $report['aSummary'] = $a + $b + $c;
        if ( $report['aAverage'] == null ) {
            $report['aAverage'] = $report['aSummary']/3;
        }
        // max
        $endDate = new \DateTime();
        $startToday = (new \DateTime())->setTime(0, 0, 0);
        $startThisMonth = (\DateTime::createFromFormat('Y-m-d', date('Y-m-01')))->setTime(0, 0, 0);

        $maxToday = $repository->findMaxCurrByCircuit(array(
            "circuit" => $circuit,
            "startDate" => $startToday,
            "endDate" => $endDate
        ));
        $maxThisMonth = $repository->findMaxCurrByCircuit(array(
            "circuit" => $circuit,
            "startDate" => $startThisMonth,
            "endDate" => $endDate
        ));
    
        if ( count($maxToday) != 0 ) {
            $maxToday[0]["time"] = $maxToday[0]["reportDatetime"]->format('Y-m-d H:i:s');
            unset($maxToday[0]["reportDatetime"]);
            $report["max_today"] = $maxToday[0];
        } else {
            $report["max_today"] = null;
        }

        if ( count($maxThisMonth) != 0 ) {
            $maxThisMonth[0]["time"] = $maxThisMonth[0]["reportDatetime"]->format('Y-m-d H:i:s');
            unset($maxThisMonth[0]["reportDatetime"]);
            $report["max_this_month"] = $maxThisMonth[0];
        } else {
            $report["max_this_month"] = null;
        }

       return $this->returnJsonResponse($request, array(
            "code" => JsonResponse::HTTP_OK,
            "data" => $report
        ));
    }

    /**
     * @param Request $request
     * @param ParamFetcher $paramFetcher
     * @param EmsCircuitMaster $circuit
     * @return JsonResponse
     *
     * use paramFetcher in FOSRestBundle to check parameters.
     * @QueryParam(name="from", requirements="[A-Z\d\-: ]+", strict=true, nullable=false)
     * @QueryParam(name="to", requirements="[A-Z\d\-: ]+", strict=true, nullable=false)
     */
    public function maxCurrentAction(Request $request, ParamFetcher $paramFetcher, EmsCircuitMaster $circuit)
    {
        $this->denyAccessUnlessGranted('view', $circuit);

        $from = $paramFetcher->get('from');
        $to = $paramFetcher->get('to');

        $startDate = \DateTime::createFromFormat('Y-m-d H:i:s', $from);
        $endDate = \DateTime::createFromFormat('Y-m-d H:i:s', $to);

        $repository = $this->getDoctrine()->getManager()
            ->getRepository('AppBundle:EmsPowerReportLog');

        $result = $repository->findMaxCurrByCircuit(array(
            "startDate" => $startDate,
            "endDate" => $endDate,
            "circuit" => $circuit
        ));

        if ( count($result) == 0 ) {
            return $this->returnJsonResponse($request, array(
                'code' => JsonResponse::HTTP_NOT_FOUND,
                'message' => 'No Data'
            ));
        }
        $report = $result[0];
        $report["time"] = $report["reportDatetime"]->format('Y-m-d H:i:s');
        unset($report["reportDatetime"]);
        return $this->returnJsonResponse($request, array(
            'code' => JsonResponse::HTTP_OK,
            'data' => $report
        ));
    }

    /**
     * @param Request $request
     * @param ParamFetcher $paramFetcher
     * @param EmsDeviceMaster $device
     * @return JsonResponse
     *
     * use paramFetcher in FOSRestBundle to check parameters.
     * @QueryParam(name="from", requirements="[A-Z\d\-: ]+", strict=true, nullable=false)
     * @QueryParam(name="to", requirements="[A-Z\d\-: ]+", strict=true, nullable=false)
     */
    public function maxDemandAction(Request $request, ParamFetcher $paramFetcher, EmsCircuitMaster $circuit)
    {
        $this->denyAccessUnlessGranted('view', $circuit);

        $from = $paramFetcher->get('from');
        $to = $paramFetcher->get('to');

        $startDate = \DateTime::createFromFormat('Y-m-d H:i:s', $from);
        $endDate = \DateTime::createFromFormat('Y-m-d H:i:s', $to);

        $repository = $this->getDoctrine()->getManager()
            ->getRepository('AppBundle:EmsPowerReportLog');

        $result = $repository->findMaxDataByCircuit('currDmdKw', array('currDmdKw'), array(
            "startDate" => $startDate,
            "endDate" => $endDate,
            "circuit" => $circuit
        ));

        if ( count($result) == 0 ) {
            return $this->returnJsonResponse($request, array(
                'code' => JsonResponse::HTTP_NOT_FOUND,
                'message' => 'No Data'
            ));
        }
        $report = $result[0];
        $report["time"] = $report["reportDatetime"]->format('Y-m-d H:i:s');
        unset($report["reportDatetime"]);
        return $this->returnJsonResponse($request, array(
            'code' => JsonResponse::HTTP_OK,
            'data' => $report
        ));
    }

    public function currentDemandAction(Request $request, EmsCircuitMaster $circuit)
    {
        $this->denyAccessUnlessGranted('view', $circuit);
        $repository = $this->getDoctrine()->getManager()
            ->getRepository('AppBundle:EmsPowerReportLog');
        $report = $repository->findNewestDataByCircuit(
            array("reportDatetime", "currDmdKw", "currLgstdmdKw", "touALgstdmdKw", "touBLgstdmdKw", "touCLgstdmdKw", "touDLgstdmdKw"),
            array("circuit" => $circuit)
        );

        if ($report == null || count($report) == 0 ) {
            return $this->returnJsonResponse($request, array(
                'code' => JsonResponse::HTTP_NOT_FOUND,
                'message' => 'No Data'
            ));
        }
        $report["time"] = $report["reportDatetime"]->format('Y-m-d H:i:s');
        unset($report["reportDatetime"]);

        $endDate = new \DateTime();
        $startToday = (new \DateTime())->setTime(0, 0, 0);
        $startThisMonth = (\DateTime::createFromFormat('Y-m-d', date('Y-m-01')))->setTime(0, 0, 0);
        $maxToday = $repository->findMaxDataByCircuit(
            'currDmdKw',
            array('currDmdKw'),
            array(
                "startDate" => $startToday,
                "endDate" => $endDate,
                "circuit" => $circuit
            )
        );

        $maxThisMonth = $repository->findMaxDataByCircuit(
            'currDmdKw',
            array('currDmdKw'),
            array(
                "startDate" => $startThisMonth,
                "endDate" => $endDate,
                "circuit" => $circuit
            )
        );


        if ( count($maxToday) != 0 ) {
            $maxToday[0]["time"] = $maxToday[0]["reportDatetime"]->format('Y-m-d H:i:s');
            unset($maxToday[0]["reportDatetime"]);
            $report["max_today"] = $maxToday[0];
        } else {
            $report["max_today"] = null;
        }

        if ( count($maxThisMonth) != 0 ) {
            $maxThisMonth[0]["time"] = $maxThisMonth[0]["reportDatetime"]->format('Y-m-d H:i:s');
            unset($maxThisMonth[0]["reportDatetime"]);
            $report["max_this_month"] = $maxThisMonth[0];
        } else {
            $report["max_this_month"] = null;
        }

        return $this->returnJsonResponse($request, array(
            "code" => JsonResponse::HTTP_OK,
            "data" => $report
        ));
    }

    // power report

    /**
     * @param Request $request
     * @param ParamFetcher $paramFetcher
     * @param EmsCircuitMaster $circuit
     * @return JsonResponse
     *
     * use paramFetcher in FOSRestBundle to check parameters.
     * @QueryParam(name="from", requirements="[A-Z\d\-: ]+", strict=true, nullable=false)
     * @QueryParam(name="to", requirements="[A-Z\d\-: ]+", strict=true, nullable=false)
     * @QueryParam(name="offset", requirements="\d+", default=0)
     * @QueryParam(name="limit", requirements="\d+", default=0)
     */
    public function voltageAction(Request $request, ParamFetcher $paramFetcher, EmsCircuitMaster $circuit)
    {
        $this->denyAccessUnlessGranted('view', $circuit);

        $from = $paramFetcher->get('from');
        $to = $paramFetcher->get('to');
        $limit = $paramFetcher->get('limit');
        $offset = $paramFetcher->get('offset');

        $startDate = (new \DateTime())->setTimestamp(strtotime($from));
        $endDate =(new \DateTime())->setTimestamp(strtotime($to));
        if ( $startDate == null || $endDate == null ) {
            return $this->returnJsonResponse($request, array(
                'code' => JsonResponse::HTTP_BAD_REQUEST
            ));
        }

        $repository = $this->getDoctrine()->getManager()
            ->getRepository('AppBundle:EmsPowerReportLog');

        $phaseJson = $circuit->getCircuitPhase();
        $phase = json_decode($phaseJson);
        if ( $phase ) {
            $fields = ['vAverage'];
            foreach( $phase as $p ) {
                $fields[]  = 'v'.strtoupper($p);
            }
        } else {
            $fields = array('vA', 'vB', 'vC', 'vAverage');
        }

        $result = $repository->findDataByCircuit(
            // array('vA', 'vB', 'vC', 'vAverage'),
            $fields, array(
                'startDate' => $startDate,
                'endDate' => $endDate,
                'circuit' => $circuit,
                'interval' => $this::demandInterval),
            array('reportDatetime' => 'ASC'), $limit, $offset
        );

        $currData = array();
        foreach($result as $data) {
            // $data['time'] = $data['reportDatetime']->format('Y-m-d H:i:s');
            unset($data['reportDatetime']);
            // unset($data['timekey']);
            if ( $data['vAverage'] == null ) {
                $a = ($data['vA'] != null) ? $data['vA'] : 0;
                $b = ($data['vB'] != null) ? $data['vB'] : 0;
                $c = ($data['vC'] != null) ? $data['vC'] : 0;
                $data['vAverage'] = ($a + $b + $c)/3;
            }

            $currData[] = $data;
        }

        return $this->returnJsonResponse($request, array(
            "code" => JsonResponse::HTTP_OK,
            "data" => $currData
        ));
    }

    /**
     * @param Request $request
     * @param ParamFetcher $paramFetcher
     * @param EmsCircuitMaster $circuit
     * @return JsonResponse
     *
     * use paramFetcher in FOSRestBundle to check parameters.
     * @QueryParam(name="from", requirements="[A-Z\d\-: ]+", strict=true, nullable=false)
     * @QueryParam(name="to", requirements="[A-Z\d\-: ]+", strict=true, nullable=false)
     */
    public function kwhAction(Request $request, ParamFetcher $paramFetcher, EmsCircuitMaster $circuit)
    {
        $this->denyAccessUnlessGranted('view', $circuit);

        $from = $paramFetcher->get('from');
        $to = $paramFetcher->get('to');

        $startDate = (new \DateTime())->setTimestamp(strtotime($from));
        $endDate = (new \DateTime())->setTimestamp(strtotime($to));

        $repository = $this->getDoctrine()->getManager()
            ->getRepository('AppBundle:EmsPowerReportLog');

        $result = $repository->findkWhByCircuit(
            array(
                "circuit" => $circuit,
                "startDate" => $startDate,
                "endDate" => $endDate
            ));

        return $this->returnJsonResponse($request, array(
            "code" => JsonResponse::HTTP_OK,
            "data" => array(
                "kWh" => $result,
                "startDate" => $from,
                "endDate" => $to
            )
        ));
    }

    /**
     * @param Request $request
     * @param ParamFetcher $paramFetcher
     * @param EmsCircuitMaster $circuit
     * @return JsonResponse
     *
     * use paramFetcher in FOSRestBundle to check parameters.
     * @QueryParam(name="from", requirements="[A-Z\d\-: ]+", strict=true, nullable=false)
     * @QueryParam(name="to", requirements="[A-Z\d\-: ]+", strict=true, nullable=false)
     * @QueryParam(name="offset", requirements="\d+", default=0)
     * @QueryParam(name="limit", requirements="\d+", default=0)
     */
    public function demandWithStationsAction(Request $request, ParamFetcher $paramFetcher, EmsCircuitMaster $circuit)
    {
        $this->denyAccessUnlessGranted('view', $circuit);

        $from = $paramFetcher->get('from');
        $to = $paramFetcher->get('to');
        $limit = $paramFetcher->get('limit');
        $offset = $paramFetcher->get('offset');

        $startDate = (new \DateTime())->setTimestamp(strtotime($from));
        $endDate =(new \DateTime())->setTimestamp(strtotime($to));
        if ( $startDate == null || $endDate == null ) {
            return $this->returnJsonResponse($request, array(
                'code' => JsonResponse::HTTP_BAD_REQUEST
            ));
        }

        $powerReport = $this->get('app.power_report');

        // prepare data - get Customer Esp Package First
        $contract = null;
        $device = $circuit->getValidDevice();
        if ( $device ) {
            $espRepo = $this->getDoctrine()->getManager()->getRepository('EspBundle:EspCustomerMaster');
            $espCustomer = $espRepo->findOneBy(array('powerMeterDeviceId' => $device->getDeviceId()));
            if ( $espCustomer ) {
                $fee = $this->get('esp.fee');
                $rate = $fee->getCustomerRate($espCustomer);
                $contract = array();
                if ($rate->getNormalContract()) {
                    $contract['normalContract'] = $rate->getNormalContract();
                }
                if ($rate->getSemiPeakContract()) {
                    $contract['semiPeakContract'] = $rate->getSemiPeakContract();
                }
                if ($rate->getSaturdaySemiPeakContract()) {
                    $contract['saturdaySemiPeakContract'] = $rate->getSaturdaySemiPeakContract();
                }
                if ($rate->getOffPeakContract()) {
                    $contract['offPeakContract'] = $rate->getOffPeakContract();
                }
            }
        }

        $result = $powerReport->getCircuitDemandWithStations($circuit,
            array(
                'startDate' => $startDate,
                'endDate' => $endDate,
                'interval' => $this::demandInterval,
                'limit' => $limit,
                'offset' => $offset
            ), array('contract' => $contract,
                'energyPackageId' => $rate->getEnergyPackage()->getEnergyPackageId()
            )
        );

        if ( $result == null ) {
            return $this->returnJsonResponse($request, array(
                'code' => JsonResponse::HTTP_NOT_FOUND,
                'msg' => 'no charging stations found'
            ));
        }
        return $this->returnJsonResponse($request, array(
            "code" => JsonResponse::HTTP_OK,
            "data" => $result
        ));
    }

    public function getChargingStationSessionsAction(Request $request, EmsCircuitMaster $circuit)
    {
        $view = View::create();
        $context = (new Context())->addGroup('list');

        $from = $request->get('from');
        $to = $request->get('to');
        $limit = $request->get('limit');
        $offset = $request->get('offset');

        $station = $circuit->getChargingStation();
        if ( !$station || count($station) == 0 ) {
            $view->setStatusCode(Response::HTTP_OK);
            $view->setData(array('code' => Response::HTTP_OK,
                'data' => array()
            ));
            return $view;
        }
        $condition = array(
            'station' => $station[0]->getChargingStationId(),
            'from' => $from,
            'to' => $to);
        $ordering = array('startAt' => 'DESC');
        $repo = $this->getDoctrine()->getManager()->getRepository('EvBundle:EvChargingSessionLog');
        $res = $repo->findByStation($condition, $ordering, $limit, $offset);

        $view->setContext($context);
        $view->setStatusCode(200);
        $view->setData(array('code' => 200, 'data' => $res));
        return $view;
        // $res = $repo->findBy(array('chargingStaion' => $station), array('startAt' => 'DESC'),  );
    }

    public function changeStatusAction(Request $request)
    {
        $logger = $this->get('logger');
        $circuitId = $request->get('circuitId');
        $circuitRepo = $this->getDoctrine()->getManager()->getRepository('AppBundle:EmsCircuitMaster');

        try {
            $circuit = $circuitRepo->find($circuitId);
        } catch ( \EXCEPTION $e ) {
             return $this->returnJsonResponse($request, array(
                'code' => JsonResponse::HTTP_NOT_FOUND,
                'msg' => 'circuit not found'
            ));
        }

        if ( !$circuit ) {
            return $this->returnJsonResponse($request, array(
                'code' => JsonResponse::HTTP_NOT_FOUND,
                'msg' => 'circuit not found'
            ));
        }
        $status = $request->get('status');

        //$message = $request->get('msg');
        $message = $request->get('message');
        $event_time = null;
        if ( $message ) {
            $json_args = json_decode($message, true);
            if ( isset($json_args['event_time']) ) {
                try {
                    $event_time = \DateTime::createFromFormat('Y-m-d H:i:s', $json_args['event_time']);
                } catch (Exception $e) {
                    $event_time = null;
                }
            }
        } else {
            $json_args = null;
        }


        $relayStatus = 0;
        switch($status) {
            case 'Started':
                $relayStatus = EmsSmartRelayTransactionLog::ACT_STARTED;
                break;
            case 'Ending':
                $relayStatus = EmsSmartRelayTransactionLog::ACT_ENDING;
                break;
            case 'Ended':
                $relayStatus = EmsSmartRelayTransactionLog::ACT_ENDED;
                break;
            case 'NoChange':
                //$relayStatus = EmsSmartRelayTransactionLog::ACT_METERON;
                $relayStatus = EmsSmartRelayTransactionLog::ACT_NOCHANGE;
                break;
            case 'Unplugged':
                $relayStatus = EmsSmartRelayTransactionLog::ACT_UNPLUGGED;
                break;
            case 'MeterOff':
                $relayStatus = EmsSmartRelayTransactionLog::ACT_METEROFF;
                break;
            case 'MeterOn':
                $relayStatus = EmsSmartRelayTransactionLog::ACT_METERON;
                break;
        }
        $kaaEndpointHash = $request->get('hash');
        $relay = $this->get('app.relay');
        $relay->setStatus($circuit, $relayStatus,
            EmsSmartRelayTransactionLog::SOURCE_GATEWAY,
            null,
            $status,
            $kaaEndpointHash,
            null, null, null, 
            $event_time
            );

        if ( $circuit->getEvChargingStation() == true ) {
            $sess = $this->get('ev.session');
            $sess->gatewayEvent($circuit, $relayStatus, $json_args);
        }

        return $this->returnJsonResponse($request, array(
            'code' => JsonResponse::HTTP_OK
        ));
    }

    public function chargingStationVipOnAction(EmsCircuitMaster $circuit)
    {
        $this->denyAccessUnlessGranted('view', $circuit);
        $view = View::create();
        $station = $circuit->getChargingStation()[0];
        if ( $station ) {
            $stationService = $this->get('ev.charging');
            $res = $stationService->setStationVipStatus($station, 1);
            if ( $res ) {
                $view->setStatusCode(200);
                $view->setData(array('code' => 200));
            } else {
                $view->setStatusCode(400);
                $view->setData(array('message' => "can't switch on vip option"));
            }
            return $view;

        }
    }

    public function chargingStationVipOffAction(EmsCircuitMaster $circuit)
    {
        $this->denyAccessUnlessGranted('view', $circuit);
        $view = View::create();
        $station = $circuit->getChargingStation()[0];
        if ( $station ) {
            $stationService = $this->get('ev.charging');
            $res = $stationService->setStationVipStatus($station, 0);
            if ( $res ) {
                $view->setStatusCode(200);
                $view->setData(array('code' => 200));
            } else {
                $view->setStatusCode(400);
                $view->setData(array('message' => "can't switch off vip option"));
            }
            return $view;

        }
    }
}
