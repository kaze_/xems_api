<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use AppBundle\Entity\EmsCustomerMaster;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Context\Context;
use Symfony\Component\HttpFoundation\Response;


/**
 * Customer controller.
 *
 */
class CustomerController extends FOSRestController
{
    /**
     * Lists all customerMaster entities.
     *
     */
    public function allAction(Request $request)
    {
        $this->denyAccessUnlessGranted('view', new EmsCustomerMaster());
        $view = View::create();
        $context = (new Context())->addGroup('list');

        $repostory = $this->getDoctrine()->getManager()->getRepository('AppBundle:EmsCustomerMaster');
        $customers = $repostory->findAll();

        $view->setContext($context);
        $view->setStatusCode(200);
        $view->setData(array('code' => 200, 'data' => $customers));
        return $view;
    }

    /**
     * Finds and displays a customerMaster entity.
     *
     */
    public function getAction(EmsCustomerMaster $customer)
    {
        $this->denyAccessUnlessGranted('view', $customer);
        $view = View::create();
        $context = (new Context())->addGroup('list');

        $view->setContext($context);
        $view->setStatusCode(200);
        $view->setData(array('code' => 200, 'data' => $customer));
        return $view;
    }

    /**
     * Creates a new customerMaster entity.
     */
    public function newAction(Request $request)
    {
        // permission set in security.yml
        $customer = new EmsCustomerMaster();
        $this->denyAccessUnlessGranted('edit', $customer);

        return $this->processForm($request, $customer);
    }

    /**
     * Displays a form to edit an existing customerMaster entity.
     */
    public function editAction(Request $request, EmsCustomerMaster $customer)
    {
        $this->denyAccessUnlessGranted('edit', $customer);
/*        $customerObj = $this->getDoctrine()->getManager()->getRepository('AppBundle:EmsCustomerMaster')->find($customer);
        if ( $customerObj === null ) {
            return View::create(null, 400);
        }*/
        return $this->processForm($request, $customer);
    }

    private function processForm($request, $customer)
    {
        $view = View::create();
        $context = (new Context())->addGroup('list');

        $em = $this->getDoctrine()->getManager();
        $statusCode = $em->contains($customer) ? 200:201;

        $data = json_decode($request->getContent(), true);
        $form = $this->createForm('AppBundle\Form\EmsCustomerMasterType', $customer );
        $clearMissing = ($request->getMethod() == 'PUT') ? true : false;
        $form->submit($data, $clearMissing);

        if ( $form->isValid() ) {
            if ( $statusCode === 201 ) {
                $em->persist($customer);
                $view->setLocation($this->generateUrl('xems_customers_get', array('customer' => $customer->getCustomerId()->toString())));
            }
            $em->flush();
            $view->setContext($context);
            $view->setStatusCode($statusCode);
            $view->setData(array('code' => $statusCode, 'data' => $customer));
        } else {
            $view->setStatusCode(400);
            $view->setData(array('code' => 400, 'message' => $form->getErrors()));
        }
        return $view;
    }
    /**
     * Deletes a customerMaster entity.
     *
     */
    public function deleteAction(Request $request, EmsCustomerMaster $customerMaster)
    {
        $form = $this->createDeleteForm($customerMaster);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($customerMaster);
            $em->flush();
        }

        return $this->redirectToRoute('customer_index');
    }

    /**
     * Creates a form to delete a customerMaster entity.
     *
     * @param EmsCustomerMaster $customerMaster The customerMaster entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(EmsCustomerMaster $customerMaster)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('customer_delete', array('customerUuid' => $customerMaster->getCustomerId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    public function listUsersAction(Request $request, EmsCustomerMaster $customer)
    {
        $this->denyAccessUnlessGranted('view', $customer);
        $view = View::create();
        $context = (new Context())->addGroup('list');

        $repository = $this->getDoctrine()->getManager()->getRepository('AppBundle\Entity\User');
        $qb = $repository->createQueryBuilder('u');
        $qb->join('u.customer', 'c')
            ->where('c.customerId = :customer')
            ->setParameter('customer', $customer->getCustomerId()->toString(), 'uuid');
        $result = $qb->getQuery()->getResult();
        /*
        $users = $repository->findBy(array('customer' => $customer));
        if( $users == null || count($users) == 0 ) {
            return $this->view(null, 404);
        }*/
        if ( $result == null || count($result) == 0 ) {
            $statusCode = Response::HTTP_NOT_FOUND;
        } else {
            $statusCode = Response::HTTP_OK;
        }
        $view->setContext($context);
        $view->setData(array('code' => $statusCode, 'data' => $result));
        return $view;
    }

    public function listLocationsAction(Request $request, EmsCustomerMaster $customer)
    {
        $this->denyAccessUnlessGranted('view', $customer);
        $view = View::create();
        $context = (new Context())->addGroup('list');

/*        $repository = $this->getDoctrine()->getManager()->getRepository('AppBundle\Entity\EmsCustomerLocationMapping.old');
        $qb = $repository->createQueryBuilder('cl');
        $qb->join('cl.location', 'l')
            ->join('cl.customer', 'c')
            ->where('c.customerId = :customer')
            ->setParameter('customer', $customer->getCustomerId()->toString(), 'uuid');*/

        $repository = $this->getDoctrine()->getManager()->getRepository('AppBundle\Entity\EmsLocationMaster');
        $qb = $repository->createQueryBuilder('l');
        $qb->innerJoin('AppBundle\Entity\EmsCustomerLocationMapping', 'cl', 'WITH', 'cl.locationId = l.locationId')
            ->where('cl.customerId = :customer')
            ->setParameter('customer', $customer->getCustomerId()->toString(), 'uuid');
        $result = $qb->getQuery()->getResult();
        if( count($result) == null || count($result) == 0 ) {
            $statusCode = Response::HTTP_NOT_FOUND;
        } else {
            $statusCode = Response::HTTP_OK;
        }

        $view->setContext($context);
        $view->setData(array('code' => $statusCode, 'data' => $result));
        return $view;
    }
}
