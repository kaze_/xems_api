<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $roles = $this->get('security.token_storage')->getToken()->getRoles();
        $rolesTab = array_map(function($role){
            return $role->getRole();
        }, $roles);

//        $this->logger->info(var_export($rolesTab, true));

        if (in_array('ROLE_ADMIN', $rolesTab) || in_array('ROLE_SUPER_ADMIN', $rolesTab)) {
            return $this->redirectToRoute('location_dashboard');
        } elseif (in_array('ROLE_CUSTOMER_USER', $rolesTab)) {
            return $this->redirectToRoute('location_dashboard');
        }
        return $this->redirectToRoute('login');

/*        // replace this example code with whatever you need
        return $this->render('default/base.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ]);
 */   }
}

