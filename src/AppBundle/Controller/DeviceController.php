<?php

namespace AppBundle\Controller;

use AppBundle\Entity\EmsCircuitMaster;
use AppBundle\Entity\EmsDeviceMaster;
use AppBundle\Utils\DbTools;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\EmsGatewayDeviceMapping;
use FOS\RestBundle\Controller\Annotations\QueryParam;

class DeviceController extends BaseApiController
{	
	private $em;
    // demand interval = 15 minutes = 900
    const demandInterval = 900;

    public function allAction(Request $request)
    {
        $this->denyAccessUnlessGranted('view', new EmsDeviceMaster());
        $view = View::create();
        $context = (new Context())->addGroup('list');

        $repostory = $this->getDoctrine()->getManager()->getRepository('AppBundle:EmsDeviceMaster');
        $devices = $repostory->findAll();

        //$view->setContext($context);
        $view->setStatusCode(200);
        $view->setData(array('code' => 200, 'data' => $devices));
        return $view;
    }

    /**
     * Finds and displays a deviceMaster entity.
     *
     */
    public function getAction(EmsDeviceMaster $device)
    {
        $this->denyAccessUnlessGranted('view', $device);
        $view = View::create();
        $context = (new Context())->addGroup('device');
        try {
            $deviceTableName = $device->getDeviceType()->getDeviceTableName();
            $entityName = DbTools::getEntityNameFromTableName($deviceTableName);
            $entity = 'AppBundle:'.$entityName;
            $repository = $this->getDoctrine()->getManager()->getRepository($entity);
            $qb = $repository->createQueryBuilder('b');
            $qb->join('b.device', 'd')
                ->where('d.deviceId = :device')
                ->setParameter('device', $device->getDeviceId(), 'uuid');
            $deviceTypeDetail = $qb->getQuery()->getResult();
            $result = array($device, $deviceTypeDetail);
        } catch(Exception $e) {
            $deviceTypeDetail = null;
        }
        if ( $deviceTypeDetail == null || count($deviceTypeDetail) == 0 ) {
            $result = $device;
        } else {
            $result = array($device, $deviceTypeDetail);
        }
        $view->setContext($context);
        $view->setStatusCode(200);
        $view->setData(array('code' => 200, 'data' => $result));
        return $view;
    }

    /**
     * Creates a new deviceMaster entity.
     */
    public function newAction(Request $request)
    {
        // permission set in security.yml
        $device = new EmsDeviceMaster();
        $this->denyAccessUnlessGranted('edit', $device);

        return $this->processForm($request, $device);
    }

    /**
     * Displays a form to edit an existing deviceMaster entity.
     */
    public function editAction(Request $request, EmsDeviceMaster $device)
    {
        $this->denyAccessUnlessGranted('edit', $device);
        return $this->processForm($request, $device);
    }

    private function processForm($request, $device)
    {
        $view = View::create();
        $context = (new Context())->addGroup('list');

        $em = $this->getDoctrine()->getManager();
        $statusCode = $em->contains($device) ? 200:201;

        $data = json_decode($request->getContent(), true);

        // todo: find an simpler way to deal with relationships
        $deviceHwId = isset($data['deviceHwId']) ? $data['deviceHwId']: null;
        $deviceTypeId = isset($data['deviceTypeId']) ? $data['deviceTypeId']: null;
        $gatewayId = isset($data['gatewayId']) ? $data['gatewayId']: null;

        // process location hierarhy and location type
        if ($deviceHwId == null || $deviceTypeId == null ) {
            if ( $request->getMethod() == 'POST') {
                $view->setStatusCode(Response::HTTP_BAD_REQUEST);
                $view->setData(array('code' => Response::HTTP_BAD_REQUEST,
                    'message' => 'device hardware/device type cannot be null.'));
                return $view;
            }
        }

        if ( $deviceHwId ) {
            $deviceHw = $this->getDoctrine()->getManager()
                ->getRepository('AppBundle:EmsHardwareInfoList')->find($deviceHwId);
            if ( $deviceHw == null ) {
                $view->setStatusCode(Response::HTTP_BAD_REQUEST);
                $view->setData(array('code' => Response::HTTP_BAD_REQUEST,
                    'message' => 'device hardware/device type does not exist.'));
                return $view;
            }
            $device->setDeviceHw($deviceHw);
        }
        if ( $deviceTypeId ) {
            $deviceType = $this->getDoctrine()->getManager()
                ->getRepository('AppBundle:EmsDeviceType')->find($deviceTypeId);
            if ( $deviceType == null ) {
                $view->setStatusCode(Response::HTTP_BAD_REQUEST);
                $view->setData(array('code' => Response::HTTP_BAD_REQUEST,
                    'message' => 'device hardware/device type does not exist.'));
                return $view;
            }
            $device->setDeviceType($deviceType);
        }
        if ( $request->getMethod() == 'POST' ) {
            if ( $deviceTypeId === 10 ) {
                $required = [ 'ipAddress', 'gatewayId' ];
            } else if ( $deviceTypeId === 100 || $deviceTypeId === 200 || $deviceTypeId === 800 ) {
                $required = [ 'deviceName', 'modbusGatewayId', 'voltageId', 'meterTypeId'];
            }
            foreach( $required as $var_name ) {
                if ( ! isset( $data[$var_name] ) )  {
                    $view->setData(array('code' => Response::HTTP_BAD_REQUEST,
                        'message' =>  $var_name . ' cannot be null.'));
                    return $view;
                }
            }
        }
        
        // todo: link locations to customer/create location belongs to customer
        if ( $gatewayId != null) {
            $gatewayDeviceMapping = $this->getDoctrine()->getRepository('AppBundle:EmsGatewayDeviceMapping');
            if ( $statusCode == 200 ) {
                if ($gatewayDeviceMapping->find(array(
                    "deviceId" => $device->getDeviceId(),
                    "status" => 1
                ))) {
                    $view->setStatusCode(Response::HTTP_BAD_REQUEST);
                    $view->setData(array(
                        'code' => Response::HTTP_BAD_REQUEST,
                        'message' => 'this device is already link to another gateway, unlink first'
                    ));
                    return $view;
                }
            }
            $gateway = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:EmsGatewayMaster')
            ->find($gatewayId);
            if ( $gateway == null) {
                $view->setStatusCode(Response::HTTP_BAD_REQUEST);
                $view->setData(array(
                    'code' => Response::HTTP_BAD_REQUEST,
                    'message' => 'gateway ID does not exist.'
                ));
                return $view;
            }
            $device->addGateway($gateway);
        }
        
        $form = $this->createForm('AppBundle\Form\EmsDeviceMasterType', $device );
        $clearMissing = ($request->getMethod() == 'PATCH') ? false : true;
        $form->submit($data, $clearMissing);

        if ( $form->isValid() ) {
            if ( $statusCode === 201 ) {
                $em->persist($device);
                $view->setLocation($this->generateUrl('xems_devices_get', array('device' => $device->getDeviceId()->toString())));
            }
            $em->flush();
            $view->setContext($context);
            $view->setStatusCode($statusCode);
            $view->setData(array('code' => $statusCode, 'data' => $device));
            if ( $gatewayId != null ) {
                $mapping = new EmsGatewayDeviceMapping();
                $mapping->setGatewayId($gatewayId);
                $mapping->setDeviceId($device->getDeviceId());
                $mapping->setStatusId(1);
                $em->persist($mapping);
            }
            $em->flush();
            $deviceManager = $this->get('app.device_manager');
            $deviceManager->processDeviceData($device, $data);
        } else {
            $view->setStatusCode(400);
            $view->setData(array('code' => 400, 'data' => $form->getErrors()));
        }
        return $view;
    }

    /**
     * Deletes a deviceMaster entity.
     *
     */
    public function deleteAction(Request $request, EmsDeviceMaster $deviceMaster)
    {
        $form = $this->createDeleteForm($deviceMaster);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($deviceMaster);
            $em->flush();
        }

        return $this->redirectToRoute('device_index');
    }

    /**
     * Creates a form to delete a deviceMaster entity.
     *
     * @param EmsDeviceMaster $deviceMaster The gatewayMaster entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(EmsDeviceMaster $deviceMaster)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('device_delete', array('gatewayUuid' => $deviceMaster->getDeviceId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    public function listCircuitsAction(Request $request, EmsDeviceMaster $device)
    {
        $this->denyAccessUnlessGranted('view', $device);
        $view = View::create();
        $context = (new Context())->addGroup('list');

        $repository = $this->getDoctrine()->getManager()->getRepository('AppBundle\Entity\EmsCircuitMaster');
        $qb = $repository->createQueryBuilder('c');
        $qb->join('c.meter', 'm')
            ->join('m.device', 'p')
            ->join('p.device', 'd')
            ->where('d.deviceId = :device')
            ->setParameter('device', $device->getDeviceId(), 'uuid');

        $result = $qb->getQuery()->getResult();
        if( $result == null || count($result) == 0 ) {
            $statusCode = Response::HTTP_NOT_FOUND;
        } else {
            $statusCode = Response::HTTP_OK;
        }
        $view->setContext($context);
        $view->setStatusCode($statusCode);
        $view->setData(array('code' => $statusCode, 'data' => $result));
        return $view;
    }

    // power report

    /**
     * @param Request $request
     * @param EmsDeviceMaster $device
     * @return JsonResponse
     *
     * use paramFetcher in FOSRestBundle to check parameters.
     * @QueryParam(name="from", requirements="[A-Z\d\-: ]+", strict=true, nullable=false)
     * @QueryParam(name="to", requirements="[A-Z\d\-: ]+", strict=true, nullable=false)
     * @QueryParam(name="offset", requirements="\d+", default=0)
     * @QueryParam(name="limit", requirements="\d+", default=0)
     */
    public function demandAction(Request $request, ParamFetcher $paramFetcher, EmsDeviceMaster $device)
    {
        $this->denyAccessUnlessGranted('view', $device);

        $from = $paramFetcher->get('from');
        $to = $paramFetcher->get('to');
        $limit = $paramFetcher->get('limit');
        $offset = $paramFetcher->get('offset');

        $startDate = (new \DateTime())->setTimestamp(strtotime($from));
        $endDate =(new \DateTime())->setTimestamp(strtotime($to));
        if ( $startDate == null || $endDate == null ) {
            return $this->returnJsonResponse($request, array(
                'code' => JsonResponse::HTTP_BAD_REQUEST
            ));
        }

        $repository = $this->getDoctrine()->getManager()
            ->getRepository('AppBundle:EmsPowerReportLog');

        $result = $repository->findCurrDmdKwByDevice(
            array(
                'startDate' => $startDate,
                'endDate' => $endDate,
                'device' => $device,
                'interval' => $this::demandInterval),
            array('reportDatetime' => 'ASC'), $limit, $offset
        );

        $count = count($result);
        $dmdData = array();
        for ($i = 0; $i < $count; $i++) {
            $data = array();
            $demand = $result[$i]['currDmdKw'];
            if ( $demand == null ) {
                continue;
            }
            $reportTime = $result[$i]['reportDatetime'];
            if ( $reportTime instanceof \DateTime ) {
                $data['time'] = $reportTime->format('Y-m-d H:i:s');
            } else {
                $data['time'] = $reportTime;
            }
            $data['demand'] = (float) $demand;
            $dmdData[] = $data;
        }

        return $this->returnJsonResponse($request, array(
            "code" => JsonResponse::HTTP_OK,
            "data" => $dmdData
        ));
    }

    /**
     * @param Request $request
     * @param ParamFetcher $paramFetcher
     * @param EmsDeviceMaster $device
     * @return JsonResponse
     *
     * use paramFetcher in FOSRestBundle to check parameters.
     * @QueryParam(name="from", requirements="[A-Z\d\-: ]+", strict=true, nullable=false)
     * @QueryParam(name="to", requirements="[A-Z\d\-: ]+", strict=true, nullable=false)
     */
    public function maxDemandAction(Request $request, ParamFetcher $paramFetcher, EmsDeviceMaster $device)
    {
        $this->denyAccessUnlessGranted('view',$device);

        $from = $paramFetcher->get('from');
        $to = $paramFetcher->get('to');

        $startDate = \DateTime::createFromFormat('Y-m-d H:i:s', $from);
        $endDate = \DateTime::createFromFormat('Y-m-d H:i:s', $to);

        $repository = $this->getDoctrine()->getManager()
            ->getRepository('AppBundle:EmsPowerReportLog');

        $result = $repository->findMaxDataByDevice('currDmdKw', array('currDmdKw'), array(
            "startDate" => $startDate,
            "endDate" => $endDate,
            "device" => $device
        ));

//        $result = $repository->findMaxDmdByDevice(array(
//            "startDate" => $startDate,
//            "endDate" => $endDate,
//            "device" => $device
//        ));

        if ( count($result) == 0 ) {
            return $this->returnJsonResponse($request, array(
                'code' => JsonResponse::HTTP_NOT_FOUND,
                'message' => 'No Data'
            ));
        }
        $report = $result[0];
        $report["time"] = $report["reportDatetime"]->format('Y-m-d H:i:s');
        unset($report["reportDatetime"]);
        return $this->returnJsonResponse($request, array(
            'code' => JsonResponse::HTTP_OK,
            'data' => $report
        ));
    }

    public function currentDemandAction(Request $request, EmsDeviceMaster $device)
    {
        $this->denyAccessUnlessGranted('view',$device);
		$this->em=$this->getDoctrine()->getManager();
    
        $repository = $this->em->getRepository('AppBundle:EmsPowerReportLog');
        //$logger = $this->get('logger');：
        //$logger->info("start findNewestDataByDevice");
        $result = $repository->findNewestDataByDevice(
            array("reportDatetime", "currDmdKw", "currLgstdmdKw", "touALgstdmdKw", "touBLgstdmdKw", "touCLgstdmdKw", "touDLgstdmdKw", "pfTotal"),
            array("device" => $device->getDeviceId())
        );

        //$logger->info("end findNewestDataByDevice");
        if (count($result) == 0 || $result[0] == null ) {
            return $this->returnJsonResponse($request, array(
                'code' => JsonResponse::HTTP_NOT_FOUND,
                'message' => 'No Data'
            ));
        }
        $report = $result[0];
        $report["time"] = $report["reportDatetime"]->format('Y-m-d H:i:s');
        unset($report["reportDatetime"]);

        $endDate = new \DateTime();
        $startToday = (new \DateTime())->setTime(0, 0, 0);
        $startThisMonth = (\DateTime::createFromFormat('Y-m-d', date('Y-m-01')))->setTime(0, 0, 0);
        //$logger->info("start findMaxDmdByDevice");
        $maxToday = $repository->findMaxDmdByDevice(array(
            "device" => $device->getDeviceId(),
            "startDate" => $startToday,
            "endDate" => $endDate
        ));
        //$logger->info("start 2nd findMaxDmdByDevice");
        $maxThisMonth = $repository->findMaxDmdByDevice(array(
            "device" => $device->getDeviceId(),
            "startDate" => $startThisMonth,
            "endDate" => $endDate
        ));
        //$logger->info("end query");
        if (count($result) == 0 || $result[0] == null ) {
            return $this->returnJsonResponse($request, array(
                'code' => JsonResponse::HTTP_NOT_FOUND,
                'message' => 'No Data'
            ));
        }

        if ( count($maxToday) != 0 ) {
            $maxToday[0]["time"] = $maxToday[0]["reportDatetime"]->format('Y-m-d H:i:s');
            unset($maxToday[0]["reportDatetime"]);
            $report["max_today"] = $maxToday[0];
        } else {
            $report["max_today"] = null;
        }

        if ( count($maxThisMonth) != 0 ) {
            $maxThisMonth[0]["time"] = $maxThisMonth[0]["reportDatetime"]->format('Y-m-d H:i:s');
            unset($maxThisMonth[0]["reportDatetime"]);
            $report["max_this_month"] = $maxThisMonth[0];
        } else {
            $report["max_this_month"] = null;
        }

        // demand contract now
        $fee = $this->get('esp.fee');
        $espRepo = $this->em->getRepository('EspBundle:EspCustomerMaster');
        $espCustomer = $espRepo->findOneBy(array('powerMeterDeviceId' => $device->getDeviceId()));
        if ( $espCustomer ) {
            $report['contract'] = $fee->getDemandContract($espCustomer, $endDate);
        }
		
		$RAW_QUERY = 
        'SELECT e1_.circuit_max_power FROM ems_meter_circuit_mapping e0_ '.
        'INNER JOIN ems_circuit_master e1_ ON (e0_.device_id=? AND e1_.circuit_id = e0_.circuit_id) ';
        $statement = $this->em->getConnection()->prepare($RAW_QUERY);
        $statement->execute(array($device->getDeviceId()));
        $the_device_circuit = $statement->fetchAll()[0];
		
		$report['circuit_max_power']= (int)$the_device_circuit['circuit_max_power'];
        return $this->returnJsonResponse($request, array(
            "code" => JsonResponse::HTTP_OK,
            "data" => $report
        ));
    }

    public function currentCurrentAction(Request $request, EmsDeviceMaster $device)
    {
        $this->denyAccessUnlessGranted('view', $device);
		$this->em=$this->getDoctrine()->getManager();
        $repository = $this->em
            ->getRepository('AppBundle:EmsPowerReportLog');

        $result = $repository->findNewestDataByDevice(
            array("aA", "aB", "aC", "vA", "vB", "vC", "aAverage"),
            array("device" => $device->getDeviceId()));

        if (count($result) == 0 || $result[0] == null ) {
            return $this->returnJsonResponse($request, array(
                'code' => JsonResponse::HTTP_NOT_FOUND,
                'message' => 'No Data'
            ));
        }
        $report = $result[0];
        $report["time"] = $report["reportDatetime"]->format('Y-m-d H:i:s');
        unset($report["reportDatetime"]);

        if ( $report['aA'] == null )
            $report['aA'] = 0;
        if ( $report['aB'] == null )
            $report['aB'] = 0;
        if ( $report['aC'] == null )
            $report['aC'] = 0;
        if ( $report['vA'] == null )
            $report['vA'] = 0;
        if ( $report['vB'] == null )
            $report['vB'] = 0;
        if ( $report['vC'] == null )
            $report['vC'] = 0;
        $report['aSummary'] = $report['aA'] + $report['aB'] + $report['aC'];
        if ( $report['aAverage'] == null )
            $report['aAverage'] = $report['aSummary']/3;

        // max
        $endDate = new \DateTime();
        $startToday = (new \DateTime())->setTime(0, 0, 0);
        $startThisMonth = (\DateTime::createFromFormat('Y-m-d', date('Y-m-01')))->setTime(0, 0, 0);

        $maxToday = $repository->findMaxCurrByDevice(array(
            "device" => $device->getDeviceId(),
            "startDate" => $startToday,
            "endDate" => $endDate
        ));
        $maxThisMonth = $repository->findMaxCurrByDevice(array(
            "device" => $device->getDeviceId(),
            "startDate" => $startThisMonth,
            "endDate" => $endDate
        ));
    
        if ( count($maxToday) != 0 ) {
            $maxToday[0]["time"] = $maxToday[0]["reportDatetime"]->format('Y-m-d H:i:s');
            unset($maxToday[0]["reportDatetime"]);
            $report["max_today"] = $maxToday[0];
        } else {
            $report["max_today"] = null;
        }

        if ( count($maxThisMonth) != 0 ) {
            $maxThisMonth[0]["time"] = $maxThisMonth[0]["reportDatetime"]->format('Y-m-d H:i:s');
            unset($maxThisMonth[0]["reportDatetime"]);
            $report["max_this_month"] = $maxThisMonth[0];
        } else {
            $report["max_this_month"] = null;
        }
		
		$RAW_QUERY = 
        'SELECT e1_.circuit_max_current FROM ems_meter_circuit_mapping e0_ '.
        'INNER JOIN ems_circuit_master e1_ ON (e0_.device_id=? AND e1_.circuit_id = e0_.circuit_id) ';
        $statement = $this->em->getConnection()->prepare($RAW_QUERY);
        $statement->execute(array($device->getDeviceId()));
        $the_device_circuit = $statement->fetchAll()[0];
		$report['circuit_max_current']= (int)$the_device_circuit['circuit_max_current'];
		
       return $this->returnJsonResponse($request, array(
            "code" => JsonResponse::HTTP_OK,
            "data" => $report
        ));
    }

    /**
     * @param Request $request
     * @param ParamFetcher $paramFetcher
     * @param EmsDeviceMaster $device
     * @return JsonResponse
     *
     * use paramFetcher in FOSRestBundle to check parameters.
     * @QueryParam(name="from", requirements="[A-Z\d\-: ]+", strict=true, nullable=false)
     * @QueryParam(name="to", requirements="[A-Z\d\-: ]+", strict=true, nullable=false)
     */
    public function maxCurrentAction(Request $request, ParamFetcher $paramFetcher, EmsDeviceMaster $device)
    {
        $this->denyAccessUnlessGranted('view', $device);


        $from = $paramFetcher->get('from');
        $to = $paramFetcher->get('to');

        $startDate = \DateTime::createFromFormat('Y-m-d H:i:s', $from);
        $endDate = \DateTime::createFromFormat('Y-m-d H:i:s', $to);

        $repository = $this->getDoctrine()->getManager()
            ->getRepository('AppBundle:EmsPowerReportLog');

        $result = $repository->findMaxCurrByDevice(array(
            "startDate" => $startDate,
            "endDate" => $endDate,
            "device" => $device
        ));

        if ( count($result) == 0 ) {
            return $this->returnJsonResponse($request, array(
                'code' => JsonResponse::HTTP_NOT_FOUND,
                'message' => 'No Data'
            ));
        }
        $report = $result[0];
        $report["time"] = $report["reportDatetime"]->format('Y-m-d H:i:s');
        unset($report["reportDatetime"]);
        return $this->returnJsonResponse($request, array(
            'code' => JsonResponse::HTTP_OK,
            'data' => $report
        ));
    }

    /**
     * @param Request $request
     * @param ParamFetcher $paramFetcher
     * @param EmsDeviceMaster $device
     * @return JsonResponse
     *
     * use paramFetcher in FOSRestBundle to check parameters.
     * @QueryParam(name="from", requirements="[A-Z\d\-: ]+", strict=true, nullable=false)
     * @QueryParam(name="to", requirements="[A-Z\d\-: ]+", strict=true, nullable=false)
     */
    public function kwhAction(Request $request, ParamFetcher $paramFetcher, EmsDeviceMaster $device)
    {
        $this->denyAccessUnlessGranted('view', $device);
        $from = $paramFetcher->get('from');
        $to = $paramFetcher->get('to');

        $startDate = (new \DateTime())->setTimestamp(strtotime($from));
        $endDate = (new \DateTime())->setTimestamp(strtotime($to));

        $repository = $this->getDoctrine()->getManager()
            ->getRepository('AppBundle:EmsPowerReportLog');

        $result = $repository->findkWhByDevice(
            array(
                "device" => $device,
                "startDate" => $startDate,
                "endDate" => $endDate
            ));

        return $this->returnJsonResponse($request, array(
            "code" => JsonResponse::HTTP_OK,
            "data" => array(
                "kWh" => $result,
                "startDate" => $from,
                "endDate" => $to
            )
        ));
    }


    /**
     * @param Request $request
     * @param ParamFetcher $paramFetcher
     * @param EmsCircuitMaster $circuit
     * @return JsonResponse
     *
     * use paramFetcher in FOSRestBundle to check parameters.
     * @QueryParam(name="from", requirements="[A-Z\d\-: ]+", strict=true, nullable=false)
     * @QueryParam(name="to", requirements="[A-Z\d\-: ]+", strict=true, nullable=false)
     * @QueryParam(name="offset", requirements="\d+", default=0)
     * @QueryParam(name="limit", requirements="\d+", default=0)
     * @QueryParam(name="interval", requirements="\d+", default=0)
     */
    public function demandWithStationsAction(Request $request, ParamFetcher $paramFetcher, EmsDeviceMaster $device)
    {
        $this->denyAccessUnlessGranted('view', $device);

        $from = $paramFetcher->get('from');
        $to = $paramFetcher->get('to');
        $limit = $paramFetcher->get('limit');
        $offset = $paramFetcher->get('offset');
        $interval = $paramFetcher->get('interval');

        $startDate = (new \DateTime())->setTimestamp(strtotime($from));
        $endDate =(new \DateTime())->setTimestamp(strtotime($to));
        if ( $startDate == null || $endDate == null || $startDate > $endDate ) {
            return $this->returnJsonResponse($request, array(
                'code' => JsonResponse::HTTP_BAD_REQUEST
            ));
        }

        $circuits = $device->getCircuit();
        if ( empty($circuits) )  {
            return $this->returnJsonResponse($request, array(
                'code' => JsonResponse::HTTP_NOT_FOUND,
                'msg' => 'no circuit is set in this device'
            ));
        }
        $circuit = $circuits->get(0);
        $powerReport = $this->get('app.power_report');
        // prepare data - get Customer Esp Package First
        $espRepo = $this->getDoctrine()->getManager()->getRepository('EspBundle:EspCustomerMaster');
        $espCustomer = $espRepo->findOneBy(array('powerMeterDeviceId' => $device->getDeviceId()));
        $rateArray = null;
        if ( $espCustomer ) {
            $fee = $this->get('esp.fee');
            $rate = $fee->getCustomerRate($espCustomer);
            if ( $rate != null ) {
                $energyPackage = $rate->getEnergyPackage()->getEnergyPackageId();
                $contract = array();
                if ($rate->getNormalContract()) {
                    $contract['p'] = $rate->getNormalContract();
                }
                if ($rate->getSemiPeakContract()) {
                    $contract['sp'] = $rate->getSemiPeakContract();
                }
                if ($rate->getSaturdaySemiPeakContract()) {
                    $contract['ssp'] = $rate->getSaturdaySemiPeakContract();
                }
                if ($rate->getOffPeakContract()) {
                    $contract['op'] = $rate->getOffPeakContract();
                }
                $rateArray =  array('contract' => $contract, 'energyPackageId' => $energyPackage);
            }
        }

        $result = $powerReport->getCircuitDemandWithStations($circuit,
            array(
                'startDate' => $startDate,
                'endDate' => $endDate,
                'interval' => $interval? $interval : $this::demandInterval,
                'limit' => $limit,
                'offset' => $offset
            ), $rateArray
        );

        if ( $result == null ) {
            return $this->returnJsonResponse($request, array(
                'code' => JsonResponse::HTTP_NOT_FOUND,
                'msg' => 'no charging stations found'
            ));
        }
        return $this->returnJsonResponse($request, array(
            "code" => JsonResponse::HTTP_OK,
            "data" => $result
        ));
    }

}
