<?php

namespace AppBundle\Controller;

use AppBundle\Entity\EmsCircuitMaster;
use AppBundle\Entity\EmsGatewayJob;
use Doctrine\ORM\Query;
use FOS\RestBundle\Controller\FOSRestController;
use AppBundle\Entity\EmsGatewayMaster;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Context\Context;
use AppBundle\Utils\DbTools;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\EmsLocationGatewayMapping;
use DateTime;
use EvBundle\Entity\EvChargingStationStatus;
use EvBundle\Services\Credit\Pay2go;
use Exception;

/**
 * Gateway controller.
 *
 */
class GatewayController extends Controller
{
    /**
     * Lists all gatewayMaster entities.
     *
     */
    public function allAction(Request $request)
    {
        $this->denyAccessUnlessGranted('view', new EmsGatewayMaster());
        $view = View::create();
        $context = (new Context())->addGroup('list');

        $repostory = $this->getDoctrine()->getManager()->getRepository('AppBundle:EmsGatewayMaster');
        $gateways = $repostory->findAll();

        $view->setContext($context);
        $view->setStatusCode(200);
        $view->setData(array('code' => 200, 'data' => $gateways));
        return $view;
    }

    /**
     * Finds and displays a gatewayMaster entity.
     *
     */
    public function getAction(EmsGatewayMaster $gateway)
    {
        $this->denyAccessUnlessGranted('view', $gateway);
        $view = View::create();
        $context = (new Context())->addGroup('list');

        $view->setContext($context);
        $view->setStatusCode(200);
        $view->setData(array('code' => 200 , 'data' => $gateway));
        return $view;
    }

    /**
     * Creates a new gatewayMaster entity.
     */
    public function newAction(Request $request)
    {
        // permission set in security.yml
        $gateway = new EmsGatewayMaster();
        $this->denyAccessUnlessGranted('edit', $gateway);

        return $this->processForm($request, $gateway);
    }

    /**
     * Displays a form to edit an existing gatewayMaster entity.
     */
    public function editAction(Request $request, EmsGatewayMaster $gateway)
    {
        $this->denyAccessUnlessGranted('edit', $gateway);
/*        $gatewayObj = $this->getDoctrine()->getManager()->getRepository('AppBundle:EmsGatewayMaster')->find($gateway);
        if ( $gatewayObj === null ) {
            return View::create(null, 400);
        }*/
        return $this->processForm($request, $gateway);
    }

    private function processForm($request, $gateway)
    {
        $logger = $this->get('logger');
        $view = View::create();
        $context = (new Context())->addGroup('list');

        $em = $this->getDoctrine()->getManager();
        $statusCode = $em->contains($gateway) ? 200:201;

        $data = json_decode($request->getContent(), true);
        
        $locationId = isset($data['locationId'])? $data['locationId'] : null;
        $gatewayHwId = isset($data['gatewayHwId']) ? $data['gatewayHwId'] : null;
        $statusId = isset($data['statusId']) ? $data['statusId'] : 1; // default = 1

        if ( $request->getMethod() == 'POST') {
            $required = [ 'gatewayName', 'gatewaySwVersion', 'gatewayMeta', ];
            foreach( $required as $var_name ) {
                if ( ! isset( $data[$var_name] ) )  {
                    $view->setData(array('code' => Response::HTTP_BAD_REQUEST,
                        'message' =>  $var_name . ' cannot be null.'));
                    return $view;
                }
            }
        }

        if ($gatewayHwId == null) {
            if ( $request->getMethod() == 'POST') {
                $view->setStatusCode(Response::HTTP_BAD_REQUEST);
                $view->setData(array('code' => Response::HTTP_BAD_REQUEST,
                    'message' => 'gateway hardware or status cannot be null.'));
                return $view;
            }
        }
        if ( $gatewayHwId ) {
            $gatewayHw = $this->getDoctrine()->getManager()
            ->getRepository('AppBundle:EmsHardwareInfoList')->find($gatewayHwId);
            if ( $gatewayHw == null ) {
                $view->setStatusCode(Response::HTTP_BAD_REQUEST);
                $view->setData(array('code' => Response::HTTP_BAD_REQUEST,
                    'message' => 'device hardware/device type does not exist.'));
                return $view;
            }
            $gateway->setGatewayHw($gatewayHw);
        }
        $status = $this->getDoctrine()->getManager()->getRepository('AppBundle:SysStatusList')->find($statusId);
        if ( $status == null ) {
                $view->setStatusCode(Response::HTTP_BAD_REQUEST);
                $view->setData(array('code' => Response::HTTP_BAD_REQUEST,
                    'message' => 'statusId - no such status ID.'));
                return $view;
        }
        $gateway->setStatus($status);
        
        if ($locationId != null) {
            $locationGatewayMapping = $this->getDoctrine()->getRepository('AppBundle:EmsLocationGatewayMapping');
            if ( $statusCode == 200 ) {
                if ($locationGatewayMapping->find(array(
                    "gatewayId" => $gateway->getGatewayId(),
                    "status" => 1
                ))) {
                    $view->setStatusCode(Response::HTTP_BAD_REQUEST);
                    $view->setData(array(
                        'code' => Response::HTTP_BAD_REQUEST,
                        'message' => 'this location is already link to another customer, unlink first'
                    ));
                    return $view;
                }
            }
            $location = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:EmsLocationMaster')
            ->find($locationId);
            if ( $location == null) {
                $view->setStatusCode(Response::HTTP_BAD_REQUEST);
                $view->setData(array(
                    'code' => Response::HTTP_BAD_REQUEST,
                    'message' => 'location ID does not exist.'
                ));
                return $view;
            }
            // $gateway->addLocatin($location);
        }
        
        $form = $this->createForm('AppBundle\Form\EmsGatewayMasterType', $gateway );
        $clearMissing = ($request->getMethod() == 'PATCH') ? false : true;
        $form->submit($data, $clearMissing);

        if ( $form->isValid() ) {
            if ( $statusCode === 201 ) {
                $em->persist($gateway);
                $view->setLocation($this->generateUrl('xems_gateways_get', array('gateway' => $gateway->getGatewayId()->toString())));
            }
            $em->flush();
            $view->setContext($context);
            $view->setStatusCode($statusCode);
            $view->setData($gateway);
            if ( $locationId != null ) {
                $mapping = new EmsLocationGatewayMapping();
                $mapping->setLocationId($locationId);
                $mapping->setGatewayId($gateway->getGatewayId());
                $mapping->setStatusId(1);
                $em->persist($mapping);
                $em->flush();
            }
            
        } else {
            $view->setStatusCode(400);
            $view->setData($form->getErrors());
        }
        return $view;
    }
    /**
     * Deletes a gatewayMaster entity.
     *
     */
    public function deleteAction(Request $request, EmsGatewayMaster $gatewayMaster)
    {
        $form = $this->createDeleteForm($gatewayMaster);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($gatewayMaster);
            $em->flush();
        }

        return $this->redirectToRoute('gateway_index');
    }

    /**
     * Creates a form to delete a gatewayMaster entity.
     *
     * @param EmsGatewayMaster $gatewayMaster The gatewayMaster entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(EmsGatewayMaster $gatewayMaster)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('gateway_delete', array('gatewayUuid' => $gatewayMaster->getGatewayId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    public function listDevicesAction(Request $request, EmsGatewayMaster $gateway)
    {
        $this->denyAccessUnlessGranted('view', $gateway);
        $view = View::create();
        $context = (new Context())->addGroup('toGateway');
        $context->setSerializeNull(false);

        //$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle\Entity\EmsDeviceMaster');
        $repository = $this->getDoctrine()->getManager()->getRepository('AppBundle\Entity\EmsModbusGateway');
        $qb = $repository->createQueryBuilder('d');
        $qb->innerJoin('AppBundle\Entity\EmsGatewayDeviceMapping', 'gd', 'WITH', 'gd.deviceId = d.device')
            ->where('gd.gatewayId = :gateway')
            ->setParameter('gateway', $gateway->getGatewayId(), 'uuid');
        $result = $qb->getQuery()->getResult();
        if( $result == null || count($result) == 0 ) {
            $statusCode = Response::HTTP_NOT_FOUND;
        } else {
            $statusCode = Response::HTTP_OK;
        }
        $view->setContext($context);
        $view->setStatusCode($statusCode);
        $view->setData(array('modbus' => $result));

//        $view->setData(array('code' => $statusCode, 'data' => $result));
        return $view;
    }

    public function listConfigurationAction(Request $request, EmsGatewayMaster $gateway)
    {
        $this->denyAccessUnlessGranted('view', $gateway);
        $view = View::create();
        $context = (new Context())->addGroup('toGateway');
        $context->setSerializeNull(false);

        $device_manager = $this->get('app.device_manager');
        $result = $device_manager->gatewayConfiguration($gateway);

        if( $result == null || count($result) == 0 ) {
            $statusCode = Response::HTTP_NOT_FOUND;
        } else {
            $statusCode = Response::HTTP_OK;
        }

        $view->setContext($context);
        $view->setStatusCode($statusCode);
        $view->setData($result);

        return $view;
    }

    private function findGatewayId($id)
    {
        // guess id is meter
        $repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:EmsDeviceMaster');
        $device = $repository->find($id);

        if ( $device != null ) {
            $gws = $device->getGateway() ;
            if ( $gws && count($gws) > 0 )
                return $gws[0]->getGatewayId()->toString();
            return null;
        }

        // guess id is gateway
        $repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:EmsGatewayMaster');
        $gateway = $repository->find($id);
        if ( $gateway != null )
            return $id;


        // guess id is circuit
        $repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:EmsCircuitMaster');
        $circuit = $repository->find($id);
        if ( $circuit != null ) {
            $device = $circuit->getValidDevice();
            if ( $device != null ) {
                $gateway = $device->getValidGateway();
                return $gateway->getGatewayId()->toString();
            }
        }

        return null;
    }

    
    private function getJobs($gwid)
    {
        $jobs = [];
        $conn = $this->getDoctrine()->getManager()->getConnection();
        $conn->beginTransaction();
        try {
            $select_stmt = $conn->prepare('
                SELECT ID, VALID_TO, JOB FROM ems_gateway_job
                WHERE TO_GATEWAY=:gwid
                    AND VALID_TO > NOW()
                    AND POLLED_AT IS NULL
                FOR UPDATE
            ');
            $update_stmt = $conn->prepare('
                UPDATE ems_gateway_job
                SET POLLED_AT = NOW()
                WHERE ID=:job_id
            ');

            $select_stmt->bindValue(':gwid',$gwid, \PDO::PARAM_STR);
            $select_stmt->execute();
            $result = $select_stmt->fetchAll( \PDO::FETCH_ASSOC );

            foreach ($result as $r) {
                $job_id = (int) $r['ID'];
                $job = [
                    'id' => $job_id,
                    'valid_to' => $r['VALID_TO'],
                    'job' => $r['JOB'],
                ];
                $jobs[] = $job;

                $update_stmt->bindValue(':job_id',$job_id, \PDO::PARAM_INT);
                $update_stmt->execute();
            }

            $conn->commit();
        } catch (Exception $e) {
            $conn->rollBack();
            throw $e;               
        }

        return $jobs;
    }



    //public function getJobsAction(Request $request, EmsGatewayMaster $gateway)
    public function getJobsAction(Request $request, string $id)
    {
        $this->denyAccessUnlessGranted('ROLE_CUSTOMER_ADMIN');
        $logger = $this->get('logger');

        $gwid = $this->findGatewayId($id);

        // $logger->info('getJobsAction id ', [ $gwid, $id] );

        $jobs = null;
        if ( $gwid != null ) {
            $jobs = $this->getJobs($gwid);
            //$jobs = $this->getJobs($gateway->getGatewayId()->toString() );
            //$logger->info('getJobsAction3', [ $jobs ] );
            $statusCode = Response::HTTP_OK;
        } else {
            $statusCode = Response::HTTP_NOT_FOUND;
        }

        return new JsonResponse( [
            'code' => Response::HTTP_OK,
            'data' => $jobs ,
        ], $statusCode );

    }

    public function setJobsAction(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_CUSTOMER_ADMIN');
        $logger = $this->get('logger');
        
        // 算了... 不要管以後會有什麼 input，以後再說
        $circuitId = $request->get('circuit');
        $repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:EmsCircuitMaster');
        $circuit = $repository->find($circuitId);
        
        // find gwid
        $gwId = null;
        if ( $circuit != null ) {
            $device = $circuit->getValidDevice();
            if ( $device != null ) {
                $gateway = $device->getValidGateway();
                if ( $gateway != null ) {
                    $gwId = $gateway->getGatewayId()->toString();
                }
            }
        }

        // check if gateway is valid
        if ( $circuitId == null || $gwId == null ) {
            return new JsonResponse([
                'code' => Response::HTTP_BAD_REQUEST,
                'message' => 'Gateway Not Found'
            ]);
        }

        $em = $this->getDoctrine()->getManager();
        $gw = $em->find('AppBundle:EmsGatewayMaster', $gwId);
        if ( $gw == null ) { 
            return new JsonResponse([
                'code' => Response::HTTP_BAD_REQUEST,
                'message' => 'Gateway Not Found'
            ]);            
        }

        $command = [];
        $cmd = $request->get('cmd');
        
        $argStatus = true;
        switch($cmd) {
            case EmsGatewayJob::CMD_NOOP:
            case EmsGatewayJob::CMD_REBOOT:
                $command['cmd'] = $cmd;
            break;
            case EmsGatewayJob::CMD_AUTH_PLATE:
                $plate = $request->get('plate');
                if ( $plate == null ) {
                    $argStatus = false;
                    break;
                }
                $command = [
                    'cmd' => $cmd,
                    'args' => [
                        'circuit' => $circuitId,
                        'plate' => $plate
                    ]
                ];
            break;
            default: 
                $argStatus = false;
            break;
        }

        if ( $argStatus == false ) {
            return new JsonResponse([
                'code' => Response::HTTP_BAD_REQUEST,
                'message' => 'Bad Arguments'
            ]); 

        }
        $uid = $this->getUser()->getId();

        $gwJob = new EmsGatewayJob();
        $gwJob->setToGateway($gwId);
        
        $now = new \DateTime();
        $valid = (clone($now))->modify('+10 minutes');

        $gwJob->setCreatedAt($now);
        $gwJob->setValidTo($valid);
        $gwJob->setCommandGiver($uid);
        $gwJob->setJob($command);
        $gwJob->setJob(json_encode($command));

        try {
            $em = $this->getDoctrine()->getManager();
            $em->persist($gwJob);
            $em->flush();
            $statusCode = Response::HTTP_OK;
            $msg = 'OK';
        } catch(Exception $e) {
            $statusCode = Response::HTTP_BAD_REQUEST;
            $msg = $e->getMessage();
        }
        return new JsonResponse( [
            'code' => $statusCode,
            'message' => $msg
        ], $statusCode );
    }

    private function formatToDateTime($dateTimeStr)
    {
        // normal format Y-m-d H:i:s
        $dateTime = DateTime::createFromFormat('Y-m-d H:i:s', $dateTimeStr);
        if ( $dateTime != false ) {
            return $dateTime;
        }

        // from gateway
        $dateTime = DateTime::createFromFormat('Y-m-d\TH:i:s.u', $dateTimeStr);
        if ( $dateTime != false ) {
            return $dateTime;
        }

        // try RCF3339_EXTENDED
        $dateTime = DateTime::createFromFormat(DateTime::RFC3339_EXTENDED, $dateTimeStr);
        if  ($dateTime != false ) {
            return $dateTime;
        }

        // try ATOM
        $dateTime = DateTime::createFromFormat(DateTime::ATOM, $dateTimeStr);
        if ( $dateTime != false ) {
            return $dateTime;
        }

        // you may try other format here

        return null;
    }

    public function setStatusAction(Request $request, EmsGatewayMaster $gateway)
    {
        $this->denyAccessUnlessGranted('edit', new EmsGatewayMaster() );

        $gatewayId = $gateway->getGatewayId();

        $data = json_decode($request->getContent(), true);
        if ( $data == false || $data == null ) {
            return new JsonResponse([
                'code' => Response::HTTP_BAD_REQUEST,
                'message' => 'no post data'
            ]);
        }

        $em = $this->getDoctrine()->getManager();

        // for bulk data, flush every 20 entities
        // https://www.doctrine-project.org/projects/doctrine-orm/en/2.6/reference/batch-processing.html#bulk-inserts
        $batchSize = 20;
        $count = 0;
        foreach($data as $circuitId => $circuitStatus) {
            foreach($circuitStatus as $oneStatus) {
                $updateTime = isset($oneStatus['update_time'])? $this->formatToDateTime($oneStatus['update_time']):null;
                $statusTime = isset($oneStatus['last_plug_status_time'])? $this->formatToDateTime($oneStatus['last_plug_status_time']):null;
                $plugStatus = isset($oneStatus['plug_status'])?$oneStatus['plug_status']:null ;
                $accPower = isset($oneStatus['acc_power'])?$oneStatus['acc_power']:null;
                $circuitPower = isset($oneStatus['circuit_power'])? $oneStatus['circuit_power']:null;
                
                $stationStatus = new EvChargingStationStatus();
                $stationStatus->setUpdateTime($updateTime);
                $stationStatus->setLastPlugStatusTime($statusTime);
                $stationStatus->setPlugStatus($plugStatus);
                $stationStatus->setAccPower($accPower);
                $stationStatus->setCircuitPower($circuitPower);
                $stationStatus->setCircuitId($circuitId);
                $stationStatus->setGatewayId($gatewayId);
                $stationStatus->setCreatedAt(new DateTime());

                $count++;
                $em->persist($stationStatus);
                if ( $count % $batchSize === 0 ) {
                    $em->flush();
                    $em->clear();
                }
            }
        }
        // for entities that did not make up to 20
        $em->flush();
        $em->clear();
        return new JsonResponse([
            'code' => Response::HTTP_OK,
            'message' => 'ok'
        ]);
    }
}
