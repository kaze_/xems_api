<?php
/**
 * Created by PhpStorm.
 * User: chiehyingpan
 * Date: 2018/3/19
 * Time: 下午2:31
 */

namespace AppBundle\Controller;

use AppBundle\Entity\EmsGatewayStructure;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\EmsGatewayMaster;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validation;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Length;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Context\Context;

class GatewayStructureController extends BaseApiController
{

    public function setParentAction(Request $request, EmsGatewayMaster $gateway)
    {
        $validator = Validation::createValidator();
        $errors = $validator->validate($request->request->all(),
            new Collection(array(
                'parent' => [new NotNull(), new Length(array('max' => 36))],
            ))
        );

        if (isset($errors[0])) {
            return $this->returnJsonResponse(
                $request, array(
                'code' => JsonResponse::HTTP_BAD_REQUEST,
                'msg' => $errors->get(0)->getPropertyPath() . ' ' . $errors->get(0)->getMessage()
            ));
        }

        $trees = $this->get('app.gateway_tree');
        $res = $trees->setParent($gateway, $request->get('parent'));

        if ($res == null) {
            return $this->returnJsonResponse($request, array(
                'code' => JsonResponse::HTTP_BAD_REQUEST,
                'msg' => 'cannot set parent for gateway'
            ));
        }
        return $this->returnJsonResponse($request, array(
            'code' => JsonResponse::HTTP_OK
        ));
    }

    public function setChildrenAction(Request $request, EmsGatewayMaster $gateway)
    {
/*        $validator = Validation::createValidator();
        $errors = $validator->validate($request->request->all(),
            new Collection(array(
                'children' => [new NotNull(), new Length(array('max' => 36))],
            ))
        );

        if (isset($errors[0])) {
            return $this->returnJsonResponse(
                $request, array(
                'code' => JsonResponse::HTTP_BAD_REQUEST,
                'msg' => $errors->get(0)->getPropertyPath() . ' ' . $errors->get(0)->getMessage()
            ));
        }*/

        $trees = $this->get('app.gateway_tree');
        $children = $request->get('children');
        $msg = array();
        foreach($children as $child) {
            $res = $trees->setParent($child, $gateway);
            if ($res == null) {
                $msg[] = 'cannot set child gateway: '.$child;
            }
        }

        if ( count($msg) == count($children)) {
            $ret = array(
                'code' => JsonResponse::HTTP_BAD_REQUEST,
                'msg' => $msg
            );
        } else {
            $ret = array(
                'code' => JsonResponse::HTTP_OK
            );
        }

        return $this->returnJsonResponse($request, $ret);
    }

    public function getChildrenAction(Request $request, EmsGatewayStructure $gatewayStr)
    {
        $view = View::create();
        $context = (new Context())->addGroup('list');
        $direct = $request->get('direct');

        $trees = $this->get('app.gateway_tree');
        if ( $direct == 'true' ) {
            $children = $trees->getDirectChildren($gatewayStr);
        } else {
            $children = $trees->getChildren($gatewayStr);
        }
        if (!$children) {
            $view->setStatusCode(Response::HTTP_NOT_FOUND);
            $view->setData(array(
                'code' => Response::HTTP_NOT_FOUND,
                'msg' => 'no station found'
            ));
            return $view;
        }
        $view->setContext($context);
        $view->setStatusCode(200);
        $view->setData(array('code' => 200, 'data' => $children));
        return $view;
    }

    public function getParentAction(Request $request, EmsGatewayStructure $gatewayStr)
    {
        $view = View::create();
        $context = (new Context())->addGroup('list');

        $trees = $this->get('app.gateway_tree');
        $parent = $trees->getParent($gatewayStr);
        if (!$parent) {
            $view->setStatusCode(Response::HTTP_NOT_FOUND);
            $view->setData(array(
                'code' => Response::HTTP_NOT_FOUND,
                'msg' => 'no station found'
            ));
            return $view;
        }
        $view->setContext($context);
        $view->setStatusCode(200);
        $view->setData(array('code' => 200, 'data' => $parent));
        return $view;
    }

    public function getChargingStationsAction(Request $request, EmsGatewayStructure $gatewayStr)
    {
        $view = View::create();
        $context = (new Context())->addGroup('list');

        $trees = $this->get('app.gateway_tree');
        $stations = $trees->getChargingStations($gatewayStr);
        $view->setContext(($context));
        if ( !$stations ) {
            $view->setStatusCode(Response::HTTP_NOT_FOUND);
            $view->setData(array(
                'code' => Response::HTTP_NOT_FOUND,
                'msg' => 'no station found'
            ));
            return $view;
        }
        $view->setContext($context);
        $view->setStatusCode(200);
        $view->setData(array('code' => 200, 'data' => $stations));
        return $view;
    }

    public function removeFromTreeAction(Request $request, EmsGatewayStructure $gatewayStr)
    {
        $view = View::create();
        $context = (new Context())->addGroup('list');

        $trees = $this->get('app.gateway_tree');
        $res = $trees->removeNode($gatewayStr);

        $view->setContext($context);
        $view->setStatusCode(200);
        $view->setData(array('code' => 200, 'data' => $res));
        return $view;
    }

}
