<?php
/**
 * Created by PhpStorm.
 * User: chiehyingpan
 * Date: 2018/1/15
 * Time: 下午5:24
 */

namespace AppBundle\Controller;

use AppBundle\Entity\EmsCircuitMaster;
use AppBundle\Entity\EmsSmartRelayTransactionLog;
use EvBundle\Services\ChargingPolicy\ChargingPolicy;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Psr\Log\LoggerInterface;

class IotController extends Controller
{
    private $iotConn;
    
//     public function __construct()
//     {
//         $this->iotConn = $this->get('app.kaa');
//         // $this->iotConn = $this->get('app.iothub');
//     }

    /* status: On/Off */
    private function setPowerSwitch($circuit, $action, $extra = null)
    {
/*        $device = $circuit->getValidDevice();
        if( $device == null ) {
            return new JsonResponse(array(
                'code' => Response::HTTP_NOT_FOUND,;
                'msg' => 'device not found'
            ));
        }*/

        $logger = $this->get('logger');
        $this->iotConn = $this->get('app.kaa');
        // $this->iotConn = $this->get('app.iothub');

        $result = $this->iotConn->powerSwitch($circuit, $action, $extra);
        /* if ( $result == null) {
            $code = 400;
        } else {
            $code = Response::HTTP_OK;
        } */
        $code = Response::HTTP_OK;
        return new JsonResponse(array(
            'code' => $code,
            'data' => $result
        ), Response::HTTP_OK);
    }


    // FIXME : if a plate owned by 2 user
    protected function findUserByPlate($plate)
    {
        $logger = $this->get('logger');

        $conn = $this->getDoctrine()->getManager()->getConnection();
        $stmt = $conn->prepare('
            SELECT USER_ID, EV_PLATE_CANONICAL FROM ev_user_own_vehicle 
            WHERE EV_PLATE_CANONICAL =:plate
            LIMIT 1
        ');
        $stmt->bindValue(':plate',$plate, \PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetchAll( \PDO::FETCH_ASSOC );
        
        if ( isset($result[0]['USER_ID']) ) {
            $str = $result[0]['USER_ID'];
            //$logger->info("findUserByPlate find plate,", [ "plate" => $plate, "user_id" => $str ]);
            return $str;
        } else {
            $logger->info("findUserByPlate no plate found", [ "plate" => $plate]);
        }

        return null;
    }

    // FIXME : if an idTag owned by 2 user
    protected function findUserByIdTag($idTag)
    {
        $logger = $this->get('logger');

        $conn = $this->getDoctrine()->getManager()->getConnection();
        $stmt = $conn->prepare('
            SELECT USER_ID, ID_TAG FROM ev_user_own_idtag
            WHERE ID_TAG =:idTag
            LIMIT 1
        ');
        $stmt->bindValue(':idTag',$idTag, \PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetchAll( \PDO::FETCH_ASSOC );
        
        if ( isset($result[0]['USER_ID']) ) {
            $str = $result[0]['USER_ID'];
            //$logger->info("findUserByIdTag ", [ "idTag" => $idTag, "user_id" => $str ]);
            return $str;
        } else {
            $logger->info("findUserByIdTag no idTag found", [ "idTag" => $idTag ]);
        }

        return null;
    }


    // V190531(first version, no carplate)
    // V200220(from  app)
    // V200226(from carplate sensor, with special uid)
    public function checkBarcode0($barcode, $tokens = null)
    {
        $logger = $this->get('logger');
        $prefix =  "XMIGHT";
        $vstrlen = strlen( "V190531" );

        // format : prefix, uuid, crypted part(time,uuid,padding)

        $time_len = 14; $uuid_len = 36; $padding_len = 14;
        $plate_len = 8;
    
        $prefix_len = strlen( $prefix );
        $total_prefix_len = $prefix_len + $vstrlen + 2;

        if ( strlen($barcode) < $total_prefix_len + $uuid_len + ( $time_len + $uuid_len + $padding_len ) ) {
            $logger->error("barcode error : wrong length. ", [$barcode] );
            return false;
        }

        $barcode = strtoupper($barcode);

        if ( substr($barcode, 0, $prefix_len) !== $prefix ) {
            $logger->error("barcode error : invalid prefix . ", [$barcode] );
            return false;
        }

        $vstr = substr($barcode, $prefix_len+1, $vstrlen) ;

        $uuid = strtolower(substr($barcode, $total_prefix_len, $uuid_len));
        $ciphertext = substr($barcode,$total_prefix_len + $uuid_len);

        //$logger->info("checkBarcode get ", ['uuid' => $uuid, 'ciphertext' => $ciphertext]);

        //$plaintext = $this->decrytByLibSodium( $ciphertext );
        // $plaintext = $this->decodeByHex2Bin($ciphertext);
        $plaintext = $ciphertext;

        if ( strlen($plaintext) != $time_len + $uuid_len + $padding_len ) {
            $logger->error(__FILE__ . " barcode error : invalid length. ", ['code' => $barcode, 'plaintext' => $plaintext] );
            return false;
        }


            $timestr = substr($plaintext, 0, $time_len);
            $hidden_uuid = strtolower(substr($plaintext, $time_len, $uuid_len));
            
            if ( $vstr === "V190531" ) {
                $platestr = null;
            } else {
                // V200226(from carplate sensor)
                // V200220(from  app)
                $platestr = trim(substr($plaintext, $time_len+$uuid_len, $plate_len));
            }

            $created_at = \DateTime::createFromFormat('YmdHis', $timestr);
            $now = new \DateTime("now");
            $diff = $now->getTimestamp() - $created_at->getTimestamp() ;
            
            //$logger->info("barcode checktime ", ["now" => $now, "created" => $created_at, 
            //    "timestr" => $timestr, "diff" => $diff] );

            //$diff = 10;

            if ( $diff >= 0 && $diff <= 20 * 60 ) {
                if ( $hidden_uuid === $uuid ) {
                    //$logger->info("barcode check pass ", ['uuid' => $uuid, "plate" => $platestr] );
                    return [ 'uuid' => $uuid, "plate" => $platestr  ];
                } else {
                    $logger->info("barcode error: uuid mismatch", ['code' => $barcode, 
                            'uuid' => $uuid, 'hidden' => $hidden_uuid] );
                    return false;
                }
            }

        return false;
    }


    // "XMIGHT_V200301_" + requestId + "_" + idTag
    public function checkBarcodeV200301($barcode, $tokens)
    {
        if ( count($tokens) < 4 ) {
            return false;
        }

        $userId = $this->findUserByIdTag( $tokens[3] );
        if ( $userId != null ) {
            return [ 'uuid' => $userId, "plate" => null, "func" => 3];
        } else {
            $userId = '058a5a31-1bb4-48b3-9755-7eb557d7be34';
            return [ 'uuid' => $userId, "plate" => null, "func" => 3];
        }
    }

    // V190531(first version, no carplate)
    // V200220(from  app)
    // V200226(from carplate sensor, with special uid)
    // return // [ 
    // 'uuid' => user_id (required)
    // 'plate' => plate or null (required)
    // 'func_id' => function id in notification (optional) 
    // ]
    public function checkBarcode($barcode)
    {
        $func_map = [
            'V190531' => 'checkBarcode0',
            'V200220' => 'checkBarcode0',
            'V200226' => 'checkBarcode0',
            'V200301' => 'checkBarcodeV200301',
        ];

        $logger = $this->get('logger');
        $prefix =  'XMIGHT';
        $vstrlen = strlen( 'V190531' );


        // XMIGHT_V190531_.....
        $prefix_len = strlen( $prefix );
        $total_prefix_len = $prefix_len + $vstrlen + 2;

        if ( strlen($barcode) < $total_prefix_len) {
            $logger->error("barcode error : wrong length. ", [$barcode] );
            return false;
        }

        $tokens = explode('_' , $barcode);
        if ( count($tokens) < 3 ) {
            $logger->error("barcode error : invalid format. ", [$barcode] );
            return false;
        }

        $vstr = $tokens[1];

        if ( isset($func_map[$vstr]) ) {
            $func = $func_map[$vstr];
            return $this->$func($barcode, $tokens);
        } else {
            $logger->error('barcode error : unknown version.', [$barcode] );
        }

        return false;

    }


    public function setPowerSwitchOnAction(EmsCircuitMaster $circuit ) {
        $this->denyAccessUnlessGranted('view', $circuit);
        $relay = $this->get('app.relay');
        $relay->setStatus($circuit,
            EmsSmartRelayTransactionLog::ACT_METERON,
            EmsSmartRelayTransactionLog::SOURCE_USER,
            $this->getUser()->getId());
        $result = $this->setPowerSwitch($circuit, 'On');
        // $this->setPowerSwitch($circuit, 'Report');
        return $result;
    }

    public function setPowerSwitchOffAction(EmsCircuitMaster $circuit) {
        $this->denyAccessUnlessGranted('view', $circuit);
        $relay = $this->get('app.relay');
        $relay->setStatus($circuit,
            EmsSmartRelayTransactionLog::ACT_METEROFF,
            EmsSmartRelayTransactionLog::SOURCE_USER,
            $this->getUser()->getId());
        $result = $this->setPowerSwitch($circuit, 'Off');
        // $this->setPowerSwitch($circuit, 'Report');
        return $result;
    }

    /** 
     * err: (目前只有訂一個)
     * 1: 餘額不足
     */
    private function chargingBarcodeReturn($chargingCheck, $circuit, $userId, $kaaEndpointHash, $barcode, $session_oid, $evPlate, $comment, $err=0)
    {
        $relay = $this->get('app.relay');
        $extra_args = [ "chargingBarcode" => $barcode, 
                        "endpointKeyHash" => $kaaEndpointHash ,
                        "authErrorCode" => $err,
        ] ;

        if ( $comment ) {
            $comment = array_merge($comment, ['barcode' => $barcode]);
        } else {
            $comment = ['barcode' => $barcode];
        }

        if ( $chargingCheck != ChargingPolicy::VALIDATE_OK ) {
            $relay->setStatus($circuit->getCircuitId(),
                EmsSmartRelayTransactionLog::ACT_METEROFF,
                EmsSmartRelayTransactionLog::SOURCE_USER,
                $barcode,
                'MeterOff', 
                $kaaEndpointHash,
                json_encode($comment),
                $session_oid,
                $evPlate
                );
            return $this->setPowerSwitch($circuit, 'Off', $extra_args);
        } else {
            $relay->setStatus($circuit->getCircuitId(),
                EmsSmartRelayTransactionLog::ACT_METERON,
                EmsSmartRelayTransactionLog::SOURCE_USER,
                $userId,
                'MeterOn', 
                $kaaEndpointHash,
                json_encode($comment),
                $session_oid,
                $evPlate
                );
            return $this->setPowerSwitch($circuit, 'On', $extra_args);
        }
    }
    // 
    public function chargingBarcodeAction(Request $request)
    {
        $circuitId = $request->get('circuitId');
        $barcode = $request->get('userId');          // barcode
        $kaaEndpointHash = $request->get('hash');

        $em = $this->getDoctrine()->getManager();

        // get charging station : object || null 
        $circuit = $em->getRepository('AppBundle:EmsCircuitMaster')->find($circuitId);
        $stations = $circuit->getChargingStation();
        $station = null;
        if ( $stations && count($stations) > 0 ) {
            $station = $stations[0];
        }

        $logger = $this->get('logger');

        // get barcode data
        $r = $this->checkBarcode($barcode);
        $userOb = null;
        $userId = null;
        $evPlate = null;
        $guestId = null;
        $chargingCheck = ChargingPolicy::VALIDATE_BAD_USER;
        if ( $r ) {
            $userId = $r['uuid'];
            $evPlate = $r['plate'];
            if ( isset( $r['func'] ) )
                $extra_args['func'] = $r['func'];

            $chargingControll = $this->get('ev.charging_controll');
            if ( $station != null) {
                $data = $chargingControll->getValidateData($userId, $evPlate, $station); 
                $userId = $data['userId'];
                $guestId = $data['guestId'];
                $chargingCheck = $chargingControll->validateCharging($userId, $evPlate, $station, $guestId);
            }
            $err = 0;
            switch($chargingCheck) {
                case ChargingPolicy::VALIDATE_NO_BALANCE:
                    $err = 1;
                case ChargingPolicy::VALIDATE_BAD_USER:
                    return $this->chargingBarcodeReturn(
                        $chargingCheck, 
                        $circuit, 
                        $userId, 
                        $kaaEndpointHash, 
                        $barcode, 
                        null, 
                        $evPlate, 
                        null,
                        $err);
                break;
                case ChargingPolicy::VALIDATE_OK:
                    // do nothing, continue;
                break;
            }
            // no way，這邊應該不會是 null
        } else {
            $comment = ["message" =>  "bad barcode"];
            return $this->chargingBarcodeReturn(
                $chargingCheck, 
                $circuit, 
                $userId, 
                $kaaEndpointHash, 
                $barcode, 
                null, 
                $evPlate, 
                $comment,
                0);
        }

        $session_oid = null;
        if ($station) {
            $sessionOb = $this->get('ev.session')->getLatestSessionByStation($station, true);
            // 如果 session 先開始了
            if ( $sessionOb != null ) {
                $chargingUser = $sessionOb->getUserId();

                if ( $chargingUser == null || $chargingUser->getId() == null ) {
                    // $logger->info(__FILE__ . " chargingBarcodeAction active session no user, use the session ", [ $userId ] );
                    if ( $userId ) {
                        $userOb = $em->find('AppBundle:User', $userId);
                        $sessionOb->setUserId($userOb);
                        if ( $evPlate ) $sessionOb->setEvPlate($evPlate);
                        try {
                            $em = $this->getDoctrine()->getManager();
                            $em->persist($sessionOb);
                            $em->flush();
                            $session_oid = $sessionOb->getOId();   
                        } catch(\Exception $e) {
                            $logger->error(__FILE__ . "chargingBarcodeAction error ", [$e->getCode(), $e->getMessage()] );
                        }
                    } else {
                        //$logger->info("chargingBarcodeAction: no active session to set", [] );
                    }
                }
            } // end of $sessionOb
        }
        
        // 認證通過
        $guestArr = null;
        if ($guestId) {
            $guestArr = ['guestId' => $guestId];
        }
        return $this->chargingBarcodeReturn(
            $chargingCheck, $circuit, $userId, $kaaEndpointHash, $barcode, $session_oid, $evPlate, $guestArr, 0);
    }


    // since it's ugly code, I add no try-catch , just let it throw error
    protected function saveParkingEvent($circuit_id, $event_id, $message)
    {
        $logger = $this->get('logger');
        $event_data = json_decode($message, TRUE);
        // $logger->info("saveParkingEvent", [$circuit_id, $event_id, $event_data]);

            $conn = $this->getDoctrine()->getManager()->getConnection();
            $stmt = $conn->prepare('
                INSERT INTO ev_parking_event(`circuit_id`,`event_id`,`event_name`, `created_at`, `ev_plate`,`image_url`)
                VALUES(:circuit_id, :event_id, :event_name, :created_at, :ev_plate, :image_url)
            ');

            $stmt->bindValue(':circuit_id', $circuit_id, \PDO::PARAM_STR);

            $stmt->bindValue(':event_id',$event_id, \PDO::PARAM_INT);
            if ( $event_id == 8 ) {
                $event_name = "PARKING_START";
            } else if ( $event_id == 9 ) {
                $event_name = "PARKING_END";
            } else {
                $event_name = "UNKNOWN";
            }
            $stmt->bindValue(':event_name',$event_name, \PDO::PARAM_STR);


            if ( isset($event_data['plate']) )
                $stmt->bindValue(':ev_plate', $event_data['plate'], \PDO::PARAM_STR);
            else
                $stmt->bindValue(':ev_plate', null, \PDO::PARAM_INT);

            if ( isset($event_data['event_time']) )
                $stmt->bindValue(':created_at', $event_data['event_time'], \PDO::PARAM_STR);
            else
                $stmt->bindValue(':created_at', null, \PDO::PARAM_INT);

            if ( isset($event_data['img']) )
                $stmt->bindValue(':image_url', $event_data['img'], \PDO::PARAM_STR);
            else
                $stmt->bindValue(':image_url', null, \PDO::PARAM_INT);


            $stmt->execute();
    }


    // request args: 
    //    circuitId,timestamp,eventId,msg,hash
    // msg : 
    // {"img":"http://192.168.1.200/img/ParkingSpaces/P003/ABC123_Park遠雄大未來(特斯拉充電樁)-P003.jpg","parking_start":"2020/12/30 23:20:03","plate":"ABC1357","source":"e19df2ea-23ec-4860-9be4-fbf99b8cb918","event_time":0}
    // { "img" : ..., "parking_start":... ,"plate":... , "source":..., "event_time": }
    public function changeParkingStatusAction(Request $request)
    {
        $logger = $this->get('logger');

        $circuitId = $request->get('circuitId');
        $circuitRepo = $this->getDoctrine()->getManager()->getRepository('AppBundle:EmsCircuitMaster');
        try {
            $circuit = $circuitRepo->find($circuitId);
        } catch ( \EXCEPTION $e ) {
             return $this->returnJsonResponse($request, array(
                'code' => JsonResponse::HTTP_NOT_FOUND,
                'msg' => 'circuit not found'
            ));
        }
        if ( !$circuit ) {
            return $this->returnJsonResponse($request, array(
                'code' => JsonResponse::HTTP_NOT_FOUND,
                'msg' => 'circuit not found'
            ));
        }
        $eventId = (int) $request->get('eventId');
        $timestamp= $request->get('timestamp');
        $message = $request->get('msg');
        // $logger->info("changeStatus 2", [$status, $message]);

        $this->saveParkingEvent($circuitId, $eventId, $message );

        $code = Response::HTTP_OK;
        $result = [];
        return new JsonResponse(array(
            'code' => $code,
            'data' => $result
        ), Response::HTTP_OK);
    }


    // since it's ugly code, I add no try-catch , just let it throw error
    // TODO : wait sombody to rewrite it by Entity
    protected function saveSysGatewayEvent($gateway_id, $args)
    {
        $logger = $this->get('logger');
        $event_data = json_decode( $args['msg'] , TRUE);
        $event_id = $args['eventId'];

            $conn = $this->getDoctrine()->getManager()->getConnection();
            $stmt = $conn->prepare('
                INSERT INTO sys_gateway_event(`created_at`, `gateway_id`, `event_id`, `software_version`,`reason`)
                VALUES(:created_at, :gateway_id, :event_id, :version, :reason)
            ');

            $stmt->bindValue(':gateway_id', $gateway_id, \PDO::PARAM_STR);
            $stmt->bindValue(':event_id', $event_id, \PDO::PARAM_STR);

            if ( isset($event_data['version']) ) {
                $stmt->bindValue(':version', $event_data['version'], \PDO::PARAM_STR);
                unset($event_data['version']);
            } else {
                $stmt->bindValue(':version', null, \PDO::PARAM_INT);
            }

            if ( isset($event_data['event_time']) ) {
                $stmt->bindValue(':created_at', $event_data['event_time'], \PDO::PARAM_STR);
                unset($event_data['event_time']);
            } else {
                $stmt->bindValue(':created_at', null, \PDO::PARAM_INT);
            }

            if ( isset($event_data['reason']) ) {
                $stmt->bindValue(':reason', $event_data['reason'], \PDO::PARAM_STR);
                //unset($event_data['reason']) ;
            } else {
                $rstr = json_encode( $event_data );
                $stmt->bindValue(':reason', $rstr, \PDO::PARAM_STR);
            }

            $stmt->execute();
    }
    

    protected function findGatewayId($id)
    {
        // guess id is meter
        $repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:EmsDeviceMaster');
        $device = $repository->find($id);

        if ( $device != null ) {
            $gws = $device->getGateway() ;
            if ( $gws && count($gws) > 0 ) 
                return $gws[0]->getGatewayId();
            return null;
        }

        // guess id is gateway
        $repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:EmsGatewayMaster');
        $gateway = $repository->find($id);
        if ( $gateway != null ) 
            return $id;


        // guess id is circuit
        $repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:EmsCircuitMaster');
        $circuit = $repository->find($id);
        if ( $circuit != null ) {
            $device = $circuit->getValidDevice();
            if ( $device != null ) {
                $gateway = $device->getValidGateway();
                return $gateway->getGatewayId()->__toString();
            }
        }

        return null;
    }

    // id : meter id
    protected function xpclientUpEvent($request, $id, $args)
    {
        $gateway_id = $this->findGatewayId( $id );

        if ( $gateway_id ) {
            $this->saveSysGatewayEvent($gateway_id, $args);
        } else  {
            $logger = $this->get('logger');
            $logger->error("xpclientUpEvent : unknown id ", ["id" => $id]);

            return new JsonResponse( [
                'code' => JsonResponse::HTTP_NOT_FOUND,
                'msg' => 'device not found. '
                ],
                Response::HTTP_NOT_FOUND
            );
        }

        return new JsonResponse(array(
            'code' => Response::HTTP_OK,
        ), Response::HTTP_OK);

    }


    protected function hostBootEvent($request, $id, $args)
    {

        $logger = $this->get('logger');
        $gateway_id = $this->findGatewayId( $id );
        //$logger->info("hostBootEvent", [ $gateway_id ]);
        
        if ( $gateway_id ) {
            $this->saveSysGatewayEvent($gateway_id ,$args);
        } else {
            if ( isset($args['msg']) ) {
                $event_data = json_decode( $args['msg'] , TRUE);
                if ( isset($event_data['reason']) ) {
                    $append_str = $event_data['reason'] . " id not found" ;
                } else {
                    $append_str = " id not found";
                }
                $event_data['reason'] = $append_str ;
                $args['msg'] = json_encode($event_data);
                
            }
            $this->saveSysGatewayEvent( $id,$args);
            $logger->error("xpclientUpEvent : unknown id ", ["id" => $id, "args" => $args]);
        }
        return new JsonResponse(array(
            'code' => Response::HTTP_OK,
        ), Response::HTTP_OK);
    }

    protected function gatewayxxEvent($request, $id, $args)
    {

        $logger = $this->get('logger');
        $gateway_id = $this->findGatewayId( $id );

        if ( $gateway_id ) {
            $this->saveSysGatewayEvent($gateway_id ,$args);
        } else {
            if ( isset($args['msg']) ) {
                $event_data = json_decode( $args['msg'] , TRUE);
                if ( isset($event_data['reason']) ) {
                    $append_str = $event_data['reason'] . ",id not found" ;
                } else {
                    $append_str = " id not found";
                }
                $event_data['reason'] = $append_str ;
                $args['msg'] = json_encode($event_data);
                
            }
            $this->saveSysGatewayEvent( $id,$args);
        }
    }


    // request : 
    //  circuitId
    //  timestamp, notice:  
    //      timestamp : at what time the event fired
    //      datetime in msg : at what time the event happened
    //  eventId
    //  msg
    //  hash

    public function getEventsAction(Request $request)
    {
        $logger = $this->get('logger');

        $id = $request->get('circuitId');
        $eventId = (int) $request->get('eventId');
        $message = $request->get('msg');        // json
        $hash = $request->get('hash');

        $args = [
            "id" => $id,
            "eventId" => $eventId,
            "msg" => $message,
            "hash" => $hash,
        ];

        if ( ! $id || ! $eventId || ! $message ) {
            return new JsonResponse( [
                'code' => JsonResponse::HTTP_BAD_REQUEST,
                'msg' => 'missing arguments.' 
                ],
                Response::HTTP_BAD_REQUEST
            );
        }

        switch( $eventId ) {
            case 999:       // xpclientUp
                return $this->xpclientUpEvent($request, $id, $args);
                break;
            case 998:       // hostUp
                $this->hostBootEvent($request, $id, $args);
                return new JsonResponse(array(
                    'code' => Response::HTTP_OK,
                ), Response::HTTP_OK);
                break;
            default :
                $this->gatewayxxEvent($request, $id, $args);
                return new JsonResponse(array(
                    'code' => Response::HTTP_OK,
                ), Response::HTTP_OK);
                break;
        }


    }

}


