<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class JavascriptController extends Controller
{
    public function langAction($locale, $func)
    {
        return $this->render("AppBundle:Javascript:$func.html.twig", array(
            // ...
        ));
    }

}
