<?php
/**
 * Created by PhpStorm.
 * User: chiehyingpan
 * Date: 2018/1/15
 * Time: 下午5:24
 */

namespace AppBundle\Controller;

use AppBundle\Entity\EmsCircuitMaster;
use AppBundle\Entity\EmsGatewayMaster;
use AppBundle\Entity\EmsSmartRelayTransactionLog;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class KaaController extends Controller
{

/*    public function getAppByAppTokenAction()
    {
        $kaaConn = $this->get('app.kaa');
        $result = $kaaConn->getAppByAppToken($this->appToken);
        return new JsonResponse(array(
            'code' => Response::HTTP_OK,
            'data' => $result),
        Response::HTTP_OK );
    }
*/
    public function getEndpointGroupsByAppTokenAction($appToken)
    {
        $kaaConn = $this->get('app.kaa');
        $result = $kaaConn->getEndpointGroupsByAppToken($appToken);
        return new JsonResponse(array(
            'code' => Response::HTTP_OK,
            'data' => $result),
            Response::HTTP_OK );
    }

    public function getConfigurationSchemasByApplicationTokenAction($appToken)
    {
        $kaaConn = $this->get('app.kaa');
        $result = $kaaConn->getConfigurationSchemasByAppToken($appToken);
        return new JsonResponse(array(
            'code' => Response::HTTP_OK,
            'data' => $result),
        Response::HTTP_OK
        );
    }


    public function getNotificationSchemasByAppTokenAction()
    {
        $kaaConn = $this->get('app.kaa');
        $result = $kaaConn->getNotificationSchemasByAppToken();
        return new JsonResponse(array(
            'code' => Response::HTTP_OK,
            'data' => $result),
            Response::HTTP_OK
        );
    }

    public function getNotificationTopicsAction()
    {
        $kaaConn = $this->get('app.kaa');
        $result = $kaaConn->getNotificationTopics();
        return new JsonResponse(array(
            'code' => Response::HTTP_OK,
            'data' => $result),
            Response::HTTP_OK
        );
    }

    public function getEndpointGroupByGatewayIdAction($gatewayId)
    {
        $kaaConn = $this->get('app.kaa');
        $result = $kaaConn->getEndpointGroupByGatewayId($gatewayId);
        return new JsonResponse(array(
            'code' => Response::HTTP_OK,
            'data' => $result),
        Response::HTTP_OK );
    }

    public function getGatewayNotificationTopicsAction($gatewayId)
    {
        $kaaConn = $this->get('app.kaa');
        $result = $kaaConn->getGatewayNotificationTopics($gatewayId);
        return new JsonResponse(array(
            'code' => Response::HTTP_OK,
            'data' => $result),
            Response::HTTP_OK );
    }

    public function getEndpointProfileByGatewayAction($gatewayId)
    {
        $kaaConn = $this->get('app.kaa');
        $result = $kaaConn->getEndpointProfileByGateway($gatewayId);
        return new JsonResponse(array(
            'code' => Response::HTTP_OK,
            'data' => $result),
            Response::HTTP_OK );
    }

    public function addTopicToEpGroupAction(Request $request)
    {
        $kaaConn = $this->get('app.kaa');
        $groupId = $request->get('groupId');
        $topicId = $request->get('topicId');

        $result = $kaaConn->addTopicToEpGroup($groupId, $topicId);
        return new JsonResponse(array(
            'code' => Response::HTTP_OK,
            'data' => $result),
            Response::HTTP_OK );
    }

    /* status: On/Off */

    public function setConfigurationAction(Request $request, EmsGatewayMaster $gateway)
    {

/*        $modbusRepo = $this->getDoctrine()->getManager()->getRepository('AppBundle:EmsModbusGateway');
        $deviceConfig = $modbusRepo->findDeviceConfigurtionsByGateway($gatewayId);
        $serializer = $this->get('jms_serializer');
        $data = $serializer->serialize($deviceConfig, 'json', SerializationContext::create()->setGroups(array('toGateway')));*/
        //$data = $serializer->toArray($deviceConfig, SerializationContext::create()->setGroups(array('toGateway')));
        // $data = $serializer->normalize($deviceConfig, null, ['groups' => ['toGateway']]);
        $deviceManager = $this->get('app.device_manager');
        $schemaVersion = $request->get('schema_version');

        if ( $schemaVersion == null ) {
            return new JsonResponse(array(
                'code' => Response::HTTP_BAD_REQUEST,
                'message' => "schema_version cannot be null ",
            ), Response::HTTP_BAD_REQUEST);
        }

        $data = $deviceManager->gatewayConfiguration($gateway);
        $kaaConn = $this->get('app.kaa');
        $gatewayId = $gateway->getGatewayId();
        $result = $kaaConn->sendGatewayConfiguration(
            is_string($gatewayId) ? $gatewayId : $gatewayId->toString(),
            array(
                'devices' => json_encode($data['devices']),
                'gatewayStructure' => isset($data['gatewayStructure']) ? json_encode($data['gatewayStructure']) : '{}'
            ),
            $schemaVersion
        );
        return new JsonResponse(array(
            'code' => Response::HTTP_OK,
            'data' => $result
        ), Response::HTTP_OK);
    }
}
