<?php
namespace AppBundle\Controller;

use AppBundle\Entity\EmsLocationMaster;
use AppBundle\Entity\EmsSmartRelayTransactionLog;
use AppBundle\Entity\Validate\ApiParameters;
use AppBundle\Entity\EvChargingStationMaster;
use AppBundle\Entity\EmsCustomerLocationMapping;
use EspBundle\Entity\EspCustomerMaster;
use EvBundle\Entity\EvLocationChargingManagement;

use FOS\RestBundle\Context\Context;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\NotBlank;
use Exception;

use Doctrine\ORM\EntityManager;
use EspBundle\Entity\EspCustomerRate;

class LocationController extends BaseApiController
{
    private $em;

    public function __construct()
    {
        // $em = $this->getDoctrine()->getManager();
    }
    // demand interval = 15 minutes = 900
    const demandInterval = 900;

    /**
     * Lists all locationMaster entities.
     */
    public function allAction(Request $request)
    {
        // $this->denyAccessUnlessGranted('view', new EmsLocationMaster());
        $view = View::create();
        $context = (new Context())->addGroup('selectList');

/*        $repository = $this->getDoctrine()->getManager()->getRepository('AppBundle\Entity\EmsCustomerLocationMapping.old');
        $qb = $repository->createQueryBuilder('cl');
        $qb->join('cl.location', 'l')
            ->join('cl.customer', 'c')
            ->where('c.customerId = :customer')
            ->setParameter('customer', $customer->getCustomerId()->toString(), 'uuid');*/
        $user = $this->getUser();
        $security = $this->get('app.security');
        $repository = $this->getDoctrine()->getManager()->getRepository('AppBundle\Entity\EmsLocationMaster');
        if ( $security->checkRole('ROLE_ADMIN', $user)) {
            $result = $repository->findBy(['locationStatus' => 1], ['locationCreateDatetime' => 'DESC']);
        } else if ( $security->checkRole('ROLE_CUSTOMER_VIEWER', $user)) {
            $customer = $user->getCustomer();
            $qb = $repository->createQueryBuilder('l');
            $qb->innerJoin('AppBundle\Entity\EmsCustomerLocationMapping', 'cl', 'WITH', 'cl.locationId = l.locationId')
                ->where('cl.customerId = :customer')
                ->setParameter('customer', $customer->getCustomerId()->toString(), 'uuid');
            $result = $qb->getQuery()->getResult();
        } else {
            $statusCode = Response::HTTP_FORBIDDEN;
            $view->setStatusCode($statusCode);
            $view->setData(array(
                        'code' => $statusCode,
                        ));
            return $view;
        }
        
        if( count($result) == null || count($result) == 0 ) {
            $statusCode = Response::HTTP_NOT_FOUND;
        } else {
            $statusCode = Response::HTTP_OK;
        }

        $view->setContext($context);
        $view->setStatusCode($statusCode);
        $view->setData(array(
                    'code' => $statusCode,
                    'data' => $result
                    ));
        return $view;
    }

    /**
     * Finds and displays a locationMaster entity.
     */
    public function getAction(EmsLocationMaster $location)
    {
        $this->denyAccessUnlessGranted('view', $location);
        $view = View::create();
        $context = (new Context())->addGroup('full');
        $view->setContext($context);
        $view->setStatusCode(200);
        $view->setData(array(
                    'code' => 200,
                    'data' => $location
                    ));
        return $view;
    }

    /**
     * Creates a new locationMaster entity.
     */
    public function newAction(Request $request)
    {
        // permission set in security.yml
        $location = new EmsLocationMaster();
        $this->denyAccessUnlessGranted('edit', $location);

        return $this->processForm($request, $location);
    }

    /**
     * Displays a form to edit an existing locationMaster entity.
     */
    public function editAction(Request $request, EmsLocationMaster $location)
    {
        $this->denyAccessUnlessGranted('edit', $location);
        return $this->processForm($request, $location);
    }

    private function processForm($request, $location)
    {
        $logger = $this->get('logger');
        $view = View::create();
        $context = (new Context())->addGroup('full');

        $em = $this->getDoctrine()->getManager();
        $statusCode = $em->contains($location) ? 200 : 201;

        $data = json_decode($request->getContent(), true);

        // todo: find an simpler way to deal with relationships
        $locationHierarchyId = isset($data['locationHierarchyId']) ? $data['locationHierarchyId'] : null;
        $locationTypeId = isset($data['locationTypeId']) ? $data['locationTypeId'] : null;
        $customerId = isset($data['customerId']) ? $data['customerId'] : null;

        $locationAddress = isset($data['locationAddress'] ) ? $data['locationAddress'] : null;
        // if ( ! isset($data['locationAddress']) ) $data['locationAddress'] = "";

        // process location hierarhy and location type
        if ($locationHierarchyId == null || $locationTypeId == null || $locationAddress == null ) {
            if ($request->getMethod() == 'POST') {
                $view->setStatusCode(Response::HTTP_BAD_REQUEST);
                $view->setData(array(
                            'code' => Response::HTTP_BAD_REQUEST,
                            'message' => 'location hierarchy/location/address type cannot be null.'
                            ));
                return $view;
            }
        }
        if ($locationTypeId) {
            $locationHierarchy = $this->getDoctrine()
                ->getManager()
                ->getRepository('AppBundle:EmsLocationHierarchy')
                ->find($locationHierarchyId);
            if ($locationHierarchy == null) {
                $view->setStatusCode(Response::HTTP_BAD_REQUEST);
                $view->setData(array(
                            'code' => Response::HTTP_BAD_REQUEST,
                            'message' => 'location hierarchy/location type does not exist.'
                            ));
                return $view;
            }
            $location->setLocationHierarchy($locationHierarchy);
        }
        if ($locationHierarchyId) {
            $locationType = $this->getDoctrine()
                ->getManager()
                ->getRepository('AppBundle:EmsLocationType')
                ->find($locationTypeId);
            if ($locationType == null) {
                $view->setStatusCode(Response::HTTP_BAD_REQUEST);
                $view->setData(array(
                            'code' => Response::HTTP_BAD_REQUEST,
                            'message' => 'location hierarchy/location type does not exist.'
                            ));
                return $view;
            }
            $location->setLocationType($locationType);
        }

        // todo: link locations to customer/create location belongs to customer
        if ($customerId != null) {
            $customerLocationMapping = $this->getDoctrine()->getRepository('AppBundle:EmsCustomerLocationMapping');
            if ( $statusCode == 200 ) {
                if ($customerLocationMapping->findBy(["locationId" => $location->getLocationId(), "statusId" => 1])) {
                    $view->setStatusCode(Response::HTTP_BAD_REQUEST);
                    $view->setData(array(
                                'code' => Response::HTTP_BAD_REQUEST,
                                'message' => 'this location is already link to another customer, unlink first'
                                ));
                    return $view;
                }
            }
            $customer = $this->getDoctrine()
                ->getManager()
                ->getRepository('AppBundle:EmsCustomerMaster')
                ->find($customerId);
            if ( $customer == null) {
                $view->setStatusCode(Response::HTTP_BAD_REQUEST);
                $view->setData(array(
                            'code' => Response::HTTP_BAD_REQUEST,
                            'message' => 'customer ID does not exist.'
                            ));
                return $view;
            }
            $location->addCustomer($customer);
        }

        // process form
        $form = $this->createForm('AppBundle\Form\EmsLocationMasterType', $location);
        $clearMissing = ($request->getMethod() == 'PUT') ? true : false;
        $form->submit($data, $clearMissing);

        if ($form->isValid()) {
            if ($statusCode === 201) {
                $em->persist($location);
                $view->setLocation($this->generateUrl('xems_location_get', array(
                                'location' => $location->getLocationId()
                                ->toString()
                                )));
            }
            $em->flush();
            $view->setContext($context);
            $view->setStatusCode($statusCode);
            $view->setData(array(
                        'code' => $statusCode,
                        'data' => $location
                        ));
            if ( $customerId != null ) {
                $mapping = new EmsCustomerLocationMapping();
                $mapping->setCustomerId($customerId);
                $mapping->setLocationId($location->getLocationId());
                $mapping->setStatusId(1);
                $em->persist($mapping);
                $em->flush();
            }
        } else {
            $view->setStatusCode(400);
            $view->setData(array(
                        'code' => 400,
                        'message' => $form->getErrors()
                        ));
        }

        return $view;
    }

    /**
     * Deletes a locationMaster entity.
     */
    public function deleteAction(Request $request, EmsLocationMaster $locationMaster)
    {
        $form = $this->createDeleteForm($locationMaster);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($locationMaster);
            $em->flush();
        }
        return $this->redirectToRoute('location_index');
    }

    /**
     * Creates a form to delete a locationMaster entity.
     *
     * @param EmsLocationMaster $locationMaster
     *            The locationMaster entity
     *            
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(EmsLocationMaster $locationMaster)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('location_delete', array(
                            'locationId' => $locationMaster->getLocationId()
                            )))
            ->setMethod('DELETE')
            ->getForm();
    }

    public function listDevicesAction(Request $request, EmsLocationMaster $location)
    {
        $this->denyAccessUnlessGranted('view', $location);
        $view = View::create();
        $context = (new Context())->addGroup('list');

        $deviceType = $request->get('type');
        $mainMeter = 0;
        if ( $deviceType == 'main') {
            $mainMeter = 1;
            $deviceType = 100;
        }
        $requestParam = new ApiParameters();
        $requestParam->setDeviceType($deviceType);

        $validator = $this->get('validator');
        $errors = $validator->validate($requestParam, null, array(
                    'device'
                    ));
        if (count($errors) > 0) {
            $view->setStatusCode(Response::HTTP_BAD_REQUEST);
            $view->setData($errors);
            return $view;
        }
        
        $result = [];
        if ( $mainMeter == 1 ) {
            $circuits = $this->findMainCircuitByLocation($location);
            if ( $circuits && count($circuits) > 0 ){
                foreach($circuits as $circuit) {
                    $device = $circuit->getDevice();
                    if ( $device && count($device) > 0 )
                    $result[] = $device[0];
                }
            }
        } else {
            $repository = $this->getDoctrine()
                ->getManager()
                ->getRepository('AppBundle:EmsDeviceMaster');
            $result = $repository->findByLocation($location, $deviceType);
        }
        $view->setContext($context);
        $view->setData(array('code' => 200, 'data' => $result));
        return $view;
    }

    public function listGatewaysAction(Request $request, EmsLocationMaster $location)
    {
        $this->denyAccessUnlessGranted('view', $location);
        $view = View::create();
        $context = (new Context())->addGroup('list');

        $repository = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle\Entity\EmsGatewayMaster');
        $qb = $repository->createQueryBuilder('g');
        $qb->innerJoin('AppBundle\Entity\EmsLocationGatewayMapping', 'lg', 'WITH', 'lg.gatewayId = g.gatewayId')
            ->where('lg.locationId = :location')
            ->setParameter('location', $location->getLocationId(), 'uuid');
        $result = $qb->getQuery()->getResult();
        if ($result == null || count($result) == 0) {
            $statusCode = Response::HTTP_NOT_FOUND;
        } else {
            $statusCode = Response::HTTP_OK;
        }

        $view->setContext($context);
        $view->setStatusCode($statusCode);
        $view->setData(array(
                    'code' => $statusCode,
                    'data' => $result
                    ));
        return $view;
    }

    private function getDetail($circuit, $status, $switch, $espCustomerRateDetail)
    {
        $relay = $this->get('app.relay');
        $fee = $this->get('esp.fee');
        $report = $this->get('app.power_report');
        $repository = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:EmsPowerReportLog');
        $currentStatus = $repository->findCurrentStatusByCircuit($circuit->getCircuitId());
        if ($status == true && $currentStatus != null) {
            // $charge = $fee->deviceFee(new \DateTime('first day of this month'), null, $circuit);
            $charge = $report->getCircuitFee($circuit, new \DateTime('first day of this month'), null, $espCustomerRateDetail);
            if ($charge) {
                $currentStatus['fee'] = $charge['amount'];
            }
            $circuit->setCurrentStatus($currentStatus);
        }
        $equipmentStatus = 'Off';
        $switchStatus = 'Off';
        $status = $relay->getCircuitStatus($circuit);
        switch ($status['statusId']) {
            case EmsSmartRelayTransactionLog::STAT_ON:
                $equipmentStatus = 'Charging';
                $switchStatus = 'On';
                break;
            case EmsSmartRelayTransactionLog::STAT_STANDBY:
                $equipmentStatus = 'Standby';
                $switchStatus = 'On';
                break;
            case EmsSmartRelayTransactionLog::STAT_OFF:
                $equipmentStatus = 'Off';
                $switchStatus = 'Off';
                break;
        }
        $circuit->setEquipment($equipmentStatus);
        $circuit->setPowerSwitch($switchStatus);
        if ( $station = $circuit->getChargingStation()[0]) {
            $vipNum = $station->getPriorityCharging();
            $vip = 'off';
            if ( $vipNum == 1 ) {
                $vip = 'on';
            }
            $circuit->setVip($vip);
        }
        return ;
    }

    public function findMainCircuitByLocation($location)
    {
        $repository = $this->getDoctrine()->getManager()
            ->getRepository('AppBundle:EmsCircuitMaster');

        $qb = $repository->createQueryBuilder('c');
        $qb->innerJoin('AppBundle:EmsMeterCircuitMapping', 'mc', 'WITH', 'mc.circuit = c.circuitId')               
            ->innerJoin('AppBundle:EmsGatewayDeviceMapping', 'gm', 'WITH', 'gm.deviceId = mc.device')
            ->innerJoin('AppBundle:EmsLocationGatewayMapping', 'lm', 'WITH', 'lm.gatewayId = gm.gatewayId')
            ->where('lm.locationId = :location')
            ->andWhere('c.circuitCategory = :mainMeterCategory')
            ->setParameter('location', $location->getLocationId(), 'uuid')
            ->setParameter('mainMeterCategory', self::MAIN_METER_CATEGORY);
        $result = $qb->getQuery()->getResult(); 
        return $result;
    }

    public const MAIN_METER_CATEGORY=60000;
    public function listCircuitsAction(Request $request, EmsLocationMaster $location)
    {
        $this->denyAccessUnlessGranted('view', $location);
        $view = View::create();
        $context = (new Context())->addGroup('list');

        $status = $request->get('status');
        $detail = $request->get('detail');
        $switch = $request->get('switch');
        $station = $request->get('station');
        $main = $request->get('main');

        if ($detail == 'true') {
            $context->addGroup('circuit');
        }
        if ($status == 'true') {
            $context->addGroup('status');
        }

        $repository = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:EmsCircuitMaster');

        // find main circuit(meter)
        if ( $main == true ) {
            $result = $this->findMainCircuitByLocation($location);
        } else {
            $qb = $repository->createQueryBuilder('cc');
            $qb->join('cc.meter', 'm')
                ->join('m.device', 'p')
                ->join('p.device', 'd')
                ->innerJoin('AppBundle:EmsGatewayDeviceMapping', 'gm', 'WITH', 'gm.deviceId = d.deviceId')
                ->innerJoin('AppBundle:EmsLocationGatewayMapping', 'lm', 'WITH', 'lm.gatewayId = gm.gatewayId')
                ->where('lm.locationId = :location')
                ->setParameter('location', $location->getLocationId(), 'uuid');
            if ( $switch == 'true' ) {
                $qb->andWhere('p.meterType in(2, 4)');
            } else if ($station == 'true') {
                $qb->andWhere('cc.evChargingStation = 1');
            }
            $qb->orderBy('cc.circuitName', 'ASC');
            $result = $qb->getQuery()->getResult();
            if (! $result || count($result) == 0) {
                $view->setData(array(
                            'code' => Response::HTTP_NOT_FOUND,
                            'msg' => 'no data'
                            ));
                return $view;
            }
            if ($status == 'true' || $switch == 'true' || $station == 'true') {
                $context->addGroup('switch');
                foreach ($result as $circuit) {
                    $this->getDetail($circuit, $status, $switch, null);
                }
            }

        }
        $view->setContext($context);
        $view->setData(array(
                    'code' => 200,
                    'data' => $result
                    ));
        return $view;
    }



    // power report related
    public function demandAction(Request $request, EmsLocationMaster $location)
    {
        $this->denyAccessUnlessGranted('view', $location);
        $from = $request->get('from');
        $to = $request->get('to');
        $limit = $request->get('limit');
        $offset = $request->get('offset');

        $requestParam = new ApiParameters();
        $requestParam->setFrom($from)
            ->setTo($to)
            ->setOffset($offset)
            ->setLimit($limit);

        $validator = $this->get('validator');
        $errors = $validator->validate($requestParam, null, array(
                    'location',
                    'timePeriod'
                    ));
        if (count($errors) > 0) {
            return $this->returnJsonResponse($request, array(
                        "code" => JsonResponse::HTTP_BAD_REQUEST,
                        "message" => $errors[0]->getMessage()
                        ));
        }

        if ($limit == null) {
            $limit = 0;
        }

        if ($limit == null) {
            $offset = 0;
        }

        $startDate = (new \DateTime())->setTimestamp(strtotime($from));
        $endDate = (new \DateTime())->setTimestamp(strtotime($to));
        if ($startDate == null || $endDate == null) {
            return $this->returnJsonResponse($request, array(
                        'code' => JsonResponse::HTTP_BAD_REQUEST
                        ));
        }

        $repository = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:EmsPowerReportLog');

        $result = $repository->findCurrDmdKwByLocation(array(
                    'startDate' => $startDate,
                    'endDate' => $endDate,
                    'location' => $location,
                    'interval' => $this::demandInterval
                    ), array(
                        'reportDatetime' => 'ASC'
                        ), $limit, $offset);

        $count = count($result);
        $dmdData = array();

        for ($i = 0; $i < $count; $i ++) {
            $data = array();
            $demand = $result[$i]['currDmdKw'];
            if ($demand == null) {
                continue;
            }
            $reportDatetime = $result[$i]['reportDatetime'];
            $data['time'] = $reportDatetime->format('Y-m-d H:i:s');
            $data['demand'] = $demand;
            $dmdData[] = $data;
        }

        return $this->returnJsonResponse($request, array(
                    "code" => JsonResponse::HTTP_OK,
                    "data" => $dmdData
                    ));
    }

    public function maxDemandAction(Request $request, EmsLocationMaster $location)
    {
        $this->denyAccessUnlessGranted('view', $location);
        $from = $request->get('from');
        $to = $request->get('to');

        $requestParam = new ApiParameters();
        $requestParam->setFrom($from)->setTo($to);

        $validator = $this->get('validator');
        $errors = $validator->validate($requestParam, null, array(
                    'location',
                    'simpleTimePeriod'
                    ));
        if (count($errors) > 0) {
            return $this->returnJsonResponse($request, array(
                        "code" => JsonResponse::HTTP_BAD_REQUEST,
                        "message" => $errors[0]->getMessage()
                        ));
        }

        $startDate = \DateTime::createFromFormat('Y-m-d H:i:s', $from);
        $endDate = \DateTime::createFromFormat('Y-m-d H:i:s', $to);

        $repository = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:EmsPowerReportLog');

        $result = $repository->findMaxDmdByLocation(array(
                    "startDate" => $startDate,
                    "endDate" => $endDate,
                    "location" => $location
                    ));

        if (count($result) == 0) {
            return $this->returnJsonResponse($request, array(
                        'code' => JsonResponse::HTTP_NOT_FOUND,
                        'message' => 'No Data'
                        ));
        }
        $report = $result[0];
        $report["time"] = $report["reportDatetime"]->format('Y-m-d H:i:s');
        unset($report["reportDatetime"]);
        return $this->returnJsonResponse($request, array(
                    'code' => JsonResponse::HTTP_OK,
                    'data' => $report
                    ));
    }

    public function currentDemandAction(Request $request, EmsLocationMaster $location)
    {
        $this->denyAccessUnlessGranted('view', $location);

        $repository = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:EmsPowerReportLog');

        $result = $repository->findNewestDataByLocation(array(
                    "reportDatetime",
                    "currDmdKw",
                    "currLgstdmdKw",
                    "touALgstdmdKw",
                    "touBLgstdmdKw",
                    "touCLgstdmdKw",
                    "touDLgstdmdKw"
                    ), array(
                        "location" => $location
                        ));

        if (count($result) == 0 || $result[0] == null) {
            return $this->returnJsonResponse($request, array(
                        'code' => JsonResponse::HTTP_NOT_FOUND,
                        'message' => 'No Data'
                        ));
        }
        $report = $result[0];
        $report["time"] = $report["reportDatetime"]->format('Y-m-d H:i:s');
        unset($report["reportDatetime"]);

        $endDate = new \DateTime();
        $startToday = (new \DateTime())->setTime(0, 0, 0);
        $startThisMonth = (\DateTime::createFromFormat('Y-m-d', date('Y-m-01')))->setTime(0, 0, 0);

        $maxToday = $repository->findMaxDmdByLocation(array(
                    "location" => $location,
                    "startDate" => $startToday,
                    "endDate" => $endDate
                    ));
        $maxThisMonth = $repository->findMaxDmdByLocation(array(
                    "location" => $location,
                    "startDate" => $startThisMonth,
                    "endDate" => $endDate
                    ));

        if (count($result) == 0 || $result[0] == null) {
            return $this->returnJsonResponse($request, array(
                        'code' => JsonResponse::HTTP_NOT_FOUND,
                        'message' => 'No Data'
                        ));
        }

        if (count($maxToday) != 0) {
            $maxToday[0]["time"] = $maxToday[0]["reportDatetime"]->format('Y-m-d H:i:s');
            unset($maxToday[0]["reportDatetime"]);
            $report["max_today"] = $maxToday[0];
        } else {
            $report["max_today"] = null;
        }

        if (count($maxThisMonth) != 0) {
            $maxThisMonth[0]["time"] = $maxThisMonth[0]["reportDatetime"]->format('Y-m-d H:i:s');
            unset($maxThisMonth[0]["reportDatetime"]);
            $report["max_this_month"] = $maxThisMonth[0];
        } else {
            $report["max_this_month"] = null;
        }

        return $this->returnJsonResponse($request, array(
                    "code" => JsonResponse::HTTP_OK,
                    "data" => $report
                    ));
    }
/*
    public function currentCurrentAction(Request $request, EmsLocationMaster $location)
    {
        $this->denyAccessUnlessGranted('view', $location);

        $this->denyAccessUnlessGranted('view', array(
                    'locationId' => $location
                    ));

        $repository = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:EmsPowerReportLog');

        $result = $repository->findNewestDataByLocation(array(
                    "aA",
                    "aB",
                    "aC",
                    "vA", 
                    "vB",
                    "vC",
                    "aAverage"
                    ), array(
                        "location" => $location
                        ));

        if (count($result) == 0 || $result[0] == null) {
            return $this->returnJsonResponse($request, array(
                        'code' => JsonResponse::HTTP_NOT_FOUND,
                        'message' => 'No Data'
                        ));
        }
        $report = $result[0];
        $report["time"] = $report["reportDatetime"]->format('Y-m-d H:i:s');
        unset($report["reportDatetime"]);

        if ($report['aA'] == null)
            $report['aA'] = 0;
        if ($report['aB'] == null)
            $report['aB'] = 0;
        if ($report['aC'] == null)
            $report['aC'] = 0;
        if ($report['vA'] == null)
            $report['vA'] = 0;
        if ($report['vB'] == null)
            $report['vB'] = 0;
        if ($report['vC'] == null)
            $report['vC'] = 0;
        $report['aSummary'] = $report['aA'] + $report['aB'] + $report['aC'];
        if ($report['aAverage'] == null)
            $report['aAverage'] = $report['aSummary'] / 3;

        // max
        $endDate = new \DateTime();
        $startToday = (new \DateTime())->setTime(0, 0, 0);
        $startThisMonth = (\DateTime::createFromFormat('Y-m-d', date('Y-m-01')))->setTime(0, 0, 0);

        $maxToday = $repository->findMaxCurrByLocation(array(
                    "location" => $location,
                    "startDate" => $startToday,
                    "endDate" => $endDate
                    ));
        $maxThisMonth = $repository->findMaxCurrByLocation(array(
                    "location" => $location,
                    "startDate" => $startThisMonth,
                    "endDate" => $endDate
                    ));

        if (count($maxToday) != 0) {
            $maxToday[0]["time"] = $maxToday[0]["reportDatetime"]->format('Y-m-d H:i:s');
            unset($maxToday[0]["reportDatetime"]);
            $report["max_today"] = $maxToday[0];
        } else {
            $report["max_today"] = null;
        }

        if (count($maxThisMonth) != 0) {
            $maxThisMonth[0]["time"] = $maxThisMonth[0]["reportDatetime"]->format('Y-m-d H:i:s');
            unset($maxThisMonth[0]["reportDatetime"]);
            $report["max_this_month"] = $maxThisMonth[0];
        } else {
            $report["max_this_month"] = null;
        }

        return $this->returnJsonResponse($request, array(
                    "code" => JsonResponse::HTTP_OK,
                    "data" => $report
                    ));
    }

    public function maxCurrentAction(Request $request, EmsLocationMaster $location)
    {
        $this->denyAccessUnlessGranted('view', $location);

        $from = $request->get('from');
        $to = $request->get('to');

        $requestParam = new ApiParameters();
        $requestParam->setLocation($location)
            ->setFrom($from)
            ->setTo($to);

        $validator = $this->get('validator');
        $errors = $validator->validate($requestParam, null, array(
                    'location',
                    'simpleTimePeriod'
                    ));
        if (count($errors) > 0) {
            return $this->returnJsonResponse($request, array(
                        "code" => JsonResponse::HTTP_BAD_REQUEST,
                        "message" => $errors[0]->getMessage()
                        ));
        }

        $startDate = \DateTime::createFromFormat('Y-m-d H:i:s', $from);
        $endDate = \DateTime::createFromFormat('Y-m-d H:i:s', $to);

        $repository = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:EmsPowerReportLog');

        $result = $repository->findMaxCurrByLocation(array(
                    "startDate" => $startDate,
                    "endDate" => $endDate,
                    "location" => $location
                    ));

        if (count($result) == 0) {
            return $this->returnJsonResponse($request, array(
                        'code' => JsonResponse::HTTP_NOT_FOUND,
                        'message' => 'No Data'
                        ));
        }
        $report = $result[0];
        $report["time"] = $report["reportDatetime"]->format('Y-m-d H:i:s');
        unset($report["reportDatetime"]);
        return $this->returnJsonResponse($request, array(
                    'code' => JsonResponse::HTTP_OK,
                    'data' => $report
                    ));
    }
*/
    public function kwhAction(Request $request, EmsLocationMaster $location)
    {
        $this->denyAccessUnlessGranted('view', $location);

        $from = $request->get('from');
        $to = $request->get('to');

        $requestParam = new ApiParameters();
        $requestParam->setLocation($location)
            ->setFrom($from)
            ->setTo($to);

        $validator = $this->get('validator');
        $errors = $validator->validate($requestParam, null, array(
                    'location',
                    'simpleTimePeriod'
                    ));
        if (count($errors) > 0) {
            return $this->returnJsonResponse($request, array(
                        "code" => JsonResponse::HTTP_BAD_REQUEST,
                        "message" => $errors[0]->getMessage()
                        ));
        }

        $startDate = (new \DateTime())->setTimestamp(strtotime($from));
        $endDate = (new \DateTime())->setTimestamp(strtotime($to));

        $repository = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:EmsPowerReportLog');

        $result = $repository->findkWhByLocation(array(
                    "location" => $location,
                    "startDate" => $startDate,
                    "endDate" => $endDate
                    ));

        return $this->returnJsonResponse($request, array(
                    "code" => JsonResponse::HTTP_OK,
                    "data" => array(
                        "kWh" => $result,
                        "startDate" => $from,
                        "endDate" => $to
                        )
                    ));
    }

    public function readEnergyPackageMetaAction(Request $request, EmsLocationMaster $location)
    {
        /*
           $RAW_QUERY = 'SELECT * FROM esp_energy_package_detail ';

           $statement = $em->getConnection()->prepare($RAW_QUERY);
           $statement->execute();

           $package_details = $statement->fetchAll();


           $test_message='';
           foreach($package_details as $package_detail){
           $test_message.=$package_detail['has_contract'];
           }
           return $this->returnJsonResponse($request, array(
           'code' => JsonResponse::HTTP_OK,
           'msg' =>$test_message
           ));



         */


        $em = $this->getDoctrine()->getManager();



        $the_esp_customer_master = $location->getEspCustomer()[0];
        $esp_customer_rate_repo = $em->getRepository('EspBundle:EspCustomerRate');
        $the_esp_customer_rate = $esp_customer_rate_repo
                        ->findOneBy(['espCustomer' => $the_esp_customer_master]);

        $ev_location_charging_management_repo = $em->getRepository('EvBundle:EvLocationChargingManagement');
        $the_ev_location_charging_management=$ev_location_charging_management_repo->findOneByEmsLocationId($location->getLocationId());
        $data=array();
        if($the_ev_location_charging_management==null){
            $data['carowner_share_regular_kw']=0;
            $data['carowner_share_saturday_semipeak_kw']=0;
            $data['carowner_share_offpeak_kw']=0;
        }else{
            $data['carowner_share_regular_kw']=$the_ev_location_charging_management->getEvNormalContract();
            $data['carowner_share_saturday_semipeak_kw']=$the_ev_location_charging_management->getEvSaturdaySemiContract();
            $data['carowner_share_offpeak_kw']=$the_ev_location_charging_management->getEvOffPeakContract();
        }


        $espRepo = $em->getRepository('EspBundle:EspEnergySp');
        $sp = $espRepo->findOneByEnergySpName('taipower');
        if ( !$sp ) {
            return $this->returnJsonResponse(
                    $request, array(
                        'code' => JsonResponse::HTTP_OK,
                        'msg' =>"Energy Service Provider Not Found"
                        )
                    );
        }
        $packageRepo = $em->getRepository('EspBundle:EspEnergyPackage');
        $packageDetailRepo = $em->getRepository('EspBundle:EspEnergyPackageDetail');
        $packages = $packageRepo->findByEnergySp($sp);
        $packages_to_array=array();
        foreach($packages as $package){
            $RAW_QUERY = 'SELECT * FROM esp_energy_package_detail WHERE energy_package_id=? AND status_id=1  ';
            $statement = $em->getConnection()->prepare($RAW_QUERY);
            $statement->execute(array($package->getEnergyPackageId()));
            $package_detail = $statement->fetchAll()[0];

            $packages_to_array[]=array(
                    'id'=>$package->getEnergyPackageId(),
                    'has_contract'=>$package_detail['has_contract']
                    );
        }

        // get voltages
        $voltageRepo = $em->getRepository('AppBundle:EmsVoltage');
        $voltages = $voltageRepo->findAll();
        $voltages_to_array=array();
        foreach($voltages as $voltage){
            $voltages_to_array[]=array(
                    'voltageId'=>$voltage->getVoltageId(),
                    'voltageName'=>$voltage->getVoltageName()
                    );
        }


        $data['voltage']=$voltages_to_array;
        $data['energyPackage']=$packages_to_array;

        return $this->returnJsonResponse($request, array(
                    'code' => JsonResponse::HTTP_OK,
                    'data' => $data
                    ));
    }

    public function readEnergyPackageAction(Request $request, EmsLocationMaster $location)
    {
        $em = $this->getDoctrine()->getManager();	

        $data = [];
        $espCustomers = $location->getEspCustomer();
        if ( $espCustomers && count($espCustomers) > 0 ) {

            $espCustomer = $location->getEspCustomer()[0];
            $espCustomerRate_repo = $em->getRepository('EspBundle:EspCustomerRate');
            $espCustomerRate = $espCustomerRate_repo->findOneBy(array(
                    'espCustomer' => $espCustomer,
                    'statusId' => 1
                    ));
            $meterId = $espCustomer->getPowerMeterDeviceId();
            $emsPowerMeter_Repo = $em->getRepository('AppBundle:EmsPowerMeter');
            $emsPowerMeter = $emsPowerMeter_Repo->find($meterId);

            $data = array(
                'customerNumber'=>$espCustomer->getEspCustomerNumber(),
                'normalContract'=>$espCustomerRate->getNormalContract(),
                'semiPeakContract'=>$espCustomerRate->getSemiPeakContract(),
                'saturdaySemiPeakContract'=>$espCustomerRate->getSaturdaySemiPeakContract(),
                'offPeakContract'=>$espCustomerRate->getOffPeakContract(),
                'energyPackage'=>$espCustomerRate->getEnergyPackage()->getEnergyPackageId(),
                'voltage'=>$emsPowerMeter->getVoltage()->getVoltageId()
                );
        }
        return $this->returnJsonResponse($request, array(
                'code' => JsonResponse::HTTP_OK,
                'data' => $data
            ));
    }

    public function getEnergyPackageAction(Request $request, EmsLocationMaster $location)
    {
        $em = $this->getDoctrine()->getManager();	
        $espCustomers = $location->getEspCustomer();
        $data = array();

        $espFee = $this->get('esp.fee');
        $esp = $this->get('esp.tpc');

        foreach ($espCustomers as $customer) {
            $res = $espFee->getCustomerRateDetail($customer);
            $res['customerNumber'] = $customer->getEspCustomerNumber();
            $res['currentRate'] = $esp->getRate($res['energyPackage'], new \DateTime());
            $data[] = $res;
        }
        return $this->returnJsonResponse($request, array(
                    'code' => JsonResponse::HTTP_OK,
                    'data' => $data
                    ));
    }

    public function setEnergyPackageAction(Request $request, EmsLocationMaster $location)
    {
        $em = $this->getDoctrine()->getManager();
        $legal_input_names=array(
                'customerNumber', 'voltage','energyPackage','contract_a','contract_b','contract_c','contract_d'
                );
        $input_data=$request->request->all();
        /*
        $input_rules=array();
        foreach($input_data as $the_input_name=>$the_input_value){
            if(in_array($the_input_name,$legal_input_names)){
                $input_rules[$the_input_name]=array(
                        new NotBlank()
                        );
            }
        }			
        $validator = Validation::createValidator();
        $errors = $validator->validate($input_data, new Collection(
                    $input_rules
                    ));

        
        if (count($errors)>0) {
            $errors_message='';
            foreach($errors as $error){
                $errors_message.=$error->getPropertyPath().' '.$error->getMessage()."\n";
            }
            return $this->returnJsonResponse($request, array(
                        'code' => JsonResponse::HTTP_OK,
                        'msg' =>$errors_message
                        ));
        }
        */
        $espCustomers = $location->getEspCustomer();
        if ( $espCustomers && count($espCustomers) > 0 ) {
            $espCustomer = $espCustomers[0];
            $meterId = $espCustomer->getPowerMeterDeviceId();
        } else {
            $espCustomer = new EspCustomerMaster();
            $espCustomer->setBillingAddress($location->getLocationAddress())
                ->setEspCustomerName($location->getLocationName());
            $location->addEspCustomer($espCustomer);
            $circuits = $this->findMainCircuitByLocation($location);
            if ( $circuits && count($circuits) > 0 ) { 
                $device = $circuits[0]->getDevice();
                if ( $device && count($device) > 0 ){
                    $meterId = $device[0]->getDeviceId();
                    $espCustomer->setPowerMeterDeviceId($meterId);
                }
            }
        }
        $powerMeter = $em->getRepository('AppBundle:EmsPowerMeter')->find($meterId);

        if ( isset( $input_data['customerNumber'])) {
            $espCustomer->setEspCustomerNumber($input_data['customerNumber']);
        }

        if ( isset( $input_data['voltage'])) {
            $ems_voltage_repo = $em->getRepository('AppBundle:EmsVoltage');
            $the_ems_voltage=$ems_voltage_repo->find($input_data['voltage']);
            $powerMeter->setVoltage($the_ems_voltage);
        }

        $espCustomerRate = $em->getRepository('EspBundle:EspCustomerRate')
            ->findOneBy(['espCustomer' => $espCustomer]);
        if ( $espCustomerRate == null ) {
            $espCustomerRate = new EspCustomerRate();
            $espCustomerRate->setEspCustomer($espCustomer)
                ->setEspCustomerName($espCustomer->getEspCustomerName())
                ->setEnableDate(new \DateTime())
                ->setStatusId(1);
        }

        if ( isset( $input_data['energyPackage'])) {
            $energyPackage = $em->getRepository('EspBundle:EspEnergyPackage')
                ->find($input_data['energyPackage']);
            $espCustomerRate->setEnergyPackage($energyPackage);
        }

        if(array_key_exists('contract_a',$input_data)){
            $espCustomerRate->setNormalContract($input_data['contract_a']);
        }

        if(array_key_exists('contract_b',$input_data)){
            $espCustomerRate->setSaturdaySemiPeakContract($input_data['contract_b']);
        }

        if(array_key_exists('contract_c',$input_data)){
            $espCustomerRate->setOffPeakContract($input_data['contract_c']);		
        }

        if(array_key_exists('contract_d',$input_data)){
            $espCustomerRate->setSemiPeakContract($input_data['contract_d']);
        }

        try {
            $em->persist($espCustomer);
            $em->persist($powerMeter);
            $em->persist($espCustomerRate);
            $em->persist($location);
            $em->flush();
        } catch (Exception $ex) {
            return $this->returnJsonResponse($request, array(
                        'code' => JsonResponse::HTTP_BAD_REQUEST,
                        'msg' => $ex->getMessage()
                        // 'msg' => 'Update contact failed'
                        ));
        }
        return $this->returnJsonResponse($request, array(
                    'code' => JsonResponse::HTTP_OK
                    ));
    }


    public function getContactAction(Request $request, EmsLocationMaster $location)
    {
        $ret = array();
        $customer = $location->getValidCustomer();
        if ($customer) {
            $ret['email'] = $customer->getCustomerEmail();
            $ret['phone'] = $customer->getCustomerContactPhone();
            $ret['name'] = $customer->getContactName();
        }
        $ret['address'] = $location->getLocationAddress();
        $user = $this->getUser();

        return $this->returnJsonResponse($request, array(
                    'code' => JsonResponse::HTTP_OK,
                    'data' => $ret
                    ));
    }

    public function setContactAction(Request $request, EmsLocationMaster $location)
    {
        $validator = Validation::createValidator();
        $errors = $validator->validate($request->request->all(), new Collection(array(
                        'email' => [
                        new NotNull(),
                        new Length(array(
                                'max' => 50
                                ))
                        ],
                        'phone' => [
                        new NotNull(),
                        new Length(array(
                                'max' => 50
                                ))
                        ],
                        'name' => [
                        new NotNull(),
                        new Length(array(
                                'max' => 50
                                ))
                        ],
                        'address' => [
                        new NotNull(),
                        new Length(array(
                                    'max' => 255
                                    ))
                        ]
                        )));

                        if (isset($errors[0])) {
                            return $this->returnJsonResponse($request, array(
                                        'code' => JsonResponse::HTTP_OK,
                                        'msg' => $errors->get(0)
                                        ->getPropertyPath() . ' ' . $errors->get(0)
                                        ->getMessage()
                                        ));
                        }
                        $email = $request->get('email');
                        $phone = $request->get('phone');
                        $name = $request->get('name');
                        $address = $request->get('address');

                        $location->setLocationAddress($address);
                        $customer = $location->getValidCustomer();
                        $customer->setCustomerEmail($email);
                        $customer->setCustomerContactPhone($phone);
                        $customer->setContactName($name);

                        $em = $this->getDoctrine()->getManager();
                        try {
                            $em->persist($customer);
                            $em->persist($location);
                            $em->flush();
                        } catch (Exception $ex) {
                            return $this->returnJsonResponse($request, array(
                                        'code' => JsonResponse::HTTP_BAD_REQUEST,
                                        'msg' => 'Update contact failed'
                                        ));
                        }
                        return $this->returnJsonResponse($request, array(
                                    'code' => JsonResponse::HTTP_OK
                                    ));
    }

    public function getEnergyFeeAction(Request $request, EmsLocationMaster $location)
    {
        $this->denyAccessUnlessGranted('view', $location);

        $from = $request->get('from');
        $to = $request->get('to');

        $circuits = $this->findMainCircuitByLocation($location);
        if ( $circuits && count($circuits) != 0 ) {
            $data = [];
            foreach($circuits as $circuit) {
                $circuitId = $circuit->getCircuitId();
                $tpc = $this->get('esp.fee');
                $oneData = $tpc->deviceFee($from, $to, $circuitId);
                $data[] = $oneData;
            }

            return new JsonResponse(array(
                'code' => Response::HTTP_OK,
                'data' => $data,
            ), Response::HTTP_OK);
        }
        return new JsonResponse(array(
            'code' => Response::HTTP_BAD_REQUEST,
            'message' => 'no main circuit for the location'
        ), Response::HTTP_BAD_REQUEST);

        /*
        $requestParam = new ApiParameters();
        $requestParam->setLocation($location)
            ->setFrom($from)
            ->setTo($to);

        $validator = $this->get('validator');
        $errors = $validator->validate($requestParam, null, array(
                    'location',
                    'simpleTimePeriod'
                    ));
        if (count($errors) > 0) {
            return $this->returnJsonResponse($request, array(
                        "code" => JsonResponse::HTTP_BAD_REQUEST,
                        "message" => $errors[0]->getMessage()
                        ));
        }

        if ($from != null) {
            $startDate = (new \DateTime())->setTimestamp(strtotime($from));
        } else {
            $startDate = (new \DateTime('first day of this month'))->setTime(0, 0, 0);
        }

        if ($to != null) {
            $endDate = (new \DateTime())->setTimestamp(strtotime($to));
        } else {
            $endDate = new \DateTime();
        }

        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('AppBundle:EmsDeviceMaster');
        $result = $repository->findByLocation($location, 100);
        $powerReport = $this->get('app.power_report');
        $espFee = $this->get('esp.fee');
        $data = array();

        $customers = $location->getEspCustomer();
        foreach($customers as $customer) {
            $deviceId = $customer->getPowerMeterDeviceId();
            $circuit = null;
            if ( $deviceId ) {
                $device = $repository->find($deviceId);
                if ( $device ) {
                    $circuits = $device->getCircuit();
                    if (count($circuits) != 0) {
                        $circuit = $circuits->get(0);
                    }
                }
            }
            if ( $circuit ) {
                $fee = $powerReport->getCircuitFee($circuit->getCircuitId(), $startDate, $endDate, null);
                // $fee = $espFee->deviceFee($startDate, $endDate, $circuit->getCircuitId(), $customer);

                if ($fee) {
                    $fee['customerNumber'] = $customer->getEspCustomerNumber();
                    $log = $em->getRepository('AppBundle:EmsPowerReportLog')->findMaxDmdByCircuit(array(
                                'startDate' => $startDate,
                                'endDate' => $endDate,
                                'circuit' => $circuit
                                ));
                    if ($log) {
                        $fee['fee']['normal']['usage'] = $log[0]['currDmdKw'];
                    }
                    $data[] = $fee;
                }
            } 
        }
        return $this->returnJsonResponse($request, array(
                    'code' => JsonResponse::HTTP_OK,
                    'data' => $data
                    ));
        */
    }

    public function getChargingStationTOUStatisticsAction(Request $request, EmsLocationMaster $location)
    {
        $this->denyAccessUnlessGranted('view', $location);

        $view = View::create();
        $yearMonth = $request->get('yearMonth');

        $mainMeter = $this->findMainCircuitByLocation($location);
        if ( $mainMeter && count($mainMeter) != 0 ) {
            $tree = $this->get('app.circuit_tree');
            $stations = $tree->getChargingStations($mainMeter[0]->getCircuitId());
            $circuitIds = [];
            foreach($stations as $station) {
                $circuitIds[] = $station->getCircuitId();
            }
            $statistics = $this->get('esp.statistics');
            $result = $statistics->getMohthlyFee($circuitIds, $yearMonth);
        } else {
            $result = [];
        }
        $view->setStatusCode('200');
        $view->setData([
                'code' => 200, 
                'data' => $result
        ]);
        return $view;
    }

    public function getChargingSessionBillAction(Request $request, EmsLocationMaster $location)
    {
        $view = View::create();
        $context = (new Context())->addGroup('list');

        $startDate = $request->get('from');
        $endDate = $request->get('to'); 

        $mainMeter = $this->findMainCircuitByLocation($location);
        if ( $mainMeter && count($mainMeter) != 0 ) {
            $tree = $this->get('app.circuit_tree');
            $stations = $tree->getChargingStations($mainMeter[0]->getCircuitId());
            $circuitIds = [];
            foreach($stations as $station) {
                $circuitIds[] = $station->getCircuitId();
            }

            $em = $this->getDoctrine()->getManager();
            $repo = $em->getRepository('EvBundle:EvChargingSessionLog');
            $qb = $repo->createQueryBuilder('q');
            $qb->where('q.endAt between :start and :end')
                ->andWhere('q.circuit in (:circuit)')
                ->setParameter('start', $startDate)
                ->setParameter('end', $endDate)
                ->setParameter('circuit', $circuitIds);
            $result = $qb->getQuery()->getResult();
        }
        $view->setContext($context);
        $view->setStatusCode(200);
        $view->setData([
                'code' => 200, 
                'data' => $result
        ]);
        return $view;
    }

    public function getChargingRevenueAction(Request $request, EmsLocationMaster $location )
    {

        $this->denyAccessUnlessGranted('view', $location);

        $view = View::create();
        $yearMonth = $request->get('yearMonth');

        // get cost
        $mainMeter = $this->findMainCircuitByLocation($location);
        if ( $mainMeter && count($mainMeter) != 0 ) {
            $tree = $this->get('app.circuit_tree');
            $stations = $tree->getChargingStations($mainMeter[0]->getCircuitId());
            $circuitIds = [];
            foreach($stations as $station) {
                $circuitIds[] = $station->getCircuitId();
            }
            $statistics = $this->get('esp.statistics');
            $result = $statistics->getMohthlyFee($circuitIds, $yearMonth);
        } else {
            $result = [];
        }

        // calculate income
        $cost = $result[0];
        if( isset($cost['normalUsage'])) unset($cost['normalUsage']);
        if( isset($cost['saturdaySemiPeakUsage'])) unset($cost['saturdaySemiPeakUsage']);
        if( isset($cost['offPeakUsage'])) unset($cost['offPeakUsage']);
        if( isset($cost['semiPeakUsage'])) unset($cost['semiPeakUsage']);
        $income = array();
        $revenue = array();
        foreach($cost as $key => $value ) {
            $income[$key] = (float) ($value) * 1.1;
            $revenue[$key] = $income[$key] - $value;
            $demand_share[$key] = 0;
        }
        $data = [
            'cost' => $cost,
            'income' => $income,
            'profit' => $revenue,
            'demand' => $demand_share
        ];
        $view->setStatusCode('200');
        $view->setData([
                'code' => 200, 
                'data' => $data
        ]);
        return $view;

    }

    public function forEms3ElectricityDeviceAction(Request $request, EmsLocationMaster $location)
    {
        $em = $this->getDoctrine()->getManager();
        $RAW_QUERY = 
            'SELECT e3_.circuit_id,e3_.circuit_max_current,e3_.circuit_desc FROM ems_location_gateway_mapping e0_ '.
            'INNER JOIN ems_gateway_device_mapping e1_ ON (e0_.location_id=? AND e1_.gateway_id = e0_.gateway_id) '.
            'INNER JOIN ems_meter_circuit_mapping e2_ ON (e2_.device_id=e1_.device_id ) '.
            'INNER JOIN ems_circuit_master e3_ ON (e3_.circuit_id=e2_.circuit_id ) '.
            'ORDER BY circuit_name ASC';
        $statement = $em->getConnection()->prepare($RAW_QUERY);
        $statement->execute(array($location->getLocationId()));
        $the_localtion_circuits = $statement->fetchAll();
        $return_data=array();
        $report = $this->get('app.power_report');
        $ems_power_report_log_repository = $em->getRepository('AppBundle:EmsPowerReportLog');
        $ems_circuit_master_repository = $em->getRepository('AppBundle:EmsCircuitMaster');
        foreach($the_localtion_circuits as $the_circuit){

            $the_circuit_model=$ems_circuit_master_repository->find($the_circuit['circuit_id']);
            $the_circuit_category_model=$the_circuit_model->getCircuitCategory();
            $currentStatus = $ems_power_report_log_repository->findCurrentStatusByCircuit($the_circuit['circuit_id']);
            if ($currentStatus != null) {
                $charge = $report->getCircuitFee($the_circuit_model, new \DateTime('first day of this month'), null, null);
                if ($charge) {
                    $currentStatus['fee'] = $charge['amount'];
                }
            }


            $return_data[]=array(
                    'id'=>$the_circuit['circuit_id'],
                    'circuitMaxCurrent'=>(int)$the_circuit['circuit_max_current'],
                    'circuitDesc'=>($the_circuit['circuit_desc']==null?'':$the_circuit['circuit_desc']),
                    'currentStatus'=>$currentStatus,
                    'circuitCategory'=>array(
                        'category1'=>$the_circuit_category_model->getCategory1(),
                        'category2'=>$the_circuit_category_model->getCategory2()
                        )
                    );


        }

        /*
           SELECT e0_.circuit_id AS circuit_id_0, e0_.circuit_name AS circuit_name_1, e0_.circuit_max_current AS circuit_max_current_2, e0_.circuit_desc AS circuit_desc_3, e0_.ev_charging_station AS ev_charging_station_4, e0_.circuit_standby_current AS circuit_standby_current_5, e0_.circuit_phase AS circuit_phase_6, e0_.parent_phase AS parent_phase_7, e0_.last_data_report AS last_data_report_8, e0_.newest_data AS newest_data_9, e0_.circuit_category_id AS circuit_category_id_10 FROM ems_circuit_master e0_ 
           INNER JOIN ems_meter_circuit_mapping e1_ ON e0_.circuit_id = e1_.circuit_id 
           INNER JOIN ems_power_meter e2_ ON e1_.device_id = e2_.device_id 
           INNER JOIN ems_device_master e3_ ON e2_.device_id = e3_.device_id 
           INNER JOIN ems_gateway_device_mapping e4_ ON (e4_.device_id = e3_.device_id) 
           INNER JOIN ems_location_gateway_mapping e5_ ON (e5_.gateway_id = e4_.gateway_id) 
           WHERE e5_.location_id = ? ORDER BY e0_.circuit_name ASC
         */
        return $this->returnJsonResponse($request, array(
                    'code' => JsonResponse::HTTP_OK,
                    'data' => $return_data
                    ));


    }
    }

