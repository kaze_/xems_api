<?php

namespace AppBundle\Controller;

use AppBundle\Entity\EmsPowerReportLog;
use AppBundle\Entity\EmsSensorReportLog;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * User controller.
 *
 */
class PowerReportController extends BaseApiController
{

    /**
     * Creates a new userMaster entity.
     */
    // see register.
    public function newAction(Request $request)
    {
        // permission set in security.yml
        // $this->denyAccessUnlessGranted('edit', $user);
        $view = View::create();
        $source = $request->get('source');
//        $context = (new Context())->addGroup('list');
	    $logger = $this->get('logger');
        //$powerReport = new EmsPowerReportLog();
        $em = $this->getDoctrine()->getManager();
        //$powerReport = new EmsPowerReportLog();
        $data = json_decode($request->getContent(), true);
        // $logger->error($request->getContent());
        foreach( $data as $key => $value ) {
            if ( is_array($value) ) {
                if ( isset($value["double"]) ) {
                    $data[$key] = $value["double"];
                } else if ( isset($value["int"] )) {
                    $data[$key] = $value["int"];
                }
            }
        }
	    //$logger->info(json_encode($data));
        $powerReport = new EmsPowerReportLog();
	    $form = $this->createForm('AppBundle\Form\EmsPowerReportLogType', $powerReport );
        $clearMissing = ($request->getMethod() == 'PATCH') ? false : true;
        $form->submit($data, $clearMissing);
        $children = $form->all();

        $retData = array();
        
        $hasData = false;
        foreach($children as $child ) {
            $name = $child->getName();
            if ( $name == 'reportTime' || $name == 'circuit' || $name == 'modbus_id') 
                continue;
            $value = $child->getData();
            if ( $value !== null ) {
                $hasData = true;
            }
        }
        
        if ( $hasData ) {
            if ( $form->isValid() ) {
                if ($source != null )
                    $powerReport->setSource($source);
    //            print_r($powerReport);
                $em->persist($powerReport);
                $em->flush();
                $view->setStatusCode(Response::HTTP_CREATED);
                $retData['power'] = $powerReport;
            } else {
                $view->setStatusCode(Response::HTTP_BAD_REQUEST);
                $view->setData($form->getErrors());
            }
        } 
        
        $sensorReport = new EmsSensorReportLog();
        $form2 = $this->createForm('AppBundle\Form\EmsSensorReportLogType', $sensorReport);
        $form2->submit($data, $clearMissing);
        
        $children2 = $form2->all();
        $hasData2 = false;
        foreach($children2 as $child ) {
            $name = $child->getName();
            if ( $name == 'reportTime' || $name == 'circuit' || $name == 'modbus_id')
                continue;
                $value = $child->getData();
                if ( $value !== null ) {
                    $hasData2 = true;
                }
        }
        if ( $hasData2 ) {
            if ( $form2->isValid() ) {
                if ($source != null )
                    $sensorReport->setSource($source);
                $em->persist($sensorReport);
                $em->flush();
    //            $view->setContext($context);
                $view->setStatusCode(Response::HTTP_CREATED);
                $retData['sensor'] = $sensorReport;
            } else {
                $view->setStatusCode(Response::HTTP_BAD_REQUEST);
                $view->setData($form->getErrors());
            }
        }
        
        if ( $hasData == false && $hasData2 == false) {
            $view->setStatusCode(Response::HTTP_NOT_MODIFIED);
        }
        $view->setData($retData);
        
        return $view;
    }

}
