<?php

namespace AppBundle\Controller;

use AppBundle\Entity\LoginToken;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\View\View;
use FOS\UserBundle\Controller\SecurityController as BaseController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Role\RoleHierarchyInterface;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\Role\RoleHierarchy;

class SecurityController extends BaseController
{

    public function basicRegisterAction(Request $request)
    {
        $userManager = $this->get('fos_user.user_manager');
        $entityManager = $this->get('doctrine')->getManager();
        $reqData = json_decode($request->getContent(), true);

        $email = isset($reqData['email']) ? $reqData['email'] : null;
        $password = isset($reqData['password']) ? $reqData['password'] : null;
        $username = isset($reqData['username']) ? $reqData['username'] : null;
        $customer_id = isset($reqData['customer_id']) ? $reqData['customer_id'] : null;

        if ( $email == null || $password == null || $username == null ) {
            return new JsonResponse(array(
                "code" => JsonResponse::HTTP_BAD_REQUEST,
            ), JsonResponse::HTTP_BAD_REQUEST);
        }

        $customer = $entityManager->find('AppBundle:EmsCustomerMaster', $customer_id);
        if ( !$customer ) {
            return new JsonResponse(array(
                'code' => JsonResponse::HTTP_NOT_FOUND,
                'msg' => 'Customer not found'
            ), JsonResponse::HTTP_NOT_FOUND);
        }
        $user = $userManager->createUser()
            ->setEmail($email)
            ->setPlainPassword($password)
            ->setEnabled(true)
            ->setUsername($username)
            ->setCustomer($customer)
            ->addRole('ROLE_CUSTOMER_USER');

        $userManager->updateUser($user);
        return new JsonResponse(array(
            'code' => JsonResponse::HTTP_CREATED
            ), JsonResponse::HTTP_CREATED
        );
    }

    public function basicLoginAction(Request $request)
    {
        $view = View::create();
        $context = (new Context())->addGroup('list');

        $reqData = json_decode($request->getContent(), true);
        $email = isset($reqData['email']) ? $reqData['email'] : null;
        $password = isset($reqData['password']) ? $reqData['password'] : null;
        $userManager = $this->container->get('fos_user.user_manager');
        if ( $email != null ) {
            $user = $userManager->findUserByEmail($reqData['email']);
        } else {
            return new JsonResponse(array(
                'code' => JsonResponse::HTTP_BAD_REQUEST,
            ), JsonResponse::HTTP_BAD_REQUEST );
        }

        if ( $user == null || $password == null ) {
            return new JsonResponse(array(
                'code' => JsonResponse::HTTP_UNAUTHORIZED
            ), JsonResponse::HTTP_UNAUTHORIZED);
        }

        if ( !$this->checkPassword($user, $password)) {
            return new JsonResponse(array(
                'code' => JsonResponse::HTTP_UNAUTHORIZED
            ), JsonResponse::HTTP_UNAUTHORIZED);
        }

        $token = $this->getToken($user);
        $view->setContext($context);
        return $this->getLoginView($user, $token, $view);
    }

    public function appRegisterAction(Request $request)
    {
        $userManager = $this->get('fos_user.user_manager');
        $entityManager = $this->get('doctrine')->getManager();
        $reqData = json_decode($request->getContent(), true);

        $phone = isset($reqData['phone']) ? $reqData['phone'] : null;

        if ( $phone == null ) {
            return new JsonResponse(array(
                "code" => JsonResponse::HTTP_BAD_REQUEST, 
            ), JsonResponse::HTTP_BAD_REQUEST);
        }

        $user = $userManager->createUser()
            ->setPhone($phone)
            ->addRole('ROLE_EV_USER');

        $userManager->updateUser($user);
        return new JsonResponse(array(
            'code' => JsonResponse::HTTP_CREATED
        ), JsonResponse::HTTP_CREATED
        );
    }

    public function appLoginAction(Request $request)
    {
        $view = View::create();
        $context = (new Context())->addGroup('list');

        $reqData = json_decode($request->getContent(), true);
        $phone = isset($reqData['phone']) ? $reqData['phone'] : null;
        $password = isset($reqData['password']) ? $reqData['password'] : null;
        $userManager = $this->container->get('fos_user.user_manager');

        $user = $userManager->findUserBy(array("phone" => $reqData['phone']));
        if ( $user == null ) {
            $user = $userManager->createUser()
                ->setPhone($phone)
                ->setUsername($phone)
                ->addRole('ROLE_EV_USER');
            $userManager->updateUser($user);
        }

        // if passowrd exists, login
        if ( $password != null ) {
            if ( ! $this->checkPassword($user)) {
                return new JsonResponse(array(
                    'code' => JsonResponse::HTTP_UNAUTHORIZED
                ), JsonResponse::HTTP_UNAUTHORIZED);
            } else {
                $token = $this->getToken($user);
                $view->setContext($context);
                return $this->getLoginView($user, $token, $view);
            }
        }
        // phone number only, use sms verification

    }

    public function apiLogoutAction(Request $request)
    {
        $authToken = $request->headers->get('X-XM-Auth-Token');
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $tokenRepo = $em->getRepository('AppBundle:LoginToken');
        $token = $tokenRepo->findOneBy(array(
            'user' => $user,
            'sessionKey' => $authToken
        ));
        if ( $token ) {
            $token->setIsEnabled(false);
            $em->persist($token);
            $em->flush();
        }
        $this->get('security.token_storage')->setToken(null);
        $request->getSession()->invalidate();


        return new JsonResponse(array(
            "code" => JsonResponse::HTTP_OK
        ), JsonResponse::HTTP_OK);
    }

    private function checkPassword($user, $password)
    {
        $encoderFactory = $this->get('security.encoder_factory');
        $encoder = $encoderFactory->getEncoder($user);
        return $encoder->isPasswordValid($user->getPassword(), $password, $user->getSalt());
    }

    private function getToken($user)
    {
        $em = $this->getDoctrine()->getManager();
        $tokenRepo = $em->getRepository('AppBundle:LoginToken');
        $token = $tokenRepo->findOneBy(array('user' => $user,));

        if (!$token) {
            $token = new LoginToken();
            $token->setUser($user);
        }

        $token->setSessionKey(bin2hex(openssl_random_pseudo_bytes(32)))
            ->setIsEnabled(true)
            ->setUpdated();

        $em->persist($token);
        $em->flush();
        return $token;
    }

    private function getActualRoles($user)
    {
        $userRoles = $user->getRoles();
        $actualRoles = array();
        $roleHierarchy = $this->get('security.role_hierarchy');
        foreach($userRoles as $role) {
            $reachableRoles = $roleHierarchy->getReachableRoles([new Role($role)]);
            //$reachableRoles = RoleHierarchy->getReachableRoles([new Role($role)]);
            $actualRoles = array_merge($actualRoles, $reachableRoles);
        }
        return array_unique($actualRoles);
    }

    private function getLoginView($user, $token, $view)
    {
        $view->setStatusCode(Response::HTTP_OK);
        $security = $this->get('app.security');
        $roles = $security->getActualRoles($user);
        // $roles = $this->getActualRoles($user);
        $view->setData(array('code' => Response::HTTP_OK,
            'data' => array(
                'token' => $token->getSessionKey(),
                'user' => $user,
                'roles' => $roles
            )));
        return $view;
    }

    public function googleLoginAction(Request $request)
    {
        $view = View::create();
        $context = (new Context())->addGroup('list');
        $userManager = $this->container->get('fos_user.user_manager');
        $idToken = $request->get('google_id');
        $sso_service = $this->get('app.single_sign_on');
        $login = $sso_service->verifyGoogleId($idToken);
        if ( $login == null ) {
            return new JsonResponse(array(
                'code' => JsonResponse::HTTP_UNAUTHORIZED
            ), JsonResponse::HTTP_UNAUTHORIZED);
        }
        $email = isset($login['email']) ? $login['email'] : null;
        if ( $email == null || $email == '') {
            return new JsonResponse(array(
                'code' => JsonResponse::HTTP_UNAUTHORIZED
            ), JsonResponse::HTTP_UNAUTHORIZED);           
        }
        $user = $userManager->findUserByEmail($email);
        $token = $this->getToken($user);
        $view->setContext($context);
        return $this->getLoginView($user, $token, $view);
    }
}
