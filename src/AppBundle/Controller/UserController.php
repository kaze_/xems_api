<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Context\Context;

/**
 * User controller.
 *
 */
class UserController extends BaseApiController
{
    /**
     * Lists all user entities.
     *
     */
    public function allAction(Request $request)
    {
        $this->denyAccessUnlessGranted('view', new User());
        $view = View::create();
        $context = (new Context())->addGroup('list');

        $repostory = $this->getDoctrine()->getManager()->getRepository('AppBundle:User');
        $users = $repostory->findAll();

        $view->setContext($context);
        $view->setStatusCode(200);
        $view->setData($users);
        return $view;
    }

    /**
     * Finds and displays a userMaster entity.
     *
     */
    public function getAction(User $user)
    {
        $this->denyAccessUnlessGranted('view', $user);
        $view = View::create();
        $context = (new Context())->addGroup('list');

        $view->setContext($context);
        $view->setStatusCode(200);
        $view->setData($user);
        return $view;
    }

    /**
     * Creates a new userMaster entity.
     */
    // see register.
    public function newAction(Request $request)
    {
        // permission set in security.yml
        $user = new User();
        $this->denyAccessUnlessGranted('edit', $user);

        return $this->processForm($request, $user);
    }

    /**
     * Displays a form to edit an existing userMaster entity.
     */
    public function editAction(Request $request, User $user)
    {
        $this->denyAccessUnlessGranted('edit', $user);
        /*        $userObj = $this->getDoctrine()->getManager()->getRepository('AppBundle:User')->find($user);
                if ( $userObj === null ) {
                    return View::create(null, 400);
                }*/
        return $this->processForm($request, $user);
    }

    private function processForm($request, $user)
    {
        $view = View::create();
        $context = (new Context())->addGroup('list');

        $em = $this->getDoctrine()->getManager();
        $statusCode = $em->contains($user) ? 200:201;

        $data = json_decode($request->getContent(), true);
        $form = $this->createForm('AppBundle\Form\UserType', $user );
        $clearMissing = ($request->getMethod() == 'PATCH') ? false : true;
        $form->submit($data, $clearMissing);

        if ( $form->isValid() ) {
            if ( $statusCode === 201 ) {
                $em->persist($user);
                $view->setLocation($this->generateUrl('xems_users_get', array('user' => $user->getId()->toString())));
            }
            $em->flush();
            $view->setContext($context);
            $view->setStatusCode($statusCode);
            $view->setData($user);
        } else {
            $view->setStatusCode(400);
            $view->setData($form->getErrors());
        }
        return $view;
    }

    
}
