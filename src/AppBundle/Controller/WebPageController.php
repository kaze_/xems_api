<?php
/**
 * Created by PhpStorm.
 * User: chiehyingpan
 * Date: 2017/6/22
 * Time: 上午5:07
 */

namespace AppBundle\Controller;

use FOS\UserBundle\Controller\SecurityController as BaseController;

class WebPageController extends BaseController
{
 /*   public function locationAction()
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $customerId = $user->getCustomerId();
        $repository = $this->getDoctrine()->getManager()
            ->getRepository('AppBundle:EmsCustomerLocationMapping');
        $qb = $repository->createQueryBuilder('cl');
        $qb->select('cl.locationId')
            ->innerJoin('AppBundle:LocationMaster', 'l', 'WITH', 'cl.locationId = l.locationId')
            ->addSelect('l.locationName')
            ->where('cl.customerId = :customer')
            ->setParameter('customer', $customerId, 'uuid_binary');

        $result = $qb->getQuery()->getResult();
        if ( count($result) == 0 ) {
            $data = array();
        } else {
            $csrfToken = $this->has('security.csrf.token_manager')
                ? $this->get('security.csrf.token_manager')->getToken('authenticate')->getValue() : null;
            $data = array(
                "locationId" => str_replace('-', '', $result[0]['locationId']),
                "locationName" => $result[0]['locationName'],
                "csrf_token" => $csrfToken
            );
        }

        return $this->render('@App/Home/location.html.twig', $data);
    }
 */
    public function locationAction()
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $customerId = $user->getCustomerId();

        $repository = $this->getDoctrine()->getManager()
            ->getRepository('AppBundle:EmsCustomerLocationMapping');
        $qb = $repository->createQueryBuilder('cl');
        $qb->select('cl.locationId')
            ->innerJoin('AppBundle:EmsLocationMaster', 'l', 'WITH', 'cl.locationId = l.locationId')
            ->addSelect('l.locationName')
            ->innerJoin('AppBundle:EmsLocationGatewayMapping', 'lg', 'WITH', 'cl.locationId = lg.locationId')
            ->innerJoin('AppBundle:EmsGatewayDeviceMapping', 'gd', 'WITH', 'lg.gatewayId = gd.gatewayId')
            ->innerJoin('AppBundle:EmsDeviceMaster', 'd', 'WITH', 'gd.deviceId = d.deviceId')
            ->join('d.deviceType', 'dt')
            ->addSelect('d.deviceId, d.deviceName')
            ->where('cl.customerId = :customer')
            ->andWhere('dt.deviceTypeId = 100')
            ->setParameter('customer', $customerId, 'uuid');

        $result = $qb->getQuery()->getResult();
        $tmpArray = array();
        $resultLen = count($result);
        for ( $i = 0; $i < $resultLen; $i++ ) {
            $locationId = $result[$i]['locationId'];
            if ( $locationId == null || $locationId == '') {
                continue;
            }
            $locationIdStr = $locationId->toString();
            if ( !isset($tmpArray[$locationIdStr])) {
                $tmpArray[$locationIdStr]['locationId'] = $locationIdStr;
                $tmpArray[$locationIdStr]['locationName'] = $result[$i]['locationName'];
            }
            $deviceId = $result[$i]['deviceId']->toString();
            if ( $deviceId == null || $deviceId == '' ) {
                continue;
            }
            $tmpArray[$locationIdStr]['devices'][] = array(
                "deviceId" => $deviceId,
                "deviceName" => $result[$i]['deviceName']
            );
        }
        $locationData = array_values($tmpArray);
        $csrfToken = $this->has('security.csrf.token_manager')
            ? $this->get('security.csrf.token_manager')->getToken('authenticate')->getValue() : null;
        $data = array(
            "locationData" => $locationData,
            "csrf_token" => $csrfToken
        );
        //print_r($data);
        return $this->render('@App/Home/location.html.twig', $data);
    }
}
