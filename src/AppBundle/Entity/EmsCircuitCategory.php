<?php

namespace AppBundle\Entity;

/**
 * EmsCircuitCategory
 */
class EmsCircuitCategory
{
    /**
     * @var integer
     */
    private $circuitCategoryId;

    /**
     * @var string
     */
    private $category1;

    /**
     * @var string
     */
    private $category2;


    /**
     * Get circuitCategoryId
     *
     * @return integer
     */
    public function getCircuitCategoryId()
    {
        return $this->circuitCategoryId;
    }

    /**
     * Set category1
     *
     * @param string $category1
     *
     * @return EmsCircuitCategory
     */
    public function setCategory1($category1)
    {
        $this->category1 = $category1;

        return $this;
    }

    /**
     * Get category1
     *
     * @return string
     */
    public function getCategory1()
    {
        return $this->category1;
    }

    /**
     * Set category2
     *
     * @param string $category2
     *
     * @return EmsCircuitCategory
     */
    public function setCategory2($category2)
    {
        $this->category2 = $category2;

        return $this;
    }

    /**
     * Get category2
     *
     * @return string
     */
    public function getCategory2()
    {
        return $this->category2;
    }
}

