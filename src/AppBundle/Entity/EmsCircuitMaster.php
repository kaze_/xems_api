<?php

namespace AppBundle\Entity;

/**
 * EmsCircuitMaster
 */
class EmsCircuitMaster
{
    /**
     * @var string
     */
    private $circuitId;

    /**
     * @var integer
     */
    private $circuitName;

    /**
     * @var integer
     */
    private $circuitMaxCurrent;

    /**
     * @var string
     */
    private $circuitDesc;

    /**
     * @var boolean
     */
    private $evChargingStation = '0';


    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $device;

    // not in the database
    private $currentStatus;
    private $powerSwitch;
    private $equipment;
    private $vip;
    public function setCurrentStatus($currnetStatus)
    {
        $this->currentStatus = $currnetStatus;
    }

    public function getCurrentStatus()
    {
        return $this->currentStatus;
    }

    public function setPowerSwitch($powerSwitch)
    {
        $this->powerSwitch = $powerSwitch;
    }

    public function getPowerSwitch()
    {
        return $this->powerSwitch;
    }

    public function setEquipment($equipment)
    {
        $this->equipment = $equipment;
    }

    public function getEquipment()
    {
        return $this->equipment;
    }

    public function setVip($vip)
    {
        $this->vip = $vip;
    }

    public function getVip()
    {
        return $this->vip;
    }

    //
    public function getCircuitLocation()
    {
        if ( $this->evChargingStation && $stations = $this->getChargingStation() ) {
            if ( !$stations->isEmpty()) {
                return array(
                    "floor" => $stations[0]->getAddrFloor(),
                    "number" => $stations[0]->getAddrNumber()
                );
            }
        }
        return null;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->device = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get circuitId
     *
     * @return string
     */
    public function getCircuitId()
    {
        return $this->circuitId;
    }

    /**
     * Set circuitName
     *
     * @param integer $circuitName
     *
     * @return EmsCircuitMaster
     */
    public function setCircuitName($circuitName)
    {
        $this->circuitName = $circuitName;

        return $this;
    }

    /**
     * Get circuitName
     *
     * @return integer
     */
    public function getCircuitName()
    {
        return $this->circuitName;
    }

    /**
     * Set circuitMaxCurrent
     *
     * @param integer $circuitMaxCurrent
     *
     * @return EmsCircuitMaster
     */
    public function setCircuitMaxCurrent($circuitMaxCurrent)
    {
        $this->circuitMaxCurrent = $circuitMaxCurrent;

        return $this;
    }

    /**
     * Get circuitMaxCurrent
     *
     * @return integer
     */
    public function getCircuitMaxCurrent()
    {
        return $this->circuitMaxCurrent;
    }

    /**
     * Set circuitDesc
     *
     * @param string $circuitDesc
     *
     * @return EmsCircuitMaster
     */
    public function setCircuitDesc($circuitDesc)
    {
        $this->circuitDesc = $circuitDesc;

        return $this;
    }

    /**
     * Get circuitDesc
     *
     * @return string
     */
    public function getCircuitDesc()
    {
        return $this->circuitDesc;
    }

    /**
     * Set evChargingStation
     *
     * @param boolean $evChargingStation
     *
     * @return EmsCircuitMaster
     */
    public function setEvChargingStation($evChargingStation)
    {
        $this->evChargingStation = $evChargingStation;

        return $this;
    }

    /**
     * Get evChargingStation
     *
     * @return boolean
     */
    public function getEvChargingStation()
    {
        return $this->evChargingStation;
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $meter;

    /**
     * Add meter
     *
     * @param \AppBundle\Entity\EmsMeterCircuitMapping $meter
     *
     * @return EmsCircuitMaster
     */
    public function addMeter(\AppBundle\Entity\EmsMeterCircuitMapping $meter)
    {
        $this->meter[] = $meter;

        return $this;
    }

    /**
     * Remove meter
     *
     * @param \AppBundle\Entity\EmsMeterCircuitMapping $meter
     */
    public function removeMeter(\AppBundle\Entity\EmsMeterCircuitMapping $meter)
    {
        $this->meter->removeElement($meter);
    }

    /**
     * Get meter
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMeter()
    {
        return $this->meter;
    }
    /**
     * @var \AppBundle\Entity\EmsCircuitCategory
     */
    private $circuitCategory;


    /**
     * Set circuitCategory
     *
     * @param \AppBundle\Entity\EmsCircuitCategory $circuitCategory
     *
     * @return EmsCircuitMaster
     */
    public function setCircuitCategory(\AppBundle\Entity\EmsCircuitCategory $circuitCategory = null)
    {
        $this->circuitCategory = $circuitCategory;

        return $this;
    }

    /**
     * Get circuitCategory
     *
     * @return \AppBundle\Entity\EmsCircuitCategory
     */
    public function getCircuitCategory()
    {
        return $this->circuitCategory;
    }
    /**
     * @var \AppBundle\Entity\EvChargingStationMaster
     */
    private $chargingStation;


    /**
     * Set chargingStation
     *
     * @param \AppBundle\Entity\EvChargingStationMaster $chargingStation
     *
     * @return EmsCircuitMaster
     */
    public function setChargingStation(\AppBundle\Entity\EvChargingStationMaster $chargingStation = null)
    {
        $this->chargingStation = $chargingStation;

        return $this;
    }

    /**
     * Get chargingStation
     *
     * @return \AppBundle\Entity\EvChargingStationMaster
     */
    public function getChargingStation()
    {
        return $this->chargingStation;
    }

    /**
     * Add device
     *
     * @param \AppBundle\Entity\EmsDeviceMaster $device
     *
     * @return EmsCircuitMaster
     */
    public function addDevice(\AppBundle\Entity\EmsDeviceMaster $device)
    {
        $this->device[] = $device;

        return $this;
    }

    /**
     * Remove device
     *
     * @param \AppBundle\Entity\EmsDeviceMaster $device
     */
    public function removeDevice(\AppBundle\Entity\EmsDeviceMaster $device)
    {
        $this->device->removeElement($device);
    }

    /**
     * Get device
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDevice()
    {
        return $this->device;
    }

    public function getValidDevice()
    {
        if ( $this->device->isEmpty()) {
            return null;
        }
        return $this->device->get(0);
    }

    /**
     * Add chargingStation
     *
     * @param \AppBundle\Entity\EvChargingStationMaster $chargingStation
     *
     * @return EmsCircuitMaster
     */
    public function addChargingStation(\AppBundle\Entity\EvChargingStationMaster $chargingStation)
    {
        $this->chargingStation[] = $chargingStation;

        return $this;
    }

    /**
     * Remove chargingStation
     *
     * @param \AppBundle\Entity\EvChargingStationMaster $chargingStation
     */
    public function removeChargingStation(\AppBundle\Entity\EvChargingStationMaster $chargingStation)
    {
        $this->chargingStation->removeElement($chargingStation);
    }
    /**
     * @var float
     */
    private $circuitStandbyCurrent;


    /**
     * Set circuitStandbyCurrent
     *
     * @param integer $circuitStandbyCurrent
     *
     * @return EmsCircuitMaster
     */
    public function setCircuitStandbyCurrent($circuitStandbyCurrent)
    {
        $this->circuitStandbyCurrent = $circuitStandbyCurrent;

        return $this;
    }

    /**
     * Get circuitStandbyCurrent
     *
     * @return integer
     */
    public function getCircuitStandbyCurrent()
    {
        return $this->circuitStandbyCurrent;
    }
    /**
     * @var string|null
     */
    private $circuitPhase;


    /**
     * Set circuitPhase.
     *
     * @param string|null $circuitPhase
     *
     * @return EmsCircuitMaster
     */
    public function setCircuitPhase($circuitPhase = null)
    {
        $this->circuitPhase = $circuitPhase;

        return $this;
    }

    /**
     * Get circuitPhase.
     *
     * @return string|null
     */
    public function getCircuitPhase()
    {
        return $this->circuitPhase;
    }
    /**
     * @var \DateTime|null
     */
    private $lastDataReport;


    /**
     * Set lastDataReport.
     *
     * @param \DateTime|null $lastDataReport
     *
     * @return EmsCircuitMaster
     */
    public function setLastDataReport($lastDataReport = null)
    {
        $this->lastDataReport = $lastDataReport;

        return $this;
    }

    /**
     * Get lastDataReport.
     *
     * @return \DateTime|null
     */
    public function getLastDataReport()
    {
        return $this->lastDataReport;
    }
    /**
     * @var \DateTime|null
     */
    private $newestData;


    /**
     * Set newestData.
     *
     * @param \DateTime|null $newestData
     *
     * @return EmsCircuitMaster
     */
    public function setNewestData($newestData = null)
    {
        if ( $this->newestData == null || $newestData > $this->newestData ) {
            $this->newestData = $newestData;
        }
        return $this;
    }

    /**
     * Get newestData.
     *
     * @return \DateTime|null
     */
    public function getNewestData()
    {
        return $this->newestData;
    }
    /**
     * @var string|null
     */
    private $parentPhase;


    /**
     * Set parentPhase.
     *
     * @param string|null $parentPhase
     *
     * @return EmsCircuitMaster
     */
    public function setParentPhase($parentPhase = null)
    {
        $this->parentPhase = $parentPhase;

        return $this;
    }

    /**
     * Get parentPhase.
     *
     * @return string|null
     */
    public function getParentPhase()
    {
        return $this->parentPhase;
    }
    /**
     * @var int|null
     */
    private $smbChannel = '0';

    /**
     * @var string|null
     */
    private $smbPhase;


    /**
     * Set smbChannel.
     *
     * @param int|null $smbChannel
     *
     * @return EmsCircuitMaster
     */
    public function setSmbChannel($smbChannel = null)
    {
        $this->smbChannel = $smbChannel;

        return $this;
    }

    /**
     * Get smbChannel.
     *
     * @return int|null
     */
    public function getSmbChannel()
    {
        return $this->smbChannel;
    }

    /**
     * Set smbPhase.
     *
     * @param string|null $smbPhase
     *
     * @return EmsCircuitMaster
     */
    public function setSmbPhase($smbPhase = null)
    {
        $this->smbPhase = $smbPhase;

        return $this;
    }

    /**
     * Get smbPhase.
     *
     * @return string|null
     */
    public function getSmbPhase()
    {
        return $this->smbPhase;
    }
    /**
     * @var \AppBundle\Entity\EvDemandAllocationStrategy
     */
    private $demandAllocionStrategy;


    /**
     * Set demandAllocionStrategy.
     *
     * @param \AppBundle\Entity\EvDemandAllocationStrategy|null $demandAllocionStrategy
     *
     * @return EmsCircuitMaster
     */
    public function setDemandAllocionStrategy(\AppBundle\Entity\EvDemandAllocationStrategy $demandAllocionStrategy = null)
    {
        $this->demandAllocionStrategy = $demandAllocionStrategy;

        return $this;
    }

    /**
     * Get demandAllocionStrategy.
     *
     * @return \AppBundle\Entity\EvDemandAllocationStrategy|null
     */
    public function getDemandAllocionStrategy()
    {
        return $this->demandAllocionStrategy;
    }
}
