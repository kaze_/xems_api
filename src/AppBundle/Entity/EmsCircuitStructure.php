<?php

namespace AppBundle\Entity;

/**
 * EmsCircuitStructure
 */
class EmsCircuitStructure
{
    /**
     * @var integer
     */
    private $lft;

    /**
     * @var integer
     */
    private $rgt;

    /**
     * @var integer
     */
    private $lvl;

    /**
     * @var string
     */
    private $treeRoot;

    /**
     * @var string
     */
    private $parentId;

    /**
     * @var \AppBundle\Entity\EmsCircuitMaster
     */
    private $circuit;


    /**
     * Set lft
     *
     * @param integer $lft
     *
     * @return EmsCircuitStructure
     */
    public function setLft($lft)
    {
        $this->lft = $lft;

        return $this;
    }

    /**
     * Get lft
     *
     * @return integer
     */
    public function getLft()
    {
        return $this->lft;
    }

    /**
     * Set rgt
     *
     * @param integer $rgt
     *
     * @return EmsCircuitStructure
     */
    public function setRgt($rgt)
    {
        $this->rgt = $rgt;

        return $this;
    }

    /**
     * Get rgt
     *
     * @return integer
     */
    public function getRgt()
    {
        return $this->rgt;
    }

    /**
     * Set lvl
     *
     * @param integer $lvl
     *
     * @return EmsCircuitStructure
     */
    public function setLvl($lvl)
    {
        $this->lvl = $lvl;

        return $this;
    }

    /**
     * Get lvl
     *
     * @return integer
     */
    public function getLvl()
    {
        return $this->lvl;
    }

    /**
     * Set treeRoot
     *
     * @param string $treeRoot
     *
     * @return EmsCircuitStructure
     */
    public function setTreeRoot($treeRoot)
    {
        $this->treeRoot = $treeRoot;

        return $this;
    }

    /**
     * Get treeRoot
     *
     * @return string
     */
    public function getTreeRoot()
    {
        return $this->treeRoot;
    }

    /**
     * Set parentId
     *
     * @param string $parentId
     *
     * @return EmsCircuitStructure
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;

        return $this;
    }

    /**
     * Get parentId
     *
     * @return string
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * Set circuit
     *
     * @param \AppBundle\Entity\EmsCircuitMaster $circuit
     *
     * @return EmsCircuitStructure
     */
    public function setCircuit(\AppBundle\Entity\EmsCircuitMaster $circuit = null)
    {
        $this->circuit = $circuit;

        return $this;
    }

    /**
     * Get circuit
     *
     * @return \AppBundle\Entity\EmsCircuitMaster
     */
    public function getCircuit()
    {
        return $this->circuit;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $chiledren;

    /**
     * @var \AppBundle\Entity\EmsCircuitStructure
     */
    private $root;

    /**
     * @var \AppBundle\Entity\EmsCircuitStructure
     */
    private $parent;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->chiledren = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add chiledren
     *
     * @param \AppBundle\Entity\EmsCircuitStructure $chiledren
     *
     * @return EmsCircuitStructure
     */
    public function addChiledren(\AppBundle\Entity\EmsCircuitStructure $chiledren)
    {
        $this->chiledren[] = $chiledren;

        return $this;
    }

    /**
     * Remove chiledren
     *
     * @param \AppBundle\Entity\EmsCircuitStructure $chiledren
     */
    public function removeChiledren(\AppBundle\Entity\EmsCircuitStructure $chiledren)
    {
        $this->chiledren->removeElement($chiledren);
    }

    /**
     * Get chiledren
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChiledren()
    {
        return $this->chiledren;
    }

    /**
     * Set root
     *
     * @param \AppBundle\Entity\EmsCircuitStructure $root
     *
     * @return EmsCircuitStructure
     */
    public function setRoot(\AppBundle\Entity\EmsCircuitStructure $root = null)
    {
        $this->root = $root;

        return $this;
    }

    /**
     * Get root
     *
     * @return \AppBundle\Entity\EmsCircuitStructure
     */
    public function getRoot()
    {
        return $this->root;
    }

    /**
     * Set parent
     *
     * @param \AppBundle\Entity\EmsCircuitStructure $parent
     *
     * @return EmsCircuitStructure
     */
    public function setParent(\AppBundle\Entity\EmsCircuitStructure $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \AppBundle\Entity\EmsCircuitStructure
     */
    public function getParent()
    {
        return $this->parent;
    }
}
