<?php

namespace AppBundle\Entity;

/**
 * EmsCustomerAdvanceInfo
 */
class EmsCustomerAdvanceInfo
{
    /**
     * @var \AppBundle\Entity\EmsCustomerMaster
     */
    private $customer;


    /**
     * Set customer
     *
     * @param \AppBundle\Entity\EmsCustomerMaster $customer
     *
     * @return EmsCustomerAdvanceInfo
     */
    public function setCustomer(\AppBundle\Entity\EmsCustomerMaster $customer = null)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return \AppBundle\Entity\EmsCustomerMaster
     */
    public function getCustomer()
    {
        return $this->customer;
    }
}
