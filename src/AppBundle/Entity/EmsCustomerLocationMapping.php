<?php

namespace AppBundle\Entity;

use Ramsey\Uuid\Uuid;

/**
 * EmsCustomerLocationMapping
 */
class EmsCustomerLocationMapping
{
    /**
     * @var uuid
     */
    private $locationId;

    /**
     * @var uuid
     */
    private $customerId;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var integer
     */
    private $statusId;

    public function __construct()
    {
        $this->date = new \DateTime();
    }
    

    /**
     * Set locationId
     *
     * @param uuid $locationId
     *
     * @return EmsCustomerLocationMapping
     */
    public function setLocationId($locationId)
    {
        $this->locationId = $locationId;

        return $this;
    }

    /**
     * Get locationId
     *
     * @return uuid
     */
    public function getLocationId()
    {
        return $this->locationId;
    }

    /**
     * Set customerId
     *
     * @param uuid $customerId
     *
     * @return EmsCustomerLocationMapping
     */
    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;

        return $this;
    }

    /**
     * Get customerId
     *
     * @return uuid
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return EmsCustomerLocationMapping
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set statusId
     *
     * @param integer $statusId
     *
     * @return EmsCustomerLocationMapping
     */
    public function setStatusId($statusId)
    {
        $this->statusId = $statusId;

        return $this;
    }

    /**
     * Get statusId
     *
     * @return integer
     */
    public function getStatusId()
    {
        return $this->statusId;
    }
}
