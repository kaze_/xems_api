<?php

namespace AppBundle\Entity;

use Ramsey\Uuid\Uuid;

/**
 * EmsCustomerMaster
 */
class EmsCustomerMaster
{
    /**
     * @var uuid
     */
    private $customerId;

    /**
     * @var string
     */
    private $customerName;

    /**
     * @var string
     */
    private $customerEmail;

    /**
     * @var string
     */
    private $customerContactPhone;

    /**
     * @var string
     */
    private $customerRegisterMobileNumber;

    /**
     * @var \DateTime
     */
    private $enableDatetime;

    /**
     * @var integer
     */
    private $statusId;

    /**
     * @var string
     */
    private $contactName;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $xservice;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->xservice = new \Doctrine\Common\Collections\ArrayCollection();
        $this->enableDatetime = new \DateTime();
    }

    public function __toString()
    {
        return (string) $this->getCustomerId();
    }

    /**
     * Get customerId
     *
     * @return uuid
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * Set customerName
     *
     * @param string $customerName
     *
     * @return EmsCustomerMaster
     */
    public function setCustomerName($customerName)
    {
        $this->customerName = $customerName;

        return $this;
    }

    /**
     * Get customerName
     *
     * @return string
     */
    public function getCustomerName()
    {
        return $this->customerName;
    }

    /**
     * Set customerEmail
     *
     * @param string $customerEmail
     *
     * @return EmsCustomerMaster
     */
    public function setCustomerEmail($customerEmail)
    {
        $this->customerEmail = $customerEmail;

        return $this;
    }

    /**
     * Get customerEmail
     *
     * @return string
     */
    public function getCustomerEmail()
    {
        return $this->customerEmail;
    }

    /**
     * Set customerContactPhone
     *
     * @param string $customerContactPhone
     *
     * @return EmsCustomerMaster
     */
    public function setCustomerContactPhone($customerContactPhone)
    {
        $this->customerContactPhone = $customerContactPhone;

        return $this;
    }

    /**
     * Get customerContactPhone
     *
     * @return string
     */
    public function getCustomerContactPhone()
    {
        return $this->customerContactPhone;
    }

    /**
     * Set customerRegisterMobileNumber
     *
     * @param string $customerRegisterMobileNumber
     *
     * @return EmsCustomerMaster
     */
    public function setCustomerRegisterMobileNumber($customerRegisterMobileNumber)
    {
        $this->customerRegisterMobileNumber = $customerRegisterMobileNumber;

        return $this;
    }

    /**
     * Get customerRegisterMobileNumber
     *
     * @return string
     */
    public function getCustomerRegisterMobileNumber()
    {
        return $this->customerRegisterMobileNumber;
    }

    /**
     * Set enableDatetime
     *
     * @param \DateTime $enableDatetime
     *
     * @return EmsCustomerMaster
     */
    public function setEnableDatetime($enableDatetime)
    {
        $this->enableDatetime = $enableDatetime;

        return $this;
    }

    /**
     * Get enableDatetime
     *
     * @return \DateTime
     */
    public function getEnableDatetime()
    {
        return $this->enableDatetime;
    }

    /**
     * Set statusId
     *
     * @param integer $statusId
     *
     * @return EmsCustomerMaster
     */
    public function setStatusId($statusId)
    {
        $this->statusId = $statusId;

        return $this;
    }

    /**
     * Get statusId
     *
     * @return integer
     */
    public function getStatusId()
    {
        return $this->statusId;
    }

    /**
     * Set contactName
     *
     * @param string $contactName
     *
     * @return EmsCustomerMaster
     */
    public function setContactName($contactName)
    {
        $this->contactName = $contactName;

        return $this;
    }

    /**
     * Get contactName
     *
     * @return string
     */
    public function getContactName()
    {
        return $this->contactName;
    }

    /**
     * Add xservice
     *
     * @param \AppBundle\Entity\XmightServiceMaster $xservice
     *
     * @return EmsCustomerMaster
     */
    public function addXservice(\AppBundle\Entity\XmightServiceMaster $xservice)
    {
        $this->xservice[] = $xservice;

        return $this;
    }

    /**
     * Remove xservice
     *
     * @param \AppBundle\Entity\XmightServiceMaster $xservice
     */
    public function removeXservice(\AppBundle\Entity\XmightServiceMaster $xservice)
    {
        $this->xservice->removeElement($xservice);
    }

    /**
     * Get xservice
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getXservice()
    {
        return $this->xservice;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $users;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $location;


    /**
     * Add user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return EmsCustomerMaster
     */
    public function addUser(\AppBundle\Entity\User $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \AppBundle\Entity\User $user
     */
    public function removeUser(\AppBundle\Entity\User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Add location
     *
     * @param \AppBundle\Entity\EmsLocationMaster $location
     *
     * @return EmsCustomerMaster
     */
    public function addLocation(\AppBundle\Entity\EmsLocationMaster $location)
    {
        $this->location[] = $location;

        return $this;
    }

    /**
     * Remove location
     *
     * @param \AppBundle\Entity\EmsLocationMaster $location
     */
    public function removeLocation(\AppBundle\Entity\EmsLocationMaster $location)
    {
        $this->location->removeElement($location);
    }

    /**
     * Get location
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLocation()
    {
        return $this->location;
    }
}
