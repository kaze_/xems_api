<?php

namespace AppBundle\Entity;

/**
 * EmsDeviceMaster
 */
class EmsDeviceMaster
{
    /**
     * @var string
     */
    private $deviceId;

    /**
     * @var string
     */
    private $deviceName;

    /**
     * @var string
     */
    private $deviceSwVersion;

    /**
     * @var string
     */
    private $deviceSn;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $gateway;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $circuit;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->gateway = new \Doctrine\Common\Collections\ArrayCollection();
        $this->circuit = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get deviceId
     *
     * @return string
     */
    public function getDeviceId()
    {
        return $this->deviceId;
    }

    /**
     * Set deviceName
     *
     * @param string $deviceName
     *
     * @return EmsDeviceMaster
     */
    public function setDeviceName($deviceName)
    {
        $this->deviceName = $deviceName;

        return $this;
    }

    /**
     * Get deviceName
     *
     * @return string
     */
    public function getDeviceName()
    {
        return $this->deviceName;
    }


    /**
     * Set deviceSwVersion
     *
     * @param string $deviceSwVersion
     *
     * @return EmsDeviceMaster
     */
    public function setDeviceSwVersion($deviceSwVersion)
    {
        $this->deviceSwVersion = $deviceSwVersion;

        return $this;
    }

    /**
     * Get deviceSwVersion
     *
     * @return string
     */
    public function getDeviceSwVersion()
    {
        return $this->deviceSwVersion;
    }

    /**
     * Set deviceSn
     *
     * @param string $deviceSn
     *
     * @return EmsDeviceMaster
     */
    public function setDeviceSn($deviceSn)
    {
        $this->deviceSn = $deviceSn;

        return $this;
    }

    /**
     * Get deviceSn
     *
     * @return string
     */
    public function getDeviceSn()
    {
        return $this->deviceSn;
    }

    /**
     * Add gateway
     *
     * @param \AppBundle\Entity\EmsGatewayMaster $gateway
     *
     * @return EmsDeviceMaster
     */
    public function addGateway(\AppBundle\Entity\EmsGatewayMaster $gateway)
    {
        $this->gateway[] = $gateway;

        return $this;
    }

    /**
     * Remove gateway
     *
     * @param \AppBundle\Entity\EmsGatewayMaster $gateway
     */
    public function removeGateway(\AppBundle\Entity\EmsGatewayMaster $gateway)
    {
        $this->gateway->removeElement($gateway);
    }

    /**
     * Get gateway
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGateway()
    {
        return $this->gateway;
    }

    /**
     * Get a gateway which status = true (1)
     */
    public function getValidGateway()
    {
        if ( $this->gateway->isEmpty()) {
            return null;
        }
        return $this->gateway->get(0);
    }

    /**
     * Add circuit
     *
     * @param \AppBundle\Entity\EmsCircuitMaster $circuit
     *
     * @return EmsDeviceMaster
     */
    public function addCircuit(\AppBundle\Entity\EmsCircuitMaster $circuit)
    {
        $this->circuit[] = $circuit;

        return $this;
    }

    /**
     * Remove circuit
     *
     * @param \AppBundle\Entity\EmsCircuitMaster $circuit
     */
    public function removeCircuit(\AppBundle\Entity\EmsCircuitMaster $circuit)
    {
        $this->circuit->removeElement($circuit);
    }

    /**
     * Get circuit
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCircuit()
    {
        return $this->circuit;
    }
    /**
     * @var \AppBundle\Entity\EmsHardwareInfoList
     */
    private $deviceHw;

    /**
     * @var \AppBundle\Entity\EmsDeviceType
     */
    private $deviceType;


    /**
     * Set deviceHw
     *
     * @param \AppBundle\Entity\EmsHardwareInfoList $deviceHw
     *
     * @return EmsDeviceMaster
     */
    public function setDeviceHw(\AppBundle\Entity\EmsHardwareInfoList $deviceHw = null)
    {
        $this->deviceHw = $deviceHw;

        return $this;
    }

    /**
     * Get deviceHw
     *
     * @return \AppBundle\Entity\EmsHardwareInfoList
     */
    public function getDeviceHw()
    {
        return $this->deviceHw;
    }

    /**
     * Set deviceType
     *
     * @param \AppBundle\Entity\EmsDeviceType $deviceType
     *
     * @return EmsDeviceMaster
     */
    public function setDeviceType(\AppBundle\Entity\EmsDeviceType $deviceType = null)
    {
        $this->deviceType = $deviceType;

        return $this;
    }

    /**
     * Get deviceType
     *
     * @return \AppBundle\Entity\EmsDeviceType
     */
    public function getDeviceType()
    {
        return $this->deviceType;
    }

    private $deviceTypeData;

    /**
     * @var \AppBundle\Entity\EmsModbusGateway
     */
    private $modbusGateway;

    /**
     * @var \AppBundle\Entity\EmsPowerMeter
     */
    private $powerMeter;


    /**
     * Set modbusGateway
     *
     * @param \AppBundle\Entity\EmsModbusGateway $modbusGateway
     *
     * @return EmsDeviceMaster
     */
    public function setModbusGateway(\AppBundle\Entity\EmsModbusGateway $modbusGateway = null)
    {
        $this->modbusGateway = $modbusGateway;

        return $this;
    }

    /**
     * Get modbusGateway
     *
     * @return \AppBundle\Entity\EmsModbusGateway
     */
    public function getModbusGateway()
    {
        return $this->modbusGateway;
    }

    /**
     * Set powerMeter
     *
     * @param \AppBundle\Entity\EmsPowerMeter $powerMeter
     *
     * @return EmsDeviceMaster
     */
    public function setPowerMeter(\AppBundle\Entity\EmsPowerMeter $powerMeter = null)
    {
        $this->powerMeter = $powerMeter;

        return $this;
    }

    /**
     * Get powerMeter
     *
     * @return \AppBundle\Entity\EmsPowerMeter
     */
    public function getPowerMeter()
    {
        return $this->powerMeter;
    }
}
