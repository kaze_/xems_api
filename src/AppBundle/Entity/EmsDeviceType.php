<?php

namespace AppBundle\Entity;

/**
 * EmsDeviceType
 */
class EmsDeviceType
{
    /**
     * @var integer
     */
    private $deviceTypeId;

    /**
     * @var string
     */
    private $deviceTypeName;

    /**
     * @var string
     */
    private $deviceTableName;


    /**
     * Get deviceTypeId
     *
     * @return integer
     */
    public function getDeviceTypeId()
    {
        return $this->deviceTypeId;
    }

    /**
     * Set deviceTypeName
     *
     * @param string $deviceTypeName
     *
     * @return EmsDeviceType
     */
    public function setDeviceTypeName($deviceTypeName)
    {
        $this->deviceTypeName = $deviceTypeName;

        return $this;
    }

    /**
     * Get deviceTypeName
     *
     * @return string
     */
    public function getDeviceTypeName()
    {
        return $this->deviceTypeName;
    }

    /**
     * Set deviceTableName
     *
     * @param string $deviceTableName
     *
     * @return EmsDeviceType
     */
    public function setDeviceTableName($deviceTableName)
    {
        $this->deviceTableName = $deviceTableName;

        return $this;
    }

    /**
     * Get deviceTableName
     *
     * @return string
     */
    public function getDeviceTableName()
    {
        return $this->deviceTableName;
    }
}
