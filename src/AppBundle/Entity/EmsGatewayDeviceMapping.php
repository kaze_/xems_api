<?php

namespace AppBundle\Entity;

use Ramsey\Uuid\Uuid;

/**
 * EmsGatewayDeviceMapping
 */
class EmsGatewayDeviceMapping
{
    /**
     * @var uuid
     */
    private $gatewayId;

    /**
     * @var uuid
     */
    private $deviceId;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var integer
     */
    private $statusId;

    public function __construct()
    {
        $this->date = new \DateTime();
    }
    /**
     * Set gatewayId
     *
     * @param uuid $gatewayId
     *
     * @return EmsGatewayDeviceMapping
     */
    public function setGatewayId($gatewayId)
    {
        $this->gatewayId = $gatewayId;

        return $this;
    }

    /**
     * Get gatewayId
     *
     * @return uuid
     */
    public function getGatewayId()
    {
        return $this->gatewayId;
    }

    /**
     * Set deviceId
     *
     * @param uuid $deviceId
     *
     * @return EmsGatewayDeviceMapping
     */
    public function setDeviceId($deviceId)
    {
        $this->deviceId = $deviceId;

        return $this;
    }

    /**
     * Get deviceId
     *
     * @return uuid
     */
    public function getDeviceId()
    {
        return $this->deviceId;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return EmsGatewayDeviceMapping
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set statusId
     *
     * @param integer $statusId
     *
     * @return EmsGatewayDeviceMapping
     */
    public function setStatusId($statusId)
    {
        $this->statusId = $statusId;

        return $this;
    }

    /**
     * Get statusId
     *
     * @return integer
     */
    public function getStatusId()
    {
        return $this->statusId;
    }
}

