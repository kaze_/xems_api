<?php

namespace AppBundle\Entity;

/**
 * EmsGatewayJob
 */
class EmsGatewayJob
{
    // gateway commands;
    const CMD_NOOP="noop";
    const CMD_REBOOT="reboot";
    const CMD_AUTH_PLATE="authByCarPlate";

    /**
     * @var int
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $validTo;

    /**
     * @var \DateTime|null
     */
    private $polledAt;

    /**
     * @var string|null
     */
    private $toGateway;

    /**
     * @var string|null
     */
    private $job;

    /**
     * @var string|null
     */
    private $commandGiver;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return EmsGatewayJob
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set validTo.
     *
     * @param \DateTime $validTo
     *
     * @return EmsGatewayJob
     */
    public function setValidTo($validTo)
    {
        $this->validTo = $validTo;

        return $this;
    }

    /**
     * Get validTo.
     *
     * @return \DateTime
     */
    public function getValidTo()
    {
        return $this->validTo;
    }

    /**
     * Set polledAt.
     *
     * @param \DateTime|null $polledAt
     *
     * @return EmsGatewayJob
     */
    public function setPolledAt($polledAt = null)
    {
        $this->polledAt = $polledAt;

        return $this;
    }

    /**
     * Get polledAt.
     *
     * @return \DateTime|null
     */
    public function getPolledAt()
    {
        return $this->polledAt;
    }

    /**
     * Set toGateway.
     *
     * @param string|null $toGateway
     *
     * @return EmsGatewayJob
     */
    public function setToGateway($toGateway = null)
    {
        $this->toGateway = $toGateway;

        return $this;
    }

    /**
     * Get toGateway.
     *
     * @return string|null
     */
    public function getToGateway()
    {
        return $this->toGateway;
    }

    /**
     * Set job.
     *
     * @param string|null $job
     *
     * @return EmsGatewayJob
     */
    public function setJob($job = null)
    {
        $this->job = $job;

        return $this;
    }

    /**
     * Get job.
     *
     * @return string|null
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     * Set commandGiver.
     *
     * @param string|null $commandGiver
     *
     * @return EmsGatewayJob
     */
    public function setCommandGiver($commandGiver = null)
    {
        $this->commandGiver = $commandGiver;

        return $this;
    }

    /**
     * Get commandGiver.
     *
     * @return string|null
     */
    public function getCommandGiver()
    {
        return $this->commandGiver;
    }
}
