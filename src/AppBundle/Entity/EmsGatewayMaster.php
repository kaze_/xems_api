<?php

namespace AppBundle\Entity;

/**
 * EmsGatewayMaster
 */
class EmsGatewayMaster
{
    /**
     * @var string
     */
    private $gatewayId;

    /**
     * @var string
     */
    private $gatewayEthernetMac;

    /**
     * @var string
     */
    private $gatewayName;

    /**
     * @var string
     */
    private $gatewaySwVersion;

    /**
     * @var string
     */
    private $gatewayMeta;

    /**
     * @var \AppBundle\Entity\EmsHardwareInfoList
     */
    private $gatewayHw;

    /**
     * @var \AppBundle\Entity\SysStatusList
     */
    private $status;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $device;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->device = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get gatewayId
     *
     * @return string
     */
    public function getGatewayId()
    {
        return $this->gatewayId;
    }

    /**
     * Set gatewayEthernetMac
     *
     * @param string $gatewayEthernetMac
     *
     * @return EmsGatewayMaster
     */
    public function setGatewayEthernetMac($gatewayEthernetMac)
    {
        $this->gatewayEthernetMac = $gatewayEthernetMac;

        return $this;
    }

    /**
     * Get gatewayEthernetMac
     *
     * @return string
     */
    public function getGatewayEthernetMac()
    {
        return $this->gatewayEthernetMac;
    }

    /**
     * Set gatewayName
     *
     * @param string $gatewayName
     *
     * @return EmsGatewayMaster
     */
    public function setGatewayName($gatewayName)
    {
        $this->gatewayName = $gatewayName;

        return $this;
    }

    /**
     * Get gatewayName
     *
     * @return string
     */
    public function getGatewayName()
    {
        return $this->gatewayName;
    }

    /**
     * Set gatewaySwVersion
     *
     * @param string $gatewaySwVersion
     *
     * @return EmsGatewayMaster
     */
    public function setGatewaySwVersion($gatewaySwVersion)
    {
        $this->gatewaySwVersion = $gatewaySwVersion;

        return $this;
    }

    /**
     * Get gatewaySwVersion
     *
     * @return string
     */
    public function getGatewaySwVersion()
    {
        return $this->gatewaySwVersion;
    }

    /**
     * Set gatewayMeta
     *
     * @param string $gatewayMeta
     *
     * @return EmsGatewayMaster
     */
    public function setGatewayMeta($gatewayMeta)
    {
        $this->gatewayMeta = $gatewayMeta;

        return $this;
    }

    /**
     * Get gatewayMeta
     *
     * @return string
     */
    public function getGatewayMeta()
    {
        return $this->gatewayMeta;
    }

    /**
     * Set gatewayHw
     *
     * @param \AppBundle\Entity\EmsHardwareInfoList $gatewayHw
     *
     * @return EmsGatewayMaster
     */
    public function setGatewayHw(\AppBundle\Entity\EmsHardwareInfoList $gatewayHw = null)
    {
        $this->gatewayHw = $gatewayHw;

        return $this;
    }

    /**
     * Get gatewayHw
     *
     * @return \AppBundle\Entity\EmsHardwareInfoList
     */
    public function getGatewayHw()
    {
        return $this->gatewayHw;
    }

    /**
     * Set status
     *
     * @param \AppBundle\Entity\SysStatusList $status
     *
     * @return EmsGatewayMaster
     */
    public function setStatus(\AppBundle\Entity\SysStatusList $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return \AppBundle\Entity\SysStatusList
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Add device
     *
     * @param \AppBundle\Entity\EmsDeviceMaster $device
     *
     * @return EmsGatewayMaster
     */
    public function addDevice(\AppBundle\Entity\EmsDeviceMaster $device)
    {
        $this->device[] = $device;

        return $this;
    }

    /**
     * Remove device
     *
     * @param \AppBundle\Entity\EmsDeviceMaster $device
     */
    public function removeDevice(\AppBundle\Entity\EmsDeviceMaster $device)
    {
        $this->device->removeElement($device);
    }

    /**
     * Get device
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDevice()
    {
        return $this->device;
    }
    /**
     * @var string|null
     */
    private $ipAddress;

    /**
     * @var string|null
     */
    private $account;

    /**
     * @var string|null
     */
    private $password;

    /**
     * @var string|null
     */
    private $localIp;

    /**
     * @var string|null
     */
    private $comment;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $location;


    /**
     * Set ipAddress.
     *
     * @param string|null $ipAddress
     *
     * @return EmsGatewayMaster
     */
    public function setIpAddress($ipAddress = null)
    {
        $this->ipAddress = $ipAddress;

        return $this;
    }

    /**
     * Get ipAddress.
     *
     * @return string|null
     */
    public function getIpAddress()
    {
        return $this->ipAddress;
    }

    /**
     * Set account.
     *
     * @param string|null $account
     *
     * @return EmsGatewayMaster
     */
    public function setAccount($account = null)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account.
     *
     * @return string|null
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set password.
     *
     * @param string|null $password
     *
     * @return EmsGatewayMaster
     */
    public function setPassword($password = null)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password.
     *
     * @return string|null
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set localIp.
     *
     * @param string|null $localIp
     *
     * @return EmsGatewayMaster
     */
    public function setLocalIp($localIp = null)
    {
        $this->localIp = $localIp;

        return $this;
    }

    /**
     * Get localIp.
     *
     * @return string|null
     */
    public function getLocalIp()
    {
        return $this->localIp;
    }

    /**
     * Set comment.
     *
     * @param string|null $comment
     *
     * @return EmsGatewayMaster
     */
    public function setComment($comment = null)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment.
     *
     * @return string|null
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Add location.
     *
     * @param \AppBundle\Entity\EmsLocationMaster $location
     *
     * @return EmsGatewayMaster
     */
    public function addLocation(\AppBundle\Entity\EmsLocationMaster $location)
    {
        $this->location[] = $location;

        return $this;
    }

    /**
     * Remove location.
     *
     * @param \AppBundle\Entity\EmsLocationMaster $location
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeLocation(\AppBundle\Entity\EmsLocationMaster $location)
    {
        return $this->location->removeElement($location);
    }

    /**
     * Get location.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLocation()
    {
        return $this->location;
    }
}
