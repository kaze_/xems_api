<?php

namespace AppBundle\Entity;

/**
 * EmsGatewayStructure
 */
class EmsGatewayStructure
{
    /**
     * @var int
     */
    private $lft;

    /**
     * @var int
     */
    private $rgt;

    /**
     * @var int
     */
    private $lvl;

    /**
     * @var \AppBundle\Entity\EmsGatewayMaster
     */
    private $entity;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $chiledren;

    /**
     * @var \AppBundle\Entity\EmsGatewayStructure
     */
    private $root;

    /**
     * @var \AppBundle\Entity\EmsGatewayStructure
     */
    private $parent;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->chiledren = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set lft.
     *
     * @param int $lft
     *
     * @return EmsGatewayStructure
     */
    public function setLft($lft)
    {
        $this->lft = $lft;

        return $this;
    }

    /**
     * Get lft.
     *
     * @return int
     */
    public function getLft()
    {
        return $this->lft;
    }

    /**
     * Set rgt.
     *
     * @param int $rgt
     *
     * @return EmsGatewayStructure
     */
    public function setRgt($rgt)
    {
        $this->rgt = $rgt;

        return $this;
    }

    /**
     * Get rgt.
     *
     * @return int
     */
    public function getRgt()
    {
        return $this->rgt;
    }

    /**
     * Set lvl.
     *
     * @param int $lvl
     *
     * @return EmsGatewayStructure
     */
    public function setLvl($lvl)
    {
        $this->lvl = $lvl;

        return $this;
    }

    /**
     * Get lvl.
     *
     * @return int
     */
    public function getLvl()
    {
        return $this->lvl;
    }

    /**
     * Set entity.
     *
     * @param \AppBundle\Entity\EmsGatewayMaster|null $entity
     *
     * @return EmsGatewayStructure
     */
    public function setEntity(\AppBundle\Entity\EmsGatewayMaster $entity = null)
    {
        $this->entity = $entity;

        return $this;
    }

    /**
     * Get entity.
     *
     * @return \AppBundle\Entity\EmsGatewayMaster|null
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * Add chiledren.
     *
     * @param \AppBundle\Entity\EmsGatewayStructure $chiledren
     *
     * @return EmsGatewayStructure
     */
    public function addChiledren(\AppBundle\Entity\EmsGatewayStructure $chiledren)
    {
        $this->chiledren[] = $chiledren;

        return $this;
    }

    /**
     * Remove chiledren.
     *
     * @param \AppBundle\Entity\EmsGatewayStructure $chiledren
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeChiledren(\AppBundle\Entity\EmsGatewayStructure $chiledren)
    {
        return $this->chiledren->removeElement($chiledren);
    }

    /**
     * Get chiledren.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChiledren()
    {
        return $this->chiledren;
    }

    /**
     * Set root.
     *
     * @param \AppBundle\Entity\EmsGatewayStructure|null $root
     *
     * @return EmsGatewayStructure
     */
    public function setRoot(\AppBundle\Entity\EmsGatewayStructure $root = null)
    {
        $this->root = $root;

        return $this;
    }

    /**
     * Get root.
     *
     * @return \AppBundle\Entity\EmsGatewayStructure|null
     */
    public function getRoot()
    {
        return $this->root;
    }

    /**
     * Set parent.
     *
     * @param \AppBundle\Entity\EmsGatewayStructure|null $parent
     *
     * @return EmsGatewayStructure
     */
    public function setParent(\AppBundle\Entity\EmsGatewayStructure $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent.
     *
     * @return \AppBundle\Entity\EmsGatewayStructure|null
     */
    public function getParent()
    {
        return $this->parent;
    }
    /**
     * @var \AppBundle\Entity\EmsGatewayMaster
     */
    private $gateway;


    /**
     * Set gateway.
     *
     * @param \AppBundle\Entity\EmsGatewayMaster $gateway
     *
     * @return EmsGatewayStructure
     */
    public function setGateway(\AppBundle\Entity\EmsGatewayMaster $gateway)
    {
        $this->gateway = $gateway;

        return $this;
    }

    /**
     * Get gateway.
     *
     * @return \AppBundle\Entity\EmsGatewayMaster
     */
    public function getGateway()
    {
        return $this->gateway;
    }
}
