<?php

namespace AppBundle\Entity;

/**
 * EmsHardwareInfoList
 */
class EmsHardwareInfoList
{
    /**
     * @var integer
     */
    private $hwId;

    /**
     * @var string
     */
    private $hwName;

    /**
     * @var string
     */
    private $hwMaker;


    /**
     * Get hwId
     *
     * @return integer
     */
    public function getHwId()
    {
        return $this->hwId;
    }

    /**
     * Set hwName
     *
     * @param string $hwName
     *
     * @return EmsHardwareInfoList
     */
    public function setHwName($hwName)
    {
        $this->hwName = $hwName;

        return $this;
    }

    /**
     * Get hwName
     *
     * @return string
     */
    public function getHwName()
    {
        return $this->hwName;
    }

    /**
     * Set hwMaker
     *
     * @param string $hwMaker
     *
     * @return EmsHardwareInfoList
     */
    public function setHwMaker($hwMaker)
    {
        $this->hwMaker = $hwMaker;

        return $this;
    }

    /**
     * Get hwMaker
     *
     * @return string
     */
    public function getHwMaker()
    {
        return $this->hwMaker;
    }
}
