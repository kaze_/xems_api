<?php

namespace AppBundle\Entity;

/**
 * EmsIothubMapping
 */
class EmsIothubMapping
{
    /**
     * @var string
     */
    private $circuitId;

    /**
     * @var string|null
     */
    private $iothubCircuitId;

    /**
     * @var int|null
     */
    private $maxId;

    /**
     * Get circuitId.
     *
     * @return string
     */
    public function getCircuitId()
    {
        return $this->circuitId;
    }

    /**
     * Set iothubCircuitId.
     *
     * @param string|null $iothubCircuitId
     *
     * @return EmsIothubMapping
     */
    public function setIothubCircuitId($iothubCircuitId = null)
    {
        $this->iothubCircuitId = $iothubCircuitId;

        return $this;
    }

    /**
     * Get iothubCircuitId.
     *
     * @return string|null
     */
    public function getIothubCircuitId()
    {
        return $this->iothubCircuitId;
    }

    /**
     * Set maxId.
     *
     * @param int|null $maxId
     *
     * @return EmsIothubMapping
     */
    public function setMaxId($maxId = null)
    {
        $this->maxId = $maxId;

        return $this;
    }

    /**
     * Get maxId.
     *
     * @return int|null
     */
    public function getMaxId()
    {
        return $this->maxId;
    }
    /**
     * @var string|null
     */
    private $updateAt;

    /**
     * @var string|null
     */
    private $createAt;


    /**
     * Set updateAt.
     *
     * @param string|null $updateAt
     *
     * @return EmsIothubMapping
     */
    public function setUpdateAt($updateAt = null)
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    /**
     * Get updateAt.
     *
     * @return string|null
     */
    public function getUpdateAt()
    {
        return $this->updateAt;
    }

    /**
     * Set createAt.
     *
     * @param string|null $createAt
     *
     * @return EmsIothubMapping
     */
    public function setCreateAt($createAt = null)
    {
        $this->createAt = $createAt;

        return $this;
    }

    /**
     * Get createAt.
     *
     * @return string|null
     */
    public function getCreateAt()
    {
        return $this->createAt;
    }
    /**
     * @var string|null
     */
    private $connection;


    /**
     * Set connection.
     *
     * @param string|null $connection
     *
     * @return EmsIothubMapping
     */
    public function setConnection($connection = null)
    {
        $this->connection = $connection;

        return $this;
    }

    /**
     * Get connection.
     *
     * @return string|null
     */
    public function getConnection()
    {
        return $this->connection;
    }
    /**
     * @var string|null
     */
    private $entity;


    /**
     * Set entity.
     *
     * @param string|null $entity
     *
     * @return EmsIothubMapping
     */
    public function setEntity($entity = null)
    {
        $this->entity = $entity;

        return $this;
    }

    /**
     * Get entity.
     *
     * @return string|null
     */
    public function getEntity()
    {
        return $this->entity;
    }
}
