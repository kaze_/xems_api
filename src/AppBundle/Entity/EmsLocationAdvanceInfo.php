<?php

namespace AppBundle\Entity;

/**
 * EmsLocationAdvanceInfo
 */
class EmsLocationAdvanceInfo
{
    /**
     * @var integer
     */
    private $chargingStationQty;

    /**
     * @var integer
     */
    private $locationLength;

    /**
     * @var integer
     */
    private $locationWidth;

    /**
     * @var integer
     */
    private $locationHigh;

    /**
     * @var integer
     */
    private $locationEmployee;

    /**
     * @var integer
     */
    private $locationCustomer;

    /**
     * @var \AppBundle\Entity\EmsLocationMaster
     */
    private $location;


    /**
     * Set chargingStationQty
     *
     * @param integer $chargingStationQty
     *
     * @return EmsLocationAdvanceInfo
     */
    public function setChargingStationQty($chargingStationQty)
    {
        $this->chargingStationQty = $chargingStationQty;

        return $this;
    }

    /**
     * Get chargingStationQty
     *
     * @return integer
     */
    public function getChargingStationQty()
    {
        return $this->chargingStationQty;
    }

    /**
     * Set locationLength
     *
     * @param integer $locationLength
     *
     * @return EmsLocationAdvanceInfo
     */
    public function setLocationLength($locationLength)
    {
        $this->locationLength = $locationLength;

        return $this;
    }

    /**
     * Get locationLength
     *
     * @return integer
     */
    public function getLocationLength()
    {
        return $this->locationLength;
    }

    /**
     * Set locationWidth
     *
     * @param integer $locationWidth
     *
     * @return EmsLocationAdvanceInfo
     */
    public function setLocationWidth($locationWidth)
    {
        $this->locationWidth = $locationWidth;

        return $this;
    }

    /**
     * Get locationWidth
     *
     * @return integer
     */
    public function getLocationWidth()
    {
        return $this->locationWidth;
    }

    /**
     * Set locationHigh
     *
     * @param integer $locationHigh
     *
     * @return EmsLocationAdvanceInfo
     */
    public function setLocationHigh($locationHigh)
    {
        $this->locationHigh = $locationHigh;

        return $this;
    }

    /**
     * Get locationHigh
     *
     * @return integer
     */
    public function getLocationHigh()
    {
        return $this->locationHigh;
    }

    /**
     * Set locationEmployee
     *
     * @param integer $locationEmployee
     *
     * @return EmsLocationAdvanceInfo
     */
    public function setLocationEmployee($locationEmployee)
    {
        $this->locationEmployee = $locationEmployee;

        return $this;
    }

    /**
     * Get locationEmployee
     *
     * @return integer
     */
    public function getLocationEmployee()
    {
        return $this->locationEmployee;
    }

    /**
     * Set locationCustomer
     *
     * @param integer $locationCustomer
     *
     * @return EmsLocationAdvanceInfo
     */
    public function setLocationCustomer($locationCustomer)
    {
        $this->locationCustomer = $locationCustomer;

        return $this;
    }

    /**
     * Get locationCustomer
     *
     * @return integer
     */
    public function getLocationCustomer()
    {
        return $this->locationCustomer;
    }

    /**
     * Set location
     *
     * @param \AppBundle\Entity\EmsLocationMaster $location
     *
     * @return EmsLocationAdvanceInfo
     */
    public function setLocation(\AppBundle\Entity\EmsLocationMaster $location = null)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return \AppBundle\Entity\EmsLocationMaster
     */
    public function getLocation()
    {
        return $this->location;
    }
}
