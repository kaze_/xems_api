<?php

namespace AppBundle\Entity;

/**
 * EmsLocationEspMapping
 */
class EmsLocationEspMapping
{
    /**
     * @var integer
     */
    private $energyPackageId;

    /**
     * @var \DateTime
     */
    private $energyMappingDate;

    /**
     * @var integer
     */
    private $locationEnergyMappingStatus;

    /**
     * @var integer
     */
    private $esp;

    /**
     * @var \AppBundle\Entity\EmsLocationMaster
     */
    private $location;

    /**
     * @var \AppBundle\Entity\EspTaipowerIdMaster
     */
    private $taipower;


    /**
     * Set energyPackageId
     *
     * @param integer $energyPackageId
     *
     * @return EmsLocationEspMapping
     */
    public function setEnergyPackageId($energyPackageId)
    {
        $this->energyPackageId = $energyPackageId;

        return $this;
    }

    /**
     * Get energyPackageId
     *
     * @return integer
     */
    public function getEnergyPackageId()
    {
        return $this->energyPackageId;
    }

    /**
     * Set energyMappingDate
     *
     * @param \DateTime $energyMappingDate
     *
     * @return EmsLocationEspMapping
     */
    public function setEnergyMappingDate($energyMappingDate)
    {
        $this->energyMappingDate = $energyMappingDate;

        return $this;
    }

    /**
     * Get energyMappingDate
     *
     * @return \DateTime
     */
    public function getEnergyMappingDate()
    {
        return $this->energyMappingDate;
    }

    /**
     * Set locationEnergyMappingStatus
     *
     * @param integer $locationEnergyMappingStatus
     *
     * @return EmsLocationEspMapping
     */
    public function setLocationEnergyMappingStatus($locationEnergyMappingStatus)
    {
        $this->locationEnergyMappingStatus = $locationEnergyMappingStatus;

        return $this;
    }

    /**
     * Get locationEnergyMappingStatus
     *
     * @return integer
     */
    public function getLocationEnergyMappingStatus()
    {
        return $this->locationEnergyMappingStatus;
    }

    /**
     * Set esp
     *
     * @param integer $esp
     *
     * @return EmsLocationEspMapping
     */
    public function setEsp($esp)
    {
        $this->esp = $esp;

        return $this;
    }

    /**
     * Get esp
     *
     * @return integer
     */
    public function getEsp()
    {
        return $this->esp;
    }

    /**
     * Set location
     *
     * @param \AppBundle\Entity\EmsLocationMaster $location
     *
     * @return EmsLocationEspMapping
     */
    public function setLocation(\AppBundle\Entity\EmsLocationMaster $location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return \AppBundle\Entity\EmsLocationMaster
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set taipower
     *
     * @param \AppBundle\Entity\EspTaipowerIdMaster $taipower
     *
     * @return EmsLocationEspMapping
     */
    public function setTaipower(\AppBundle\Entity\EspTaipowerIdMaster $taipower)
    {
        $this->taipower = $taipower;

        return $this;
    }

    /**
     * Get taipower
     *
     * @return \AppBundle\Entity\EspTaipowerIdMaster
     */
    public function getTaipower()
    {
        return $this->taipower;
    }
}

