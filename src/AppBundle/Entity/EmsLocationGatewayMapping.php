<?php

namespace AppBundle\Entity;

use Ramsey\Uuid\Uuid;

/**
 * EmsLocationGatewayMapping
 */
class EmsLocationGatewayMapping
{
    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var integer
     */
    private $statusId;

    /**
     * @var \AppBundle\Entity\EmsGatewayMaster
     */
    private $gateway;

    /**
     * @var \AppBundle\Entity\EmsLocationMaster
     */
    private $location;

    public function __construct()
    {
        $this->date = new \DateTime();
    }
    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return EmsLocationGatewayMapping
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set statusId
     *
     * @param integer $statusId
     *
     * @return EmsLocationGatewayMapping
     */
    public function setStatusId($statusId)
    {
        $this->statusId = $statusId;

        return $this;
    }

    /**
     * Get statusId
     *
     * @return integer
     */
    public function getStatusId()
    {
        return $this->statusId;
    }

    /**
     * Set gateway
     *
     * @param \AppBundle\Entity\EmsGatewayMaster $gateway
     *
     * @return EmsLocationGatewayMapping
     */
    public function setGateway(\AppBundle\Entity\EmsGatewayMaster $gateway = null)
    {
        $this->gateway = $gateway;

        return $this;
    }

    /**
     * Get gateway
     *
     * @return \AppBundle\Entity\EmsGatewayMaster
     */
    public function getGateway()
    {
        return $this->gateway;
    }

    /**
     * Set location
     *
     * @param \AppBundle\Entity\EmsLocationMaster $location
     *
     * @return EmsLocationGatewayMapping
     */
    public function setLocation(\AppBundle\Entity\EmsLocationMaster $location = null)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return \AppBundle\Entity\EmsLocationMaster
     */
    public function getLocation()
    {
        return $this->location;
    }
    /**
     * @var uuid
     */
    private $locationId;

    /**
     * @var uuid
     */
    private $gatewayId;


    /**
     * Set locationId
     *
     * @param uuid $locationId
     *
     * @return EmsLocationGatewayMapping
     */
    public function setLocationId($locationId)
    {
        $this->locationId = $locationId;

        return $this;
    }

    /**
     * Get locationId
     *
     * @return uuid
     */
    public function getLocationId()
    {
        return $this->locationId;
    }

    /**
     * Set gatewayId
     *
     * @param uuid $gatewayId
     *
     * @return EmsLocationGatewayMapping
     */
    public function setGatewayId($gatewayId)
    {
        $this->gatewayId = $gatewayId;

        return $this;
    }

    /**
     * Get gatewayId
     *
     * @return uuid
     */
    public function getGatewayId()
    {
        return $this->gatewayId;
    }
}
