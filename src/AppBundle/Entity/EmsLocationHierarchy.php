<?php

namespace AppBundle\Entity;

/**
 * EmsLocationHierarchy
 */
class EmsLocationHierarchy
{
    /**
     * @var integer
     */
    private $locationHierarchyId;

    public function __toString()
    {
        return $this->getLocationHierarchyId()->toString();
    }

    /**
     * Get locationHierarchyId
     *
     * @return integer
     */
    public function getLocationHierarchyId()
    {
        return $this->locationHierarchyId;
    }
}
