<?php

namespace AppBundle\Entity;

use Ramsey\Uuid\Uuid;

/**
 * EmsLocationMaster
 */
class EmsLocationMaster
{
    /**
     * @var Uuid
     */
    private $locationId;

    /**
     * @var string
     */
    private $locationName;

    /**
     * @var string
     */
    private $locationAddress;

    /**
     * @var string
     */
    private $countryZip;

    /**
     * @var integer
     */
    private $locationCountryId;

    /**
     * @var integer
     */
    private $locationStatus;

    /**
     * @var \DateTime
     */
    private $locationCreateDatetime;

    /**
     * @var \DateTime
     */
    private $locationEnableDatetime;

    /**
     * @var string
     */
    private $gpsN;

    /**
     * @var string
     */
    private $gpsE;

    /**
     * @var \AppBundle\Entity\EmsLocationHierarchy
     */
    private $locationHierarchy;

    /**
     * @var \AppBundle\Entity\EmsLocationType
     */
    private $locationType;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $taipower;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->locationCreateDatetime = new \DateTime();
        $this->locationEnableDatetime = new \DateTime();
    }

    /**
     * Get locationId
     *
     * @return string
     */
    public function getLocationId()
    {
        return $this->locationId;
    }

    /**
     * Set locationName
     *
     * @param string $locationName
     *
     * @return EmsLocationMaster
     */
    public function setLocationName($locationName)
    {
        $this->locationName = $locationName;

        return $this;
    }

    /**
     * Get locationName
     *
     * @return string
     */
    public function getLocationName()
    {
        return $this->locationName;
    }

    /**
     * Set locationAddress
     *
     * @param string $locationAddress
     *
     * @return EmsLocationMaster
     */
    public function setLocationAddress($locationAddress)
    {
        $this->locationAddress = $locationAddress;

        return $this;
    }

    /**
     * Get locationAddress
     *
     * @return string
     */
    public function getLocationAddress()
    {
        return $this->locationAddress;
    }

    /**
     * Set countryZip
     *
     * @param string $countryZip
     *
     * @return EmsLocationMaster
     */
    public function setCountryZip($countryZip)
    {
        $this->countryZip = $countryZip;

        return $this;
    }

    /**
     * Get countryZip
     *
     * @return string
     */
    public function getCountryZip()
    {
        return $this->countryZip;
    }

    /**
     * Set locationCountryId
     *
     * @param integer $locationCountryId
     *
     * @return EmsLocationMaster
     */
    public function setLocationCountryId($locationCountryId)
    {
        $this->locationCountryId = $locationCountryId;

        return $this;
    }

    /**
     * Get locationCountryId
     *
     * @return integer
     */
    public function getLocationCountryId()
    {
        return $this->locationCountryId;
    }

    /**
     * Set locationStatus
     *
     * @param integer $locationStatus
     *
     * @return EmsLocationMaster
     */
    public function setLocationStatus($locationStatus)
    {
        $this->locationStatus = $locationStatus;

        return $this;
    }

    /**
     * Get locationStatus
     *
     * @return integer
     */
    public function getLocationStatus()
    {
        return $this->locationStatus;
    }

    /**
     * Set locationCreateDatetime
     *
     * @param \DateTime $locationCreateDatetime
     *
     * @return EmsLocationMaster
     */
    public function setLocationCreateDatetime($locationCreateDatetime)
    {
        $this->locationCreateDatetime = $locationCreateDatetime;

        return $this;
    }

    /**
     * Get locationCreateDatetime
     *
     * @return \DateTime
     */
    public function getLocationCreateDatetime()
    {
        return $this->locationCreateDatetime;
    }

    /**
     * Set locationEnableDatetime
     *
     * @param \DateTime $locationEnableDatetime
     *
     * @return EmsLocationMaster
     */
    public function setLocationEnableDatetime($locationEnableDatetime)
    {
        $this->locationEnableDatetime = $locationEnableDatetime;

        return $this;
    }

    /**
     * Get locationEnableDatetime
     *
     * @return \DateTime
     */
    public function getLocationEnableDatetime()
    {
        return $this->locationEnableDatetime;
    }

    /**
     * Set gpsN
     *
     * @param string $gpsN
     *
     * @return EmsLocationMaster
     */
    public function setGpsN($gpsN)
    {
        $this->gpsN = $gpsN;

        return $this;
    }

    /**
     * Get gpsN
     *
     * @return string
     */
    public function getGpsN()
    {
        return $this->gpsN;
    }

    /**
     * Set gpsE
     *
     * @param string $gpsE
     *
     * @return EmsLocationMaster
     */
    public function setGpsE($gpsE)
    {
        $this->gpsE = $gpsE;

        return $this;
    }

    /**
     * Get gpsE
     *
     * @return string
     */
    public function getGpsE()
    {
        return $this->gpsE;
    }

    /**
     * Set locationHierarchy
     *
     * @param \AppBundle\Entity\EmsLocationHierarchy $locationHierarchy
     *
     * @return EmsLocationMaster
     */
    public function setLocationHierarchy(\AppBundle\Entity\EmsLocationHierarchy $locationHierarchy = null)
    {
        $this->locationHierarchy = $locationHierarchy;

        return $this;
    }

    /**
     * Get locationHierarchy
     *
     * @return \AppBundle\Entity\EmsLocationHierarchy
     */
    public function getLocationHierarchy()
    {
        return $this->locationHierarchy;
    }

    /**
     * Set locationType
     *
     * @param \AppBundle\Entity\EmsLocationType $locationType
     *
     * @return EmsLocationMaster
     */
    public function setLocationType(\AppBundle\Entity\EmsLocationType $locationType = null)
    {
        $this->locationType = $locationType;

        return $this;
    }

    /**
     * Get locationType
     *
     * @return \AppBundle\Entity\EmsLocationType
     */
    public function getLocationType()
    {
        return $this->locationType;
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $espCustomer;


    /**
     * Add espCustomer
     *
     * @param \EspBundle\Entity\EspCustomerMaster $espCustomer
     *
     * @return EmsLocationMaster
     */
    public function addEspCustomer(\EspBundle\Entity\EspCustomerMaster $espCustomer)
    {
        $this->espCustomer[] = $espCustomer;

        return $this;
    }

    /**
     * Remove espCustomer
     *
     * @param \EspBundle\Entity\EspCustomerMaster $espCustomer
     */
    public function removeEspCustomer(\EspBundle\Entity\EspCustomerMaster $espCustomer)
    {
        $this->espCustomer->removeElement($espCustomer);
    }

    /**
     * Get espCustomer
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEspCustomer()
    {
        return $this->espCustomer;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $customer;


    /**
     * Add customer
     *
     * @param \AppBundle\Entity\EmsCustomerMaster $customer
     *
     * @return EmsLocationMaster
     */
    public function addCustomer(\AppBundle\Entity\EmsCustomerMaster $customer)
    {
        $this->customer[] = $customer;

        return $this;
    }

    /**
     * Remove customer
     *
     * @param \AppBundle\Entity\EmsCustomerMaster $customer
     */
    public function removeCustomer(\AppBundle\Entity\EmsCustomerMaster $customer)
    {
        $this->customer->removeElement($customer);
    }

    /**
     * Get customer
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    public function getValidCustomer()
    {
        if ($this->customer != null  ) {
            return $this->customer[0];
        }
        return null;
    }
}
