<?php

namespace AppBundle\Entity;

/**
 * EmsLocationType
 */
class EmsLocationType
{
    /**
     * @var integer
     */
    private $locationTypeId;

    /**
     * @var string
     */
    private $locationTypeName;


    public function __toString()
    {
        return $this->getLocationTypeName();
    }

    /**
     * Get locationTypeId
     *
     * @return integer
     */
    public function getLocationTypeId()
    {
        return $this->locationTypeId;
    }

    /**
     * Set locationTypeName
     *
     * @param string $locationTypeName
     *
     * @return EmsLocationType
     */
    public function setLocationTypeName($locationTypeName)
    {
        $this->locationTypeName = $locationTypeName;

        return $this;
    }

    /**
     * Get locationTypeName
     *
     * @return string
     */
    public function getLocationTypeName()
    {
        return $this->locationTypeName;
    }
}
