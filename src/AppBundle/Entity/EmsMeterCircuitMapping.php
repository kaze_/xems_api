<?php

namespace AppBundle\Entity;

/**
 * EmsMeterCircuitMapping
 */
class EmsMeterCircuitMapping
{
    /**
     * @var integer
     */
    private $channel;


    public function __construct()
    {
        $this->date = new \DateTime();
    }
    
    
    /**
     * Set channel
     *
     * @param integer $channel
     *
     * @return EmsMeterCircuitMapping
     */
    public function setChannel($channel)
    {
        $this->channel = $channel;

        return $this;
    }

    /**
     * Get channel
     *
     * @return integer
     */
    public function getChannel()
    {
        return $this->channel;
    }
    /**
     * @var \AppBundle\Entity\EmsPowerMeter
     */
    private $device;

    /**
     * @var \AppBundle\Entity\EmsCircuitMaster
     */
    private $circuit;


    /**
     * Set device
     *
     * @param \AppBundle\Entity\EmsPowerMeter $device
     *
     * @return EmsMeterCircuitMapping
     */
    public function setDevice(\AppBundle\Entity\EmsPowerMeter $device)
    {
        $this->device = $device;

        return $this;
    }

    /**
     * Get device
     *
     * @return \AppBundle\Entity\EmsPowerMeter
     */
    public function getDevice()
    {
        return $this->device;
    }

    /**
     * Set circuit
     *
     * @param \AppBundle\Entity\EmsCircuitMaster $circuit
     *
     * @return EmsMeterCircuitMapping
     */
    public function setCircuit(\AppBundle\Entity\EmsCircuitMaster $circuit)
    {
        $this->circuit = $circuit;

        return $this;
    }

    /**
     * Get circuit
     *
     * @return \AppBundle\Entity\EmsCircuitMaster
     */
    public function getCircuit()
    {
        return $this->circuit;
    }
    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var int
     */
    private $statusId;


    /**
     * Set date.
     *
     * @param \DateTime $date
     *
     * @return EmsMeterCircuitMapping
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date.
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set statusId.
     *
     * @param int $statusId
     *
     * @return EmsMeterCircuitMapping
     */
    public function setStatusId($statusId)
    {
        $this->statusId = $statusId;

        return $this;
    }

    /**
     * Get statusId.
     *
     * @return int
     */
    public function getStatusId()
    {
        return $this->statusId;
    }
}
