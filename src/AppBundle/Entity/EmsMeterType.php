<?php

namespace AppBundle\Entity;

/**
 * EmsMeterType
 */
class EmsMeterType
{
    /**
     * @var integer
     */
    private $meterTypeId;

    /**
     * @var string
     */
    private $meterName;

    /**
     * @var string
     */
    private $meterModelName;

    /**
     * @var float
     */
    private $meterClassic;


    /**
     * Get meterTypeId
     *
     * @return integer
     */
    public function getMeterTypeId()
    {
        return $this->meterTypeId;
    }

    /**
     * Set meterName
     *
     * @param string $meterName
     *
     * @return EmsMeterType
     */
    public function setMeterName($meterName)
    {
        $this->meterName = $meterName;

        return $this;
    }

    /**
     * Get meterName
     *
     * @return string
     */
    public function getMeterName()
    {
        return $this->meterName;
    }

    /**
     * Set meterModelName
     *
     * @param string $meterModelName
     *
     * @return EmsMeterType
     */
    public function setMeterModelName($meterModelName)
    {
        $this->meterModelName = $meterModelName;

        return $this;
    }

    /**
     * Get meterModelName
     *
     * @return string
     */
    public function getMeterModelName()
    {
        return $this->meterModelName;
    }

    /**
     * Set meterClassic
     *
     * @param float $meterClassic
     *
     * @return EmsMeterType
     */
    public function setMeterClassic($meterClassic)
    {
        $this->meterClassic = $meterClassic;

        return $this;
    }

    /**
     * Get meterClassic
     *
     * @return float
     */
    public function getMeterClassic()
    {
        return $this->meterClassic;
    }
    /**
     * @var string
     */
    private $channel;


    /**
     * Set channel
     *
     * @param string $channel
     *
     * @return EmsMeterType
     */
    public function setChannel($channel)
    {
        $this->channel = $channel;

        return $this;
    }

    /**
     * Get channel
     *
     * @return string
     */
    public function getChannel()
    {
        return $this->channel;
    }
}
