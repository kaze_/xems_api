<?php

namespace AppBundle\Entity;

/**
 * EmsModbusGateway
 */
class EmsModbusGateway
{
    /**
     * @var string
     */
    private $ipAddress;

    /**
     * @var string
     */
    private $account;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $model;

    /**
     * @var integer
     */
    private $boudRate;

    /**
     * @var string
     */
    private $modbusGwDesc;

    /**
     * @var string
     */
    private $emsDeviceMastergatewayId;

    /**
     * @var \AppBundle\Entity\EmsDeviceMaster
     */
    private $device;


    /**
     * Set ipAddress
     *
     * @param string $ipAddress
     *
     * @return EmsModbusGateway
     */
    public function setIpAddress($ipAddress)
    {
        $this->ipAddress = $ipAddress;

        return $this;
    }

    /**
     * Get ipAddress
     *
     * @return string
     */
    public function getIpAddress()
    {
        return $this->ipAddress;
    }

    /**
     * Set account
     *
     * @param string $account
     *
     * @return EmsModbusGateway
     */
    public function setAccount($account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return string
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return EmsModbusGateway
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set model
     *
     * @param string $model
     *
     * @return EmsModbusGateway
     */
    public function setModel($model)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * Get model
     *
     * @return string
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Set boudRate
     *
     * @param integer $boudRate
     *
     * @return EmsModbusGateway
     */
    public function setBoudRate($boudRate)
    {
        $this->boudRate = $boudRate;

        return $this;
    }

    /**
     * Get boudRate
     *
     * @return integer
     */
    public function getBoudRate()
    {
        return $this->boudRate;
    }

    /**
     * Set modbusGwDesc
     *
     * @param string $modbusGwDesc
     *
     * @return EmsModbusGateway
     */
    public function setModbusGwDesc($modbusGwDesc)
    {
        $this->modbusGwDesc = $modbusGwDesc;

        return $this;
    }

    /**
     * Get modbusGwDesc
     *
     * @return string
     */
    public function getModbusGwDesc()
    {
        return $this->modbusGwDesc;
    }

    /**
     * Set device
     *
     * @param \AppBundle\Entity\EmsDeviceMaster $device
     *
     * @return EmsModbusGateway
     */
    public function setDevice(\AppBundle\Entity\EmsDeviceMaster $device = null)
    {
        $this->device = $device;

        return $this;
    }

    /**
     * Get device
     *
     * @return \AppBundle\Entity\EmsDeviceMaster
     */
    public function getDevice()
    {
        return $this->device;
    }


    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $powerMeters;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->powerMeters = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add powerMeter
     *
     * @param \AppBundle\Entity\EmsPowerMeter $powerMeter
     *
     * @return EmsModbusGateway
     */
    public function addPowerMeter(\AppBundle\Entity\EmsPowerMeter $powerMeter)
    {
        $this->powerMeters[] = $powerMeter;

        return $this;
    }

    /**
     * Remove powerMeter
     *
     * @param \AppBundle\Entity\EmsPowerMeter $powerMeter
     */
    public function removePowerMeter(\AppBundle\Entity\EmsPowerMeter $powerMeter)
    {
        $this->powerMeters->removeElement($powerMeter);
    }

    /**
     * Get powerMeters
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPowerMeters()
    {
        return $this->powerMeters;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $sensors;


    /**
     * Add sensor.
     *
     * @param \AppBundle\Entity\EmsSensor $sensor
     *
     * @return EmsModbusGateway
     */
    public function addSensor(\AppBundle\Entity\EmsSensor $sensor)
    {
        $this->sensors[] = $sensor;

        return $this;
    }

    /**
     * Remove sensor.
     *
     * @param \AppBundle\Entity\EmsSensor $sensor
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeSensor(\AppBundle\Entity\EmsSensor $sensor)
    {
        return $this->sensors->removeElement($sensor);
    }

    /**
     * Get sensors.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSensors()
    {
        return $this->sensors;
    }
}
