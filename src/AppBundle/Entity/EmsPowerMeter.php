<?php

namespace AppBundle\Entity;

/**
 * EmsPowerMeter
 */
class EmsPowerMeter
{
    /**
     * @var integer
     */
    private $modbusId;

    /**
     * @var integer
     */
    private $boudRate;

    /**
     * @var string
     */
    private $connectDeviceId;

    /**
     * @var integer
     */
    private $connectDeviceType;

    /**
     * @var \AppBundle\Entity\EmsDeviceMaster
     */
    private $device;

    /**
     * @var \AppBundle\Entity\EmsVoltage
     */
    private $voltage;

    /**
     * @var \AppBundle\Entity\EmsModbusGateway
     */
    private $modbusGateway;

    /**
     * @var \AppBundle\Entity\EmsMeterType
     */
    private $meterType;

    /**
     * @var integer
     */
    private $ctRatio;

    /**
     * @var integer
     */
    private $logInterval;

//    private $meterTypeId;
//    private $deviceId;
    private $modbusGatewayId;

    /**
     * Set modbusId
     *
     * @param integer $modbusId
     *
     * @return EmsPowerMeter
     */
    public function setModbusId($modbusId)
    {
        $this->modbusId = $modbusId;

        return $this;
    }

    /**
     * Get modbusId
     *
     * @return integer
     */
    public function getModbusId()
    {
        return $this->modbusId;
    }

    /**
     * Set boudRate
     *
     * @param integer $boudRate
     *
     * @return EmsPowerMeter
     */
    public function setBoudRate($boudRate)
    {
        $this->boudRate = $boudRate;

        return $this;
    }

    /**
     * Get boudRate
     *
     * @return integer
     */
    public function getBoudRate()
    {
        return $this->boudRate;
    }

    /**
     * Set connectDeviceId
     *
     * @param string $connectDeviceId
     *
     * @return EmsPowerMeter
     */
    public function setConnectDeviceId($connectDeviceId)
    {
        $this->connectDeviceId = $connectDeviceId;

        return $this;
    }

    /**
     * Get connectDeviceId
     *
     * @return string
     */
    public function getConnectDeviceId()
    {
        return $this->connectDeviceId;
    }

    /**
     * Set connectDeviceType
     *
     * @param integer $connectDeviceType
     *
     * @return EmsPowerMeter
     */
    public function setConnectDeviceType($connectDeviceType)
    {
        $this->connectDeviceType = $connectDeviceType;

        return $this;
    }

    /**
     * Get connectDeviceType
     *
     * @return integer
     */
    public function getConnectDeviceType()
    {
        return $this->connectDeviceType;
    }

    /**
     * Set device
     *
     * @param \AppBundle\Entity\EmsDeviceMaster $device
     *
     * @return EmsPowerMeter
     */
    public function setDevice(\AppBundle\Entity\EmsDeviceMaster $device = null)
    {
        $this->device = $device;
        if ( $device != null ) {
            $this->deviceId = $device->getDeviceId();
        }

        return $this;
    }

    /**
     * Get device
     *
     * @return \AppBundle\Entity\EmsDeviceMaster
     */
    public function getDevice()
    {
        return $this->device;
    }

    /**
     * Set voltage
     *
     * @param \AppBundle\Entity\EmsVoltage $voltage
     *
     * @return EmsPowerMeter
     */
    public function setVoltage(\AppBundle\Entity\EmsVoltage $voltage = null)
    {
        $this->voltage = $voltage;

        return $this;
    }

    /**
     * Get voltage
     *
     * @return \AppBundle\Entity\EmsVoltage
     */
    public function getVoltage()
    {
        return $this->voltage;
    }

    /**
     * Set modbusGateway
     *
     * @param \AppBundle\Entity\EmsModbusGateway $modbusGateway
     *
     * @return EmsPowerMeter
     */
    public function setModbusGateway(\AppBundle\Entity\EmsModbusGateway $modbusGateway = null)
    {
        $this->modbusGateway = $modbusGateway;
        if ( $modbusGateway != null ) {
            $this->modbusGatewayId = $modbusGateway->getDevice()->getDeviceId();
        }

        return $this;
    }

    /**
     * Get modbusGateway
     *
     * @return \AppBundle\Entity\EmsModbusGateway
     */
    public function getModbusGateway()
    {
        return $this->modbusGateway;
    }

    public function getModbusGatewayId()
    {
        return $this->modbusGatewayId;
    }

    /**
     * Set meterType
     *
     * @param \AppBundle\Entity\EmsMeterType $meterType
     *
     * @return EmsPowerMeter
     */
    public function setMeterType(\AppBundle\Entity\EmsMeterType $meterType = null)
    {
        $this->meterType = $meterType;
//        if ($meterType) {
//            $this->meterTypeId = $meterType->getMeterTypeId();
//        }
        return $this;
    }

    /**
     * Get meterType
     *
     * @return \AppBundle\Entity\EmsMeterType
     */
    public function getMeterType()
    {
        return $this->meterType;
    }

//    public function getDeviceId()
//    {
//        return $this->deviceId;
//    }

//    public function getMeterTypeId()
//    {
//        return $this->meterTypeId;
//    }

    /**
     * Set ctRatio
     *
     * @param integer $ctRatio
     *
     * @return EmsPowerMeter
     */
    public function setCtRatio($ctRatio)
    {
        $this->ctRatio = $ctRatio;

        return $this;
    }

    /**
     * Get ctRatio
     *
     * @return integer
     */
    public function getCtRatio()
    {
        return $this->ctRatio;
    }

    /**
     * Set logInterval
     *
     * @param integer $logInterval
     *
     * @return EmsPowerMeter
     */
    public function setLogInterval($logInterval)
    {
        $this->logInterval = $logInterval;

        return $this;
    }

    /**
     * Get logInterval
     *
     * @return integer
     */
    public function getLogInterval()
    {
        return $this->logInterval;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $circuits;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->circuits = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add circuit
     *
     * @param \AppBundle\Entity\EmsCircuitMaster $circuit
     *
     * @return EmsPowerMeter
     */
    public function addCircuit(\AppBundle\Entity\EmsCircuitMaster $circuit)
    {
        $this->circuits[] = $circuit;

        return $this;
    }

    /**
     * Remove circuit
     *
     * @param \AppBundle\Entity\EmsCircuitMaster $circuit
     */
    public function removeCircuit(\AppBundle\Entity\EmsCircuitMaster $circuit)
    {
        $this->circuits->removeElement($circuit);
    }

    /**
     * Get circuits
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCircuits()
    {
        return $this->circuits;
    }
    /**
     * @var int|null
     */
    private $ptRatio;


    /**
     * Set ptRatio.
     *
     * @param int|null $ptRatio
     *
     * @return EmsPowerMeter
     */
    public function setPtRatio($ptRatio = null)
    {
        $this->ptRatio = $ptRatio;

        return $this;
    }

    /**
     * Get ptRatio.
     *
     * @return int|null
     */
    public function getPtRatio()
    {
        return $this->ptRatio;
    }
    /**
     * @var int|null
     */
    private $monitorInterval;


    /**
     * Set monitorInterval.
     *
     * @param int|null $monitorInterval
     *
     * @return EmsPowerMeter
     */
    public function setMonitorInterval($monitorInterval = null)
    {
        $this->monitorInterval = $monitorInterval;

        return $this;
    }

    /**
     * Get monitorInterval.
     *
     * @return int|null
     */
    public function getMonitorInterval()
    {
        return $this->monitorInterval;
    }
}
