<?php

namespace AppBundle\Entity;

/**
 * EmsPowerReportLog
 */
class EmsPowerReportLog
{
    /**
     * @var \DateTime
     */
    private $reportDatetime;

    /**
     * @var float
     */
    private $vA;

    /**
     * @var float
     */
    private $vB;

    /**
     * @var float
     */
    private $vC;

    /**
     * @var float
     */
    private $vAverage;

    /**
     * @var float
     */
    private $aA;

    /**
     * @var float
     */
    private $aB;

    /**
     * @var float
     */
    private $aC;

    /**
     * @var float
     */
    private $aAverage;

    /**
     * @var float
     */
    private $kwA;

    /**
     * @var float
     */
    private $kwB;

    /**
     * @var float
     */
    private $kwC;

    /**
     * @var float
     */
    private $kwTotal;

    /**
     * @var float
     */
    private $kwhA;

    /**
     * @var float
     */
    private $kwhB;

    /**
     * @var float
     */
    private $kwhC;

    /**
     * @var float
     */
    private $kwhTotal;

    /**
     * @var float
     */
    private $kvahTotal;

    /**
     * @var float
     */
    private $kvaA;

    /**
     * @var float
     */
    private $kvaB;

    /**
     * @var float
     */
    private $kvaC;

    /**
     * @var float
     */
    private $kvaTotal;

    /**
     * @var float
     */
    private $currLgstdmdKw;

    /**
     * @var integer
     */
    private $currRateId;

    /**
     * @var float
     */
    private $frequency;

    /**
     * @var float
     */
    private $pfA;

    /**
     * @var float
     */
    private $pfB;

    /**
     * @var float
     */
    private $pfC;

    /**
     * @var float
     */
    private $pfTotal;

    /**
     * @var float
     */
    private $kvarhA;

    /**
     * @var float
     */
    private $kvarhB;

    /**
     * @var float
     */
    private $kvarhC;

    /**
     * @var float
     */
    private $kvarhTotal;

    /**
     * @var float
     */
    private $kvarA;

    /**
     * @var float
     */
    private $kvarB;

    /**
     * @var float
     */
    private $kvarC;

    /**
     * @var float
     */
    private $kvarTotal;

    /**
     * @var integer
     */
    private $relayStatus;

    /**
     * @var \DateTime
     */
    private $reportTime;

    /**
     * @var float
     */
    private $touALgstdmdcontaccKw;

    /**
     * @var float
     */
    private $touATotalKwh;

    /**
     * @var float
     */
    private $touBLgstdmdcontaccKw;

    /**
     * @var float
     */
    private $touBTotalKwh;

    /**
     * @var float
     */
    private $touCLgstdmdaccKw;

    /**
     * @var float
     */
    private $touCLgstdmdcontaccKw;

    /**
     * @var float
     */
    private $touCTotalKwh;

    /**
     * @var float
     */
    private $touDLgstdmdaccKw;

    /**
     * @var float
     */
    private $touDLgstdmdcontaccKw;

    /**
     * @var float
     */
    private $touDTotalKwh;

    /**
     * @var float
     */
    private $touBLgstdmdaccKw;

    /**
     * @var float
     */
    private $touALgstdmdaccKw;

    /**
     * @var float
     */
    private $touALgstdmdKw;

    /**
     * @var float
     */
    private $touBLgstdmdKw;

    /**
     * @var float
     */
    private $touCLgstdmdKw;

    /**
     * @var float
     */
    private $touDLgstdmdKw;

    /**
     * @var float
     */
    private $currDmdKw;

    /**
     * @var integer
     */
    private $modbusId;

    /**
     * @var integer
     */
    private $currDmdRemainTime;

    /**
     * @var string
     */
    private $emsCircuitInfodeviceId;

    /**
     * @var \AppBundle\Entity\EmsCircuitMaster
     */
    private $circuit;


    /**
     * Set reportDatetime
     *
     * @param \DateTime $reportDatetime
     *
     * @return EmsPowerReportLog
     */
    public function setReportDatetime($reportDatetime)
    {
        $this->reportDatetime = $reportDatetime;

        return $this;
    }

    /**
     * Get reportDatetime
     *
     * @return \DateTime
     */
    public function getReportDatetime()
    {
        return $this->reportDatetime;
    }

    /**
     * Set vA
     *
     * @param float $vA
     *
     * @return EmsPowerReportLog
     */
    public function setVA($vA)
    {
        $this->vA = $vA;

        return $this;
    }

    /**
     * Get vA
     *
     * @return float
     */
    public function getVA()
    {
        return $this->vA;
    }

    /**
     * Set vB
     *
     * @param float $vB
     *
     * @return EmsPowerReportLog
     */
    public function setVB($vB)
    {
        $this->vB = $vB;

        return $this;
    }

    /**
     * Get vB
     *
     * @return float
     */
    public function getVB()
    {
        return $this->vB;
    }

    /**
     * Set vC
     *
     * @param float $vC
     *
     * @return EmsPowerReportLog
     */
    public function setVC($vC)
    {
        $this->vC = $vC;

        return $this;
    }

    /**
     * Get vC
     *
     * @return float
     */
    public function getVC()
    {
        return $this->vC;
    }

    /**
     * Set vAverage
     *
     * @param float $vAverage
     *
     * @return EmsPowerReportLog
     */
    public function setVAverage($vAverage)
    {
        $this->vAverage = $vAverage;
        return $this;
    }

    /**
     * Get vAverage
     *
     * @return float
     */
    public function getVAverage()
    {
        return $this->vAverage;
    }

    /**
     * Set aA
     *
     * @param float $aA
     *
     * @return EmsPowerReportLog
     */
    public function setAA($aA)
    {
        $this->aA = $aA;

        return $this;
    }

    /**
     * Get aA
     *
     * @return float
     */
    public function getAA()
    {
        return $this->aA;
    }

    /**
     * Set aB
     *
     * @param float $aB
     *
     * @return EmsPowerReportLog
     */
    public function setAB($aB)
    {
        $this->aB = $aB;

        return $this;
    }

    /**
     * Get aB
     *
     * @return float
     */
    public function getAB()
    {
        return $this->aB;
    }

    /**
     * Set aC
     *
     * @param float $aC
     *
     * @return EmsPowerReportLog
     */
    public function setAC($aC)
    {
        $this->aC = $aC;

        return $this;
    }

    /**
     * Get aC
     *
     * @return float
     */
    public function getAC()
    {
        return $this->aC;
    }

    /**
     * Set aAverage
     *
     * @param float $aAverage
     *
     * @return EmsPowerReportLog
     */
    public function setAAverage($aAverage)
    {
        $this->aAverage = $aAverage;

        return $this;
    }

    /**
     * Get aAverage
     *
     * @return float
     */
    public function getAAverage()
    {
        return $this->aAverage;
    }

    /**
     * Set kwA
     *
     * @param float $kwA
     *
     * @return EmsPowerReportLog
     */
    public function setKwA($kwA)
    {
        $this->kwA = $kwA;

        return $this;
    }

    /**
     * Get kwA
     *
     * @return float
     */
    public function getKwA()
    {
        return $this->kwA;
    }

    /**
     * Set kwB
     *
     * @param float $kwB
     *
     * @return EmsPowerReportLog
     */
    public function setKwB($kwB)
    {
        $this->kwB = $kwB;

        return $this;
    }

    /**
     * Get kwB
     *
     * @return float
     */
    public function getKwB()
    {
        return $this->kwB;
    }

    /**
     * Set kwC
     *
     * @param float $kwC
     *
     * @return EmsPowerReportLog
     */
    public function setKwC($kwC)
    {
        $this->kwC = $kwC;

        return $this;
    }

    /**
     * Get kwC
     *
     * @return float
     */
    public function getKwC()
    {
        return $this->kwC;
    }

    /**
     * Set kwTotal
     *
     * @param float $kwTotal
     *
     * @return EmsPowerReportLog
     */
    public function setKwTotal($kwTotal)
    {
        $this->kwTotal = $kwTotal;

        return $this;
    }

    /**
     * Get kwTotal
     *
     * @return float
     */
    public function getKwTotal()
    {
        return $this->kwTotal;
    }

    /**
     * Set kwhA
     *
     * @param float $kwhA
     *
     * @return EmsPowerReportLog
     */
    public function setKwhA($kwhA)
    {
        $this->kwhA = $kwhA;

        return $this;
    }

    /**
     * Get kwhA
     *
     * @return float
     */
    public function getKwhA()
    {
        return $this->kwhA;
    }

    /**
     * Set kwhB
     *
     * @param float $kwhB
     *
     * @return EmsPowerReportLog
     */
    public function setKwhB($kwhB)
    {
        $this->kwhB = $kwhB;

        return $this;
    }

    /**
     * Get kwhB
     *
     * @return float
     */
    public function getKwhB()
    {
        return $this->kwhB;
    }

    /**
     * Set kwhC
     *
     * @param float $kwhC
     *
     * @return EmsPowerReportLog
     */
    public function setKwhC($kwhC)
    {
        $this->kwhC = $kwhC;

        return $this;
    }

    /**
     * Get kwhC
     *
     * @return float
     */
    public function getKwhC()
    {
        return $this->kwhC;
    }

    /**
     * Set kwhTotal
     *
     * @param float $kwhTotal
     *
     * @return EmsPowerReportLog
     */
    public function setKwhTotal($kwhTotal)
    {
        $this->kwhTotal = $kwhTotal;

        return $this;
    }

    /**
     * Get kwhTotal
     *
     * @return float
     */
    public function getKwhTotal()
    {
        return $this->kwhTotal;
    }

    /**
     * Set kvahTotal
     *
     * @param float $kvahTotal
     *
     * @return EmsPowerReportLog
     */
    public function setKvahTotal($kvahTotal)
    {
        $this->kvahTotal = $kvahTotal;

        return $this;
    }

    /**
     * Get kvahTotal
     *
     * @return float
     */
    public function getKvahTotal()
    {
        return $this->kvahTotal;
    }

    /**
     * Set kvaA
     *
     * @param float $kvaA
     *
     * @return EmsPowerReportLog
     */
    public function setKvaA($kvaA)
    {
        $this->kvaA = $kvaA;

        return $this;
    }

    /**
     * Get kvaA
     *
     * @return float
     */
    public function getKvaA()
    {
        return $this->kvaA;
    }

    /**
     * Set kvaB
     *
     * @param float $kvaB
     *
     * @return EmsPowerReportLog
     */
    public function setKvaB($kvaB)
    {
        $this->kvaB = $kvaB;

        return $this;
    }

    /**
     * Get kvaB
     *
     * @return float
     */
    public function getKvaB()
    {
        return $this->kvaB;
    }

    /**
     * Set kvaC
     *
     * @param float $kvaC
     *
     * @return EmsPowerReportLog
     */
    public function setKvaC($kvaC)
    {
        $this->kvaC = $kvaC;

        return $this;
    }

    /**
     * Get kvaC
     *
     * @return float
     */
    public function getKvaC()
    {
        return $this->kvaC;
    }

    /**
     * Set kvaTotal
     *
     * @param float $kvaTotal
     *
     * @return EmsPowerReportLog
     */
    public function setKvaTotal($kvaTotal)
    {
        $this->kvaTotal = $kvaTotal;

        return $this;
    }

    /**
     * Get kvaTotal
     *
     * @return float
     */
    public function getKvaTotal()
    {
        return $this->kvaTotal;
    }

    /**
     * Set currLgstdmdKw
     *
     * @param float $currLgstdmdKw
     *
     * @return EmsPowerReportLog
     */
    public function setCurrLgstdmdKw($currLgstdmdKw)
    {
        $this->currLgstdmdKw = $currLgstdmdKw;

        return $this;
    }

    /**
     * Get currLgstdmdKw
     *
     * @return float
     */
    public function getCurrLgstdmdKw()
    {
        return $this->currLgstdmdKw;
    }

    /**
     * Set currRateId
     *
     * @param integer $currRateId
     *
     * @return EmsPowerReportLog
     */
    public function setCurrRateId($currRateId)
    {
        $this->currRateId = $currRateId;

        return $this;
    }

    /**
     * Get currRateId
     *
     * @return integer
     */
    public function getCurrRateId()
    {
        return $this->currRateId;
    }

    /**
     * Set frequency
     *
     * @param float $frequency
     *
     * @return EmsPowerReportLog
     */
    public function setFrequency($frequency)
    {
        $this->frequency = $frequency;

        return $this;
    }

    /**
     * Get frequency
     *
     * @return float
     */
    public function getFrequency()
    {
        return $this->frequency;
    }

    /**
     * Set pfA
     *
     * @param float $pfA
     *
     * @return EmsPowerReportLog
     */
    public function setPfA($pfA)
    {
        $this->pfA = $pfA;

        return $this;
    }

    /**
     * Get pfA
     *
     * @return float
     */
    public function getPfA()
    {
        return $this->pfA;
    }

    /**
     * Set pfB
     *
     * @param float $pfB
     *
     * @return EmsPowerReportLog
     */
    public function setPfB($pfB)
    {
        $this->pfB = $pfB;

        return $this;
    }

    /**
     * Get pfB
     *
     * @return float
     */
    public function getPfB()
    {
        return $this->pfB;
    }

    /**
     * Set pfC
     *
     * @param float $pfC
     *
     * @return EmsPowerReportLog
     */
    public function setPfC($pfC)
    {
        $this->pfC = $pfC;

        return $this;
    }

    /**
     * Get pfC
     *
     * @return float
     */
    public function getPfC()
    {
        return $this->pfC;
    }

    /**
     * Set pfTotal
     *
     * @param float $pfTotal
     *
     * @return EmsPowerReportLog
     */
    public function setPfTotal($pfTotal)
    {
        $this->pfTotal = $pfTotal;

        return $this;
    }

    /**
     * Get pfTotal
     *
     * @return float
     */
    public function getPfTotal()
    {
        return $this->pfTotal;
    }

    /**
     * Set kvarhA
     *
     * @param float $kvarhA
     *
     * @return EmsPowerReportLog
     */
    public function setKvarhA($kvarhA)
    {
        $this->kvarhA = $kvarhA;

        return $this;
    }

    /**
     * Get kvarhA
     *
     * @return float
     */
    public function getKvarhA()
    {
        return $this->kvarhA;
    }

    /**
     * Set kvarhB
     *
     * @param float $kvarhB
     *
     * @return EmsPowerReportLog
     */
    public function setKvarhB($kvarhB)
    {
        $this->kvarhB = $kvarhB;

        return $this;
    }

    /**
     * Get kvarhB
     *
     * @return float
     */
    public function getKvarhB()
    {
        return $this->kvarhB;
    }

    /**
     * Set kvarhC
     *
     * @param float $kvarhC
     *
     * @return EmsPowerReportLog
     */
    public function setKvarhC($kvarhC)
    {
        $this->kvarhC = $kvarhC;

        return $this;
    }

    /**
     * Get kvarhC
     *
     * @return float
     */
    public function getKvarhC()
    {
        return $this->kvarhC;
    }

    /**
     * Set kvarhTotal
     *
     * @param float $kvarhTotal
     *
     * @return EmsPowerReportLog
     */
    public function setKvarhTotal($kvarhTotal)
    {
        $this->kvarhTotal = $kvarhTotal;

        return $this;
    }

    /**
     * Get kvarhTotal
     *
     * @return float
     */
    public function getKvarhTotal()
    {
        return $this->kvarhTotal;
    }

    /**
     * Set kvarA
     *
     * @param float $kvarA
     *
     * @return EmsPowerReportLog
     */
    public function setKvarA($kvarA)
    {
        $this->kvarA = $kvarA;

        return $this;
    }

    /**
     * Get kvarA
     *
     * @return float
     */
    public function getKvarA()
    {
        return $this->kvarA;
    }

    /**
     * Set kvarB
     *
     * @param float $kvarB
     *
     * @return EmsPowerReportLog
     */
    public function setKvarB($kvarB)
    {
        $this->kvarB = $kvarB;

        return $this;
    }

    /**
     * Get kvarB
     *
     * @return float
     */
    public function getKvarB()
    {
        return $this->kvarB;
    }

    /**
     * Set kvarC
     *
     * @param float $kvarC
     *
     * @return EmsPowerReportLog
     */
    public function setKvarC($kvarC)
    {
        $this->kvarC = $kvarC;

        return $this;
    }

    /**
     * Get kvarC
     *
     * @return float
     */
    public function getKvarC()
    {
        return $this->kvarC;
    }

    /**
     * Set kvarTotal
     *
     * @param float $kvarTotal
     *
     * @return EmsPowerReportLog
     */
    public function setKvarTotal($kvarTotal)
    {
        $this->kvarTotal = $kvarTotal;

        return $this;
    }

    /**
     * Get kvarTotal
     *
     * @return float
     */
    public function getKvarTotal()
    {
        return $this->kvarTotal;
    }

    /**
     * Set relayStatus
     *
     * @param integer $relayStatus
     *
     * @return EmsPowerReportLog
     */
    public function setRelayStatus($relayStatus)
    {
        $this->relayStatus = $relayStatus;

        return $this;
    }

    /**
     * Get relayStatus
     *
     * @return integer
     */
    public function getRelayStatus()
    {
        return $this->relayStatus;
    }

    /**
     * Set reportTime
     *
     * @param \DateTime $reportTime
     *
     * @return EmsPowerReportLog
     */
    public function setReportTime($reportTime)
    {
        $this->reportTime = $reportTime;

        return $this;
    }

    /**
     * Get reportTime
     *
     * @return \DateTime
     */
    public function getReportTime()
    {
        return $this->reportTime;
    }

    /**
     * Set touALgstdmdcontaccKw
     *
     * @param float $touALgstdmdcontaccKw
     *
     * @return EmsPowerReportLog
     */
    public function setTouALgstdmdcontaccKw($touALgstdmdcontaccKw)
    {
        $this->touALgstdmdcontaccKw = $touALgstdmdcontaccKw;

        return $this;
    }

    /**
     * Get touALgstdmdcontaccKw
     *
     * @return float
     */
    public function getTouALgstdmdcontaccKw()
    {
        return $this->touALgstdmdcontaccKw;
    }

    /**
     * Set touATotalKwh
     *
     * @param float $touATotalKwh
     *
     * @return EmsPowerReportLog
     */
    public function setTouATotalKwh($touATotalKwh)
    {
        $this->touATotalKwh = $touATotalKwh;

        return $this;
    }

    /**
     * Get touATotalKwh
     *
     * @return float
     */
    public function getTouATotalKwh()
    {
        return $this->touATotalKwh;
    }

    /**
     * Set touBLgstdmdcontaccKw
     *
     * @param float $touBLgstdmdcontaccKw
     *
     * @return EmsPowerReportLog
     */
    public function setTouBLgstdmdcontaccKw($touBLgstdmdcontaccKw)
    {
        $this->touBLgstdmdcontaccKw = $touBLgstdmdcontaccKw;

        return $this;
    }

    /**
     * Get touBLgstdmdcontaccKw
     *
     * @return float
     */
    public function getTouBLgstdmdcontaccKw()
    {
        return $this->touBLgstdmdcontaccKw;
    }

    /**
     * Set touBTotalKwh
     *
     * @param float $touBTotalKwh
     *
     * @return EmsPowerReportLog
     */
    public function setTouBTotalKwh($touBTotalKwh)
    {
        $this->touBTotalKwh = $touBTotalKwh;

        return $this;
    }

    /**
     * Get touBTotalKwh
     *
     * @return float
     */
    public function getTouBTotalKwh()
    {
        return $this->touBTotalKwh;
    }

    /**
     * Set touCLgstdmdaccKw
     *
     * @param float $touCLgstdmdaccKw
     *
     * @return EmsPowerReportLog
     */
    public function setTouCLgstdmdaccKw($touCLgstdmdaccKw)
    {
        $this->touCLgstdmdaccKw = $touCLgstdmdaccKw;

        return $this;
    }

    /**
     * Get touCLgstdmdaccKw
     *
     * @return float
     */
    public function getTouCLgstdmdaccKw()
    {
        return $this->touCLgstdmdaccKw;
    }

    /**
     * Set touCLgstdmdcontaccKw
     *
     * @param float $touCLgstdmdcontaccKw
     *
     * @return EmsPowerReportLog
     */
    public function setTouCLgstdmdcontaccKw($touCLgstdmdcontaccKw)
    {
        $this->touCLgstdmdcontaccKw = $touCLgstdmdcontaccKw;

        return $this;
    }

    /**
     * Get touCLgstdmdcontaccKw
     *
     * @return float
     */
    public function getTouCLgstdmdcontaccKw()
    {
        return $this->touCLgstdmdcontaccKw;
    }

    /**
     * Set touCTotalKwh
     *
     * @param float $touCTotalKwh
     *
     * @return EmsPowerReportLog
     */
    public function setTouCTotalKwh($touCTotalKwh)
    {
        $this->touCTotalKwh = $touCTotalKwh;

        return $this;
    }

    /**
     * Get touCTotalKwh
     *
     * @return float
     */
    public function getTouCTotalKwh()
    {
        return $this->touCTotalKwh;
    }

    /**
     * Set touDLgstdmdaccKw
     *
     * @param float $touDLgstdmdaccKw
     *
     * @return EmsPowerReportLog
     */
    public function setTouDLgstdmdaccKw($touDLgstdmdaccKw)
    {
        $this->touDLgstdmdaccKw = $touDLgstdmdaccKw;

        return $this;
    }

    /**
     * Get touDLgstdmdaccKw
     *
     * @return float
     */
    public function getTouDLgstdmdaccKw()
    {
        return $this->touDLgstdmdaccKw;
    }

    /**
     * Set touDLgstdmdcontaccKw
     *
     * @param float $touDLgstdmdcontaccKw
     *
     * @return EmsPowerReportLog
     */
    public function setTouDLgstdmdcontaccKw($touDLgstdmdcontaccKw)
    {
        $this->touDLgstdmdcontaccKw = $touDLgstdmdcontaccKw;

        return $this;
    }

    /**
     * Get touDLgstdmdcontaccKw
     *
     * @return float
     */
    public function getTouDLgstdmdcontaccKw()
    {
        return $this->touDLgstdmdcontaccKw;
    }

    /**
     * Set touDTotalKwh
     *
     * @param float $touDTotalKwh
     *
     * @return EmsPowerReportLog
     */
    public function setTouDTotalKwh($touDTotalKwh)
    {
        $this->touDTotalKwh = $touDTotalKwh;

        return $this;
    }

    /**
     * Get touDTotalKwh
     *
     * @return float
     */
    public function getTouDTotalKwh()
    {
        return $this->touDTotalKwh;
    }

    /**
     * Set touBLgstdmdaccKw
     *
     * @param float $touBLgstdmdaccKw
     *
     * @return EmsPowerReportLog
     */
    public function setTouBLgstdmdaccKw($touBLgstdmdaccKw)
    {
        $this->touBLgstdmdaccKw = $touBLgstdmdaccKw;

        return $this;
    }

    /**
     * Get touBLgstdmdaccKw
     *
     * @return float
     */
    public function getTouBLgstdmdaccKw()
    {
        return $this->touBLgstdmdaccKw;
    }

    /**
     * Set touALgstdmdaccKw
     *
     * @param float $touALgstdmdaccKw
     *
     * @return EmsPowerReportLog
     */
    public function setTouALgstdmdaccKw($touALgstdmdaccKw)
    {
        $this->touALgstdmdaccKw = $touALgstdmdaccKw;

        return $this;
    }

    /**
     * Get touALgstdmdaccKw
     *
     * @return float
     */
    public function getTouALgstdmdaccKw()
    {
        return $this->touALgstdmdaccKw;
    }

    /**
     * Set touALgstdmdKw
     *
     * @param float $touALgstdmdKw
     *
     * @return EmsPowerReportLog
     */
    public function setTouALgstdmdKw($touALgstdmdKw)
    {
        $this->touALgstdmdKw = $touALgstdmdKw;

        return $this;
    }

    /**
     * Get touALgstdmdKw
     *
     * @return float
     */
    public function getTouALgstdmdKw()
    {
        return $this->touALgstdmdKw;
    }

    /**
     * Set touBLgstdmdKw
     *
     * @param float $touBLgstdmdKw
     *
     * @return EmsPowerReportLog
     */
    public function setTouBLgstdmdKw($touBLgstdmdKw)
    {
        $this->touBLgstdmdKw = $touBLgstdmdKw;

        return $this;
    }

    /**
     * Get touBLgstdmdKw
     *
     * @return float
     */
    public function getTouBLgstdmdKw()
    {
        return $this->touBLgstdmdKw;
    }

    /**
     * Set touCLgstdmdKw
     *
     * @param float $touCLgstdmdKw
     *
     * @return EmsPowerReportLog
     */
    public function setTouCLgstdmdKw($touCLgstdmdKw)
    {
        $this->touCLgstdmdKw = $touCLgstdmdKw;

        return $this;
    }

    /**
     * Get touCLgstdmdKw
     *
     * @return float
     */
    public function getTouCLgstdmdKw()
    {
        return $this->touCLgstdmdKw;
    }

    /**
     * Set touDLgstdmdKw
     *
     * @param float $touDLgstdmdKw
     *
     * @return EmsPowerReportLog
     */
    public function setTouDLgstdmdKw($touDLgstdmdKw)
    {
        $this->touDLgstdmdKw = $touDLgstdmdKw;

        return $this;
    }

    /**
     * Get touDLgstdmdKw
     *
     * @return float
     */
    public function getTouDLgstdmdKw()
    {
        return $this->touDLgstdmdKw;
    }

    /**
     * Set currDmdKw
     *
     * @param float $currDmdKw
     *
     * @return EmsPowerReportLog
     */
    public function setCurrDmdKw($currDmdKw)
    {
        $this->currDmdKw = $currDmdKw;

        return $this;
    }

    /**
     * Get currDmdKw
     *
     * @return float
     */
    public function getCurrDmdKw()
    {
        return $this->currDmdKw;
    }

    /**
     * Set modbusId
     *
     * @param integer $modbusId
     *
     * @return EmsPowerReportLog
     */
    public function setModbusId($modbusId)
    {
        $this->modbusId = $modbusId;

        return $this;
    }

    /**
     * Get modbusId
     *
     * @return integer
     */
    public function getModbusId()
    {
        return $this->modbusId;
    }

    /**
     * Set currDmdRemainTime
     *
     * @param integer $currDmdRemainTime
     *
     * @return EmsPowerReportLog
     */
    public function setCurrDmdRemainTime($currDmdRemainTime)
    {
        $this->currDmdRemainTime = $currDmdRemainTime;

        return $this;
    }

    /**
     * Get currDmdRemainTime
     *
     * @return integer
     */
    public function getCurrDmdRemainTime()
    {
        return $this->currDmdRemainTime;
    }

    /**
     * Set emsCircuitInfodeviceId
     *
     * @param string $emsCircuitInfodeviceId
     *
     * @return EmsPowerReportLog
     */
    public function setEmsCircuitInfodeviceId($emsCircuitInfodeviceId)
    {
        $this->emsCircuitInfodeviceId = $emsCircuitInfodeviceId;

        return $this;
    }

    /**
     * Get emsCircuitInfodeviceId
     *
     * @return string
     */
    public function getEmsCircuitInfodeviceId()
    {
        return $this->emsCircuitInfodeviceId;
    }

    /**
     * @var string
     */
    private $circuitId;


    /**
     * Set circuitId
     *
     * @param string $circuitId
     *
     * @return EmsPowerReportLog
     */
    public function setCircuitId($circuitId)
    {
        $this->circuitId = $circuitId;

        return $this;
    }

    /**
     * Get circuitId
     *
     * @return string
     */
    public function getCircuitId()
    {
        return $this->circuitId;
    }
    /**
     * @var integer
     */
    private $id;


    /**
     * Set id
     *
     * @param integer $id
     *
     * @return EmsPowerReportLog
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function calculateAverage()
    {
        $factor = 0;
        if ( $this->vA != null || $this->vA != 0 ) $factor++;
        if ( $this->vB != null || $this->vB != 0 ) $factor++;
        if ( $this->vC != null || $this->vC != 0 ) $factor++;

        if ( $factor == 0 ) return ;

        if ( $this->vAverage == null ) {
            $this->vAverage = ( $this->vA + $this->vB + $this->vC )/ $factor;
        }

        // maybe /2 or /1
        if ( $this->aAverage == null ) {
            $this->aAverage = ( $this->aA + $this->aB + $this->aC )/ $factor ;
        }

        if ( $this->kwTotal == null  ) {
            $this->kwTotal = $this->kwA + $this->kwB + $this->kwC;
        }
    }
    /**
     * @var string|null
     */
    private $source;


    /**
     * Set source.
     *
     * @param string|null $source
     *
     * @return EmsPowerReportLog
     */
    public function setSource($source = null)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source.
     *
     * @return string|null
     */
    public function getSource()
    {
        return $this->source;
    }
}
