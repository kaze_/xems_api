<?php

namespace AppBundle\Entity;

/**
 * EmsSensor
 */
class EmsSensor
{
    /**
     * @var int|null
     */
    private $modbusId;

    /**
     * @var int|null
     */
    private $logInterval;

    /**
     * @var int|null
     */
    private $monitorInterval;

    /**
     * @var \AppBundle\Entity\EmsDeviceMaster
     */
    private $device;

    /**
     * @var \AppBundle\Entity\EmsSensorType
     */
    private $sensorType;

    /**
     * @var \AppBundle\Entity\EmsModbusGateway
     */
    private $modbusGateway;


    /**
     * Set modbusId.
     *
     * @param int|null $modbusId
     *
     * @return EmsSensor
     */
    public function setModbusId($modbusId = null)
    {
        $this->modbusId = $modbusId;

        return $this;
    }

    /**
     * Get modbusId.
     *
     * @return int|null
     */
    public function getModbusId()
    {
        return $this->modbusId;
    }

    /**
     * Set logInterval.
     *
     * @param int|null $logInterval
     *
     * @return EmsSensor
     */
    public function setLogInterval($logInterval = null)
    {
        $this->logInterval = $logInterval;

        return $this;
    }

    /**
     * Get logInterval.
     *
     * @return int|null
     */
    public function getLogInterval()
    {
        return $this->logInterval;
    }

    /**
     * Set monitorInterval.
     *
     * @param int|null $monitorInterval
     *
     * @return EmsSensor
     */
    public function setMonitorInterval($monitorInterval = null)
    {
        $this->monitorInterval = $monitorInterval;

        return $this;
    }

    /**
     * Get monitorInterval.
     *
     * @return int|null
     */
    public function getMonitorInterval()
    {
        return $this->monitorInterval;
    }

    /**
     * Set device.
     *
     * @param \AppBundle\Entity\EmsDeviceMaster $device
     *
     * @return EmsSensor
     */
    public function setDevice(\AppBundle\Entity\EmsDeviceMaster $device)
    {
        $this->device = $device;

        return $this;
    }

    /**
     * Get device.
     *
     * @return \AppBundle\Entity\EmsDeviceMaster
     */
    public function getDevice()
    {
        return $this->device;
    }

    /**
     * Set sensorType.
     *
     * @param \AppBundle\Entity\EmsSensorType|null $sensorType
     *
     * @return EmsSensor
     */
    public function setSensorType(\AppBundle\Entity\EmsSensorType $sensorType = null)
    {
        $this->sensorType = $sensorType;

        return $this;
    }

    /**
     * Get sensorType.
     *
     * @return \AppBundle\Entity\EmsSensorType|null
     */
    public function getSensorType()
    {
        return $this->sensorType;
    }

    /**
     * Set modbusGateway.
     *
     * @param \AppBundle\Entity\EmsModbusGateway|null $modbusGateway
     *
     * @return EmsSensor
     */
    public function setModbusGateway(\AppBundle\Entity\EmsModbusGateway $modbusGateway = null)
    {
        $this->modbusGateway = $modbusGateway;

        return $this;
    }

    /**
     * Get modbusGateway.
     *
     * @return \AppBundle\Entity\EmsModbusGateway|null
     */
    public function getModbusGateway()
    {
        return $this->modbusGateway;
    }
    
    /**
     * @var \DateTime|null
     */
    private $lastDataReport;
    
    
    /**
     * Set lastDataReport.
     *
     * @param \DateTime|null $lastDataReport
     *
     * @return EmsCircuitMaster
     */
    public function setLastDataReport($lastDataReport = null)
    {
        $this->lastDataReport = $lastDataReport;
        
        return $this;
    }
    
    /**
     * Get lastDataReport.
     *
     * @return \DateTime|null
     */
    public function getLastDataReport()
    {
        return $this->lastDataReport;
    }
    
    /**
     * @var \DateTime|null
     */
    private $newestData;


    /**
     * Set newestData.
     *
     * @param \DateTime|null $newestData
     *
     * @return EmsSensor
     */
    public function setNewestData($newestData = null)
    {
        if ( $this->newestData == null || $newestData > $this->newestData ) {
            $this->newestData = $newestData;
        }
        return $this;
    }

    /**
     * Get newestData.
     *
     * @return \DateTime|null
     */
    public function getNewestData()
    {
        return $this->newestData;
    }
}
