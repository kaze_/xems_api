<?php

namespace AppBundle\Entity;

/**
 * EmsSensorReportLog
 */
class EmsSensorReportLog
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $circuitId = '';

    /**
     * @var \DateTime
     */
    private $reportDatetime;

    /**
     * @var float|null
     */
    private $temperature;

    /**
     * @var float|null
     */
    private $humidity;

    /**
     * @var float|null
     */
    private $radiant;

    /**
     * @var float|null
     */
    private $windDirection;

    /**
     * @var float|null
     */
    private $windSpeed;

    /**
     * @var float|null
     */
    private $rainfall;

    /**
     * @var int|null
     */
    private $modbusId;

    /**
     * @var string|null
     */
    private $source;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set circuitId.
     *
     * @param string $circuitId
     *
     * @return EmsSensorReportLog
     */
    public function setCircuitId($circuitId)
    {
        $this->circuitId = $circuitId;

        return $this;
    }

    /**
     * Get circuitId.
     *
     * @return string
     */
    public function getCircuitId()
    {
        return $this->circuitId;
    }

    /**
     * Set reportDatetime.
     *
     * @param \DateTime $reportDatetime
     *
     * @return EmsSensorReportLog
     */
    public function setReportDatetime($reportDatetime)
    {
        $this->reportDatetime = $reportDatetime;

        return $this;
    }

    /**
     * Get reportDatetime.
     *
     * @return \DateTime
     */
    public function getReportDatetime()
    {
        return $this->reportDatetime;
    }

    /**
     * Set temperature.
     *
     * @param float|null $temperature
     *
     * @return EmsSensorReportLog
     */
    public function setTemperature($temperature = null)
    {
        $this->temperature = $temperature;

        return $this;
    }

    /**
     * Get temperature.
     *
     * @return float|null
     */
    public function getTemperature()
    {
        return $this->temperature;
    }

    /**
     * Set humidity.
     *
     * @param float|null $humidity
     *
     * @return EmsSensorReportLog
     */
    public function setHumidity($humidity = null)
    {
        $this->humidity = $humidity;

        return $this;
    }

    /**
     * Get humidity.
     *
     * @return float|null
     */
    public function getHumidity()
    {
        return $this->humidity;
    }

    /**
     * Set radiant.
     *
     * @param float|null $radiant
     *
     * @return EmsSensorReportLog
     */
    public function setRadiant($radiant = null)
    {
        $this->radiant = $radiant;

        return $this;
    }

    /**
     * Get radiant.
     *
     * @return float|null
     */
    public function getRadiant()
    {
        return $this->radiant;
    }

    /**
     * Set windDirection.
     *
     * @param float|null $windDirection
     *
     * @return EmsSensorReportLog
     */
    public function setWindDirection($windDirection = null)
    {
        $this->windDirection = $windDirection;

        return $this;
    }

    /**
     * Get windDirection.
     *
     * @return float|null
     */
    public function getWindDirection()
    {
        return $this->windDirection;
    }

    /**
     * Set windSpeed.
     *
     * @param float|null $windSpeed
     *
     * @return EmsSensorReportLog
     */
    public function setWindSpeed($windSpeed = null)
    {
        $this->windSpeed = $windSpeed;

        return $this;
    }

    /**
     * Get windSpeed.
     *
     * @return float|null
     */
    public function getWindSpeed()
    {
        return $this->windSpeed;
    }

    /**
     * Set rainfall.
     *
     * @param float|null $rainfall
     *
     * @return EmsSensorReportLog
     */
    public function setRainfall($rainfall = null)
    {
        $this->rainfall = $rainfall;

        return $this;
    }

    /**
     * Get rainfall.
     *
     * @return float|null
     */
    public function getRainfall()
    {
        return $this->rainfall;
    }

    /**
     * Set modbusId.
     *
     * @param int|null $modbusId
     *
     * @return EmsSensorReportLog
     */
    public function setModbusId($modbusId = null)
    {
        $this->modbusId = $modbusId;

        return $this;
    }

    /**
     * Get modbusId.
     *
     * @return int|null
     */
    public function getModbusId()
    {
        return $this->modbusId;
    }

    /**
     * Set source.
     *
     * @param string|null $source
     *
     * @return EmsSensorReportLog
     */
    public function setSource($source = null)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source.
     *
     * @return string|null
     */
    public function getSource()
    {
        return $this->source;
    }
}
