<?php

namespace AppBundle\Entity;

/**
 * EmsSensorType
 */
class EmsSensorType
{
    /**
     * @var int
     */
    private $sensorTypeId;

    /**
     * @var string
     */
    private $sensorName;

    /**
     * @var string
     */
    private $sensorModelName;


    /**
     * Get sensorTypeId.
     *
     * @return int
     */
    public function getSensorTypeId()
    {
        return $this->sensorTypeId;
    }

    /**
     * Set sensorName.
     *
     * @param string $sensorName
     *
     * @return EmsSensorType
     */
    public function setSensorName($sensorName)
    {
        $this->sensorName = $sensorName;

        return $this;
    }

    /**
     * Get sensorName.
     *
     * @return string
     */
    public function getSensorName()
    {
        return $this->sensorName;
    }

    /**
     * Set sensorModelName.
     *
     * @param string $sensorModelName
     *
     * @return EmsSensorType
     */
    public function setSensorModelName($sensorModelName)
    {
        $this->sensorModelName = $sensorModelName;

        return $this;
    }

    /**
     * Get sensorModelName.
     *
     * @return string
     */
    public function getSensorModelName()
    {
        return $this->sensorModelName;
    }
}
