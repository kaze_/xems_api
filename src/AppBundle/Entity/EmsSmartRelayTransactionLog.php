<?php

namespace AppBundle\Entity;

/**
 * EmsSmartRelayTransactionLog
 */
class EmsSmartRelayTransactionLog
{
    const ACT_NOCHANGE = 0;
    const ACT_STARTED = 1;
    const ACT_ENDING = 2;
    const ACT_ENDED = 3;
    const ACT_UNPLUGGED = 4;
    const ACT_METEROFF = 5;
    const ACT_METERON = 6;

    const STAT_OFF = 10;
    const STAT_STANDBY = 11;
    const STAT_ON = 12;

    const SOURCE_GATEWAY = 1;
    const SOURCE_USER = 2;

    function __construct()
    {
        $this->transactionTime = new \DateTime();
    }


    /**
     * @var \AppBundle\Entity\EmsCircuitMaster
     */
    private $circuit;

    /**
     * @var \AppBundle\Entity\EvChargingStationMaster
     */
    private $chargingStation;

    /**
     * Set circuit
     *
     * @param \AppBundle\Entity\EmsCircuitMaster $circuit
     *
     * @return EmsSmartRelayTransactionLog
     */
    public function setCircuit(\AppBundle\Entity\EmsCircuitMaster $circuit = null)
    {
        $this->circuit = $circuit;

        return $this;
    }

    /**
     * Get circuit
     *
     * @return \AppBundle\Entity\EmsCircuitMaster
     */
    public function getCircuit()
    {
        return $this->circuit;
    }

    /**
     * Set chargingStation
     *
     * @param \AppBundle\Entity\EvChargingStationMaster $chargingStation
     *
     * @return EmsSmartRelayTransactionLog
     */
    public function setChargingStation(\AppBundle\Entity\EvChargingStationMaster $chargingStation = null)
    {
        $this->chargingStation = $chargingStation;

        return $this;
    }

    /**
     * Get chargingStation
     *
     * @return \AppBundle\Entity\EvChargingStationMaster
     */
    public function getChargingStation()
    {
        return $this->chargingStation;
    }
    /**
     * @var \DateTime
     */
    private $transactionTime;

    /**
     * @var integer
     */
    private $sessionId;


    /**
     * Set transactionTime
     *
     * @param \DateTime $transactionTime
     *
     * @return EmsSmartRelayTransactionLog
     */
    public function setTransactionTime($transactionTime)
    {
        $this->transactionTime = $transactionTime;

        return $this;
    }

    /**
     * Get transactionTime
     *
     * @return \DateTime
     */
    public function getTransactionTime()
    {
        return $this->transactionTime;
    }

    /**
     * Set sessionId
     *
     * @param integer $sessionId
     *
     * @return EmsSmartRelayTransactionLog
     */
    public function setSessionId($sessionId)
    {
        $this->sessionId = $sessionId;

        return $this;
    }

    /**
     * Get sessionId
     *
     * @return integer
     */
    public function getSessionId()
    {
        return $this->sessionId;
    }
    /**
     * @var integer
     */
    private $id;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @var string|null
     */
    private $source;

    /**
     * @var string|null
     */
    private $sourceId;

    /**
     * @var string
     */
    private $statusId;


    /**
     * Set source.
     *
     * @param string|null $source
     *
     * @return EmsSmartRelayTransactionLog
     */
    public function setSource($source = null)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source.
     *
     * @return string|null
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set sourceId.
     *
     * @param string|null $sourceId
     *
     * @return EmsSmartRelayTransactionLog
     */
    public function setSourceId($sourceId = null)
    {
        $this->sourceId = $sourceId;

        return $this;
    }

    /**
     * Get sourceId.
     *
     * @return string|null
     */
    public function getSourceId()
    {
        return $this->sourceId;
    }

    /**
     * Set statusId.
     *
     * @param string $statusId
     *
     * @return EmsSmartRelayTransactionLog
     */
    public function setStatusId($statusId)
    {
        $this->statusId = $statusId;

        return $this;
    }

    /**
     * Get statusId.
     *
     * @return string
     */
    public function getStatusId()
    {
        return $this->statusId;
    }
    /**
     * @var string
     */
    private $sessionStatus;


    /**
     * Set sessionStatus.
     *
     * @param string $sessionStatus
     *
     * @return EmsSmartRelayTransactionLog
     */
    public function setSessionStatus($sessionStatus)
    {
        $this->sessionStatus = $sessionStatus;

        return $this;
    }

    /**
     * Get sessionStatus.
     *
     * @return string
     */
    public function getSessionStatus()
    {
        return $this->sessionStatus;
    }
    /**
     * @var string|null
     */
    private $comment;


    /**
     * Set comment.
     *
     * @param string|null $comment
     *
     * @return EmsSmartRelayTransactionLog
     */
    public function setComment($comment = null)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment.
     *
     * @return string|null
     */
    public function getComment()
    {
        return $this->comment;
    }
    /**
     * @var string|null
     */
    private $kaaEndpointHash;


    /**
     * Set kaaEndpointHash.
     *
     * @param string|null $kaaEndpointHash
     *
     * @return EmsSmartRelayTransactionLog
     */
    public function setKaaEndpointHash($kaaEndpointHash = null)
    {
        $this->kaaEndpointHash = $kaaEndpointHash;

        return $this;
    }

    /**
     * Get kaaEndpointHash.
     *
     * @return string|null
     */
    public function getKaaEndpointHash()
    {
        return $this->kaaEndpointHash;
    }

    /**
     * @var string|null
     */
    private $evPlate;


    /**
     * Set evPlate.
     *
     * @param string|null $evPlate
     *
     * @return EmsSmartRelayTransactionLog
     */
    public function setEvPlate($evPlate = null)
    {
        $this->evPlate = $evPlate;

        return $this;
    }

    /**
     * Get evPlate.
     *
     * @return string|null
     */
    public function getEvPlate()
    {
        return $this->evPlate;
    }


}
