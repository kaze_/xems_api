<?php

namespace AppBundle\Entity;

/**
 * EmsVoltage
 */
class EmsVoltage
{
    /**
     * @var integer
     */
    private $voltageId;

    /**
     * @var string
     */
    private $voltageName;

    /**
     * @var float
     */
    private $mainVoltage;

    /**
     * @var float
     */
    private $subVoltage;

    /**
     * @var float
     */
    private $frequency;

    /**
     * @var boolean
     */
    private $withEutralLine;


    /**
     * Get voltageId
     *
     * @return integer
     */
    public function getVoltageId()
    {
        return $this->voltageId;
    }

    /**
     * Set voltageName
     *
     * @param string $voltageName
     *
     * @return EmsVoltage
     */
    public function setVoltageName($voltageName)
    {
        $this->voltageName = $voltageName;

        return $this;
    }

    /**
     * Get voltageName
     *
     * @return string
     */
    public function getVoltageName()
    {
        return $this->voltageName;
    }

    /**
     * Set mainVoltage
     *
     * @param float $mainVoltage
     *
     * @return EmsVoltage
     */
    public function setMainVoltage($mainVoltage)
    {
        $this->mainVoltage = $mainVoltage;

        return $this;
    }

    /**
     * Get mainVoltage
     *
     * @return float
     */
    public function getMainVoltage()
    {
        return $this->mainVoltage;
    }

    /**
     * Set subVoltage
     *
     * @param float $subVoltage
     *
     * @return EmsVoltage
     */
    public function setSubVoltage($subVoltage)
    {
        $this->subVoltage = $subVoltage;

        return $this;
    }

    /**
     * Get subVoltage
     *
     * @return float
     */
    public function getSubVoltage()
    {
        return $this->subVoltage;
    }

    /**
     * Set frequency
     *
     * @param float $frequency
     *
     * @return EmsVoltage
     */
    public function setFrequency($frequency)
    {
        $this->frequency = $frequency;

        return $this;
    }

    /**
     * Get frequency
     *
     * @return float
     */
    public function getFrequency()
    {
        return $this->frequency;
    }

    /**
     * Set withEutralLine
     *
     * @param boolean $withEutralLine
     *
     * @return EmsVoltage
     */
    public function setWithEutralLine($withEutralLine)
    {
        $this->withEutralLine = $withEutralLine;

        return $this;
    }

    /**
     * Get withEutralLine
     *
     * @return boolean
     */
    public function getWithEutralLine()
    {
        return $this->withEutralLine;
    }
}
