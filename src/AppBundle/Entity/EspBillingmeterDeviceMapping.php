<?php

namespace AppBundle\Entity;

/**
 * EspBillingmeterDeviceMapping
 */
class EspBillingmeterDeviceMapping
{
    /**
     * @var integer
     */
    private $statusId;

    /**
     * @var \DateTime
     */
    private $mappingDate;

    /**
     * @var \AppBundle\Entity\EmsDeviceMaster
     */
    private $powerMeterDevice;

    /**
     * @var \AppBundle\Entity\EspTaipowerIdMaster
     */
    private $taipower;


    /**
     * Set statusId
     *
     * @param integer $statusId
     *
     * @return EspBillingmeterDeviceMapping
     */
    public function setStatusId($statusId)
    {
        $this->statusId = $statusId;

        return $this;
    }

    /**
     * Get statusId
     *
     * @return integer
     */
    public function getStatusId()
    {
        return $this->statusId;
    }

    /**
     * Set mappingDate
     *
     * @param \DateTime $mappingDate
     *
     * @return EspBillingmeterDeviceMapping
     */
    public function setMappingDate($mappingDate)
    {
        $this->mappingDate = $mappingDate;

        return $this;
    }

    /**
     * Get mappingDate
     *
     * @return \DateTime
     */
    public function getMappingDate()
    {
        return $this->mappingDate;
    }

    /**
     * Set powerMeterDevice
     *
     * @param \AppBundle\Entity\EmsDeviceMaster $powerMeterDevice
     *
     * @return EspBillingmeterDeviceMapping
     */
    public function setPowerMeterDevice(\AppBundle\Entity\EmsDeviceMaster $powerMeterDevice = null)
    {
        $this->powerMeterDevice = $powerMeterDevice;

        return $this;
    }

    /**
     * Get powerMeterDevice
     *
     * @return \AppBundle\Entity\EmsDeviceMaster
     */
    public function getPowerMeterDevice()
    {
        return $this->powerMeterDevice;
    }

    /**
     * Set taipower
     *
     * @param \AppBundle\Entity\EspTaipowerIdMaster $taipower
     *
     * @return EspBillingmeterDeviceMapping
     */
    public function setTaipower(\AppBundle\Entity\EspTaipowerIdMaster $taipower = null)
    {
        $this->taipower = $taipower;

        return $this;
    }

    /**
     * Get taipower
     *
     * @return \AppBundle\Entity\EspTaipowerIdMaster
     */
    public function getTaipower()
    {
        return $this->taipower;
    }
}
