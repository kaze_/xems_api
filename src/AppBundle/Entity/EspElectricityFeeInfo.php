<?php

namespace AppBundle\Entity;

/**
 * EspElectricityFeeInfo
 */
class EspElectricityFeeInfo
{
    /**
     * @var string
     */
    private $taipowerId;

    /**
     * @var \DateTime
     */
    private $billingStartDate;

    /**
     * @var \DateTime
     */
    private $billingEndDate;

    /**
     * @var integer
     */
    private $billingDays;

    /**
     * @var integer
     */
    private $touaContract;

    /**
     * @var integer
     */
    private $toubContract;

    /**
     * @var integer
     */
    private $toucContract;

    /**
     * @var integer
     */
    private $toudContract;

    /**
     * @var integer
     */
    private $touaUsage;

    /**
     * @var integer
     */
    private $toubUsage;

    /**
     * @var integer
     */
    private $toucUsage;

    /**
     * @var integer
     */
    private $toudUsage;

    /**
     * @var integer
     */
    private $touaDemand;

    /**
     * @var integer
     */
    private $toubDemand;

    /**
     * @var integer
     */
    private $toucDemand;

    /**
     * @var integer
     */
    private $toudDemand;

    /**
     * @var integer
     */
    private $powerFactor;

    /**
     * @var boolean
     */
    private $nonPringingBilling;

    /**
     * @var float
     */
    private $basicCharge;

    /**
     * @var float
     */
    private $energyCharge;

    /**
     * @var float
     */
    private $powerFactorDiscount;

    /**
     * @var string
     */
    private $billingAddress;

    /**
     * @var \AppBundle\Entity\EspEnergyPackage
     */
    private $energyPackage;


    /**
     * Set taipowerId
     *
     * @param string $taipowerId
     *
     * @return EspElectricityFeeInfo
     */
    public function setTaipowerId($taipowerId)
    {
        $this->taipowerId = $taipowerId;

        return $this;
    }

    /**
     * Get taipowerId
     *
     * @return string
     */
    public function getTaipowerId()
    {
        return $this->taipowerId;
    }

    /**
     * Set billingStartDate
     *
     * @param \DateTime $billingStartDate
     *
     * @return EspElectricityFeeInfo
     */
    public function setBillingStartDate($billingStartDate)
    {
        $this->billingStartDate = $billingStartDate;

        return $this;
    }

    /**
     * Get billingStartDate
     *
     * @return \DateTime
     */
    public function getBillingStartDate()
    {
        return $this->billingStartDate;
    }

    /**
     * Set billingEndDate
     *
     * @param \DateTime $billingEndDate
     *
     * @return EspElectricityFeeInfo
     */
    public function setBillingEndDate($billingEndDate)
    {
        $this->billingEndDate = $billingEndDate;

        return $this;
    }

    /**
     * Get billingEndDate
     *
     * @return \DateTime
     */
    public function getBillingEndDate()
    {
        return $this->billingEndDate;
    }

    /**
     * Set billingDays
     *
     * @param integer $billingDays
     *
     * @return EspElectricityFeeInfo
     */
    public function setBillingDays($billingDays)
    {
        $this->billingDays = $billingDays;

        return $this;
    }

    /**
     * Get billingDays
     *
     * @return integer
     */
    public function getBillingDays()
    {
        return $this->billingDays;
    }

    /**
     * Set touaContract
     *
     * @param integer $touaContract
     *
     * @return EspElectricityFeeInfo
     */
    public function setTouaContract($touaContract)
    {
        $this->touaContract = $touaContract;

        return $this;
    }

    /**
     * Get touaContract
     *
     * @return integer
     */
    public function getTouaContract()
    {
        return $this->touaContract;
    }

    /**
     * Set toubContract
     *
     * @param integer $toubContract
     *
     * @return EspElectricityFeeInfo
     */
    public function setToubContract($toubContract)
    {
        $this->toubContract = $toubContract;

        return $this;
    }

    /**
     * Get toubContract
     *
     * @return integer
     */
    public function getToubContract()
    {
        return $this->toubContract;
    }

    /**
     * Set toucContract
     *
     * @param integer $toucContract
     *
     * @return EspElectricityFeeInfo
     */
    public function setToucContract($toucContract)
    {
        $this->toucContract = $toucContract;

        return $this;
    }

    /**
     * Get toucContract
     *
     * @return integer
     */
    public function getToucContract()
    {
        return $this->toucContract;
    }

    /**
     * Set toudContract
     *
     * @param integer $toudContract
     *
     * @return EspElectricityFeeInfo
     */
    public function setToudContract($toudContract)
    {
        $this->toudContract = $toudContract;

        return $this;
    }

    /**
     * Get toudContract
     *
     * @return integer
     */
    public function getToudContract()
    {
        return $this->toudContract;
    }

    /**
     * Set touaUsage
     *
     * @param integer $touaUsage
     *
     * @return EspElectricityFeeInfo
     */
    public function setTouaUsage($touaUsage)
    {
        $this->touaUsage = $touaUsage;

        return $this;
    }

    /**
     * Get touaUsage
     *
     * @return integer
     */
    public function getTouaUsage()
    {
        return $this->touaUsage;
    }

    /**
     * Set toubUsage
     *
     * @param integer $toubUsage
     *
     * @return EspElectricityFeeInfo
     */
    public function setToubUsage($toubUsage)
    {
        $this->toubUsage = $toubUsage;

        return $this;
    }

    /**
     * Get toubUsage
     *
     * @return integer
     */
    public function getToubUsage()
    {
        return $this->toubUsage;
    }

    /**
     * Set toucUsage
     *
     * @param integer $toucUsage
     *
     * @return EspElectricityFeeInfo
     */
    public function setToucUsage($toucUsage)
    {
        $this->toucUsage = $toucUsage;

        return $this;
    }

    /**
     * Get toucUsage
     *
     * @return integer
     */
    public function getToucUsage()
    {
        return $this->toucUsage;
    }

    /**
     * Set toudUsage
     *
     * @param integer $toudUsage
     *
     * @return EspElectricityFeeInfo
     */
    public function setToudUsage($toudUsage)
    {
        $this->toudUsage = $toudUsage;

        return $this;
    }

    /**
     * Get toudUsage
     *
     * @return integer
     */
    public function getToudUsage()
    {
        return $this->toudUsage;
    }

    /**
     * Set touaDemand
     *
     * @param integer $touaDemand
     *
     * @return EspElectricityFeeInfo
     */
    public function setTouaDemand($touaDemand)
    {
        $this->touaDemand = $touaDemand;

        return $this;
    }

    /**
     * Get touaDemand
     *
     * @return integer
     */
    public function getTouaDemand()
    {
        return $this->touaDemand;
    }

    /**
     * Set toubDemand
     *
     * @param integer $toubDemand
     *
     * @return EspElectricityFeeInfo
     */
    public function setToubDemand($toubDemand)
    {
        $this->toubDemand = $toubDemand;

        return $this;
    }

    /**
     * Get toubDemand
     *
     * @return integer
     */
    public function getToubDemand()
    {
        return $this->toubDemand;
    }

    /**
     * Set toucDemand
     *
     * @param integer $toucDemand
     *
     * @return EspElectricityFeeInfo
     */
    public function setToucDemand($toucDemand)
    {
        $this->toucDemand = $toucDemand;

        return $this;
    }

    /**
     * Get toucDemand
     *
     * @return integer
     */
    public function getToucDemand()
    {
        return $this->toucDemand;
    }

    /**
     * Set toudDemand
     *
     * @param integer $toudDemand
     *
     * @return EspElectricityFeeInfo
     */
    public function setToudDemand($toudDemand)
    {
        $this->toudDemand = $toudDemand;

        return $this;
    }

    /**
     * Get toudDemand
     *
     * @return integer
     */
    public function getToudDemand()
    {
        return $this->toudDemand;
    }

    /**
     * Set powerFactor
     *
     * @param integer $powerFactor
     *
     * @return EspElectricityFeeInfo
     */
    public function setPowerFactor($powerFactor)
    {
        $this->powerFactor = $powerFactor;

        return $this;
    }

    /**
     * Get powerFactor
     *
     * @return integer
     */
    public function getPowerFactor()
    {
        return $this->powerFactor;
    }

    /**
     * Set nonPringingBilling
     *
     * @param boolean $nonPringingBilling
     *
     * @return EspElectricityFeeInfo
     */
    public function setNonPringingBilling($nonPringingBilling)
    {
        $this->nonPringingBilling = $nonPringingBilling;

        return $this;
    }

    /**
     * Get nonPringingBilling
     *
     * @return boolean
     */
    public function getNonPringingBilling()
    {
        return $this->nonPringingBilling;
    }

    /**
     * Set basicCharge
     *
     * @param float $basicCharge
     *
     * @return EspElectricityFeeInfo
     */
    public function setBasicCharge($basicCharge)
    {
        $this->basicCharge = $basicCharge;

        return $this;
    }

    /**
     * Get basicCharge
     *
     * @return float
     */
    public function getBasicCharge()
    {
        return $this->basicCharge;
    }

    /**
     * Set energyCharge
     *
     * @param float $energyCharge
     *
     * @return EspElectricityFeeInfo
     */
    public function setEnergyCharge($energyCharge)
    {
        $this->energyCharge = $energyCharge;

        return $this;
    }

    /**
     * Get energyCharge
     *
     * @return float
     */
    public function getEnergyCharge()
    {
        return $this->energyCharge;
    }

    /**
     * Set powerFactorDiscount
     *
     * @param float $powerFactorDiscount
     *
     * @return EspElectricityFeeInfo
     */
    public function setPowerFactorDiscount($powerFactorDiscount)
    {
        $this->powerFactorDiscount = $powerFactorDiscount;

        return $this;
    }

    /**
     * Get powerFactorDiscount
     *
     * @return float
     */
    public function getPowerFactorDiscount()
    {
        return $this->powerFactorDiscount;
    }

    /**
     * Set billingAddress
     *
     * @param string $billingAddress
     *
     * @return EspElectricityFeeInfo
     */
    public function setBillingAddress($billingAddress)
    {
        $this->billingAddress = $billingAddress;

        return $this;
    }

    /**
     * Get billingAddress
     *
     * @return string
     */
    public function getBillingAddress()
    {
        return $this->billingAddress;
    }

    /**
     * Set energyPackage
     *
     * @param \AppBundle\Entity\EspEnergyPackage $energyPackage
     *
     * @return EspElectricityFeeInfo
     */
    public function setEnergyPackage(\AppBundle\Entity\EspEnergyPackage $energyPackage = null)
    {
        $this->energyPackage = $energyPackage;

        return $this;
    }

    /**
     * Get energyPackage
     *
     * @return \AppBundle\Entity\EspEnergyPackage
     */
    public function getEnergyPackage()
    {
        return $this->energyPackage;
    }
}
