<?php

namespace AppBundle\Entity;

/**
 * EspEnergyPackage
 */
class EspEnergyPackage
{
    /**
     * @var integer
     */
    private $energyPackageId;

    /**
     * @var string
     */
    private $energyPackageName;

    /**
     * @var \AppBundle\Entity\EspEnergySp
     */
    private $energySp;


    /**
     * Get energyPackageId
     *
     * @return integer
     */
    public function getEnergyPackageId()
    {
        return $this->energyPackageId;
    }

    /**
     * Set energyPackageName
     *
     * @param string $energyPackageName
     *
     * @return EspEnergyPackage
     */
    public function setEnergyPackageName($energyPackageName)
    {
        $this->energyPackageName = $energyPackageName;

        return $this;
    }

    /**
     * Get energyPackageName
     *
     * @return string
     */
    public function getEnergyPackageName()
    {
        return $this->energyPackageName;
    }

    /**
     * Set energySp
     *
     * @param \AppBundle\Entity\EspEnergySp $energySp
     *
     * @return EspEnergyPackage
     */
    public function setEnergySp(\AppBundle\Entity\EspEnergySp $energySp = null)
    {
        $this->energySp = $energySp;

        return $this;
    }

    /**
     * Get energySp
     *
     * @return \AppBundle\Entity\EspEnergySp
     */
    public function getEnergySp()
    {
        return $this->energySp;
    }
}
