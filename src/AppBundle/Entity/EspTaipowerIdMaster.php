<?php

namespace AppBundle\Entity;

/**
 * EspTaipowerIdMaster
 */
class EspTaipowerIdMaster
{
    /**
     * @var string
     */
    private $taipowerId;

    /**
     * @var \DateTime
     */
    private $enableDate;

    /**
     * @var integer
     */
    private $touaContract;

    /**
     * @var integer
     */
    private $toubContract;

    /**
     * @var integer
     */
    private $toucContract;

    /**
     * @var integer
     */
    private $toudContract;

    /**
     * @var string
     */
    private $billingAddress;

    /**
     * @var string
     */
    private $taipowerAccountName;

    /**
     * @var integer
     */
    private $statusId;

    /**
     * @var \AppBundle\Entity\EspEnergyPackage
     */
    private $energyPackage;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $location;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->location = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set taipowerId
     *
     * @param string $taipowerId
     *
     * @return EspTaipowerIdMaster
     */
    public function setTaipowerId($taipowerId)
    {
        $this->taipowerId = $taipowerId;

        return $this;
    }

    /**
     * Get taipowerId
     *
     * @return string
     */
    public function getTaipowerId()
    {
        return $this->taipowerId;
    }

    /**
     * Set enableDate
     *
     * @param \DateTime $enableDate
     *
     * @return EspTaipowerIdMaster
     */
    public function setEnableDate($enableDate)
    {
        $this->enableDate = $enableDate;

        return $this;
    }

    /**
     * Get enableDate
     *
     * @return \DateTime
     */
    public function getEnableDate()
    {
        return $this->enableDate;
    }

    /**
     * Set touaContract
     *
     * @param integer $touaContract
     *
     * @return EspTaipowerIdMaster
     */
    public function setTouaContract($touaContract)
    {
        $this->touaContract = $touaContract;

        return $this;
    }

    /**
     * Get touaContract
     *
     * @return integer
     */
    public function getTouaContract()
    {
        return $this->touaContract;
    }

    /**
     * Set toubContract
     *
     * @param integer $toubContract
     *
     * @return EspTaipowerIdMaster
     */
    public function setToubContract($toubContract)
    {
        $this->toubContract = $toubContract;

        return $this;
    }

    /**
     * Get toubContract
     *
     * @return integer
     */
    public function getToubContract()
    {
        return $this->toubContract;
    }

    /**
     * Set toucContract
     *
     * @param integer $toucContract
     *
     * @return EspTaipowerIdMaster
     */
    public function setToucContract($toucContract)
    {
        $this->toucContract = $toucContract;

        return $this;
    }

    /**
     * Get toucContract
     *
     * @return integer
     */
    public function getToucContract()
    {
        return $this->toucContract;
    }

    /**
     * Set toudContract
     *
     * @param integer $toudContract
     *
     * @return EspTaipowerIdMaster
     */
    public function setToudContract($toudContract)
    {
        $this->toudContract = $toudContract;

        return $this;
    }

    /**
     * Get toudContract
     *
     * @return integer
     */
    public function getToudContract()
    {
        return $this->toudContract;
    }

    /**
     * Set billingAddress
     *
     * @param string $billingAddress
     *
     * @return EspTaipowerIdMaster
     */
    public function setBillingAddress($billingAddress)
    {
        $this->billingAddress = $billingAddress;

        return $this;
    }

    /**
     * Get billingAddress
     *
     * @return string
     */
    public function getBillingAddress()
    {
        return $this->billingAddress;
    }

    /**
     * Set taipowerAccountName
     *
     * @param string $taipowerAccountName
     *
     * @return EspTaipowerIdMaster
     */
    public function setTaipowerAccountName($taipowerAccountName)
    {
        $this->taipowerAccountName = $taipowerAccountName;

        return $this;
    }

    /**
     * Get taipowerAccountName
     *
     * @return string
     */
    public function getTaipowerAccountName()
    {
        return $this->taipowerAccountName;
    }

    /**
     * Set statusId
     *
     * @param integer $statusId
     *
     * @return EspTaipowerIdMaster
     */
    public function setStatusId($statusId)
    {
        $this->statusId = $statusId;

        return $this;
    }

    /**
     * Get statusId
     *
     * @return integer
     */
    public function getStatusId()
    {
        return $this->statusId;
    }

    /**
     * Set energyPackage
     *
     * @param \AppBundle\Entity\EspEnergyPackage $energyPackage
     *
     * @return EspTaipowerIdMaster
     */
    public function setEnergyPackage(\AppBundle\Entity\EspEnergyPackage $energyPackage = null)
    {
        $this->energyPackage = $energyPackage;

        return $this;
    }

    /**
     * Get energyPackage
     *
     * @return \AppBundle\Entity\EspEnergyPackage
     */
    public function getEnergyPackage()
    {
        return $this->energyPackage;
    }

    /**
     * Add location
     *
     * @param \AppBundle\Entity\EmsLocationMaster $location
     *
     * @return EspTaipowerIdMaster
     */
    public function addLocation(\AppBundle\Entity\EmsLocationMaster $location)
    {
        $this->location[] = $location;

        return $this;
    }

    /**
     * Remove location
     *
     * @param \AppBundle\Entity\EmsLocationMaster $location
     */
    public function removeLocation(\AppBundle\Entity\EmsLocationMaster $location)
    {
        $this->location->removeElement($location);
    }

    /**
     * Get location
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLocation()
    {
        return $this->location;
    }
}
