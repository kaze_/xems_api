<?php

namespace AppBundle\Entity;

/**
 * EvCarUltrasonicSensor
 */
class EvCarUltrasonicSensor
{
    /**
     * @var integer
     */
    private $modbusId;

    /**
     * @var integer
     */
    private $boudRate;

    /**
     * @var \AppBundle\Entity\EmsDeviceMaster
     */
    private $device;


    /**
     * Set modbusId
     *
     * @param integer $modbusId
     *
     * @return EvCarUltrasonicSensor
     */
    public function setModbusId($modbusId)
    {
        $this->modbusId = $modbusId;

        return $this;
    }

    /**
     * Get modbusId
     *
     * @return integer
     */
    public function getModbusId()
    {
        return $this->modbusId;
    }

    /**
     * Set boudRate
     *
     * @param integer $boudRate
     *
     * @return EvCarUltrasonicSensor
     */
    public function setBoudRate($boudRate)
    {
        $this->boudRate = $boudRate;

        return $this;
    }

    /**
     * Get boudRate
     *
     * @return integer
     */
    public function getBoudRate()
    {
        return $this->boudRate;
    }

    /**
     * Set device
     *
     * @param \AppBundle\Entity\EmsDeviceMaster $device
     *
     * @return EvCarUltrasonicSensor
     */
    public function setDevice(\AppBundle\Entity\EmsDeviceMaster $device = null)
    {
        $this->device = $device;

        return $this;
    }

    /**
     * Get device
     *
     * @return \AppBundle\Entity\EmsDeviceMaster
     */
    public function getDevice()
    {
        return $this->device;
    }
}
