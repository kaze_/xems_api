<?php

namespace AppBundle\Entity;

use Ramsey\Uuid\Uuid;

/**
 * EvChargingStationCircuitMapping
 */
class EvChargingStationCircuitMapping
{
    /**
     * @var uuid
     */
    private $chargingStationId;

    /**
     * @var uuid
     */
    private $circuitId;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var integer
     */
    private $statusId;


    /**
     * Set chargingStationId
     *
     * @param uuid $chargingStationId
     *
     * @return EvChargingStationCircuitMapping
     */
    public function setChargingStationId($chargingStationId)
    {
        $this->chargingStationId = $chargingStationId;

        return $this;
    }

    /**
     * Get chargingStationId
     *
     * @return uuid
     */
    public function getChargingStationId()
    {
        return $this->chargingStationId;
    }

    /**
     * Set circuitId
     *
     * @param uuid $circuitId
     *
     * @return EvChargingStationCircuitMapping
     */
    public function setCircuitId($circuitId)
    {
        $this->circuitId = $circuitId;

        return $this;
    }

    /**
     * Get circuitId
     *
     * @return uuid
     */
    public function getCircuitId()
    {
        return $this->circuitId;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return EvChargingStationCircuitMapping
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set statusId
     *
     * @param integer $statusId
     *
     * @return EvChargingStationCircuitMapping
     */
    public function setStatusId($statusId)
    {
        $this->statusId = $statusId;

        return $this;
    }

    /**
     * Get statusId
     *
     * @return integer
     */
    public function getStatusId()
    {
        return $this->statusId;
    }
}

