<?php

namespace AppBundle\Entity;

/**
 * EvChargingStationManufacturerMaster
 */
class EvChargingStationManufacturerMaster
{
    /**
     * @var integer
     */
    private $chargingStationManufacturerId;

    /**
     * @var string
     */
    private $chargingStationManufacturerName;


    /**
     * Get chargingStationManufacturerId
     *
     * @return integer
     */
    public function getChargingStationManufacturerId()
    {
        return $this->chargingStationManufacturerId;
    }

    /**
     * Set chargingStationManufacturerName
     *
     * @param string $chargingStationManufacturerName
     *
     * @return EvChargingStationManufacturerMaster
     */
    public function setChargingStationManufacturerName($chargingStationManufacturerName)
    {
        $this->chargingStationManufacturerName = $chargingStationManufacturerName;

        return $this;
    }

    /**
     * Get chargingStationManufacturerName
     *
     * @return string
     */
    public function getChargingStationManufacturerName()
    {
        return $this->chargingStationManufacturerName;
    }
}
