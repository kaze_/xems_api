<?php

namespace AppBundle\Entity;

/**
 * EvChargingStationMaster
 */
class EvChargingStationMaster
{
    const STAT_NOCHANGE = 0;
    const STAT_STARTED = 1;
    const STAT_ENDING = 2;
    const STAT_ENDED = 3;
    const STAT_UNPLUGGED = 4;
    const STAT_METEROFF = 5;

    const CHARGE_NONE = 0;
    const CHARGE_MONTHLY = 1;
    const CHARGE_LOCATION = 2;
    const CHARGE_PUBLIC = 3;

    /**
     * @var string
     */
    private $chargingStationId;

    /**
     * @var string
     */
    private $name;

    /**
     * @var integer
     */
    private $statusId;

    /**
     * @var string
     */
    private $addrFloor;

    /**
     * @var string
     */
    private $addrNumber;

    /**
     * @var integer
     */
    private $maxCurrent = '0';

    /**
     * @var \AppBundle\Entity\EvChargingStationModelMaster
     */
    private $chargingStationModel;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $circuit;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $user;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->circuit = new \Doctrine\Common\Collections\ArrayCollection();
        $this->user = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get chargingStationId
     *
     * @return string
     */
    public function getChargingStationId()
    {
        return $this->chargingStationId;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return EvChargingStationMaster
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set statusId
     *
     * @param integer $statusId
     *
     * @return EvChargingStationMaster
     */
    public function setStatusId($statusId)
    {
        $this->statusId = $statusId;

        return $this;
    }

    /**
     * Get statusId
     *
     * @return integer
     */
    public function getStatusId()
    {
        return $this->statusId;
    }

    /**
     * Set addrFloor
     *
     * @param string $addrFloor
     *
     * @return EvChargingStationMaster
     */
    public function setAddrFloor($addrFloor)
    {
        $this->addrFloor = $addrFloor;

        return $this;
    }

    /**
     * Get addrFloor
     *
     * @return string
     */
    public function getAddrFloor()
    {
        return $this->addrFloor;
    }

    /**
     * Set addrNumber
     *
     * @param string $addrNumber
     *
     * @return EvChargingStationMaster
     */
    public function setAddrNumber($addrNumber)
    {
        $this->addrNumber = $addrNumber;

        return $this;
    }

    /**
     * Get addrNumber
     *
     * @return string
     */
    public function getAddrNumber()
    {
        return $this->addrNumber;
    }

    /**
     * Set maxCurrent
     *
     * @param integer $maxCurrent
     *
     * @return EvChargingStationMaster
     */
    public function setMaxCurrent($maxCurrent)
    {
        $this->maxCurrent = $maxCurrent;

        return $this;
    }

    /**
     * Get maxCurrent
     *
     * @return integer
     */
    public function getMaxCurrent()
    {
        return $this->maxCurrent;
    }

    /**
     * Set chargingStationModel
     *
     * @param \AppBundle\Entity\EvChargingStationModelMaster $chargingStationModel
     *
     * @return EvChargingStationMaster
     */
    public function setChargingStationModel(\AppBundle\Entity\EvChargingStationModelMaster $chargingStationModel = null)
    {
        $this->chargingStationModel = $chargingStationModel;

        return $this;
    }

    /**
     * Get chargingStationModel
     *
     * @return \AppBundle\Entity\EvChargingStationModelMaster
     */
    public function getChargingStationModel()
    {
        return $this->chargingStationModel;
    }

    /**
     * Add circuit
     *
     * @param \AppBundle\Entity\EmsCircuitMaster $circuit
     *
     * @return EvChargingStationMaster
     */
    public function addCircuit(\AppBundle\Entity\EmsCircuitMaster $circuit)
    {
        $this->circuit[] = $circuit;

        return $this;
    }

    /**
     * Remove circuit
     *
     * @param \AppBundle\Entity\EmsCircuitMaster $circuit
     */
    public function removeCircuit(\AppBundle\Entity\EmsCircuitMaster $circuit)
    {
        $this->circuit->removeElement($circuit);
    }

    /**
     * Get circuit
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCircuit()
    {
        return $this->circuit;
    }

    public function getValidCircuit()
    {
        if ( $this->circuit->isEmpty()) {
            return null;
        }
        return $this->circuit->get(0);
    }
    /**
     * Add user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return EvChargingStationMaster
     */
    public function addUser(\AppBundle\Entity\User $user)
    {
        $this->user[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \AppBundle\Entity\User $user
     */
    public function removeUser(\AppBundle\Entity\User $user)
    {
        $this->user->removeElement($user);
    }

    /**
     * Get user
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUser()
    {
        return $this->user;
    }
    /**
     * @var float|null
     */
    private $chargingCurrent = '0';

    /**
     * @var float|null
     */
    private $chargingThreashold = '0';


    /**
     * Set chargingCurrent.
     *
     * @param float|null $chargingCurrent
     *
     * @return EvChargingStationMaster
     */
    public function setChargingCurrent($chargingCurrent = null)
    {
        $this->chargingCurrent = $chargingCurrent;

        return $this;
    }

    /**
     * Get chargingCurrent.
     *
     * @return float|null
     */
    public function getChargingCurrent()
    {
        return $this->chargingCurrent;
    }

    /**
     * Set chargingThreashold.
     *
     * @param float|null $chargingThreashold
     *
     * @return EvChargingStationMaster
     */
    public function setChargingThreashold($chargingThreashold = null)
    {
        $this->chargingThreashold = $chargingThreashold;

        return $this;
    }

    /**
     * Get chargingThreashold.
     *
     * @return float|null
     */
    public function getChargingThreashold()
    {
        return $this->chargingThreashold;
    }
    /**
     * @var bool
     */
    private $authHome = '0';

    /**
     * @var bool
     */
    private $authQrcode = '0';

    /**
     * @var bool
     */
    private $authCarplate = '0';

    /**
     * @var int|null
     */
    private $chargeMonthly;


    /**
     * Set authHome.
     *
     * @param bool $authHome
     *
     * @return EvChargingStationMaster
     */
    public function setAuthHome($authHome)
    {
        $this->authHome = $authHome;

        return $this;
    }

    /**
     * Get authHome.
     *
     * @return bool
     */
    public function getAuthHome()
    {
        return $this->authHome;
    }

    /**
     * Set authQrcode.
     *
     * @param bool $authQrcode
     *
     * @return EvChargingStationMaster
     */
    public function setAuthQrcode($authQrcode)
    {
        $this->authQrcode = $authQrcode;

        return $this;
    }

    /**
     * Get authQrcode.
     *
     * @return bool
     */
    public function getAuthQrcode()
    {
        return $this->authQrcode;
    }

    /**
     * Set authCarplate.
     *
     * @param bool $authCarplate
     *
     * @return EvChargingStationMaster
     */
    public function setAuthCarplate($authCarplate)
    {
        $this->authCarplate = $authCarplate;

        return $this;
    }

    /**
     * Get authCarplate.
     *
     * @return bool
     */
    public function getAuthCarplate()
    {
        return $this->authCarplate;
    }

    /**
     * Set chargeMonthly.
     *
     * @param int|null $chargeMonthly
     *
     * @return EvChargingStationMaster
     */
    public function setChargeMonthly($chargeMonthly = null)
    {
        $this->chargeMonthly = $chargeMonthly;

        return $this;
    }

    /**
     * Get chargeMonthly.
     *
     * @return int|null
     */
    public function getChargeMonthly()
    {
        return $this->chargeMonthly;
    }
    /**
     * @var int|null
     */
    private $priorityCharging;


    /**
     * Set priorityCharging.
     *
     * @param int|null $priorityCharging
     *
     * @return EvChargingStationMaster
     */
    public function setPriorityCharging($priorityCharging = null)
    {
        $this->priorityCharging = $priorityCharging;

        return $this;
    }

    /**
     * Get priorityCharging.
     *
     * @return int|null
     */
    public function getPriorityCharging()
    {
        return $this->priorityCharging;
    }
    /**
     * @var int
     */
    private $chargingFeeType = '0';


    /**
     * Set chargingFeeType.
     *
     * @param int $chargingFeeType
     *
     * @return EvChargingStationMaster
     */
    public function setChargingFeeType($chargingFeeType)
    {
        $this->chargingFeeType = $chargingFeeType;

        return $this;
    }

    /**
     * Get chargingFeeType.
     *
     * @return int
     */
    public function getChargingFeeType()
    {
        return $this->chargingFeeType;
    }
    /**
     * @var string|null
     */
    private $chargingFeePolicy;


    /**
     * Set chargingFeePolicy.
     *
     * @param string|null $chargingFeePolicy
     *
     * @return EvChargingStationMaster
     */
    public function setChargingFeePolicy($chargingFeePolicy = null)
    {
        $this->chargingFeePolicy = $chargingFeePolicy;

        return $this;
    }

    /**
     * Get chargingFeePolicy.
     *
     * @return string|null
     */
    public function getChargingFeePolicy()
    {
        return $this->chargingFeePolicy;
    }
}
