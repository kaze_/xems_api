<?php

namespace AppBundle\Entity;

/**
 * EvChargingStationModelMaster
 */
class EvChargingStationModelMaster
{
    /**
     * @var integer
     */
    private $chargingStationModelId;

    /**
     * @var float
     */
    private $voltage;

    /**
     * @var float
     */
    private $maxCurrent;

    /**
     * @var string
     */
    private $chargingStationDesc;

    /**
     * @var \AppBundle\Entity\EvChargingStationManufacturerMaster
     */
    private $chargingStationManufacturer;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $evModel;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->evModel = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get chargingStationModelId
     *
     * @return integer
     */
    public function getChargingStationModelId()
    {
        return $this->chargingStationModelId;
    }

    /**
     * Set voltage
     *
     * @param float $voltage
     *
     * @return EvChargingStationModelMaster
     */
    public function setVoltage($voltage)
    {
        $this->voltage = $voltage;

        return $this;
    }

    /**
     * Get voltage
     *
     * @return float
     */
    public function getVoltage()
    {
        return $this->voltage;
    }

    /**
     * Set maxCurrent
     *
     * @param float $maxCurrent
     *
     * @return EvChargingStationModelMaster
     */
    public function setMaxCurrent($maxCurrent)
    {
        $this->maxCurrent = $maxCurrent;

        return $this;
    }

    /**
     * Get maxCurrent
     *
     * @return float
     */
    public function getMaxCurrent()
    {
        return $this->maxCurrent;
    }

    /**
     * Set chargingStationDesc
     *
     * @param string $chargingStationDesc
     *
     * @return EvChargingStationModelMaster
     */
    public function setChargingStationDesc($chargingStationDesc)
    {
        $this->chargingStationDesc = $chargingStationDesc;

        return $this;
    }

    /**
     * Get chargingStationDesc
     *
     * @return string
     */
    public function getChargingStationDesc()
    {
        return $this->chargingStationDesc;
    }

    /**
     * Set chargingStationManufacturer
     *
     * @param \AppBundle\Entity\EvChargingStationManufacturerMaster $chargingStationManufacturer
     *
     * @return EvChargingStationModelMaster
     */
    public function setChargingStationManufacturer(\AppBundle\Entity\EvChargingStationManufacturerMaster $chargingStationManufacturer = null)
    {
        $this->chargingStationManufacturer = $chargingStationManufacturer;

        return $this;
    }

    /**
     * Get chargingStationManufacturer
     *
     * @return \AppBundle\Entity\EvChargingStationManufacturerMaster
     */
    public function getChargingStationManufacturer()
    {
        return $this->chargingStationManufacturer;
    }

    /**
     * Add evModel
     *
     * @param \EvBundle\Entity\EvModelMaster $evModel
     *
     * @return EvChargingStationModelMaster
     */
    public function addEvModel(\EvBundle\Entity\EvModelMaster $evModel)
    {
        $this->evModel[] = $evModel;

        return $this;
    }

    /**
     * Remove evModel
     *
     * @param \EvBundle\Entity\EvModelMaster $evModel
     */
    public function removeEvModel(\EvBundle\Entity\EvModelMaster $evModel)
    {
        $this->evModel->removeElement($evModel);
    }

    /**
     * Get evModel
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEvModel()
    {
        return $this->evModel;
    }
}
