<?php

namespace AppBundle\Entity;

class EvDemandAllocationStrategy
{
    /**
     * @var integer
     */
    private $strategyId;

    /**
     * @var string
     */
    private $name;



    /**
     * Get strategyId.
     *
     * @return int
     */
    public function getStrategyId()
    {
        return $this->strategyId;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return EvDemandAllocationStrategy
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
