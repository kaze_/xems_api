<?php

namespace AppBundle\Entity;

/**
 * EvMobileDeviceLogin
 */
class EvMobileDeviceLogin
{
    /**
     * @var string
     */
    private $mobileDeviceId;

    /**
     * @var string
     */
    private $evUserevUserMobiephone;

    /**
     * @var \AppBundle\Entity\EvUserMaster
     */
    private $belonUser;


    /**
     * Set mobileDeviceId
     *
     * @param string $mobileDeviceId
     *
     * @return EvMobileDeviceLogin
     */
    public function setMobileDeviceId($mobileDeviceId)
    {
        $this->mobileDeviceId = $mobileDeviceId;

        return $this;
    }

    /**
     * Get mobileDeviceId
     *
     * @return string
     */
    public function getMobileDeviceId()
    {
        return $this->mobileDeviceId;
    }

    /**
     * Set evUserevUserMobiephone
     *
     * @param string $evUserevUserMobiephone
     *
     * @return EvMobileDeviceLogin
     */
    public function setEvUserevUserMobiephone($evUserevUserMobiephone)
    {
        $this->evUserevUserMobiephone = $evUserevUserMobiephone;

        return $this;
    }

    /**
     * Get evUserevUserMobiephone
     *
     * @return string
     */
    public function getEvUserevUserMobiephone()
    {
        return $this->evUserevUserMobiephone;
    }

    /**
     * Set belonUser
     *
     * @param \AppBundle\Entity\EvUserMaster $belonUser
     *
     * @return EvMobileDeviceLogin
     */
    public function setBelonUser(\AppBundle\Entity\EvUserMaster $belonUser = null)
    {
        $this->belonUser = $belonUser;

        return $this;
    }

    /**
     * Get belonUser
     *
     * @return \AppBundle\Entity\EvUserMaster
     */
    public function getBelonUser()
    {
        return $this->belonUser;
    }
}
