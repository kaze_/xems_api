<?php

namespace AppBundle\Entity;

/**
 * EvQrReader
 */
class EvQrReader
{
    /**
     * @var integer
     */
    private $connectInterface;

    /**
     * @var \AppBundle\Entity\EmsDeviceMaster
     */
    private $device;


    /**
     * Set connectInterface
     *
     * @param integer $connectInterface
     *
     * @return EvQrReader
     */
    public function setConnectInterface($connectInterface)
    {
        $this->connectInterface = $connectInterface;

        return $this;
    }

    /**
     * Get connectInterface
     *
     * @return integer
     */
    public function getConnectInterface()
    {
        return $this->connectInterface;
    }

    /**
     * Set device
     *
     * @param \AppBundle\Entity\EmsDeviceMaster $device
     *
     * @return EvQrReader
     */
    public function setDevice(\AppBundle\Entity\EmsDeviceMaster $device = null)
    {
        $this->device = $device;

        return $this;
    }

    /**
     * Get device
     *
     * @return \AppBundle\Entity\EmsDeviceMaster
     */
    public function getDevice()
    {
        return $this->device;
    }
}
