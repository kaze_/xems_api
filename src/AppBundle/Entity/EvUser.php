<?php

namespace AppBundle\Entity;

/**
 * EvUser
 */
class EvUser
{
    /**
     * @var string
     */
    private $customerId;

    /**
     * @var string
     */
    private $evUserMobilephone;

    /**
     * @var integer
     */
    private $evUserName;


    /**
     * Get customerId
     *
     * @return string
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * Set evUserMobilephone
     *
     * @param string $evUserMobilephone
     *
     * @return EvUser
     */
    public function setEvUserMobilephone($evUserMobilephone)
    {
        $this->evUserMobilephone = $evUserMobilephone;

        return $this;
    }

    /**
     * Get evUserMobilephone
     *
     * @return string
     */
    public function getEvUserMobilephone()
    {
        return $this->evUserMobilephone;
    }

    /**
     * Set evUserName
     *
     * @param integer $evUserName
     *
     * @return EvUser
     */
    public function setEvUserName($evUserName)
    {
        $this->evUserName = $evUserName;

        return $this;
    }

    /**
     * Get evUserName
     *
     * @return integer
     */
    public function getEvUserName()
    {
        return $this->evUserName;
    }
}
