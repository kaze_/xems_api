<?php

namespace AppBundle\Entity;

/**
 * EvUserMessageBoard
 */
class EvUserMessageBoard
{
    /**
     * @var string
     */
    private $messageId;

    /**
     * @var \DateTime
     */
    private $messageDatetime;

    /**
     * @var string
     */
    private $parkingLocationId;

    /**
     * @var string
     */
    private $messageContent;

    /**
     * @var string
     */
    private $evUserevUserMobiephone;

    /**
     * @var \AppBundle\Entity\EvUserMaster
     */
    private $user;


    /**
     * Set messageId
     *
     * @param string $messageId
     *
     * @return EvUserMessageBoard
     */
    public function setMessageId($messageId)
    {
        $this->messageId = $messageId;

        return $this;
    }

    /**
     * Get messageId
     *
     * @return string
     */
    public function getMessageId()
    {
        return $this->messageId;
    }

    /**
     * Set messageDatetime
     *
     * @param \DateTime $messageDatetime
     *
     * @return EvUserMessageBoard
     */
    public function setMessageDatetime($messageDatetime)
    {
        $this->messageDatetime = $messageDatetime;

        return $this;
    }

    /**
     * Get messageDatetime
     *
     * @return \DateTime
     */
    public function getMessageDatetime()
    {
        return $this->messageDatetime;
    }

    /**
     * Set parkingLocationId
     *
     * @param string $parkingLocationId
     *
     * @return EvUserMessageBoard
     */
    public function setParkingLocationId($parkingLocationId)
    {
        $this->parkingLocationId = $parkingLocationId;

        return $this;
    }

    /**
     * Get parkingLocationId
     *
     * @return string
     */
    public function getParkingLocationId()
    {
        return $this->parkingLocationId;
    }

    /**
     * Set messageContent
     *
     * @param string $messageContent
     *
     * @return EvUserMessageBoard
     */
    public function setMessageContent($messageContent)
    {
        $this->messageContent = $messageContent;

        return $this;
    }

    /**
     * Get messageContent
     *
     * @return string
     */
    public function getMessageContent()
    {
        return $this->messageContent;
    }

    /**
     * Set evUserevUserMobiephone
     *
     * @param string $evUserevUserMobiephone
     *
     * @return EvUserMessageBoard
     */
    public function setEvUserevUserMobiephone($evUserevUserMobiephone)
    {
        $this->evUserevUserMobiephone = $evUserevUserMobiephone;

        return $this;
    }

    /**
     * Get evUserevUserMobiephone
     *
     * @return string
     */
    public function getEvUserevUserMobiephone()
    {
        return $this->evUserevUserMobiephone;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\EvUserMaster $user
     *
     * @return EvUserMessageBoard
     */
    public function setUser(\AppBundle\Entity\EvUserMaster $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\EvUserMaster
     */
    public function getUser()
    {
        return $this->user;
    }
}
