<?php

namespace AppBundle\Entity;

/**
 * EvUserTransationLog
 */
class EvUserTransationLog
{
    /**
     * @var \DateTime
     */
    private $datetime;

    /**
     * @var integer
     */
    private $statusId;

    /**
     * @var string
     */
    private $statusDesc;

    /**
     * @var \AppBundle\Entity\EvUserMaster
     */
    private $user;


    /**
     * Set datetime
     *
     * @param \DateTime $datetime
     *
     * @return EvUserTransationLog
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;

        return $this;
    }

    /**
     * Get datetime
     *
     * @return \DateTime
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

    /**
     * Set statusId
     *
     * @param integer $statusId
     *
     * @return EvUserTransationLog
     */
    public function setStatusId($statusId)
    {
        $this->statusId = $statusId;

        return $this;
    }

    /**
     * Get statusId
     *
     * @return integer
     */
    public function getStatusId()
    {
        return $this->statusId;
    }

    /**
     * Set statusDesc
     *
     * @param string $statusDesc
     *
     * @return EvUserTransationLog
     */
    public function setStatusDesc($statusDesc)
    {
        $this->statusDesc = $statusDesc;

        return $this;
    }

    /**
     * Get statusDesc
     *
     * @return string
     */
    public function getStatusDesc()
    {
        return $this->statusDesc;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\EvUserMaster $user
     *
     * @return EvUserTransationLog
     */
    public function setUser(\AppBundle\Entity\EvUserMaster $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\EvUserMaster
     */
    public function getUser()
    {
        return $this->user;
    }
}
