<?php

namespace AppBundle\Entity;

/**
 * LoginToken
 */
class LoginToken
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $os;

    /**
     * @var string
     */
    private $deviceId;

    /**
     * @var string
     */
    private $deviceToken;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var string
     */
    private $sessionKey;

    /**
     * @var boolean
     */
    private $isEnabled;

    /**
     * @var \AppBundle\Entity\User
     */
    private $user;

    /**
     * @var \DateTime
     */
    private $updatedAt;

    const OS_ANDROID = 'android';
    const OS_IOS = 'ios';
    
    public function __construct() {
        $this->createdAt = new \DateTime();
        $this->isEnabled = true;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set os
     *
     * @param string $os
     *
     * @return LoginToken
     */
    public function setOS($os)
    {
        $this->os = $os;

        return $this;
    }

    /**
     * Get os
     *
     * @return string
     */
    public function getOS()
    {
        return $this->os;
    }
    
    /**
     * Set deviceId
     *
     * @param string $deviceId
     *
     * @return LoginToken
     */
    public function setDeviceId($deviceId)
    {
        $this->deviceId = $deviceId;

        return $this;
    }

    /**
     * Get deviceId
     *
     * @return string
     */
    public function getDeviceId()
    {
        return $this->deviceId;
    }
    
    /**
     * Set deviceToken
     *
     * @param string $deviceToken
     *
     * @return LoginToken
     */
    public function setDeviceToken($deviceToken)
    {
        $this->deviceToken = $deviceToken;

        return $this;
    }

    /**
     * Get deviceToken
     *
     * @return string
     */
    public function getDeviceToken()
    {
        return $this->deviceToken;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return LoginToken
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set sessionKey
     *
     * @param string $sessionKey
     *
     * @return LoginToken
     */
    public function setSessionKey($sessionKey)
    {
        $this->sessionKey = $sessionKey;

        return $this;
    }

    /**
     * Get sessionKey
     *
     * @return string
     */
    public function getSessionKey()
    {
        return $this->sessionKey;
    }

    /**
     * Set isEnabled
     *
     * @param boolean $isEnabled
     *
     * @return LoginToken
     */
    public function setIsEnabled($isEnabled)
    {
        $this->isEnabled = $isEnabled;

        return $this;
    }

    /**
     * Get isEnabled
     *
     * @return boolean
     */
    public function getIsEnabled()
    {
        return $this->isEnabled;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return LoginToken
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set updatedAt
     *
     * @param \datetime: $updatedAt
     *
     * @return LoginToken
     */
    public function setUpdatedAt(\Datetime $updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \datetime:
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function setUpdated()
    {
        $this->updatedAt = new \Datetime();
    }
}
