<?php

namespace AppBundle\Entity;

/**
 * SysCountryList
 */
class SysCountryList
{
    /**
     * @var integer
     */
    private $countryId;

    /**
     * @var string
     */
    private $countryName;


    /**
     * Get countryId
     *
     * @return integer
     */
    public function getCountryId()
    {
        return $this->countryId;
    }

    /**
     * Set countryName
     *
     * @param string $countryName
     *
     * @return SysCountryList
     */
    public function setCountryName($countryName)
    {
        $this->countryName = $countryName;

        return $this;
    }

    /**
     * Get countryName
     *
     * @return string
     */
    public function getCountryName()
    {
        return $this->countryName;
    }
}
