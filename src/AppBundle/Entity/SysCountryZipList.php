<?php

namespace AppBundle\Entity;

/**
 * SysCountryZipList
 */
class SysCountryZipList
{
    /**
     * @var integer
     */
    private $countryZip;

    /**
     * @var string
     */
    private $countryZipName;

    /**
     * @var \AppBundle\Entity\SysCountryList
     */
    private $country;


    /**
     * Set countryZip
     *
     * @param integer $countryZip
     *
     * @return SysCountryZipList
     */
    public function setCountryZip($countryZip)
    {
        $this->countryZip = $countryZip;

        return $this;
    }

    /**
     * Get countryZip
     *
     * @return integer
     */
    public function getCountryZip()
    {
        return $this->countryZip;
    }

    /**
     * Set countryZipName
     *
     * @param string $countryZipName
     *
     * @return SysCountryZipList
     */
    public function setCountryZipName($countryZipName)
    {
        $this->countryZipName = $countryZipName;

        return $this;
    }

    /**
     * Get countryZipName
     *
     * @return string
     */
    public function getCountryZipName()
    {
        return $this->countryZipName;
    }

    /**
     * Set country
     *
     * @param \AppBundle\Entity\SysCountryList $country
     *
     * @return SysCountryZipList
     */
    public function setCountry(\AppBundle\Entity\SysCountryList $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \AppBundle\Entity\SysCountryList
     */
    public function getCountry()
    {
        return $this->country;
    }
}
