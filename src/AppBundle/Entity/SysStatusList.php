<?php

namespace AppBundle\Entity;

/**
 * SysStatusList
 */
class SysStatusList
{
    /**
     * @var integer
     */
    private $statusId;

    /**
     * @var string
     */
    private $statusName;


    /**
     * Get statusId
     *
     * @return integer
     */
    public function getStatusId()
    {
        return $this->statusId;
    }

    /**
     * Set statusName
     *
     * @param string $statusName
     *
     * @return SysStatusList
     */
    public function setStatusName($statusName)
    {
        $this->statusName = $statusName;

        return $this;
    }

    /**
     * Get statusName
     *
     * @return string
     */
    public function getStatusName()
    {
        return $this->statusName;
    }
}
