<?php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Ramsey\Uuid\Uuid;

/**
 * User
 *
 */
class User extends BaseUser
{
    /**
     * @var uuid
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string")
     */
    private $phone;

    private $loginTokens;
    
    /**
     * Get id
     *
     * @return uuid
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Get customerId
     *
     * @return uuid
     */
    public function getCustomerId()
    {
        if ( $this->getCustomer() != null ) {
            return $this->getCustomer()->getCustomerId();
        } else {
            return null;
        } //return $this->customerId;
    }

    /**
     * Add loginToken
     *
     * @param \AppBundle\Entity\LoginToken $loginToken
     *
     * @return User
     */
/*    public function addLoginToken(\AppBundle\Entity\LoginToken $loginToken)
    {
        $this->loginTokens[] = $loginToken;

        return $this;
    }*/

    /**
     * Remove loginToken
     *
     * @param \AppBundle\Entity\LoginToken $loginToken
     */
/*    public function removeLoginToken(\AppBundle\Entity\LoginToken $loginToken)
    {
        $this->loginTokens->removeElement($loginToken);
    }*/

    /**
     * Get loginTokens
     *
     * @return \Doctrine\Common\Collections\Collection
     */
/*    public function getLoginTokens()
    {
        return $this->loginTokens;
    }*/

    /**
     * @var \AppBundle\Entity\EmsCustomerMaster
     */
    private $customer;


    /**
     * Set customer
     *
     * @param \AppBundle\Entity\EmsCustomerMaster $customer
     *
     * @return User
     */
    public function setCustomer(\AppBundle\Entity\EmsCustomerMaster $customer = null)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return \AppBundle\Entity\EmsCustomerMaster
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @var \EvBundle\Entity\EvUserDetail
     */
    private $detail;
    
    /**
     * Set detail
     *
     * @param \EvBundle\Entity\EvUserDetail $detail
     *
     * @return User
     */
    public function setDetail(\EvBundle\Entity\EvUserDetail $detail = null)
    {
        $this->detail = $detail;

        return $this;
    }

    /**
     * Get detail
     *
     * @return \EvBundle\Entity\EvUserDetail
     */
    public function getDetail()
    {
        return $this->detail;
    }
}
