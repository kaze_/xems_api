<?php
/**
 * Created by PhpStorm.
 * User: chiehyingpan
 * Date: 2017/6/19
 * Time: 下午4:55
 */

namespace AppBundle\Entity\Validate;

use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Entity\Validate\TimePeriodParametersTraits;

class ApiParameters
{
    use TimePeriodParametersTrait;

    /**
     * @Assert\Uuid(
     *     strict=false,
     *     groups={"location"})
     * @Assert\NotBlank()
     */
    private $location;

    /**
     * @Assert\Uuid(
     *     strict=false,
     *     groups={"device"})
     * @Assert\NotBlank()
     */
    private $device;

    /**
     * @Assert\Type(
     *     type = "digit",
     *     groups={"device"})
     */
    private $deviceType;

    /**
     * @return mixed
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param mixed $location
     */
    public function setLocation($location)
    {
        $this->location = $location;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDevice()
    {
        return $this->device;
    }

    /**
     * @param mixed $device
     */
    public function setDevice($device)
    {
        $this->device = $device;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDeviceType()
    {
        return $this->deviceType;
    }

    /**
     * @param mixed $deviceType
     */
    public function setDeviceType($deviceType)
    {
        $this->deviceType = $deviceType;
        return $this;
    }


}
