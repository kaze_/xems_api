<?php
/**
 * Created by PhpStorm.
 * User: chiehyingpan
 * Date: 2017/6/19
 * Time: 下午5:09
 */

namespace AppBundle\Entity\Validate;

use Symfony\Component\Validator\Constraints as Assert;

trait TimePeriodParametersTrait
{
    /**
     * @Assert\DateTime(groups={"timePeriod", "simpleTimePeriod"})
     * @Assert\NotBlank()
     */
    protected $from;
    /**
     * @Assert\DateTime(groups={"timePeriod", "simpleTimePeriod"})
     * @Assert\NotBlank()
     */
    protected $to;
    /**
     * @Assert\Type("digit", groups={"timePeriod"})
     */
    protected $offset;
    /**
     * @Assert\Type("digit", groups={"timePeriod"})
     */
    protected $limit;

    /**
     * @return mixed
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param mixed $from
     */
    public function setFrom($from)
    {
        $this->from = $from;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @param mixed $to
     */
    public function setTo($to)
    {
        $this->to = $to;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * @param mixed $offset
     */
    public function setOffset($offset)
    {
        $this->offset = $offset;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param mixed $limit
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
        return $this;
    }


}
