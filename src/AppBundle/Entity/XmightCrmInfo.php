<?php

namespace AppBundle\Entity;

/**
 * XmightCrmInfo
 */
class XmightCrmInfo
{
    /**
     * @var \DateTime
     */
    private $crmDatetime;

    /**
     * @var \DateTime
     */
    private $customerDisableDatetime;

    /**
     * @var integer
     */
    private $statusId;

    /**
     * @var string
     */
    private $description;

    /**
     * @var \AppBundle\Entity\EmsCustomerMaster
     */
    private $customer;


    /**
     * Set crmDatetime
     *
     * @param \DateTime $crmDatetime
     *
     * @return XmightCrmInfo
     */
    public function setCrmDatetime($crmDatetime)
    {
        $this->crmDatetime = $crmDatetime;

        return $this;
    }

    /**
     * Get crmDatetime
     *
     * @return \DateTime
     */
    public function getCrmDatetime()
    {
        return $this->crmDatetime;
    }

    /**
     * Set customerDisableDatetime
     *
     * @param \DateTime $customerDisableDatetime
     *
     * @return XmightCrmInfo
     */
    public function setCustomerDisableDatetime($customerDisableDatetime)
    {
        $this->customerDisableDatetime = $customerDisableDatetime;

        return $this;
    }

    /**
     * Get customerDisableDatetime
     *
     * @return \DateTime
     */
    public function getCustomerDisableDatetime()
    {
        return $this->customerDisableDatetime;
    }

    /**
     * Set statusId
     *
     * @param integer $statusId
     *
     * @return XmightCrmInfo
     */
    public function setStatusId($statusId)
    {
        $this->statusId = $statusId;

        return $this;
    }

    /**
     * Get statusId
     *
     * @return integer
     */
    public function getStatusId()
    {
        return $this->statusId;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return XmightCrmInfo
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set customer
     *
     * @param \AppBundle\Entity\EmsCustomerMaster $customer
     *
     * @return XmightCrmInfo
     */
    public function setCustomer(\AppBundle\Entity\EmsCustomerMaster $customer = null)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return \AppBundle\Entity\EmsCustomerMaster
     */
    public function getCustomer()
    {
        return $this->customer;
    }
}

