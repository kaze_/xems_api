<?php

namespace AppBundle\Entity;

/**
 * XmightServiceMaster
 */
class XmightServiceMaster
{
    /**
     * @var integer
     */
    private $xserviceId;

    /**
     * @var string
     */
    private $xserviceName;

    /**
     * @var integer
     */
    private $xserviceMonthlyFee;

    /**
     * @var integer
     */
    private $xserviceInitialFee;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $customer;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->customer = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get xserviceId
     *
     * @return integer
     */
    public function getXserviceId()
    {
        return $this->xserviceId;
    }

    /**
     * Set xserviceName
     *
     * @param string $xserviceName
     *
     * @return XmightServiceMaster
     */
    public function setXserviceName($xserviceName)
    {
        $this->xserviceName = $xserviceName;

        return $this;
    }

    /**
     * Get xserviceName
     *
     * @return string
     */
    public function getXserviceName()
    {
        return $this->xserviceName;
    }

    /**
     * Set xserviceMonthlyFee
     *
     * @param integer $xserviceMonthlyFee
     *
     * @return XmightServiceMaster
     */
    public function setXserviceMonthlyFee($xserviceMonthlyFee)
    {
        $this->xserviceMonthlyFee = $xserviceMonthlyFee;

        return $this;
    }

    /**
     * Get xserviceMonthlyFee
     *
     * @return integer
     */
    public function getXserviceMonthlyFee()
    {
        return $this->xserviceMonthlyFee;
    }

    /**
     * Set xserviceInitialFee
     *
     * @param integer $xserviceInitialFee
     *
     * @return XmightServiceMaster
     */
    public function setXserviceInitialFee($xserviceInitialFee)
    {
        $this->xserviceInitialFee = $xserviceInitialFee;

        return $this;
    }

    /**
     * Get xserviceInitialFee
     *
     * @return integer
     */
    public function getXserviceInitialFee()
    {
        return $this->xserviceInitialFee;
    }

    /**
     * Add customer
     *
     * @param \AppBundle\Entity\EmsCustomerMaster $customer
     *
     * @return XmightServiceMaster
     */
    public function addCustomer(\AppBundle\Entity\EmsCustomerMaster $customer)
    {
        $this->customer[] = $customer;

        return $this;
    }

    /**
     * Remove customer
     *
     * @param \AppBundle\Entity\EmsCustomerMaster $customer
     */
    public function removeCustomer(\AppBundle\Entity\EmsCustomerMaster $customer)
    {
        $this->customer->removeElement($customer);
    }

    /**
     * Get customer
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCustomer()
    {
        return $this->customer;
    }
}
