<?php

namespace AppBundle\EventListener;

use AppBundle\Services\EventLogService;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\Event\PostResponseEvent;

class APILogsEventListener
{
    private $em;
    private $logger;
    private $eventLogService;

    public function __construct(LoggerInterface $logger, EventLogService $eventLogService)
    {
        $this->logger = $logger;
        $this->eventLogService = $eventLogService;
    }

    /* API responsed */
    public function onKernelTerminate(PostResponseEvent $event)
    {
        $request = $event->getRequest();
        $response = $event->getResponse();
        try {
            $this->eventLogService->logApiEvent($request, $response);
        } catch (Exception $e) {
            $this->logger->error('error when log api', [$e->getMessage()]);
        }
    }
}