<?php
/**
 * Created by PhpStorm.
 * User: chiehyingpan
 * Date: 2018/4/9
 * Time: 下午2:44
 */

namespace AppBundle\EventListener;


use Doctrine\ORM\Event\LifecycleEventArgs;
use AppBundle\Entity\EmsPowerReportLog;
use AppBundle\Entity\EmsSensorReportLog;

class PowerReport
{
    private $prefixFunA = 'getA';
    private $prefixFunV = 'getV';
    private $prefixFunKw = 'getKw';

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if (!$entity instanceof EmsPowerReportLog) {
            return;
        }

        $em = $args->getEntityManager();
        $circuit = $em->getRepository('AppBundle:EmsCircuitMaster')->find($entity->getCircuitId());
        if ( $circuit ) {
            $phaseJson = $circuit->getCircuitPhase();
            $phase = json_decode($phaseJson);
            if ( $phase && !empty($phase)) {
                $factor = 0;
                $totalV = 0;
                $totalA = 0;
                $totalKw = 0;
                foreach($phase as $p ) {
                    $factor++;
                    $funA = $this->prefixFunA.strtoupper($p);
                    $funV = $this->prefixFunV.strtoupper($p);
                    $funKw = $this->prefixFunKw.strtoupper($p);
                    if ( method_exists($entity, $funA) )
                        $totalA += $entity->$funA();
                    if ( method_exists($entity, $funV) )
                        $totalV += $entity->$funV();
                    if ( method_exists($entity, $funKw) )
                        $totalKw += $entity->$funKw();
                }
                if ( $entity->getAAverage() == null ) $entity->setAAverage($totalA/$factor);
                if ( $entity->getVAverage() == null ) $entity->setVAverage($totalV/$factor);
                if ( $entity->getKwTotal() == null ) $entity->setKwTotal($totalKw);
                return ;
            }
            // if no phase is set in circuit
            $factor = 0;
            if ( $entity->getVA() != null || $entity->getVA() != 0 ) $factor++;
            if ( $entity->getVB() != null || $entity->getVB() != 0 ) $factor++;
            if ( $entity->getVC() != null || $entity->getVC() != 0 ) $factor++;

            if ( $factor == 0 ) return ;

            if ( $entity->getVAverage() == null ) {
                $entity->setVAverage (( $entity->getVA() + $entity->getVB() + $entity->getVC() )/ $factor);
            }

            // maybe /2 or /1
            if ( $entity->getAAverage() == null ) {
                $entity->setAAverage(( $entity->getAA() + $entity->getAB() + $entity->getAC() )/ $factor);
            }

            if ( $entity->getKwTotal() == null  ) {
                $entity->setKwTotal($entity->getKwA() + $entity->getKwhB() + $entity->getKwC());
            }
            return ;
        }
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();
        
        $collector = null;
        if ($entity instanceof EmsPowerReportLog) {
            $collector = $em->getRepository('AppBundle:EmsCircuitMaster')->find($entity->getCircuitId());
        } else  if ( $entity instanceof EmsSensorReportLog ) {
            $collector = $em->getRepository('AppBundle:EmsSensor')->find($entity->getCircuitId());
        } else {
            return ;
        }
         
        if ( $collector ) {
            $collector->setLastDataReport(new \DateTime());
            $collector->setNewestData($entity->getReportDatetime());
            $em->persist($collector);
            $em->flush();
        }
    }
}
