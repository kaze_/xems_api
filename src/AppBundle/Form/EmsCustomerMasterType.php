<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
class EmsCustomerMasterType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('customerId', null, array(
                'label' => 'label.customer.uuid',
                'disabled' => true ))
            ->add('customerName', null, array('label' => 'label.customer.name'))
            ->add('customerEmail', null, array('label' => 'label.customer.contact.email'))
            ->add('customerContactPhone', null, array('label' => 'label.customer.contact.phone'))
            ->add('customerRegisterMobileNumber', null, array('label' => 'label.customer.registration.phone'))
            ->add('statusId', null, array('label' => 'label.status.id'))
            ->add('customerId', null, array('label' => 'label.customer.registration_id'))
            ->add('contactName', null, array('label' => 'label.customer.contact.name'))
            ->add('enableDatetime', null, array(
                'label' => 'label.enabled.datetime',
                'disabled' => true,
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd HH:mm:ss'));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\EmsCustomerMaster'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_emscustomermaster';
    }


}
