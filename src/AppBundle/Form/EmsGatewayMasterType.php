<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
class EmsGatewayMasterType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('gatewayId', null, array('disabled' => true ))
            ->add('gatewayName')
            ->add('gatewayEthernetMac')
            ->add('gatewaySwVersion')
            ->add('gatewayMeta')
            ->add('ipAddress')
            ->add('account')
            ->add('password')
            ->add('localIp')
            ->add('comment');
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\EmsGatewayMaster',
            'allow_extra_fields' => true
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_emsgatewaymaster';
    }


}
