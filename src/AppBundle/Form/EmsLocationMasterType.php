<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
class EmsLocationMasterType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('locationId', null, array('disabled' => true ))
            ->add('locationName')
            ->add('locationAddress')
            ->add('countryZip')
            ->add('locationCountryId')
            ->add('locationStatus')
            ->add('gpsN')
            ->add('gpsE');
//            ->add('locationHierarchy', EmsLocationHierarchyType::class)
//            ->add('locationType', EmsLocationTypeType::class);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\EmsLocationMaster',
            'allow_extra_fields' => true,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_emslocationmaster';
    }


}
