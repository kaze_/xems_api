<?php

namespace AppBundle\Form;

use Doctrine\DBAL\Types\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EmsPowerReportLogType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
/*        $builder->add('circuitId')->add('reportDatetime', null, array('widget' => 'single_text', 'input' => 'string', 'date_format' => 'yyyy-MM-dd HH:mm:ss'))
            ->add('vA')->add('vB')->add('vC')->add('vAverage')
            ->add('aA')->add('aB')->add('aC')->add('aAverage')
            ->add('kwA')->add('kwB')->add('kwC')->add('kwTotal')
            ->add('kwhA')->add('kwhB')->add('kwhC')->add('kwhTotal')
            ->add('kvahTotal')
            ->add('kvaA')->add('kvaB')->add('kvaC')->add('kvaTotal')
            ->add('currLgstdmdKw')->add('currRateId')->add('frequency')
            ->add('pfA')->add('pfB')->add('pfC')->add('pfTotal')
            ->add('kvarhA')->add('kvarhB')->add('kvarhC')->add('kvarhTotal')
            ->add('kvarA')->add('kvarB')->add('kvarC')->add('kvarTotal')
            ->add('relayStatus')
            ->add('touALgstdmdaccKw')->add('touALgstdmdcontaccKw')->add('touATotalKwh')
            ->add('touBLgstdmdaccKw')->add('touBLgstdmdcontaccKw')->add('touBTotalKwh')
            ->add('touCLgstdmdaccKw')->add('touCLgstdmdcontaccKw')->add('touCTotalKwh')
            ->add('touDLgstdmdaccKw')->add('touDLgstdmdcontaccKw')->add('touDTotalKwh')
            ->add('touALgstdmdKw')->add('touBLgstdmdKw')->add('touCLgstdmdKw')->add('touDLgstdmdKw')
            ->add('currDmdKw')->add('modbusId')->add('currDmdRemainTime')->add('circuit');*/

        $builder->add('circuit', TextareaType::class, array('property_path' => 'circuitId'))
            ->add('reportTime', DateTimeType::class, array('property_path' => 'reportDateTime', 'widget' => 'single_text', 'input' => 'string', 'date_format' => 'yyyy-MM-dd HH:mm:ss'))

            ->add('currentA', null, array('property_path' => 'aA'))
            ->add('currentB', null, array('property_path' => 'aB'))
            ->add('currentC', null, array('property_path' => 'aC'))
            ->add('currentAverage', null, array('property_path' => 'aAverage'))

            ->add('voltageA', null, array('property_path' => 'vA'))
            ->add('voltageB', null, array('property_path' => 'vB'))
            ->add('voltageC', null, array('property_path' => 'vC'))
            ->add('voltageAverage', null, array('property_path' => 'vAverage'))
            // power (kW)
            ->add('powerA', null, array('property_path' => 'kwA'))
            ->add('powerB', null, array('property_path' => 'kwB'))
            ->add('powerC', null, array('property_path' => 'kwC'))
            ->add('powerTotal', null, array('property_path'  => 'kwTotal'))
            // Energy (kWh)
            ->add('energyA', null, array('property_path' => 'kwhA'))
            ->add('energyB', null, array('property_path' => 'kwhB'))
            ->add('energyC', null, array('property_path' => 'kwhC'))
            ->add('energyTotal', null, array('property_path' => 'kwhTotal'))
            // Apparent Power/Energy
            ->add('apparentPowerA', null, array('property_path' => 'kvaA'))
            ->add('apparentPowerB', null, array('property_path' => 'kvaB'))
            ->add('apparentPowerC', null, array('property_path' => 'kvaC'))
            ->add('apparentPowerTotal', null, array('property_path' => 'kvaTotal'))
            ->add('apparentEnergyTotal', null, array('property_path' => 'kvahTotal'))
            // Power Factor
            ->add('powerFactorA', null, array('property_path' => 'pfA'))
            ->add('powerFactorB', null, array('property_path' => 'pfB'))
            ->add('powerFactorC', null, array('property_path' => 'pfC'))
            ->add('powerFactorTotal', null, array('property_path' => 'pfTotal'))
            // Reactive Energy (kVArh)
            ->add('reactiveEnergyA', null, array('property_path' => 'kvarhA'))
            ->add('reactiveEnergyB', null, array('property_path' => 'kvarhB'))
            ->add('reactiveEnergyC', null, array('property_path' => 'kvarhC'))
            ->add('reactiveEnergyTotal', null, array('property_path' => 'kvarhTotal'))
            // Reactive Power
            ->add('reactivePowerA', null, array('property_path' => 'kvarA'))
            ->add('reactivePowerB', null, array('property_path' => 'kvarB'))
            ->add('reactivePowerC', null, array('property_path' => 'kvarC'))
            ->add('reactivePowerTotal', null, array('property_path' => 'kvarTotal'))
            // time of use (TOU)
            ->add('touA_power_largestDemand', null, array('property_path' => 'touALgstdmdKw'))
            ->add('touA_power_largestDemand_acc', null, array('property_path' => 'touALgstdmdaccKw'))
            ->add('touA_power_largestDemand_contAcc', null, array('property_path' => 'touALgstdmdcontaccKw'))
            ->add('touA_totalEnergy', null, array('property_path' => 'touATotalKwh'))
            ->add('touB_power_largestDemand', null, array('property_path' => 'touBLgstdmdKw'))
            ->add('touB_power_largestDemand_acc', null, array('property_path' => 'touBLgstdmdaccKw'))
            ->add('touB_power_largestDemand_contAcc', null, array('property_path' => 'touBLgstdmdcontaccKw'))
            ->add('touB_totalEnergy', null, array('property_path' => 'touBTotalKwh'))
            ->add('touC_power_largestDemand', null, array('property_path' => 'touCLgstdmdKw'))
            ->add('touC_power_largestDemand_acc', null, array('property_path' => 'touCLgstdmdaccKw'))
            ->add('touC_power_largestDemand_contAcc', null, array('property_path' => 'touCLgstdmdcontaccKw'))
            ->add('touC_totalEnergy', null, array('property_path' => 'touCTotalKwh'))
            ->add('touD_power_largestDemand', null, array('property_path' => 'touDLgstdmdKw'))
            ->add('touD_power_largestDemand_acc', null, array('property_path' => 'touDLgstdmdaccKw'))
            ->add('touD_power_largestDemand_contAcc', null, array('property_path' => 'touDLgstdmdcontaccKw'))
            ->add('touD_totalEnergy', null, array('property_path' => 'touDTotalKwh'))
            ->add('currentLargestPowerDemand', null, array('property_path' => 'currLgstdmdKw'))

            ->add('currentRateId', null, array('property_path' => 'currRateId'))
            ->add('frequency')->add('relayStatus')
            ->add('curr_dmd_kw', null, array('property_path' => 'currDmdKw'))
            ->add('modbus_id', null, array('property_path' => 'modbusId'))
            ->add('curr_dmd_remain_time', null, array('property_path' => 'currDmdRemainTime'));

        $builder->get('reportTime')
            ->addModelTransformer(new CallbackTransformer(
                function ($origionalDescription) {
                    if ( $origionalDescription == null ) {
                        return "";
                    }
                    return $origionalDescription->format('Y-m-d H:i:s');
                },
                function ($submittedDescription) {
                    return (new \DateTime())->setTimestamp(strtotime($submittedDescription));
                }
            ));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\EmsPowerReportLog',
            'csrf_protection' => false,
            'allow_extra_fields' => true,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_emspowerreportlog';
    }


}
