<?php

namespace AppBundle\Form;

use Doctrine\DBAL\Types\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EmsSensorReportLogType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('circuit', TextareaType::class, array('property_path' => 'circuitId'))
            ->add('reportTime', DateTimeType::class, array('property_path' => 'reportDateTime', 'widget' => 'single_text', 'input' => 'string', 'date_format' => 'yyyy-MM-dd HH:mm:ss'))
            ->add('modbus_id', null, array('property_path' => 'modbusId'))
            ->add('temperature')
            ->add('humidity')
            ->add('radiant')
            ->add('windDirection')
            ->add('windSpeed')
            ->add('rainfall');

        $builder->get('reportTime')
            ->addModelTransformer(new CallbackTransformer(
                function ($origionalDescription) {
                    if ( $origionalDescription == null ) {
                        return "";
                    }
                    return $origionalDescription->format('Y-m-d H:i:s');
                },
                function ($submittedDescription) {
                    return (new \DateTime())->setTimestamp(strtotime($submittedDescription));
                }
            ));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\EmsSensorReportLog',
            'csrf_protection' => false,
            'allow_extra_fields' => true,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_emssensorreportlog';
    }


}
