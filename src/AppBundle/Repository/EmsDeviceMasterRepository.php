<?php
/**
 * Created by PhpStorm.
 * User: chiehyingpan
 * Date: 2017/6/17
 * Time: 上午12:39
 */

namespace AppBundle\Repository;


use AppBundle\Entity\EmsLocationMaster;
use Doctrine\ORM\EntityRepository;

class EmsDeviceMasterRepository extends EntityRepository
{
    public function findWithDeviceTable($location, $device, $deviceEntities)
    {
        $qb2 = $this->createQueryBuilder('d');
        $qb2->addSelect('d')
            ->from('AppBundle:DeviceMaster','d');
        //->select('d.deviceUuid, d.deviceName, d.deviceTypeId');
        $entityLen = count($deviceEntities);
        for ( $i = 0; $i < $entityLen; $i++) {
            $entity = 'AppBundle:'.$deviceEntities[$i];
            $alias = 'd'.$i;
            $qb2->leftJoin($entity, $alias, 'WITH', $alias.'.deviceUuid = d.deviceUuid')
                ->addSelect($alias);
        }
        $qb2->innerJoin('AppBundle:GatewayDeviceMapping', 'gm', 'WITH', 'd.deviceUuid = gm.deviceUuid')
        ->innerJoin('AppBundle:LocationGatewayMapping', 'lm', 'WITH', 'gm.gatewayUuid = lm.gatewayUuid')
        ->where('lm.locationUuid = :location')
        ->andWhere('d.deviceUuid = :device')
        ->setParameter('device', hex2bin($device))
        ->setParameter('location', hex2bin($location));

        $result = $qb2->getQuery()->getResult();
        return $result;

    }

    public function findByLocation(EmsLocationMaster $location, $deviceType)
    {
        $qb = $this->createQueryBuilder('d');
        $qb->join('d.deviceType', 'dt')
            ->innerJoin('AppBundle:EmsGatewayDeviceMapping', 'gm', 'WITH', 'd.deviceId = gm.deviceId')
            ->innerJoin('AppBundle:EmsLocationGatewayMapping', 'lm', 'WITH', 'gm.gatewayId = lm.gatewayId')
            ->where('lm.locationId = :location')
            ->andWhere('lm.statusId = 1')
            ->setParameter('location', $location->getLocationId(), 'uuid');
        if ($deviceType != null) {
            $qb->andWhere('dt.deviceTypeId = :deviceType')
                ->setParameter('deviceType', $deviceType);
        }

        $result = $qb->getQuery()->getResult();
        return $result;
    }
}
