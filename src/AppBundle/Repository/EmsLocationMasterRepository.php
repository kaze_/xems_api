<?php
/**
 * Created by PhpStorm.
 * User: chiehyingpan
 * Date: 2018/2/5
 * Time: 下午12:02
 */

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class EmsLocationMasterRepository extends  EntityRepository
{
    public function findByChargingStation($chargingStationId)
    {
        $qb = $this->createQueryBuilder('l');
        $qb->innerJoin('AppBundle:EmsLocationGatewayMapping', 'lg', 'WITH', 'lg.locationId = l.locationId and lg.statusId = 1')
            ->innerJoin('AppBundle:EmsGatewayDeviceMapping', 'gd', 'WITH', 'gd.gatewayId = lg.gatewayId and gd.statusId = 1')
            ->innerJoin('AppBundle:EmsMeterCircuitMapping', 'mc', 'WITH', 'mc.device = gd.deviceId')
            ->innerJoin('AppBundle:EvChargingStationCircuitMapping', 'cc', 'WITH', 'cc.circuitId = mc.circuit and cc.chargingStationId = :station and cc.statusId = 1')
            ->setParameter(':station', $chargingStationId, 'uuid' );

        $result = $qb->getQuery()->getOneOrNullResult();
        return $result;
    }

    public function findByCircuit($circuitId)
    {
        $qb = $this->createQueryBuilder('l');
        $qb->innerJoin('AppBundle:EmsLocationGatewayMapping', 'lg', 'WITH', 'lg.locationId = l.locationId and lg.statusId = 1')
            ->innerJoin('AppBundle:EmsGatewayDeviceMapping', 'gd', 'WITH', 'gd.gatewayId = lg.gatewayId and gd.statusId = 1')
            //->innerJoin('AppBundle:EmsMeterCircuitMapping', 'mc', 'WITH', 'mc.device = gd.deviceId and mc.circuit = :circuit' )
            //->setParameter(':circuit', $circuitId, 'uuid' );
            ->innerJoin('AppBundle:EmsMeterCircuitMapping', 'mc', 'WITH', 'mc.device = gd.deviceId' )
            ->join('mc.circuit', 'c')
            ->where('c.circuitId = :circuit')
            ->setParameter('circuit', $circuitId);

        $result = $qb->getQuery()->getOneOrNullResult();
        return $result;
    }
}