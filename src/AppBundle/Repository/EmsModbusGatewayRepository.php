<?php
/**
 * Created by PhpStorm.
 * User: chiehyingpan
 * Date: 2018/1/17
 * Time: 下午3:36
 */

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class EmsModbusGatewayRepository extends EntityRepository
{
    public function findDeviceConfigurtionsByGateway($gatewayId)
    {
        $qb = $this->createQueryBuilder('d');
        $qb->innerJoin('AppBundle:EmsGatewayDeviceMapping', 'gd', 'WITH', 'gd.deviceId = d.device')
            ->where('gd.gatewayId = :gateway')
            ->setParameter('gateway', $gatewayId, 'uuid');
        $result = $qb->getQuery()->getResult();
        $data = array(
            "modbus" => $result
        );
        return $data;
    }

}