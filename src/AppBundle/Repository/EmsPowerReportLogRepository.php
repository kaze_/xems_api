<?php
/**
 * Created by PhpStorm.
 * User: chiehyingpan
 * Date: 2017/5/12
 * Time: 下午12:10
 */

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class EmsPowerReportLogRepository extends EntityRepository
{
    const defaultInterval = 300;

    public function getCircuitIdByLocation($location)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb->select('c.circuitId')
            ->from('AppBundle:EmsMeterCircuitMapping', 'mc')
            ->join('mc.circuit', 'c')
            ->join('mc.device', 'p')
            ->join('p.device', 'd')
            ->innerJoin('AppBundle:EmsGatewayDeviceMapping', 'gm', 'WITH', 'gm.deviceId = d.deviceId')
            ->innerJoin('AppBundle:EmsLocationGatewayMapping', 'lm', 'WITH', 'lm.gatewayId = gm.gatewayId')
            ->where('lm.locationId = :location')
//            ->innerJoin('AppBundle:EmsDeviceMaster', 'd', 'WITH', 'd.deviceId = mc.deviceId')
            ->join('d.deviceType', 'dt')
            ->andWhere('dt.deviceTypeId = 100')
            ->setParameter('location', $location, 'uuid');

        $result = $qb->getQuery()->getResult();
        $circuitId = null;
        foreach ($result as $data) {
            if (($circuitId = $data['circuitId']) != null) {
                break;
            }
        }

        return $circuitId;
    }

    public function getCircuitIdByDevice($device)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb->select('c.circuitId')
            ->from('AppBundle:EmsMeterCircuitMapping', 'mc')
            ->join('mc.circuit', 'c')
            ->join('mc.device', 'p')
            ->join('p.device', 'd')
            ->join('d.deviceType', 'dt')
            ->where('d.deviceId = :device')
            ->andWhere('dt.deviceTypeId = 100')
            ->setParameter('device', $device, 'uuid');

        $result = $qb->getQuery()->getResult();
        $circuitId = null;
        foreach ($result as $data) {
            if (($circuitId = $data['circuitId']) != null) {
                break;
            }
        }
        return $circuitId;
    }


    /**
     * TODO: move "reportDatetime" to every 00, 15, 30, 45 minutes (if interval = 15 minutes)
     * @param $condition
     * @param $ordering
     * @param $limit
     * @param $offset
     * @return array
     */
    public function findCurrDmdKwByLocation($condition, $ordering, $limit, $offset)
    {
        $startDate = $condition['startDate'];
        $endDate = $condition['endDate'];
        $location = $condition['location'];
        $orderType = $ordering['reportDatetime'];
        $interval = $condition['interval'];

        $circuit = $this->getCircuitIdByLocation($location);

        $qb = $this->createQueryBuilder('p');
        $qb->select('p.reportDatetime, p.currDmdKw, p.circuitId, ROUND(UNIX_TIMESTAMP(p.reportDatetime)/:interval) as timekey')
            ->where('p.reportDatetime Between :startDate and :endDate')
            ->andWhere('p.circuitId = :circuit')
            ->setParameter('startDate', $startDate)
            ->setParameter('endDate', $endDate)
            ->setParameter('interval', $interval)
            ->setParameter('circuit', $circuit)
            ->groupBy('timekey')
            ->orderBy('p.reportDatetime', $orderType);

        if ($offset) {
            $qb->setFirstResult($offset);
        }

        if ($limit) {
            $qb->setMaxResults($limit);
        }

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /**
     * @param $condition
     * @return array
     */
    public function findMaxDmdByLocation($condition)
    {
        $startDate = $condition['startDate'];
        $endDate = $condition['endDate'];
        $location = $condition['location'];

        $circuit = $this->getCircuitIdByLocation($location);

        $qb = $this->createQueryBuilder('p');
        $qb2 = $this->createQueryBuilder('md')
            ->select('MAX(md.currDmdKw) maxDmd')
            ->where('md.reportDatetime between :startDate and :endDate')
            ->andWhere('md.circuitId = :circuit');

        $qb->select('p.reportDatetime, p.currDmdKw')
            ->where($qb2->expr()->eq('p.currDmdKw', '(' . $qb2->getDql() . ')'))
            ->andWhere('p.reportDatetime Between :startDate and :endDate')
            ->andWhere('p.circuitId = :circuit')
            ->setParameter('startDate', $startDate)
            ->setParameter('endDate', $endDate)
            ->setParameter('circuit', $circuit);

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /**
     * @param $condition
     * @return array
     */
    public function findMaxCurrByLocation($condition)
    {
        $startDate = $condition['startDate'];
        $endDate = $condition['endDate'];
        $location = $condition['location'];

        $circuit = $this->getCircuitIdByLocation($location);

        $qb = $this->createQueryBuilder('p');

        $qb2 = $this->createQueryBuilder('md')
            ->select('MAX(md.aA + md.aB + md.aC) as maxA')
            ->where('md.reportDatetime between :startDate and :endDate')
            ->andWhere('md.circuitId = :circuit');

//        $qb->select('p.reportDatetime, p.aAverage')
        $qb->select('p.reportDatetime, p.aA + p.aB + p.aC')
            ->where($qb2->expr()->eq('p.aA + p.aB + p.aC', '(' . $qb2->getDql() . ')'))
            ->andWhere('p.reportDatetime Between :startDate and :endDate')
            ->andWhere('p.circuitId = :circuit')
            ->setParameter('startDate', $startDate)
            ->setParameter('endDate', $endDate)
            ->setParameter('circuit', $circuit);

        $result = $qb->getQuery()->getResult();
        if (isset($result[0][1])) {
            $result[0]['aAverage'] = $result[0][1] / 3;
            $result[0]['aSummary'] = $result[0][1];
            unset($result[0][1]);
        }
        return $result;
    }

    /**
     * @param $fields
     * @param $condition
     * @return array
     */
    public function findNewestDataByLocation($fields, $condition)
    {
        $location = $condition['location'];

        $circuit = $this->getCircuitIdByLocation($location);

        $qb = $this->createQueryBuilder('p');

        $qb->select('p.reportDatetime');
        for ($i = 0; $i < count($fields); $i++) {
            $qb->addSelect('p.' . $fields[$i]);
        }
        $qb->where('p.circuitId = :circuit')
            ->setParameter('circuit', $circuit)
            ->setMaxResults(1)
            ->orderBy('p.reportDatetime', 'DESC');

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    public function findkWhByLocation($condition)
    {
        $startDate = $condition['startDate'];
        $endDate = $condition['endDate'];
        $location = $condition['location'];

        $circuit = $this->getCircuitIdByLocation($location);

        $qb = $this->createQueryBuilder('p');
        $qb2 = $this->createQueryBuilder('p2');
        $qb->select("p.reportDatetime, p.kwhTotal")
            ->where('p.circuitId = :circuit')
            ->andWhere('p.reportDatetime between :startDate and :endDate')
            ->setParameter('circuit', $circuit)
            ->setParameter('endDate', $endDate)
            ->setParameter('startDate', $startDate)
            ->setMaxResults(1)
            ->orderBy('p.reportDatetime', 'DESC');

        $qb2->select("p2.reportDatetime, p2.kwhTotal")
            ->where('p2.circuitId = :circuit')
            ->andWhere('p2.reportDatetime between :startDate and :endDate')
            ->setParameter('circuit', $circuit)
            ->setParameter('startDate', $startDate)
            ->setParameter('endDate', $endDate)
            ->setMaxResults(1)
            ->orderBy('p2.reportDatetime', 'ASC');

        $result = $qb->getQuery()->getResult();
        $result2 = $qb2->getQuery()->getResult();

        if ($result == null || count($result) == 0 || $result2 == null || count($result2) == 0) {
            return 0;
        }

        $endkWh = $result[0]['kwhTotal'];
        $startkWh = $result2[0]['kwhTotal'];

        return $endkWh - $startkWh;
    }

    /**
     * TODO: move "reportDatetime" to every 00, 15, 30, 45 minutes (if interval = 15 minutes)
     * @param $condition
     * @param $ordering
     * @param $limit
     * @param $offset
     * @return array
     */
    public function findCurrDmdKwByDevice($condition, $ordering, $limit, $offset)
    {
        $startDate = $condition['startDate'];
        $endDate = $condition['endDate'];
        $deviceId = $condition['device'];
        $orderType = $ordering['reportDatetime'];
        $interval = $condition['interval'];

        $circuit = $this->getCircuitIdByDevice($deviceId);
        $qb = $this->createQueryBuilder('p');
        $qb->select('p.reportDatetime, p.currDmdKw, p.circuitId, ROUND(UNIX_TIMESTAMP(p.reportDatetime)/:interval) as timekey')
            ->where('p.reportDatetime Between :startDate and :endDate')
            ->andWhere('p.circuitId = :circuit')
            ->setParameter('startDate', $startDate)
            ->setParameter('endDate', $endDate)
            ->setParameter('circuit', $circuit, 'uuid')
            ->setParameter('interval', $interval)
            ->groupBy('p.circuitId')
            ->groupBy('timekey')
            ->orderBy('p.reportDatetime', $orderType);

        if ($offset) {
            $qb->setFirstResult($offset);
        }

        if ($limit) {
            $qb->setMaxResults($limit);
        }

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    public function findMaxDataByDevice($maxField, $fields, $condition)
    {
        $startDate = $condition['startDate'];
        $endDate = $condition['endDate'];
        $device = $condition['device'];
        $circuit = $this->getCircuitIdByDevice($device);

        $qb = $this->createQueryBuilder('p');
        $qb2 = $this->createQueryBuilder('md')
            ->select('MAX(md.' . $maxField . ') maxData')
            ->where('md.reportDatetime between :startDate and :endDate')
            ->andWhere('md.circuitId = :circuit');

        $qb->select('p.reportDatetime');
        foreach ($fields as $field) {
            $qb->addSelect('p.' . $field);
        }
        $qb->where($qb2->expr()->eq('p.' . $maxField, '(' . $qb2->getDql() . ')'))
            ->andWhere('p.reportDatetime Between :startDate and :endDate')
            ->andWhere('p.circuitId = :circuit')
            ->setParameter('startDate', $startDate)
            ->setParameter('endDate', $endDate)
            ->setParameter('circuit', $circuit);

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /**
     * @param $condition
     * @return array
     */
    public function findMaxDmdByDevice($condition)
    {
        $startDate = $condition['startDate'];
        $endDate = $condition['endDate'];
        $deviceId = $condition['device'];

        $circuit = $this->getCircuitIdByDevice($deviceId);

        $qb = $this->createQueryBuilder('p');

        $qb2 = $this->createQueryBuilder('md')
            ->select('MAX(md.currDmdKw) maxDmd')
            ->where('md.reportDatetime between :startDate and :endDate')
            ->andWhere('md.circuitId = :circuit');

        $qb->select('p.reportDatetime, p.currDmdKw')
            ->where($qb2->expr()->eq('p.currDmdKw', '(' . $qb2->getDql() . ')'))
            ->andWhere('p.reportDatetime Between :startDate and :endDate')
            ->andWhere('p.circuitId = :circuit')
            ->setParameter('startDate', $startDate)
            ->setParameter('endDate', $endDate)
            ->setParameter('circuit', $circuit);

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    public function findMaxCurrByDevice($condition)
    {
        $startDate = $condition['startDate'];
        $endDate = $condition['endDate'];
        $deviceId = $condition['device'];

        $circuit = $this->getCircuitIdByDevice($deviceId);

        $qb = $this->createQueryBuilder('p');

        $qb2 = $this->createQueryBuilder('md')
//            ->select('MAX(md.aAverage) maxA')
            ->select('MAX(md.aA + md.aB + md.aC) as maxA')
            ->where('md.reportDatetime between :startDate and :endDate')
            ->andWhere('md.circuitId = :circuit');

//        $qb->select('p.reportDatetime, p.aAverage')
        $qb->select('p.reportDatetime, p.aA + p.aB + p.aC')
            ->where($qb2->expr()->eq('p.aA + p.aB + p.aC', '(' . $qb2->getDql() . ')'))
            ->andWhere('p.reportDatetime Between :startDate and :endDate')
            ->andWhere('p.circuitId = :circuit')
            ->setParameter('startDate', $startDate)
            ->setParameter('endDate', $endDate)
            ->setParameter('circuit', $circuit);

        $result = $qb->getQuery()->getResult();
        if (isset($result[0][1])) {
            $result[0]['aAverage'] = $result[0][1] / 3;
            $result[0]['aSummary'] = $result[0][1];
            unset($result[0][1]);
        }
        return $result;
    }

    /**
     * @param $fields
     * @param $condition
     * @return array
     */
    public function findNewestDataByDevice($fields, $condition)
    {
        $deviceId = $condition['device'];

        $circuit = $this->getCircuitIdByDevice($deviceId);

        $qb = $this->createQueryBuilder('p');

        $qb->select('p.reportDatetime');
        for ($i = 0; $i < count($fields); $i++) {
            $qb->addSelect('p.' . $fields[$i]);
        }
        $qb->where('p.circuitId = :circuit')
            ->setParameter('circuit', $circuit)
            ->setMaxResults(1)
            ->orderBy('p.reportDatetime', 'DESC');

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    public function findkWhByDevice($condition)
    {
        $startDate = $condition['startDate'];
        $endDate = $condition['endDate'];
        $deviceId = $condition['device'];

        $circuit = $this->getCircuitIdByDevice($deviceId);

        $qb = $this->createQueryBuilder('p');
        $qb2 = $this->createQueryBuilder('p2');
        $qb->select("p.reportDatetime, p.kwhTotal")
            ->where('p.circuitId = :circuit')
            ->andWhere('p.reportDatetime between :startDate and :endDate')
            ->setParameter('circuit', $circuit)
            ->setParameter('endDate', $endDate)
            ->setParameter('startDate', $startDate)
            ->setMaxResults(1)
            ->orderBy('p.reportDatetime', 'DESC');

        $qb2->select("p2.reportDatetime, p2.kwhTotal")
            ->where('p2.circuitId = :circuit')
            ->andWhere('p2.reportDatetime between :startDate and :endDate')
            ->setParameter('circuit', $circuit)
            ->setParameter('startDate', $startDate)
            ->setParameter('endDate', $endDate)
            ->setMaxResults(1)
            ->orderBy('p2.reportDatetime', 'ASC');

        $result = $qb->getQuery()->getResult();
        $result2 = $qb2->getQuery()->getResult();

        if ($result == null || count($result) == 0 || $result2 == null || count($result2) == 0) {
            return 0;
        }

        $endkWh = $result[0]['kwhTotal'];
        $startkWh = $result2[0]['kwhTotal'];

        return $endkWh - $startkWh;
    }

    public function findCurrDmdKwByCircuit($condition, $ordering, $limit, $offset)
    {
        $startDate = $condition['startDate'];
        $endDate = $condition['endDate'];
        $circuitId = $condition['circuit'];
        $orderType = $ordering['reportDatetime'];
        $interval = $condition['interval'];

        $qb = $this->createQueryBuilder('p');
        $qb->select('p.reportDatetime, p.currDmdKw, p.circuitId, ROUND(UNIX_TIMESTAMP(p.reportDatetime)/:interval) as timekey')
            ->where('p.reportDatetime Between :startDate and :endDate')
            ->andWhere('p.circuitId = :circuit')
            ->setParameter('startDate', $startDate)
            ->setParameter('endDate', $endDate)
            ->setParameter('circuit', $circuitId)
            ->setParameter('interval', $interval)
            ->groupBy('p.circuitId')
            ->groupBy('timekey')
            ->orderBy('p.reportDatetime', $orderType);

        if ($offset) {
            $qb->setFirstResult($offset);
        }

        if ($limit) {
            $qb->setMaxResults($limit);
        }
        $result = $qb->getQuery()->getResult();
        return $result;
    }

    public function findDataByCircuit($fields, $condition, $ordering, $limit = null, $offset = null)
    {
        $startDate = $condition['startDate'];
        $endDate = $condition['endDate'];
        $circuitId = $condition['circuit'];
        $orderType = $ordering['reportDatetime'];
        $interval = isset($condition['interval'])? $condition['interval'] : $this::defaultInterval;

        $qb = $this->createQueryBuilder('p');
        $qb->select('p.reportDatetime, FROM_UNIXTIME((FLOOR(UNIX_TIMESTAMP(p.reportDatetime)/:interval)*:interval)) as time');
        for ($i = 0; $i < count($fields); $i++) {
            $qb->addSelect('p.' . $fields[$i]);
        }
        $qb->where('p.reportDatetime Between :startDate and :endDate')
            ->andWhere('p.circuitId = :circuit')
            ->setParameter('startDate', $startDate)
            ->setParameter('endDate', $endDate)
            ->setParameter('circuit', $circuitId)
            ->setParameter('interval', $interval)
            ->groupBy('p.circuitId')
            ->groupBy('time')
            ->orderBy('p.reportDatetime', $orderType);

        if ($offset) {
            $qb->setFirstResult($offset);
        }

        if ($limit) {
            $qb->setMaxResults($limit);
        }
        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /**
     * @param $condition
     * @return array
     */
    public function findMaxDmdByCircuit($condition)
    {
        $startDate = $condition['startDate'];
        $endDate = $condition['endDate'];
        $circuitId = $condition['circuit'];

        $qb = $this->createQueryBuilder('p');

        $qb2 = $this->createQueryBuilder('md')
            ->select('MAX(md.currDmdKw) maxDmd')
            ->where('md.reportDatetime between :startDate and :endDate')
            ->andWhere('md.circuitId = :circuit');

        $qb->select('p.reportDatetime, p.currDmdKw')
            ->where($qb2->expr()->eq('p.currDmdKw', '(' . $qb2->getDql() . ')'))
            ->andWhere('p.reportDatetime Between :startDate and :endDate')
            ->andWhere('p.circuitId = :circuit')
            ->setParameter('startDate', $startDate)
            ->setParameter('endDate', $endDate)
            ->setParameter('circuit', $circuitId);

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    public function findMaxCurrByCircuit($condition)
    {
        $startDate = $condition['startDate'];
        $endDate = $condition['endDate'];
        $circuitId = $condition['circuit'];

        $qb = $this->createQueryBuilder('p');

        $qb2 = $this->createQueryBuilder('md')
//            ->select('MAX(md.aAverage) maxA')
            ->select('MAX(md.aA + md.aB + md.aC) as maxA')
            ->where('md.reportDatetime between :startDate and :endDate')
            ->andWhere('md.circuitId = :circuit');

//        $qb->select('p.reportDatetime, p.aAverage')
        $qb->select('p.reportDatetime, p.aA + p.aB + p.aC')
            ->where($qb2->expr()->eq('p.aA + p.aB + p.aC', '(' . $qb2->getDql() . ')'))
            ->andWhere('p.reportDatetime Between :startDate and :endDate')
            ->andWhere('p.circuitId = :circuit')
            ->setParameter('startDate', $startDate)
            ->setParameter('endDate', $endDate)
            ->setParameter('circuit', $circuitId);

        $result = $qb->getQuery()->getResult();
        if (isset($result[0][1])) {
            $result[0]['aAverage'] = $result[0][1] / 3;
            $result[0]['aSummary'] = $result[0][1];
            unset($result[0][1]);
        }
        return $result;
    }

    /**
     * @param $fields
     * @param $condition
     * @return array
     */
    public function findNewestDataByCircuit($fields, $condition, $returnObject = false)
    {
        $circuit = $condition['circuit'];

        $qb = $this->createQueryBuilder('p');
        if ($returnObject == true) {
            $qb->select('partial p.{id, reportDatetime,' . implode(',', $fields) . '}');
        } else {
            $qb->select('p.reportDatetime');
            for ($i = 0; $i < count($fields); $i++) {
                $qb->addSelect('p.' . $fields[$i]);
            }
        }
        $qb->where('p.circuitId = :circuit')
            ->setParameter('circuit', $circuit)
            ->setMaxResults(1)
            ->orderBy('p.reportDatetime', 'DESC');

        $result = $qb->getQuery()->getOneOrNullResult();
        return $result;
    }

    public function findkWhByCircuit($condition)
    {
        $endDate = isset($condition['endDate']) ? $condition['endDate'] : new \DateTime();
        // TODO: start date of endDate
        $startDate = isset($condition['startDate']) ? $condition['startDate'] :
            (\DateTime::createFromFormat('Y-m-d', date('Y-m-01')))->setTime(0, 0, 0);
        $circuitId = $condition['circuit'];

        $qb = $this->createQueryBuilder('p');
        $qb2 = $this->createQueryBuilder('p2');
        $qb->select("p.reportDatetime, p.kwhTotal")
            ->where('p.circuitId = :circuit')
            ->andWhere('p.reportDatetime between :startDate and :endDate')
            ->setParameter('circuit', $circuitId)
            ->setParameter('endDate', $endDate)
            ->setParameter('startDate', $startDate)
            ->setMaxResults(1)
            ->orderBy('p.reportDatetime', 'DESC');

        $qb2->select("p2.reportDatetime, p2.kwhTotal")
            ->where('p2.circuitId = :circuit')
            ->andWhere('p2.reportDatetime between :startDate and :endDate')
            ->setParameter('circuit', $circuitId)
            ->setParameter('startDate', $startDate)
            ->setParameter('endDate', $endDate)
            ->setMaxResults(1)
            ->orderBy('p2.reportDatetime', 'ASC');

        $result = $qb->getQuery()->getResult();
        $result2 = $qb2->getQuery()->getResult();

        if ( $result == null || count($result) == 0 || $result2 == null || count($result2) == 0 ) {
            return 0;
        }

        $endkWh = $result[0]['kwhTotal'];
        $startkWh = $result2[0]['kwhTotal'];

        return $endkWh - $startkWh;
    }


    public function findCurrentStatusByCircuit($circuit)
    {
        $qb = $this->createQueryBuilder('p');
        $qb->select('partial p.{id, reportDatetime, aA, aB, aC, aAverage, vA, vB, vC, vAverage, kwA, kwB, kwC, kwTotal, currDmdKw, circuitId, kwhTotal}')
            ->where('p.circuitId = :circuit')
            ->setParameter('circuit', $circuit)
            ->setMaxResults(1)
            ->orderBy('p.reportDatetime', 'DESC');
        $result = $qb->getQuery()->getOneOrNullResult();
        if (  $result != null ) {

            $currentStatus['time'] = $result->getReportDateTime();
            $currentStatus['power'] = $result->getkwTotal();
            $currentStatus['current'] = $result->getAAverage();
            $currentStatus['voltage'] = $result->getVAverage();
            $currentStatus['demand'] = $result->getCurrDmdKw();

            // get average start kwh time
            // TODO: stand-alone function for this
            $endTime = $result->getReportDateTime();
            $startTime = clone($endTime);
            $startTime->setDate( $startTime->format('Y'), $startTime->format('m'), 1)
                ->setTime(0, 0, 0);

            $qb2 = $this->createQueryBuilder('k');
            $qb2->select('partial k.{id, reportDatetime, kwhTotal}')
                ->where('k.circuitId = :circuit')
                ->andWhere('k.reportDatetime > :dateTime')
                ->setMaxResults(1)
                ->orderBy('k.reportDatetime', 'ASC')
                ->setParameters(array('circuit' => $circuit, 'dateTime' => $startTime));
            $kwhResult = $qb2->getQuery()->getOneOrNullResult();

            if ( $kwhResult  == null ) {
                $kwh = 0;
            } else {
                $kwh = $result->getKwhTotal() - $kwhResult->getKwhTotal();
            }
            $currentStatus['kwh'] = $kwh;
            return $currentStatus;
            //$circuit->setCurrentStatus($currentStatus);
            // $circuit['currentStatus'] = $currentStatus;
        }
    }

    public function findMaxDataByCircuit($maxField, $fields, $condition)
    {
        $startDate = $condition['startDate'];
        $endDate = $condition['endDate'];
        $circuit = $condition['circuit'];

        $qb = $this->createQueryBuilder('p');
        $qb2 = $this->createQueryBuilder('md')
            ->select('MAX(md.' . $maxField . ') maxData')
            ->where('md.reportDatetime between :startDate and :endDate')
            ->andWhere('md.circuitId = :circuit');

        $qb->select('p.reportDatetime');
        foreach ($fields as $field) {
            $qb->addSelect('p.' . $field);
        }
        $qb->where($qb2->expr()->eq('p.' . $maxField, '(' . $qb2->getDql() . ')'))
            ->andWhere('p.reportDatetime Between :startDate and :endDate')
            ->andWhere('p.circuitId = :circuit')
            ->setParameter('startDate', $startDate)
            ->setParameter('endDate', $endDate)
            ->setParameter('circuit', $circuit);

        $result = $qb->getQuery()->getResult();
        return $result;
    }
}
