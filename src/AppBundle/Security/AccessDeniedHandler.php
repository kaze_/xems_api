<?php
/**
 * Created by PhpStorm.
 * User: chiehyingpan
 * Date: 2017/6/23
 * Time: 下午4:52
 */

namespace AppBundle\Security;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Http\Authorization\AccessDeniedHandlerInterface;

class AccessDeniedHandler implements AccessDeniedHandlerInterface
{
    public function handle(Request $request, AccessDeniedException $accessDeniedException)
    {
        $response = new JsonResponse(array(
            'code' => JsonResponse::HTTP_FORBIDDEN,
            'message' => 'Access Denied'), JsonResponse::HTTP_FORBIDDEN);
        return $response;
    }
}
