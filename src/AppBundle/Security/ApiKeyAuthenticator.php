<?php

namespace AppBundle\Security;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\PreAuthenticatedToken;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Http\Authentication\SimplePreAuthenticatorInterface;

class ApiKeyAuthenticator implements SimplePreAuthenticatorInterface
{
    public $sessionToken;
    public $userID;
    public $sendTime;

    public function __construct() {
        $this->sessionToken = null;
        $this->userID = null;
        $this->sendTime = 0;
    }

    public function createToken(Request $request, $providerKey)
    {
        // look for an apikey query parameter

        $this->sessionToken = $request->headers->get('X-XM-Auth-Token');
        $this->userID = $request->headers->get('X-XM-User');
        $this->sendTime = $request->headers->get('X-XM-Time');


/*
        if ( !$this->sessionToken ) {
            //return null;
            throw new BadCredentialsException('No API key found');
        }
*/
        return new PreAuthenticatedToken(
            'anon.',
            $this->sessionToken,
            $providerKey
        );
    }

    public function supportsToken(TokenInterface $token, $providerKey)
    {
        return $token instanceof PreAuthenticatedToken && $token->getProviderKey() === $providerKey;
    }

    public function authenticateToken(TokenInterface $token, UserProviderInterface $userProvider, $providerKey)
    {
        if (!$userProvider instanceof ApiKeyUserProvider) {
            throw new \InvalidArgumentException(
                sprintf(
                    'The user provider must be an instance of ApiKeyUserProvider (%s was given).',
                    get_class($userProvider)
                )
            );
        }

        $key = $token->getCredentials();
        $user = null;
        $token = null;
        if ($key == null || $key == '') {
            $user = new User(
                    'anonymous.',
                    null,
                    // the roles for the user - you may choose to determine
                    // these dynamically somehow based on the user
                    array()
                );
            $token = new PreAuthenticatedToken(
                $user,
                '',
                $providerKey,
                array()
            );
            $token->setAuthenticated(false);
        } else {
            if ($this->sessionToken) {
                $user = $userProvider->validate($this->userID, $this->sessionToken);
                if ($user == null) {
                    $user = new User(
                        'anonymous.',
                        null,
                        // the roles for the user - you may choose to determine
                        // these dynamically somehow based on the user
                        array()
                    );
                    $token = new PreAuthenticatedToken(
                        $user,
                        $this->sessionToken,
                        $providerKey,
                        $user->getRoles()
                    );
                    $token->setAuthenticated(true);
                } else {
                    $token = new PreAuthenticatedToken(
                        $user,
                        $this->sessionToken,
                        $providerKey,
                        $user->getRoles()
                    );
                
                }
            } else {
                $user = new User(
                    'anonymous.',
                    null,
                    // the roles for the user - you may choose to determine
                    // these dynamically somehow based on the user
                    array()
                );
                $token = new PreAuthenticatedToken(
                    $user,
                    $this->sessionToken,
                    $providerKey,
                    $user->getRoles()
                );
                $token->setAuthenticated(true);
            }
        }
        return $token;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        echo 'onAuthenticationFailure';
        return new Response(
            // this contains information about *why* authentication failed
            // use it, or return your own message
            strtr($exception->getMessageKey(), $exception->getMessageData())
        , 403);
    }

    protected function validateDigest($digest, $nonce, $created, $secret)
    {
        $expected = base64_encode(sha1(base64_decode($nonce).$created.$secret, true));
        file_put_contents('/tmp/playlab_log', 'digest: '.$digest."\n", FILE_APPEND);
        file_put_contents('/tmp/playlab_log', 'expected: '.$expected."\n", FILE_APPEND);
        return $digest === $expected;
    }

}
