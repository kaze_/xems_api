<?php
namespace AppBundle\Security;

use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Doctrine\Common\Persistence\ObjectManager;
class ApiKeyUserProvider implements UserProviderInterface
{
    protected $objectManager;
    protected $tokenRepository;
    protected $userRepository;

    public function __construct(ObjectManager $om, $class) {
        $this->userManager = $om;
        $this->userRepository = $om->getRepository($class);
        $this->tokenRepository = $om->getRepository('AppBundle:LoginToken');
        $metadata = $om->getClassMetadata($class);
        $this->class = $metadata->getName();
    }
/*
    public function getUsernameForApiKey($apiKey)
    {
        $username = ....;
        return $username;
    }
*/
    public function loadUserByToken($token) {
        $loginToken = $this->tokenRepository->findOneBy(array('sessionKey'=>$token));
        if ($loginToken) {
            return $loginToken->getUser();
        }
        return null;

    }
    public function loadUserById($userId)
    {
        return $this->userRepository->find($userId);
    }

    public function loadUserByPhone($phone)
    {
        return $this->userRepository->findOneBy(array('phone' => $phone));
    }

    public function loadUserByEmail($email)
    {
        return $this->userRepository->findOneBy(array('email' => $email));
    }

    public function loadUserByUsername($username) {
        return $this->userRepository->findOneBy(array('username'=>$username));
    }

    public function validate($userid, $token) {
        $user = $this->userRepository->findOneBy(array('id'=>$userid));
        if ($this->tokenRepository->findOneBy(array(
                'user' => $user,
                'sessionKey' => $token,
                'isEnabled' => true
                ))) {
            return $user;
        } else {
            return null;
        }
    }
    
    public function refreshUser(UserInterface $user)
    {
        // this is used for storing authentication in the session
        // but in this example, the token is sent in each request,
        // so authentication can be stateless. Throwing this exception
        // is proper to make things stateless
        throw new UnsupportedUserException();
    }

    public function supportsClass($class)
    {
        return 'Symfony\Component\Security\Core\User\User' === $class;
    }
}
