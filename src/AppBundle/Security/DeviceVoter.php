<?php
/**
 * Created by PhpStorm.
 * User: chiehyingpan
 * Date: 2017/7/3
 * Time: 下午3:58
 */

namespace AppBundle\Security;


use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use AppBundle\Entity\EmsDeviceMaster;
use AppBundle\Entity\User;


class DeviceVoter extends Voter
{
    private $decisionManager;
    private $entityManager;

    const VIEW = 'view';
    const EDIT = 'edit';

    public function __construct(AccessDecisionManagerInterface $decisionManager, EntityManager $entityManager)
    {
        $this->decisionManager = $decisionManager;
        $this->entityManager = $entityManager;
    }

    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, array(self::VIEW, self::EDIT))) {
            return false;
        }
        if (!$subject instanceof EmsDeviceMaster) {
            return false;
        }
        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        if (!$user instanceof User) {
            return false;
        }

        if ( $this->decisionManager->decide($token, array('ROLE_ADMIN'))) {
            return true;
        }

        switch ($attribute) {
            case self::VIEW:
                return $this->canView($subject, $user);
            case self::EDIT:
                return $this->canEdit($subject, $user, $token);
        }
        throw new \LogicException('This code should not be reached!');
    }

    private function canView($device, $user)
    {
        $customerId = $user->getCustomerId();
        $cmMappingRepository = $this->entityManager->getRepository('AppBundle:EmsCustomerLocationMapping');
        $qb = $cmMappingRepository->createQueryBuilder('cl');
        $qb->innerJoin('AppBundle:EmsLocationGatewayMapping', 'lg', 'WITH', 'lg.locationId = cl.locationId')
            ->innerJoin('AppBundle:EmsGatewayDeviceMapping', 'gd', 'WITH', 'gd.gatewayId = lg.gatewayId')
            ->where('gd.deviceId = :device')
            ->andWhere('cl.customerId = :customer')
            ->setParameter('device', $device->getDeviceId(), 'uuid')
            ->setParameter('customer', $customerId, 'uuid');

        $result = $qb->getQuery()->getResult();
        if (count($result) == 0) {
            return false;
        }
        return true;
    }

    private function canEdit($subject, $user, $token)
    {

        if (!$this->canView($subject, $user)) {
            return false;
        }

        if ($this->decisionManager->decide($token, array('ROLE_CUSTOMER_ADMIN'))) {
            return true;
        }
        return false;

    }
}