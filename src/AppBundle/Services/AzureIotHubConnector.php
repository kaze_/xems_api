<?php

namespace AppBundle\Services;

class AzureIotHubConnector
{
    private $uri = null;
    private $sasKeyName = null;
    private $sasKeyValue = null;
    private $expires = null;
    private $sasToken = null;
    
    public function __construct($uri, $sasKeyName, $sasKeyValue)
    {
        $this->uri = $uri;
        $this->sasKeyName = $sasKeyName;
        $this->sasKeyValue = $sasKeyValue;
    }
    
    public static function generateSasToken($uri, $sasKeyName, $sasKeyValue)
    {
        $targetUri = strtolower(rawurlencode(strtolower($uri)));
        //$targetUri = rawurlencode($uri);
        $expires = time();
        $expiresInMins = 60;
        $week = 60*60*24*7;
        $expires = $expires + $expiresInMins*60;
        $toSign = $targetUri . "\n" . $expires;
        $signature = rawurlencode(base64_encode(hash_hmac('sha256',
            $toSign, base64_decode($sasKeyValue), TRUE)));
        
        $token = "SharedAccessSignature sr=" . $targetUri
            ."&sig=" . $signature 
            ."&se=" . $expires;
        if ( $sasKeyName != null ) 
            $token = $token ."&skn=" . $sasKeyName;
        
        return array("token" => $token, "expiry" => $expires );
    }
    
    private function getSasToken()
    {
        $now = time();
        
        if ( $this->sasToken == null || $this->expires <= $now ) {
            $tokenA = $this->generateSasToken($this->uri, $this->sasKeyName, $this->sasKeyValue);
            if ( $tokenA != null || count($tokenA) != 0 ) {
                $this->expires = $tokenA["expiry"];
                $this->sasToken = $tokenA["token"];
            }
        }
        return $this->sasToken;
    }
    
    protected function sendCloudToDeviceMethod($deviceId, $method, $payload)
    {
        // $api = "/twins/".$deviceId."/methods?api-version=2018-06-30";
        $api = "/twins/".$deviceId."/modules/xMightPowerClient/methods?api-version=2018-06-30";
        $data = array(
            "methodName" => $method,
            "responseTimeoutInSeconds" => 200,
            "payload" => $payload
        );
        $this->sendApiCall("POST", $api, $data);
    }
    
    protected function sendApiCall($method, $api, $data=false, $form=false)
    {
        $ch = curl_init();
        $url = "https://".$this->uri.$api;
        
        // print_r($url);
        // print_r($data);
        
        $headers = array();
        $headers[] = 'Accept: application/json';
        $headers[] = 'Authorization: '.$this->getSasToken();
        // print_r($this->getSasToken());
        
        $postFields = null;
        switch(strtoupper($method)) {
            case "POST":
                curl_setopt($ch, CURLOPT_POST, true);
                if ($data) {
                    if ( $form ) {
                        $boundary = md5(time());
                        $headers[] = 'Content-Type: multipart/form-data; boundary=' . $boundary;
                        $postFields = $this->multipart_build_query($data, $boundary);
                    } else {
                        $headers[] = 'Content-Type: application/json';
                        $postFields = json_encode($data);
                    }
                }
                break;
            case "PUT":
                curl_setopt($ch, CURLOPT_PUT, true);
                break;
            default:
                if ($data) {
                    $url = sprintf("%s?%s", $url, http_build_query($data));
                }
                break;
        }
        
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, '3');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_URL, $url);
        if ( $postFields != null ) {
            curl_setopt($ch, CURLOPT_POSTFIELDS,  $postFields);
        }
        $content = curl_exec($ch);
        /* if ( !$content ) {
            print_r(curl_error($ch));
        } */
        curl_close($ch);
        $result = json_decode($content, true);
        /* if ( !$result )  {
            print_r($content);
        } else {
            print_r($result);
        } */
        return $result;
    }
    
    public function powerSwitch($circuit, $action)
    {
        if ( !$circuit ) {
            echo("no circuit");
            return null;
        }
        $device = $circuit->getValidDevice();
        if ( !$device ) {
            echo("no device");
            return null;
        }
        $gateway = $device->getValidGateway();
        if ( !$gateway ) {
            echo("no gateway");
            return null;
        }
        $gatewayId = $gateway->getGatewayId()->__toString();
        $circuitId = $circuit->getCircuitId()->__toString();
        $result = $this->sendCloudToDeviceMethod($gatewayId, "powerSwitch", array(
            "id" => $circuitId,
            "action" => $action
        ));
        return $result;
    }
}
