<?php
/**
 * Created by PhpStorm.
 * User: chiehyingpan
 * Date: 2018/3/20
 * Time: 上午11:19
 */

namespace AppBundle\Services;


use AppBundle\Entity\EmsCircuitStructure;
use AppBundle\Services\tree\NestedTree;
use Doctrine\ORM\EntityManager;

class CircuitTree extends NestedTree
{
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->entityRepo = $em->getRepository('AppBundle:EmsCircuitMaster');
        $this->treeRepo = $em->getRepository('AppBundle:EmsCircuitStructure');
    }

    protected function newEntityTree($entity)
    {
        $entityStr = new EmsCircuitStructure();
        $entityStr->setCircuit($entity);
        return $entityStr;
    }

    protected function getEntityTree($entity)
    {
        $entityStr = new EmsCircuitStructure();
        $entityStr->setCircuit($entity);
        return $entityStr;
    }

    protected function getEntity($entityTree)
    {
        // TODO: Implement getEntity() method.
        return $entityTree->getCircuit();
    }
}