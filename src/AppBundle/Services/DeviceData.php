<?php

/**
 * Created by PhpStorm.
 * User: chiehyingpan
 * Date: 2018/9/10
 * Time: 下午1:37
 */

namespace AppBundle\Services;

use AppBundle\Entity\EmsGatewayMaster;
use AppBundle\Entity\EmsModbusGateway;
use AppBundle\Entity\EmsPowerMeter;
use AppBundle\Entity\EmsSensor;
use Doctrine\ORM\EntityManager;
use AppBundle\Entity\EmsSmartRelayTransactionLog;
use AppBundle\Entity\EmsCircuitMaster;

class DeviceData
{
    public const MAIN_METER_CATEGORY = 60000;
    private $_em;
    private $tree;
    private $_relay;
    private $_fee;

    public function __construct(EntityManager $em, $tree, $relay, $fee)
    {
        $this->_em = $em;
        $this->_tree = $tree;
        $this->_relay = $relay;
        $this->_fee = $fee;
    }

    public function createForm($type, $data = null, array $options = array())
    {
        return $this->formFactory->create($type, $data, $options);
    }

    public function getMainCircuitFromLocation($location)
    {
        $espCustomers = $location->getEspCustomer();
        if (!$espCustomers || count($espCustomers) == 0) {
            $repository = $this->_em->getRepository('AppBundle:EmsCircuitMaster');
            $qb = $repository->createQueryBuilder('c');
            $qb->join('c.circuit', 'c')
                ->innerJoin('AppBundle:EmsMeterCircuitMapping', 'mc', 'WITH', 'mc.circuitId = c.circuitId')               
                ->innerJoin('AppBundle:EmsGatewayDeviceMapping', 'gm', 'WITH', 'gm.deviceId = mc.deviceId')
                ->innerJoin('AppBundle:EmsLocationGatewayMapping', 'lm', 'WITH', 'lm.gatewayId = gm.gatewayId')
                ->where('lm.locationId = :location')
                ->andWhere('c.category = :mainMeterCategory')
                ->setParameter('location', $location->getLocationId(), 'uuid')
                ->setParameter('mainMeterCategory', self::MAIN_METER_CATEGORY);
            $result = $qb->getQuery()->getResult();
        } else {
            
        }
    }

    public function getCircuitChargingStatus($circuit)
    {
        $relay = $this->_relay;
        $rtnStatus = [];
        $equipmentStatus = 'Off';
        $switchStatus = 'Off';
        $status = $relay->getCircuitStatus($circuit);
        if ( $status == null ) {
            return null;
        }
        switch ($status['statusId']) {
            case EmsSmartRelayTransactionLog::STAT_ON:
                $equipmentStatus = 'Charging';
                $switchStatus = 'On';
                break;
            case EmsSmartRelayTransactionLog::STAT_STANDBY:
                $equipmentStatus = 'Standby';
                $switchStatus = 'On';
                break;
            case EmsSmartRelayTransactionLog::STAT_OFF:
                $equipmentStatus = 'Off';
                $switchStatus = 'Off';
                break;
        }
        if ( $circuit instanceof EmsCircuitMaster  ) {
            $circuit->setEquipment($equipmentStatus);
            $circuit->setPowerSwitch($switchStatus);
        }
        $rtnStatus['equipment'] = $equipmentStatus;
        $rtnStatus['powerSwitch'] = $switchStatus;
        $rtnStatus['relayStatus'] = $status;
        return $rtnStatus;
    }

    public function getCircuitDetail($circuit, $status, $vip, $espCustomerRateDetail)
    {
        $relay = $this->_relay;
        $fee = $this->_fee;
        $repository = $this->_em->getRepository('AppBundle:EmsPowerReportLog');
        $rtnStatus = [];
        $circuit_is_obj = false;
        if ( $circuit instanceof EmsCircuitMaster ) {
            $circuit_is_obj = true;
        }

        $currentStatus = $repository->findCurrentStatusByCircuit($circuit);
        if ($status == true && $currentStatus != null) {
            $charge = $fee->deviceFee(new \DateTime('first day of this month'), null, $circuit);
            // $charge = $report->getCircuitFee($circuit, new \DateTime('first day of this month'), null, $espCustomerRateDetail);
            if ($charge) {
                $currentStatus['fee'] = $charge['amount'];
            }
            if ( $circuit_is_obj ) $circuit->setCurrentStatus($currentStatus);
            $rtnStatus['current_status'] = $currentStatus;
        }
        $equipmentStatus = 'Off';
        $switchStatus = 'Off';
        $status = $relay->getCircuitStatus($circuit);
        switch ($status['statusId']) {
            case EmsSmartRelayTransactionLog::STAT_ON:
                $equipmentStatus = 'Charging';
                $switchStatus = 'On';
                break;
            case EmsSmartRelayTransactionLog::STAT_STANDBY:
                $equipmentStatus = 'Standby';
                $switchStatus = 'On';
                break;
            case EmsSmartRelayTransactionLog::STAT_OFF:
                $equipmentStatus = 'Off';
                $switchStatus = 'Off';
                break;
        }
        if ( $circuit_is_obj ) {
            $circuit->setEquipment($equipmentStatus);
            $circuit->setPowerSwitch($switchStatus);
        }
        $rtnStatus['equipment'] = $equipmentStatus;
        $rtnStatus['powerSwitch'] = $switchStatus;

        if ( $vip == true && $station = $circuit->getChargingStation()[0]) {
            $vipNum = $station->getPriorityCharging();
            $vip = 'off';
            if ( $vipNum == 1 ) {
                $vip = 'on';
            }
            if ( $circuit_is_obj) $circuit->setVip($vip);
            $rtnStatus['vip'] = $vip;
        }
        return $rtnStatus;
    }

    public function getCircuitsFromLocation($location, $status, $switch, $station)
    {
        // find main circuit(meter)
        $espCustomers = $location->getEspCustomer();
        if (!$espCustomers || count($espCustomers) == 0) {
            $repository = $this->_em->getRepository('AppBundle:EmsCircuitMaster');
            $qb = $repository->createQueryBuilder('c');
            $qb->join('c.meter', 'm')
                ->join('m.device', 'p')
                ->join('p.device', 'd')
                ->innerJoin('AppBundle:EmsGatewayDeviceMapping', 'gm', 'WITH', 'gm.deviceId = d.deviceId')
                ->innerJoin('AppBundle:EmsLocationGatewayMapping', 'lm', 'WITH', 'lm.gatewayId = gm.gatewayId')
                ->where('lm.locationId = :location')
                ->setParameter('location', $location->getLocationId(), 'uuid');
            if ($switch == 'true') {
                $qb->andWhere('p.meterType in(2, 4)');
            } else if ($station == 'true') {
                $qb->andWhere('c.evChargingStation = 1');
            }
            $result = $qb->getQuery()->getResult();

            if (!$result || count($result) == 0) {

            }
            if ($status == 'true' || $switch == 'true') {
                foreach ($result as $circuit) {
                    $this->getCircuitDetail($circuit, $status, $switch, null);
                }
            }
        } else {
            $meterRepo = $this->_em->getRepository('AppBundle:EmsPowerMeter');
            $tree = $this->_tree;
            $result = array();
            foreach ($espCustomers as $espCustomer) {
                $espCustomerRateDetail = $this->get('esp.fee')->getCustomerRateDetail($espCustomer);
                $meter = $meterRepo->find($espCustomer->getPowerMeterDeviceId());
                if (!$meter) {
                    continue;
                }
                $circuits = $meter->getCircuits();
                if (empty($circuits)) {
                    continue;
                }
                $circuit = $circuits[0]->getCircuit();
                // echo($circuit->getCircuitId());
                $children = $tree->getChildren($circuit);
                if (empty($children)) {
                    continue;
                }
                $selected = array();
                if ($status == 'true' || $switch == 'true') {
                    foreach ($children as $circuit) {
                        if ($switch == true) {
                            try {
                                $meterType = $circuit->getMeter()->get(0)->getDevice()
                                    ->getMeterType()->getMeterTypeId();
                                if ($meterType != 2 && $meterType != 4) {
                                    continue;
                                }
                            } catch (\Exception $e) {
                                continue;
                            }
                        }
                        $this->getCircuitDetail($circuit, $status, $switch, $espCustomerRateDetail);
                        $selected[] = $circuit;
                    }
                    $result = array_merge($result, $selected);
                } else {
                    $result = array_merge($result, $children);
                }
            }
        }
    }
}
