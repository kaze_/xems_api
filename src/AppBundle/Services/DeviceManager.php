<?php
/**
 * Created by PhpStorm.
 * User: chiehyingpan
 * Date: 2018/9/10
 * Time: 下午1:37
 */

namespace AppBundle\Services;

use AppBundle\Entity\EmsGatewayMaster;
use AppBundle\Entity\EmsModbusGateway;
use AppBundle\Entity\EmsPowerMeter;
use AppBundle\Entity\EmsSensor;
use Doctrine\ORM\EntityManager;

class DeviceManager
{
    private $_em;
    private $_tree;
    private $formFactory;

    public function __construct(EntityManager $em, $formFactory, CircuitTree $tree, GatewayTree $gatewayTree)
    {
        $this->_em = $em;
        $this->formFactory = $formFactory;
        $this->_tree = $tree;
        $this->_gtree = $gatewayTree;
    }
    
    public function createForm($type, $data = null, array $options = array())
    {
        return $this->formFactory->create($type, $data, $options);
    }
    
    // 5. 表燈標準型時間電價-二段式時間電價
    // 7. 低壓電力-二段式時間電價
    // 8. 高壓電力-二段式時間電價
    // 二段式時間電價
    function gen2StageContractCap( $rate )
    {
        $normal_cap = $rate->getNormalContract();
        $non_summer_additional_puchase = $rate->getSemiPeakContract();
        $saturday_semi_peak_additional_puchase = $rate->getSaturdaySemiPeakContract();
        $off_peak_additional_puchase = $rate->getOffPeakContract();

        $non_summer_cap = $normal_cap + $non_summer_additional_puchase;
        $saturday_semi_peak_cap = $non_summer_cap + $saturday_semi_peak_additional_puchase;
        $off_peak_cap = $saturday_semi_peak_cap + $off_peak_additional_puchase;

        /*
        if ( $non_summer_additional_puchase == 0 ) {
            return [
                "1" => [
                    [ "0000", "0730", $off_peak_cap ],
                    [ "0730", "2230", $normal_cap ],
                    [ "2230", "2400", $off_peak_cap ],
                ],
                "6" => [
                    [ "0000", "0730", $off_peak_cap],
                    [ "0730", "2230", $saturday_semi_peak_cap],
                    [ "2230", "2400", $off_peak_cap],
                ],
                "7" => [
                    [ "0000", "2400", $off_peak_cap ]
                ],
            ] ;
        } */

        $workweek = [
            "default" => [
                ["0000", "0730", $off_peak_cap],
                ["0730", "2230", $normal_cap],
                ["2230", "2400", $off_peak_cap],
            ],
        ];

        if ( $non_summer_additional_puchase > 0 ) {
            $workweek["non-summer"] =  [
                ["0000", "0730", $off_peak_cap],
                ["0730", "2230", $non_summer_cap],
                ["2230", "2400", $off_peak_cap],
            ];
        }

        if ( $saturday_semi_peak_cap != $off_peak_cap ) {
            $saturday = [
                ["0000", "0730", $off_peak_cap],
                ["0730", "2230", $saturday_semi_peak_cap],
                ["2230", "2400", $off_peak_cap],
            ];
        } else {
            $saturday = [
                ["0000", "2400", $off_peak_cap]
            ];
        }

        return [
            "1" => $workweek,
            "6" => $saturday,
            "7" => [
                ["0000", "2400", $off_peak_cap]
            ],
        ] ;
    }

    // 9.  高壓電力-三段式時間電價
    // 11. 特高壓電力-三段式時間電價
    function gen3StageContractCap( $rate )
    {
        $normal_cap = $rate->getNormalContract();
        $semi_peak_additional_puchase = $rate->getSemiPeakContract();
        if ( $semi_peak_additional_puchase === null ) $semi_peak_additional_puchase = 0;
        $saturday_semi_peak_additional_puchase = $rate->getSaturdaySemiPeakContract();
        $off_peak_additional_puchase = $rate->getOffPeakContract();

        $semi_peak_cap = $normal_cap + $semi_peak_additional_puchase ;
        $saturday_semi_peak_cap = $semi_peak_cap + $saturday_semi_peak_additional_puchase;
        $off_peak_cap = $saturday_semi_peak_cap + $off_peak_additional_puchase;

        return [
            "1" => [
               "default" => [
                    ["0000", "0730", $off_peak_cap],
                    ["0730", "1000", $semi_peak_cap],
                    ["1000", "1200", $normal_cap],
                    ["1200", "1300", $semi_peak_cap],
                    ["1300", "1700", $normal_cap],
                    ["1700", "2230", $semi_peak_cap],
                    ["2330", "2400", $off_peak_cap],
                ] ,
               "non-summer" => [             
                    ["0000", "0730", $off_peak_cap],
                    ["0730", "2230", $semi_peak_cap],
                    ["2230", "2400", $off_peak_cap]                
                ]           
            ],
            "6" => [
                ["0000", "0730", $off_peak_cap],
                ["0730", "2230", $saturday_semi_peak_cap],
                ["2230", "2400", $off_peak_cap],
            ],
            "7" => [
                ["0000", "2400", $off_peak_cap]
            ],
        ] ;
    }


    function genContractCapConfig($packId, $rate)
    {

        switch($packId) {
            case 5:
            case 7:
            case 8:
            case 10:
                return $this->gen2StageContractCap( $rate );
                break;
            case 9:
            case 11:
                return $this->gen3StageContractCap( $rate );
                break;
        }

        return null;
    }


    public function gatewayConfiguration(EmsGatewayMaster $gateway)
    {
        $repository = $this->_em->getRepository('AppBundle\Entity\EmsModbusGateway');
        $qb = $repository->createQueryBuilder('d');
        $qb->innerJoin('AppBundle\Entity\EmsGatewayDeviceMapping', 'gd', 'WITH', 'gd.deviceId = d.device')
            ->where('gd.gatewayId = :gateway')
            ->andWhere('gd.statusId = 1')
            ->setParameter('gateway', $gateway->getGatewayId(), 'uuid');
        $result = $qb->getQuery()->getResult();

        // return $result;
        $maptogroup = [ 'R' => 0, 'S' => 1, 'T' => 2, ];

        $espRepo = $this->_em->getRepository('EspBundle:EspCustomerMaster');
        $espRateRepo = $this->_em->getRepository('EspBundle:EspCustomerRate');
        $espPackDetailRepo = $this->_em->getRepository('EspBundle:EspEnergyPackageDetail');
        $circuitRepo = $this->_em->getRepository('AppBundle:EmsCircuitMaster');
        $data = array();
        $modbuses = array();
        foreach( $result as $modbus ) {
            $modbusData = array();
            $modbusData['id'] = $modbus->getDevice()->getDeviceId()->toString();
            $modbusData['ipAddress'] = $modbus->getIpAddress();
            $meters = $modbus->getPowerMeters();
            $sensors = $modbus->getSensors();
            $meterDatas = array();
            foreach($meters as $meter) {
                $meterData = array();
                $meterId = $meter->getDevice()->getDeviceId()->toString();
                $meterData['id'] = $meter->getDevice()->getDeviceId()->toString();
                $meterData['meterType'] = $meter->getMeterType()->getMeterTypeId();
                $meterData['modbusId'] = $meter->getModbusId();
                $meterData['logInterval'] = $meter->getLogInterval();
                if ( $meter->getMonitorInterval() != null ) {
                    $meterData['monitorInterval'] = $meter->getMonitorInterval();
                }
                if ( $meter->getCtRatio() != null ) $meterData['ctRatio'] = $meter->getCtRatio();
                if ( $meter->getPtRatio() != null ) $meterData['ptRatio'] = $meter->getPtRatio();
                $circuits = $meter->getCircuits();
                $circuitDatas = array();
                $espCustomer = $espRepo->findOneBy(array('powerMeterDeviceId' => $meterId));
                $isMaster = 0;
                $hasContract = 0;
                $hasTou = 0;
                $isRoot = 0;
                if ( $espCustomer != null ) {
                    $isMaster = 1;
                    $rate = $espRateRepo->findOneBy(array('espCustomer' => $espCustomer, 'statusId' => 1 ));
                    if ( $rate != null ) {
                        $pack = $rate->getEnergyPackage();
                        $packDetail = $espPackDetailRepo->findOneBy(array('energyPackage' => $pack, 'statusId' => 1));
                        $hasContract = $packDetail->getHasContract();
                        $hasTou = $packDetail->getHasTou();
                    }
                }
                foreach($circuits as $circuitMapping) {
                    $circuitData = array();
                    $circuit = $circuitMapping->getCircuit();
                    $circuitId = $circuit->getCircuitId();
                    $circuitData['id'] = is_object($circuitId) ? $circuitId->toString(): $circuitId;
                    $circuitData['uuid'] = $circuitData['id'];
                    //$circuitData['channel'] = $circuitMapping->getChannel();
                    $circuitData['maxCurrent'] = $circuit->getCircuitMaxCurrent();
                    $circuitData['standbyCurrent'] = $circuit->getCircuitStandbyCurrent();

                    $smbPhase = $circuit->getSmbPhase();
                    if ( $smbPhase != null ) {
                        $circuitData['channel'] = $circuit->getSmbChannel();
                        $circuitData['phase'] = strtoupper($circuit->getSmbPhase());
                    }
                    /* max demand  [{"1":["0000","0730",15]},{"1":["0730","2230",6]},{"1":["2230","0000",15]},{"6":["0000","0730",15]},{"6":["0730","2230",6]},{"6":["2230","0000",15]},{"7":["0000","0000",15]}] */
                    if ( $isMaster ) {
                        if ( $hasContract ) {
                            if ( $hasTou ) {
                                $circuitData['touDemand'] = 1;
                                $circuitData['touOffPeakDay'] = array();
                                $circuitData['maxDemand'] = array();
                                // off peak day TBD...
                                // tou contract TBD.............
                                $tmp = $this->genContractCapConfig($pack->getEnergyPackageId(), $rate );
                                if ( $tmp !== null )
                                    $circuitData['maxDemand'] = $tmp;
                            } else {
                                $circuitData['touDemand'] = 0;
                                $circuitData['maxDemand'] = $rate->getNormalContract();
                                // ["1" => ['0000', '0000', $rate->getNormalContract()]]
                               
                            }
                        }

                        $strategy = $circuit->getDemandAllocionStrategy();
                        if ( $strategy != null ) {
                            $circuitData['chargingStrategy'] = $strategy->getName();
                        }
                    }
                    if ( $this->_tree->getRoot($circuit) == $circuit && $isRoot == 0 ) {
                        $isRoot = 1;
                    }
                    $children = $this->_tree->getDirectChildren($circuit);
                    if ( $children != null && count($children) != 0 ) {
                        $childList = array();
                        foreach($children as $child) {
                            $childData = array();
                            if ( !is_string($child) ) {
                                $id = $child->getCircuitId();
                                $childData['id'] = is_string($id)? $id : $id->toString();
                                $childCircuit = $child;
                            } else {
                                $childData['id'] = $child;
                                $childCircuit = $circuitRepo->find($child);
                            }
                            try {
                                $childDevice = $childCircuit->getValidDevice();
                                if ( $childDevice == null ) continue;
                                $childGateway = $childDevice->getValidGateway();
                                if ( $childGateway != null && $childGateway != $gateway ) {
                                    $childData['gateway'] = $childGateway->getGatewayId()->toString();
                                    //$childData['ip'] = $childGateway->getLocalIp();
                                }

                                $childData['maxCurrent'] = $childCircuit->getCircuitMaxCurrent();
                                try { 
                                    $parent_phase_str = $childCircuit->getParentPhase();
                                    if ( $parent_phase_str ) {
                                        $parent_phase = json_decode($parent_phase_str,TRUE);
                                        foreach( $parent_phase as $key => $value ) {
                                            $childData['group'] = $maptogroup[$key];
                                        }
                                    }
                                } catch(\Exception $e) {
                                    // do nothing
                                    $childData['group'] = 0;
                                }
                                
                                $isChargingStation = $childCircuit->getEvChargingStation();
                                if ( $isChargingStation == true ) {
                                    $station = $childCircuit->getChargingStation();
                                    if ( $station != null && count($station) != 0) {
                                        $stationModel = $station[0]->getChargingStationModel();
                                        $modelId = $stationModel->getChargingStationModelId();
                                        if ( $modelId == 1 ) { // tesla
                                            $childData['chargerModel'] = 'tesla';
                                        } else { // others
                                            $childData['chargerModel'] = 'fixed:' . $stationModel->getMaxCurrent();
                                        }

                                        if ( $station[0]->getAuthQrcode() ) 
                                            $childData['enableBarcode'] = 1;
                                    }
                                }

                            } catch(\Exception $e) {
                                // do nothing
                            }
                            $childList[] = $childData;
                        }
                        $circuitData['children'] = $childList;
                    }
                    $isChargingStation = $circuit->getEvChargingStation();
                    if ( $isChargingStation == true ) {
                        $circuitData['chargingCircuit'] = 1;
                        $station = $circuit->getChargingStation();
                        if ( $station != null && count($station) != 0) {
                            $circuitData['chargingCurrent'] = $station[0]->getChargingCurrent();
                            $circuitData['chargingThreashold'] = $station[0]->getChargingThreashold();
                            if ( $station[0]->getAuthQrcode() ) $circuitData['enableBarcode'] = 1;
                            $stationModel = $station[0]->getChargingStationModel();
                            $modelId = $stationModel->getChargingStationModelId();
                            if ( $modelId == 1 ) { // tesla
                                $circuitData['chargerModel'] = 'tesla';
                            } else { // others
                                $circuitData['chargerModel'] = 'fixed:' . $stationModel->getMaxCurrent();
                            }
                        }
                    } else {
                        $circuitData['chargingCircuit'] = 0;
                    }
                    $circuitDatas[] = $circuitData;
                }
                if ( $isRoot ) {
                    $meterData['isRoot'] = 1;
                }
                $meterData['circuits'] = $circuitDatas;
                $meterDatas[] = $meterData;
            }
            if ( count($meterDatas) != 0 ) {
                $modbusData['powerMeters'] = $meterDatas;
            }
            $sensorDatas = array();
            foreach($sensors as $sensor) {
                $sensorData = array();
                $sensorData['id'] = $sensor->getDevice()->getDeviceId()->toString();
                $sensorData['sensorType'] = $sensor->getSensorType()->getSensorTypeId();
                $sensorData['modbusId'] = $sensor->getModbusId();
                $sensorData['logInterval'] = $sensor->getLogInterval();
                if ( $sensor->getMonitorInterval() != null ) {
                    $sensorData['monitorInterval'] = $sensor->getMonitorInterval();
                }
                $sensorDatas[] = $sensorData;
            }
            if ( count($sensorDatas) != 0 ) {
                $modbusData['sensors'] = $sensorDatas;
            }
            $modbuses[] = $modbusData;
        }
        // $data['id'] = $gateway->getGatewayId()->toString();
        $data['modbus'] = $modbuses;
        // get gateway structure
        $gatewayStructure = array();
        $parent = $this->_gtree->getParent($gateway);
        if ( $parent ) {
            $id = $parent->getGatewayId();
            $gatewayStructure['parent'] = array(
                'id' => is_object($id)? $id->toString(): $id,
                'ip' => $parent->getLocalIp()
            );
        }
        $children = $this->_gtree->getChildren($gateway);
        if ( $children ) {
            $childArray = array();
            foreach($children as $child) {
                $id = $child->getGatewayId();
                // echo($child->getLocalIp());
                $childArray[] = array(
                    'id' => is_object($id)? $id->toString():$id,
                    'ip' => $child->getLocalIp(),
                );
            }
            $gatewayStructure['children'] = $childArray;
        }
        $ret = array(
            'devices' => $data,
        );
        if ( !empty($gatewayStructure)) {
            $ret['gatewayStructure'] = $gatewayStructure;
        }
        //return array($result, $modbuses);
        return $ret;
    }
    
    function processInterface($device, $data)
    {
        $repo =  $this->_em->getRepository('AppBundle:EmsModbusGateway');
        $interface = $repo->find($device);
        $new = false;
        if ( !$interface ) {
            $interface = new EmsModbusGateway();
            $new = true;
        }
        $interface->setDevice($device);
        $form = $this->createForm('AppBundle\Form\EmsModbusGatewayType', $interface );
        $clearMissing = $new;
        $form->submit($data, $clearMissing);
        
        if ( $form->isValid() ) {
            $this->_em->persist($interface);
            $this->_em->flush();
        }
        return ;
    }
    
    function processPowerMeter($device, $data)
    {
        $repo =  $this->_em->getRepository('AppBundle:EmsPowerMeter');
        $meter = $repo->find($device);
        $new = false;
        if ( !$meter ) {
            $meter = new EmsPowerMeter();
            $new = true;
        }
        $meter->setDevice($device);
        
        $voltage = $this->_em->getRepository('AppBundle:EmsVoltage')->find($data['voltageId']);
        $meterType = $this->_em->getRepository('AppBundle:EmsMeterType')->find($data['meterTypeId']);
        $interface = $this->_em->getRepository('AppBundle:EmsModbusGateway')->find($data['modbusGatewayId']);
       
        $meter->setVoltage($voltage);
        $meter->setMeterType($meterType);
        $meter->setModbusGateway($interface);
       
        $form = $this->createForm('AppBundle\Form\EmsPowerMeterType', $meter );
        $clearMissing = $new;
        $form->submit($data, $clearMissing);
        
        if ( $form->isValid() ) {
            $this->_em->persist($meter);
            $this->_em->flush();
        }
        return ;
    }
    
    function processSensor($device, $data)
    {
        $repo =  $this->_em->getRepository('AppBundle:EmsSensor');
        $sensor = $repo->find($device);
        $new = false;
        if ( !$sensor ) {
            $sensor = new EmsSensor();
            $new = true;
        }
        $sensor->setDevice($device);
        
        $sensorType = $this->_em->getRepository('AppBundle:EmsSensorType')->find($data['sensorTypeId']);
        $interface = $this->_em->getRepository('AppBundle:EmsModbusGateway')->find($data['modbusGatewayId']);
       
        $sensor->setSensorType($sensorType);
        $sensor->setModbusGateway($interface);
       
        $form = $this->createForm('AppBundle\Form\EmsSensorType', $sensor );
        $clearMissing = $new;
        $form->submit($data, $clearMissing);
        
        if ( $form->isValid() ) {
            $this->_em->persist($sensor);
            $this->_em->flush();
        }
        return ;
    }
    
    function processDeviceData($device, $data)
    {
        $deviceType = $data["deviceTypeId"];
        switch($deviceType)
        {
            case 10:
                $this->processInterface($device, $data);
                break;
            case 100:
            case 200:
            case 800:
                $this->processPowerMeter($device, $data);
                break;
            case 1000:
                $this->processQRcodeReader($device, $data);
                break;
            case 2000:
                $this->processSensor($device, $data);
                break;
            default:
                break;
        }
    }
}
