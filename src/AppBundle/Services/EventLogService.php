<?php

namespace AppBundle\Services;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class EventLogService
{
    private $_em;
    private $_logger;

    public function __construct(EntityManager $em, $logger)
    {
        $this->_em = $em;
        $this->_logger = $logger;
    }
    
    /*
    $headers = $request->headers->all();
    $user = $headers['x-xm-user'];
    $this->logger->info('data', [
        'user' => $user[0],
        'route' => $request->getMethod() . ' ' . $request->getRequestUri(),
        'from' => $request->getClientIps(), 
        'headers' => $request->headers->all(),
        'request body' => $request->request->all(),
        'route_name' => $request->get('_route'),
        
        'status' => $response->getStatusCode(),
        'response' => json_decode($response->getContent(), true),
    ]);
    */ 

    public function checkLogApi($route_name)
    {
        if ( $route_name == 'ev_profile_edit_device_token' || 
            $route_name == 'ev_sms_login' || $route_name == 'ev_vehicle_list') {
            return true;
        }
        return false;
    }

    public function logApiEvent(Request $request, Response $response)
    {
        $route_name = $request->get('_route');
        
        // log certein route only
        $logOrNot = $this->checkLogApi($route_name);
        if ( $logOrNot == false ) {
            return false;
        }
        $headers = $request->headers->all();
        $route = $request->getMethod() . '' . $request->getRequestUri();
        $from = $request->getClientIp();
        $request_body = $request->request->all();
        $response_status = $response->getStatusCode();
        $response_body = json_decode($response->getContent(), true);
        
        $user = '';
        if ( isset($headers['x-xm-user'] )) {
            if ( count($headers['x-xm-user']) != 0) {
                $user = $headers['x-xm-user'][0];
            }
        } 

        // test -- log to file first
        $this->_logger->info('api_data', [
            'user' => $user,
            'route' => $route,
            'from' => $from, 
            'headers' => $headers,
            'request body' => $request_body,
            'route_name' => $route_name,
            
            'status' => $response_status,
            'response' => $response_body,
        ]);
    }
}