<?php
/**
 * Created by PhpStorm.
 * User: chiehyingpan
 * Date: 2018/3/20
 * Time: 上午11:19
 */

namespace AppBundle\Services;

use AppBundle\Services\tree\NestedTree;
use AppBundle\Entity\EmsGatewayStructure;
use Doctrine\ORM\EntityManager;

class GatewayTree extends NestedTree
{
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->entityRepo = $em->getRepository('AppBundle:EmsGatewayMaster');
        $this->treeRepo = $em->getRepository('AppBundle:EmsGatewayStructure');
    }

    protected function newEntityTree($entity)
    {
        $entityStr = new EmsGatewayStructure();
        $entityStr->setGateway($entity);
        return $entityStr;
    }

    protected function getEntityTree($entity)
    {
        $entityStr = new EmsGatewayStructure();
        $entityStr->setGateway($entity);
        return $entityStr;
    }

    protected function getEntity($entityTree)
    {
        return $entityTree->getGateway();
    }
}