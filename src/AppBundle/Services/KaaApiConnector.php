<?php
/**
 * Created by PhpStorm.
 * User: chiehyingpan
 * Date: 2017/12/14
 * Time: 下午1:54
 */
namespace AppBundle\Services;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class KaaApiConnector
{

    private $kaaUrl;
    private $kaaUserPwd;
    private $appToken;
    private $gatewayGroup = array();
    private $appId;
    private $logger;

    public function __construct($url, $userPwd, $appToken, $logger)
    {
        $this->kaaUrl = $url;
        $this->kaaUserPwd = $userPwd;
        $this->appToken = $appToken;
        $this->logger = $logger;
        $app = $this->getAppByAppToken($appToken);
        $this->appId = $app['id'];
    }

    private function getAppByAppToken($appToken)
    {
        // echo $appToken;
        $result = $this->sendApiCall("GET", "application/" . $appToken);
        return $result;
    }

    public function getEndpointGroupsByAppToken($appToken)
    {
        $result = $this->sendApiCall('GET', 'endpointGroups/' . $appToken);
        return $result;
    }

    public function getConfigurationSchemasByAppToken($appToken)
    {
        $result = $this->sendApiCall('GET', 'configurationSchemas/' . $appToken);
        return $result;
    }

    public function getConfigurationSchemaIdByVersion($schemaVersion)
    {
        $result = $this->sendApiCall('GET', 'configurationSchemas/' . $this->appToken);
        foreach($result as $schema)
        {
            if ( $schema['version'] == $schemaVersion ) {
                return $schema['id'];
            }
        }
        return null;
    }

    public function getNotificationSchemasByAppToken() {
        $result = $this->sendApiCall('GET', 'notificationSchemas/'.$this->appToken);
        return $result;
    }

    public function getNotificationTopics() {
        $result = $this->sendApiCall('GET', 'topics/'.$this->appToken);
        return $result;
    }

    public function addTopicToEpGroup($groupId, $topicId) {
        $result = $this->sendApiCall('POST', 'addTopicToEpGroup?endpointGroupId='.$groupId.'&topicId='.$topicId);
        return $result;
    }

    public function getEndpointGroupIdByGatewayId($gatewayId)
    {
        if (!isset($this->gatewayGroup[$gatewayId])) {
            $groupName = 'GW-' . strtoupper($gatewayId);
            $endpointGroups = $this->getEndpointGroupsByAppToken($this->appToken);
            if ($endpointGroups == null) {
                return null;
            }
            // print_r($endpointGroups);
            foreach ($endpointGroups as $group) {
                if ($group['name'] == $groupName) {
                    $this->gatewayGroup[$gatewayId] = $group['id'];
                    return $group['id'];
                }
            }
            return null;
        }
        return $this->gatewayGroup[$gatewayId];
    }

    /*
     * Power Meter Notification
     * Power Meter Switch: On/Off/NoChange
     * Set Power Meter Time:
     * Reset Power Meter: Reset/NoChange
     * Device (Meter) ID: <device ID>
     */

    public function getGatewayNotificationTopics($gatewayId)
    {
        $groupId = $this->getEndpointGroupIdByGatewayId($gatewayId);
        if ($groupId == null ) {
            return null;
        }
        $result = $this->sendApiCall('GET', 'topics', array('endpointGroupId' => $groupId));
        return $result;
    }

/*    public function getEndpointProfileByGateway($gatewayId) {
        $groupId = $this->getEndpointGroupIdByGatewayId($gatewayId);
        if ( $groupId == null ) {
            return null;
        }
        $data = array(
            'endpointGroupId' => $groupId,
            'limit' => 20,
            'offset' => 0
        );
        $result = $this->sendApiCall('GET', 'endpointProfileBodyByGroupId', $data);
        return $result;
    }
 */
    public function getEndpointHashByGateway($gatewayId) {
        $groupId = $this->getEndpointGroupIdByGatewayId($gatewayId);
        if ( $groupId == null ) {
            return null;
        }
        $data = array(
            'endpointGroupId' => $groupId,
            'limit' => 20,
            'offset' => 0
        );
        $result = $this->sendApiCall('GET', 'endpointProfileBodyByGroupId', $data);
        if ( $result == null  ) {
            return null;
        }
        $endpointHash = array();
        foreach ($result['endpointProfilesBody'] as $endpoint) {
            $endpointHash[] = $endpoint['endpointKeyHash'];
        }
        return $endpointHash;
    }

    //public function getPowerMeterNotificationIdByApp()
    public function getPowerStationTopicIdByName($topic_name='Power Meter')
    {
        $result = $this->sendApiCall('GET', 'topics/' . $this->appToken);
        if ( $result == null ) {
            return null;
        }

        // result example :
        // [{"id":"32769","applicationId":"98304","name":"Power Meter","type":"MANDATORY","description":"Notification of Power Meters","createdUsername":"chiehying","createdTime":1492506085517,"secNum":31}]
        foreach ( $result as $topic ) {
            if ( $topic['name'] == $topic_name ) {
                return $topic['id'] ;
            }
        }
        return null;
    }


    public function getPowerStationNotificationSchemaId()
    {
        //$result = $this->sendApiCall('GET', 'userNotificationSchemas/' . $this->appToken);
        $result = $this->sendApiCall('GET', 'notificationSchemas/' . $this->appToken);
        if ( $result == null ) {
            return null;
        }

        // $this->logger->info("getPowerStationNotificationSchemaId", $result );
        // result example :
        // [{"id":"98307","version":1},{"id":"131072","version":2},{"id":"131073","version":3},{"id":"131074","version":4},{"id":"196611","version":5},{"id":"196612","version":6},{"id":"360448","version":7},{"id":"491520","version":8},{"id":"491521","version":9}]
        // it looks sorted, but no garentee

        //
        // assume there is only one schema with diferent version
        //
        $schemas = $result;
        $latest_schema = $schemas[ count($schemas) - 1 ];
        foreach ($schemas as $schema) {
            if ( $schema['version'] > $latest_schema['version'] ) {
                $latest_schema = $schema;
            }
        }

        return $latest_schema['id'];
    }

    public function getPowerMeterNotificationIdByGateway($gatewayId)
    {
        $result = $this->getGatewayNotificationTopics($gatewayId);
        if ( $result == null ) {
            return null;
        }
        foreach ($result as $topic) {
            if ( $topic['name'] == "Power Meter") {
                return $topic['id'];
            }
        }
        return null;
    }

    function multipart_build_query($fields, $boundary){
        $retval = '';
        foreach($fields as $name => $v){
            $data = $v[0];
            $filename = (isset($v[1])) ? '; filename="' . $v[1] . '"' : '';
            $type = (isset($v[2])) ? 'Content-Type: ' . $v[2] . "\r\n" : '';

            $retval .=  "--$boundary\r\n" .
                "Content-Disposition: form-data; name=\"$name\"$filename\r\n$type\r\n" .
                "$data\r\n";
        }
        $retval .= "--$boundary--\r\n";
        return $retval;
    }

/*    public function setPowerSwitch($device, $action)
    {
        if ( is_int($action ) ) {
            if ( $action == 1 ) {
                $action = 'On';
            }
            if ( $action == 0 ) {
                $action = 'Off';
            }
        }
        $this->sendPowerMeterNotification($device, arra('powerSwitch')})
    }*/

    /*
     * param notificationContent:
     *      powerSwitch: Emu (String): On/Off/NoChange
     *      setTimestamp: long
     *      reset: Emu (String) : Reset/NoChange
     *      id: string
     */
    public function sendPowerMeterNotification($circuit, $notificationContent, $extra = null)
    {
        if ( !$circuit ) {
            return null;
        }

        $device = $circuit->getValidDevice();
        if ( !$device ) {
            $this->logger->error("Cannot notification because no device ", [$circuit->getCircuitId(), $notificationContent] );
            return null;
        }
        $gateway = $device->getValidGateway();
        if ( !$gateway ) {
            $this->logger->error("Cannot notification because no such gateway, check mapping status first", [$circuit->getCircuitId(), $device->getDeviceId(), $notificationContent] );
            return null;
        }
        $gatewayId = $gateway->getGatewayId()->__toString();
        $circuitId = $circuit->getCircuitId();
        $body = array(
            "powerSwitch" => array(
                "com.xmight.iot.power.notification.Switch" =>
                    isset($notificationContent["powerSwitch"]) ? $notificationContent["powerSwitch"]:"NoChange"
            ),
            "setTimestamp" => array(
                "long" => isset($notificationContent["setTimestamp"]) ? $notificationContent["setTimestamp"]:0
            ),
            "reset" => array(
                "com.xmight.iot.power.notification.Reset" =>
                    isset($notificationContent["reset"]) ? $notificationContent["reset"]:"NoChange"
            ),
            //"id" => $circuitId
            "id" => $circuitId->__toString(),
            // "id" => [ "string" => $circuitId->__toString(), ],
        );

        if ( isset( $extra["chargingBarcode"] ) ) {
            $args = [ "barCodeId" => $extra["chargingBarcode"] ] ;
            if ( isset( $extra["authErrorCode"] ) ) {
                $args["authErrorCode"] = $extra["authErrorCode"] ;
            }
            $args_jsonstr = json_encode( $args );
            $body["args"] = [ "string" => $args_jsonstr ];
            $body["func"] = [ "long" => 1 ];
        } else if ( isset( $extra["userId"] )  ) {
            $body["args"] = [ "string" => json_encode( ["userId" => $extra["userId"]] ) ];
            $body["func"] = [ "long" => 2 ];
        } else {
            $body["func"] = [ "long" => 0 ];
            $body["args"] = [ "string" => "" ];
        }
        if ( isset($extra["func"]) ) {
            $func_id = $extra["func"] ;
            $body["func"] = [ "long" => $func_id ];
        }


        $this->logger->info("notificationBody", $body);

        $topicId = $this->getPowerStationTopicIdByName('Power Meter');
        //$topicId = $this->getPowerMeterNotificationIdByGateway($gatewayId);
        if ( $topicId == null ) {
            $this->logger->error("Cannot notification because null topic", ["gatewayId" => $gatewayId, $topicId] );
            return ;
        }

        if ( isset($extra["endpointKeyHash"]) ) {
            $keyhash = $extra["endpointKeyHash"] ;
            $endpointHash = [ $keyhash ];
        } else {
            $endpointHash = $this->getEndpointHashByGateway($gatewayId);
        }

        $schemaId = $this->getPowerStationNotificationSchemaId();
        //$this->logger->info("sendPowerMeterNotification: schemaId is ", [ $schemaId ] );
        $notification = array(
            'applicationId' => $this->appId,
            //'schemaId' => '196612',
            //'schemaId' => '491521',
            'schemaId' => $schemaId,
            'topicId' => $topicId,
            'type' => 'USER',
        );

        $notificationData = json_encode($notification, true);
        $this->logger->info("notificationData ", $notification);
        $bodyData = json_encode($body, true);
        $endpoints = array();

        foreach($endpointHash as $endpoint ) {
            $data = array(
                "notification" => array(
                    $notificationData,
                    null,
                    'application/json'),
                "endpointKeyHash" => array(
                    $endpoint,
                    null,
                    'text/plain'),
                "file" => array(
                    //file_get_contents('/Users/chiehyingpan/Desktop/notification.json'),
                    $bodyData,
                    'notification.json',
                    'application/octet-stream')
            );

            $this->logger->info("sendPowerMeterNotification ", $data);

            $result = $this->sendApiCall('POST', 'sendUnicastNotification', $data, true );
            if ( is_array($result) && isset($result["endpointKeyHash"])) {
                $endpoints[] = $result['endpointKeyHash'];
            }
        }

        return $endpoints;
    }

/*    public function sendNotificaition()
    {

        $result = $this->sendApiCall('POST', 'activateConfiguration', null);
    }*/

    /*
     * @param gatewayID gateway ID
     * @param configuration array.
     *      firmwareVersion
     *      firmwareTimestamp
     *      firmwareURL
     *      firmwareChecksum
     *      devices
     *  unchanged elemets are set to array("org.kaaproject.configuration.unchangedT" => "unchanged")
     */
    public function sendGatewayConfiguration($gatewayId, $configurations, $schemaVersion)
    {
        $config = array(
            "firmwareVersion" => array(
                "org.kaaproject.configuration.unchangedT" => "unchanged"),
            "firmwareTimestamp" => array(
                "org.kaaproject.configuration.unchangedT" => "unchanged"),
            "firmwareURL" => array(
                "org.kaaproject.configuration.unchangedT" => "unchanged"),
            "firmwareChecksum" => array(
                "org.kaaproject.configuration.unchangedT" => "unchanged"),
            "devices" => array(
                "org.kaaproject.configuration.unchangedT" => "unchanged")
        );

        if ( $schemaVersion >= 7 ) {
            $config["gatewayStructure"] = ["org.kaaproject.configuration.unchangedT" => "unchanged"];
            $config["chargingKey"] = ["org.kaaproject.configuration.unchangedT" => "unchanged"];
        }

        foreach($configurations as $oneConfig => $oneValue ) {
            $config[$oneConfig] = array("string" => $oneValue);
        }

        $groupId = $this->getEndpointGroupIdByGatewayId($gatewayId);
        if ( $groupId == null ) {
            throw new \Exception( "cannot find EndpointGroupId create it first.  " . $gatewayId);
            return null;
        }
        $schemaId = $this->getConfigurationSchemaIdByVersion($schemaVersion);
        if ( $schemaId == null ) {
            return null;
        }
        //TODO get record version
        $data = array(
            'applicationId' => $this->appId,
            'endpointGroupId' => $groupId,
            "schemaVersion" => 5,
            "schemaId" =>  $schemaId,
            "status" => "ACTIVE",
            "version" => 0,
            'body' => json_encode($config, true)
        );
        $result = $this->sendapicall('POST', 'configuration', $data);
        if ( !$result ) {
            return null;
        }
        // print_r($result);
        if ( !is_array($result)) {
            return null;
        }

        if ( isset($result['code']) && $result['code'] != 200 ) {
            return $result['code'];
        }
        $configId = isset($result['id']) ? $result['id'] : null;
        if ( $configId != null ) {
            $result2 = $this->sendApiCall('POST', 'activateConfiguration', $configId);
            return $result2;
        }
        return null;
    }

    protected function sendApiCall($method, $api, $data=false, $form=false)
    {


        $ch = curl_init();
        $url = $this->kaaUrl.$api;

        $headers = array();
        $headers[] = "Accept: application/json";

        $postFields = null;
        switch(strtoupper($method)) {
            case "POST":
                curl_setopt($ch, CURLOPT_POST, true);
                if ($data) {
                    if ( $form ) {
                        $boundary = md5(time());
                        $headers[] = 'Content-Type: multipart/form-data; boundary=' . $boundary;
                        $postFields = $this->multipart_build_query($data, $boundary);
//                         $headers[] = 'Content-Type: multipart/form-data';
//                         $postFields = $data;
                    } else {
                        $headers[] = 'Content-Type: application/json';
                        $postFields = json_encode($data);
                    }
                }
                break;
            case "PUT":
                curl_setopt($ch, CURLOPT_PUT, true);
                break;
            default:
                if ($data) {
                    $url = sprintf("%s?%s", $url, http_build_query($data));
                }
                break;
        }

        curl_setopt($ch, CURLOPT_USERPWD, $this->kaaUserPwd);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, '3');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_URL, $url);
        if ( $postFields != null ) {
            curl_setopt($ch, CURLOPT_POSTFIELDS,  $postFields);
        }
        $content = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if ( $httpcode != 200 ) {
            if ( isset($this->logger) ) {
                $this->logger->error("sendAPI fail", [ "status code" => $httpcode, "url" => $url ] );
            }
        }

        // echo $content;
        $result = json_decode($content, true);

        return $result;
    }
    
    function powerSwitch($circuit, $action, $extra = null) {
        //$this->logger->error("enter powerSwitch");
        return $this->sendPowerMeterNotification($circuit, array('powerSwitch' => $action), $extra);
    }
    
}
