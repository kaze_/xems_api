<?php
/**
 * Created by PhpStorm.
 * User: chiehyingpan
 * Date: 2018/3/22
 * Time: 下午2:50
 */

namespace AppBundle\Services;

use DateInterval;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use EspBundle\Services\EnergyFee;
use EspBundle\Services\TaiPower;
use Symfony\Component\Finder\Iterator\DateRangeFilterIterator;

class PowerReport
{

    const demandInterval = 900;

    private $em;
    private $tree;
    private $tpc;

    public function __construct(EntityManager $em, CircuitTree $tree, EnergyFee $esp, TaiPower $tpc, $logger)
    {
        $this->em = $em;
        $this->tree = $tree;
        $this->esp = $esp;
        $this->tpc = $tpc;
        $this->logger = $logger;
    }

    public function getCircuitDemandWithStations($circuit, $condition, $customerRate)
    {
        $repository = $this->em->getRepository('AppBundle:EmsPowerReportLog');
        if ( $condition['startDate'] > $condition['endDate'] ) {
            return [];
        }
        $interval = $condition['interval'] ? $condition['interval'] : $this::demandInterval;
        // get main circuit demand
        try {
            $qb = $repository->createQueryBuilder('q');
            $qb->select('FROM_UNIXTIME((FLOOR(UNIX_TIMESTAMP(q.reportDatetime)/:interval)*:interval)) as time, max(q.currDmdKw) as currDmdKw')
            ->where('q.reportDatetime Between :startDate and :endDate')
            ->andWhere('q.circuitId = :circuit')
            ->setParameter('startDate', $condition['startDate'])
            ->setParameter('endDate', $condition['endDate'])
            ->setParameter('circuit', $circuit)
            ->setParameter('interval', $interval)
            ->groupBy('time')
            ->orderBy('time', 'ASC');

            if (isset($condition['offset']) && $condition['offset'] != null ) {
                $qb->setFirstResult($condition['offset']);
            }
            
            if (isset($condition['limit']) && $condition['limit'] != null ) {
                $qb->setMaxResults($condition['limit']);
            }
            
            $circuitDemand = $qb->getQuery()->getResult();
            $circuitDemand2 = array();
            foreach($circuitDemand as $row) {
                $circuitDemand2[$row['time']] = $row;
            }
        } catch(ORMException $e) {
            // echo($e->getMessage());
            return null;
        }

        // get charging station demand
        $stations = $this->tree->getChargingStations($circuit);
        $hasStation = true;
        if ( $stations == null || count($stations) == 0 ) {
            $hasStation = false;
        } else {
            $circuitIds = array();
            foreach($stations as $station){
                $circuitIds[] = $station->getCircuitId();
                // $circuitId = $station->getCircuitId();
            }
            $qb2 = $repository->createQueryBuilder('p');
            $qb2->select('p.circuitId as circuit, FROM_UNIXTIME((FLOOR(UNIX_TIMESTAMP(p.reportDatetime)/:interval)*:interval)) as time, max(p.currDmdKw) as stationDmd')
            ->where('p.reportDatetime Between :startDate and :endDate')
            ->andWhere('p.circuitId in (:circuits)')
            ->setParameter('startDate', $condition['startDate'])
            ->setParameter('endDate', $condition['endDate'])
            ->setParameter('circuits', $circuitIds)
            ->setParameter('interval', $interval)
            ->groupBy('time')
            ->addGroupBy('circuit')
            ->orderBy('time', 'ASC');
            
            if (isset($condition['offset']) && $condition['offset'] != null ) {
                $qb2->setFirstResult($condition['offset']);
            }
            
            if (isset($condition['limit']) && $condition['limit'] != null ) {
                $qb2->setMaxResults($condition['limit']);
            }
            
            $stationDemand = $qb2->getQuery()->getResult();
            $stationDemand2 = array();
            
            foreach($stationDemand as $row) {
                $time = $row['time'];
                if ( isset($stationDemand2[$time]) ) {
                    $stationDemand2[$time] += $row['stationDmd'];
                } else {
                    $stationDemand2[$time] = $row['stationDmd'];
                }
            }
        }
        $singleContract = false;
        $oneContract = 0;
        $contract = null;
        if ( $customerRate ) {
            $contract = $customerRate['contract'];
            $package = $customerRate['energyPackageId'];
            if ( count($contract) == 1 ) {
                $singleContract = true;
                $oneContract = $contract['p'];
            }
        } else {
            $singleContract = true;
            $oneContract = 0;
        }
        $data = array();

        $startTimestamp = $condition['startDate']->getTimestamp();
        $range = $condition['endDate']->getTimestamp() - $startTimestamp;
        $realStart = floor($startTimestamp/$interval) * $interval;

        // get timelinee from startTime to endTime
        $timeline = [];
        for ( $i = 0; $i < $range; $i += $interval) {
            $timeline[] = (new \DateTime())->setTimestamp( $realStart + $i )->format('Y-m-d H:i:s');
        }
        // print_r($timeline);

        // get main circuit demand last week for forecast
        try {
            $qb3 = $repository->createQueryBuilder('r');
            $qb3->select('FROM_UNIXTIME((FLOOR(UNIX_TIMESTAMP(r.reportDatetime)/:interval)*:interval)) as time, max(r.currDmdKw) as currDmdKw')
            ->where('r.reportDatetime Between :startDate and :endDate')
            ->andWhere('r.circuitId = :circuit')
            ->setParameter('startDate', $condition['startDate']->sub(new DateInterval('P1W')))
            ->setParameter('endDate', $condition['endDate']->sub(new DateInterval('P1W')))
            ->setParameter('circuit', $circuit)
            ->setParameter('interval', $interval)
            ->groupBy('time')
            ->orderBy('time', 'ASC');

            if (isset($condition['offset']) && $condition['offset'] != null ) {
                $qb3->setFirstResult($condition['offset']);
            }
            
            if (isset($condition['limit']) && $condition['limit'] != null ) {
                $qb3->setMaxResults($condition['limit']);
            }
            
            $circuitDemandLastweek = $qb3->getQuery()->getResult();
            $circuitDemandLastweek2 = array();
            foreach($circuitDemandLastweek as $row) {
                $circuitDemandLastweek2[$row['time']] = $row;
            }
        } catch(ORMException $e) {
            // echo($e->getMessage());
            return null;
        } 

        // prepare return data
        foreach($timeline as $time)  {
            $totalDemand = isset($circuitDemand2[$time]) ? $circuitDemand2[$time]['currDmdKw'] : 0;
            $stationDmd = 0;
            if ( $hasStation ) {
                if ( isset($stationDemand2[$time])) {
                    $stationDmd = $stationDemand2[$time];
                }
                $subDemand = $totalDemand - $stationDmd;
                if ( $subDemand < 0 ) $subDemand = 0;
            } else {
                $subDemand = $totalDemand;
            }

            $forecast_demand = null;
            if ( $subDemand == 0 && $stationDmd == 0 ) { 
                $lastWeek = ((\Datetime::createFromFormat('Y-m-d H:i:s', $time))->modify('-1 week'))->format('Y-m-d H:i:s');
                if( isset($circuitDemandLastweek2[$lastWeek])){
                    $forecast_demand = (float) $circuitDemandLastweek2[$lastWeek]['currDmdKw'];
                }
            }

            if ( $singleContract ) {
                $max_demand = $oneContract;
            } else {
                $rate = $this->tpc->getRate($package, \DateTime::createFromFormat('Y-m-d H:i:s', $time));
                $tou = isset( $rate['tou'] ) ? $rate['tou'] : null;
                $max_demand = 0;
                if ( $tou ) {
                    switch ($tou) {
                        case 'p' :
                            $max_demand = $contract['p'];
                            break;
                        case 'ssp': 
                            $max_demand = $contract['p'] +
                               (isset($contract['ssp'])?$contract['ssp']:0);
                            break;
                        case 'op':
                            $max_demand = $contract['p'] + 
                                (isset($contract['ssp'])?$contract['ssp']:0) + 
                                (isset($contract['op'])?$contract['op']:0);
                            break;
                    }
                }
            }
            array_push($data, array($time, (float) $subDemand, (float) $stationDmd,$max_demand,$forecast_demand));
        } 
        return $data;
    }

    // circuitID : AppBundle\\Entity\\EmsCircuitMaster
    public function getCircuitFee($circuitID, $start, $end, $espCustomerRateDetail=null)
    {
        if ( !$espCustomerRateDetail ) {
            $rootCircuit = $this->tree->getRoot($circuitID);
            if ( !$rootCircuit ) {
                // todo: find by location?;
                return null;
            }
            $meter = $rootCircuit->getValidDevice();

            $espRepo = $this->em->getRepository('EspBundle:EspCustomerMaster');
            $espCustomer = $espRepo->findOneBy(array('powerMeterDeviceId' => $meter->getDeviceId()));
            if ( !$espCustomer ) {
                return null;
            }
            $espCustomerRateDetail = $this->esp->getCustomerRateDetail($espCustomer);
            if ( !$espCustomerRateDetail ) {
                return null;
            }
        }

        if ( $end == null ) {
            $endDatetime = new \DateTime();
            $end = $endDatetime->format('Y-m-d H:i:s');
        }

        $logRepo = $this->em->getRepository('AppBundle:EmsPowerReportLog');
        try {
            $logs = $logRepo->findDataByCircuit(array('reportDatetime', 'kwhTotal', 'currDmdKw'), array(
                'startDate' => $start,
                'endDate' => $end,
                'circuit' => $circuitID
            ), array('reportDatetime' => 'ASC'));
        } catch(ORMException $exception) {
            // catch error for ignore it ?
            $this->logger->error("getCircuitFee error ", [$exception->getMessage()]);
            return null;
        }

        if ( empty($logs) ) {
            return null;
        }

        $data = array();
        $dmdData = array();
        $firstDatetime = $logs[0]['reportDatetime'];
        foreach($logs as $log) {
            $oneData = array($log['reportDatetime']->format('Y-m-d H:i'), $log['kwhTotal']);
            $data[] = $oneData;
            $oneDmdData = array($log['reportDatetime']->format('Y-m-d H:i'), $log['currDmdKw']);
            $dmdData[] = $oneDmdData;
        }

        $energy = $this->tpc->reduceMonthlyToPower($espCustomerRateDetail['energyPackage'], $data);
        $maxDmd = null;
        $energyArray = array('energy' => $energy, 'date' => $firstDatetime->format('Y-m-d'));
        if ( $espCustomerRateDetail['hasContract']) {
            $maxDmd = $this->tpc->getMaxDmdData($espCustomerRateDetail['energyPackage'], $dmdData);
            $contract = array(
                "normal" => $espCustomerRateDetail["normalContract"],
                "semi_peak" => $espCustomerRateDetail["semiPeakContract"],
                "saturday_semi_peak" => $espCustomerRateDetail["saturdaySemiPeakContract"],
                "off_peak" => $espCustomerRateDetail["offPeakContract"]
            );
            $fee = $this->tpc->getPricing($espCustomerRateDetail['energyPackage'], $data, $energyArray, $contract, $maxDmd);
        } else {
            $fee = $this->tpc->getPricing($espCustomerRateDetail['energyPackage'], $data, $energyArray);
        }
        $ret = array();
        $ret['startKwh'] = $data[0][1];
        $ret['endKwh'] = $data[count($data)-1][1];
        $ret['totalKwh'] = $ret['endKwh'] - $ret['startKwh'];
        $basic = 0;
        $basic_excees = 0;
        $amount = 0;
        $extra = array();
        $total_kwh = 0;

        if ( $fee && count($fee) != null ) {
            foreach ($fee as $tou => $touFee) {
                if ($tou == 'basic') {
                    $basic = $touFee;
                    continue;
                }
                if ($tou == 'basic_excees') {
                    $basic_excees = $touFee;
                    continue;
                }
                $extra[$tou]['amount'] = $touFee;
                $extra[$tou]['kwh'] = $energy[$tou];
                if ( $maxDmd ) {
                    $extra[$tou]['usage'] = $maxDmd[$tou];
                }
                $amount += $touFee;
//                $total_kwh += $energy[$tou];                
            }
        }
        $ret['fee'] = $extra;
/*        foreach($fee as $tou => $touFee) {
            $basic += $touFee['basic'];
            $amount += $touFee['amount'];
            $fee[$tou]['usage'] = 0;
        }*/
        // $ret['fee'] = $fee;
        $ret['basic'] = $basic;
        $ret['basicExcees'] = $basic_excees;
        $ret['amount'] = $amount;
        return $ret;
    }

    public function getCircuitDailyFee($circuitId, $date, $espCustomerRateDetail=null)
    {

    }

    public function getCircuiDailyUsage($circuitId, $date, $tou)
    {

    }
}
