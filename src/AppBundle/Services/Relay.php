<?php
/**
 * Created by PhpStorm.
 * User: chiehyingpan
 * Date: 2018/4/3
 * Time: 上午1:04
 */
namespace AppBundle\Services;

use AppBundle\Entity\EmsCircuitMaster;
use AppBundle\Entity\EmsSmartRelayTransactionLog;
use AppBundle\Entity\EvChargingStationMaster;
use Doctrine\ORM\EntityManager;

class Relay
{

    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    // source : gateway or user
    // sourceId : user id
    public function setStatus($circuit, $statusId, $source, $sourceId, $status = null, $kaaHash = null, $comment = null, $session_oid = null, $ev_plate = null, $event_time = null)
    {
        if ($circuit instanceof EmsCircuitMaster) {
            $circuitObj = $circuit;
        } else {
            $circuitRepo = $this->em->getRepository('AppBundle:EmsCircuitMaster');
            $circuitObj = $circuitRepo->find($circuit);
        }

        $relayLog = new EmsSmartRelayTransactionLog();
        $relayLog->setCircuit($circuitObj);
        $relayLog->setSource($source);
        $relayLog->setSourceId($sourceId);
        $relayLog->setStatusId($statusId);
        $relayLog->setComment($comment);

        if ( $status ) {
            $relayLog->setSessionStatus($status);
        }
        if ($kaaHash) {
            $relayLog->setKaaEndpointHash($kaaHash);
        }

        if ($circuitObj->getEvChargingStation() == true) {
            $stations = $circuitObj->getChargingStation();
            if ($stations && ! empty($stations)) {
                $relayLog->setChargingStation($stations[0]);
            }
        }

        if ( $session_oid != null ) {
            $relayLog->setSessionId($session_oid);
        }
        if ( $ev_plate != null ) {
            $relayLog->setEvPlate( $ev_plate );
        }

        if ( $event_time != null ) {
            $relayLog->setTransactionTime( $event_time );
        }

        try {
            $this->em->persist($relayLog);
            $this->em->flush();
        } catch (\Exception $e) {
            return;
        }
        return;
    }

    /**
     *
     * @param
     *            $circuit
     * @return array( 'stationId' => statusId,
     *         'sessionId' => sessionId
     *         )
     */
    public function getCircuitStatus($circuit)
    {
        if ($circuit instanceof EmsCircuitMaster) {
            $circuitObj = $circuit;
        } else {
            $circuitRepo = $this->em->getRepository('AppBundle:EmsCircuitMaster');
            $circuitObj = $circuitRepo->find($circuit);
        }
        $relayRepo = $this->em->getRepository('AppBundle:EmsSmartRelayTransactionLog');
        $relay = $relayRepo->findOneBy(array(
            'circuit' => $circuitObj,
            'source' => EmsSmartRelayTransactionLog::SOURCE_GATEWAY
        ), array(
            'transactionTime' => 'DESC'
        ));

        $logRepo = $this->em->getRepository('AppBundle:EmsPowerReportLog');
        $log = $logRepo->createQueryBuilder('c')
            ->where('c.circuitId = :circuitId')
            ->andWhere('c.reportDatetime > :datetime')
            ->orderBy('c.reportDatetime', 'DESC')
            ->setMaxResults(1)
            ->setParameter('circuitId', $circuitObj->getCircuitId())
            ->setParameter('datetime', (new \Datetime())->modify('-1 days'))
            ->getQuery()->getOneOrNullResult();
        
        if (! $logRepo && ! $log) {
            return null;
        }

        $relayTime = ($relay) ? $relay->getTransactionTime() : (new \Datetime())->setTimestamp(0);
        $logTime = ($log) ? $log->getReportDatetime() : (new \DateTime())->setTimestamp(0);
        $status = null;
        if ( $relay != null ) {
//        if ($relay && $relayTime >= $logTime) {
            $statusId = $relay->getStatusId();
            switch ($statusId) {
                case EmsSmartRelayTransactionLog::ACT_STARTED:
                case EmsSmartRelayTransactionLog::ACT_ENDING:
                    $status = EmsSmartRelayTransactionLog::STAT_ON;
                    break;
                case EmsSmartRelayTransactionLog::ACT_ENDED:
                case EmsSmartRelayTransactionLog::ACT_METERON:
                case EmsSmartRelayTransactionLog::ACT_UNPLUGGED:
                case EmsSmartRelayTransactionLog::ACT_NOCHANGE:
                    $status = EmsSmartRelayTransactionLog::STAT_STANDBY;
                    break;
                case EmsSmartRelayTransactionLog::ACT_METEROFF:
                    $status = EmsSmartRelayTransactionLog::STAT_OFF;
                    break;
            }
/*        } else if ($log) {
            $current = $log->getAAverage();
            if ($current == 0) {
                $status = EmsSmartRelayTransactionLog::STAT_OFF;
            } else if ($current <= ($circuitObj->getCircuitStandbyCurrent() * 1.1)) {
                $status = EmsSmartRelayTransactionLog::STAT_STANDBY;
            } else {
                $status = EmsSmartRelayTransactionLog::STAT_ON;
            }*/
        } else {
            // ask!!!
            $status = EmsSmartRelayTransactionLog::STAT_STANDBY;
        }
        
        $data = array();
        if ($circuitObj->getEvChargingStation() == true && $status == EmsSmartRelayTransactionLog::STAT_ON) {
            $sessRepo = $this->em->getRepository('EvBundle:EvChargingSessionLog');
            $session = $sessRepo->findOneBy(array(
                'circuit' => $circuitObj
            ), array(
                'startAt' => 'DESC'
            ));
            if ($session && $session->getEndAt() == null) {
                $data['sessionId'] = $session->getSessionId();
            }
        }
        if ( $log ) {
            $data['logCurrent'] = $log->getAAverage();
            $data['logTime'] = $logTime;
        }
        $data['relayTime'] = $relayTime;
        $data['relayStatus'] = 
        $data['statusId'] = $status;
        return $data;
    }

    public function getStationStatus($station)
    {
        if ($station instanceof EvChargingStationMaster) {
            $stationObj = $station;
        } else {
            $stationRepo = $this->em->getRepository('AppBundle:EvChargingStationMaster');
            $stationObj = $stationRepo->find($station);
        }

        $circuits = $stationObj->getCircuit();
        if ($circuits != null && ! empty($circuits)) {
            return $this->getCircuitStatus($circuits[0]);
        }
        return null;
    }

    // get relay status only, don't check current
    public function getRelayStatus($circuit) 
    {
        /*
        if ($circuit instanceof EmsCircuitMaster) {
            $circuitObj = $circuit;
        } else {
            $circuitRepo = $this->em->getRepository('AppBundle:EmsCircuitMaster');
            $circuitObj = $circuitRepo->find($circuit);
        }
        */
        $relayRepo = $this->em->getRepository('AppBundle:EmsSmartRelayTransactionLog');
        $relay = $relayRepo->findOneBy(array(
            'circuit' => $circuit,
            'source' => EmsSmartRelayTransactionLog::SOURCE_GATEWAY
        ), array(
            'transactionTime' => 'DESC'
        ));
        if ( !$relay ) {
            return null;
        }
        $statusId = $relay->getStatusId();
        switch ($statusId) {
            case EmsSmartRelayTransactionLog::ACT_STARTED:
            case EmsSmartRelayTransactionLog::ACT_ENDING:
                $status = EmsSmartRelayTransactionLog::STAT_ON;
                break;
            case EmsSmartRelayTransactionLog::ACT_ENDED:
            case EmsSmartRelayTransactionLog::ACT_METERON:
            case EmsSmartRelayTransactionLog::ACT_UNPLUGGED:
            case EmsSmartRelayTransactionLog::ACT_NOCHANGE:
                $status = EmsSmartRelayTransactionLog::STAT_STANDBY;
                break;
            case EmsSmartRelayTransactionLog::ACT_METEROFF:
                $status = EmsSmartRelayTransactionLog::STAT_OFF;
                break;
        }
        return ['statusId' => $status];
    }
}
