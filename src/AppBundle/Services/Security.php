<?php

namespace AppBundle\Services;

use Symfony\Component\Security\Core\Role\Role;

class Security 
{
    private $rolesHierarchy;

    public function __construct($rolesHierarchy)
    {
        $this->rolesHierarchy = $rolesHierarchy;
    }

    public function getActualRoles($user)
    {
        $userRoles = $user->getRoles();
        
        $actualRoles = array();
        // $reachableRoles = $this->rolesHierarchy->getReachableRoles($userRoles);
        foreach($userRoles as $role) {
            $reachableRoles = $this->rolesHierarchy->getReachableRoles([new Role($role)]);
            $actualRoles = array_merge($actualRoles, $reachableRoles);
        }
        $strRoles = [];
        foreach($actualRoles as $role) {
            $strRoles[] = $role->getRole();
        } 
        return array_unique($strRoles);
    }

    public function checkRole($role, $user)
    {
        $actualRoles = $this->getActualRoles($user);
        if ( in_array($role, $actualRoles, true)) {
            return true;
        }
        return false;
    }
}