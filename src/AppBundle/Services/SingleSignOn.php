<?php

namespace AppBundle\Services;

use AppBundle\Entity\EmsCircuitMaster;
use AppBundle\Entity\EmsSmartRelayTransactionLog;
use AppBundle\Entity\EvChargingStationMaster;
use Doctrine\ORM\EntityManager;
use Exception;
use Google_Client;

class SingleSignOn
{
    private $em;

    private $googleClientId;

    public function __construct(EntityManager $em, $googleClientId)
    {
        $this->em = $em;
        $this->googleClientId = $googleClientId;
    }

    public function verifyGoogleId($idToken)
    {
        $client = new Google_Client(['client_id' => $this->googleClientId]);  // Specify the CLIENT_ID of the app that accesses the backend
        try {
            $payload = $client->verifyIdToken($idToken);
        } catch(Exception $e) { 
            return false;
        }
        $payload = $client->verifyIdToken($idToken);
        if ($payload) {
            $clientId = $payload['aud']; //client id
            $userId = $payload['sub']; // user id
            $iss = $payload['iss']; // should be https://accounts.google.com.
            $exp = $payload['exp']; // expire timestamp
            $email = $payload['email']; // user email
            return $payload;
          // If request specified a G Suite domain:
          // $domain = $payload['hd'];
        } else {
          // Invalid ID token
          return null;
        }
    }
}