<?php
/**
 * Created by PhpStorm.
 * User: chiehyingpan
 * Date: 2018/3/20
 * Time: 上午11:19
 */

namespace AppBundle\Services\tree;


use Doctrine\ORM\EntityManager;
use Exception;

abstract class NestedTree
{
    protected $em;
    protected $entityRepo;
    protected $treeRepo;

    abstract public function __construct(EntityManager $em);

/*    public function __construct(EntityManager $em)
        {
            $this->em = $em;
            $this->entityRepo = $em->getRepository('AppBundle:EmsCircuitMaster');
            $this->treeRepo = $em->getRepository('AppBundle:EmsCircuitStructure');
        }*/

    abstract protected function newEntityTree($entity);
    abstract protected function getEntityTree($entity);
    abstract protected function getEntity($entityTree);

/*    private function findStructureByCircuit($entity){
        $this->treeRepo = $this->em->getRepository('AppBundle:EmsCircuitStructure');
        if ( $entity instanceof EmsCircuitMaster || is_string($entity)) {
            $entityStr = $this->treeRepo->find($entity);
        } else if ( $entity instanceof EmsCircuitStructure ) {
            $entityStr = $entity;
        } else {
            $entityStr = null;
        }
        return $entityStr;
    }*/

    public function setParent($child, $parent)
    {
        // find entity object
        $childEntity = is_string($child) ? $this->entityRepo->find($child):$child;
        $parentEntity = is_string($parent) ? $this->entityRepo->find($parent):$parent;

        if ( $childEntity == null || $parentEntity == null ) {
            return null;
        }

        $parentTree = $this->treeRepo->find($parentEntity);
        if ( !$parentTree ) {
            $parentTree = $this->getEntityTree($parentEntity);
        }

        $childTree = $this->treeRepo->find($childEntity);
        if ( !$childTree ) {
            $childTree = $this->getEntityTree($childEntity);
        }

        $childTree->setParent($parentTree);
        try {
            $this->em->persist($parentTree);
            $this->em->persist($childTree);
            $this->em->flush();
            return $childTree;
        } catch (Exception $ex) {
            return null;
        }
    }

    public function getChildren($entity)
    {
        $entityTree = $this->treeRepo->find($entity);
        if ( $entityTree == null ) {
            return null;
        }
        $children = $this->treeRepo->children($entityTree);
        if ( !$children ) {
            return null;
        }
        $childCircuits = array();
        foreach ($children as $child) {
            $childCircuits[] = $this->getEntity($child);
        }
        return $childCircuits;
    }

    public function getDirectChildren($entity)
    {
        $entityTree = $this->treeRepo->find($entity);
        if ( $entityTree == null ) {
            return null;
        }

        $children = $this->treeRepo->children($entityTree, true);
        if ( !$children ) {
            return null;
        }
        $childCircuits = array();
        foreach ($children as $child) {
            $childCircuits[] = $this->getEntity($child);
        }
        return $childCircuits;
    }

    public function getChargingStations($entity)
    {
        $children = $this->getChildren($entity);
        if ( !$children ) {
            return null;
        }
        $stations = array();
        foreach($children as $child) {
            if($child->getEvChargingStation() == true ) {
                $stations[] = $child;
            }
        }
        if ( empty($stations) ) {
            return null;
        }
        return $stations;
    }

    public function getParent($entity)
    {
        $entityTree = $this->treeRepo->find($entity);
        if ( $entityTree == null ) {
            return null;
        }
        $parent = $entityTree->getParent();
        if ( $parent )  {
            return $this->getEntity($parent);
        }
        return null;
    }

    const maxTraversal = 100;

    public function findRootByStr($entityTree)
    {
        $s = $entityTree;
        $count = 0;
        while ($s->getParent() != null ) {
            if ( $count++  > $this::maxTraversal ) {
                return null;
            }
            $s = $s->getParent();
        }
        return $s;
    }

    public function getRoot($entity)
    {
        $entityTree = $this->treeRepo->find($entity);
        if ( $entityTree == null ) {
            return null;
        }

        $root = $entityTree->getRoot();
        if ( !$root ) {
            $root = $this->findRootByStr($entityTree);
            if ( !$root ) {
                return null;
            }
        }
        $rootCircuit = $this->getEntity($root);
        // $rootCircuit = $root->getEntity();
        return $rootCircuit;
    }

    public function removeNode($entity)
    {
        $entityTree = $this->treeRepo->find($entity);

        if ( !$entityTree ) {
            return null;
        }
        $this->treeRepo->removeFromTree($entityTree);
        $this->em->clear();
    }
    
    public function exists($entity) 
    {
        if ( $this->entityRepo->find($entity) ) {
            return true;
        }
        return false;
    }
}