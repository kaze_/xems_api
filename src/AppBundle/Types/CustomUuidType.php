<?php
/**
 * Created by PhpStorm.
 * User: chiehyingpan
 * Date: 2017/7/28
 * Time: 下午12:13
 */

namespace AppBundle\Types;

use Ramsey\Uuid\Doctrine\UuidType;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use InvalidArgumentException;
use Ramsey\Uuid\Uuid;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\Type;

class CustomUuidType extends UuidType
{
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if (empty($value)) {
            return null;
        }

        if ($value instanceof Uuid || Uuid::isValid($value)) {
            return (string) $value;
        }

        try {
            return Uuid::fromString($value);
        } catch (InvalidArgumentException $e) {
            throw ConversionException::conversionFailed($value, static::NAME);
        }
//        throw ConversionException::conversionFailed($value, static::NAME);
    }

}