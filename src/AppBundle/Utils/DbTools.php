<?php
/**
 * Created by PhpStorm.
 * User: chiehyingpan
 * Date: 2017/6/15
 * Time: 下午5:38
 */
namespace AppBundle\Utils;

class DbTools
{
    static function getEntityNameFromTableName($tableName)
    {
        $nameArray= explode('_', $tableName);
        $len = count($nameArray);
        switch($len) {
            case 0:
                return null;
            case 1:
                return $tableName;
            default:
                $newNameArray = array();
                for($i = 0; $i < $len; $i++ ) {
                    $newNameArray[$i] = ucfirst($nameArray[$i]);
                }
                $entityName = implode('', $newNameArray);
                return $entityName;
        }
    }
}
