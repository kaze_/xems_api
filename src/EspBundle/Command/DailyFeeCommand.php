<?php
/**
 * Created by PhpStorm.
 * User: chiehyingpan
 * Date: 2018/5/10
 * Time: 上午10:13
 */

namespace EspBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use EspBundle\Entity\EspElectricityFeeDaily;
use Symfony\Component\Validator\Constraints\DateTime;

class DailyFeeCommand extends ContainerAwareCommand
{
    private $_em;
    private $_statService;

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        parent::initialize($input, $output); // TODO: Change the autogenerated stub
        $this->_statService = $this->getContainer()->get('esp.statistics');
        
    }

    protected function configure()
    {
        $this->setName('esp:dailyfee')
            ->setDescription('Calculate daily fee for each circuit')
            ->setHelp('This command allow you to calculate daily fee for previous date')
//            ->addArgument('interval', InputArgument::REQUIRED, 'daily or monthly')
            ->addArgument('date', InputArgument::OPTIONAL, 'date')
            ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // get every circuit
        $date = $input->getArgument('date');
        if ( $date == null ) {
            $startDate = new \DateTime('yesterday');
        } else {
            $startDate = new \DateTime($date);
            $startDate->setTime(0,0,0);
        }
        
        $this->_statService->cycleFee($this->_statService::DAILY, $startDate);
    }
}