<?php
/**
 * Created by PhpStorm.
 * User: chiehyingpan
 * Date: 2018/2/20
 * Time: 上午10:14
 */

namespace EspBundle\Controller;

use AppBundle\Entity\EmsCircuitMaster;
use EvBundle\Controller\BaseApiController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Context\Context;
use AppBundle\Entity\EmsDeviceMaster;

class PricingController extends BaseApiController
{
    public function TaipowerPricingAction(Request $request, $type)
    {
        $reqData = json_decode($request->getContent(), true);
        $data = isset($reqData['data']) ? $reqData['data'] : null;
        if ( $data == null ) {
            return new JsonResponse(array(
                'code' => JsonResponse::HTTP_BAD_REQUEST,
                'msg' => 'missing data log'
            ));
        }
        foreach($data as $datalog) {
        }
        $contract = isset($reqData['contract']) ? $reqData['contract'] : null;
        // no use nowd
        $max_demand = isset($reqData['max_demand']) ? $reqData['max_demand'] : null;

        $tpc = $this->get('esp.tpc');
        $result = $tpc->getPricing($type, $data, $contract);

        if ( !$result ) {
            return new JsonResponse(array(
                'code' => Response::HTTP_BAD_REQUEST
            ));
        }
        return new JsonResponse(array(
            'code' => Response::HTTP_OK,
            'data' => $result),
            Response::HTTP_OK);
    }

    // public function deviceFeeAction(EmsCircuitMaster $circuit)
    public function circuitFeeAction(Request $request, $circuitId)
    {
        $from = $request->get('from');
        $to = $request->get('to');

        $tpc = $this->get('esp.fee');
        $data = $tpc->deviceFee($from, $to, $circuitId);
        /*
        $fee = $this->get('app.power_report');
        $data = $fee->getCircuitFee($circuitId, $from, $to);
        */
        return new JsonResponse(array(
            'code' => Response::HTTP_OK,
            'data' => $data,
        ), Response::HTTP_OK);
    }

    public function deviceFeeAction(Request $request, EmsDeviceMaster $device)
    {
        $from = $request->get('from');
        $to = $request->get('to');
        
        /*        $tpc = $this->get('esp.fee');
         $data = $tpc->deviceFee($from, $to, $circuitId);*/
        $fee = $this->get('app.power_report');
        $circuits = $device->getCircuit();
        if ( $circuits == null || count($circuits) == 0 ) {
            return new JsonResponse(array(
                'code' => Response::HTTP_NOT_FOUND,
                'msg' => 'circuits not found'
            ));
        }
        $id = $circuits[0]->getCircuitId();
        $circuitId = is_object($id) ? $id->toString() : $id ;
        $data = $fee->getCircuitFee($circuitId, $from, $to);
        return new JsonResponse(array(
            'code' => Response::HTTP_OK,
            'data' => $data,
        ), Response::HTTP_OK);
    }
    
    public function metadataAction($esp)
    {
//        $this->denyAccessUnlessGranted('view', $location);
        $view = View::create();
        $context = (new Context())->addGroup('metadata');

        $em = $this->getDoctrine()->getManager();
        // get energy packages
        $espRepo = $em->getRepository('EspBundle:EspEnergySp');
        $sp = $espRepo->findOneByEnergySpName($esp);
        if ( !$sp ) {
            return $this->returnJsonResponse(Response::HTTP_NOT_FOUND, "Energy Service Provider Not Found");
        }
        $packageRepo = $em->getRepository('EspBundle:EspEnergyPackage');
        $packages = $packageRepo->findByEnergySp($sp);
        
        $packageMeta = [];
        foreach($packages as $package) {
            $oneData = [
                'id' => $package->getEnergyPackageId(),
                'name' => $package->getEnergyPackageName()
            ];
            $packageDetail = $em->getRepository('EspBundle:EspEnergyPackageDetail')->findOneBy([
                'energyPackage' => $package->getEnergyPackageId(),
                'statusId' => 1
            ]);
            // print_r($packageDetail->getEnergyPackage()->getEnergyPackageId());
            if ( $packageDetail ) {
                $oneData['hasContract'] = $packageDetail->getHasContract();
                $oneData['hasTou'] = $packageDetail->getHasTou();
            }
            $packageMeta[] = $oneData;
        }
        
        // get voltages
        $voltageRepo = $em->getRepository('AppBundle:EmsVoltage');
        $voltages = $voltageRepo->findAll();

        // get off peak days
        $tpc = $this->get('esp.tpc');
        $days = $tpc->getOffPeakDays();

        $view->setContext($context);
        $view->setStatusCode(200);
        
        $view->setData(array('code' => 200, 'data' => array(
            'energyPackage' => $packageMeta,
            'voltage' => $voltages,
            'offPeakDays' => $days
        )));
        return $view;
    }
}
