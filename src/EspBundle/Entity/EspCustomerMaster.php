<?php

namespace EspBundle\Entity;

/**
 * EspCustomerMaster
 */
class EspCustomerMaster
{
    /**
     * @var string
     */
    private $espCustomerNumber;

    /**
     * @var string
     */
    private $espCustomerName;

    /**
     * @var string
     */
    private $billingAddress;

    /**
     * @var string
     */
    private $circuitId;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $location;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->location = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get espCustomerNumber
     *
     * @return string
     */
    public function getEspCustomerNumber()
    {
        return $this->espCustomerNumber;
    }
	
    /**
     * Get espCustomerNumber
     *
     * @return string
     */
    public function setEspCustomerNumber($espCustomerNumber)
    {
        $this->espCustomerNumber = $espCustomerNumber;
        
        return $this;
    }

    /**
     * Set espCustomerName
     *
     * @param string $espCustomerName
     *
     * @return EspCustomerMaster
     */
    public function setEspCustomerName($espCustomerName)
    {
        $this->espCustomerName = $espCustomerName;

        return $this;
    }

    /**
     * Get espCustomerName
     *
     * @return string
     */
    public function getEspCustomerName()
    {
        return $this->espCustomerName;
    }

    /**
     * Set billingAddress
     *
     * @param string $billingAddress
     *
     * @return EspCustomerMaster
     */
    public function setBillingAddress($billingAddress)
    {
        $this->billingAddress = $billingAddress;

        return $this;
    }

    /**
     * Get billingAddress
     *
     * @return string
     */
    public function getBillingAddress()
    {
        return $this->billingAddress;
    }

    /**
     * Add location
     *
     * @param \AppBundle\Entity\EmsLocationMaster $location
     *
     * @return EspCustomerMaster
     */
    public function addLocation(\AppBundle\Entity\EmsLocationMaster $location)
    {
        $this->location[] = $location;

        return $this;
    }

    /**
     * Remove location
     *
     * @param \EspBundle\Entity\EmsLocationMaster $location
     */
    public function removeLocation(\AppBundle\Entity\EmsLocationMaster $location)
    {
        $this->location->removeElement($location);
    }

    /**
     * Get location
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLocation()
    {
        return $this->location;
    }
    /**
     * @var string
     */
    private $powerMeterDeviceId;


    /**
     * Set powerMeterDeviceId
     *
     * @param string $powerMeterDeviceId
     *
     * @return EspCustomerMaster
     */
    public function setPowerMeterDeviceId($powerMeterDeviceId)
    {
        $this->powerMeterDeviceId = $powerMeterDeviceId;

        return $this;
    }

    /**
     * Get powerMeterDeviceId
     *
     * @return string
     */
    public function getPowerMeterDeviceId()
    {
        return $this->powerMeterDeviceId;
    }
    /**
     * @var int
     */
    private $espCustomerId;


    /**
     * Get espCustomerId.
     *
     * @return int
     */
    public function getEspCustomerId()
    {
        return $this->espCustomerId;
    }
}
