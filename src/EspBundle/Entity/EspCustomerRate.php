<?php

namespace EspBundle\Entity;

/**
 * EspCustomerRate
 */
class EspCustomerRate
{
    /**
     * @var \DateTime
     */
    private $enableDate;

    /**
     * @var string
     */
    private $espCustomerName;

    /**
     * @var string
     */
    private $billingAddress;

    /**
     * @var integer
     */
    private $statusId;

    /**
     * @var string
     */
    private $circuitId;

    /**
     * @var \EspBundle\Entity\EspCustomerMaster
     */
    private $espCustomer;

    /**
     * @var \EspBundle\Entity\EspEnergyPackage
     */
    private $energyPackage;


    /**
     * Set enableDate
     *
     * @param \DateTime $enableDate
     *
     * @return EspCustomerRate
     */
    public function setEnableDate($enableDate)
    {
        $this->enableDate = $enableDate;

        return $this;
    }

    /**
     * Get enableDate
     *
     * @return \DateTime
     */
    public function getEnableDate()
    {
        return $this->enableDate;
    }

    /**
     * Set espCustomerName
     *
     * @param string $espCustomerName
     *
     * @return EspCustomerRate
     */
    public function setEspCustomerName($espCustomerName)
    {
        $this->espCustomerName = $espCustomerName;

        return $this;
    }

    /**
     * Get espCustomerName
     *
     * @return string
     */
    public function getEspCustomerName()
    {
        return $this->espCustomerName;
    }

    /**
     * Set billingAddress
     *
     * @param string $billingAddress
     *
     * @return EspCustomerRate
     */
    public function setBillingAddress($billingAddress)
    {
        $this->billingAddress = $billingAddress;

        return $this;
    }

    /**
     * Get billingAddress
     *
     * @return string
     */
    public function getBillingAddress()
    {
        return $this->billingAddress;
    }

    /**
     * Set statusId
     *
     * @param integer $statusId
     *
     * @return EspCustomerRate
     */
    public function setStatusId($statusId)
    {
        $this->statusId = $statusId;

        return $this;
    }

    /**
     * Get statusId
     *
     * @return integer
     */
    public function getStatusId()
    {
        return $this->statusId;
    }

    /**
     * Set circuitId
     *
     * @param string $circuitId
     *
     * @return EspCustomerRate
     */
    public function setCircuitId($circuitId)
    {
        $this->circuitId = $circuitId;

        return $this;
    }

    /**
     * Get circuitId
     *
     * @return string
     */
    public function getCircuitId()
    {
        return $this->circuitId;
    }

    /**
     * Set espCustomer
     *
     * @param \EspBundle\Entity\EspCustomerMaster $espCustomer
     *
     * @return EspCustomerRate
     */
    public function setEspCustomer(\EspBundle\Entity\EspCustomerMaster $espCustomer = null)
    {
        $this->espCustomer = $espCustomer;

        return $this;
    }

    /**
     * Get espCustomer
     *
     * @return \EspBundle\Entity\EspCustomerMaster
     */
    public function getEspCustomer()
    {
        return $this->espCustomer;
    }

    /**
     * Set energyPackage
     *
     * @param \EspBundle\Entity\EspEnergyPackage $energyPackage
     *
     * @return EspCustomerRate
     */
    public function setEnergyPackage(\EspBundle\Entity\EspEnergyPackage $energyPackage = null)
    {
        $this->energyPackage = $energyPackage;

        return $this;
    }

    /**
     * Get energyPackage
     *
     * @return \EspBundle\Entity\EspEnergyPackage
     */
    public function getEnergyPackage()
    {
        return $this->energyPackage;
    }
    /**
     * @var string
     */
    private $id;

    /**
     * @var \EspBundle\Entity\EspCustomerMaster
     */
    private $espCustomerNumber;


    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set espCustomerNumber
     *
     * @param \EspBundle\Entity\EspCustomerMaster $espCustomerNumber
     *
     * @return EspCustomerRate
     */
    public function setEspCustomerNumber(\EspBundle\Entity\EspCustomerMaster $espCustomerNumber = null)
    {
        $this->espCustomerNumber = $espCustomerNumber;

        return $this;
    }

    /**
     * Get espCustomerNumber
     *
     * @return \EspBundle\Entity\EspCustomerMaster
     */
    public function getEspCustomerNumber()
    {
        return $this->espCustomerNumber;
    }
    /**
     * @var integer
     */
    private $normalContract;

    /**
     * @var integer
     */
    private $semiPeakContract;

    /**
     * @var integer
     */
    private $saturdaySemiPeakContract;

    /**
     * @var integer
     */
    private $offPeakContract;


    /**
     * Set normalContract
     *
     * @param integer $normalContract
     *
     * @return EspCustomerRate
     */
    public function setNormalContract($normalContract)
    {
        $this->normalContract = $normalContract;

        return $this;
    }

    /**
     * Get normalContract
     *
     * @return integer
     */
    public function getNormalContract()
    {
        return $this->normalContract;
    }

    /**
     * Set semiPeakContract
     *
     * @param integer $semiPeakContract
     *
     * @return EspCustomerRate
     */
    public function setSemiPeakContract($semiPeakContract)
    {
        $this->semiPeakContract = $semiPeakContract;

        return $this;
    }

    /**
     * Get semiPeakContract
     *
     * @return integer
     */
    public function getSemiPeakContract()
    {
        return $this->semiPeakContract;
    }

    /**
     * Set saturdaySemiPeakContract
     *
     * @param integer $saturdaySemiPeakContract
     *
     * @return EspCustomerRate
     */
    public function setSaturdaySemiPeakContract($saturdaySemiPeakContract)
    {
        $this->saturdaySemiPeakContract = $saturdaySemiPeakContract;

        return $this;
    }

    /**
     * Get saturdaySemiPeakContract
     *
     * @return integer
     */
    public function getSaturdaySemiPeakContract()
    {
        return $this->saturdaySemiPeakContract;
    }

    /**
     * Set offPeakContract
     *
     * @param integer $offPeakContract
     *
     * @return EspCustomerRate
     */
    public function setOffPeakContract($offPeakContract)
    {
        $this->offPeakContract = $offPeakContract;

        return $this;
    }

    /**
     * Get offPeakContract
     *
     * @return integer
     */
    public function getOffPeakContract()
    {
        return $this->offPeakContract;
    }
}
