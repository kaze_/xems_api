<?php

namespace EspBundle\Entity;

/**
 * EspElectricityBill
 */
class EspElectricityBill
{
    /**
     * @var \DateTime
     */
    private $billingStartDate;

    /**
     * @var \DateTime
     */
    private $billingEndDate;

    /**
     * @var string
     */
    private $billingAddress;

    /**
     * @var integer
     */
    private $billingDays;

    /**
     * @var integer
     */
    private $touaContract;

    /**
     * @var integer
     */
    private $toubContract;

    /**
     * @var integer
     */
    private $toucContract;

    /**
     * @var integer
     */
    private $toudContract;

    /**
     * @var integer
     */
    private $touaUsage;

    /**
     * @var integer
     */
    private $toubUsage;

    /**
     * @var integer
     */
    private $toucUsage;

    /**
     * @var integer
     */
    private $toudUsage;

    /**
     * @var integer
     */
    private $touaDemand;

    /**
     * @var integer
     */
    private $toubDemand;

    /**
     * @var integer
     */
    private $toucDemand;

    /**
     * @var integer
     */
    private $toudDemand;

    /**
     * @var integer
     */
    private $powerFactor;

    /**
     * @var boolean
     */
    private $nonPringingBilling;

    /**
     * @var float
     */
    private $basicCharge;

    /**
     * @var float
     */
    private $energyCharge;

    /**
     * @var float
     */
    private $powerFactorDiscount;

    /**
     * @var \EspBundle\Entity\EspEnergyPackage
     */
    private $energyPackage;

    /**
     * @var \EspBundle\Entity\EspCustomerMaster
     */
    private $espCustomer;


    /**
     * Set billingStartDate
     *
     * @param \DateTime $billingStartDate
     *
     * @return EspElectricityBill
     */
    public function setBillingStartDate($billingStartDate)
    {
        $this->billingStartDate = $billingStartDate;

        return $this;
    }

    /**
     * Get billingStartDate
     *
     * @return \DateTime
     */
    public function getBillingStartDate()
    {
        return $this->billingStartDate;
    }

    /**
     * Set billingEndDate
     *
     * @param \DateTime $billingEndDate
     *
     * @return EspElectricityBill
     */
    public function setBillingEndDate($billingEndDate)
    {
        $this->billingEndDate = $billingEndDate;

        return $this;
    }

    /**
     * Get billingEndDate
     *
     * @return \DateTime
     */
    public function getBillingEndDate()
    {
        return $this->billingEndDate;
    }

    /**
     * Set billingAddress
     *
     * @param string $billingAddress
     *
     * @return EspElectricityBill
     */
    public function setBillingAddress($billingAddress)
    {
        $this->billingAddress = $billingAddress;

        return $this;
    }

    /**
     * Get billingAddress
     *
     * @return string
     */
    public function getBillingAddress()
    {
        return $this->billingAddress;
    }

    /**
     * Set billingDays
     *
     * @param integer $billingDays
     *
     * @return EspElectricityBill
     */
    public function setBillingDays($billingDays)
    {
        $this->billingDays = $billingDays;

        return $this;
    }

    /**
     * Get billingDays
     *
     * @return integer
     */
    public function getBillingDays()
    {
        return $this->billingDays;
    }

    /**
     * Set touaContract
     *
     * @param integer $touaContract
     *
     * @return EspElectricityBill
     */
    public function setTouaContract($touaContract)
    {
        $this->touaContract = $touaContract;

        return $this;
    }

    /**
     * Get touaContract
     *
     * @return integer
     */
    public function getTouaContract()
    {
        return $this->touaContract;
    }

    /**
     * Set toubContract
     *
     * @param integer $toubContract
     *
     * @return EspElectricityBill
     */
    public function setToubContract($toubContract)
    {
        $this->toubContract = $toubContract;

        return $this;
    }

    /**
     * Get toubContract
     *
     * @return integer
     */
    public function getToubContract()
    {
        return $this->toubContract;
    }

    /**
     * Set toucContract
     *
     * @param integer $toucContract
     *
     * @return EspElectricityBill
     */
    public function setToucContract($toucContract)
    {
        $this->toucContract = $toucContract;

        return $this;
    }

    /**
     * Get toucContract
     *
     * @return integer
     */
    public function getToucContract()
    {
        return $this->toucContract;
    }

    /**
     * Set toudContract
     *
     * @param integer $toudContract
     *
     * @return EspElectricityBill
     */
    public function setToudContract($toudContract)
    {
        $this->toudContract = $toudContract;

        return $this;
    }

    /**
     * Get toudContract
     *
     * @return integer
     */
    public function getToudContract()
    {
        return $this->toudContract;
    }

    /**
     * Set touaUsage
     *
     * @param integer $touaUsage
     *
     * @return EspElectricityBill
     */
    public function setTouaUsage($touaUsage)
    {
        $this->touaUsage = $touaUsage;

        return $this;
    }

    /**
     * Get touaUsage
     *
     * @return integer
     */
    public function getTouaUsage()
    {
        return $this->touaUsage;
    }

    /**
     * Set toubUsage
     *
     * @param integer $toubUsage
     *
     * @return EspElectricityBill
     */
    public function setToubUsage($toubUsage)
    {
        $this->toubUsage = $toubUsage;

        return $this;
    }

    /**
     * Get toubUsage
     *
     * @return integer
     */
    public function getToubUsage()
    {
        return $this->toubUsage;
    }

    /**
     * Set toucUsage
     *
     * @param integer $toucUsage
     *
     * @return EspElectricityBill
     */
    public function setToucUsage($toucUsage)
    {
        $this->toucUsage = $toucUsage;

        return $this;
    }

    /**
     * Get toucUsage
     *
     * @return integer
     */
    public function getToucUsage()
    {
        return $this->toucUsage;
    }

    /**
     * Set toudUsage
     *
     * @param integer $toudUsage
     *
     * @return EspElectricityBill
     */
    public function setToudUsage($toudUsage)
    {
        $this->toudUsage = $toudUsage;

        return $this;
    }

    /**
     * Get toudUsage
     *
     * @return integer
     */
    public function getToudUsage()
    {
        return $this->toudUsage;
    }

    /**
     * Set touaDemand
     *
     * @param integer $touaDemand
     *
     * @return EspElectricityBill
     */
    public function setTouaDemand($touaDemand)
    {
        $this->touaDemand = $touaDemand;

        return $this;
    }

    /**
     * Get touaDemand
     *
     * @return integer
     */
    public function getTouaDemand()
    {
        return $this->touaDemand;
    }

    /**
     * Set toubDemand
     *
     * @param integer $toubDemand
     *
     * @return EspElectricityBill
     */
    public function setToubDemand($toubDemand)
    {
        $this->toubDemand = $toubDemand;

        return $this;
    }

    /**
     * Get toubDemand
     *
     * @return integer
     */
    public function getToubDemand()
    {
        return $this->toubDemand;
    }

    /**
     * Set toucDemand
     *
     * @param integer $toucDemand
     *
     * @return EspElectricityBill
     */
    public function setToucDemand($toucDemand)
    {
        $this->toucDemand = $toucDemand;

        return $this;
    }

    /**
     * Get toucDemand
     *
     * @return integer
     */
    public function getToucDemand()
    {
        return $this->toucDemand;
    }

    /**
     * Set toudDemand
     *
     * @param integer $toudDemand
     *
     * @return EspElectricityBill
     */
    public function setToudDemand($toudDemand)
    {
        $this->toudDemand = $toudDemand;

        return $this;
    }

    /**
     * Get toudDemand
     *
     * @return integer
     */
    public function getToudDemand()
    {
        return $this->toudDemand;
    }

    /**
     * Set powerFactor
     *
     * @param integer $powerFactor
     *
     * @return EspElectricityBill
     */
    public function setPowerFactor($powerFactor)
    {
        $this->powerFactor = $powerFactor;

        return $this;
    }

    /**
     * Get powerFactor
     *
     * @return integer
     */
    public function getPowerFactor()
    {
        return $this->powerFactor;
    }

    /**
     * Set nonPringingBilling
     *
     * @param boolean $nonPringingBilling
     *
     * @return EspElectricityBill
     */
    public function setNonPringingBilling($nonPringingBilling)
    {
        $this->nonPringingBilling = $nonPringingBilling;

        return $this;
    }

    /**
     * Get nonPringingBilling
     *
     * @return boolean
     */
    public function getNonPringingBilling()
    {
        return $this->nonPringingBilling;
    }

    /**
     * Set basicCharge
     *
     * @param float $basicCharge
     *
     * @return EspElectricityBill
     */
    public function setBasicCharge($basicCharge)
    {
        $this->basicCharge = $basicCharge;

        return $this;
    }

    /**
     * Get basicCharge
     *
     * @return float
     */
    public function getBasicCharge()
    {
        return $this->basicCharge;
    }

    /**
     * Set energyCharge
     *
     * @param float $energyCharge
     *
     * @return EspElectricityBill
     */
    public function setEnergyCharge($energyCharge)
    {
        $this->energyCharge = $energyCharge;

        return $this;
    }

    /**
     * Get energyCharge
     *
     * @return float
     */
    public function getEnergyCharge()
    {
        return $this->energyCharge;
    }

    /**
     * Set powerFactorDiscount
     *
     * @param float $powerFactorDiscount
     *
     * @return EspElectricityBill
     */
    public function setPowerFactorDiscount($powerFactorDiscount)
    {
        $this->powerFactorDiscount = $powerFactorDiscount;

        return $this;
    }

    /**
     * Get powerFactorDiscount
     *
     * @return float
     */
    public function getPowerFactorDiscount()
    {
        return $this->powerFactorDiscount;
    }

    /**
     * Set energyPackage
     *
     * @param \EspBundle\Entity\EspEnergyPackage $energyPackage
     *
     * @return EspElectricityBill
     */
    public function setEnergyPackage(\EspBundle\Entity\EspEnergyPackage $energyPackage = null)
    {
        $this->energyPackage = $energyPackage;

        return $this;
    }

    /**
     * Get energyPackage
     *
     * @return \EspBundle\Entity\EspEnergyPackage
     */
    public function getEnergyPackage()
    {
        return $this->energyPackage;
    }

    /**
     * Set espCustomer
     *
     * @param \EspBundle\Entity\EspCustomerMaster $espCustomer
     *
     * @return EspElectricityBill
     */
    public function setEspCustomer(\EspBundle\Entity\EspCustomerMaster $espCustomer = null)
    {
        $this->espCustomer = $espCustomer;

        return $this;
    }

    /**
     * Get espCustomer
     *
     * @return \EspBundle\Entity\EspCustomerMaster
     */
    public function getEspCustomer()
    {
        return $this->espCustomer;
    }
}

