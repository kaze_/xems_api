<?php

namespace EspBundle\Entity;

/**
 * EspElectricityFeeDaily
 */
class EspElectricityFeeDaily
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var int
     */
    private $energyPackageId;

    /**
     * @var float|null
     */
    private $normalUsage = '0';

    /**
     * @var float|null
     */
    private $semiPeakUsage = '0';

    /**
     * @var float|null
     */
    private $saturdaySemiPeakUsage = '0';

    /**
     * @var float|null
     */
    private $offPeakUsage = '0';

    /**
     * @var float|null
     */
    private $normalFee = '0';

    /**
     * @var float|null
     */
    private $semiPeakFee = '0';

    /**
     * @var float|null
     */
    private $saturdaySemiPeakFee = '0';

    /**
     * @var float|null
     */
    private $offPeakFee = '0';

    /**
     * @var float
     */
    private $basicFee = '0';

    /**
     * @var float
     */
    private $energyFee = '0';

    /**
     * @var float
     */
    private $totalUsage = '0';

    /**
     * @var float
     */
    private $totalFee = '0';

    /**
     * @var \EspBundle\Entity\EspCustomerMaster
     */
    private $espCustomer;

    /**
     * @var \AppBundle\Entity\EmsCircuitMaster
     */
    private $circuit;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date.
     *
     * @param \DateTime $date
     *
     * @return EspElectricityFeeDaily
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date.
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set energyPackageId.
     *
     * @param int $energyPackageId
     *
     * @return EspElectricityFeeDaily
     */
    public function setEnergyPackageId($energyPackageId)
    {
        $this->energyPackageId = $energyPackageId;

        return $this;
    }

    /**
     * Get energyPackageId.
     *
     * @return int
     */
    public function getEnergyPackageId()
    {
        return $this->energyPackageId;
    }

    /**
     * Set normalUsage.
     *
     * @param float|null $normalUsage
     *
     * @return EspElectricityFeeDaily
     */
    public function setNormalUsage($normalUsage = null)
    {
        $this->normalUsage = $normalUsage;

        return $this;
    }

    /**
     * Get normalUsage.
     *
     * @return float|null
     */
    public function getNormalUsage()
    {
        return $this->normalUsage;
    }

    /**
     * Set semiPeakUsage.
     *
     * @param float|null $semiPeakUsage
     *
     * @return EspElectricityFeeDaily
     */
    public function setSemiPeakUsage($semiPeakUsage = null)
    {
        $this->semiPeakUsage = $semiPeakUsage;

        return $this;
    }

    /**
     * Get semiPeakUsage.
     *
     * @return float|null
     */
    public function getSemiPeakUsage()
    {
        return $this->semiPeakUsage;
    }

    /**
     * Set saturdaySemiPeakUsage.
     *
     * @param float|null $saturdaySemiPeakUsage
     *
     * @return EspElectricityFeeDaily
     */
    public function setSaturdaySemiPeakUsage($saturdaySemiPeakUsage = null)
    {
        $this->saturdaySemiPeakUsage = $saturdaySemiPeakUsage;

        return $this;
    }

    /**
     * Get saturdaySemiPeakUsage.
     *
     * @return float|null
     */
    public function getSaturdaySemiPeakUsage()
    {
        return $this->saturdaySemiPeakUsage;
    }

    /**
     * Set offPeakUsage.
     *
     * @param float|null $offPeakUsage
     *
     * @return EspElectricityFeeDaily
     */
    public function setOffPeakUsage($offPeakUsage = null)
    {
        $this->offPeakUsage = $offPeakUsage;

        return $this;
    }

    /**
     * Get offPeakUsage.
     *
     * @return float|null
     */
    public function getOffPeakUsage()
    {
        return $this->offPeakUsage;
    }

    /**
     * Set normalFee.
     *
     * @param float|null $normalFee
     *
     * @return EspElectricityFeeDaily
     */
    public function setNormalFee($normalFee = null)
    {
        $this->normalFee = $normalFee;

        return $this;
    }

    /**
     * Get normalFee.
     *
     * @return float|null
     */
    public function getNormalFee()
    {
        return $this->normalFee;
    }

    /**
     * Set semiPeakFee.
     *
     * @param float|null $semiPeakFee
     *
     * @return EspElectricityFeeDaily
     */
    public function setSemiPeakFee($semiPeakFee = null)
    {
        $this->semiPeakFee = $semiPeakFee;

        return $this;
    }

    /**
     * Get semiPeakFee.
     *
     * @return float|null
     */
    public function getSemiPeakFee()
    {
        return $this->semiPeakFee;
    }

    /**
     * Set saturdaySemiPeakFee.
     *
     * @param float|null $saturdaySemiPeakFee
     *
     * @return EspElectricityFeeDaily
     */
    public function setSaturdaySemiPeakFee($saturdaySemiPeakFee = null)
    {
        $this->saturdaySemiPeakFee = $saturdaySemiPeakFee;

        return $this;
    }

    /**
     * Get saturdaySemiPeakFee.
     *
     * @return float|null
     */
    public function getSaturdaySemiPeakFee()
    {
        return $this->saturdaySemiPeakFee;
    }

    /**
     * Set offPeakFee.
     *
     * @param float|null $offPeakFee
     *
     * @return EspElectricityFeeDaily
     */
    public function setOffPeakFee($offPeakFee = null)
    {
        $this->offPeakFee = $offPeakFee;

        return $this;
    }

    /**
     * Get offPeakFee.
     *
     * @return float|null
     */
    public function getOffPeakFee()
    {
        return $this->offPeakFee;
    }

    /**
     * Set basicFee.
     *
     * @param float $basicFee
     *
     * @return EspElectricityFeeDaily
     */
    public function setBasicFee($basicFee)
    {
        $this->basicFee = $basicFee;

        return $this;
    }

    /**
     * Get basicFee.
     *
     * @return float
     */
    public function getBasicFee()
    {
        return $this->basicFee;
    }

    /**
     * Set energyFee.
     *
     * @param float $energyFee
     *
     * @return EspElectricityFeeDaily
     */
    public function setEnergyFee($energyFee)
    {
        $this->energyFee = $energyFee;

        return $this;
    }

    /**
     * Get energyFee.
     *
     * @return float
     */
    public function getEnergyFee()
    {
        return $this->energyFee;
    }

    /**
     * Set totalUsage.
     *
     * @param float $totalUsage
     *
     * @return EspElectricityFeeDaily
     */
    public function setTotalUsage($totalUsage)
    {
        $this->totalUsage = $totalUsage;

        return $this;
    }

    /**
     * Get totalUsage.
     *
     * @return float
     */
    public function getTotalUsage()
    {
        return $this->totalUsage;
    }

    /**
     * Set totalFee.
     *
     * @param float $totalFee
     *
     * @return EspElectricityFeeDaily
     */
    public function setTotalFee($totalFee)
    {
        $this->totalFee = $totalFee;

        return $this;
    }

    /**
     * Get totalFee.
     *
     * @return float
     */
    public function getTotalFee()
    {
        return $this->totalFee;
    }

    /**
     * Set espCustomer.
     *
     * @param \EspBundle\Entity\EspCustomerMaster|null $espCustomer
     *
     * @return EspElectricityFeeDaily
     */
    public function setEspCustomer(\EspBundle\Entity\EspCustomerMaster $espCustomer = null)
    {
        $this->espCustomer = $espCustomer;

        return $this;
    }

    /**
     * Get espCustomer.
     *
     * @return \EspBundle\Entity\EspCustomerMaster|null
     */
    public function getEspCustomer()
    {
        return $this->espCustomer;
    }

    /**
     * Set circuit.
     *
     * @param \AppBundle\Entity\EmsCircuitMaster|null $circuit
     *
     * @return EspElectricityFeeDaily
     */
    public function setCircuit(\AppBundle\Entity\EmsCircuitMaster $circuit = null)
    {
        $this->circuit = $circuit;

        return $this;
    }

    /**
     * Get circuit.
     *
     * @return \AppBundle\Entity\EmsCircuitMaster|null
     */
    public function getCircuit()
    {
        return $this->circuit;
    }
}
