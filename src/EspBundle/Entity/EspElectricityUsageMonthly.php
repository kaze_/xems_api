<?php

namespace EspBundle\Entity;

/**
 * EspElectricityUsageMonthly
 */
class EspElectricityUsageMonthly
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var int|null
     */
    private $updateTime;

    /**
     * @var int
     */
    private $energyPackageId;

    /**
     * @var float
     */
    private $normalUsage = '0';

    /**
     * @var float
     */
    private $semiPeakUsage = '0';

    /**
     * @var float
     */
    private $saturdaySemiPeakUsage = '0';

    /**
     * @var float
     */
    private $offPeakUsage = '0';

    /**
     * @var float
     */
    private $normalDemand = '0';

    /**
     * @var float
     */
    private $semiPeakDemand = '0';

    /**
     * @var float
     */
    private $saturdaySemiPeakDemand = '0';

    /**
     * @var float
     */
    private $offPeakDemand = '0';

    /**
     * @var float
     */
    private $normalFee = '0';

    /**
     * @var float
     */
    private $semiPeakFee = '0';

    /**
     * @var float
     */
    private $saturdaySemiPeakFee = '0';

    /**
     * @var float
     */
    private $offPeakFee = '0';

    /**
     * @var float
     */
    private $totalUsage = '0';

    /**
     * @var float
     */
    private $basicFee = '0';

    /**
     * @var float
     */
    private $energyFee = '0';

    /**
     * @var float
     */
    private $totalFee = '0';

    /**
     * @var \EspBundle\Entity\EspCustomerMaster
     */
    private $espCustomer;

    /**
     * @var \AppBundle\Entity\EmsCircuitMaster
     */
    private $circuit;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date.
     *
     * @param \DateTime $date
     *
     * @return EspElectricityUsageMonthly
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date.
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set updateTime.
     *
     * @param \DateTime|null $updateTime
     *
     * @return EspElectricityUsageMonthly
     */
    public function setUpdateTime($updateTime = null)
    {
        $this->updateTime = $updateTime;

        return $this;
    }

    /**
     * Get updateTime.
     *
     * @return int|null
     */
    public function getUpdateTime()
    {
        return $this->updateTime;
    }

    /**
     * Set energyPackageId.
     *
     * @param int $energyPackageId
     *
     * @return EspElectricityUsageMonthly
     */
    public function setEnergyPackageId($energyPackageId)
    {
        $this->energyPackageId = $energyPackageId;

        return $this;
    }

    /**
     * Get energyPackageId.
     *
     * @return int
     */
    public function getEnergyPackageId()
    {
        return $this->energyPackageId;
    }

    /**
     * Set normalUsage.
     *
     * @param float $normalUsage
     *
     * @return EspElectricityUsageMonthly
     */
    public function setNormalUsage($normalUsage)
    {
        $this->normalUsage = $normalUsage;

        return $this;
    }

    /**
     * Get normalUsage.
     *
     * @return float
     */
    public function getNormalUsage()
    {
        return $this->normalUsage;
    }

    /**
     * Set semiPeakUsage.
     *
     * @param float $semiPeakUsage
     *
     * @return EspElectricityUsageMonthly
     */
    public function setSemiPeakUsage($semiPeakUsage)
    {
        $this->semiPeakUsage = $semiPeakUsage;

        return $this;
    }

    /**
     * Get semiPeakUsage.
     *
     * @return float
     */
    public function getSemiPeakUsage()
    {
        return $this->semiPeakUsage;
    }

    /**
     * Set saturdaySemiPeakUsage.
     *
     * @param float $saturdaySemiPeakUsage
     *
     * @return EspElectricityUsageMonthly
     */
    public function setSaturdaySemiPeakUsage($saturdaySemiPeakUsage)
    {
        $this->saturdaySemiPeakUsage = $saturdaySemiPeakUsage;

        return $this;
    }

    /**
     * Get saturdaySemiPeakUsage.
     *
     * @return float
     */
    public function getSaturdaySemiPeakUsage()
    {
        return $this->saturdaySemiPeakUsage;
    }

    /**
     * Set offPeakUsage.
     *
     * @param float $offPeakUsage
     *
     * @return EspElectricityUsageMonthly
     */
    public function setOffPeakUsage($offPeakUsage)
    {
        $this->offPeakUsage = $offPeakUsage;

        return $this;
    }

    /**
     * Get offPeakUsage.
     *
     * @return float
     */
    public function getOffPeakUsage()
    {
        return $this->offPeakUsage;
    }

    /**
     * Set normalDemand.
     *
     * @param float $normalDemand
     *
     * @return EspElectricityUsageMonthly
     */
    public function setNormalDemand($normalDemand)
    {
        $this->normalDemand = $normalDemand;

        return $this;
    }

    /**
     * Get normalDemand.
     *
     * @return float
     */
    public function getNormalDemand()
    {
        return $this->normalDemand;
    }

    /**
     * Set semiPeakDemand.
     *
     * @param float $semiPeakDemand
     *
     * @return EspElectricityUsageMonthly
     */
    public function setSemiPeakDemand($semiPeakDemand)
    {
        $this->semiPeakDemand = $semiPeakDemand;

        return $this;
    }

    /**
     * Get semiPeakDemand.
     *
     * @return float
     */
    public function getSemiPeakDemand()
    {
        return $this->semiPeakDemand;
    }

    /**
     * Set saturdaySemiPeakDemand.
     *
     * @param float $saturdaySemiPeakDemand
     *
     * @return EspElectricityUsageMonthly
     */
    public function setSaturdaySemiPeakDemand($saturdaySemiPeakDemand)
    {
        $this->saturdaySemiPeakDemand = $saturdaySemiPeakDemand;

        return $this;
    }

    /**
     * Get saturdaySemiPeakDemand.
     *
     * @return float
     */
    public function getSaturdaySemiPeakDemand()
    {
        return $this->saturdaySemiPeakDemand;
    }

    /**
     * Set offPeakDemand.
     *
     * @param float $offPeakDemand
     *
     * @return EspElectricityUsageMonthly
     */
    public function setOffPeakDemand($offPeakDemand)
    {
        $this->offPeakDemand = $offPeakDemand;

        return $this;
    }

    /**
     * Get offPeakDemand.
     *
     * @return float
     */
    public function getOffPeakDemand()
    {
        return $this->offPeakDemand;
    }

    /**
     * Set normalFee.
     *
     * @param float $normalFee
     *
     * @return EspElectricityUsageMonthly
     */
    public function setNormalFee($normalFee)
    {
        $this->normalFee = $normalFee;

        return $this;
    }

    /**
     * Get normalFee.
     *
     * @return float
     */
    public function getNormalFee()
    {
        return $this->normalFee;
    }

    /**
     * Set semiPeakFee.
     *
     * @param float $semiPeakFee
     *
     * @return EspElectricityUsageMonthly
     */
    public function setSemiPeakFee($semiPeakFee)
    {
        $this->semiPeakFee = $semiPeakFee;

        return $this;
    }

    /**
     * Get semiPeakFee.
     *
     * @return float
     */
    public function getSemiPeakFee()
    {
        return $this->semiPeakFee;
    }

    /**
     * Set saturdaySemiPeakFee.
     *
     * @param float $saturdaySemiPeakFee
     *
     * @return EspElectricityUsageMonthly
     */
    public function setSaturdaySemiPeakFee($saturdaySemiPeakFee)
    {
        $this->saturdaySemiPeakFee = $saturdaySemiPeakFee;

        return $this;
    }

    /**
     * Get saturdaySemiPeakFee.
     *
     * @return float
     */
    public function getSaturdaySemiPeakFee()
    {
        return $this->saturdaySemiPeakFee;
    }

    /**
     * Set offPeakFee.
     *
     * @param float $offPeakFee
     *
     * @return EspElectricityUsageMonthly
     */
    public function setOffPeakFee($offPeakFee)
    {
        $this->offPeakFee = $offPeakFee;

        return $this;
    }

    /**
     * Get offPeakFee.
     *
     * @return float
     */
    public function getOffPeakFee()
    {
        return $this->offPeakFee;
    }

    /**
     * Set totalUsage.
     *
     * @param float $totalUsage
     *
     * @return EspElectricityUsageMonthly
     */
    public function setTotalUsage($totalUsage)
    {
        $this->totalUsage = $totalUsage;

        return $this;
    }

    /**
     * Get totalUsage.
     *
     * @return float
     */
    public function getTotalUsage()
    {
        return $this->totalUsage;
    }

    /**
     * Set basicFee.
     *
     * @param float $basicFee
     *
     * @return EspElectricityUsageMonthly
     */
    public function setBasicFee($basicFee)
    {
        $this->basicFee = $basicFee;

        return $this;
    }

    /**
     * Get basicFee.
     *
     * @return float
     */
    public function getBasicFee()
    {
        return $this->basicFee;
    }

    /**
     * Set energyFee.
     *
     * @param float $energyFee
     *
     * @return EspElectricityUsageMonthly
     */
    public function setEnergyFee($energyFee)
    {
        $this->energyFee = $energyFee;

        return $this;
    }

    /**
     * Get energyFee.
     *
     * @return float
     */
    public function getEnergyFee()
    {
        return $this->energyFee;
    }

    /**
     * Set totalFee.
     *
     * @param float $totalFee
     *
     * @return EspElectricityUsageMonthly
     */
    public function setTotalFee($totalFee)
    {
        $this->totalFee = $totalFee;

        return $this;
    }

    /**
     * Get totalFee.
     *
     * @return float
     */
    public function getTotalFee()
    {
        return $this->totalFee;
    }

    /**
     * Set espCustomer.
     *
     * @param \EspBundle\Entity\EspCustomerMaster|null $espCustomer
     *
     * @return EspElectricityUsageMonthly
     */
    public function setEspCustomer(\EspBundle\Entity\EspCustomerMaster $espCustomer = null)
    {
        $this->espCustomer = $espCustomer;

        return $this;
    }

    /**
     * Get espCustomer.
     *
     * @return \EspBundle\Entity\EspCustomerMaster|null
     */
    public function getEspCustomer()
    {
        return $this->espCustomer;
    }

    /**
     * Set circuit.
     *
     * @param \AppBundle\Entity\EmsCircuitMaster|null $circuit
     *
     * @return EspElectricityUsageMonthly
     */
    public function setCircuit(\AppBundle\Entity\EmsCircuitMaster $circuit = null)
    {
        $this->circuit = $circuit;

        return $this;
    }

    /**
     * Get circuit.
     *
     * @return \AppBundle\Entity\EmsCircuitMaster|null
     */
    public function getCircuit()
    {
        return $this->circuit;
    }
    
    public function doUpdate()
    {
        $this->updateTime = new \Datetime();   
    }
}
