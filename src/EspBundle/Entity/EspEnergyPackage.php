<?php

namespace EspBundle\Entity;

/**
 * EspEnergyPackage
 */
class EspEnergyPackage
{
    /**
     * @var integer
     */
    private $energyPackageId;

    /**
     * @var string
     */
    private $energyPackageName;

    /**
     * @var \EspBundle\Entity\EspEnergySp
     */
    private $energySp;


    /**
     * Get energyPackageId
     *
     * @return integer
     */
    public function getEnergyPackageId()
    {
        return $this->energyPackageId;
    }

    /**
     * Set energyPackageName
     *
     * @param string $energyPackageName
     *
     * @return EspEnergyPackage
     */
    public function setEnergyPackageName($energyPackageName)
    {
        $this->energyPackageName = $energyPackageName;

        return $this;
    }

    /**
     * Get energyPackageName
     *
     * @return string
     */
    public function getEnergyPackageName()
    {
        return $this->energyPackageName;
    }

    /**
     * Set energySp
     *
     * @param \EspBundle\Entity\EspEnergySp $energySp
     *
     * @return EspEnergyPackage
     */
    public function setEnergySp(\EspBundle\Entity\EspEnergySp $energySp = null)
    {
        $this->energySp = $energySp;

        return $this;
    }

    /**
     * Get energySp
     *
     * @return \EspBundle\Entity\EspEnergySp
     */
    public function getEnergySp()
    {
        return $this->energySp;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $energyPackageDetail;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->energyPackageDetail = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add energyPackageDetail
     *
     * @param \EspBundle\Entity\EspEnergyPackageDetail $energyPackageDetail
     *
     * @return EspEnergyPackage
     */
    public function addEnergyPackageDetail(\EspBundle\Entity\EspEnergyPackageDetail $energyPackageDetail)
    {
        $this->energyPackageDetail[] = $energyPackageDetail;

        return $this;
    }

    /**
     * Remove energyPackageDetail
     *
     * @param \EspBundle\Entity\EspEnergyPackageDetail $energyPackageDetail
     */
    public function removeEnergyPackageDetail(\EspBundle\Entity\EspEnergyPackageDetail $energyPackageDetail)
    {
        $this->energyPackageDetail->removeElement($energyPackageDetail);
    }

    /**
     * Get energyPackageDetail
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEnergyPackageDetail()
    {
        return $this->energyPackageDetail;
    }

    public function getCurrentEnergyPackageDetail()
    {
        if ( $this->energyPackageDetail->isEmpty() ) {
            return null;
        }
        foreach($this->energyPackageDetail as $detail ) {
            if ( $detail->getStatusId() == 1) {
                return $detail;
            }
        }
        return null;
    }
}
