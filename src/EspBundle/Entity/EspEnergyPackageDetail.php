<?php

namespace EspBundle\Entity;

/**
 * EspEnergyPackageDetail
 */
class EspEnergyPackageDetail
{
    /**
     * @var integer
     */
    private $startDate;

    /**
     * @var integer
     */
    private $energyPackageName;

    /**
     * @var integer
     */
    private $rateDetail;

    /**
     * @var \EspBundle\Entity\EspEnergyPackage
     */
    private $energyPackage;


    /**
     * Set startDate
     *
     * @param integer $startDate
     *
     * @return EspEnergyPackageDetail
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return integer
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set energyPackageName
     *
     * @param integer $energyPackageName
     *
     * @return EspEnergyPackageDetail
     */
    public function setEnergyPackageName($energyPackageName)
    {
        $this->energyPackageName = $energyPackageName;

        return $this;
    }

    /**
     * Get energyPackageName
     *
     * @return integer
     */
    public function getEnergyPackageName()
    {
        return $this->energyPackageName;
    }

    /**
     * Set rateDetail
     *
     * @param integer $rateDetail
     *
     * @return EspEnergyPackageDetail
     */
    public function setRateDetail($rateDetail)
    {
        $this->rateDetail = $rateDetail;

        return $this;
    }

    /**
     * Get rateDetail
     *
     * @return integer
     */
    public function getRateDetail()
    {
        return $this->rateDetail;
    }

    /**
     * Set energyPackage
     *
     * @param \EspBundle\Entity\EspEnergyPackage $energyPackage
     *
     * @return EspEnergyPackageDetail
     */
    public function setEnergyPackage(\EspBundle\Entity\EspEnergyPackage $energyPackage = null)
    {
        $this->energyPackage = $energyPackage;

        return $this;
    }

    /**
     * Get energyPackage
     *
     * @return \EspBundle\Entity\EspEnergyPackage
     */
    public function getEnergyPackage()
    {
        return $this->energyPackage;
    }
    /**
     * @var integer
     */
    private $hasContract = '0';

    /**
     * @var integer
     */
    private $hasTou = '0';


    /**
     * Set hasContract
     *
     * @param integer $hasContract
     *
     * @return EspEnergyPackageDetail
     */
    public function setHasContract($hasContract)
    {
        $this->hasContract = $hasContract;

        return $this;
    }

    /**
     * Get hasContract
     *
     * @return integer
     */
    public function getHasContract()
    {
        return $this->hasContract;
    }

    /**
     * Set hasTou
     *
     * @param integer $hasTou
     *
     * @return EspEnergyPackageDetail
     */
    public function setHasTou($hasTou)
    {
        $this->hasTou = $hasTou;

        return $this;
    }

    /**
     * Get hasTou
     *
     * @return integer
     */
    public function getHasTou()
    {
        return $this->hasTou;
    }
    
    /**
     * @var integer
     */
    private $statusId = '0';


    /**
     * Set statusId
     *
     * @param integer $statusId
     *
     * @return EspEnergyPackageDetail
     */
    public function setStatusId($statusId)
    {
        $this->statusId = $statusId;

        return $this;
    }

    /**
     * Get statusId
     *
     * @return integer
     */
    public function getStatusId()
    {
        return $this->statusId;
    }
    /**
     * @var int
     */
    private $id;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}
