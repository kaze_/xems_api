<?php

namespace EspBundle\Entity;

/**
 * EspEnergySp
 */
class EspEnergySp
{
    /**
     * @var integer
     */
    private $energySpId;

    /**
     * @var string
     */
    private $energySpName;

    /**
     * @var \AppBundle\Entity\SysCountryList
     */
    private $country;

    /**
     * @var \EspBundle\Entity\EspServiceType
     */
    private $energySpType;


    /**
     * Get energySpId
     *
     * @return integer
     */
    public function getEnergySpId()
    {
        return $this->energySpId;
    }

    /**
     * Set energySpName
     *
     * @param string $energySpName
     *
     * @return EspEnergySp
     */
    public function setEnergySpName($energySpName)
    {
        $this->energySpName = $energySpName;

        return $this;
    }

    /**
     * Get energySpName
     *
     * @return string
     */
    public function getEnergySpName()
    {
        return $this->energySpName;
    }

    /**
     * Set country
     *
     * @param \AppBundle\Entity\SysCountryList $country
     *
     * @return EspEnergySp
     */
    public function setCountry(\AppBundle\Entity\SysCountryList $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \AppBundle\Entity\SysCountryList
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set energySpType
     *
     * @param \EspBundle\Entity\EspServiceType $energySpType
     *
     * @return EspEnergySp
     */
    public function setEnergySpType(\EspBundle\Entity\EspServiceType $energySpType = null)
    {
        $this->energySpType = $energySpType;

        return $this;
    }

    /**
     * Get energySpType
     *
     * @return \EspBundle\Entity\EspServiceType
     */
    public function getEnergySpType()
    {
        return $this->energySpType;
    }
}
