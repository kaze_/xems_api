<?php

namespace EspBundle\Entity;

/**
 * EspServiceType
 */
class EspServiceType
{
    /**
     * @var integer
     */
    private $serviceId;

    /**
     * @var string
     */
    private $serviceName;


    /**
     * Get serviceId
     *
     * @return integer
     */
    public function getServiceId()
    {
        return $this->serviceId;
    }

    /**
     * Set serviceName
     *
     * @param string $serviceName
     *
     * @return EspServiceType
     */
    public function setServiceName($serviceName)
    {
        $this->serviceName = $serviceName;

        return $this;
    }

    /**
     * Get serviceName
     *
     * @return string
     */
    public function getServiceName()
    {
        return $this->serviceName;
    }
}
