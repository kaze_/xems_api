<?php

namespace EspBundle\Services\Billing\Core;
/*
 * xMight TPC Billing
 */
class TPC_Billing{
	const rate_table= "./tpc20180401.json";
	//
	protected static $TPLIST = ["normal","semi_peak","saturday_semi_peak","off_peak"];
	//
	protected $summer_day;
	protected $service_type;
	protected $service_rate;
	protected $cotp;
	protected $doop;
	//
	protected $demand_contract;
	protected $max_demand;
	protected $power_factor;
	protected $phase;
	protected $_energy;
	protected $_summer;
	
	function __construct($type, $rtable=null){
		$rate_table = is_null($rtable)? json_decode(file_get_contents(TPC_Billing::rate_table)) : $rtable;
		$this->service_type = $type;
		$this->service_rate = $rate_table->{"service_".$type};
		$this->summer_day = $rate_table->summer;
		$this->cotp = $rate_table->code_time_period;
		$this->doop = explode(',',$rate_table->day_offpeak);
	}
	//
	function pricing($datalog=null){
		if (is_null($datalog) && is_null($this->_energy)) return null;
		$a= [];
		switch ($this->service_type){
			case 1: 
			case 2:
				$a = $this->model_ntou_ml($datalog);
				break;
			case 3:
			case 4:
				$a = $this->model_tou_simple($datalog);
				break;
			case 5:
				$a = $this->model_tou_std($datalog);
				break;
			case 6:
				$a = $this->model_ntou_lt($datalog);
				break;
			case 7:
				$a = $this->model_tou_lt($datalog);
				break;
			case 8:
			case 10:
				$a = $this->model_tou_ht($datalog);
				break;
			case 9:
			case 11:
				$a = $this->model_tou_htB($datalog);
				break;
			default: break;
		}
		return $a;
	}
	//--- property setters
	function setDemandContract($contract){
		$this->demand_contract = $contract;
	}
	function setMaxDemand($md){
		$this->max_demand = $md;
	}
	function setPhase($phase){
		$this->phase = $phase;
	}
	function setPowerFactor($pf){
		$this->power_factor = $pf;
	}
	function setEnergy($e){
		$t = date_create_from_format("Y-m-d", $e["date"]);
		$this->isSummer($t);
		$this->_energy = $e["energy"];
	}
	function addOffPeakDays($doop){
		$this->doop = array_unique(array_merge($this->$doop,$doop));
	}

	function getOffPeakDays() {
		return $this->doop;
	}
	
	//--- pricing model functions
	protected function model_ntou_ml($datalog){
		if (is_null($datalog)){
			$e = $this->_energy["normal"];
			$s = $this->_summer? 0:1;
		} else{
			$c = $this->reduce_monthly_ntou($datalog);
			$kwh = $c[0][1][1] - $c[0][0][1];
			$s = $this->isSummer($c[0][1][0])? 0:1;
			$e = $kwh;
		}
		$amount = 0.0;
		$rate = $this->service_rate->rate;
		for ($i = count($rate); $i > 0; --$i){
			if ($e > $rate[$i-1][0]){
				$amount += $rate[$i-1][$s+1] *($e-$rate[$i-1][0]);
				$e = $rate[$i-1][0];
			}
		}
		return ["basic"=>0,"normal"=>$amount];
		// return ["normal"=>["basic"=>0,"amount"=>$amount]];
	}
	//
	protected function model_ntou_lt($datalog){
		if (empty($this->demand_contract)) return null;
		if (is_null($datalog)){
			$s = $this->_summer? 0:1;
			$amount_energy = $this->service_rate->energy_charge[$s] *$this->_energy["normal"];
		} else{
			$c = $this->reduce_monthly_ntou($datalog);
			$s = $this->isSummer($c[0][1][0])? 0:1;
			$amount_energy = $this->service_rate->energy_charge[$s]
				*($c[0][1][1]-$c[0][0][1]);
		}
		$demand_charge = $this->service_rate->demand_charge;
		//
		if (is_numeric($this->demand_contract)){
			// install capacity contract
			$amount_demand = $demand_charge->inst_cap * $this->demand_contract;
		} else{
			// max demand contract
			if ($this->isSummer()){
				$a = $demand_charge->max_demand->regular[0];
				$amount_demand = $a * $this->demand_contract['normal'];
			}else {
				$a = $demand_charge->max_demand->regular[1];
				$b = $demand_charge->max_demand->nonsummer;
				$amount_demand = $a * $this->demand_contract['normal']
					+ $b * $this->demand_contract['semi_peak'];
			}
		}
		return ["basic"=>$amount_demand, "normal"=>$amount_energy];
		//return ["normal"=>["basic"=>$amount_demand,"amount"=>$amount_energy]];
	}
	//
	protected function model_tou_lt($datalog){
		if (empty($this->demand_contract)) return null;
		$trset = $this->getTimeRateSet();
		$amount = [];
		if (is_null($datalog)) {
			$u = $this->calcEnergyAmount($this->_energy, $trset);
			foreach ($u as $k=>$v){
				$amount[$k] = $v;
			}
		} else{
			$c = $this->reduce_monthly_tou($datalog, $trset);
			$summer = $this->isSummer($c[0][0][0]);
			$u = $this->reduce_time_period($c);
			foreach ($u as $k=>$v){
				$amount[$this->cotp->{$k}] = $v[1];
			}
		}
		//
 		$demand_charge = $this->service_rate->demand_charge;
		if (is_numeric($this->demand_contract)){
			// install capacity contract
			$amount_demand = $demand_charge->inst_cap->customer 
				+ $demand_charge->inst_cap->capacity * $this->demand_contract;
		} else{
			// max demand contract
 			$amount_demand = $demand_charge->max_demand->customer;
			$a = $this->calcDemandAmount($demand_charge->max_demand, $this->isSummer());
			$amount_demand += $a[0];
			$amount_excees = $a[1];
		}
		$amount["basic"] = $amount_demand + (isset($amount_excees)? $amount_excees : 0.0);
		if (isset($amount_excees)) $amount["basic_excees"] = $amount_excees;
		return $amount;
	}
	//
	protected function model_tou_ht($datalog){
		if (empty($this->demand_contract)) return null;
		$trset = $this->getTimeRateSet();
		$amount = [];
		if (is_null($datalog)) {
			$u = $this->calcEnergyAmount($this->_energy, $trset);
			foreach ($u as $k=>$v){
				$amount[$k] = $v;
			}
		} else{
			$c = $this->reduce_monthly_tou($datalog, $trset);
			$summer = $this->isSummer($c[0][0][0]);
			$u = $this->reduce_time_period($c);
			foreach ($u as $k=>$v){
				$amount[$this->cotp->{$k}] = $v[1];
			}
		}
		$a = $this->calcDemandAmount($this->service_rate->demand_charge, $this->isSummer());
		$amount_demand = $a[0];
		$amount_excees = $a[1];
		$amount["basic"] = $amount_demand + (isset($amount_excees)? $amount_excees : 0.0);
		if (isset($amount_excees)) $amount["basic_excees"] = $amount_excees;
		return $amount;
	}
	//
	protected function model_tou_htB($datalog){
		if (empty($this->demand_contract)) return null;
		$trset = $this->getTimeRateSet();
		$amount = [];
		if (is_null($datalog)) {
			$u = $this->calcEnergyAmount($this->_energy, $trset);
			foreach ($u as $k=>$v){
				$amount[$k] = $v;
			}
		} else{
			$c = $this->reduce_monthly_tou($datalog, $trset);
			$summer = $this->isSummer($c[0][0][0]);
			$u = $this->reduce_time_period($c);
			foreach ($u as $k=>$v){
				$amount[$this->cotp->{$k}] = $v[1];
			}
		}
		$a = $this->calcDemandAmountB($this->service_rate->demand_charge, $this->isSummer());
		$amount_demand = $a[0];
		$amount_excees = $a[1];
		$amount["basic"] = $amount_demand + (isset($amount_excees)? $amount_excees : 0.0);
		if (isset($amount_excees)) $amount["basic_excees"] = $amount_excees;
		return $amount;
	}
	//
	protected function model_tou_std($datalog){
		if (empty($this->demand_contract)) return null;
		$amount = [];
		$trset = $this->getTimeRateSet();
		if (is_null($datalog)) {
			$u = $this->calcEnergyAmount($this->_energy, $trset);
			foreach ($u as $k=>$v){
				$amount[$k] = $v;
			}
		} else{
			$c = $this->reduce_monthly_tou($datalog, $trset);
			$summer = $this->isSummer($c[0][0][0]);
			$u = $this->reduce_time_period($c);
			foreach ($u as $k=>$v){
				$amount[$this->cotp->{$k}] = $v[1];
			}
		}
		$amount_demand = $this->service_rate->customer_charge->{$this->phase.'_phase'};
		$a = $this->calcDemandAmount($this->service_rate->demand_charge, $this->isSummer());
		$amount_demand += $a[0];
		$amount_excees = $a[1];
		$amount["basic"] = $amount_demand + (isset($amount_excees)? $amount_excees : 0.0);
		if (isset($amount_excees)) $amount["basic_excees"] = $amount_excees;
		return $amount;
	}
	//
	protected function model_tou_simple($datalog){
		$amount = ["basic"=>$this->service_rate->customer_charge];
		$trset = $this->getTimeRateSet();
		$e = 0;
		if (is_null($datalog)){
			$u = $this->calcEnergyAmount($this->_energy, $trset);
			foreach ($u as $k=>$v){
				$amount[$k] = $v;
				$e += $this->_energy[$k];
			}
		} else{
			$c = $this->reduce_monthly_tou($datalog, $trset);
			$summer = $this->isSummer($c[0][0][0]);
			$u = $this->reduce_time_period($c);
			foreach ($u as $k=>$v){
				$amount[$this->cotp->{$k}] = $v[1];
				$e += $v[0];
			}
		}
		//
		$e -= $this->service_rate->energy_charge->exceed[0];
		if ($e > 0)
			$amount["excees"] = $this->service_rate->energy_charge->exceed[1] * $e;
		return $amount;
	}
	
	//--- reducers
	protected function reduce_monthly_tou($datalog, $trset){
		return array_reduce($datalog, function($a,$d) use ($trset){
			$t = date_create_from_format("Y-m-d H:i", $d[0]);
			$tr = $this->getTimeRateInfo($t, $trset);
			$z = [$t,$d[1],$tr];
			if (empty($a)) array_push($a,[$z,[]]);
			$r = end($a);
			if ($tr[2] == $r[0][2][2] && $tr[0][1] == $r[0][2][0][1]){
				$r[1] = $z;
				$a[count($a)-1] = $r;
			} elseif ($tr[2] == $r[0][2][2] && $tr[0][1] != $r[0][2][0][1]){ //&& $tr[0][0]==$t->format("Hi")){
				$r[1] = $z;
				$a[count($a)-1] = $r;
				array_push($a, [$z,[]]);
			} else{
				array_push($a, [$z,[]]);
			}
			return $a;
		},[]);
	}
	protected function reduce_monthly_ntou($datalog){
		return array_reduce($datalog,function($a,$d){
			$t = date_create_from_format("Y-m-d H:i", $d[0]);
			$month = $t->format("Ym");
			$z = [$t,$d[1],$month];
			if (empty($a)) array_push($a, [$z,[]]);
			$r = end($a);
			if ($month == $r[0][2]){
				$r[1] = $z;
				$a[count($a)-1] = $r;
			} else{
				array_push($a, [$z,[]]);
			}
			return $a;
		},[]);
	}
	protected function reduce_time_period($c){
		return array_reduce($c, function($a,$d){
			$k = $d[0][2][0][1];
			if (empty($d[1])) return $a;
			if (!array_key_exists($k, $a)){
				$e = $d[1][1] - $d[0][1];
				$a[$k] = [$e, $d[0][2][0][2] * $e];
			} else{
				$e = $d[1][1] - $d[0][1];
				$a[$k][0] += $e;
				$a[$k][1] += $d[0][2][0][2] * $e;
			}
			return $a;
		},[]);
	}
	
	//--- demand amount calculation
	protected function calcDemandAmount($demand_charge, $summer){
		$amount_demand = 0;
		$a = $demand_charge->regular[$summer? 0:1];
		$b = $demand_charge->ssp[$summer? 0:1];
		$c = $b * (($this->demand_contract['saturday_semi_peak'] + $this->demand_contract['off_peak'])
				-0.5 * ($this->demand_contract['normal']+$this->demand_contract['semi_peak']));
		if ($c < 0) $c = 0;
		if ($summer){
			$amount_demand += $a * $this->demand_contract['normal'] + $c;
		}else {
			$amount_demand += $a * ($this->demand_contract['normal']+$this->demand_contract['semi_peak']) + $c;
		}
		// calc excees charge
		$cap = [
			$summer? $this->demand_contract['normal']:$this->demand_contract['normal']+$this->demand_contract['semi_peak'],
			$this->demand_contract['normal']+$this->demand_contract['semi_peak']+$this->demand_contract['saturday_semi_peak']
		];
		array_push($cap, $cap[1]+$this->demand_contract['off_peak']);
		$max = [$this->max_demand['normal'],$this->max_demand['saturday_semi_peak'],$this->max_demand['off_peak']];
		$f = [];
		for ($i = 0; $i < 3; $i++){
			$e = [$max[$i] - $cap[$i], ceil($cap[$i]/10.0)];
			if ($e[0] < 0) $e[0] = 0;
			//echo '<br>'.print_r($e,true);
			if (isset($d)){
				if ($i > 1){
					$e[0] -= ($d[$i-1]+$d[$i-2]);
				}else $e[0] -= $d[$i-1];
				if ($e[0] < 0) $e[0] = 0;
				array_push($d, $e[0]);
			} else {
				$d = [$e[0]];
			}
			array_push($f, $e[0]>$e[1]? $e[1]*2+($e[0]-$e[1])*3 : $e[0]*2);
		}
		$amount_excees = $a * $f[0] + $b * $f[1] + $demand_charge->op[$summer? 0:1] * $f[2];
		//echo '<br>cap:'.print_r($cap,true).'<br>d:'.print_r($d,true).'<br>f:'.print_r($f,true);
		return [$amount_demand, $amount_excees];
	}
	protected function calcDemandAmountB($demand_charge,$summer){
		$amount_demand = 0;
		$b = $demand_charge->ssp[$summer? 0:1];
		$amount_demand= $b * (($this->demand_contract['saturday_semi_peak'] + $this->demand_contract['off_peak'])
				-0.5 * ($this->demand_contract['normal']+$this->demand_contract['semi_peak']));
		if ($amount_demand< 0) $amount_demand= 0;
		$amount_demand+= $demand_charge->regular[$summer? 0:1] * $this->demand_contract['normal'];
		$amount_demand+= $demand_charge->pp * $this->demand_contract['semi_peak'];
		// calc excees charge
		foreach (self::$TPLIST as $i=>$n){
			if ($i == 0){
				$cap = [$this->demand_contract[$n]];
				$max = [$this->max_demand[$n]];
			} else{
				array_push($cap, $cap[$i-1] + $this->demand_contract[$n]);
				array_push($max, $this->max_demand[$n]);
			}
		}
		$f = [];
		for ($i = 0; $i < 4; $i++){
			$e = [$max[$i] - $cap[$i], ceil($cap[$i]/10.0)];
			if ($e[0] < 0) $e[0] = 0;
			//echo '<br>'.print_r($e,true);
			if (isset($d)){
				if ($i > 1){
					$e[0] -= ($d[$i-1]+$d[$i-2]);
					if ($i > 2) $e[0] -= $d[$i-3];
				}else $e[0] -= $d[$i-1];
				if ($e[0] < 0) $e[0] = 0;
				array_push($d, $e[0]);
			} else {
				$d = [$e[0]];
			}
			array_push($f, $e[0]>$e[1]? $e[1]*2+($e[0]-$e[1])*3 : $e[0]*2);
		}
		$amount_excees = $demand_charge->regular[$summer? 0:1]* $f[0] 
			+ $demand_charge->pp * $f[1]
			+ $b * $f[2] + $demand_charge->op[$summer? 0:1] * $f[3];
		//echo '<br>cap:'.print_r($cap,true).'<br>d:'.print_r($d,true).'<br>f:'.print_r($f,true);
		return [$amount_demand, $amount_excees];
	}
	//--- calc charge amount by using total energy
	protected function calcEnergyAmount($u, $trset=null){
		if(is_null($trset)) $trset = $this->getTimeRateSet();
		$s = $this->isSummer()? "summer":"nonsummer";
		$tr = array_reduce($this->service_rate->energy_charge->{$s}, function($a,$i) use ($trset){
			$r = $trset[$i];
			foreach ($r as $b){
				$a[$this->cotp->{$b[1]}] = $b[2];
			}
			return $a;
		},[]);
		//echo '<br>'.print_r($tr,true);
		$amount_energy = [];
		foreach ($u as $tp=>$e){
			//echo '<br>'.print_r($tp,true).'='.$e;
			if (array_key_exists($tp, $tr))
				$amount_energy[$tp] = ($tr[$tp] * $e);
		}
		//echo '<br>'.print_r($amount_energy,true);
		return $amount_energy;
	}
	//--- time rate getters
	protected function getTimeRateSet(){
		$trset = [];
		foreach ($this->service_rate->rate as $r){
			$tr = [];
			foreach ( explode(',',$r) as $a){
				array_push($tr, explode(':', $a));
			}
			array_push($trset,$tr);
		}
		return $trset;
	}
	protected function getTimeRateInfo($t, $trset){
		// get time rate info: [tr,index,periodoftime,issummer]
		$s = $this->isSummer($t)? "summer":"nonsummer";
		$dow = in_array($t->format("Ymd"), $this->doop)? 7 : $t->format("N");
		$hm = $t->format("Hi");
		$i_tr = $this->service_rate->energy_charge->{$s}[$dow-1];
		$tr = $trset[$i_tr];
		for ($i = count($tr); $i > 0; --$i){
			if ($hm >= $tr[$i-1][0]) break;
		}
		return [$tr[$i-1],$i-1,$t->format("Ym"),$s];
	}
	protected function isSummer($t=null){
		if (!is_null($t)) {
			$day = $t->format("md");
			$this->_summer = !($day < $this->summer_day[0] || $day > $this->summer_day[1]);
		}
		return $this->_summer;
	}
	
	//--- test function
	static function test($st,$dt,$contract=null,$max_dmd=null,$phase=3){
		$data1 = [];
		$t = date_create_from_format("Y-m-d H:i", $dt);
		$pot = $t->format("Ym");
		$kwh = mt_rand(100,1000);
		do{
			$kwh += mt_rand(1,100)/100.0;
			array_push($data1,[$t->format("Y-m-d H:i"), $kwh]);
			$t->modify("+5 minutes");
		} while($pot == $t->format("Ym"));
		//
		//$r = new TPC_Billing($st);
		$r = new TPC_Billing($st, json_decode(file_get_contents("./tpc20180401.json")));
		$r->setPhase($phase);
		echo "<hr>".$r->service_rate->desc."(".$data1[0][0].") ed:".$r->service_rate->effective_date;
		if ($contract != null) $r->setDemandContract($contract);
		if ($max_dmd != null) $r->setMaxDemand($max_dmd);
		echo '<br>'.json_encode($r->pricing($data1));
	}
}


/*
// testing 1...
header("Content-Type:text/html; charset=utf-8");

$r = new TPC_Billing(2);
$testdata = [
["2017-03-16 00:00",26882.52],
["2017-03-16 00:05",28894.56],
["2017-03-16 00:10",28895.64],
["2017-03-16 00:15",28896.6],
["2017-03-16 00:20",28897.68],
["2017-03-16 00:25",28898.52],
["2017-03-16 07:35",28899.99]
];
echo '<br>'.json_encode($r->pricing($testdata));
//
$r = new TPC_Billing(7);
$contract = ["normal"=>60, 'semi_peak'=>0, "saturday_semi_peak"=>20, "off_peak"=>10];
$r->setDemandContract($contract);
echo '<br>'.json_encode($r->pricing($testdata)); 

$contract = ["normal"=>78, "semi_peak"=>0, "saturday_semi_peak"=>0, "off_peak"=>0];
$max_dmd1 = ["normal"=>71, "semi_peak"=>0, "saturday_semi_peak"=>73, "off_peak"=>76];
$energy = ["energy"=>["peak"=>8480, "semi_peak"=>0, "saturday_semi_peak"=>2080, "off_peak"=>4400], "date"=>"2017-03-30"];
$r = new TPC_Billing(5);
$r->setDemandContract($contract);
$r->setMaxDemand($max_dmd1);
$r->setPhase(3);
$r->setEnergy($energy);
echo '<br>'.json_encode($r->pricing()); 
*/
/*
// testing 2...
header("Content-Type:text/html; charset=utf-8");

TPC_Billing::test(1, "2017-07-01 00:00");
TPC_Billing::test(2, "2017-10-01 00:00");
TPC_Billing::test(3, "2017-10-01 00:00");
TPC_Billing::test(4, "2017-07-01 00:00");

$contract = ["normal"=>60, "semi_peak"=>10, "saturday_semi_peak"=>6, "off_peak"=>5];
$max_dmd1 = ["normal"=>69, "semi_peak"=>0, "saturday_semi_peak"=>85, "off_peak"=>90];
TPC_Billing::test(5, "2017-07-01 00:00",$contract,$max_dmd1);

$contract = ["normal"=>50, "semi_peak"=>0, "saturday_semi_peak"=>0, "off_peak"=>0];
TPC_Billing::test(6,"2017-07-01 00:00",$contract);
TPC_Billing::test(6,"2017-10-01 00:00",$contract);

$contract = ["normal"=>60, "semi_peak"=>10, "saturday_semi_peak"=>6, "off_peak"=>5];
$max_dmd1 = ["normal"=>69, "semi_peak"=>0, "saturday_semi_peak"=>85, "off_peak"=>90];
TPC_Billing::test(7,"2017-07-01 00:00",$contract,$max_dmd1);
$max_dmd2 = ["normal"=>70, "semi_peak"=>0, "saturday_semi_peak"=>77, "off_peak"=>95];
TPC_Billing::test(7,"2017-10-01 00:00",$contract,$max_dmd2);

$contract = ["normal"=>100, "semi_peak"=>20, "saturday_semi_peak"=>10, "off_peak"=>5];
$max_dmd1 = ["normal"=>110, "semi_peak"=>0, "saturday_semi_peak"=>146, "off_peak"=>140];
TPC_Billing::test(8,"2017-07-01 00:00",$contract,$max_dmd1);
$max_dmd2 = ["normal"=>124, "semi_peak"=>0, "saturday_semi_peak"=>139, "off_peak"=>159];
TPC_Billing::test(8,"2017-10-01 00:00",$contract,$max_dmd2);

$contract = ["normal"=>200, "semi_peak"=>20, "saturday_semi_peak"=>10, "off_peak"=>5];
$max_dmd1 = ["normal"=>201, "semi_peak"=>223, "saturday_semi_peak"=>236, "off_peak"=>245];
TPC_Billing::test(9,"2017-07-01 00:00",$contract,$max_dmd1);
$max_dmd2 = ["normal"=>0, "semi_peak"=>223, "saturday_semi_peak"=>236, "off_peak"=>268];
TPC_Billing::test(9,"2017-10-01 00:00",$contract,$max_dmd2);

$contract = ["normal"=>78, "semi_peak"=>0, "saturday_semi_peak"=>0, "off_peak"=>0];
$max_dmd1 = ["normal"=>86, "semi_peak"=>0, "saturday_semi_peak"=>86, "off_peak"=>91];
TPC_Billing::test(5, "2017-04-01 00:00",$contract,$max_dmd1);
*/
/*
// testing 3...
$rtable = json_decode(file_get_contents("./tpc20180401.json"));
$r = new TPC_Billing(5, $rtable);
$contract = ["normal"=>78, "semi_peak"=>0, "saturday_semi_peak"=>0, "off_peak"=>0];
$mx_dmd = ["normal"=>71, "semi_peak"=>0, "saturday_semi_peak"=>73, "off_peak"=>76];
$energy = ["energy"=>["peak"=>8480, "semi_peak"=>0, "saturday_semi_peak"=>2080, "off_peak"=>4400], "date"=>"2017-03-30"];
$r->setDemandContract($contract);
$r->setMaxDemand($mx_dmd);
$r->setPhase(3);
$r->setEnergy($energy);
echo '<br>'.json_encode($r->pricing());
*/
?>