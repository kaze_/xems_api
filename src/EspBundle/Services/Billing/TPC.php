<?php
namespace EspBundle\Services\Billing;
/*
 * xMight TPC Billing
 */
use EspBundle\Services\Billing\Core\TPC_Billing;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;

class TPC extends TPC_Billing {

/*    public function __construct($type, $rtable = null)
    {
        //$path = $this->get('kernel')->getRootDir() . '/../src/EspBundle/Resources/config/tpc20180401.json';
        $path = __DIR__.'/../../../../../src/EspBundle/Resources/config/tpc20180401.json';
        $rate_table = is_null($rtable)? json_decode(file_get_contents($path)) : $rtable;
        parent::__construct($type, $rate_table);
    }*/

    public function isSummer($t=null){
        return parent::isSummer($t);
    }

    public function getRateInfo($t) {
	    $r['isSummer'] = $this->isSummer($t) ? true: false;
	    switch($this->service_type) {
            case 1:
            case 2:
                if (is_null($this->_energy))
                $rate = $this->service_rate->rate;
                $s = $this->isSummer($t) ? 0: 1;
                $e = is_null($this->_energy) ? 0: $this->_energy["normal"];
                for ($i = count($rate); $i > 0; --$i){
                    if ($e >= $rate[$i-1][0]){
                        $r['rate'] = $rate[$i-1][$s+1];
                    }
                }
                break;
            case 6:
                $s = $this->isSummer($t) ? 0: 1;
                $r['rate'] = $this->service_rate->energy_charge[$s];
                break;
            default:
                $trset = $this->getTimeRateSet();
                $tr = $this->getTimeRateInfo($t, $trset);
                $r['tou'] = $tr[0][1];
                $r['rate'] = $tr[0][2];
                break;
        }
        return $r;
    }

    public function getRateSet()
    {
        return $this->getTimeRateSet();
    }

    public static function getTpList($camelCase=false)
    {
        if ( $camelCase == true ) {
            $tpList = array();
            $converter = new CamelCaseToSnakeCaseNameConverter(null, true );
            foreach(self::$TPLIST as $period)
            {
                $tpList[] = $converter->denormalize($period);
            }
            return $tpList;
        }
        return self::$TPLIST;
    }

    public function getPeriodFromCode($c)
    {
        return $this->cotp->{$c};
    }

    public function reduce_monthly_to_power($datalog)
    {
        switch($this->service_type) {
            case 1:
            case 2:
            case 6:
                $c = $this->reduce_monthly_ntou($datalog);
                $kwh = $c[0][1][1] - $c[0][0][1];
                // $s = $this->isSummer($c[0][1][0])? 0:1;
                return ["normal" => $kwh];
                break;
            default:
                $c = $this->reduce_monthly_tou($datalog, $this->getTimeRateSet());
                // $summer = $this->isSummer($c[0][0][0]);
                $u = $this->reduce_time_period($c);
                foreach ($u as $k=>$v){
                    $amount[$this->cotp->{$k}] = $v[0];
                }
                return $amount;
                break;
        }

    }
    public function getMaxDmdData($datalog)
    {
        switch($this->service_type) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 6:
                $dmd=[];
                foreach($datalog as $log) { 
                    $dmd[] = $log[1];
                }
                return ["normal" => max(array_values($dmd))];
                break;
            default:
                $max_dmd = array();
                foreach($datalog as $d) {
                    $r = $this->getRateInfo(date_create_from_format("Y-m-d H:i", $d[0]));
                    if ( !isset($max_dmd[$r["tou"]])) {
                        $max_dmd[$r["tou"]] = $d[1];
                    } else {
                        if ( $d[1] > $max_dmd[$r["tou"]]) {
                            $max_dmd[$r["tou"]] = $d[1];
                        } 
                    }
                }
                $max_dmd_new = array();
                foreach($this->cotp as $k => $v){
                    $max_dmd_new[$v] = isset($max_dmd[$k]) ? $max_dmd[$k] : 0;
                }
                $max_dmd_new['normal'] = $max_dmd_new['peak'];
                return $max_dmd_new;
                break;
        }

    }
    //--- test function
	static function test($st,$dt,$contract=null,$max_dmd=null,$phase=3){
		$data1 = [];
		$t = date_create_from_format("Y-m-d H:i", $dt);
		$pot = $t->format("Ym");
		$kwh = mt_rand(100,1000);
		do{
			$kwh += mt_rand(1,100)/100.0;
			array_push($data1,[$t->format("Y-m-d H:i"), $kwh]);
			$t->modify("+5 minutes");
		} while($pot == $t->format("Ym"));
		//
		$r = new TPC_Billing($st);
		$r->setPhase($phase);
		echo "<hr>".$r->service_rate->desc."(".$data1[0][0].")";
		if ($contract != null) $r->setDemandContract($contract);
		if ($max_dmd != null) $r->setMaxDemand($max_dmd);
		echo '<br>'.json_encode($r->pricing($data1));
	}
}
?>