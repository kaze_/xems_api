<?php
/**
 * Created by PhpStorm.
 * User: chiehyingpan
 * Date: 2018/2/22
 * Time: 上午11:51
 */

namespace EspBundle\Services;


use Doctrine\ORM\EntityManager;
use AppBundle\Repository\EmsLocationMasterRepository;
use Doctrine\ORM\ORMException;
use EspBundle\Entity\EspCustomerRate;

class EnergyFee
{
    private $em;
    private $taiPower;
    private $tree;

    public function __construct(EntityManager $em, $taipower, $tree)
    {
        $this->em = $em;
        $this->taiPower = $taipower;
        $this->tree = $tree;
    }

    /**
     * @param $espCustomer
     * @return EspCustomerRate|object
     */
    public function getCustomerRate($espCustomer)
    {
        $repo = $this->em->getRepository('EspBundle:EspCustomerRate');
        $rate = $repo->findOneBy(array(
            'espCustomer' => $espCustomer,
            'statusId' => 1
        ));
        return $rate;
    }

    public function getCustomerRateDetail($espCustomer)
    {
        $repo = $this->em->getRepository('EspBundle:EspCustomerRate');
        $rate = $repo->findOneBy(array(
            'espCustomer' => $espCustomer,
            'statusId' => 1
        ));
        if ( !$rate ) {
            return null;
        }
        $detailRepo = $this->em->getRepository('EspBundle:EspEnergyPackageDetail');
        $qb = $detailRepo->createQueryBuilder('q');
        $qb->where('q.energyPackage = :package')
            ->setParameter('package', $rate->getEnergyPackage())
            ->orderBy('q.startDate', 'DESC');
        $res = $qb->getQuery()->getOneOrNullResult();

        if ( !$res ) {
            return null;
        }
        $data = array();
        $data['energyPackage'] = $rate->getEnergyPackage()->getEnergyPackageId();
        $data['esp'] = $rate->getEnergyPackage()->getEnergySp()->getEnergySpId();
        $data['hasContract'] = $res->getHasContract();
        if ( $res->getHasContract()) {
            $data['normalContract'] = $rate->getNormalContract();
            $data['semiPeakContract'] = $rate->getSemiPeakContract();
            $data['saturdaySemiPeakContract'] = $rate->getSaturdaySemiPeakContract();
            $data['offPeakContract'] = $rate->getOffPeakContract();
        }
        $meterId = $espCustomer->getPowerMeterDeviceId();
        $meterRepo = $this->em->getRepository('AppBundle:EmsPowerMeter');
        $meter = $meterRepo->find($meterId);
        if ( $meter ) {
            $voltage = $meter->getVoltage();
            if ( $voltage ) {
                $data['voltage'] = $voltage->getVoltageId();
            }
        }
        return $data;
    }

    public function getCustomerRateDetailByCircuit($circuitID)
    {
        // 1. find by circuit root
        $espCustomer = null;
        $rootCircuit = $this->tree->getRoot($circuitID);
        if ( $rootCircuit ) {
            $meter = $rootCircuit->getValidDevice();
            $espRepo = $this->em->getRepository('EspBundle:EspCustomerMaster');
            $espCustomer = $espRepo->findOneBy(array('powerMeterDeviceId' => $meter->getDeviceId()));
        }
        // if can't find by circuit root, find by location
        if ( $espCustomer == null ) {
            $repo = $this->em->getRepository('AppBundle:EmsLocationMaster');
            if ( !$espCustomer ) {
                $location = $repo->findByCircuit($circuitID);
                if ( $location == null ) {
                    return null;
                }
                $espCustomers = $location->getEspCustomer();
                if ( $espCustomers->isEmpty() ) {
                    return null;
                }
                $espCustomer = $espCustomers[0];
            }
        }
        if ( $espCustomer == null ) {
            return null;
        }
        return $this->getCustomerRateDetail($espCustomer);
    }

    public function deviceFee($start, $end, $circuitID, $espCustomer=null)
    {
        $rate = $this->getCustomerRateDetailByCircuit($circuitID);
        if ( $rate == null ) {
            $rate = [
                'energyPackage' => 1 , 
                'hasContract' => 0
            ];
        }
        if ( $end == null ) {
            $endDatetime = new \DateTime();
            $end = $endDatetime->format('Y-m-d H:i:s');
        }

        $logRepo = $this->em->getRepository('AppBundle:EmsPowerReportLog');
        try {
            $logs = $logRepo->findDataByCircuit(
                array('reportDatetime', 'kwhTotal', 'currDmdKw'), 
                array(
                    'startDate' => $start,
                    'endDate' => $end,
                    'circuit' => $circuitID, 
                    'interval' => 60
            ), array('reportDatetime' => 'ASC'));

        } catch(ORMException $exception) {
            $this->logger->error("getCircuitFee error ", [$exception->getMessage()]);
            return null;
        }

        if ( count($logs) == 0  ) {
            return null;
        }
        $data = array();
        $dmdData = array();
        $firstDatetime = $logs[0]['reportDatetime'];
        foreach($logs as $log) {
            $oneData = array($log['reportDatetime']->format('Y-m-d H:i'), $log['kwhTotal']);
            $data[] = $oneData;
            $oneDmdData = array($log['reportDatetime']->format('Y-m-d H:i'), $log['currDmdKw']);
            $dmdData[] = $oneDmdData;
        }
        $energy = $this->taiPower->reduceMonthlyToPower($rate['energyPackage'], $data);
        $energyArray = array('energy' => $energy, 'date' => $firstDatetime->format('Y-m-d'));
        $maxDmd = $this->taiPower->getMaxDmdData($rate['energyPackage'], $dmdData);
        if ( $rate['hasContract']) {
            $contract = array(
                "normal" => $rate["normalContract"],
                "semi_peak" => $rate["semiPeakContract"],
                "saturday_semi_peak" => $rate["saturdaySemiPeakContract"],
                "off_peak" => $rate["offPeakContract"]
            );
            $fee = $this->taiPower->getPricing($rate['energyPackage'], $data, $energyArray, $contract, $maxDmd);
        } else {
            $fee = $this->taiPower->getPricing($rate['energyPackage'], $data, $energyArray);
        }
        $ret = array();
        $ret['startKwh'] = $data[0][1];
        $ret['endKwh'] = $data[count($data)-1][1];
        $ret['totalKwh'] = $ret['endKwh'] - $ret['startKwh'];
        $basic = 0;
        $amount = 0;
        $basic_excees = 0;
        $amount = 0;
        $extra = array();
        if ( $fee != null && count($fee) != 0 ) {
            foreach($fee as $tou => $touFee) {
                if ($tou == 'basic') {
                    $basic = $touFee;
                    continue;
                }
                if ($tou == 'basic_excees') {
                    $basic_excees = $touFee;
                    continue;
                }
                $extra[$tou]['amount'] = $touFee;
                $extra[$tou]['kwh'] = $energy[$tou];
                if ( $maxDmd ) {
                    $extra[$tou]['usage'] = $maxDmd[$tou];
                }
                $amount += $touFee;
            }
        }
        $ret['fee'] = $extra;
        $ret['basic'] = $basic;
        $ret['amount'] = $amount;
        $ret['basicExcees'] = $basic_excees;
        return $ret;
    }

    public function circuitFee($start, $end, $circuitID, $espCustomer=null)
    {
        return $this->deviceFee($start, $end, $circuitID, $espCustomer);
    }

    public function getDemandContract($espCustomer, $time)
    {
        $customerRate = $this->getCustomerRate($espCustomer);
        if ( $customerRate == null ) {
            return 0;
        }
        $espRate = $this->taiPower->getRate($customerRate->getEnergyPackage()->getEnergyPackageId(), $time);

        if ( !isset($espRate['tou'])) {
            return $customerRate->getNormalContract();
        }

        switch($espRate['tou']) {
            case 'p':
                return $customerRate->getNormalContract();
            case 'pp':
                return $customerRate->getSemiPeakContract();
            case 'ssp':
                return $customerRate->getSaturdaySemiPeakContract();
            case 'op':
                return $customerRate->getOffPeakContract();
            default:
                return 0;
        }
    }

    public function getCurrentTou($espCustomer, $time=null)
    {
        $customerRate = $this->getCustomerRate($espCustomer);
        if ( $customerRate == null ) {
            return 0;
        }
        if ( $time == null ) {
            $time = new \Datetime();
        }
        $espRate = $this->taiPower->getRate($customerRate->getEnergyPackage()->getEnergyPackageId(), $time);
        if ( !isset($espRate['tou'])) {
            return "peak";
        }
        switch($espRate['tou']) {
            case 'p':
                return "peak";
            case 'pp':
                return "semi_peak";
            case 'ssp':
                return "saturday_semi_peak";
            case 'op':
                return "off_peak";
            default:
                return 0;
        }
    }

    public function getCurrentTouByLocation($location, $time=null)
    {
        $objLocation = $location;
        if ( is_string($location)) {
            $objLocation = $this->em->find('Appbundle:EmsLocationMaster', $location);
        }
        if ( $objLocation ) {
            $espCustomers = $objLocation->getEspCustomer();
            if ( count($espCustomers) == 0) {
                return 0;
            }
            $espCustomer = $espCustomers[0];
            return $this->getCurrentTou($espCustomer, $time );
        }
        return 0;
    }
}
