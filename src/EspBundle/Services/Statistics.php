<?php
namespace EspBundle\Services;

use Doctrine\ORM\EntityManager;
use EspBundle\Entity\EspElectricityUsageMonthly;
use EspBundle\Entity\EspElectricityFeeDaily;
use Symfony\Component\Debug\Exception\FatalErrorException;

class Statistics
{

    private $_em;
    private $_report;
    private $_feeService;
    private $_tree;
    
    private $_createTime;
    
    private $_cycleFeeRepo;
    private $_monthlyRepo;
    private $_dailyRepo;
    
    public const MONTHLY=1;
    public const DAILY=2;
    
    public function __construct(EntityManager $em, $powerReport, $feeService, $circuitTree)
    {
        $this->_em = $em;
        $this->_report = $powerReport;
        $this->_feeService = $feeService;
        $this->_tree = $circuitTree;
        
        $this->_createTime = new \DateTime();
        $this->_monthlyRepo = $this->_em->getRepository('EspBundle:EspElectricityUsageMonthly');
        $this->_dailyRepo = $this->_em->getRepository('EspBundle:EspElectricityFeeDaily');
        $this->_meterRepo = $this->_em->getRepository('AppBundle:EmsPowerMeter');
        
    }
    
    private function getCycleFee($circuit, $startDate, $cycle, $customer, $customerRateDetail)
    {
        // echo ($child->getCircuitId());
        
        $now = new \DateTime('today');
        $endDate = clone($startDate);
        
        $repo = null;
        $dummy = null;
        switch($cycle) {
            case $this::MONTHLY:
                $repo = $this->_monthlyRepo;
                $endDate->modify('+1 month');
                $dummy = new EspElectricityUsageMonthly();
                break;
            case $this::DAILY:
                $repo = $this->_dailyRepo;
                $endDate->modify('+1 day');
                $dummy = new EspElectricityFeeDaily();
                break;
            default:
                return null;
        }
        $endDate->modify('+5 sec');
        if ( $now < $endDate ) {
            $endDate = $now->setTime(0,0,5);
        }
        
        $cycleFee = $repo->findOneBy(array(
            'date' => $startDate,
            'circuit' => $circuit
        ));
        
        if ( $cycleFee == null ) {
            $cycleFee = $dummy;
            $cycleFee->setCircuit($circuit)
            ->setDate($startDate)
            ->setEspCustomer($customer)
            ->setEnergyPackageId($customerRateDetail['energyPackage']);
        }
        
        $charge = $this->_report->getCircuitFee($circuit, $startDate, $endDate, $customerRateDetail);
        if ( $charge == null ) {
            return null;
        }
        $fee = $charge['fee'];
        foreach($fee as $tou => $touData) {
            if ( strtolower($tou) == 'peak') {
                $tou = 'normal';
            }
            $touCamelCase = str_replace('_', '', ucwords($tou, '_'));
            $feeMethod = 'set'.$touCamelCase.'Fee';
            $usageMethod = 'set'.$touCamelCase.'Usage';
            $cycleFee->$feeMethod($touData['amount']);
            $cycleFee->$usageMethod($touData['kwh']);
        }
        $cycleFee->setTotalUsage($charge['endKwh'] - $charge['startKwh']);
        $cycleFee->setBasicFee($charge['basic']);
        $cycleFee->setEnergyFee($charge['amount']);
        $cycleFee->setTotalFee($charge['basic'] + $charge['amount']);
        return $cycleFee;
        
    }
    
    public function singleCustomerFee($cycle, $startDate, $customer)
    {
        echo($customer->getEspCustomerNumber().PHP_EOL);
        // get customer rate
        $customerRateDetail = $this->_feeService->getCustomerRateDetail($customer);
        // print_r($customerRateDetail);
        $meter = $this->_meterRepo->find($customer->getPowerMeterDeviceId());
        if ( !$meter ) return ;
        
        // get related main meter
        $this->_em->getConnection()->beginTransaction();
        try {
            $circuit = $meter->getCircuits()[0]->getCircuit();
            // get master meter fee
            // echo ("root: ".$circuit->getCircuitId().PHP_EOL);
            $cycleFee = $this->getCycleFee($circuit, $startDate, $cycle, $customer, $customerRateDetail);
            if ( $cycleFee ) {
                $this->_em->persist($cycleFee);
            }
            
            $children = $this->_tree->getChildren($circuit);
            
            foreach($children as $child) {
                // echo ("child: ".$child->getCircuitId().PHP_EOL);
                $cycleFee = $this->getCycleFee($child, $startDate, $cycle, $customer, $customerRateDetail);
                if ( $cycleFee ) {
                    $this->_em->persist($cycleFee);
                }
            }
            $this->_em->flush();
            $this->_em->getConnection()->commit();
        } catch (FatalErrorException $fex) {
            echo($fex->getMessage());
            $this->_em->getConnection()->rollBack();
        } catch (\Exception $ex) {
            echo($ex->getMessage());
            $this->_em->getConnection()->rollBack();
        }
    }
    
    public function cycleFee($cycle, $startDate=null)
    {
        if ( $startDate == null ) {
            $startDate = new \DateTime('first day of this month');
            $startDate->setTime(0,0,0);
        }
        
        $espCustomers = $this->_em->getRepository('EspBundle:EspCustomerMaster')->findAll();
        foreach($espCustomers as $customer) {
            $this->singleCustomerFee($cycle, $startDate, $customer);
        }
    }

    public function getMohthlyFee($circuits, $yearMonth) {
        
        $date = \Datetime::createFromFormat('Ymd', $yearMonth.'01');
        $date->setTime(0, 0, 0);
        $qb = $this->_monthlyRepo->createQueryBuilder('q');
        $qb->select('sum(q.normalUsage) as normalUsage, sum(q.semiPeakUsage) as semiPeakUsage, sum(q.saturdaySemiPeakUsage) as saturdaySemiPeakUsage, sum(q.offPeakUsage) as offPeakUsage, 
            sum(q.normalFee) as normalFee, sum(q.semiPeakFee) as semiPeakFee, sum(q.saturdaySemiPeakFee) as saturdaySemiPeakFee, sum(q.offPeakFee) as offPeakFee')
        ->where('q.date = :date')
        ->andWhere('q.circuit in (:circuits)')
        ->setParameter('date', $date)
        ->setParameter('circuits', $circuits);

        $result = $qb->getQuery()->getResult();
        return $result;
    }
}

