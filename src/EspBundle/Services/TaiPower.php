<?php
/**
 * Created by PhpStorm.
 * User: chiehyingpan
 * Date: 2018/2/20
 * Time: 上午10:05
 */

namespace EspBundle\Services;

use EspBundle\Services\Billing\TPC;
use Symfony\Component\Config\FileLocatorInterface as FileLocator;

class TaiPower
{
    private $rateTable;

    public function __construct(FileLocator $fileLocator)
    {
        $path = $fileLocator->locate('@EspBundle/Resources/config/tpc20180401.json');
        //$path = $fileLocator->locate('@EspBundle/Resources/config/tpc20160401.json');
        $this->rateTable = json_decode(file_get_contents($path));
        // print_r($this->rateTable);
    }

    // public function getPricing($type, $data, $contract=null)
    public function getPricing($type, $data, $energy=null, $contract=null,$max_dmd=null,$phase=3)
    {
        try {
            $tpc = new TPC($type, $this->rateTable);
            $tpc->setPhase($phase);
            if ( $contract ) {
                $tpc->setDemandContract($contract);
            }
            if ( $max_dmd ) {
                $tpc->setMaxDemand($max_dmd);
            }
            if ( $energy ) {
                $tpc->setEnergy($energy);
                return $tpc->pricing(null);
            } else {
                return $tpc->pricing($data);
            }
        } catch (\Exception $e) {
            // echo($e->__toString());
            return null;
        }
    }

    public function isSummer($type, $time)
    {
        try {
            $tpc = new TPC($type, $this->rateTable);
            return $tpc->isSummer($time);
        } catch(\Exception $e) {
            return null;
        }
    }

    public function getRate($type, $time){
        $rate = null;
        try {
            $tpc = new TPC($type, $this->rateTable);
            // $rate['isSummer'] = $tpc->isSummer($time);
            $rate = $tpc->getRateInfo($time);
            return $rate;
        } catch (\Exception $exception) {
            return null;
        }
    }

    public function getRateSet($type){
        $rate = null;
        try {
            $tpc = new TPC($type, $this->rateTable);
            // $rate['isSummer'] = $tpc->isSummer($time);
            $rate = $tpc->getRateSet();
            return $rate;
        } catch (\Exception $exception) {
            return null;
        }
    }
    public function reduceMonthlyToPower($type, $data)
    {
        $rate = null;
        try {
            $tpc = new TPC($type, $this->rateTable);
            $power = $tpc->reduce_monthly_to_power($data);
            return $power;
        } catch (\Exception $exception) {
            return null;
        }
    }

    public function getMaxDmdData($type, $data)
    {
        try {
            $tpc = new TPC($type, $this->rateTable);
            $max_dmd = $tpc->getMaxDmdData($data) ;
            return $max_dmd;
        } catch (\Exception $exception) {
            // echo($exception->__toString());
            return null;
        }
    }

    public function getOffPeakDays()
    {
        try {
            $tpc = new TPC(1, $this->rateTable);
            $days = $tpc->getOffPeakDays();
            return $days;
        } catch(\Exception $exception) {
            echo($exception->getTraceAsString());
            return null;
        }
    }

/*    public function getDemandContract($type, )*/
}
