<?php

namespace EvBundle\Command;

use Doctrine\DBAL\DriverManager;
use EvBundle\Entity\CreditCard;
use EvBundle\Entity\PayHistory;
use Exception;
use Predis\Client;
use Predis\Connection\ConnectionException;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PayTradeCommand extends RedisSubCommand
{
    function cmdName()
    {
        return 'pay:trade';
    }

    function cmdDesc()
    {
        return 'Trade Order';
    }

    function topic()
    {
        return 'paytrade';
    }
    
    protected function send($orderId)
    {
        parent::send($orderId);

        $container = $this->getContainer();
        $credit = $container->get('credit');

        if( ! $credit->doTrade($orderId)){
            $this->_log('[deamon] trade order failed: id='.$orderId.', error='.$credit->error());
            return false;
        }
        return true;
    }

}
