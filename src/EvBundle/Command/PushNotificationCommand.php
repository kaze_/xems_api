<?php

namespace EvBundle\Command;

use Exception;
use Predis\Client;
use Predis\Connection\ConnectionException;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PushNotificationCommand extends RedisSubCommand
{
    function cmdName()
    {
        return 'push:notification';
    }

    function cmdDesc()
    {
        return 'Push notification to mobile.';
    }

    function topic()
    {
        return 'push_notification';
    }
    
    protected function send($id)
    {
        parent::send($id);
        
        $pn = $this->getContainer()->get('push_notification');
        
        if( ! $pn->push($id)){
            $this->_log('[deamon] push notification failed: id='.$id.', error='.$pn->error());
            return false;
        }
        return true;
    }
}
