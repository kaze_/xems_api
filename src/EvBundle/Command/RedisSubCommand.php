<?php

namespace EvBundle\Command;

use Doctrine\DBAL\DriverManager;
use EvBundle\Entity\CreditCard;
use EvBundle\Entity\PayHistory;
use Exception;
use Predis\Client;
use Predis\Connection\ConnectionException;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class RedisSubCommand extends ContainerAwareCommand
{
    abstract function cmdName();
    abstract function cmdDesc();
    abstract function topic();
    
    private $isAsync = false;
    
    protected function configure()
    {
        $this
            ->setName($this->cmdName())
            ->setDescription($this->cmdDesc())
            ->addArgument('id', InputArgument::OPTIONAL, 'primary key (not require)')
//            ->addOption('option', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        set_time_limit(0);
        ini_set('memory_limit', '1024M');

        if (!$this->testEnv()) {
            return $output->writeln('<fg=white;bg=red>Use nohup php bin/console '.$this->cmdName().' [id] --env=prod</>');
        }

        $this->isAsync = false;
        //check redis


        $id = $input->getArgument('id');
        if (!empty($id)) {
            return $output->writeln(json_encode([
                'success' => $this->send($id)
            ]));
        }

        try {
            $client = new Client(array('read_write_timeout' => 0));
            $pubsub = $client->pubSubLoop();

            // Initialize a new pubsub consumer.
            // Subscribe to your channels
            $pubsub->subscribe($this->topic());
            // Start processing the pubsup messages. Open a terminal and use redis-cli
            // to push messages to the channels. Examples:
            //   ./redis-cli PUBLISH notifications "this is a test"
            //   ./redis-cli PUBLISH paytrade quit_loop
            foreach ($pubsub as $message) {
                switch ($message->kind) {
                    case 'subscribe':
                        echo "Subscribed to {$message->channel}", PHP_EOL;
                        break;
                    case 'message':
                        if ($message->channel == $this->topic()) {
                            $this->isAsync = true;
                            echo 'id:' . $message->payload . PHP_EOL;
                            try {
                                $this->send($message->payload);
                            } catch (Exception $ex) {

                                return $output->writeln(json_encode([
                                    'success' => false,
                                    'msg' => $ex->getMessage()
                                ]));
                            }
                            //                        $pubsub->unsubscribe();
                        }
                        break;
                }
            }
            // Always unset the pubsub consumer instance when you are done! The
            // class destructor will take care of cleanups and prevent protocol
            // desynchronizations between the client and the server.
            unset($pubsub);
            // Say goodbye :-)
            $version = redis_version($client->info());
            echo "Goodbye from Redis {$version}!", PHP_EOL;
        }catch(ConnectionException $ex){
            return $output->writeln('<fg=white;bg=red>need run docker redis first. </>'.$ex->getMessage());
        }
    }


    protected function getRedisClient()
    {
        error_reporting(0);
        ini_set('display_errors', 0);
    }
    
    protected function testEnv()
    {
        error_reporting(0);
        ini_set('display_errors', 0);

        try {
            $container = $this->getContainer();
            $doctrine = $container->get('doctrine');
            $em = $doctrine->getManager();
            $em->find('EvBundle:PayHistory', '123');
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }
    
    protected function send($id)
    {
        $container = $this->getContainer();
        $this->_mysqlReconnect($container);
        $this->_log('[deamon] '.$this->cmdName().': '.$id);
    }

    protected function _log($msg)
    {
        $container = $this->getContainer();
        $logger = $container->get('logger');
        $logger->info($msg);
        if ($this->isAsync) echo $msg;
    }
    
    protected function _mysqlReconnect(ContainerInterface $container)
    {
        $em = $container->get('doctrine.orm.entity_manager');
        if ($em->getConnection()->ping() === false) {
            $em->getConnection()->close();
            $em->getConnection()->connect();
        }
    }
}
