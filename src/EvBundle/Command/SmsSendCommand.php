<?php

namespace EvBundle\Command;

use Exception;
use Predis\Client;
use Predis\Connection\ConnectionException;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SmsSendCommand extends RedisSubCommand
{
    function cmdName()
    {
        return 'sms:send';
    }

    function cmdDesc()
    {
        return 'Sms';
    }

    function topic()
    {
        return 'sms_send';
    }

    protected function send($id)
    {
        parent::send($id);

        $sms = $this->getContainer()->get('sms');

        if (!$sms->send($id)) {
            $this->_log('[deamon] send sms failed: id=' . $id . ', error=' . $sms->error());
            return false;
        }
        return true;
    }
}
    
