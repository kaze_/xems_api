<?php

namespace EvBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
class BaseApiController extends Controller
{
    protected function returnJsonResponse($code, $msg = null, $data = null)
    {
        $return['code'] = $code;

        if($code === JsonResponse::HTTP_OK){
            $httpcode = $code;

            if($data !== null){
                $return['data'] = $data;
            }
            if($msg !== null){
                $return['msg'] = $msg;
            }
        }else{
            // $httpcode = JsonResponse::HTTP_CREATED;
            $httpcode = $code;
            if($data !== null){
                $return['data'] = $data;
            }
            if($msg !== null){
                $return['msg'] = $msg;
            }
        }

        return new JsonResponse($return, $httpcode);
    }
}

