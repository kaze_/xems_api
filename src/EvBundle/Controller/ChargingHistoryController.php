<?php
/**
 * Created by PhpStorm.
 * User: chiehyingpan
 * Date: 2018/2/27
 * Time: 下午2:06
 */

namespace EvBundle\Controller;


use AppBundle\Entity\EvChargingStationMaster;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Context\Context;
use Symfony\Component\HttpFoundation\Request;

class ChargingHistoryController extends BaseApiController
{

    const sessionHistoryLimit = 10;
    const monthlyHistoryLimit = 6;
    const recentSessionLimit = 5;

    public function chargingHistoryAction(EvChargingStationMaster $station)
    {
        $view = View::create();
        $context = (new Context())->addGroup('history');

        $em = $this->getDoctrine()->getManager();
        // get 5 most recent history
        $repo = $em->getRepository('EvBundle:EvChargingSessionLog');
        // $res = $repo->findBy(array('chargingStation' => $station), array('startAt' => 'DESC'), 5);
        // $res = $repo->findDataByStation(array('sessionId', 'startAt', 'basicCost', 'amountCost'), array('chargingStation' => $station), array('startAt' => 'DESC'), 5);
        $condition = array(
            'station' => $station->getChargingStationId());
        $ordering = array('startAt' => 'DESC');
        $res = $repo->findByStation($condition, $ordering, 5);
        $monthRepo = $em->getRepository('EvBundle:EvChargingLogMonthly');
        $monthRes = $monthRepo->findBy(array('chargingStationId' => $station->getChargingStationId()), array('date' => 'DESC'));

        $data = array(
            'recentSessions' => $res,
            'monthly' => $monthRes
        );
        $view->setContext($context);
        $view->setStatusCode(200);
        $view->setData(array('code' => 200, 'data' => $data));
        return $view;
    }

    public function chargingHistoryMonthlyAction(EvChargingStationMaster $station, $yearMonth)
    {
        $view = View::create();
        $context = (new Context())->addGroup('detailHistory');
        $context->addGroup('history');

        $em = $this->getDoctrine()->getManager();
        $monthRepo = $em->getRepository('EvBundle:EvChargingLogMonthly');
        $date = \DateTime::createFromFormat('Y-m-d', $yearMonth.'-01')->setTime(0,0,0);
        $monthRes = $monthRepo->findOneBy(array('chargingStationId' => $station->getChargingStationId(), 'date' => $date));
        // get 5 most recent history
        $repo = $em->getRepository('EvBundle:EvChargingSessionLog');
        $to = clone($date);
        $to->modify('first day of next month');
        $res = $repo->findByStation(
            array('station' => $station->getChargingStationId(), 'from' => $date, 'to' => $to),
            array('startAt' => 'DESC'));
        $data = array(
            'monthly' => $monthRes,
            'allSessions' => $res
        );
        $view->setContext($context);
        $view->setStatusCode(200);
        $view->setData(array('code' => 200, 'data' => $data));
        return $view;
    }

    /**
     * @return array[
     *  'monthly' => this month summary,
     *  'allSessions' => sessions,
     *  'next' => true/false
     * ]
     */
    public function userChargingHistoryMonthlyAction(Request $request, $yearMonth)
    {
        $view = View::create();
        $context = (new Context())->addGroup('detailHistory');
        $context->addGroup('history');

        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $userId = $user->getId();
        
        $from = $request->get('from');
        $date = \DateTime::createFromFormat('Y-m-d', $yearMonth.'-01')->setTime(0,0,0);
        if ( $from != null ) {
            $to = \DateTime::createFromFormat('Y-m-d H:i:s', $from);
        } else {
            $to = clone($date);
            $to->modify('first day of next month');
        }

        $payService = $this->get('pay.points');

        // get monthly carbon reduction and charging fee
        $monthlyPay = $payService->getUserPointsMonthly($userId, $date, $to, 1, 'CHARGING');
        if ( $monthlyPay ) {
            $monthlyCharging = $em->getRepository('EvBundle:EvUserChargingLogMonthly')
                ->findOneBy(['userId' => $userId, 'date' => $date]);
        
            $monthlyRes = [
                'date' => $date,
                'basicCost' => 0,
                'amountCost' => - $monthlyPay[0]['points']
            ];
            if ( $monthlyCharging ) {
                $monthlyRes['carbonReduction'] = $monthlyCharging->getCarbonReduction();
            } else {
                $monthlyRes['carbonReduction'] = 0;
            }
        } else {
            $monthlyRes = [];
        }
        
        // sesison Log
        $next = false;
        $pays = $payService->getUserPointsLogs($userId, $date, $to, $this::sessionHistoryLimit+1, 'CHARGING');
        if ( count($pays) > $this::sessionHistoryLimit ) {
            $next = true;
            array_pop($pays);
        }

        $recentSessionIds = [];
        $payData = [];
        foreach($pays as $chargingLog) {
            $sessionId = $chargingLog->getSessionId();
            if ( $sessionId != null ) {
                $payData[$sessionId] = - $chargingLog->getPoints();
                $recentSessionIds[] = $sessionId;
            }
        }
        $repo = $em->getRepository('EvBundle:EvChargingSessionLog');
        $res = $repo->createQueryBuilder('p')
            ->where('p.sessionId in (:sessionIds)')
            ->setParameter('sessionIds', $recentSessionIds)
            ->orderBy('p.startAt', 'DESC')
            ->getQuery()->getResult();

        $sessions = [];
        foreach($res as $session) {
            $chargingStation = $session->getChargingStation();
            $chargingStationName = $chargingStation? $chargingStation->getName() : '';
            $sessionId = $session->getSessionId()->__toString();
            $oneSess = [
                'chargingStationName' => $chargingStationName,
                'sessionId' => $sessionId,
                'startAt' => $session->getStartAt(),
                'basicCost' => 0,
                'amountCost' => $payData[$sessionId],
                'evPlate' => $session->getEvPlate() ? $session->getEvPlate(): ''
            ];
            $sessions[] = $oneSess;
        }

            $data = array(
                'monthly' => $monthlyRes,
                'allSessions' => $sessions,
                'next' => $next
            );

        $view->setContext($context);
        $view->setStatusCode(200);
        $view->setData(array('code' => 200, 'data' => $data));
        return $view;
    }

    /** 
     * @return array[
     *  'recentSessions' => recent 5 sessions,
     *  'monthly' => summary of months,
     *  'next' => true/false
     * ]
     */
    public function userChargingHistoryAction(Request $request)
    {
        $view = View::create();
        $context = (new Context())->addGroup('history');

        $from = $request->get('from');

        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $userId = $user->getId();
        $payService = $this->get('pay.points');

        // get 5 most recent history
        $pays = $payService->getUserPointsLogs($userId, null, null, $this::recentSessionLimit, 'CHARGING');
        $recentSessionIds = [];
        $payData = [];
        foreach($pays as $chargingLog) {
            $sessionId = $chargingLog->getSessionId();
            if ( $sessionId != null ) {
                $payData[$sessionId] = - $chargingLog->getPoints();
                $recentSessionIds[] = $sessionId;
            }
        }
        $repo = $em->getRepository('EvBundle:EvChargingSessionLog');
        $res = $repo->createQueryBuilder('p')
            ->where('p.sessionId in (:sessionIds)')
            ->setParameter('sessionIds', $recentSessionIds)
            ->orderBy('p.startAt', 'DESC')
            ->getQuery()->getResult();

        $sessions = [];
        foreach($res as $session) {
            $chargingStation = $session->getChargingStation();
            $chargingStationName = $chargingStation? $chargingStation->getName() : '';
            $sessionId = $session->getSessionId()->__toString();
            $oneSess = [
                'chargingStationName' => $chargingStationName,
                'sessionId' => $sessionId,
                'startAt' => $session->getStartAt(),
                'basicCost' => 0,
                'amountCost' => $payData[$sessionId],
                'evPlate' => $session->getEvPlate() ? $session->getEvPlate(): ''
            ];
            $sessions[] = $oneSess;
        }
        
        // get monthly status
        if ( $from != null ) {
            $date = \DateTime::createFromFormat('Y-m-d+', $from)->setTime(0,0,0);
        } else {
            $date = null;
        }
        $monthlyRes = $payService->getUserPointsMonthly($userId, null, $date, $this::monthlyHistoryLimit + 1, 'CHARGING');
        
        $next = false;
        if ( count($monthlyRes) > $this::monthlyHistoryLimit ) {
            $next = true;
            array_pop($monthlyRes);
        }

        $newMonth = [];
        foreach($monthlyRes as $month) {
            $newMonth[] = [
                'date' => $month['date'],
                'amountCost' => - $month['points'],
                'basicCost' => 0
            ];
        }
        $data = array(
            'recentSessions' => $sessions,
            'monthly' => $newMonth,
            'next' => $next
        );

        $view->setContext($context);
        $view->setStatusCode(200);
        $view->setData(array('code' => 200, 'data' => $data));
        return $view; 

    }

    /*
    public function userChargingHistoryMonthlyAction(Request $request, $yearMonth)
    {
        $view = View::create();
        $context = (new Context())->addGroup('detailHistory');
        $context->addGroup('history');

        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $userId = $user->getId();
        
        $from = $request->get('from');
        $monthRepo = $em->getRepository('EvBundle:EvUserChargingLogMonthly');
        $date = \DateTime::createFromFormat('Y-m-d', $yearMonth.'-01')->setTime(0,0,0);
        $monthRes = $monthRepo->findOneBy(['userId' => $userId, 'date' => $date]);

        $repo = $em->getRepository('EvBundle:EvChargingSessionLog');
        if ( $from != null ) {
            $to = \DateTime::createFromFormat('Y-m-d H:i:s', $from);
        } else {
            $to = clone($date);
            $to->modify('first day of next month');
        }
        $res = $repo->findByUser(
            ['user' => $userId, 'from' => $date, 'to' => $to],
            ['startAt' => 'DESC'], $this::sessionHistoryLimit+1 );

        $next = false;
        if ( count($res) > $this::sessionHistoryLimit ) {
            $next = true;
            array_pop($res);
        }
        $sessions = [];
        foreach($res as $session) {
            $chargingStation = $session->getChargingStation();
            $chargingStationName = $chargingStation? $chargingStation->getName() : '';
            $oneSess = [
                'chargingStationName' => $chargingStationName,
                'sessionId' => $session->getSessionId()->__toString(),
                'startAt' => $session->getStartAt(),
                'basicCost' => $session->getBasicCost(),
                'amountCost' => $session->getAmountCost(),
                'evPlate' => $session->getEvPlate() ? $session->getEvPlate(): ''
            ];
            $sessions[] = $oneSess;
        }

        if ( $from != null ) {
            $data = array(
                'monthly' => $monthRes,
                'allSessions' => $sessions,
                'next' => $next
            );
        } else {
            $data = array(
                'monthly' => $monthRes,
                'allSessions' => $sessions, 
                'next' => $next
            );
        }

        $view->setContext($context);
        $view->setStatusCode(200);
        $view->setData(array('code' => 200, 'data' => $data));
        return $view;
    }

    public function userChargingHistoryAction(Request $request)
    {
        $view = View::create();
        $context = (new Context())->addGroup('history');

        $from = $request->get('from');

        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $userId = $user->getId();
        // get 5 most recent history
        $repo = $em->getRepository('EvBundle:EvChargingSessionLog');
        $res = $repo->createQueryBuilder('p')
            ->where('p.userId = :userId')
            ->andWhere('p.endAt is not null')
            ->orderBy('p.startAt', 'DESC')
            ->setMaxResults($this::recentSessionLimit)
            ->setParameter('userId', $userId)
            ->getQuery()->getResult();
        // $res = $repo->findBy(['userId' => $userId, 'endAt' ], ['startAt' => 'DESC'], $this::recentSessionLimit);

        $sessions = [];
        foreach($res as $session) {
            $chargingStation = $session->getChargingStation();
            $chargingStationName = $chargingStation? $chargingStation->getName() : '';
            $oneSess = [
                'chargingStationName' => $chargingStationName,
                'sessionId' => $session->getSessionId()->__toString(),
                'startAt' => $session->getStartAt(),
                'basicCost' => $session->getBasicCost(),
                'amountCost' => $session->getAmountCost(),
                'evPlate' => $session->getEvPlate() ? $session->getEvPlate(): ''
            ];
            $sessions[] = $oneSess;
        }
        $monthRepo = $em->getRepository('EvBundle:EvUserChargingLogMonthly');
        $next = false;
        if ( $from != null ) {
            $date = \DateTime::createFromFormat('Y-m-d+', $from)->setTime(0,0,0);
            $monthRes = $monthRepo->createQueryBuilder('q')
                ->where('q.date < :date')
                ->andWhere('q.userId = :userId')
                ->orderBy('q.date', 'DESC')
                ->setMaxResults($this::monthlyHistoryLimit + 1)
                ->setParameter('date', $date)
                ->setParameter('userId', $userId)
                ->getQuery()->getResult();

                if ( count($monthRes) > $this::monthlyHistoryLimit ) {
                    $next = true;
                    array_pop($monthRepo);
                }
                $data = array(
                    'recentSessions' => $sessions,
                    'monthly' => $monthRes,
                    'next' => $next
                );
        } else {
            $monthRes = $monthRepo->findBy(['userId' => $userId], ['date' => 'DESC'], $this::monthlyHistoryLimit + 1);
            if ( count($monthRes) > $this::monthlyHistoryLimit ) {
                $next = true;
                array_pop($monthRes);
            }
            $data = array(
                'recentSessions' => $sessions,
                'monthly' => $monthRes,
                'next' => $next
            );
        }

        $view->setContext($context);
        $view->setStatusCode(200);
        $view->setData(array('code' => 200, 'data' => $data));
        return $view; 
    }
    */
}
