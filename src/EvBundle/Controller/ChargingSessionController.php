<?php
/**
 * Created by PhpStorm.
 * User: chiehyingpan
 * Date: 2018/2/21
 * Time: 上午9:23
 */

namespace EvBundle\Controller;


use EvBundle\Entity\EvChargingSessionLog;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\EvChargingStationMaster;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use EvBundle\Entity\EvUserStationMapping;
use AppBundle\Entity\EmsSmartRelayTransactionLog;
use Exception;

class ChargingSessionController extends BaseApiController
{

    public function getSessionAction(Request $request, EvChargingSessionLog $session)
    {
        $this->denyAccessUnlessGranted('view', $session);
/*        $validator = Validation::createValidator();
        $errors = $validator->validate($request->request->all(),
            new Collection(array(
                'from' => [new DateTime(), new Optional()],
            ))
        );

        if(isset($errors[0])){
            return $this->returnJsonResponse(
                JsonResponse::HTTP_BAD_REQUEST,
                $errors->get(0)->getPropertyPath().' '.$errors->get(0)->getMessage()
            );
        }*/

        $from = $request->get('from');
        $startAt = $session->getStartAt();
        $circuit = $session->getCircuit()->getCircuitId();
        $endAt = $session->getEndAt();
        if ( $endAt == null  ) {
            $endAt = new \DateTime();
        }

        $sessionService = $this->get('ev.session') ;
        $data = $sessionService->getSessionData($session);
        $em = $this->getDoctrine()->getManager();

        // current
        $repo = $em->getRepository('AppBundle:EmsPowerReportLog');
        $startDate = null;
        if ( $from ) {
            $startDate = (new \DateTime())->setTimestamp(strtotime($from));
        }
        if ( $startDate == null ) {
            $startDate = $startAt;
        }
        $current = $repo->findDataByCircuit(array('reportDatetime', 'aAverage', 'vAverage', 'kwhTotal'), array(
            'startDate' => $startDate->modify('-5 minutes'),
            'endDate' => $endAt->modify('+5 minutes'),
            'circuit' => $circuit,
            'interval' => 60
        ), array('reportDatetime' => 'ASC'));
        if ( $current && count($current) != 0 ) {
            $voltageSum = 0;
            $currSum = 0;
            $maxCurr = 0;
            $currData = array();
            foreach($current as $curr) {
                $oneData = array(
                    'time' => $curr['time'],
                    'current' => $curr['aAverage'],
                );
                $currData[] = $oneData;
                $voltageSum += $curr['vAverage'];
                $currSum += $curr['aAverage'];
                $maxCurr = ($curr['aAverage'] > $maxCurr) ? $curr['aAverage'] : $maxCurr;
            }
            $data['maxCurrent']= $maxCurr;
            $data['avgCurrent'] = $currSum/count($currData);
            $data['avgVoltage'] = $voltageSum/count($currData);
            $data['current'] = $currData;
        }
        return $this->returnJsonResponse(JsonResponse::HTTP_OK, null, $data);
    }

    public function getLatestSessionAction(Request $request, EvChargingStationMaster $station)
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('EvBundle:EvChargingSessionLog');
        $session = $repo->findNewestSessionByStation($station);
        if ( $session ) {
            return $this->getSessionAction($request, $session);
        } else {
            return $this->returnJsonResponse(JsonResponse::HTTP_NOT_FOUND, "Session Data Not Found");
        }
    }
    
    public function getUserActiveSessionAction(Request $request)
    {
        $view = new View();
        
        $sess = $this->get('ev.session');
        $user = $this->getUser();
        $status = $request->get('status');
        if ( $status == 'active' ) {
            $sessions = $sess->getActiveSessionByUser($user);
        }
        $em = $this->getDoctrine()->getManager();
        $res = array();
        if ( $sessions != null ) {
            foreach($sessions as $session) {
                $oneData = array();
                $oneData['sessionId'] = $session->getSessionId()->toString();
                $station = $session->getChargingStation();
                $oneData['id'] = $station->getChargingStationId();
                $oneData['name'] = $station->getName(); 
                $oneData['chargingStationName'] = $station->getName();   
                $location = $em->getRepository('AppBundle:EmsLocationMaster')->findByChargingStation($station->getChargingStationId());
                // $oneData['status'] = $station->getStatusId();
                $oneData['status'] = 2;
		        $oneData['longitude'] =  ($location)? $location->getGpsE():null ;
                $oneData['latitude'] = ($location)? $location->getGpsN():null;
                $oneData['addr_name'] = ($location)?$location->getLocationName():null;
                $oneData['addr_addr'] = ($location)?$location->getLocationAddress():null;
                $oneData['addr_floor'] = $station->getAddrFloor();
                $oneData['addr_number'] = $station->getAddrNumber();
                $oneData['am'] = $station->getChargingCurrent();

                $oneData['switch'] = [
                    EvUserStationMapping::SW_ST_AUTO_ONOFF => false,
                    EvUserStationMapping::SW_CH_OFF_PEAK => false
                ];
                $oneData['noti'] = [
                    EvUserStationMapping::IS_NOTI_CHARGING_ST => true,
                    EvUserStationMapping::IS_NOTI_CHARGING_AM => true,
                    EvUserStationMapping::IS_NOTI_CHARGING_FN => true,
                    EvUserStationMapping::IS_ST_AUTO_OFF => true,
                    EvUserStationMapping::IS_ST_AUTO_ON => true,
                    EvUserStationMapping::IS_NOTI_CHARGING_RM => true,
                ];
                $res[] = $oneData;
            }
        }
        $view->setData([
            'code' => 200,
            'data' => $res
        ]);
        $view->getStatusCode(200);
        return $view;
    }
}
