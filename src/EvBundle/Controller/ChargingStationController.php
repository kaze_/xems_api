<?php

namespace EvBundle\Controller;

use AppBundle\Entity\EmsSmartRelayTransactionLog;
use AppBundle\Entity\EvChargingStationMaster;
use EvBundle\Entity\EvUserStationMapping;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Range;
use Exception;

class ChargingStationController extends BaseApiController
{
    public function allAction()
    {
        $logger = $this->get('logger');
        
        $em = $this->get('doctrine')->getManager();
        $mappings = $em->getRepository('EvBundle:EvUserStationMapping')->findBy([
            'user' => $this->getUser()
        ]);
        
        $re = [];
        foreach ($mappings as $mapping){
            $station = $mapping->getChargingStation();
            try {
                if(empty($station)) throw new Exception('');
                $station->getAddrFloor();
            } catch (Exception $ex) {
                $logger->error('Cant find station, '.$ex->getMessage());
                continue;
            }

            $model = $station->getChargingStationModel();
            try {
                if(empty($model)) throw new Exception('');
                $model->getVoltage();
            } catch (Exception $ex) {
                $logger->error('Cant find station model, '.$ex->getMessage());
            }

            $location = $em->getRepository('AppBundle:EmsLocationMaster')->findByChargingStation($station);
            try {
                if ( empty($location)) throw new Exception('');
                $location->getLocationName();
                $location->getLocationAddress();
            } catch (Exception $ex) {
                $logger->error('Cant find location, '.$ex->getMessage());
            }

            $re[] = [
                'id'   => $station->getChargingStationId(), 
                'name' => $station->getName(),
                'status' => $station->getStatusId(),
                'longitude' =>  ($location)? $location->getGpsE():null ,
                'latitude' => ($location)? $location->getGpsN():null,
                'addr_name' => ($location)?$location->getLocationName():null,
                'addr_addr' => ($location)?$location->getLocationAddress():null,
                'addr_floor' => $station->getAddrFloor(),
                'addr_number' => $station->getAddrNumber(),
                'owner' => $mapping->getOwner(),
                'am' => $station->getChargingCurrent(),
                'switch' => [
                    EvUserStationMapping::SW_ST_AUTO_ONOFF => $mapping->getSwitchStationAutoOnOff(),
                    EvUserStationMapping::SW_CH_OFF_PEAK => $mapping->getSwitchOffPeakCharging()
                ],
                'noti' => [
                    EvUserStationMapping::IS_NOTI_CHARGING_ST => $mapping->getIsNotiChargingStart(),
                    EvUserStationMapping::IS_NOTI_CHARGING_AM => $mapping->getIsNotiChargingAlmost(),
                    EvUserStationMapping::IS_NOTI_CHARGING_FN => $mapping->getIsNotiChargingFinish(),
                    EvUserStationMapping::IS_ST_AUTO_OFF => $mapping->getIsNotiStationAutoOff(),
                    EvUserStationMapping::IS_ST_AUTO_ON => $mapping->getIsNotiStationAutoOn(),
                    EvUserStationMapping::IS_NOTI_CHARGING_RM=> $mapping->getIsNotiChargingRemoval(),
                ],
            ];
        }
        
        return $this->returnJsonResponse(
            JsonResponse::HTTP_OK,
            null,
            $re
        );
    }

    public function editSetupAction(Request $request, $chargingStationId = '')
    {
        $validator = Validation::createValidator();
        $errors = $validator->validate($request->request->all(),
            new Collection(array(
                'key' => [new NotNull()],
                'val' => [new NotNull(), new Range(array('min' => 0, 'max' => 1,))],
            ))
        );
        if(isset($errors[0])){
            return $this->returnJsonResponse(
                JsonResponse::HTTP_BAD_REQUEST,
                $errors->get(0)->getPropertyPath().' '.$errors->get(0)->getMessage());
        }

        $em = $this->get('doctrine')->getManager();

        if(empty($chargingStationId)){
            return $this->returnJsonResponse(
                JsonResponse::HTTP_BAD_REQUEST,
                'Require ChargingStationId'
            );
        }
        
        
//        $station = $em->find('AppBundle:EvChargingStationMaster', $chargingStationId);
        $stationmapping = $em->getRepository('EvBundle:EvUserStationMapping')->findOneBy([
            'user' => $this->getUser(),
            'chargingStation' => $chargingStationId,
        ]);

        if(empty($stationmapping)){
            return $this->returnJsonResponse(
                JsonResponse::HTTP_BAD_REQUEST,
                'Cant find Station'
            );
        }

        switch ($request->get('key')){
            case EvUserStationMapping::IS_NOTI_CHARGING_ST:
                $stationmapping->setIsNotiChargingStart($request->get('val'));
                break;
            case EvUserStationMapping::IS_NOTI_CHARGING_AM:
                $stationmapping->setIsNotiChargingAlmost($request->get('val'));
                break;
            case EvUserStationMapping::IS_NOTI_CHARGING_FN:
                $stationmapping->setIsNotiChargingFinish($request->get('val'));
                break;
            case EvUserStationMapping::IS_ST_AUTO_OFF:
                $stationmapping->setIsNotiStationAutoOff($request->get('val'));
                break;
            case EvUserStationMapping::IS_ST_AUTO_ON:
                $stationmapping->setIsNotiStationAutoOn($request->get('val'));
                break;
            case EvUserStationMapping::IS_NOTI_CHARGING_RM:
                $stationmapping->setIsNotiChargingRemoval($request->get('val'));
                break;
            case EvUserStationMapping::SW_ST_AUTO_ONOFF:
                $stationmapping->setSwitchStationAutoOnOff($request->get('val'));
                break;
            case EvUserStationMapping::SW_CH_OFF_PEAK:
                $stationmapping->setSwitchOffPeakCharging($request->get('val'));
                break;
            default:
                return $this->returnJsonResponse(
                    JsonResponse::HTTP_BAD_REQUEST,
                    'key is not exist.'
                );
        }

        $em = $this->getDoctrine()->getManager();

        try {
            $em->persist($stationmapping);
            $em->flush();
            return $this->returnJsonResponse(JsonResponse::HTTP_OK);
        } catch (Exception $ex) {
            return $this->returnJsonResponse(
                JsonResponse::HTTP_BAD_REQUEST,
                'update Setup failed.');
        }
    }

/*    public function getLatestSessionAction(EvChargingStationMaster $station)
    {
        $repo = $this->getDoctrine()->getManager()->getRepository('EvBundle:EvChargingSessionLog');
        $res = $repo->findBy(array('chargingStation' => $station), array('startedAt' => 'DESC'), 1);
        if ( !isset($res[0])) {
            // something is wrong, no session record
            return $this->returnJsonResponse(JsonResponse::HTTP_NOT_FOUND, "Session Data Not Found");
        }
        $data = getSessionRecord($res[0]);
        if ( !$data ) {
            return $this->returnJsonResponse(JsonResponse::HTTP_NOT_FOUND, "Session Data Not Found");
        }
        return $this->returnJsonResponse(JsonResponse::HTTP_OK, null, $data);
    }*/

    public function getStatusAction(Request $request, EvChargingStationMaster $chargingStation)
    {
        // request: brief/detail
        // $detail = $request->get('detail');
        // test status
        $status = $request->get('status');
        if ( $status ) {
            $ret = array('status' => $status);
            if ( $status == 'charging') {
                $repo = $this->getDoctrine()->getManager()->getRepository('EvBundle:EvChargingSessionLog');
                $session = $repo->findNewestSessionByStation($chargingStation);
                if ( $session ) {
                    $sessionId = $session->getSessionId();
                    if ( $sessionId ) {
                        $ret['sessionId'] = $sessionId;
                    }
                }
            }
            return $this->returnJsonResponse(JsonResponse::HTTP_OK, null, $ret);
        }

        $relay = $this->get('app.relay');
        $data = $relay->getStationStatus($chargingStation);
        switch($data['statusId']) {
            case EmsSmartRelayTransactionLog::STAT_ON:
                $data['status'] = 'charging';
                break;
            case EmsSmartRelayTransactionLog::STAT_OFF:
                $data['status'] = 'off';
                break;
            case EmsSmartRelayTransactionLog::STAT_STANDBY:
                $data['status'] = 'standby';
                break;
        }
/*        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('AppBundle:EmsSmartRelayTransactionLog');
        $res = $repo->findBy(array('chargingStation' => $chargingStation ), array('transactionTime' => 'DESC'), 1);
        $status = 'noData';
        $session = null;
        $sessionData = null;

        if ( isset($res[0]) ) {
            switch($res[0]->getSessionStatus())  {
                case "started" :
                    $status = 'charging';
                    $session = $res[0]->getSessionId();
                    break;
                case "powerOff" :
                    $status = 'off';
                    break;
                case "ended" :
                    $status = 'standby';
                    break;
            }
        }

        $data = array('status' => $status);
        if ( $session ) {
            $data['sessionId'] = $session;
        }*/
        return $this->returnJsonResponse(JsonResponse::HTTP_OK, null, $data);
    }

    public function getSessionsAction(Request $request, EvChargingStationMaster $station)
    {
        $view = View::create();
        $context = (new Context())->addGroup('list');

        $from = $request->get('from');
        $to = $request->get('to');
        $limit = $request->get('limit');
        $offset = $request->get('offset');

        $condition = array(
            'station' => $station->getChargingStationId(),
            'from' => $from,
            'to' => $to);
        $ordering = array('startAt' => 'DESC');
        $repo = $this->getDoctrine()->getManager()->getRepository('EvBundle:EvChargingSessionLog');
        $res = $repo->findByStation($condition, $ordering, $limit, $offset);

        $view->setContext($context);
        $view->setStatusCode(200);
        $view->setData(array('code' => 200, 'data' => $res));
        return $view;
        // $res = $repo->findBy(array('chargingStaion' => $station), array('startAt' => 'DESC'),  );
    }


    public function switchOnAction(EvChargingStationMaster $station)
    {
        // $this->denyAccessUnlessGranted('view', $station);
        $circuit = $station->getValidCircuit() ;
        if ( $circuit ) {
            $kaaConn = $this->get('app.kaa');
            $relay = $this->get('app.relay');
            $relay->setStatus($circuit,
                EmsSmartRelayTransactionLog::ACT_METERON,
                EmsSmartRelayTransactionLog::SOURCE_USER,
                $this->getUser()->getId());
            //$result = $kaaConn->sendPowerMeterNotification($circuit, array('powerSwitch' => 'On'));
            $result = $kaaConn->powerSwitch($circuit,'On',[ 'userId' => $this->getUser()->getId()] );
            if ( $result ) {
                return $this->returnJsonResponse(JsonResponse::HTTP_OK);
            }
        }
        return $this->returnJsonResponse(JsonResponse::HTTP_NOT_FOUND, 'device not found');
    }

    public function switchOffAction(EvChargingStationMaster $station)
    {
        //$this->denyAccessUnlessGranted('view', $station);

        $session = $this->getDoctrine()->getManager()
                    ->getRepository('EvBundle:EvChargingSessionLog')
                    ->findNewestSessionByStation($station);

        $logger = $this->get('logger');
        $logger->info("switchOffAction ", [$session]);
        if ( $session != null ) {
            $chargingUser = $session->getUserId();
            if ( $chargingUser ) {
                $chargingUserId = $chargingUser->getId();
            } else {
                return $this->returnJsonResponse(JsonResponse::HTTP_LOCKED, 'station is busy.');
            }
            $userId = $this->getUser()->getId();

            $logger->info("switchOffAction ", [ $userId ]);

            if ( $userId !== $chargingUserId ) {
                return $this->returnJsonResponse(JsonResponse::HTTP_LOCKED, 'station is busy.');
            }
        }

        $circuit = $station->getValidCircuit() ;
        if ( $circuit ) {
            $kaaConn = $this->get('app.kaa');
            $relay = $this->get('app.relay');
            $relay->setStatus($circuit,
                EmsSmartRelayTransactionLog::ACT_METEROFF,
                EmsSmartRelayTransactionLog::SOURCE_USER,
                $this->getUser()->getId());

            //$result = $kaaConn->sendPowerMeterNotification($circuit, array('powerSwitch' => 'Off'));
            $result = $kaaConn->powerSwitch($circuit,'Off',[ 'userId' => $this->getUser()->getId()] );
            if ( $result ) {
                return $this->returnJsonResponse(JsonResponse::HTTP_OK);
            }
        }
        return $this->returnJsonResponse(JsonResponse::HTTP_NOT_FOUND, 'device not found');
    }

}
