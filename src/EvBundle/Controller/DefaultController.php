<?php

namespace EvBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\BrowserKit\Response;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('EvBundle:Default:index.html.twig');
//        return new Response('It\'s work!');
    }
}
