<?php

namespace EvBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\EmsLocationMaster;
use EvBundle\Entity\EvLocationChargingManagement;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Date as ConstrainedDate;
use Symfony\Component\Validator\Constraints\DateTime as ConstraintsDateTime;
use DateTime;
use Doctrine\DBAL\Types\BooleanType;
use Exception;
use Symfony\Component\Validator\Constraints\Time;

class LocationSettingController extends BaseApiController
{
    public function readChargingMetaAction(Request $request, EmsLocationMaster $location)
    {
        $this->denyAccessUnlessGranted('view', $location);

        $em = $this->getDoctrine()->getManager();
        $espCustomers = $location->getEspCustomer();
        
        $energyPackageId = 0;
        $espPackageDetail = null;
        
        if ( $espCustomers && count($espCustomers) != 0 ) {
            $espCustomer = $espCustomers[0];
            $espCustomerRate = $em->getRepository('EspBundle:EspCustomerRate')->findOneBy(['espCustomer' => $espCustomer]);

        // if ( $espCustomerRate ) {
            $normalContract = $espCustomerRate->getNormalContract();
            $semiPeakContract = $espCustomerRate->getSemiPeakContract();
            $saturdaySemiPeakContract = $espCustomerRate->getSaturdaySemiPeakContract();
            $offPeakContract = $espCustomerRate->getOffPeakContract();
            $espPackageDetail = $em->getRepository('EspBundle:EspEnergyPackageDetail')
                ->findOneBy(['energyPackage' => $espCustomerRate->getEnergyPackage()->getEnergyPackageId(), 'statusId' => 1 ]);
            $energyPackageId = $espCustomerRate->getEnergyPackage()->getEnergyPackageId();
        }

        $evChargingSetting = $em->getRepository('EvBundle:EvLocationChargingManagement')
            ->findOneBy(['emsLocationId' => $location->getLocationId()]);
    
        if ( $evChargingSetting == null ) {
            $evChargingSetting = new EvLocationChargingManagement();
            
            $evChargingSetting->setEmsLocationId($location->getLocationId())
                ->setOptimizedNormalContract($normalContract)
                ->setOptimizedSemiContract($semiPeakContract)
                ->setOptimizedSaturdaySemiContract($saturdaySemiPeakContract)
                ->setOptimizedOffPeakContract($offPeakContract)
                ->setEvNormalContract(0)
                ->setEvSemiContract(0)
                ->setEvSaturdaySemiContract(0)
                ->setEvOffPeakContract(0);
            $em->persist($evChargingSetting);
            $em->flush();
        }

        if ( $espPackageDetail && $espPackageDetail->getHasContract() ) { 
            $data = array(
                'package_id' => $energyPackageId,
                'has_contract'=> $espPackageDetail->getHasContract(),
                'normal_contract'=> $normalContract,
                'semi_peak_contract'=> $semiPeakContract,
                'saturday_semi_peak_contract'=> $saturdaySemiPeakContract,
                'off_peak_contract'=> $offPeakContract,
                'optimized_normal_contract'=> $evChargingSetting->getOptimizedNormalContract(),
                'optimized_semi_peak_contract'=> $evChargingSetting->getOptimizedSemiContract(),
                'optimized_saturday_semi_peak_contract'=> $evChargingSetting->getOptimizedSaturdaySemiContract(),
                'optimized_off_peak_contract'=> $evChargingSetting->getOptimizedOffPeakContract(),
            );
        } else {
            $data = array(
                'package_id' => $energyPackageId,
                'has_contract'=>'0',           
            );
        }

        return $this->returnJsonResponse(
            JsonResponse::HTTP_OK, 
            null,
            $data
        );
    }

    public function readChargingAction(Request $request, EmsLocationMaster $location)
    {

        $this->denyAccessUnlessGranted('view', $location);

        $locationSettingService = $this->get('ev.location_setting');
        $setting = $locationSettingService->getEvChargingSetting($location);
        $chargingRate = $locationSettingService->getCurrentEvChargingRate($location);
        $scheduledRate = $locationSettingService->getScheduledEvChargingRate($location);
        $data = [];
        if ( $setting ) {
            foreach($setting as $key => $value ) {
                $data[$key] = $value;
            }
        }

        if ( $chargingRate ) {
            $data['bill_way'] = $chargingRate['rate_type'];
            if ( isset($chargingRate['rate_parameter'])) {
                $param = $chargingRate['rate_parameter'];
                if ( is_array($param) ) {
                    if ( isset($param['peak'])) $data['static_billing_peak'] = $param['peak'];
                    if ( isset($param['semi_peak'])) $data['static_billing_semi_peak'] = $param['semi_peak']; 
                    if ( isset($param['saturday_semi_peak'])) $data['static_billing_saturday_semi_peak'] = $param['saturday_semi_peak'];
                    if ( isset($param['off_peak'])) $data['static_billing_off_peak'] = $param['off_peak'];
                    if ( isset($param['guest_peak'])) $data['static_billing_guest_peak'] = $param['guest_peak'];
                    if ( isset($param['guest_semi_peak'])) $data['static_billing_guest_semi_peak'] = $param['guest_semi_peak']; 
                    if ( isset($param['guest_saturday_semi_peak'])) $data['static_billing_guest_saturday_semi_peak'] = $param['guest_saturday_semi_peak'];
                    if ( isset($param['guest_off_peak'])) $data['static_billing_guest_off_peak'] = $param['guest_off_peak'];
                    if ( isset($param['off_peak_bonus'])) $data['off_peak_bonus'] = $param['off_peak_bonus'];
                } else {
                    $data['bill_way_number_'.$chargingRate['rate_type']] = $param;
                }
            }
        }
        if ( $scheduledRate ) {
            $data['bill_setting_take_effect_date'] = $scheduledRate['effective_at']->format('Y-m-d');
            $data['scheduled_bill_way'] = $scheduledRate['rate_type'];
            if ( isset($scheduledRate['rate_parameter'])) {
                $param = $scheduledRate['rate_parameter'];
                if ( is_array($param) ) {
                    if ( isset($param['peak'])) $data['scheduled_static_billing_peak'] = $param['peak'];
                    if ( isset($param['semi_peak'])) $data['scheduled_static_billing_semi_peak'] = $param['semi_peak']; 
                    if ( isset($param['saturday_semi_peak'])) $data['scheduled_static_billing_saturday_semi_peak'] = $param['saturday_semi_peak'];
                    if ( isset($param['off_peak'])) $data['scheduled_static_billing_off_peak'] = $param['off_peak'];
                    if ( isset($param['guest_peak'])) $data['scheduled_static_billing_guest_peak'] = $param['guest_peak'];
                    if ( isset($param['guest_semi_peak'])) $data['scheduled_static_billing_guest_semi_peak'] = $param['guest_semi_peak']; 
                    if ( isset($param['guest_saturday_semi_peak'])) $data['scheduled_static_billing_guest_saturday_semi_peak'] = $param['guest_saturday_semi_peak'];
                    if ( isset($param['guest_off_peak'])) $data['scheduled_static_billing_guest_off_peak'] = $param['guest_off_peak'];
                    if ( isset($param['off_peak_bonus'])) $data['scheduled_off_peak_bonus'] = $param['off_peak_bonus'];
                } else {
                    $data['scheduled_bill_way_number_'.$scheduledRate['rate_type']] = $param;
                 }
            }
        }

        return $this->returnJsonResponse(
            JsonResponse::HTTP_OK, 
            null, $data
        );
    }

    public function setChargingAction(Request $request, EmsLocationMaster $location)
    {
        $em = $this->getDoctrine()->getManager();
        $this->denyAccessUnlessGranted('edit', $location);

        $validator = Validation::createValidator();
        $constraints = array(
            'ev_shared_normal_contract' => [new Type(['type' => 'numeric'])],
            'ev_shared_semi_peak_contract' => [new Type(['type' => 'numeric'])],
            'ev_shared_saturday_semi_peak_contract' => [new Type(['type' => 'numeric'])],
            'ev_shared_off_peak_contract' => [new Type(['type' => 'numeric'])],
            'charging_way' => [new NotNull(), new NotBlank()],
            'bill_way' => [new NotNull(), new NotBlank()],
            'bill_setting_take_effect_date' => [new ConstraintsDateTime()],
            'off_peak_bonus' => [new Type('string')]
        );
        $off_peak_bonus = $request->get('off_peak_bonus');
        if ($off_peak_bonus == 'true') {
            $constraints['off_peak_bonus_time'] = [new Time()];
            $constraints['off_peak_bonus_time_end'] = [new Time()];
            $constraints['off_peak_bonus_kwh'] = [new Type(['type' => 'numeric'])];
        }
        $rateType = $request->get('bill_way');
        switch($rateType) {
            case 3: 
                $constraints['bill_way_number_3'] = [new Type(['type' => 'numeric'])];
                // $constraints['bill_way_number_3_guest'] = [new Type(['type' => 'numeric'])];
            break;
            case 4:
                $constraints['static_billing_peak'] = [new Type(['type' => 'numeric'])];
                $constraints['static_billing_semi_peak'] = [new Type(['type' => 'numeric'])];
                $constraints['static_billing_saturday_semi_peak'] = [new Type(['type' => 'numeric'])];
                $constraints['static_billing_off_peak'] = [new Type(['type' => 'numeric'])];
                $constraints['static_billing_guest_peak'] = [new Type(['type' => 'numeric'])];
                $constraints['static_billing_guest_semi_peak'] = [new Type(['type' => 'numeric'])];
                $constraints['static_billing_guest_saturday_semi_peak'] = [new Type(['type' => 'numeric'])];
                $constraints['static_billing_guest_off_peak'] = [new Type(['type' => 'numeric'])];
            break;
            case 5:
                $constraints['bill_way_number_5'] = [new Type(['type' => 'numeric'])];
            break;
            default:
        }

        $errors = $validator->validate($request->request->all(), new Collection($constraints)); 

        if(isset($errors[0])){
            return new JsonResponse([
                'code' => JsonResponse::HTTP_BAD_REQUEST,
                'msg' => $errors->get(0)->getPropertyPath().' '.$errors->get(0)->getMessage()
            ], JsonResponse::HTTP_BAD_REQUEST);
        }

        // update static parameter(ev sharing contract, charging strategy)
        $ev_location_charging_management_repo = $em->getRepository('EvBundle:EvLocationChargingManagement');
        $the_ev_location_charging_management=$ev_location_charging_management_repo->findOneBy(['emsLocationId' => $location->getLocationId()]);

        $the_ev_location_charging_management->setEvNormalContract($request->get('ev_shared_normal_contract'));
        $the_ev_location_charging_management->setEvSaturdaySemiContract($request->get('ev_shared_semi_peak_contract'));
        $the_ev_location_charging_management->setEvOffPeakContract($request->get('ev_shared_saturday_semi_peak_contract'));
        $the_ev_location_charging_management->setEvChargingStrategyId($request->get('ev_shared_off_peak_contract'));
        $the_ev_location_charging_management->setEvChargingStrategyId($request->get('charging_way'));

        try {
            $em->flush();
        } catch (Exception $ex) {
            return $this->returnJsonResponse(
                JsonResponse::HTTP_BAD_REQUEST,
                'Update Ev Location Charging Management failed'.$ex->getMessage()
            );
        }
        // update charging fee options
        $rateType = $request->get('bill_way');
        $parameter = null;
        switch($rateType) {
            case 3:
                $parameter = $request->get("bill_way_number_3");
            break;
            case 4:
                $parameter = [];
                $peak = $request->get("static_billing_peak");
                $semi_peak = $request->get("static_billing_semi_peak");
                $saturday_semi_peak = $request->get("static_billing_saturday_semi_peak");
                $off_peak = $request->get("static_billing_off_peak");
                $guest_peak = $request->get("static_billing_guest_peak");
                $guest_semi_peak = $request->get("static_billing_guest_semi_peak");
                $guest_saturday_semi_peak = $request->get("static_billing_guest_saturday_semi_peak");
                $guest_off_peak = $request->get("static_billing_guest_off_peak");
                if ( $peak !== null ) $parameter['peak'] = $peak;
                if ( $semi_peak !== null ) $parameter['semi_peak'] = $semi_peak;
                if ( $saturday_semi_peak !== null ) $parameter['saturday_semi_peak'] = $saturday_semi_peak;
                if ( $off_peak !== null ) $parameter['off_peak'] = $off_peak;
                if ( $guest_peak !== null ) $parameter['guest_peak'] = $guest_peak;
                if ( $guest_semi_peak !== null ) $parameter['guest_semi_peak'] = $guest_semi_peak;
                if ( $guest_saturday_semi_peak !== null ) $parameter['guest_saturday_semi_peak'] = $guest_saturday_semi_peak;
                if ( $guest_off_peak !== null ) $parameter['guest_off_peak'] = $guest_off_peak;

                // off_peak_bonus - 跨越尖峰 => 離峰時間點要加成
                $off_peak_bonus = $request->get("off_peak_bonus");
                if ($off_peak_bonus == 'true') {
                    $off_peak_bonus = [];
                    $off_peak_bunus_time = $request->get("off_peak_bonus_time");
                    $off_peak_bunus_time_end = $request->get("off_peak_bonus_time_end");
                    $off_peak_bonus_kwh = $request->get("off_peak_bonus_kwh");
                    if ( $off_peak_bunus_time != null ) $off_peak_bonus['time'] = $off_peak_bunus_time;
                    if ( $off_peak_bunus_time_end != null ) $off_peak_bonus['time_end'] = $off_peak_bunus_time_end;
                    if ( $off_peak_bonus_kwh != null ) $off_peak_bonus['kwh'] = $off_peak_bonus_kwh;
                    $parameter['off_peak_bonus'] = $off_peak_bonus;
                }
            break;
            case 5:
                $parameter = $request->get("bill_way_number_5");
            break;
        }
        $locationSettingService = $this->get('ev.location_setting');
        $effectiveDate = DateTime::createFromFormat('Y-m-d', $request->get('bill_setting_take_effect_date'));
        if ( $effectiveDate == null ) {
            $effectiveDate = DateTime::createFromFormat('Y-m-d H:i:s', $request->get('bill_setting_take_effect_date'));
        }
        $result = $locationSettingService->setScheduledEvChargingRate($location, $rateType, $parameter, $effectiveDate);
        $msg = "";
        if ( $result[0] == false ) {
            $msg = $result[1];
        } 
        return $this->returnJsonResponse(
            JsonResponse::HTTP_OK, 
            $msg
        );
    }

}