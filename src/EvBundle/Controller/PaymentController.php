<?php

namespace EvBundle\Controller;

use AppBundle\Entity\EmsLocationMaster;
use DateTime;
use EvBundle\Entity\CreditCard;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\Validator\Validation;
use EvBundle\Entity\EvUserDetail;
use function var_dump;

class PaymentController extends BaseApiController
{
    public function addCreditAction(Request $request)
    {
        //verify field
        $validator = Validation::createValidator();
        $errors = $validator->validate($request->request->all(),
            new Collection(array(
                'cardno' => [new NotNull(), new Length(array('max' => 16))],
                'exp_y' => [new NotNull(), new Length(array('min' => 2, 'max' => 2)), new Range(array('min' => 18, 'max' => 99))],  //2018~2099
                'exp_m' => [new NotNull(), new Length(array('min' => 2, 'max' => 2)), new Range(array('min' => 1, 'max' => 12))],    //01~12
                'cvc' => [new NotNull(), new Length(array('max' => 4))],
                'is_ae' => [],

            ))
        );

        if(isset($errors[0])){
            return $this->returnJsonResponse(
                JsonResponse::HTTP_BAD_REQUEST,
                $errors->get(0)->getPropertyPath().' '.$errors->get(0)->getMessage()
            );
        }

        //new Object
        $creditCard = new CreditCard();
        $creditCard->setCardno($request->get('cardno'));
        $creditCard->setUserId($this->getUser()->getId()->toString());
        $creditCard->setIsAe(intval($request->get('is_ae')));
        $creditCard->setExpY($request->get('exp_y'));
        $creditCard->setExpM($request->get('exp_m'));
        $creditCard->setCvc($request->get('cvc'));
        
        
        //soft delete current CreditCard
        $em = $this->getDoctrine()->getManager();
		
        $detail = $this->getDoctrine()->getRepository(EvUserDetail::class)->find($this->getUser()->getId()->toString());
		 if( $detail===NULL){
			 return $this->returnJsonResponse(
                JsonResponse::HTTP_BAD_REQUEST,
                'You need set profile first.'
            );
		}

        $repo = $em->getRepository('EvBundle:PayPay2goToken');
        $oldCard = $repo->findOneBy(['userId' => $this->getUser()->getId()->toString(), 'status' => 1]);
        
        //add and verify
        $credit = $this->get('credit');
        $isPublish = $credit->add($this->getUser(), $creditCard);
        if ( $isPublish == true && $oldCard != null ) {
            $credit->delete($oldCard->getPaymentId());
        }
        return $this->returnJsonResponse($isPublish ? JsonResponse::HTTP_OK : JsonResponse::HTTP_BAD_REQUEST);
    }

    public function checkCreditAction()
    {
        $credit = $this->get('credit');
        $status = $credit->check($this->getUser());
        
        if ( $status == null ) {
            return $this->returnJsonResponse(401, 'Cant find current Credit card.');
        }
        
        try {
            switch ($status){
                case CreditCard::STATUS_OK:
                    $msg = 'OK';
                    break;
                case CreditCard::STATUS_VERIFY_FAILED:
                    $msg = 'verify failed';
                    break;
                case CreditCard::STATUS_UNVERIFIED:
                    $msg = 'unverified';
                    break;
                default:
                    $msg = 'delete';
                    break;
            }
            return $this->returnJsonResponse(JsonResponse::HTTP_OK, $msg, ['status' => $status]);
            
        } catch (Exception $ex) {
            return $this->returnJsonResponse(JsonResponse::HTTP_BAD_REQUEST, 'This user havnt credit card.');
        }
    }

    public function deleteCreditAction()
    {
        try {
            $em = $this->get('doctrine')->getManager();
            $card = $this->getUser()->getDetail()->getCreditCard();
            $detail = $em->find('EvBundle:EvUserDetail', $this->getUser()->getId()->toString());
            if( empty($card)) {
                return $this->returnJsonResponse(401, 'Cant find current Credit card.');
            }
            
            $card->setStatus(CreditCard::STATUS_DELETE);
            $em->persist($card);
            $em->flush();

            //clear UserDetail CreditCard field
            $detail->setCreditCard();
            $em->persist($detail);
            $em->flush();
            
            return $this->returnJsonResponse(JsonResponse::HTTP_OK);
        } catch (Exception $ex) {
            
        }

        return $this->returnJsonResponse(JsonResponse::HTTP_BAD_REQUEST, 'delete failed.');
    }
    
    public function testCreditAction()
    {
        $credit = $this->get('credit');
        $credit->placeOrder($this->getUser(), 2, 'test order');
        return $this->returnJsonResponse(JsonResponse::HTTP_OK);
    }

    public function getUserPointsAction(Request $request)
    {
        $user = $this->getUser();
        $userId = $user->getId();


        $payPointService = $this->get('pay.points');
        $payPoints = $payPointService->getUserPointStatus($userId);
        
        $data = [];

        $em = $this->getDoctrine()->getManager();
        foreach($payPoints as $payPoint) {
            $oneData = [];
            $locationId = $payPoint->getLocationId(); 
            $locationName = "";
            if ( $locationId ) {
                $location = $em->find('AppBundle:EmsLocationMaster', $locationId);
                if ( $location && $location->getLocationStatus() == 1  ) {
                    $locationName = $location->getLocationName();
                }
            } else {
                continue;
            }
            $oneData['locationName'] = $locationName;
            $oneData['locationId'] = $locationId;
            $oneData['points'] = $payPoint->getPoints();
            $oneData['updatedAt'] = $payPoint->getupdatedAt()->format('Y/m/d H:i');
            $data[] = $oneData;
        }

        return $this->returnJsonResponse(
            JsonResponse::HTTP_OK,  
            null, 
            $data
        );
    }

    public function getUserLocationPointsLogsAction(Request $Request, EmsLocationMaster $location, $yearMonth)
    {
        $user = $this->getUser();
        $userId = $user->getId();
        $locationId = $location->getLocationId();

        $date = \DateTime::createFromFormat('Y-m-d', $yearMonth.'-01')->setTime(0,0,0);
        $end = clone($date);
        $end->modify('first day of next month');
        
        // get pay points logs
        $payPointService = $this->get('pay.points');
        $payPoints = $payPointService->getLocationUserPointsLogs($locationId, $userId, $date, $end);
        $em = $this->getDoctrine()->getManager();
        $results = [];
        if ( $payPoints ) {
            foreach($payPoints as $payPoint) {
                $action = $payPoint->getAction();
                // get Points
                $points = $payPoint->getPoints();
                if ( $points == 0 ) {
                    continue;
                }
                $pointsLeft = $payPoint->getPointsLeft();

                $chargingStationName = null;
                if ( $action == 'CHARGING' ) {
                    $points = -$points;
                    // get charging station name
                    $sessionId = $payPoint->getSessionId();
                    if ( $sessionId ) {
                        $session = $em->find('EvBundle:EvChargingSessionLog', $sessionId);
                        if ($session) {
                            $chargingStation = $session->getChargingStation();
                            if ( $chargingStation ) {
                                $chargingStationName = $chargingStation->getName();
                            }
                        }
                    }
                }
                $oneResult = [
                    'action' => $action,
                    'points' => $points,
                    'pointsLeft' => $pointsLeft,
                    'datetime' => $payPoint->getLoggedAt()->format('Y/m/d h:i')
                ];
                if ( $chargingStationName ) {
                    $oneResult['chargingStationName'] = $chargingStationName;
                }
                // get kwh
                $comment = $payPoint->getComment();
                if ($comment && strlen($comment) != 0) {
                    $commentObj = json_decode($comment, true);
                    if ( $commentObj ) {
                        if ( isset($commentObj["totalKwh"]) ) {
                            $oneResult['kwh'] = $commentObj["totalKwh"];
                        } 
                        if ( isset($commentObj['total_kwh'])) {
                            $oneResult['kwh'] = $commentObj["total_kwh"];
                        }
                    }
                } 
                $results[] = $oneResult;
            }
        }
        // end of getting paypoint logs
        // get month list
        $locationUser = $em->getRepository('EvBundle:EvUserLocationMapping')->findOneBy(['userId' => $userId, 'locationId' => $locationId]);
        $yearMonthList = [];
        if ( $locationUser ) {
            $date = $locationUser->getCreatedAt();
            if ( $date ) {
                $now = new \DateTime();
                $date->modify('first day of this month')->setTime(0,0,0);
                while ($date < $now ) {
                    $yearMonthList[] = $date->format('Y-m');
                    $date->modify('+1 month');
                }
            }
        } 
        return $this->returnJsonResponse(
            JsonResponse::HTTP_OK,  
            null, 
            [
                'payment' => $results,
                'monthList' => array_reverse($yearMonthList)
            ]
        );
    }
}
