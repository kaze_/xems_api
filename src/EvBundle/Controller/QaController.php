<?php

namespace EvBundle\Controller;

use Exception;
use SendGrid\Mail\Mail;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Validation;
use FOS\RestBundle\View\View;

class QaController extends BaseApiController
{
    public function contactAction(Request $request)
    {
        //verify field
        $validator = Validation::createValidator();
        $errors = $validator->validate($request->request->all(),
            new Collection(array(
                'title' => [new NotNull()],
                'content' => [new NotNull()]
            ))
        );

        if(isset($errors[0])){
            return $this->returnJsonResponse(
                JsonResponse::HTTP_BAD_REQUEST,
                $errors->get(0)->getPropertyPath().' '.$errors->get(0)->getMessage()
            );
        }


        try {
	        $user = $this->getUser();
            $userEmail = $user->getEmail();
	        $userAccount = $user->getUsername(); // for EV User, Account = mobile phone;
	        if (empty($userEmail)) throw new Exception('No email');
        } catch (Exception $ex) {
            return $this->returnJsonResponse(
                301,
                'need Email'
            );
        }
        
        $title = htmlspecialchars(strip_tags($request->get('title')));
        $content = 'From: '.$userEmail.PHP_EOL;
        $content = 'Account: '.$userAccount.PHP_EOL.PHP_EOL;
	$content .= htmlspecialchars(strip_tags($request->get('content')));
        
        try {
            /*
            $message = \Swift_Message::newInstance()
                ->setSubject('[電動車問題回報] '.$title)
                ->setFrom($userEmail)
                ->setTo($this->getParameter('admin_email'))
                ->setBody($content);
            $this->get('mailer')->send($message);
            */ 
            // use sendgrid instead
            $email = new Mail();
            $email->setFrom($userEmail);
            $email->setSubject("[App 充電問題回報] ".$title);
            $email->addTo($this->getParameter('admin_email'));
            $email->addContent("text/plain", $content);
            $sendgrid = new \SendGrid($this->getParameter('sendgrid_api_key'));
            $response = $sendgrid->send($email);
            $logger = $this->get('logger');
            $logger->info($response->statusCode(), $response->headers(), $response->body());
        } catch (Exception $ex) {
            $logger = $this->get('logger');
            $logger->error('[Email] Send failed.'.$ex->getMessage());
            
            return $this->returnJsonResponse(
                JsonResponse::HTTP_BAD_REQUEST,
                '[Email] send failed, from='.$userEmail.', title='.$title
            );            
        }
        return $this->returnJsonResponse(
            JsonResponse::HTTP_OK
        );
    }
    
    public function allAction(Request $request)
    {
        $view = View::create();

        $faqServ = $this->get('ev.FAQ');
        $faq = $faqServ->getFAQData();

        $view->setStatusCode(JsonResponse::HTTP_OK);
        $view->setData([
            'code' => JsonResponse::HTTP_OK, 
            'version' => $faq['version'],
            'data' => [
                "list" => $faq['data']
            ]
        ]);
        return $view;
    }
}
