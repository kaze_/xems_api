<?php

namespace EvBundle\Controller;

use AppBundle\Entity\LoginToken;
use EvBundle\Validator\Constraints\TaiwanPhone;
use Exception;
use FOS\UserBundle\Controller\SecurityController as BaseController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Validation;
use EvBundle\Entity\EvUserDetail;
use function var_dump;

class SecurityController extends BaseApiController
{

    public function isTestPhoneNumber($phone) {
        if ( $phone === '0987654321') {
            return true;
        }
        if ( substr($phone, 0, 7) === '0999999') {
            return true;
        }
        return false;
    }

    public function sendSMSAction(Request $request)
    {
        if ( $this->isTestPhoneNumber($request->get('phone')) ) {
            return $this->_re(JsonResponse::HTTP_OK); 
        }
        
        $validator = Validation::createValidator();
        $errors = $validator->validate($request->request->all(), 
            new Collection(array(
                'phone' => [new NotNull(), new TaiwanPhone()],
            ))
        );
        if(isset($errors[0])){
            return $this->_re(
                JsonResponse::HTTP_BAD_REQUEST,
                $errors->get(0)->getPropertyPath().' '.$errors->get(0)->getMessage());
        }
        
        $sms = $this->get('sms');
        if( ! $sms->addVerify($request->get('phone'), 28)){
            return $this->_re(
                JsonResponse::HTTP_BAD_REQUEST,
                $sms->error());
        }
        
        return $this->_re(JsonResponse::HTTP_OK);
    }
    
    public function smsLoginAction(Request $request)
    {
        if ( !($this->isTestPhoneNumber($request->get('phone')) && ($request->get('pincode') === '111111') )) {
            $validator = Validation::createValidator();
            $errors = $validator->validate($request->request->all(),
                new Collection(array(
                    'phone' => [new NotNull(), new TaiwanPhone()],
                    'device_id' => [new NotNull()],
                    'pincode' => [new NotNull(), new Length(array('min' => 6, 'max' => 6)),]
                ))
            );
            if(isset($errors[0])){
                return $this->_re(
                    JsonResponse::HTTP_BAD_REQUEST,
                    $errors->get(0)->getPropertyPath().' '.$errors->get(0)->getMessage());
            }
            
            $sms = $this->get('sms');
            if (!$sms->validation($request->get('phone'), $request->get('pincode'), 5)) {
                return $this->_re(
                    JsonResponse::HTTP_BAD_REQUEST,
                    $sms->error());
            }
        }

        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->findUserByUsername($request->get('phone'));
        if ( $user == null) {

            $user = $userManager->createUser()
                ->setEnabled(true)
                ->setUsername($request->get('phone'))
            ;

            $user
            ->addRole('ROLE_EV_USER');
            $userManager->updateUser($user);

            $user = $userManager->findUserByUsername($request->get('phone'));
            if($user == null){
                return $this->_re(
                    JsonResponse::HTTP_BAD_REQUEST,
                    'Register user failed.');
            }
        }

        $token = $this->_getToken($user, $request->get('device_id'));

        $noProfile = true;
        $em = $this->getDoctrine()->getManager();
        $detail = $em->getRepository(EvUserDetail::class)->find($user->getId());
        if( $detail === NULL ) {
            $detail = new EvUserDetail();
            $userId = $user->getId();
            $detail->setUserId($userId);
            $detail->setCellPhone($request->get('phone'));
            $em->persist($detail);
            $em->flush();
        } else if (!empty($detail->getName()) && !empty($user->getEmail())) {
            $noProfile = false;
        }  
        
        $noCreditCard = true;
        $em = $this->getDoctrine()->getManager();
        
        $repo = $em->getRepository('EvBundle:PayPay2goToken');
        $credit = $repo->findOneBy(['userId' => $user->getId()->toString(), 'status' => 1] ) ;
        if ( $credit != null ) { 
            $noCreditCard = false;
        }
        
        return $this->_re(
            JsonResponse::HTTP_OK,
            null,
            [
                'token' => $token->getSessionKey(),
                'user' => [
                    'id' => $user->getId(),
                ],
                'no_profile' => $noProfile,
                'no_creditcard' => $noCreditCard,
            ]
        );
    }


    private function _getToken($user, $deviceId = '')
    {
        $em = $this->getDoctrine()->getManager();
        $tokenRepo = $em->getRepository('AppBundle:LoginToken');
        $token = $tokenRepo->findOneBy(array('user' => $user,));
        
        if (!$token) {
            $token = new LoginToken();
            $token->setUser($user);
        }
        
        $token->setDeviceId($deviceId);

        $token->setSessionKey(bin2hex(openssl_random_pseudo_bytes(32)))
            ->setIsEnabled(true)
            ->setUpdated();

        $em->persist($token);
        $em->flush();
        return $token;
    }
    
    private function _re($code, $msg = null, $data = null)
    {
        $return = array();
        $return['code'] = $code;
        $httpcode = $code;
        if($data !== null){
            $return['data'] = $data;
        }
        if($msg !== null){
            $return['msg'] = $msg;
        }
        return new JsonResponse($return, $httpcode);
    }
}
