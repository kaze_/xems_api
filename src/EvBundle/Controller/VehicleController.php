<?php

namespace EvBundle\Controller;

use EvBundle\Entity\EvUserOwnVehicle;
use EvBundle\Entity\EvVehicleGpsLog;
use EvBundle\Validator\Constraints\TaiwanCarNumber;

use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use \Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\Validation;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Context\Context;

class VehicleController extends BaseApiController
{
    public function allAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $status=$request->get('charging_status');
        $list = [];
        try {
            /*
            $vehicles = $em->getRepository('EvBundle:EvUserOwnVehicle')->findBy([
                'user' => $this->getUser(),
            ]);
            */
            // move vehicle operation to vehicle service
            $vehicleService = $this->get('ev.vehicle');
            $vehicles = $vehicleService->getVehiclesByUser($this->getuser());
            $activeEv = [];
            if ( $status == 'all') {
                $sessService = $this->get('ev.session');
                $sessions = $sessService->getActiveSessionByUser($this->getUser());
                foreach($sessions as $session) {
                    if ( $session->getEvPlate() != null ) {
                        $activeEv[strtoupper(str_replace('-', '', $session->getEvPlate()))] = [
                            'sessionId' => $session->getSessionId(), 
                            'chargingStationName' => $session->getChargingStation()->getName()
                        ];
                    }
                }
            }
            if( ! empty($vehicles)){
                // $calculators = $this->get('ev.calculator');
                $metadata = $this->get('ev.metadata');
                $list = [];
                foreach ($vehicles as $vehicle) {
                    // $manufactor = $vehicle->getEvManufactorMaster()->getEvManufactorId();
                    // $battery = $vehicle->getEvBattery()->getEvBatteryId();
                    $model = $metadata->getModelDataById($vehicle->getEvModelId());
                    $evPlate = $vehicle->getEvPlate();
                    $modifiedEvPlate = strtoupper(str_replace('-', '', $evPlate));
                    $oneList = [
                        'plate' => $evPlate,
                        'model' => $model->getEvModelId(),
                        'mileage' => $model->getMileage(),
                        //'mileage' => $calculators->getMileage(1, $model, $battery)
                    ];
                    if ( $status == 'all') {
                        if ( $evPlate && isset($activeEv[$modifiedEvPlate])) {
                            $evSession = $activeEv[$modifiedEvPlate];
                            $charging = true;
                            $chargingStationName = $evSession['chargingStationName'];
                            $sessionId = $evSession['sessionId'];
                        } else {
                            $charging = false;
                            $chargingStationName = '';
                            $sessionId = '';
                        }
                        $oneList['charging'] = $charging;
                        $oneList['charging_station_name'] = $chargingStationName;
                        $oneList['sessionId'] = $sessionId;                        
                    }
                    $list[] = $oneList;
                }
            }
        } catch (Exception $ex) {
            echo $ex->getMessage();
            //echo $ex->getTraceAsString();
            return $this->returnJsonResponse(
                JsonResponse::HTTP_BAD_REQUEST,
                'add car failed.'
            );
        }
        
        return $this->returnJsonResponse(
            JsonResponse::HTTP_OK,
            'ok',
            ['list' => $list]
        );
        
    }
    
    public function newAction(Request $request)
    {
        //verify field
        $validator = Validation::createValidator();
        $errors = $validator->validate($request->request->all(),
            new Collection(array(
            //    'manufactor' => [new NotNull(), new Type('numeric')],
                'plate' => [new NotNull(), new TaiwanCarNumber()],
                'model' => [new NotNull(), new Type('string')],
            //    'battery' => [new NotNull(), new Type('numeric')],
            ))
        );

        if(isset($errors[0])){
            return $this->returnJsonResponse(
                JsonResponse::HTTP_BAD_REQUEST,
                $errors->get(0)->getPropertyPath().' '.$errors->get(0)->getMessage()
            );
        }
        
        $em = $this->getDoctrine()->getManager();
        
        
        /*
        $vehicleMetadata = $this->get('ev.metadata');
        $model = $vehicleMetadata->getModelDataByModelId($request->get('model'));
        
        if($model == null ){
            return $this->returnJsonResponse(
                JsonResponse::HTTP_BAD_REQUEST,
                'This model id is not exist.'
            );            
        }
        
        /*
        $vehicle = new EvUserOwnVehicle();
        $vehicle->setUser($this->getUser());
        $vehicle->setEvPlate($request->get('plate'));
        $vehicle->setEvModelId($model->getId());
        $vehicle->setUpdatedAt(new \DateTime());

        try {
            $em->persist($vehicle);
            $em->flush();
        } catch (Exception $ex) {
            if(strpos($ex->getMessage(), 'Duplicate entry') !== false){
                return $this->returnJsonResponse(
                    301,
                    'Plate is exist.');
            }
            $logger = $this->get('logger');
            $logger->error($ex->getMessage());
            return $this->returnJsonResponse(
                JsonResponse::HTTP_BAD_REQUEST,
                'add car failed.'.$ex->getMessage()
            );
        }
        */

        // move vehicle operation to vehicle service
        $vehicleService = $this->get('ev.vehicle');
        $rtn = $vehicleService->addVehicle($this->getUser(), $request->get('plate'), $request->get('model'));

        if ( is_array($rtn) && $rtn["result"] == false) {
            $errMessage = isset($rtn["message"])? $rtn["message"]: "";
            if ( strpos($errMessage, 'Duplicate entry') != false) {
                return $this->returnJsonResponse(
                    301,
                    'Plate is exist.');               
            }
            $logger = $this->get('logger');
            $logger->error($errMessage);
            return $this->returnJsonResponse(
                JsonResponse::HTTP_BAD_REQUEST,
                'add car failed.'.$errMessage
            );
        }

        return $this->returnJsonResponse(
            JsonResponse::HTTP_OK,
            'ok'
        );
    }
    
    public function editAction(Request $request, $evPlate = '')
    {
        //verify field
        $validator = Validation::createValidator();
        $errors = $validator->validate($request->request->all(),
            new Collection(array(
//                'plate' => [new NotNull(), new TaiwanCarNumber()],
                'model' => [new NotNull(), new Type('string')],
            ))
        );

        if(isset($errors[0])){
            return $this->returnJsonResponse(
                JsonResponse::HTTP_BAD_REQUEST,
                $errors->get(0)->getPropertyPath().' '.$errors->get(0)->getMessage()
            );
        }

        $vehicleService = $this->get('ev.vehicle');
        $rtn = $vehicleService->editVehicle($this->getUser(), $evPlate, $request->get('model'));

        if ( $rtn["result"] == false) {
            $errMessage = isset($rtn["message"])? $rtn["message"]: "";
            if ( strpos($errMessage, 'Duplicate entry') != false) {
                return $this->returnJsonResponse(
                    301,
                    'Plate is exist.');               
            }
            $logger = $this->get('logger');
            $logger->error($errMessage);
            return $this->returnJsonResponse(
                JsonResponse::HTTP_BAD_REQUEST,
                'edit car failed.'.$errMessage
            );
        }
        /*
        $em = $this->getDoctrine()->getManager();
        $evPlateCanonical = EvUserOwnVehicle::canonicalizeEvPlate($evPlate);
        $vehicle = $em->getRepository('EvBundle:EvUserOwnVehicle')->findOneBy([
            'user' => $this->getUser(),
            'evPlateCanonical' => $evPlateCanonical
        ]);
        try {
            if(empty($vehicle)) throw new Exception('');
            $vehicle->getEvPlate();
        } catch (Exception $ex) {
            return $this->returnJsonResponse(
                JsonResponse::HTTP_BAD_REQUEST,
                'This car doesnt exist.'
            );
        }

        $vehicleMetadata = $this->get('ev.metadata');
        $model = $vehicleMetadata->getModelDataByModelId($request->get('model'));
        if( $model == null ) {
            return $this->returnJsonResponse(
                JsonResponse::HTTP_BAD_REQUEST,
                'This model id is not exist.'
            );
        }
        
        $vehicle->setEvModelId($model->getId());
        $vehicle->setUpdatedAt(new \DateTime());
        try {
            $em->persist($vehicle);
            $em->flush();
        } catch (Exception $ex) {
            return $this->returnJsonResponse(
                JsonResponse::HTTP_BAD_REQUEST,
                'Update car failed.'
            );
        }
*/
        return $this->returnJsonResponse(
            JsonResponse::HTTP_OK,
            'ok'
        );       
    }

    // not used 
    public function editMilageAction(Request $request)
    {
        //verify field
        $validator = Validation::createValidator();
        $errors = $validator->validate($request->request->all(),
            new Collection(array(
                'plate' => [new NotNull(), new TaiwanCarNumber()],
                'milage' => [new NotNull(), new Type('numeric')],
            ))
        );

        if(isset($errors[0])){
            return $this->returnJsonResponse(
                JsonResponse::HTTP_BAD_REQUEST,
                $errors->get(0)->getPropertyPath().' '.$errors->get(0)->getMessage()
            );
        }

        $em = $this->getDoctrine()->getManager();
        $evPlateCanonical = EvUserOwnVehicle::canonicalizeEvPlate($request->get('plate'));
        $vehicle = $em->getRepository('EvBundle:EvUserOwnVehicle')->findOneBy([
            'user' => $this->getUser(),
            'evPlateCanonical' => $evPlateCanonical
        ]);
        try {
            if(empty($vehicle)) throw new Exception('');
            $vehicle->getEvPlate();
        } catch (Exception $ex) {
            return $this->returnJsonResponse(
                JsonResponse::HTTP_BAD_REQUEST,
                'This car doesnt exist.'
            );
        }
        
        $vehicle->setEvMilage($request->get('milage'));
        $vehicle->setUpdatedAt(new \DateTime());
        
        try {
            $em->persist($vehicle);
            $em->flush();
        } catch (Exception $ex) {
            return $this->returnJsonResponse(
                JsonResponse::HTTP_BAD_REQUEST,
                'Update car failed.'
            );
        }

        return $this->returnJsonResponse(
            JsonResponse::HTTP_OK,
            'ok'
        );       
    }
    
    public function  deleteAction(Request $request, $evPlate = '')
    {

        //verify field
        $validator = Validation::createValidator();
        $errors = $validator->validate(['plate' => $evPlate],
            new Collection(array(
                // 'plate' => [new NotNull(), new TaiwanCarNumber()],
                'plate' => [new NotNull(), new Type('string')],
            ))
        );

        if(isset($errors[0])){
            return $this->returnJsonResponse(
                JsonResponse::HTTP_BAD_REQUEST,
                $errors->get(0)->getPropertyPath().' '.$errors->get(0)->getMessage()
            );
        }

        $em = $this->getDoctrine()->getManager();
        $vehicleService = $this->get('ev.vehicle');
        $res = $vehicleService->deleteVehicle($this->getUser(), $evPlate);

        if ( !$res || !isset($res["result"])) {
            return $this->returnJsonResponse(
                JsonResponse::HTTP_BAD_REQUEST, 
                ""
            );
        }
        if ( $res["result"]== false ) { 
            $errMessage = isset($res["message"])? $res["message"] : "" ;
            return $this->returnJsonResponse(
                JsonResponse::HTTP_BAD_REQUEST, 
                $errMessage
            );
        } 

/*
        $evPlateCanonical = EvUserOwnVehicle::canonicalizeEvPlate($evPlate);
        $vehicle = $em->getRepository('EvBundle:EvUserOwnVehicle')->findOneBy([
            'user' => $this->getUser(),
            'evPlateCanonical' => $evPlateCanonical
        ]);
        try {
            if(empty($vehicle)) throw new Exception('');
            $vehicle->getEvPlate();
        } catch (Exception $ex) {
            return $this->returnJsonResponse(
                JsonResponse::HTTP_BAD_REQUEST,
                'This car doesnt exist.'
            );
        }

        try {
            $em->remove($vehicle);
            $em->flush();
        } catch (Exception $ex) {
            return $this->returnJsonResponse(
                JsonResponse::HTTP_BAD_REQUEST,
                'Delete car failed.'
            );
        }
*/
        return $this->returnJsonResponse(
            JsonResponse::HTTP_OK,
            'ok'
        );
    }

    private function createManufactorEntry($manufactor)
    {
        $mArray = array();
        $mArray['id'] = $manufactor->getManufactorId();
        $mArray['name'] = $manufactor->getManufactorName();
        $mArray['models'] = array();
        $mArray['batteries'] = array();
        return $mArray;
    }

    public function metadataAction(Request $request)
    {
        $view = View::create();
        $context = (new Context())->addGroup('list');

        $metadata = $this->get('ev.metadata');
        $modelData = $metadata->getVehicleMetadata();

        $view->setContext($context);
        $view->setStatusCode(200);
        $view->setData([
            'code' => 200, 
            'version' => $modelData['version'],
            'data' => [
                "models" => $modelData['data']
            ]
        ]);
        return $view;
    }
    
    public function gpslogAction(Request $request, $evPlate = '')
    {
        //verify field
        $validator = Validation::createValidator();
        $errors = $validator->validate($request->request->all(),
            new Collection(array(
                'lat' => [new NotNull()],
                'lng' => [new NotNull()],
            ))
        );

        if( ! isset($errors[0])){
            $errors = $validator->validate(['plate' => $evPlate],
                new Collection(array(
                    'plate' => [new NotNull(), new TaiwanCarNumber()],
                ))
            );
        }
        
        if(isset($errors[0])){
            return $this->returnJsonResponse(
                JsonResponse::HTTP_BAD_REQUEST,
                $errors->get(0)->getPropertyPath().' '.$errors->get(0)->getMessage()
            );
        }
        
        $em = $this->getDoctrine()->getManager();
        
        $vehicle = $em->getRepository('EvBundle:EvUserOwnVehicle')->findOneBy([
            'user' => $this->getUser(),
            'evPlate' => $evPlate
        ]);
        if(empty($vehicle)){
            return $this->returnJsonResponse(
                JsonResponse::HTTP_BAD_REQUEST,
                'This vehicle id is not exist.'
            );
        }
        
        $gpslog = new EvVehicleGpsLog();
        $gpslog->setEvUserOwnVehicle($vehicle);
        $gpslog->setLat($request->get('lat'));
        $gpslog->setLng($request->get('lng'));

        try {
            $em->persist($gpslog);
            $em->flush();
        } catch (Exception $ex) {
            return $this->returnJsonResponse(
                JsonResponse::HTTP_BAD_REQUEST,
                'Insert GPS log failed.'
            );
        }

        return $this->returnJsonResponse(
            JsonResponse::HTTP_OK,
            'ok'
        );
    }
    
}
