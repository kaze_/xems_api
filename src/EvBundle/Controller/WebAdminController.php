<?php

namespace EvBundle\Controller;

use DateTime;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\EmsLocationMaster;
use AppBundle\Entity\EmsSmartRelayTransactionLog;
use AppBundle\Entity\EmsUserMaster;
use AppBundle\Entity\User;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\Constraints\NotBlank;
use EvBundle\Validator\Constraints\TaiwanCarNumber;
use EvBundle\Entity\EvUserDetail;
use EvBundle\Entity\EvUserLocationMapping;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class WebAdminController extends BaseApiController
{
    function locationPointsStatMonthlyAction(EmsLocationMaster $location, $yearMonth)
    {
        $this->denyAccessUnlessGranted('view', $location);

        $start = (new DateTime())::createFromFormat('Y-m-d', $yearMonth.'-01')->setTime(0,0,0);
        $end = (clone($start))->modify('last day of this month')->setTime(23, 59, 59);

        $pointsPayment = $this->get('pay.points');
        
        $sessionRepo = $this->getDoctrine()->getManager()->getRepository('EvBundle:EvChargingSessionLog');
        
        $qb = $sessionRepo->createQueryBuilder('q')
        ->leftJoin('EvBundle:EvUserLocationMapping', 'u', 'WITH', 'q.userId = u.userId')
        ->leftJoin('EvBundle:EvUserDetail', 'd', 'WITH', 'q.userId = d.userId')
        ->innerJoin('AppBundle:EmsMeterCircuitMapping', 'mc', 'WITH', 'q.circuit = mc.circuit')
        ->innerJoin('AppBundle:EmsGatewayDeviceMapping', 'gd', 'WITH', 'mc.device = gd.deviceId')
        ->innerJoin('AppBundle:EmsLocationGatewayMapping', 'lg', 'WITH', 'gd.gatewayId = lg.gatewayId')
        ->select('u.userId, d.name, u.unitNumber, GROUP_CONCAT(distinct q.evPlate) as evPlate, count(q) as count, sum(q.amountCost) as cost, sum(q.totalKwh) as kwh')
        // ->select('d.userId, d.name, q.evPlate, count(q) as count, sum(q.amountCost) as cost, sum(q.totalKwh) as kwh')
        ->where('lg.locationId = :location')
        ->andWhere('q.endAt between :from and :to')
        ->setParameter('location', $location)
        ->setParameter('from', $start)
        ->setParameter('to', $end)
        ->groupBy('u.userId');

        $result = $qb->getQuery()->getResult();
        // get total user
        $activeUsers = $pointsPayment->getLocationUsersWithPointsLeftAt($location, $end);
        $users = [];
        $userIdList = [];
        foreach($result as $user) 
        {
            $pointData = $pointsPayment->getLocationPointStatusByUser($location, $user["userId"], $start, $end);
            if ( isset($pointData["points"])) $user["points"] = $pointData["points"];
            if ( isset($pointData["pointsDeposit"])) $user["pointsDeposit"] = $pointData["pointsDeposit"];
            if ( isset($pointData["pointsUsed"])) $user["pointsUsed"] = $pointData["pointsUsed"];
            if ( isset($pointData["pointsLastMonth"])) $user["pointsLastMonth"] = $pointData["pointsLastMonth"];
            $users[] = $user;        
            $userIdList[] = $user["userId"];
        }

        $dataRepo = $this->getDoctrine()->getManager()->getRepository('EvBundle:EvUserDetail');
        foreach($activeUsers as $activeUser) 
        {
            if ( in_array($activeUser["user_id"], $userIdList)  ) {
                continue;
            }
            $user = [
                'userId' => $activeUser["user_id"],
                'kwh' => 0,
                'count' => 0,
                'cost' => 0
            ];
            $result = $dataRepo->createQueryBuilder('q')
                ->join('EvBundle:EvUserLocationMapping', 'u', 'WITH', 'u.userId = q.userId')
                ->select('q.name, u.unitNumber')
                ->where('q.userId = :user')
                ->andWhere('u.locationId = :location')
                ->setParameter('user', $user['userId']) 
                ->setParameter('location', $location)
                ->getQuery()->getOneOrNullResult();
            if ( $result ) {
                $user['unitNumber'] = $result['unitNumber'];
                $user['name'] = $result['name'];
            }
            $pointData = $pointsPayment->getLocationPointStatusByUser($location, $user["userId"], $start, $end);
            if ( isset($pointData["points"])) $user["points"] = $pointData["points"];
            if ( isset($pointData["pointsDeposit"])) $user["pointsDeposit"] = $pointData["pointsDeposit"];
            if ( isset($pointData["pointsUsed"])) $user["pointsUsed"] = $pointData["pointsUsed"];
            if ( isset($pointData["pointsLastMonth"])) $user["pointsLastMonth"] = $pointData["pointsLastMonth"];
            $users[] = $user;
        }

        // get related points
        $pointSummary = $pointsPayment->getLocationPointStatus($location, $start, $end);
        
        return $this->returnJsonResponse(
            JsonResponse::HTTP_OK,
            'ok',
            ['users' => $users, 'summary' => $pointSummary]
        );    
    }

    function evUserListByLocationAction(EmsLocationMaster $location)
    {
        $this->denyAccessUnlessGranted('view', $location); 
        $userRepo = $this->getDoctrine()->getManager()->getRepository('EvBundle:EvUserLocationMapping');
        $qb = $userRepo->createQueryBuilder('q')
            ->leftJoin('EvBundle:EvUserDetail', 'd', 'WITH', 'q.userId = d.userId')
            ->leftJoin('AppBundle:EmsUserMaster', 'u', 'WITH', 'q.userId = u.userId')
            ->leftJoin('EvBundle:EvLocationPoints', 'p', 'WITH', 'q.userId = p.userId and q.locationId = p.locationId')
            ->select('q.userId, d.name, u.username as cellPhone, q.contactPhone, q.unitNumber as unit, p.points, q.statusId')
            ->where('q.locationId = :location')
            ->setParameter('location', $location);
        $result = $qb->getQuery()->getResult();
        return $this->returnJsonResponse(
            JsonResponse::HTTP_OK,
            'ok',
            $result
        );    

    }

    // user page
    function evUserByLocationAction(EmsLocationMaster $location, EmsUserMaster $user)
    {
        $this->denyAccessUnlessGranted('view', $location); 
        
        $em = $this->getDoctrine()->getManager();
        $userDetail = $em->getRepository('EvBundle:EvUserDetail')->findOneBy(['userId' => $user]);
        $locationUser = $em->getRepository('EvBundle:EvUserLocationMapping')->findOneby(['userId' => $user, 'locationId'  => $location]);
/*
        $vehicleRepo = $this->getDoctrine()->getManager()->getRepository('EvBundle:EvUserOwnVehicle');
        $userVehicle = $vehicleRepo->createQueryBuilder('q')
            ->join('EvBundle:EvVehicleModelMaster', 'v', 'WITH', 'q.evModelId = v.id')
            ->select('q.evPlate, q.vinNumber, v.evModelName')
            ->where('q.user = :user')
            ->setParameter('user', $user)
            ->getQuery()->getResult();
*/
        $vehicleService = $this->get('ev.vehicle');
        $userVehicle = $vehicleService->getVehiclesByLocationUsers($location, $user);
        $evPoints = $em->getRepository('EvBundle:EvLocationPoints')
            ->findOneBy(['locationId' => $location->getLocationId(), 'userId' => $user->getUserId()]);
        $evPointsLog = $em->getRepository('EvBundle:EvLocationPointsLog')
            ->createQueryBuilder('q')
            ->select('q.action, sum(q.points) as points')
            ->where('q.locationId = :location')
            ->andWhere('q.userId = :user')
            ->setParameter('location', $location->getLocationId())
            ->setParameter('user', $user->getUserId())
            ->groupby('q.action')
            ->getQuery()->getResult();

        if ( !$evPoints ) {
            $points = 0;
        } else {
            $points = $evPoints->getPoints();
        }

        $pointsDeposit = 0;
        $pointsUsed = 0;
        if ( $evPointsLog ) {
            foreach($evPointsLog as $log){
                switch($log["action"]) {
                    case 'DEPOSIT':
                    case 'PRECHARGE':
                    case 'REFUND':
                        $pointsDeposit += $log["points"];
                    break;
                    case 'CHARGING':
                        $pointsUsed += $log["points"];
                    break;
                }
            }
        }
        
        return $this->returnJsonResponse(
            JsonResponse::HTTP_OK,
            'ok',
            [
                'user' => [
                    'name' => $userDetail ? $userDetail->getName() : "", 
                    'cellPhone' => $user->getUserName(), 
                    'contactPhone' => $locationUser->getContactPhone(),
                    'email' => $user->getEmail()
                ], 
                'vehicles' => $userVehicle, 
                'points' => [
                    'total_deposit' => $pointsDeposit,
                    'total_used' => $pointsUsed,
                    'points_left' => $points,
                ]
            ]
        );    
    }

    function evEnableLocationUserVehicleAction(EmsLocationMaster $location, EmsUserMaster $user, $evPlate = '')
    {
        $vehicleService = $this->get('ev.vehicle');
        $res = $vehicleService->enableVehecleByLocationUsers($location, $user, $evPlate);
        if ( $res == false ) {
            return $this->returnJsonResponse(
                JsonResponse::HTTP_BAD_REQUEST,
                'enable failed'
            );  
        }
        return $this->returnJsonResponse(
            JsonResponse::HTTP_OK,
            'ok'
        );  
    }

    function evDisableLocationUserVehicleAction(EmsLocationMaster $location, EmsUserMaster $user, $evPlate = '')
    {
        $vehicleService = $this->get('ev.vehicle');
        $res = $vehicleService->disableVehicleByLocationUsers($location, $user, $evPlate);
        if ( $res == false ) {
            return $this->returnJsonResponse(
                JsonResponse::HTTP_BAD_REQUEST,
                'enable failed'
            );  
        }
        return $this->returnJsonResponse(
            JsonResponse::HTTP_OK,
            'ok'
        );  
    }

    function sortByStartedAt($a, $b) 
    {
        return $a['startAtTimestamp'] - $b['startAtTimestamp'];
    }

    function evUnkownUserUsageByLocationAction(EmsLocationMaster $location, $yearMonth)
    {
        $this->denyAccessUnlessGranted('view', $location);
        $start = (new DateTime())::createFromFormat('Y-m-d', $yearMonth.'-01')->setTime(0,0,0);
        $end = (clone($start))->modify('last day of this month')->setTime(23, 59, 59);
        // from session 
        $sessionRepo = $this->getDoctrine()->getManager()->getRepository('EvBundle:EvChargingSessionLog');
        $qbSess = $sessionRepo->createQueryBuilder('q')
            ->innerJoin('AppBundle:EmsMeterCircuitMapping', 'mc', 'WITH', 'q.circuit = mc.circuit')
            ->innerJoin('AppBundle:EmsGatewayDeviceMapping', 'gd', 'WITH', 'mc.device = gd.deviceId')
            ->innerJoin('AppBundle:EmsLocationGatewayMapping', 'lg', 'WITH', 'gd.gatewayId = lg.gatewayId')
            ->leftJoin('AppBundle:EvChargingStationMaster', 'c', 'WITH', 'c.chargingStationId = q.chargingStation')
            // ->select('q.evPlate, v.vinNumber, unix_timestamp(q.startAt) as startAt, unix_timestamp(q.endAt) as endAt, q.totalKwh')
            ->select('q.sessionId, q.evPlate, q.startAt, q.endAt, unix_timestamp(q.startAt)-8*60*60 as startAtTimestamp, unix_timestamp(q.endAt)-8*60*60 as endAtTiemstamp, c.name, q.totalKwh')
            ->where('lg.locationId = :location')
            ->andWhere('q.userId is null')
            ->andWhere('q.endAt between :from and :to')
            ->setParameter('location', $location)
            ->setParameter('from', $start)
            ->setParameter('to', $end);

        // 
        $sessResult = $qbSess->getQuery()->getResult();
        $results = [];
        foreach($sessResult as $sess) {
            $sess['action'] = "CHARGING";
            $results[] = $sess;
        }
    
        // sorting by startAt
        if ( $results ) {
            usort($results, array($this, "sortByStartedAt"));
        } 

        return $this->returnJsonResponse(
            JsonResponse::HTTP_OK,
            'ok',
            $results
        );  
    }

    function evUserPointsByLocationAction(EmsLocationMaster $location, EmsUserMaster $user, $yearMonth)
    {
        $this->denyAccessUnlessGranted('view', $location);

        $start = (new DateTime())::createFromFormat('Y-m-d', $yearMonth.'-01')->setTime(0,0,0);
        $end = (clone($start))->modify('last day of this month')->setTime(23, 59, 59);
        // find user session 

        $em = $this->getDoctrine()->getManager();
        $sessionRepo = $em->getRepository('EvBundle:EvChargingSessionLog');
        $pointsLogRepo  = $em->getRepository('EvBundle:EvLocationPointsLog');

        // from session 
        $qbSess = $sessionRepo->createQueryBuilder('q')
            ->innerJoin('AppBundle:EmsMeterCircuitMapping', 'mc', 'WITH', 'q.circuit = mc.circuit')
            ->innerJoin('AppBundle:EmsGatewayDeviceMapping', 'gd', 'WITH', 'mc.device = gd.deviceId')
            ->innerJoin('AppBundle:EmsLocationGatewayMapping', 'lg', 'WITH', 'gd.gatewayId = lg.gatewayId')
            ->leftJoin('AppBundle:EvChargingStationMaster', 'c', 'WITH', 'c.chargingStationId = q.chargingStation')
            // ->select('q.evPlate, v.vinNumber, unix_timestamp(q.startAt) as startAt, unix_timestamp(q.endAt) as endAt, q.totalKwh')
            ->select('q.sessionId, q.evPlate, q.startAt, q.endAt, unix_timestamp(q.startAt)-8*60*60 as startAtTimestamp, unix_timestamp(q.endAt)-8*60*60 as endAtTiemstamp, c.name, q.totalKwh')
            ->where('lg.locationId = :location')
            ->andWhere('q.userId = :user')
            ->andWhere('q.endAt between :from and :to')
            ->setParameter('location', $location)
            ->setParameter('user', $user)
            ->setParameter('from', $start)
            ->setParameter('to', $end);

        // 
        $sessResult = $qbSess->getQuery()->getResult();

        // from point table
        $qbPoints = $pointsLogRepo->createQueryBuilder('p')
            ->leftJoin('EvBundle:EvChargingSessionLog', 's', 'WITH', 'p.sessionId = s.sessionId')
            ->leftJoin('AppBundle:EvChargingStationMaster', 'c', 'WITH', 's.chargingStation = c.chargingStationId')
            ->select('p.sessionId, p.action, s.evPlate, p.loggedAt, unix_timestamp(p.loggedAt)-8*60*60 as loggedAtTimestamp, s.startAt, s.endAt, unix_timestamp(s.startAt)-8*60*60 as startAtTimestamp, unix_timestamp(s.endAt)-8*60*60 as endAtTiemstamp, c.name, s.totalKwh, p.points, p.pointsLeft, p.comment')
            // ->select('p.sessionId, p.action, p.loggedAt, unix_timestamp(p.loggedAt)-8*60*60 as loggedAtTimestamp, p.points, p.pointsLeft')
            ->where('p.locationId = :location')
            ->andWhere('p.userId = :user')
            ->andWhere('p.loggedAt between :from and :to')
            ->setParameter('location', $location)
            ->setParameter('user', $user)
            ->setParameter('from', $start)
            ->setParameter('to', $end);

        $pointResult = $qbPoints->getQuery()->getResult();
        // combine. remove same session id
        $results = [];
        $sessionList = [];
        $chargingControllService = $this->get('ev.charging_controll');
        foreach($pointResult as $log) {
            if ( $log['comment'] != null ) {
                if ( ($commentArr = json_decode($log['comment'], true)) != null ) {
                    if ( isset($commentArr['guest_id'])) {
                        $guest = $chargingControllService->getLocationGuest($commentArr['guest_id'], $location->getLocationId());
                        $evPlate = $guest->getEvPlate();
                        $log['evPlate'] = $evPlate;
                    }
                }
            }
            if ( $log['sessionId'] != null ) {
                $sessionList[] = $log['sessionId'];
            } 
            if ( $log['startAt'] == null ) {
                $log['startAt'] = $log['loggedAt'];
                $log['startAtTimestamp'] = $log['loggedAtTimestamp'];
            }
            $results[] = $log;
        }
        foreach($sessResult as $sess) {
            if ( in_array($sess['sessionId'], $sessionList) ) {
                continue;
            }
            $sess['action'] = "CHARGING";
            $results[] = $sess;
        }
    
        // sorting by startAt
        if ( $results ) {
            usort($results, array($this, "sortByStartedAt"));
        }

        // summary 
        $payPoints = $this->get('pay.points');
        $userTotal = $payPoints->getLocationPointStatusByUser($location, $user, $start, $end);

        return $this->returnJsonResponse(
            JsonResponse::HTTP_OK,
            'ok',
            ['sessions' => $results, 'summary' => $userTotal]
        );    

    }

    function addLocationUserAction(Request $request, EmsLocationMaster $location)
    {
        $this->denyAccessUnlessGranted('view', $location);

        $validator = Validation::createValidator();
        $errors = $validator->validate($request->request->all(),
            new Collection(array(
                'user_name' => [new NotNull(), new Length(array('min' => 2))],
                'unit_number' => [new NotNull()],
                'mobile_phone' => [new NotNull()],
                'contact_phone' => [new NotNull()],
 //               'email' => [new NotNull(), new Email()],
            ))
        );

        if(isset($errors[0])){
            return new JsonResponse([
                'code' => JsonResponse::HTTP_BAD_REQUEST,
                'msg' => $errors->get(0)->getPropertyPath().' '.$errors->get(0)->getMessage()
            ], JsonResponse::HTTP_BAD_REQUEST);
        }

        $name = $request->get("user_name");
        $unit = $request->get("unit_number");
        $phone = $request->get("mobile_phone");
        $contact = $request->get("contact_phone");

//        $email = $request->get("email");

        // check if use exist
        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->findUserByUsername($phone);

        $em = $this->getDoctrine()->getManager();

        if ( $user == null) {
            try {
                $user = $userManager->createUser()
                    ->setEnabled(true)
                    ->setUsername($phone);
//                    ->setEmail($email);
                $user->addRole('ROLE_EV_USER');
                $userManager->updateUser($user);
            } catch (Exception $e ) {
                return $this->returnJsonResponse(
                    JsonResponse::HTTP_BAD_REQUEST,
                    $e->getMessage()); 
            }
            $user = $userManager->findUserByUsername($phone);
            if($user == null){
                return $this->returnJsonResponse(
                    JsonResponse::HTTP_BAD_REQUEST,
                    'Register user failed.');
            }
        }

        $userId = $user->getId();
        $locationId = $location->getLocationId();
        $locationUserRepo = $this->getDoctrine()->getManager()->getRepository('EvBundle:EvUserLocationMapping');
        if ( $locationUserRepo->findOneBy(['userId' => $userId, 'locationId' => $location->getLocationId(), 'statusId' => 1 ])){
            return $this->returnJsonResponse(
                JsonResponse::HTTP_BAD_REQUEST, 
                'user already exists.'
            );
        }

        try {
            $locationUser = new EvUserLocationMapping();
            $locationUser->setUserId($userId);
            $locationUser->setLocationId($locationId);
            $locationUser->setUnitNumber($unit);
            $locationUser->setContactPhone($contact);
            $locationUser->setStatusId(1);
            $locationUser->setCreatedAt(new \DateTime());
            $locationUser->setUpdatedAt(new DateTime());
            $em->persist($locationUser);
            $detail = $em->getRepository(EvUserDetail::class)->find($user->getId());
            if( $detail === NULL ) {
                $detail = new EvUserDetail();
                $detail->setUserId($userId);
                $detail->setCellPhone($phone);
                $detail->setName($name);
                $em->persist($detail);
            }
            $em->flush();
        } catch ( Exception $e ) {
            return $this->returnJsonResponse(
                JsonResponse::HTTP_BAD_REQUEST, 
                $e->getMessage()
            );
        }
        return $this->returnJsonResponse(
            JsonResponse::HTTP_OK,
            'ok'
        );    
    }


    public function editLocationUserAction(Request $request, EmsLocationMaster $location, EmsUserMaster $user)
    {
        $this->denyAccessUnlessGranted('view', $location);

        $validator = Validation::createValidator();
        $errors = $validator->validate($request->request->all(),
            new Collection(array(
                'user_name' => [new NotNull(), new Length(array('min' => 2))],
                'contact_phone' => [new NotNull()],
                'unit_number' => [new NotNull()],
                'user_status' => [new NotNull()],
            ))
        );

        if(isset($errors[0])){
            return new JsonResponse([
                'code' => JsonResponse::HTTP_BAD_REQUEST,
                'msg' => $errors->get(0)->getPropertyPath().' '.$errors->get(0)->getMessage()
            ], JsonResponse::HTTP_BAD_REQUEST);
        }

        $name = $request->get("user_name");
        $unit = $request->get("unit_number");
        $status = $request->get("user_status");
        $phone = $request->get("contact_phone");

        if ( $status != 0 && $status != 1 ) {
            return $this->returnJsonResponse(
                JsonResponse::HTTP_BAD_REQUEST,
                'user status must be 0 or 1: '.$status); 
        }

        $em = $this->getDoctrine()->getManager();
        $locationUser= $em->getRepository('EvBundle:EvUserLocationMapping')->findOneBy(['locationId' => $location, 'userId' => $user]);
        $userDetail = $em->getRepository('EvBundle:EvUserDetail')->findOneBy(['userId' => $user]);;

        if ( !$locationUser) {
            return $this->returnJsonResponse(
                JsonResponse::HTTP_BAD_REQUEST,
                'user doesn\'t exit'); 
        }
        if (!$userDetail) {
		    $userDetail = new EvUserDetail();
			$userDetail->setUserId($user->getUserId());
			$userDetail->setUpdatedAt(new \DateTime());
			$userDetail->setCellPhone($user->getUsername());
        }
        // update user status
        $locationUser->setStatusId($status);
        $locationUser->setUnitNumber($unit);
        $locationUser->setContactPhone($phone);
        $locationUser->setUpdatedAt(new \DateTime());
        $userDetail->setName($name);
        try {
            $em->persist($locationUser);
            $em->persist($userDetail);
            $em->flush();
        } catch ( Exception $e ) {
            return $this->returnJsonResponse(
                JsonResponse::HTTP_BAD_REQUEST, 
                $e->getMessage()
            );
        }
        return $this->returnJsonResponse(
            JsonResponse::HTTP_OK,
            'ok'
        );    
    }

    public function depositToLocationAction(Request $request, EmsLocationMaster $location)
    {
        // check admin permission
        $this->denyAccessUnlessGranted('view', $location);

        $validator = Validation::createValidator();
        $errors = $validator->validate($request->request->all(),
            new Collection(array(
                'user_id' => [new NotNull(), new Length(['min' => 32, 'max' => 36])],
                'points' => [new NotNull(), new Type(['type' => 'numeric'])],
                'reason' => [new Length(['max' => 128])],
            ))
        );
        if(isset($errors[0])){
            return $this->returnJsonResponse(
                JsonResponse::HTTP_BAD_REQUEST,
                $errors->get(0)->getPropertyPath().' '.$errors->get(0)->getMessage()
            );
        }
        
        $userId = $request->get('user_id');
        $points = $request->get('points');
        $reason = $request->get('reason');

        $em = $this->getDoctrine()->getManager();
        $locationUser = $em->getRepository('EvBundle:EvUserLocationMapping')->findOneBy(['userId' => $userId, 'locationId' => $location->getLocationId()]);
        
        // check if user belong to location
        if ( !$locationUser ) {
            return $this->returnJsonResponse(
                JsonResponse::HTTP_BAD_REQUEST, 
                'user doesn\'t exists'
            );
        }


        $admin = $this->getUser();
        $advReason = json_encode([
            'message' => $reason,
            'admin' => $admin->getId()
        ]);
        $pointsPayment = $this->get('pay.points');
        $result = $pointsPayment->modifyLocationPoints($userId, $location->getLocationId(), $points, 'DEPOSIT', $advReason);
        
        if ( $result["result"] == true ) {
            $data = $result["data"];
            $returnValue = [];
            if ( $data ) {
                $returnValue = [
                    "userId" => $data->getUserId(),
                    "locationId" => $data->getLocationId(),
                    "pointsOrigin" => $data->getPointsLeft() - $data->getPoints(), 
                    "points" => $data->getPoints(),
                    "pointsLeft" => $data->getPointsLeft(), 
                    "time" => $data->getLoggedAt()->format('Y-m-d H:i:s')
                ];
            }
            return $this->returnJsonResponse(
                JsonResponse::HTTP_OK, 
                null, 
                $returnValue
            );
        } else {
            return $this->returnJsonResponse(
                JsonResponse::HTTP_INTERNAL_SERVER_ERROR,
                'Error while deposit'
            );
        }
    }   

    public function addVehicleToUserAction(Request $request, EmsLocationMaster $location, EmsUserMaster $user) 
    {
        $this->denyAccessUnlessGranted('edit', $location);

        $validator = Validation::createValidator();
        $errors = $validator->validate($request->request->all(),
            new Collection(array(
                'ev_plate' => [new NotNull(), new TaiwanCarNumber()],
                'ev_model' => [new NotNull(), new Type('string')],
            ))
        );

        if(isset($errors[0])){
            return $this->returnJsonResponse(
                JsonResponse::HTTP_BAD_REQUEST,
                $errors->get(0)->getPropertyPath().' '.$errors->get(0)->getMessage()
            );
        }

        // check if user belongs to location
        $userLocationRepo = $this->getDoctrine()->getManager()->getRepository('EvBundle:EvUserLocationMapping');
        if ( $userLocationRepo->findOneBy( ['userId' => $user->getUserId(), 'locationId' => $location->getLocationId(), 'statusId' => 1]) == null ) {
            return $this->returnJsonResponse(
                JsonResponse::HTTP_BAD_REQUEST, 
                'user does not belong current location'
            );
        }
        $vehicleService = $this->get('ev.vehicle');
        $userManager = $this->get('fos_user.user_manager');
        $sysUser = $userManager->findUserBy(['id' => $user->getUserId()]);
        $result = $vehicleService->addVehicleByLocationUsers($location, $sysUser, $request->get('ev_plate'), $request->get('ev_model'));
        if ( is_array($result) && $result["result"] == true ) {
            return $this->returnJsonResponse(
                JsonResponse::HTTP_OK
            );
        } else {
            return $this->returnJsonResponse(
                JsonResponse::HTTP_BAD_REQUEST, 
                $result["message"]
            );
        }
    }

    private function adminLog($action, $location, $locationUser, $comment)
    {
        $admin = $this->getUser();
        // log... 
    }

    public function ListLocationGuestAction(Request $request, EmsLocationMaster $location)
    {
        $this->denyAccessUnlessGranted('view', $location);

        $chargingService = $this->get('ev.charging_controll');
        $res = $chargingService->ListLocationActiveGuest($location->getLocationId()) ;
        if ( $res == null || count($res) == 0 ) {
            return $this->returnJsonResponse(
                JsonResponse::HTTP_OK,
                'not data'
            );
        }
        $data = [];
        $sessionService = $this->get('ev.session');
        foreach($res as $guest) {
            $oneData = [
                'id' => $guest->getId(),
                'phone' => $guest->getPhone(),
                'unit' => $guest->getVisitUnit(),
                'evPlate' => $guest->getEvPlate(),
                'preCharge' => $guest->getPreCharge(),
            ];
            if ( ( $sessionId = $guest->getSessionId()) != null ) {
                $session = $sessionService->getSessionData($sessionId);
                $oneData['station'] = isset($session['chargingStationName']) ? $session['chargingStationName']: "";
                $oneData['kwh'] = isset($session['totalKwh']) ? ceil($session['totalKwh']*100)/100: "";
                $oneData['ended'] = isset($session['ended']) ? $session['ended'] : false;
                if ( $oneData['ended'] ==  true ) {
                    $oneData['fee'] = isset($session['amountCost']) ? $session['amountCost']: "";
                } else {
                    $oneData['fee'] = isset($session['amountCost']) ? ceil($session['amountCost']): "";
                }
            }
            $data[] = $oneData;
        }
        return $this->returnJsonResponse(
            JsonResponse::HTTP_OK,
            'ok', 
            $data
        );
    }

    public function AddLocationGuestAction(Request $request, EmsLocationMaster $location) 
    {
        // $phone, $evPlate, $unit, $preCharge
        $this->denyAccessUnlessGranted('edit', $location);

        $validator = Validation::createValidator();
        $errors = $validator->validate($request->request->all(),
            new Collection(array(
                'ev_plate' => [new NotNull(), new TaiwanCarNumber()],
                'phone' => [new NotNull(), new Type('string')],
                'unit' => [new NotNull(), new Type('string')],
                'pre_charge' => [new NotNull(), new Type(['type' => 'numeric'])],
            ))
        );

        if(isset($errors[0])){
            return $this->returnJsonResponse(
                JsonResponse::HTTP_BAD_REQUEST,
                $errors->get(0)->getPropertyPath().' '.$errors->get(0)->getMessage()
            );
        }

        $phone = $request->get('phone');
        $evPlate = $request->get('ev_plate');
        $unit = $request->get('unit');
        $preCharge = $request->get('pre_charge');

        $chargingService = $this->get('ev.charging_controll');
        $res = $chargingService->addLocationGuest($location->getLocationId(), $phone, $evPlate, $unit, $preCharge);
        if ( $res == true  ){
            return $this->returnJsonResponse(
                JsonResponse::HTTP_OK,
                'ok'
            );
        } else {
            return $this->returnJsonResponse(
                JsonResponse::HTTP_BAD_REQUEST, 
                "add guest user failed"
            );
        }
    }

    /**
     * 1. 如果 session 還沒結束，檢查是否已經結算過
     *      1. 若是 - 回應結算金額
     *      2. 若否 - 
     *          1. 通知 gateway 關閉
     *          2. 結算
     */
    public function preCheckOutGuestAction(Request $request, EmsLocationMaster $location, $guestId=0)
    {
        //
        $this->denyAccessUnlessGranted('edit', $location);
        $chargingService = $this->get('ev.charging_controll');
        $guest = $chargingService->getLocationGuest($guestId, $location->getLocationId()) ;
        if ( !$guest  ) {
            return $this->returnJsonResponse(
                JsonResponse::HTTP_NOT_FOUND,
                'guest data not found'
            );            
        }
        $data = [
            'id' => $guest->getId(),
            'phone' => $guest->getPhone(),
            'unit' => $guest->getVisitUnit(),
            'evPlate' => $guest->getEvPlate(),
            'preCharge' => $guest->getPreCharge(),
        ];
        $em = $this->getDoctrine()->getManager();
        $sessionService = $this->get('ev.session');
        if ( ( $sessionId = $guest->getSessionId()) != null ) {
            $session = $em->getRepository('EvBundle:EvChargingSessionLog')->findOneBy(['sessionId' => $sessionId]); 
            $sessionObj = null;
            if ( $session && ($session->getEndAt() == null) ) {
                // stop charging immediately
                $this->iotConn = $this->get('app.kaa');
                $sessionObj = $sessionService->endSessionDryRun($session->getCircuit(), $session->getChargingStation());
                $session_oid = $session->getOId();
                $circuit = $sessionObj->getCircuit();
                if ($circuit) {
                    $relay = $this->get('app.relay');
                    $this->iotConn = $this->get('app.kaa');
                    $adminUserId = $this->getUser()->getId();
                    $relay->setStatus($circuit->getCircuitId(),
                        EmsSmartRelayTransactionLog::ACT_METEROFF,
                        EmsSmartRelayTransactionLog::SOURCE_USER,
                        $adminUserId,
                        'MeterOff', 
                        null,
                        null,
                        $session_oid,
                        $guest->getEvPlate()
                    );
                    $result = $this->iotConn->powerSwitch($circuit,'Off', [ 'userId' => $adminUserId]);
                }
            } else {
                $sessionObj = $session;
            }
            
            $payService = $this->get('pay.points');
            $sessionPayLog = $payService->getPointsLogBySession($sessionId);
            // collect charging fee immediately
            $point = 0;
            if ( $sessionPayLog ) {
                $point = $sessionPayLog->getPoints();
            } else {
                if ($sessionObj) { 
                    $chargingControllService = $this->get('ev.charging_controll');
                    $ret = $chargingControllService->collectChargingFee($sessionObj, $guest=true);
                    // print_r($ret);
                    if ( is_array($ret) ) {
                        if ( isset($ret['result'])&& $ret['result'] == true && $ret['data'] != null){
                            $point = $ret['data']->getPoints();
                        }
                    }                
                } 
            }

            $stationName = "";
            $station = $session->getChargingStation();
            if ($station) {
                $stationName = $station->getName();
            }
            $data['fee'] = -$point;
            $data['station'] = $stationName;
            $data['startAt'] = $sessionObj->getStartAt()->format('Y-m-d h:i:s');
            $data['endAt'] = $sessionObj->getEndAt()->format('Y-m-d h:i:s');
            $data['kwh'] = $sessionObj->getTotalKwh();
        } else {
            $data['station'] = null;
            $data['startAt'] = null;
            $data['endAt'] = null;
            $data['kwh'] = null;
            $data['fee'] = null;
        }
        return $this->returnJsonResponse(
            JsonResponse::HTTP_OK,
            'OK', 
            $data
        );
    }

    public function checkOutGuestAction(Request $request, EmsLocationMaster $location, $guestId=0)
    {
        $this->denyAccessUnlessGranted('edit', $location);

        $validator = Validation::createValidator();
        $errors = $validator->validate($request->request->all(),
            new Collection(array(
                'edit_refund' => [new NotNull(), new notBlank()],
            ))
        );
        if(isset($errors[0])){
            return $this->returnJsonResponse(
                JsonResponse::HTTP_BAD_REQUEST,
                $errors->get(0)->getPropertyPath().' '.$errors->get(0)->getMessage()
            );
        }
        $refund = $request->get('edit_refund');

        $chargingService = $this->get('ev.charging_controll');
        $res = $chargingService->checkoutLocationGuest($location->getLocationId(), $guestId, $refund);
        if ( $res === false  ){
            return $this->returnJsonResponse(
                JsonResponse::HTTP_BAD_REQUEST, 
                "guest user checkout failed"
            );
        } else {
            $log = $res['data'];
            $guest = $chargingService->getLocationGuest($guestId, $location->getLocationId());
            $sessionId = $guest->getSessionId();
            $points = 0;
            if ( $sessionId ) {
                $payService = $this->get('pay.points');
                $sessionPayLog = $payService->getPointsLogBySession($sessionId);
                if ( $sessionPayLog ) {
                    $points = -$sessionPayLog->getPoints();
                }
            }
            $data = [
                // 'time' => $log->getLoggedAt()->format(\DateTime::ATOM),
                'time' => $log->getLoggedAt()->format('Y-m-d H:i:s'),
                'points' => $points,
                'evPlate' => $guest->getEvPlate()
            ];
            return $this->returnJsonResponse(
                JsonResponse::HTTP_OK,
                'ok',
                $data
            );
        }
    }

    function getUserPlatesByLocationAction(EmsLocationMaster $location) 
    {
        $this->denyAccessUnlessGranted('edit', $location);
        
        $vehicleService = $this->get('ev.vehicle');
        $plates = $vehicleService->getUserVehicleByLocation($location->getLocationId());

        if ( $plates == null ) {
            $retCode = Response::HTTP_NOT_FOUND;
        } else {
            $retCode = Response::HTTP_OK;
        }
        return $this->returnJsonResponse(
            $retCode, 
            'ok', 
            $plates
        );
    }

}