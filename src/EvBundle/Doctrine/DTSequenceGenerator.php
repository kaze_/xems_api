<?php
/**
 * Created by PhpStorm.
 * User: wild
 * Date: 2018/1/22
 * Time: 下午12:24
 */

namespace EvBundle\Doctrine;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Id\AbstractIdGenerator;
use EvBundle\Entity\Sequence;
use Exception;
use Symfony\Component\Validator\Constraints\DateTime;

abstract class DTSequenceGenerator extends AbstractIdGenerator
{
    abstract protected function prefix();
    abstract protected function format();

    /**
     * Generates an identifier for an entity.
     *
     * @param EntityManager|EntityManager $em
     * @param \Doctrine\ORM\Mapping\Entity $entity
     * @return string
     */
    public function generate(EntityManager $em, $entity)
    {
        if(empty($this->prefix())){
            throw new Exception('DTSequenceGenerator need prefix');
        }
        if(empty($this->format())){
            throw new Exception('DTSequenceGenerator need format');
        }
        
        
        $em->getConnection()->beginTransaction();
        
        $classname = (new \ReflectionClass($entity))->getShortName();
        try {
            $seq = $em->find('EvBundle:Sequence', $classname);
            if (empty($seq)) throw new Exception();
            
//            $seq->setEntity($classname);
            $seq->setSeq($seq->getSeq()+1);
            
            //cross date
            if(date('Ymd') !== $seq->getUpdatedAt()->format('Ymd')){
                $seq->setSeq(1);    
            }
            
            $seq->setUpdatedAt(new \DateTime());
            $em->persist($seq);
            $em->flush($seq);
            
            
            
        } catch (Exception $ex) {
            $seq = new Sequence();
            $seq->setEntity($classname);
            $seq->setPrefix($this->prefix());
            $seq->setFormat($this->format());
            $seq->setSeq(1);
            $em->persist($seq);
            $em->flush();
        }

        $em->getConnection()->commit();
        
        
        return $seq->getPrefix().date(sprintf($seq->getFormat(), $seq->getSeq()));
    }
}