<?php
/**
 * Created by PhpStorm.
 * User: wild
 * Date: 2018/1/22
 * Time: 下午2:55
 */

namespace EvBundle\Doctrine;

class PayHistorySequenceGenerator extends DTSequenceGenerator
{

    protected function prefix()
    {
        return 'P';
    }

    protected function format()
    {
        return 'Ymd%05d';
    }
}