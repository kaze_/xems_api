<?php

namespace EvBundle\Entity;

/**
 * CreditCard
 */
class CreditCard
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $userId;
    
    /**
     * @var string
     */
    private $cardno;

    /**
     * @var string
     */
    private $expY;
    
    /**
     * @var string
     */
    private $expM;

    /**
     * @var string
     */
    private $cvc;

    /**
     * @var bool
     */
    private $isAe;
    
    /**
     * @var int
     */
    private $status;

    /**
     * @var \DateTime
     */
    private $createdAt;

    const STATUS_UNVERIFIED = 0;
    const STATUS_OK = 1;
    const STATUS_VERIFY_FAILED = 2;
    const STATUS_DELETE = 3;
    
    private $key = 'cDKidaf3c0M5J6CW';
    private $iv = '5O6V10ckKYGzX1UP';
    private $disableEncrypt = false;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->status = self::STATUS_UNVERIFIED;
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set userId
     *
     * @param string $userId
     *
     * @return CreditCard
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return string
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set cardno
     *
     * @param string $cardno
     *
     * @return CreditCard
     */
    public function setCardno($cardno)
    {
        $this->cardno = $this->_encrypt($cardno);

        return $this;
    }

    /**
     * Get cardno
     *
     * @return string
     */
    public function getCardno()
    {
        return $this->_decrypt($this->cardno);
    }

    /**
     * Set exp_y
     *
     * @param string $expY
     *
     * @return CreditCard
     */
    public function setExpY($expY)
    {
        $this->expY = $this->_encrypt($expY);

        return $this;
    }

    /**
     * Get exp_y
     *
     * @return string
     */
    public function getExpY()
    {
        return $this->_decrypt($this->expY);
    }
    
    /**
     * Set exp_m
     *
     * @param string $expM
     *
     * @return CreditCard
     */
    public function setExpM($expM)
    {
        $this->expM = $this->_encrypt($expM);

        return $this;
    }

    /**
     * Get exp_m
     *
     * @return string
     */
    public function getExpM()
    {
        return $this->_decrypt($this->expM);
    }

    /**
     * Set cvc
     *
     * @param string $cvc
     *
     * @return CreditCard
     */
    public function setCvc($cvc)
    {
        $this->cvc = $this->_encrypt($cvc);

        return $this;
    }

    /**
     * Get cvc
     *
     * @return string
     */
    public function getCvc()
    {
        return $this->_decrypt($this->cvc);
    }

    /**
     * Set isAe
     *
     * @param boolean $isAe
     *
     * @return CreditCard
     */
    public function setIsAe($isAe)
    {
        $this->isAe = $isAe;

        return $this;
    }

    /**
     * Get isAe
     *
     * @return bool
     */
    public function getIsAe()
    {
        return $this->isAe;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return CreditCard
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return CreditCard
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    private function _encrypt($data)
    {
        if($this->disableEncrypt){
            return $data;
        }
        $endata = @openssl_encrypt($data , 'aes-128-cbc', $this->key, true, $this->iv);
        return base64_encode($endata);
    }
    
    private function _decrypt($data)
    {
        if($this->disableEncrypt){
            return $data;
        }
        $data = base64_decode($data);
        return @openssl_decrypt($data, 'aes-128-cbc', $this->key, true, $this->iv);
    }
    
    public function disableEncrypt($yn)
    {
        if( ! in_array(boolval($yn), [true, false])){
            throw new \Exception('Wrong argument');
        }
        $this->disableEncrypt = boolval($yn);
    }
    
    public function getMaskedCardNo()
    {
        $pad = str_pad('', 8, '*');
        $number = substr_replace($this->getCardno(), $pad, 4, strlen($pad));
        return $number;
    }

}

