<?php

namespace EvBundle\Entity;

/**
 * EvBatterySize
 */
class EvBatterySize
{
    /**
     * @var integer
     */
    private $evBatteryId;

    /**
     * @var integer
     */
    private $evBatterySize;

    /**
     * @var string
     */
    private $evBatteryName;


    /**
     * Get evBatteryId
     *
     * @return integer
     */
    public function getEvBatteryId()
    {
        return $this->evBatteryId;
    }

    /**
     * Set evBatterySize
     *
     * @param integer $evBatterySize
     *
     * @return EvBatterySize
     */
    public function setEvBatterySize($evBatterySize)
    {
        $this->evBatterySize = $evBatterySize;

        return $this;
    }

    /**
     * Get evBatterySize
     *
     * @return integer
     */
    public function getEvBatterySize()
    {
        return $this->evBatterySize;
    }

    /**
     * Set evBatteryName
     *
     * @param string $evBatteryName
     *
     * @return EvBatterySize
     */
    public function setEvBatteryName($evBatteryName)
    {
        $this->evBatteryName = $evBatteryName;

        return $this;
    }

    /**
     * Get evBatteryName
     *
     * @return string
     */
    public function getEvBatteryName()
    {
        return $this->evBatteryName;
    }
    /**
     * @var \EvBundle\Entity\EvManufactorMaster
     */
    private $evManufactor;


    /**
     * Set evManufactor
     *
     * @param \EvBundle\Entity\EvManufactorMaster $evManufactor
     *
     * @return EvBatterySize
     */
    public function setEvManufactor(\EvBundle\Entity\EvManufactorMaster $evManufactor = null)
    {
        $this->evManufactor = $evManufactor;

        return $this;
    }

    /**
     * Get evManufactor
     *
     * @return \EvBundle\Entity\EvManufactorMaster
     */
    public function getEvManufactor()
    {
        return $this->evManufactor;
    }
}
