<?php

namespace EvBundle\Entity;

/**
 * EvChargingLogMonthly
 */
class EvChargingLogMonthly
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $chargingStationId;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var float
     */
    private $totalKwh;

    /**
     * @var float
     */
    private $basicCost;

    /**
     * @var float
     */
    private $amountCost;

    /**
     * @var float
     */
    private $carbonReduction;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set chargingStationId
     *
     * @param string $chargingStationId
     *
     * @return EvChargingLogMonthly
     */
    public function setChargingStationId($chargingStationId)
    {
        $this->chargingStationId = $chargingStationId;

        return $this;
    }

    /**
     * Get chargingStationId
     *
     * @return string
     */
    public function getChargingStationId()
    {
        return $this->chargingStationId;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return EvChargingLogMonthly
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set totalKwh
     *
     * @param float $totalKwh
     *
     * @return EvChargingLogMonthly
     */
    public function setTotalKwh($totalKwh)
    {
        $this->totalKwh = $totalKwh;

        return $this;
    }

    /**
     * Get totalKwh
     *
     * @return float
     */
    public function getTotalKwh()
    {
        return $this->totalKwh;
    }

    /**
     * Set basicCost
     *
     * @param float $basicCost
     *
     * @return EvChargingLogMonthly
     */
    public function setBasicCost($basicCost)
    {
        $this->basicCost = $basicCost;

        return $this;
    }

    /**
     * Get basicCost
     *
     * @return float
     */
    public function getBasicCost()
    {
        return $this->basicCost;
    }

    /**
     * Set amountCost
     *
     * @param float $amountCost
     *
     * @return EvChargingLogMonthly
     */
    public function setAmountCost($amountCost)
    {
        $this->amountCost = $amountCost;

        return $this;
    }

    /**
     * Get amountCost
     *
     * @return float
     */
    public function getAmountCost()
    {
        return $this->amountCost;
    }

    /**
     * Set carbonReduction
     *
     * @param float $carbonReduction
     *
     * @return EvChargingLogMonthly
     */
    public function setCarbonReduction($carbonReduction)
    {
        $this->carbonReduction = $carbonReduction;

        return $this;
    }

    /**
     * Get carbonReduction
     *
     * @return float
     */
    public function getCarbonReduction()
    {
        return $this->carbonReduction;
    }
    /**
     * @var string
     */
    private $orderId;


    /**
     * Set orderId
     *
     * @param string $orderId
     *
     * @return EvChargingLogMonthly
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;

        return $this;
    }

    /**
     * Get orderId
     *
     * @return string
     */
    public function getOrderId()
    {
        return $this->orderId;
    }
    /**
     * @var \EvBundle\Entity\PayHistory
     */
    private $order;


    /**
     * Set order
     *
     * @param \EvBundle\Entity\PayHistory $order
     *
     * @return EvChargingLogMonthly
     */
    public function setOrder(\EvBundle\Entity\PayHistory $order = null)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return \EvBundle\Entity\PayHistory
     */
    public function getOrder()
    {
        return $this->order;
    }

    public function getOrderStatus()
    {
        if ( $this->order ) {
            return $this->order->getStatus();
        }
        return -1;
    }
    /**
     * @var float|null
     */
    private $startKwh;

    /**
     * @var float|null
     */
    private $endKwh;


    /**
     * Set startKwh.
     *
     * @param float|null $startKwh
     *
     * @return EvChargingLogMonthly
     */
    public function setStartKwh($startKwh = null)
    {
        $this->startKwh = $startKwh;

        return $this;
    }

    /**
     * Get startKwh.
     *
     * @return float|null
     */
    public function getStartKwh()
    {
        return $this->startKwh;
    }

    /**
     * Set endKwh.
     *
     * @param float|null $endKwh
     *
     * @return EvChargingLogMonthly
     */
    public function setEndKwh($endKwh = null)
    {
        $this->endKwh = $endKwh;

        return $this;
    }

    /**
     * Get endKwh.
     *
     * @return float|null
     */
    public function getEndKwh()
    {
        return $this->endKwh;
    }
    /**
     * @var int|null
     */
    private $sessionKwh;

    /**
     * @var int|null
     */
    private $sessionAmount;


    /**
     * Set sessionKwh.
     *
     * @param int|null $sessionKwh
     *
     * @return EvChargingLogMonthly
     */
    public function setSessionKwh($sessionKwh = null)
    {
        $this->sessionKwh = $sessionKwh;

        return $this;
    }

    /**
     * Get sessionKwh.
     *
     * @return int|null
     */
    public function getSessionKwh()
    {
        return $this->sessionKwh;
    }

    /**
     * Set sessionAmount.
     *
     * @param int|null $sessionAmount
     *
     * @return EvChargingLogMonthly
     */
    public function setSessionAmount($sessionAmount = null)
    {
        $this->sessionAmount = $sessionAmount;

        return $this;
    }

    /**
     * Get sessionAmount.
     *
     * @return int|null
     */
    public function getSessionAmount()
    {
        return $this->sessionAmount;
    }
    /**
     * @var \DateTime|null
     */
    private $updatedAt ;

    /**
     * @var int|null
     */
    private $orderReady = '0';


    /**
     * Set updatedAt.
     *
     * @param \DateTime|null $updatedAt
     *
     * @return EvChargingLogMonthly
     */
    public function setUpdatedAt($updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set orderReady.
     *
     * @param int|null $orderReady
     *
     * @return EvChargingLogMonthly
     */
    public function setOrderReady($orderReady = null)
    {
        $this->orderReady = $orderReady;

        return $this;
    }

    /**
     * Get orderReady.
     *
     * @return int|null
     */
    public function getOrderReady()
    {
        return $this->orderReady;
    }
    
    public function doUpdate()
    {
        $this->updatedAt = new \Datetime();
    }
}
