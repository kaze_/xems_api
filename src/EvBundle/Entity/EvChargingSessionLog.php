<?php

namespace EvBundle\Entity;

use AppBundle\Entity\EmsCircuitMaster;


/**
 * EvChargingSessionLog
 */
class EvChargingSessionLog
{
    /**
     * @var string
     */
    private $sessionId;

    /**
     * @var string
     */
    private $kaaEndpointHash;

    /**
     * @var string
     */
    private $userId;

    /**
     * @var \DateTime
     */
    private $startAt = 'CURRENT_TIMESTAMP';

    /**
     * @var integer
     */
    private $startKwh;

    /**
     * @var \DateTime
     */
    private $endAt;

    /**
     * @var integer
     */
    private $endKwh;

    /**
     * @var integer
     */
    private $totalKwh;

    /**
     * @var \AppBundle\Entity\EmsCircuitMaster
     */
    private $circuit;


    /**
     * Get sessionId
     *
     * @return string
     */
    public function getSessionId()
    {
        return $this->sessionId;
    }

    /**
     * Set kaaEndpointHash
     *
     * @param string $kaaEndpointHash
     *
     * @return EvChargingSessionLog
     */
    public function setKaaEndpointHash($kaaEndpointHash)
    {
        $this->kaaEndpointHash = $kaaEndpointHash;

        return $this;
    }

    /**
     * Get kaaEndpointHash
     *
     * @return string
     */
    public function getKaaEndpointHash()
    {
        return $this->kaaEndpointHash;
    }

    /**
     * Set userId
     *
     * @param string $userId
     *
     * @return EvChargingSessionLog
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return string
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set startAt
     *
     * @param \DateTime $startAt
     *
     * @return EvChargingSessionLog
     */
    public function setStartAt($startAt)
    {
        $this->startAt = $startAt;

        return $this;
    }

    /**
     * Get startAt
     *
     * @return \DateTime
     */
    public function getStartAt()
    {
        return $this->startAt;
    }

    /**
     * Set startKwh
     *
     * @param integer $startKwh
     *
     * @return EvChargingSessionLog
     */
    public function setStartKwh($startKwh)
    {
        $this->startKwh = $startKwh;

        return $this;
    }

    /**
     * Get startKwh
     *
     * @return integer
     */
    public function getStartKwh()
    {
        return $this->startKwh;
    }

    /**
     * Set endAt
     *
     * @param \DateTime $endAt
     *
     * @return EvChargingSessionLog
     */
    public function setEndAt($endAt)
    {
        $this->endAt = $endAt;

        return $this;
    }

    /**
     * Get endAt
     *
     * @return \DateTime
     */
    public function getEndAt()
    {
        return $this->endAt;
    }

    /**
     * Set endKwh
     *
     * @param integer $endKwh
     *
     * @return EvChargingSessionLog
     */
    public function setEndKwh($endKwh)
    {
        $this->endKwh = $endKwh;

        return $this;
    }

    /**
     * Get endKwh
     *
     * @return integer
     */
    public function getEndKwh()
    {
        return $this->endKwh;
    }

    /**
     * Set totalKwh
     *
     * @param integer $totalKwh
     *
     * @return EvChargingSessionLog
     */
    public function setTotalKwh($totalKwh)
    {
        $this->totalKwh = $totalKwh;

        return $this;
    }

    /**
     * Get totalKwh
     *
     * @return integer
     */
    public function getTotalKwh()
    {
        return $this->totalKwh;
    }

    /**
     * Set circuit
     *
     * @param \AppBundle\Entity\EmsCircuitMaster $circuit
     *
     * @return EvChargingSessionLog
     */
    public function setCircuit(EmsCircuitMaster $circuit = null)
    {
        $this->circuit = $circuit;

        return $this;
    }

    /**
     * Get circuit
     *
     * @return \AppBundle\Entity\EmsCircuitMaster
     */
    public function getCircuit()
    {
        return $this->circuit;
    }
    /**
     * @var integer
     */
    private $cost = '0';


    /**
     * Set cost
     *
     * @param integer $cost
     *
     * @return EvChargingSessionLog
     */
    public function setCost($cost)
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * Get cost
     *
     * @return integer
     */
    public function getCost()
    {
        return $this->cost;
    }
    /**
     * @var \AppBundle\Entity\EvChargingStationMaster
     */
    private $chargingStation;


    /**
     * Set chargingStation
     *
     * @param \AppBundle\Entity\EvChargingStationMaster $chargingStation
     *
     * @return EvChargingSessionLog
     */
    public function setChargingStation(\AppBundle\Entity\EvChargingStationMaster $chargingStation = null)
    {
        $this->chargingStation = $chargingStation;

        return $this;
    }

    /**
     * Get chargingStation
     *
     * @return \AppBundle\Entity\EvChargingStationMaster
     */
    public function getChargingStation()
    {
        return $this->chargingStation;
    }
    /**
     * @var integer
     */
    private $basicCost = '0';

    /**
     * @var integer
     */
    private $amountCost = '0';


    /**
     * Set basicCost
     *
     * @param integer $basicCost
     *
     * @return EvChargingSessionLog
     */
    public function setBasicCost($basicCost)
    {
        $this->basicCost = $basicCost;

        return $this;
    }

    /**
     * Get basicCost
     *
     * @return integer
     */
    public function getBasicCost()
    {
        return $this->basicCost;
    }

    /**
     * Set amountCost
     *
     * @param integer $amountCost
     *
     * @return EvChargingSessionLog
     */
    public function setAmountCost($amountCost)
    {
        $this->amountCost = $amountCost;

        return $this;
    }

    /**
     * Get amountCost
     *
     * @return integer
     */
    public function getAmountCost()
    {
        return $this->amountCost;
    }
    /**
     * @var string
     */
    private $chargingStationId;

    /**
     * @var string
     */
    private $ownerId;


    /**
     * Set chargingStationId
     *
     * @param string $chargingStationId
     *
     * @return EvChargingSessionLog
     */
    public function setChargingStationId($chargingStationId)
    {
        $this->chargingStationId = $chargingStationId;

        return $this;
    }

    /**
     * Get chargingStationId
     *
     * @return string
     */
    public function getChargingStationId()
    {
        return $this->chargingStationId;
    }

    /**
     * Set ownerId
     *
     * @param string $ownerId
     *
     * @return EvChargingSessionLog
     */
    public function setOwnerId($ownerId)
    {
        $this->ownerId = $ownerId;

        return $this;
    }

    /**
     * Get ownerId
     *
     * @return string
     */
    public function getOwnerId()
    {
        return $this->ownerId;
    }
    /**
     * @var \AppBundle\Entity\User
     */
    private $owner;

    /**
     * @var \AppBundle\Entity\User
     */
    private $user;


    /**
     * Set owner
     *
     * @param \AppBundle\Entity\User $owner
     *
     * @return EvChargingSessionLog
     */
    public function setOwner(\AppBundle\Entity\User $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \AppBundle\Entity\User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return EvChargingSessionLog
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
    /**
     * @var string|null
     */
    private $evPlate;

    /**
     * @var float|null
     */
    private $carbonReduction;


    /**
     * Set evPlate.
     *
     * @param string|null $evPlate
     *
     * @return EvChargingSessionLog
     */
    public function setEvPlate($evPlate = null)
    {
        $this->evPlate = $evPlate;

        return $this;
    }

    /**
     * Get evPlate.
     *
     * @return string|null
     */
    public function getEvPlate()
    {
        return $this->evPlate;
    }

    /**
     * Set carbonReduction.
     *
     * @param float|null $carbonReduction
     *
     * @return EvChargingSessionLog
     */
    public function setCarbonReduction($carbonReduction = null)
    {
        $this->carbonReduction = $carbonReduction;

        return $this;
    }

    /**
     * Get carbonReduction.
     *
     * @return float|null
     */
    public function getCarbonReduction()
    {
        return $this->carbonReduction;
    }



    /**
     * @var integer
     */
    private $oId = null;

    /**
     * Get getOId
     *
     * @return integer
     */
    public function getOId()
    {
        return $this->oId;
    }



    /**
     * Set oId.
     *
     * @param int $oId
     *
     * @return EvChargingSessionLog
     */
    public function setOId($oId)
    {
        $this->oId = $oId;

        return $this;
    }
    /**
     * @var float|null
     */
    private $maxNormalDemand;

    /**
     * @var float|null
     */
    private $maxSaturdaySemiPeakDemand;

    /**
     * @var float|null
     */
    private $maxOffPeakDemand;

    /**
     * @var float|null
     */
    private $maxSemiPeakDemand;


    /**
     * Set maxNormalDemand.
     *
     * @param float|null $maxNormalDemand
     *
     * @return EvChargingSessionLog
     */
    public function setMaxNormalDemand($maxNormalDemand = null)
    {
        $this->maxNormalDemand = $maxNormalDemand;

        return $this;
    }

    /**
     * Get maxNormalDemand.
     *
     * @return float|null
     */
    public function getMaxNormalDemand()
    {
        return $this->maxNormalDemand;
    }

    /**
     * Set maxSaturdaySemiPeakDemand.
     *
     * @param float|null $maxSaturdaySemiPeakDemand
     *
     * @return EvChargingSessionLog
     */
    public function setMaxSaturdaySemiPeakDemand($maxSaturdaySemiPeakDemand = null)
    {
        $this->maxSaturdaySemiPeakDemand = $maxSaturdaySemiPeakDemand;

        return $this;
    }

    /**
     * Get maxSaturdaySemiPeakDemand.
     *
     * @return float|null
     */
    public function getMaxSaturdaySemiPeakDemand()
    {
        return $this->maxSaturdaySemiPeakDemand;
    }

    /**
     * Set maxOffPeakDemand.
     *
     * @param float|null $maxOffPeakDemand
     *
     * @return EvChargingSessionLog
     */
    public function setMaxOffPeakDemand($maxOffPeakDemand = null)
    {
        $this->maxOffPeakDemand = $maxOffPeakDemand;

        return $this;
    }

    /**
     * Get maxOffPeakDemand.
     *
     * @return float|null
     */
    public function getMaxOffPeakDemand()
    {
        return $this->maxOffPeakDemand;
    }

    /**
     * Set maxSemiPeakDemand.
     *
     * @param float|null $maxSemiPeakDemand
     *
     * @return EvChargingSessionLog
     */
    public function setMaxSemiPeakDemand($maxSemiPeakDemand = null)
    {
        $this->maxSemiPeakDemand = $maxSemiPeakDemand;

        return $this;
    }

    /**
     * Get maxSemiPeakDemand.
     *
     * @return float|null
     */
    public function getMaxSemiPeakDemand()
    {
        return $this->maxSemiPeakDemand;
    }
    /**
     * @var float|null
     */
    private $maxPeakDemand;

    /**
     * @var float|null
     */
    private $peakKwh;

    /**
     * @var float|null
     */
    private $saturdaySemiPeakKwh;

    /**
     * @var float|null
     */
    private $semiPeakKwh;

    /**
     * @var float|null
     */
    private $offPeakKwh;


    /**
     * Set maxPeakDemand.
     *
     * @param float|null $maxPeakDemand
     *
     * @return EvChargingSessionLog
     */
    public function setMaxPeakDemand($maxPeakDemand = null)
    {
        $this->maxPeakDemand = $maxPeakDemand;

        return $this;
    }

    /**
     * Get maxPeakDemand.
     *
     * @return float|null
     */
    public function getMaxPeakDemand()
    {
        return $this->maxPeakDemand;
    }

    /**
     * Set peakKwh.
     *
     * @param float|null $peakKwh
     *
     * @return EvChargingSessionLog
     */
    public function setPeakKwh($peakKwh = null)
    {
        $this->peakKwh = $peakKwh;

        return $this;
    }

    /**
     * Get peakKwh.
     *
     * @return float|null
     */
    public function getPeakKwh()
    {
        return $this->peakKwh;
    }

    /**
     * Set saturdaySemiPeakKwh.
     *
     * @param float|null $saturdaySemiPeakKwh
     *
     * @return EvChargingSessionLog
     */
    public function setSaturdaySemiPeakKwh($saturdaySemiPeakKwh = null)
    {
        $this->saturdaySemiPeakKwh = $saturdaySemiPeakKwh;

        return $this;
    }

    /**
     * Get saturdaySemiPeakKwh.
     *
     * @return float|null
     */
    public function getSaturdaySemiPeakKwh()
    {
        return $this->saturdaySemiPeakKwh;
    }

    /**
     * Set semiPeakKwh.
     *
     * @param float|null $semiPeakKwh
     *
     * @return EvChargingSessionLog
     */
    public function setSemiPeakKwh($semiPeakKwh = null)
    {
        $this->semiPeakKwh = $semiPeakKwh;

        return $this;
    }

    /**
     * Get semiPeakKwh.
     *
     * @return float|null
     */
    public function getSemiPeakKwh()
    {
        return $this->semiPeakKwh;
    }

    /**
     * Set offPeakKwh.
     *
     * @param float|null $offPeakKwh
     *
     * @return EvChargingSessionLog
     */
    public function setOffPeakKwh($offPeakKwh = null)
    {
        $this->offPeakKwh = $offPeakKwh;

        return $this;
    }

    /**
     * Get offPeakKwh.
     *
     * @return float|null
     */
    public function getOffPeakKwh()
    {
        return $this->offPeakKwh;
    }
}
