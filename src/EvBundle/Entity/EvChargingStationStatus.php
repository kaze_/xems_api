<?php

namespace EvBundle\Entity;

/**
 * EvChargingStationStatus
 */
class EvChargingStationStatus
{
    /**
     * @var string
     */
    private $circuitId;

    /**
     * @var string|null
     */
    private $chargingStationId;

    /**
     * @var \DateTime|null
     */
    private $updateTime;

    /**
     * @var \DateTime|null
     */
    private $lastPlugStatusTime;

    /**
     * @var string|null
     */
    private $plugStatus;

    /**
     * @var float|null
     */
    private $accPower;

    /**
     * @var float|null
     */
    private $circuitPower;

    /**
     * @var \DateTime|null
     */
    private $createdAt;

    /**
     * @var int
     */
    private $id;


    /**
     * Set circuitId.
     *
     * @param string $circuitId
     *
     * @return EvChargingStationStatus
     */
    public function setCircuitId($circuitId)
    {
        $this->circuitId = $circuitId;

        return $this;
    }

    /**
     * Get circuitId.
     *
     * @return string
     */
    public function getCircuitId()
    {
        return $this->circuitId;
    }

    /**
     * Set chargingStationId.
     *
     * @param string|null $chargingStationId
     *
     * @return EvChargingStationStatus
     */
    public function setChargingStationId($chargingStationId = null)
    {
        $this->chargingStationId = $chargingStationId;

        return $this;
    }

    /**
     * Get chargingStationId.
     *
     * @return string|null
     */
    public function getChargingStationId()
    {
        return $this->chargingStationId;
    }

    /**
     * Set updateTime.
     *
     * @param \DateTime|null $updateTime
     *
     * @return EvChargingStationStatus
     */
    public function setUpdateTime($updateTime = null)
    {
        $this->updateTime = $updateTime;

        return $this;
    }

    /**
     * Get updateTime.
     *
     * @return \DateTime|null
     */
    public function getUpdateTime()
    {
        return $this->updateTime;
    }

    /**
     * Set lastPlugStatusTime.
     *
     * @param \DateTime|null $lastPlugStatusTime
     *
     * @return EvChargingStationStatus
     */
    public function setLastPlugStatusTime($lastPlugStatusTime = null)
    {
        $this->lastPlugStatusTime = $lastPlugStatusTime;

        return $this;
    }

    /**
     * Get lastPlugStatusTime.
     *
     * @return \DateTime|null
     */
    public function getLastPlugStatusTime()
    {
        return $this->lastPlugStatusTime;
    }

    /**
     * Set plugStatus.
     *
     * @param string|null $plugStatus
     *
     * @return EvChargingStationStatus
     */
    public function setPlugStatus($plugStatus = null)
    {
        $this->plugStatus = $plugStatus;

        return $this;
    }

    /**
     * Get plugStatus.
     *
     * @return string|null
     */
    public function getPlugStatus()
    {
        return $this->plugStatus;
    }

    /**
     * Set accPower.
     *
     * @param float|null $accPower
     *
     * @return EvChargingStationStatus
     */
    public function setAccPower($accPower = null)
    {
        $this->accPower = $accPower;

        return $this;
    }

    /**
     * Get accPower.
     *
     * @return float|null
     */
    public function getAccPower()
    {
        return $this->accPower;
    }

    /**
     * Set circuitPower.
     *
     * @param float|null $circuitPower
     *
     * @return EvChargingStationStatus
     */
    public function setCircuitPower($circuitPower = null)
    {
        $this->circuitPower = $circuitPower;

        return $this;
    }

    /**
     * Get circuitPower.
     *
     * @return float|null
     */
    public function getCircuitPower()
    {
        return $this->circuitPower;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime|null $createdAt
     *
     * @return EvChargingStationStatus
     */
    public function setCreatedAt($createdAt = null)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime|null
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @var string|null
     */
    private $gatewayId;


    /**
     * Set gatewayId.
     *
     * @param string|null $gatewayId
     *
     * @return EvChargingStationStatus
     */
    public function setGatewayId($gatewayId = null)
    {
        $this->gatewayId = $gatewayId;

        return $this;
    }

    /**
     * Get gatewayId.
     *
     * @return string|null
     */
    public function getGatewayId()
    {
        return $this->gatewayId;
    }
}
