<?php

namespace EvBundle\Entity;

/**
 * EvDataVersion
 */
class EvDataVersion
{
    /**
     * @var string|null
     */
    private $evDataName;

    /**
     * @var string|null
     */
    private $tableName;

    /**
     * @var string|null
     */
    private $tableDescription;

    /**
     * @var string|null
     */
    private $version;

    /**
     * @var \DateTime|null
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     */
    private $updatedAt;

    /**
     * @var int
     */
    private $evDataId;


    /**
     * Set evDataName.
     *
     * @param string|null $evDataName
     *
     * @return EvDataVersion
     */
    public function setEvDataName($evDataName = null)
    {
        $this->evDataName = $evDataName;

        return $this;
    }

    /**
     * Get evDataName.
     *
     * @return string|null
     */
    public function getEvDataName()
    {
        return $this->evDataName;
    }

    /**
     * Set tableName.
     *
     * @param string|null $tableName
     *
     * @return EvDataVersion
     */
    public function setTableName($tableName = null)
    {
        $this->tableName = $tableName;

        return $this;
    }

    /**
     * Get tableName.
     *
     * @return string|null
     */
    public function getTableName()
    {
        return $this->tableName;
    }

    /**
     * Set tableDescription.
     *
     * @param string|null $tableDescription
     *
     * @return EvDataVersion
     */
    public function setTableDescription($tableDescription = null)
    {
        $this->tableDescription = $tableDescription;

        return $this;
    }

    /**
     * Get tableDescription.
     *
     * @return string|null
     */
    public function getTableDescription()
    {
        return $this->tableDescription;
    }

    /**
     * Set version.
     *
     * @param string|null $version
     *
     * @return EvDataVersion
     */
    public function setVersion($version = null)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * Get version.
     *
     * @return string|null
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime|null $createdAt
     *
     * @return EvDataVersion
     */
    public function setCreatedAt($createdAt = null)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime|null
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime|null $updatedAt
     *
     * @return EvDataVersion
     */
    public function setUpdatedAt($updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Get evDataId.
     *
     * @return int
     */
    public function getEvDataId()
    {
        return $this->evDataId;
    }
}
