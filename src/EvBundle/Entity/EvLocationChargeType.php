<?php

namespace EvBundle\Entity;

/**
 * EvLocationChargeType
 */
class EvLocationChargeType
{
    
    private $id;
    private $name;
    private $comment;
    private $createdAt;
    private $updatedAt;


    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }
    
	public function getId()
    {
        return $this->id;
    }
	
	public function getName()
    {
        return $this->name;
    }
	
	public function getCreatedAt()
    {
        return $this->createdAt;
    }
	
	public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt= $updatedAt;

        return $this;
    }
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
	
	
}

