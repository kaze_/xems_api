<?php

namespace EvBundle\Entity;

/**
 * EvLocationChargingGuest
 */
class EvLocationChargingGuest
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $locationId;

    /**
     * @var string|null
     */
    private $userId;

    /**
     * @var string|null
     */
    private $phone;

    /**
     * @var string|null
     */
    private $visitUnit;

    /**
     * @var string|null
     */
    private $preCharge;

    /**
     * @var string|null
     */
    private $sessionId;

    /**
     * @var string|null
     */
    private $chargingFee;

    /**
     * @var int
     */
    private $checked = '0';

    /**
     * @var \DateTime|null
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     */
    private $updatedAt;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set locationId.
     *
     * @param string $locationId
     *
     * @return EvLocationChargingGuest
     */
    public function setLocationId($locationId)
    {
        $this->locationId = $locationId;

        return $this;
    }

    /**
     * Get locationId.
     *
     * @return string
     */
    public function getLocationId()
    {
        return $this->locationId;
    }

    /**
     * Set userId.
     *
     * @param string|null $userId
     *
     * @return EvLocationChargingGuest
     */
    public function setUserId($userId = null)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId.
     *
     * @return string|null
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set phone.
     *
     * @param string|null $phone
     *
     * @return EvLocationChargingGuest
     */
    public function setPhone($phone = null)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone.
     *
     * @return string|null
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set visitUnit.
     *
     * @param string|null $visitUnit
     *
     * @return EvLocationChargingGuest
     */
    public function setVisitUnit($visitUnit = null)
    {
        $this->visitUnit = $visitUnit;

        return $this;
    }

    /**
     * Get visitUnit.
     *
     * @return string|null
     */
    public function getVisitUnit()
    {
        return $this->visitUnit;
    }

    /**
     * Set preCharge.
     *
     * @param string|null $preCharge
     *
     * @return EvLocationChargingGuest
     */
    public function setPreCharge($preCharge = null)
    {
        $this->preCharge = $preCharge;

        return $this;
    }

    /**
     * Get preCharge.
     *
     * @return string|null
     */
    public function getPreCharge()
    {
        return $this->preCharge;
    }

    /**
     * Set sessionId.
     *
     * @param string|null $sessionId
     *
     * @return EvLocationChargingGuest
     */
    public function setSessionId($sessionId = null)
    {
        $this->sessionId = $sessionId;

        return $this;
    }

    /**
     * Get sessionId.
     *
     * @return string|null
     */
    public function getSessionId()
    {
        return $this->sessionId;
    }

    /**
     * Set chargingFee.
     *
     * @param string|null $chargingFee
     *
     * @return EvLocationChargingGuest
     */
    public function setChargingFee($chargingFee = null)
    {
        $this->chargingFee = $chargingFee;

        return $this;
    }

    /**
     * Get chargingFee.
     *
     * @return string|null
     */
    public function getChargingFee()
    {
        return $this->chargingFee;
    }

    /**
     * Set checked.
     *
     * @param int $checked
     *
     * @return EvLocationChargingGuest
     */
    public function setChecked($checked)
    {
        $this->checked = $checked;

        return $this;
    }

    /**
     * Get checked.
     *
     * @return int
     */
    public function getChecked()
    {
        return $this->checked;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime|null $createdAt
     *
     * @return EvLocationChargingGuest
     */
    public function setCreatedAt($createdAt = null)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime|null
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime|null $updatedAt
     *
     * @return EvLocationChargingGuest
     */
    public function setUpdatedAt($updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
    /**
     * @var string|null
     */
    private $evPlate;


    /**
     * Set evPlate.
     *
     * @param string|null $evPlate
     *
     * @return EvLocationChargingGuest
     */
    public function setEvPlate($evPlate = null)
    {
        $this->evPlate = $evPlate;

        return $this;
    }

    /**
     * Get evPlate.
     *
     * @return string|null
     */
    public function getEvPlate()
    {
        return $this->evPlate;
    }
}
