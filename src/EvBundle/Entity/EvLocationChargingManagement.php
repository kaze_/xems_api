<?php

namespace EvBundle\Entity;

/**
 * EvLocationChargingManagement
 */
class EvLocationChargingManagement
{
    
    private $id;
    private $emsLocationId;
    private $optimizedNormalContract;
    private $optimizedSaturdaySemiContract;
    private $optimizedOffPeakContract;
    private $evNormalContract;
    private $evSaturdaySemiContract;
    private $evOffPeakContract;
	private $evChargingStrategyId;
    private $evChargeTypeId;
    private $evChargeTypeParameter;
	private $effectiveAt;
    private $createdAt;
    private $updatedAt;


    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }
    

    public function getId()
    {
        return $this->id;
    }
	
	public function setEmsLocationId($emsLocationId)
    {
        $this->emsLocationId = $emsLocationId;

        return $this;
    }
    public function getEmsLocationId()
    {
        return $this->emsLocationId;
    }
	
	
	public function setOptimizedNormalContract($optimizedNormalContract)
    {
        $this->optimizedNormalContract = $optimizedNormalContract;

        return $this;
    }
	public function getOptimizedNormalContract()
    {
        return $this->optimizedNormalContract;
    }
	
	public function setOptimizedSaturdaySemiContract($optimizedSaturdaySemiContract)
    {
        $this->optimizedSaturdaySemiContract = $optimizedSaturdaySemiContract;

        return $this;
    }
    public function getOptimizedSaturdaySemiContract()
    {
        return $this->optimizedSaturdaySemiContract;
    }
	
	
	public function setOptimizedOffPeakContract($optimizedOffPeakContract)
    {
        $this->optimizedOffPeakContract = $optimizedOffPeakContract;

        return $this;
    }
    public function getOptimizedOffPeakContract()
    {
        return $this->optimizedOffPeakContract;
    }
	
	
	public function setEvNormalContract($evNormalContract)
    {
        $this->evNormalContract = $evNormalContract;

        return $this;
    }
    public function getEvNormalContract()
    {
        return $this->evNormalContract;
    }
	
	public function setEvSaturdaySemiContract($evSaturdaySemiContract)
    {
        $this->evSaturdaySemiContract = $evSaturdaySemiContract;

        return $this;
    }
    public function getEvSaturdaySemiContract()
    {
        return $this->evSaturdaySemiContract;
    }
	
	public function setEvOffPeakContract($evOffPeakContract)
    {
        $this->evOffPeakContract = $evOffPeakContract;

        return $this;
    }
    public function getEvOffPeakContract()
    {
        return $this->evOffPeakContract;
    }
	

	public function setEvChargingStrategyId($evChargingStrategyId)
    {
        $this->evChargingStrategyId = $evChargingStrategyId;

        return $this;
    }
    public function getEvChargingStrategyId()
    {
        return $this->evChargingStrategyId;
    }
	
	public function setEvChargeTypeId($evChargeTypeId)
    {
        $this->evChargeTypeId = $evChargeTypeId;

        return $this;
    }
    public function getEvChargeTypeId()
    {
        return $this->evChargeTypeId;
    }
	
	public function setEvChargeTypeParameter($evChargeTypeParameter)
    {
        $this->evChargeTypeParameter = $evChargeTypeParameter;

        return $this;
    }
    public function getEvChargeTypeParameter()
    {
        return $this->evChargeTypeParameter;
    }
	
	public function setEffectiveAt($effectiveAt)
    {
        $this->effectiveAt = $effectiveAt;

        return $this;
    }
    public function getEffectiveAt()
    {
        return $this->effectiveAt;
    }
	

    public function getCreatedAt()
    {
        return $this->createdAt;
    }
	
	public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt= $updatedAt;

        return $this;
    }
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
    /**
     * @var float|null
     */
    private $optimizedSemiContract = '0';

    /**
     * @var float|null
     */
    private $evSemiContract = '0';


    /**
     * Set optimizedSemiContract.
     *
     * @param float|null $optimizedSemiContract
     *
     * @return EvLocationChargingManagement
     */
    public function setOptimizedSemiContract($optimizedSemiContract = null)
    {
        $this->optimizedSemiContract = $optimizedSemiContract;

        return $this;
    }

    /**
     * Get optimizedSemiContract.
     *
     * @return float|null
     */
    public function getOptimizedSemiContract()
    {
        return $this->optimizedSemiContract;
    }

    /**
     * Set evSemiContract.
     *
     * @param float|null $evSemiContract
     *
     * @return EvLocationChargingManagement
     */
    public function setEvSemiContract($evSemiContract = null)
    {
        $this->evSemiContract = $evSemiContract;

        return $this;
    }

    /**
     * Get evSemiContract.
     *
     * @return float|null
     */
    public function getEvSemiContract()
    {
        return $this->evSemiContract;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime|null $createdAt
     *
     * @return EvLocationChargingManagement
     */
    public function setCreatedAt($createdAt = null)
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
