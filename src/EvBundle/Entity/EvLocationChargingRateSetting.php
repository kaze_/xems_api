<?php

namespace EvBundle\Entity;

/**
 * EvLocationChargingRateSetting
 */
class EvLocationChargingRateSetting
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $evLocationId;

    /**
     * @var int|null
     */
    private $evRateTypeId;

    /**
     * @var string|null
     */
    private $evRateParameter;

    /**
     * @var int|null
     */
    private $enabled = '1';

    /**
     * @var \DateTime|null
     */
    private $effectiveAt;

    /**
     * @var \DateTime|null
     */
    private $createdAt;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set evLocationId.
     *
     * @param string $evLocationId
     *
     * @return EvLocationChargingRateSetting
     */
    public function setEvLocationId($evLocationId)
    {
        $this->evLocationId = $evLocationId;

        return $this;
    }

    /**
     * Get evLocationId.
     *
     * @return string
     */
    public function getEvLocationId()
    {
        return $this->evLocationId;
    }

    /**
     * Set evRateTypeId.
     *
     * @param int|null $evRateTypeId
     *
     * @return EvLocationChargingRateSetting
     */
    public function setEvRateTypeId($evRateTypeId = null)
    {
        $this->evRateTypeId = $evRateTypeId;

        return $this;
    }

    /**
     * Get evRateTypeId.
     *
     * @return int|null
     */
    public function getEvRateTypeId()
    {
        return $this->evRateTypeId;
    }

    /**
     * Set evRateParameter.
     *
     * @param string|null $evRateParameter
     *
     * @return EvLocationChargingRateSetting
     */
    public function setEvRateParameter($evRateParameter = null)
    {
        $this->evRateParameter = $evRateParameter;

        return $this;
    }

    /**
     * Get evRateParameter.
     *
     * @return string|null
     */
    public function getEvRateParameter()
    {
        return $this->evRateParameter;
    }

    /**
     * Set enabled.
     *
     * @param int|null $enabled
     *
     * @return EvLocationChargingRateSetting
     */
    public function setEnabled($enabled = null)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled.
     *
     * @return int|null
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set effectiveAt.
     *
     * @param \DateTime|null $effectiveAt
     *
     * @return EvLocationChargingRateSetting
     */
    public function setEffectiveAt($effectiveAt = null)
    {
        $this->effectiveAt = $effectiveAt;

        return $this;
    }

    /**
     * Get effectiveAt.
     *
     * @return \DateTime|null
     */
    public function getEffectiveAt()
    {
        return $this->effectiveAt;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime|null $createdAt
     *
     * @return EvLocationChargingRateSetting
     */
    public function setCreatedAt($createdAt = null)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime|null
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
    /**
     * @var \DateTime|null
     */
    private $updatedAt;


    /**
     * Set updatedAt.
     *
     * @param \DateTime|null $updatedAt
     *
     * @return EvLocationChargingRateSetting
     */
    public function setUpdatedAt($updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
