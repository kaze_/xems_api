<?php

namespace EvBundle\Entity;

/**
 * EvLocationPointsLog
 */
class EvLocationPointsLog
{
    /**
     * @var string
     */
    private $userId;

    /**
     * @var string
     */
    private $locationId;

    /**
     * @var int|null
     */
    private $points;

    /**
     * @var int|null
     */
    private $pointsLeft;

    /**
     * @var string|null
     */
    private $action = 'DEPOSIT/WITHDRAW';

    /**
     * @var string|null
     */
    private $comment;

    /**
     * @var string|null
     */
    private $loggedAt;


    /**
     * Set userId.
     *
     * @param string $userId
     *
     * @return EvLocationPointsLog
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId.
     *
     * @return string
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set locationId.
     *
     * @param string $locationId
     *
     * @return EvLocationPointsLog
     */
    public function setLocationId($locationId)
    {
        $this->locationId = $locationId;

        return $this;
    }

    /**
     * Get locationId.
     *
     * @return string
     */
    public function getLocationId()
    {
        return $this->locationId;
    }

    /**
     * Set points.
     *
     * @param int|null $points
     *
     * @return EvLocationPointsLog
     */
    public function setPoints($points = null)
    {
        $this->points = $points;

        return $this;
    }

    /**
     * Get points.
     *
     * @return int|null
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * Set pointsLeft.
     *
     * @param int|null $pointsLeft
     *
     * @return EvLocationPointsLog
     */
    public function setPointsLeft($pointsLeft = null)
    {
        $this->pointsLeft = $pointsLeft;

        return $this;
    }

    /**
     * Get pointsLeft.
     *
     * @return int|null
     */
    public function getPointsLeft()
    {
        return $this->pointsLeft;
    }

    /**
     * Set action.
     *
     * @param string|null $action
     *
     * @return EvLocationPointsLog
     */
    public function setAction($action = null)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action.
     *
     * @return string|null
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set comment.
     *
     * @param string|null $comment
     *
     * @return EvLocationPointsLog
     */
    public function setComment($comment = null)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment.
     *
     * @return string|null
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set loggedAt.
     *
     * @param string|null $loggedAt
     *
     * @return EvLocationPointsLog
     */
    public function setLoggedAt($loggedAt = null)
    {
        $this->loggedAt = $loggedAt;

        return $this;
    }

    /**
     * Get loggedAt.
     *
     * @return string|null
     */
    public function getLoggedAt()
    {
        return $this->loggedAt;
    }
    /**
     * @var int
     */
    private $id;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps(): void
    {
        $this->setLoggedAt(new \DateTime('now'));    
    }
    /**
     * @var string
     */
    private $sessionId;


    /**
     * Set sessionId.
     *
     * @param string $sessionId
     *
     * @return EvLocationPointsLog
     */
    public function setSessionId($sessionId)
    {
        $this->sessionId = $sessionId;

        return $this;
    }

    /**
     * Get sessionId.
     *
     * @return string
     */
    public function getSessionId()
    {
        return $this->sessionId;
    }
}
