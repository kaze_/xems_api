<?php

namespace EvBundle\Entity;

/**
 * EvLocationUserVehicle
 */
class EvLocationUserVehicle
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $userVehicleId;

    /**
     * @var int
     */
    private $enabled = '1';

    /**
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @var \DateTime
     */
    private $createdAt;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userVehicleId.
     *
     * @param int $userVehicleId
     *
     * @return EvLocationUserVehicle
     */
    public function setUserVehicleId($userVehicleId)
    {
        $this->userVehicleId = $userVehicleId;

        return $this;
    }

    /**
     * Get userVehicleId.
     *
     * @return int
     */
    public function getUserVehicleId()
    {
        return $this->userVehicleId;
    }

    /**
     * Set enabled.
     *
     * @param int $enabled
     *
     * @return EvLocationUserVehicle
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled.
     *
     * @return int
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return EvLocationUserVehicle
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return EvLocationUserVehicle
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
    /**
     * @var string
     */
    private $locationId;


    /**
     * Set locationId.
     *
     * @param string $locationId
     *
     * @return EvLocationUserVehicle
     */
    public function setLocationId($locationId)
    {
        $this->locationId = $locationId;

        return $this;
    }

    /**
     * Get locationId.
     *
     * @return string
     */
    public function getLocationId()
    {
        return $this->locationId;
    }
}
