<?php

namespace EvBundle\Entity;

/**
 * EvManufactorMaster
 */
class EvManufactorMaster
{
    /**
     * @var integer
     */
    private $evManufactorId;

    /**
     * @var string
     */
    private $evManufactorName;


    /**
     * Get evManufactorId
     *
     * @return integer
     */
    public function getEvManufactorId()
    {
        return $this->evManufactorId;
    }

    /**
     * Set evManufactorName
     *
     * @param string $evManufactorName
     *
     * @return EvManufactorMaster
     */
    public function setEvManufactorName($evManufactorName)
    {
        $this->evManufactorName = $evManufactorName;

        return $this;
    }

    /**
     * Get evManufactorName
     *
     * @return string
     */
    public function getEvManufactorName()
    {
        return $this->evManufactorName;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $battery;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $model;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->battery = new \Doctrine\Common\Collections\ArrayCollection();
        $this->model = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add battery
     *
     * @param \EvBundle\Entity\EvBatterySize $battery
     *
     * @return EvManufactorMaster
     */
    public function addBattery(\EvBundle\Entity\EvBatterySize $battery)
    {
        $this->battery[] = $battery;

        return $this;
    }

    /**
     * Remove battery
     *
     * @param \EvBundle\Entity\EvBatterySize $battery
     */
    public function removeBattery(\EvBundle\Entity\EvBatterySize $battery)
    {
        $this->battery->removeElement($battery);
    }

    /**
     * Get battery
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBattery()
    {
        return $this->battery;
    }

    /**
     * Add model
     *
     * @param \EvBundle\Entity\EvModelMaster $model
     *
     * @return EvManufactorMaster
     */
    public function addModel(\EvBundle\Entity\EvModelMaster $model)
    {
        $this->model[] = $model;

        return $this;
    }

    /**
     * Remove model
     *
     * @param \EvBundle\Entity\EvModelMaster $model
     */
    public function removeModel(\EvBundle\Entity\EvModelMaster $model)
    {
        $this->model->removeElement($model);
    }

    /**
     * Get model
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getModel()
    {
        return $this->model;
    }
}
