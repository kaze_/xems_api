<?php

namespace EvBundle\Entity;

/**
 * EvMileage
 */
class EvMileage
{
    /**
     * @var integer
     */
    private $evMileageId;

    /**
     * @var integer
     */
    private $evMileage;

    /**
     * @var \EvBundle\Entity\EvBatterySize
     */
    private $evBattery;

    /**
     * @var \EvBundle\Entity\EvModelMaster
     */
    private $evModel;


    /**
     * Get evMileageId
     *
     * @return integer
     */
    public function getEvMileageId()
    {
        return $this->evMileageId;
    }

    /**
     * Set evMileage
     *
     * @param integer $evMileage
     *
     * @return EvMileage
     */
    public function setEvMileage($evMileage)
    {
        $this->evMileage = $evMileage;

        return $this;
    }

    /**
     * Get evMileage
     *
     * @return integer
     */
    public function getEvMileage()
    {
        return $this->evMileage;
    }

    /**
     * Set evBattery
     *
     * @param \EvBundle\Entity\EvBatterySize $evBattery
     *
     * @return EvMileage
     */
    public function setEvBattery(\EvBundle\Entity\EvBatterySize $evBattery = null)
    {
        $this->evBattery = $evBattery;

        return $this;
    }

    /**
     * Get evBattery
     *
     * @return \EvBundle\Entity\EvBatterySize
     */
    public function getEvBattery()
    {
        return $this->evBattery;
    }

    /**
     * Set evModel
     *
     * @param \EvBundle\Entity\EvModelMaster $evModel
     *
     * @return EvMileage
     */
    public function setEvModel(\EvBundle\Entity\EvModelMaster $evModel = null)
    {
        $this->evModel = $evModel;

        return $this;
    }

    /**
     * Get evModel
     *
     * @return \EvBundle\Entity\EvModelMaster
     */
    public function getEvModel()
    {
        return $this->evModel;
    }
}

