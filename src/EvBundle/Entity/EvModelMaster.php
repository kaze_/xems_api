<?php

namespace EvBundle\Entity;

/**
 * EvModelMaster
 */
class EvModelMaster
{
    /**
     * @var integer
     */
    private $evModelId;

    /**
     * @var string
     */
    private $evModelName;

    /**
     * @var \EvBundle\Entity\EvManufactorMaster
     */
    private $evManufactor;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $chargingStationModel;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->chargingStationModel = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get evModelId
     *
     * @return integer
     */
    public function getEvModelId()
    {
        return $this->evModelId;
    }

    /**
     * Set evModelName
     *
     * @param string $evModelName
     *
     * @return EvModelMaster
     */
    public function setEvModelName($evModelName)
    {
        $this->evModelName = $evModelName;

        return $this;
    }

    /**
     * Get evModelName
     *
     * @return string
     */
    public function getEvModelName()
    {
        return $this->evModelName;
    }

    /**
     * Set evManufactor
     *
     * @param \EvBundle\Entity\EvManufactorMaster $evManufactor
     *
     * @return EvModelMaster
     */
    public function setEvManufactor(\EvBundle\Entity\EvManufactorMaster $evManufactor = null)
    {
        $this->evManufactor = $evManufactor;

        return $this;
    }

    /**
     * Get evManufactor
     *
     * @return \EvBundle\Entity\EvManufactorMaster
     */
    public function getEvManufactor()
    {
        return $this->evManufactor;
    }

    /**
     * Add chargingStationModel
     *
     * @param \AppBundle\Entity\EvChargingStationModelMaster $chargingStationModel
     *
     * @return EvModelMaster
     */
    public function addChargingStationModel(\AppBundle\Entity\EvChargingStationModelMaster $chargingStationModel)
    {
        $this->chargingStationModel[] = $chargingStationModel;

        return $this;
    }

    /**
     * Remove chargingStationModel
     *
     * @param \AppBundle\Entity\EvChargingStationModelMaster $chargingStationModel
     */
    public function removeChargingStationModel(\AppBundle\Entity\EvChargingStationModelMaster $chargingStationModel)
    {
        $this->chargingStationModel->removeElement($chargingStationModel);
    }

    /**
     * Get chargingStationModel
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChargingStationModel()
    {
        return $this->chargingStationModel;
    }
}

