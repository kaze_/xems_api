<?php

namespace EvBundle\Entity;

/**
 * EvUserChargingLogMonthly
 */
class EvUserChargingLogMonthly
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string|null
     */
    private $userId;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var float|null
     */
    private $startKwh;

    /**
     * @var float|null
     */
    private $endKwh;

    /**
     * @var float|null
     */
    private $totalKwh = '0';

    /**
     * @var float|null
     */
    private $basicCost = '0';

    /**
     * @var float|null
     */
    private $amountCost = '0';

    /**
     * @var float|null
     */
    private $carbonReduction = '0';

    /**
     * @var string|null
     */
    private $orderId;

    /**
     * @var int|null
     */
    private $sessionKwh;

    /**
     * @var int|null
     */
    private $sessionAmount;

    /**
     * @var \DateTime|null
     */
    private $updatedAt = 'CURRENT_TIMESTAMP';

    /**
     * @var int|null
     */
    private $orderReady = '0';

    /**
     * @var int|null
     */
    private $column;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId.
     *
     * @param string|null $userId
     *
     * @return EvUserChargingLogMonthly
     */
    public function setUserId($userId = null)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId.
     *
     * @return string|null
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set date.
     *
     * @param \DateTime $date
     *
     * @return EvUserChargingLogMonthly
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date.
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set startKwh.
     *
     * @param float|null $startKwh
     *
     * @return EvUserChargingLogMonthly
     */
    public function setStartKwh($startKwh = null)
    {
        $this->startKwh = $startKwh;

        return $this;
    }

    /**
     * Get startKwh.
     *
     * @return float|null
     */
    public function getStartKwh()
    {
        return $this->startKwh;
    }

    /**
     * Set endKwh.
     *
     * @param float|null $endKwh
     *
     * @return EvUserChargingLogMonthly
     */
    public function setEndKwh($endKwh = null)
    {
        $this->endKwh = $endKwh;

        return $this;
    }

    /**
     * Get endKwh.
     *
     * @return float|null
     */
    public function getEndKwh()
    {
        return $this->endKwh;
    }

    /**
     * Set totalKwh.
     *
     * @param float|null $totalKwh
     *
     * @return EvUserChargingLogMonthly
     */
    public function setTotalKwh($totalKwh = null)
    {
        $this->totalKwh = $totalKwh;

        return $this;
    }

    /**
     * Get totalKwh.
     *
     * @return float|null
     */
    public function getTotalKwh()
    {
        return $this->totalKwh;
    }

    /**
     * Set basicCost.
     *
     * @param float|null $basicCost
     *
     * @return EvUserChargingLogMonthly
     */
    public function setBasicCost($basicCost = null)
    {
        $this->basicCost = $basicCost;

        return $this;
    }

    /**
     * Get basicCost.
     *
     * @return float|null
     */
    public function getBasicCost()
    {
        return $this->basicCost;
    }

    /**
     * Set amountCost.
     *
     * @param float|null $amountCost
     *
     * @return EvUserChargingLogMonthly
     */
    public function setAmountCost($amountCost = null)
    {
        $this->amountCost = $amountCost;

        return $this;
    }

    /**
     * Get amountCost.
     *
     * @return float|null
     */
    public function getAmountCost()
    {
        return $this->amountCost;
    }

    /**
     * Set carbonReduction.
     *
     * @param float|null $carbonReduction
     *
     * @return EvUserChargingLogMonthly
     */
    public function setCarbonReduction($carbonReduction = null)
    {
        $this->carbonReduction = $carbonReduction;

        return $this;
    }

    /**
     * Get carbonReduction.
     *
     * @return float|null
     */
    public function getCarbonReduction()
    {
        return $this->carbonReduction;
    }

    /**
     * Set orderId.
     *
     * @param string|null $orderId
     *
     * @return EvUserChargingLogMonthly
     */
    public function setOrderId($orderId = null)
    {
        $this->orderId = $orderId;

        return $this;
    }

    /**
     * Get orderId.
     *
     * @return string|null
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * Set sessionKwh.
     *
     * @param int|null $sessionKwh
     *
     * @return EvUserChargingLogMonthly
     */
    public function setSessionKwh($sessionKwh = null)
    {
        $this->sessionKwh = $sessionKwh;

        return $this;
    }

    /**
     * Get sessionKwh.
     *
     * @return int|null
     */
    public function getSessionKwh()
    {
        return $this->sessionKwh;
    }

    /**
     * Set sessionAmount.
     *
     * @param int|null $sessionAmount
     *
     * @return EvUserChargingLogMonthly
     */
    public function setSessionAmount($sessionAmount = null)
    {
        $this->sessionAmount = $sessionAmount;

        return $this;
    }

    /**
     * Get sessionAmount.
     *
     * @return int|null
     */
    public function getSessionAmount()
    {
        return $this->sessionAmount;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime|null $updatedAt
     *
     * @return EvUserChargingLogMonthly
     */
    public function setUpdatedAt($updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set orderReady.
     *
     * @param int|null $orderReady
     *
     * @return EvUserChargingLogMonthly
     */
    public function setOrderReady($orderReady = null)
    {
        $this->orderReady = $orderReady;

        return $this;
    }

    /**
     * Get orderReady.
     *
     * @return int|null
     */
    public function getOrderReady()
    {
        return $this->orderReady;
    }

    /**
     * Set column.
     *
     * @param int|null $column
     *
     * @return EvUserChargingLogMonthly
     */
    public function setColumn($column = null)
    {
        $this->column = $column;

        return $this;
    }

    /**
     * Get column.
     *
     * @return int|null
     */
    public function getColumn()
    {
        return $this->column;
    }
}
