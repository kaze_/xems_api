<?php

namespace EvBundle\Entity;

/**
 * EvUserDetail
 */
class EvUserDetail
{

    /**
     * @var string
     */
    private $userId;

    /**
     * @var string
     */
    private $cellPhone;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $avatar;

    /**
     * @var bool
     */
    private $isNotiChargingStart;
    
    /**
     * @var bool
     */
    private $isNotiChargingAlmost;

    /**
     * @var bool
     */
    private $isNotiChargingFinish;

    /**
     * @var bool
     */
    private $isNotiStationAutoOn;

    /**
     * @var bool
     */
    private $isNotiStationAutoOff;

    /**
     * @var bool
     */
    private $SwitchStationAutoOnOff;

    /**
     * @var bool
     */
    private $SwitchOffPeakCharging;
    
    
    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;

    const IS_NOTI_CHARGING_ST = 'charg_st';
    const IS_NOTI_CHARGING_AM = 'charg_am';
    const IS_NOTI_CHARGING_FN = 'charg_fn';
    const IS_ST_AUTO_OFF = 'st_auto_off';
    const IS_ST_AUTO_ON = 'st_auto_on';
    const SW_ST_AUTO_ONOFF = 'st_auto_onoff';
    const SW_CH_OFF_PEAK = 'off_peak';

    
    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
        $this->isNotiChargingStart = 0;
        $this->isNotiChargingAlmost = 0;
        $this->isNotiChargingFinish = 0;
        $this->isNotiStationAutoOff = 0;
        $this->isNotiStationAutoOn = 0;
        $this->SwitchStationAutoOnOff = 0;
        $this->SwitchOffPeakCharging = 0;
    }


    /**
     * Set userId
     *
     * @param string $userId
     *
     * @return EvUserDetail
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return string
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set cellPhone
     *
     * @param string $cellPhone
     *
     * @return EvUserDetail
     */
    public function setCellPhone($cellPhone)
    {
        $this->cellPhone = $cellPhone;

        return $this;
    }

    /**
     * Get cellPhone
     *
     * @return string
     */
    public function getCellPhone()
    {
        return $this->cellPhone;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return EvUserDetail
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set avatar
     *
     * @param string $avatar
     *
     * @return EvUserDetail
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * Get avatar
     *
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return EvUserDetail
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return EvUserDetail
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
    
    
}

