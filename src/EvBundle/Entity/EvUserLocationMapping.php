<?php

namespace EvBundle\Entity;

/**
 * EvUserLocationMapping
 */
class EvUserLocationMapping
{
    /**
     * @var string
     */
    private $userId;

    /**
     * @var string
     */
    private $locationId;

    /**
     * @var int|null
     */
    private $statusId;

    /**
     * @var \DateTime|null
     */
    private $updatedAt;

    /**
     * @var \DateTime|null
     */
    private $createdAt;


    /**
     * Set userId.
     *
     * @param string $userId
     *
     * @return EvUserLocationMapping
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId.
     *
     * @return string
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set locationId.
     *
     * @param string $locationId
     *
     * @return EvUserLocationMapping
     */
    public function setLocationId($locationId)
    {
        $this->locationId = $locationId;

        return $this;
    }

    /**
     * Get locationId.
     *
     * @return string
     */
    public function getLocationId()
    {
        return $this->locationId;
    }

    /**
     * Set statusId.
     *
     * @param int|null $statusId
     *
     * @return EvUserLocationMapping
     */
    public function setStatusId($statusId = null)
    {
        $this->statusId = $statusId;

        return $this;
    }

    /**
     * Get statusId.
     *
     * @return int|null
     */
    public function getStatusId()
    {
        return $this->statusId;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime|null $updatedAt
     *
     * @return EvUserLocationMapping
     */
    public function setUpdatedAt($updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime|null $createdAt
     *
     * @return EvUserLocationMapping
     */
    public function setCreatedAt($createdAt = null)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime|null
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
    /**
     * @var string|null
     */
    private $unitNumber;


    /**
     * Set unitNumber.
     *
     * @param string|null $unitNumber
     *
     * @return EvUserLocationMapping
     */
    public function setUnitNumber($unitNumber = null)
    {
        $this->unitNumber = $unitNumber;

        return $this;
    }

    /**
     * Get unitNumber.
     *
     * @return string|null
     */
    public function getUnitNumber()
    {
        return $this->unitNumber;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps(): void
    {
        $this->setUpdatedAt(new \DateTime('now'));    
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }
    /**
     * @var string|null
     */
    private $contactPhone;


    /**
     * Set contactPhone.
     *
     * @param string|null $contactPhone
     *
     * @return EvUserLocationMapping
     */
    public function setContactPhone($contactPhone = null)
    {
        $this->contactPhone = $contactPhone;

        return $this;
    }

    /**
     * Get contactPhone.
     *
     * @return string|null
     */
    public function getContactPhone()
    {
        return $this->contactPhone;
    }
}
