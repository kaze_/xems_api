<?php

namespace EvBundle\Entity;

/**
 * EvUserOwnVehicle
 */
class EvUserOwnVehicle
{
    /**
     * @var string
     */
    private $userId;
    
    /**
     * @var string
     */
    private $evPlate;

    /**
     * @var \EvBundle\Entity\EvManufactorMaster
     */
    private $evManufactorMaster;
    
    /**
     * @var \EvBundle\Entity\EvBatterySize
     */
    private $evBattery;

    /**
     * @var \EvBundle\Entity\EvModelMaster
     */
    private $evModel;
    
    /**
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @var \AppBundle\Entity\User
     */
    private $user;

    public function __construct()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * Set evPlate
     *
     * @param string $evPlate
     *
     * @return EvUserOwnVehicle
     */
    public function setEvPlate($evPlate)
    {
        $this->evPlate = $evPlate;
        $this->evPlateCanonical = $this::canonicalizeEvPlate($evPlate);

        return $this;
    }

    /**
     * Get evPlate
     *
     * @return string
     */
    public function getEvPlate()
    {
        return $this->evPlate;
    }

    /**
     * Set userId
     *
     * @param string $userId
     *
     * @return EvUserOwnVehicle
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return string
     */
    public function getUserId()
    {
        return $this->userId;
    }
    
    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return EvUserOwnVehicle
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return EvUserOwnVehicle
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set evManufactorMaster
     *
     * @param \EvBundle\Entity\EvManufactorMaster $evManufactor
     *
     * @return EvUserOwnVehicle
     */
    public function setEvManufactorMaster(\EvBundle\Entity\EvManufactorMaster $evManufactor = null)
    {
        $this->evManufactorMaster = $evManufactor;

        return $this;
    }

    /**
     * Get evManufactorMaster
     *
     * @return \EvBundle\Entity\EvManufactorMaster
     */
    public function getEvManufactorMaster()
    {
        return $this->evManufactorMaster;
    }

    /**
     * Set evBattery
     *
     * @param \EvBundle\Entity\EvBatterySize $evBattery
     *
     * @return EvUserOwnVehicle
     */
    public function setEvBattery(\EvBundle\Entity\EvBatterySize $evBattery = null)
    {
        $this->evBattery = $evBattery;

        return $this;
    }

    /**
     * Get evBattery
     *
     * @return \EvBundle\Entity\EvBatterySize
     */
    public function getEvBattery()
    {
        return $this->evBattery;
    }

    /**
     * Set evModel
     *
     * @param \EvBundle\Entity\EvModelMaster $evModel
     *
     * @return EvUserOwnVehicle
     */
    public function setEvModel(\EvBundle\Entity\EvModelMaster $evModel = null)
    {
        $this->evModel = $evModel;

        return $this;
    }

    /**
     * Get evModel
     *
     * @return \EvBundle\Entity\EvModelMaster
     */
    public function getEvModel()
    {
        return $this->evModel;
    }
    /**
     * @var int|null
     */
    private $evManufactorId;

    /**
     * @var int|null
     */
    private $evBatteryId;

    /**
     * @var int
     */
    private $evModelId;


    /**
     * Set evManufactorId.
     *
     * @param int|null $evManufactorId
     *
     * @return EvUserOwnVehicle
     */
    public function setEvManufactorId($evManufactorId = null)
    {
        $this->evManufactorId = $evManufactorId;

        return $this;
    }

    /**
     * Get evManufactorId.
     *
     * @return int|null
     */
    public function getEvManufactorId()
    {
        return $this->evManufactorId;
    }

    /**
     * Set evBatteryId.
     *
     * @param int|null $evBatteryId
     *
     * @return EvUserOwnVehicle
     */
    public function setEvBatteryId($evBatteryId = null)
    {
        $this->evBatteryId = $evBatteryId;

        return $this;
    }

    /**
     * Get evBatteryId.
     *
     * @return int|null
     */
    public function getEvBatteryId()
    {
        return $this->evBatteryId;
    }

    /**
     * Set evModelId.
     *
     * @param int $evModelId
     *
     * @return EvUserOwnVehicle
     */
    public function setEvModelId($evModelId)
    {
        $this->evModelId = $evModelId;

        return $this;
    }

    /**
     * Get evModelId.
     *
     * @return int
     */
    public function getEvModelId()
    {
        return $this->evModelId;
    }
    /**
     * @var string
     */
    private $evPlateCanonical;


    /**
     * Set evPlateCanonical.
     *
     * @param string $evPlateCanonical
     *
     * @return EvUserOwnVehicle
     */
    public function setEvPlateCanonical($evPlateCanonical)
    {
        $this->evPlateCanonical = $evPlateCanonical;

        return $this;
    }

    /**
     * Get evPlateCanonical.
     *
     * @return string
     */
    public function getEvPlateCanonical()
    {
        return $this->evPlateCanonical;
    }

    static function canonicalizeEvPlate($evPlate)
    {
        if ( $evPlate == null ) return null;
        return strtoupper(str_replace('-', '', $evPlate));
    }
    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var string|null
     */
    private $vinNumber;


    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return EvUserOwnVehicle
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set vinNumber.
     *
     * @param string|null $vinNumber
     *
     * @return EvUserOwnVehicle
     */
    public function setVinNumber($vinNumber = null)
    {
        $this->vinNumber = $vinNumber;

        return $this;
    }

    /**
     * Get vinNumber.
     *
     * @return string|null
     */
    public function getVinNumber()
    {
        return $this->vinNumber;
    }
    /**
     * @var int|null
     */
    private $enabled = '1';

    /**
     * @var int
     */
    private $id;


    /**
     * Set enabled.
     *
     * @param int|null $enabled
     *
     * @return EvUserOwnVehicle
     */
    public function setEnabled($enabled = null)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled.
     *
     * @return int|null
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set id.
     *
     * @param int $id
     *
     * @return EvUserOwnVehicle
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}
