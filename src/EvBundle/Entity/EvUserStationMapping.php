<?php

namespace EvBundle\Entity;

/**
 * EvUserStationMapping
 */
class EvUserStationMapping
{
    /**
     * @var string
     */
    private $chargingStationId;

    /**
     * @var int
     */
    private $owner;

    /**
     * @var int
     */
    private $statusId;

    /**
     * @var bool
     */
    private $isNotiChargingStart;

    /**
     * @var bool
     */
    private $isNotiChargingAlmost;

    /**
     * @var bool
     */
    private $isNotiChargingFinish;

    /**
     * @var bool
     */
    private $isNotiStationAutoOn;

    /**
     * @var bool
     */
    private $isNotiStationAutoOff;

    /**
     * @var bool
     */
    private $isNotiChargingRemoval;

    /**
     * @var bool
     */
    private $SwitchStationAutoOnOff;

    /**
     * @var bool
     */
    private $SwitchOffPeakCharging;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @var \AppBundle\Entity\User
     */
    private $user;

    /**
     * @var \AppBundle\Entity\EvChargingStationMaster
     */
    private $chargingStation;
    
    const IS_NOTI_CHARGING_ST = 'charg_st';
    const IS_NOTI_CHARGING_AM = 'charg_am';
    const IS_NOTI_CHARGING_FN = 'charg_fn';
    const IS_ST_AUTO_OFF = 'st_auto_off';
    const IS_ST_AUTO_ON = 'st_auto_on';
    const IS_NOTI_CHARGING_RM = 'charg_rm';
    const SW_ST_AUTO_ONOFF = 'st_auto_onoff';
    const SW_CH_OFF_PEAK = 'off_peak';


    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
        $this->isNotiChargingStart = 0;
        $this->isNotiChargingAlmost = 0;
        $this->isNotiChargingFinish = 0;
        $this->isNotiStationAutoOff = 0;
        $this->isNotiStationAutoOn = 0;
        $this->isNotiChargingRemoval = 0;
        $this->SwitchStationAutoOnOff = 0;
        $this->SwitchOffPeakCharging = 0;
    }

    /**
     * Set chargingStationId
     *
     * @param string $chargingStationId
     *
     * @return EvUserStationMapping
     */
    public function setChargingStationId($chargingStationId)
    {
        $this->chargingStationId = $chargingStationId;

        return $this;
    }

    /**
     * Get chargingStationId
     *
     * @return string
     */
    public function getChargingStationId()
    {
        return $this->chargingStationId;
    }

    /**
     * Set owner
     *
     * @param integer $owner
     *
     * @return EvUserStationMapping
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return int
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set statusId
     *
     * @param integer $statusId
     *
     * @return EvUserStationMapping
     */
    public function setStatusId($statusId)
    {
        $this->statusId = $statusId;

        return $this;
    }

    /**
     * Get statusId
     *
     * @return int
     */
    public function getStatusId()
    {
        return $this->statusId;
    }
    
    /**
     * Set isNotiChargingStart
     *
     * @param boolean $isVerify
     *
     * @return EvUserStationMapping
     */
    public function setIsNotiChargingStart($isNotiChargingStart)
    {
        $this->isNotiChargingStart = $isNotiChargingStart;

        return $this;
    }

    /**
     * Get isNotiChargingStart
     *
     * @return bool
     */
    public function getIsNotiChargingStart()
    {
        return boolval($this->isNotiChargingStart);
    }

    /**
     * Set isNotiChargingAlmost
     *
     * @param boolean $isNotiChargingAlmost
     *
     * @return EvUserStationMapping
     */
    public function setIsNotiChargingAlmost($isNotiChargingAlmost)
    {
        $this->isNotiChargingAlmost = $isNotiChargingAlmost;

        return $this;
    }

    /**
     * Get isNotiChargingAlmost
     *
     * @return bool
     */
    public function getIsNotiChargingAlmost()
    {
        return boolval($this->isNotiChargingAlmost);
    }

    /**
     * Set isNotiChargingFinish
     *
     * @param boolean $isNotiChargingFinish
     *
     * @return EvUserStationMapping
     */
    public function setIsNotiChargingFinish($isNotiChargingFinish)
    {
        $this->isNotiChargingFinish = $isNotiChargingFinish;

        return $this;
    }

    /**
     * Get isNotiChargingFinish
     *
     * @return bool
     */
    public function getIsNotiChargingFinish()
    {
        return boolval($this->isNotiChargingFinish);
    }

    /**
     * Set isNotiStationAutoOff
     *
     * @param boolean $isNotiStationAutoOff
     *
     * @return EvUserStationMapping
     */
    public function setIsNotiStationAutoOff($isNotiStationAutoOff)
    {
        $this->isNotiStationAutoOff = $isNotiStationAutoOff;

        return $this;
    }

    /**
     * Get isNotiStationAutoOff
     *
     * @return bool
     */
    public function getIsNotiStationAutoOff()
    {
        return boolval($this->isNotiStationAutoOff);
    }

    /**
     * Set isNotiChargingRemoval
     *
     * @param boolean $isNotiChargingRemoval
     *
     * @return EvUserStationMapping
     */
    public function setIsNotiChargingRemoval($isNotiChargingRemoval)
    {
        $this->isNotiChargingRemoval = $isNotiChargingRemoval;

        return $this;
    }

    /**
     * Get isNotiChargingRemoval
     *
     * @return bool
     */
    public function getIsNotiChargingRemoval()
    {
        return boolval($this->isNotiChargingRemoval);
    }

    /**
     * Set isNotiStationAutoOn
     *
     * @param boolean $isNotiStationAutoOn
     *
     * @return EvUserStationMapping
     */
    public function setIsNotiStationAutoOn($isNotiStationAutoOn)
    {
        $this->isNotiStationAutoOn = $isNotiStationAutoOn;

        return $this;
    }

    /**
     * Get isNotiStationAutoOn
     *
     * @return bool
     */
    public function getIsNotiStationAutoOn()
    {
        return boolval($this->isNotiStationAutoOn);
    }

    /**
     * Set SwitchStationAutoOnOff
     *
     * @param boolean $SwitchStationAutoOnOff
     *
     * @return EvUserStationMapping
     */
    public function setSwitchStationAutoOnOff($SwitchStationAutoOnOff)
    {
        $this->SwitchStationAutoOnOff = $SwitchStationAutoOnOff;

        return $this;
    }

    /**
     * Get SwitchStationAutoOnOff
     *
     * @return bool
     */
    public function getSwitchStationAutoOnOff()
    {
        return boolval($this->SwitchStationAutoOnOff);
    }

    /**
     * Set SwitchOffPeakCharging
     *
     * @param boolean $SwitchOffPeakCharging
     *
     * @return EvUserStationMapping
     */
    public function setSwitchOffPeakCharging($SwitchOffPeakCharging)
    {
        $this->SwitchOffPeakCharging = $SwitchOffPeakCharging;

        return $this;
    }

    /**
     * Get SwitchOffPeakCharging
     *
     * @return bool
     */
    public function getSwitchOffPeakCharging()
    {
        return boolval($this->SwitchOffPeakCharging);
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return EvUserStationMapping
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return EvUserStationMapping
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
    
    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return EvUserStationMapping
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
    
    /**
     * Set user
     *
     * @param \AppBundle\Entity\EvChargingStationMaster $chargingStation
     *
     * @return EvUserStationMapping
     */
    public function setChargingStation(\AppBundle\Entity\EvChargingStationMaster $chargingStation = null)
    {
        $this->chargingStation = $chargingStation;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\EvChargingStationMaster
     */
    public function getChargingStation()
    {
        return $this->chargingStation;
    }
    /**
     * @var int
     */
    private $usageFee;


    /**
     * Set usageFee.
     *
     * @param int $usageFee
     *
     * @return EvUserStationMapping
     */
    public function setUsageFee($usageFee)
    {
        $this->usageFee = $usageFee;

        return $this;
    }

    /**
     * Get usageFee.
     *
     * @return int
     */
    public function getUsageFee()
    {
        return $this->usageFee;
    }
}
