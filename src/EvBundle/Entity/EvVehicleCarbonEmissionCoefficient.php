<?php

namespace EvBundle\Entity;

/**
 * EvVehicleCarbonEmissionCoefficient
 */
class EvVehicleCarbonEmissionCoefficient
{
    /**
     * @var integer
     */
    private $countryCode;

    /**
     * @var string
     */
    private $city;

    /**
     * @var integer
     */
    private $vehicleAge;

    /**
     * @var float
     */
    private $kgco2;


    /**
     * Set countryCode
     *
     * @param integer $countryCode
     *
     * @return EvVehicleCarbonEmissionCoefficient
     */
    public function setCountryCode($countryCode)
    {
        $this->countryCode = $countryCode;

        return $this;
    }

    /**
     * Get countryCode
     *
     * @return integer
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return EvVehicleCarbonEmissionCoefficient
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set vehicleAge
     *
     * @param integer $vehicleAge
     *
     * @return EvVehicleCarbonEmissionCoefficient
     */
    public function setVehicleAge($vehicleAge)
    {
        $this->vehicleAge = $vehicleAge;

        return $this;
    }

    /**
     * Get vehicleAge
     *
     * @return integer
     */
    public function getVehicleAge()
    {
        return $this->vehicleAge;
    }

    /**
     * Set kgco2
     *
     * @param float $kgco2
     *
     * @return EvVehicleCarbonEmissionCoefficient
     */
    public function setKgco2($kgco2)
    {
        $this->kgco2 = $kgco2;

        return $this;
    }

    /**
     * Get kgco2
     *
     * @return float
     */
    public function getKgco2()
    {
        return $this->kgco2;
    }
}

