<?php

namespace EvBundle\Entity;

/**
 * EvVehicleGpsLog
 */
class EvVehicleGpsLog
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var float
     */
    private $lat;

    /**
     * @var string
     */
    private $lng;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \EvBundle\Entity\EvUserOwnVehicle
     */
    private $evUserOwnVehicle;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lat.
     *
     * @param float $lat
     *
     * @return EvVehicleGpsLog
     */
    public function setLat($lat)
    {
        $this->lat = $lat;

        return $this;
    }

    /**
     * Get lat.
     *
     * @return float
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Set lng.
     *
     * @param string $lng
     *
     * @return EvVehicleGpsLog
     */
    public function setLng($lng)
    {
        $this->lng = $lng;

        return $this;
    }

    /**
     * Get lng.
     *
     * @return string
     */
    public function getLng()
    {
        return $this->lng;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return EvVehicleGpsLog
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set evUserOwnVehicle
     *
     * @param \EvBundle\Entity\EvUserOwnVehicle $evUserOwnVehicle
     *
     * @return EvVehicleGpsLog
     */
    public function setEvUserOwnVehicle(\EvBundle\Entity\EvUserOwnVehicle $evUserOwnVehicle = null)
    {
        $this->evUserOwnVehicle = $evUserOwnVehicle;

        return $this;
    }

    /**
     * Get evUserOwnVehicle
     *
     * @return \EvBundle\Entity\EvUserOwnVehicle
     */
    public function getEvBattery()
    {
        return $this->evUserOwnVehicle;
    }
}
