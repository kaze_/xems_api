<?php

namespace EvBundle\Entity;

/**
 * EvVehicleModelMaster
 */
class EvVehicleModelMaster
{
    /**
     * @var string
     */
    private $evTableId;

    /**
     * @var string|null
     */
    private $evModelName;

    /**
     * @var float|null
     */
    private $mileage;

    /**
     * @var float|null
     */
    private $batteryCapacity;

    /**
     * @var float|null
     */
    private $realRange;

    /**
     * @var float|null
     */
    private $efficiency;

    /**
     * @var int|null
     */
    private $userVisible;

    /**
     * @var string|null
     */
    private $link;

    /**
     * @var \DateTime|null
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     */
    private $updatedAt;

    /**
     * @var int
     */
    private $evModelId;


    /**
     * Set evTableId.
     *
     * @param string $evTableId
     *
     * @return EvVehicleModelMaster
     */
    public function setEvTableId($evTableId)
    {
        $this->evTableId = $evTableId;

        return $this;
    }

    /**
     * Get evTableId.
     *
     * @return string
     */
    public function getEvTableId()
    {
        return $this->evTableId;
    }

    /**
     * Set evModelName.
     *
     * @param string|null $evModelName
     *
     * @return EvVehicleModelMaster
     */
    public function setEvModelName($evModelName = null)
    {
        $this->evModelName = $evModelName;

        return $this;
    }

    /**
     * Get evModelName.
     *
     * @return string|null
     */
    public function getEvModelName()
    {
        return $this->evModelName;
    }

    /**
     * Set mileage.
     *
     * @param float|null $mileage
     *
     * @return EvVehicleModelMaster
     */
    public function setMileage($mileage = null)
    {
        $this->mileage = $mileage;

        return $this;
    }

    /**
     * Get mileage.
     *
     * @return float|null
     */
    public function getMileage()
    {
        return $this->mileage;
    }

    /**
     * Set batteryCapacity.
     *
     * @param float|null $batteryCapacity
     *
     * @return EvVehicleModelMaster
     */
    public function setBatteryCapacity($batteryCapacity = null)
    {
        $this->batteryCapacity = $batteryCapacity;

        return $this;
    }

    /**
     * Get batteryCapacity.
     *
     * @return float|null
     */
    public function getBatteryCapacity()
    {
        return $this->batteryCapacity;
    }

    /**
     * Set realRange.
     *
     * @param float|null $realRange
     *
     * @return EvVehicleModelMaster
     */
    public function setRealRange($realRange = null)
    {
        $this->realRange = $realRange;

        return $this;
    }

    /**
     * Get realRange.
     *
     * @return float|null
     */
    public function getRealRange()
    {
        return $this->realRange;
    }

    /**
     * Set efficiency.
     *
     * @param float|null $efficiency
     *
     * @return EvVehicleModelMaster
     */
    public function setEfficiency($efficiency = null)
    {
        $this->efficiency = $efficiency;

        return $this;
    }

    /**
     * Get efficiency.
     *
     * @return float|null
     */
    public function getEfficiency()
    {
        return $this->efficiency;
    }

    /**
     * Set userVisible.
     *
     * @param int|null $userVisible
     *
     * @return EvVehicleModelMaster
     */
    public function setUserVisible($userVisible = null)
    {
        $this->userVisible = $userVisible;

        return $this;
    }

    /**
     * Get userVisible.
     *
     * @return int|null
     */
    public function getUserVisible()
    {
        return $this->userVisible;
    }

    /**
     * Set link.
     *
     * @param string|null $link
     *
     * @return EvVehicleModelMaster
     */
    public function setLink($link = null)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link.
     *
     * @return string|null
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime|null $createdAt
     *
     * @return EvVehicleModelMaster
     */
    public function setCreatedAt($createdAt = null)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime|null
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime|null $updatedAt
     *
     * @return EvVehicleModelMaster
     */
    public function setUpdatedAt($updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Get evModelId.
     *
     * @return int
     */
    public function getEvModelId()
    {
        return $this->evModelId;
    }
    /**
     * @var int
     */
    private $id;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set evModelId.
     *
     * @param string $evModelId
     *
     * @return EvVehicleModelMaster
     */
    public function setEvModelId($evModelId)
    {
        $this->evModelId = $evModelId;

        return $this;
    }
}
