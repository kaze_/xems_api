<?php

namespace EvBundle\Entity;

/**
 * PayAuthHistory
 */
class PayAuthHistory
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $orderId = '';

    /**
     * @var string|null
     */
    private $result;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     */
    private $updatedAt;

    /**
     * @var string|null
     */
    private $auth;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set orderId.
     *
     * @param string $orderId
     *
     * @return PayAuthHistory
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;

        return $this;
    }

    /**
     * Get orderId.
     *
     * @return string
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * Set result.
     *
     * @param string|null $result
     *
     * @return PayAuthHistory
     */
    public function setResult($result = null)
    {
        $this->result = $result;

        return $this;
    }

    /**
     * Get result.
     *
     * @return string|null
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return PayAuthHistory
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime|null $updatedAt
     *
     * @return PayAuthHistory
     */
    public function setUpdatedAt($updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set auth.
     *
     * @param string|null $auth
     *
     * @return PayAuthHistory
     */
    public function setAuth($auth = null)
    {
        $this->auth = $auth;

        return $this;
    }

    /**
     * Get auth.
     *
     * @return string|null
     */
    public function getAuth()
    {
        return $this->auth;
    }
    
    public function doUpdate()
    {
        $this->updatedAt = new \Datetime();
    }
    
    public function doCreate()
    {
        $this->createdAt = new \Datetime();
    }
    
    /**
     * @var int|null
     */
    private $status;


    /**
     * Set status.
     *
     * @param int|null $status
     *
     * @return PayAuthHistory
     */
    public function setStatus($status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return int|null
     */
    public function getStatus()
    {
        return $this->status;
    }
    /**
     * @var string|null
     */
    private $action;


    /**
     * Set action.
     *
     * @param string|null $action
     *
     * @return PayAuthHistory
     */
    public function setAction($action = null)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action.
     *
     * @return string|null
     */
    public function getAction()
    {
        return $this->action;
    }
    
}
