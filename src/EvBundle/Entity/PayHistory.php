<?php

namespace EvBundle\Entity;

/**
 * PayHistory
 */
class PayHistory
{
    /**
     * @var string
     */
    private $orderId;

    /**
     * @var string
     */
    private $userId;

    /**
     * @var int
     */
    private $amount;

    /**
     * @var string
     */
    private $pdesc;

    /**
     * @var int
     */
    private $inst;

    /**
     * @var bool
     */
    private $isVerify;
    
    /**
     * @var int
     */
    private $status;

    /**
     * @var \DateTime
     */
    private $tradeAt;
    
    /**
     * @var string
     */
    private $errorCode;

    /**
     * @var json
     */
    private $result;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;

    const STATUS_TRADING = 0;
    const STATUS_SUCCESS = 1;
    const STATUS_FAILED = 2;
    const STATUS_CANCELED = 3;
    
    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
        $this->inst = 0;
        $this->status = 0;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->orderId;
    }

    /**
     * Set userId
     *
     * @param string $userId
     *
     * @return PayHistory
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return string
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     *
     * @return PayHistory
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set pdesc
     *
     * @param string $pdesc
     *
     * @return PayHistory
     */
    public function setPdesc($pdesc)
    {
        $this->pdesc = $pdesc;

        return $this;
    }

    /**
     * Get pdesc
     *
     * @return string
     */
    public function getPdesc()
    {
        return $this->pdesc;
    }

    /**
     * Set inst
     *
     * @param integer $inst
     *
     * @return PayHistory
     */
    public function setInst($inst)
    {
        $this->inst = $inst;

        return $this;
    }

    /**
     * Get inst
     *
     * @return int
     */
    public function getInst()
    {
        return $this->inst;
    }


    /**
     * Set isVerify
     *
     * @param boolean $isVerify
     *
     * @return PayHistory
     */
    public function setIsVerify($isVerify)
    {
        $this->isVerify = $isVerify;

        return $this;
    }

    /**
     * Get isAe
     *
     * @return bool
     */
    public function getIsVerify()
    {
        return $this->isVerify;
    }
    
    /**
     * Set status
     *
     * @param integer $status
     *
     * @return PayHistory
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set TradeAt
     *
     * @param \DateTime $tradeAt
     *
     * @return PayHistory
     */
    public function setTradeAt($tradeAt)
    {
        $this->tradeAt = $tradeAt;

        return $this;
    }

    /**
     * Get TradeAt
     *
     * @return \DateTime
     */
    public function getTradedAt()
    {
        return $this->tradeAt;
    }
    
    /**
     * Set errorCode
     *
     * @param string $errorCode
     *
     * @return PayHistory
     */
    public function setErrorCode($errorCode)
    {
        $this->errorCode = $errorCode;

        return $this;
    }

    /**
     * Get errorCode
     *
     * @return string
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * Set result
     *
     * @param json $result
     *
     * @return PayHistory
     */
    public function setResult($result)
    {
        $this->result = $result;

        return $this;
    }

    /**
     * Get result
     *
     * @return json
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return PayHistory
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return PayHistory
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @var \EvBundle\Entity\CreditCard
     */
    private $creditCard;

    /**
     * Set CreditCard
     *
     * @param \EvBundle\Entity\CreditCard $creditCard
     *
     * @return PayHistory
     */
    public function setCreditCard(\EvBundle\Entity\CreditCard $creditCard = null)
    {
        $this->creditCard = $creditCard;

        return $this;
    }

    /**
     * Get CreditCard
     *
     * @return \EvBundle\Entity\CreditCard
     */
    public function getCreditCard()
    {
        return $this->creditCard;
    }
    /**
     * @var int|null
     */
    private $paymentTypeId;

    /**
     * @var string|null
     */
    private $paymentId = '';

    /**
     * @var string|null
     */
    private $auth;


    /**
     * Get tradeAt.
     *
     * @return \DateTime
     */
    public function getTradeAt()
    {
        return $this->tradeAt;
    }

    /**
     * Set paymentTypeId.
     *
     * @param int|null $paymentTypeId
     *
     * @return PayHistory
     */
    public function setPaymentTypeId($paymentTypeId = null)
    {
        $this->paymentTypeId = $paymentTypeId;

        return $this;
    }

    /**
     * Get paymentTypeId.
     *
     * @return int|null
     */
    public function getPaymentTypeId()
    {
        return $this->paymentTypeId;
    }

    /**
     * Set paymentId.
     *
     * @param string|null $paymentId
     *
     * @return PayHistory
     */
    public function setPaymentId($paymentId = null)
    {
        $this->paymentId = $paymentId;

        return $this;
    }

    /**
     * Get paymentId.
     *
     * @return string|null
     */
    public function getPaymentId()
    {
        return $this->paymentId;
    }

    /**
     * Set auth.
     *
     * @param string|null $auth
     *
     * @return PayHistory
     */
    public function setAuth($auth = null)
    {
        $this->auth = $auth;

        return $this;
    }

    /**
     * Get auth.
     *
     * @return string|null
     */
    public function getAuth()
    {
        return $this->auth;
    }

    /**
     * Get orderId.
     *
     * @return string
     */
    public function getOrderId()
    {
        return $this->orderId;
    }
}
