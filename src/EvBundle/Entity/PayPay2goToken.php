<?php

namespace EvBundle\Entity;

/**
 * PayPay2goToken
 */
class PayPay2goToken
{
    /**
     * @var string
     */
    private $userId = '';

    /**
     * @var string
     */
    private $token = '';

    /**
     * @var string|null
     */
    private $tokenLife;

    /**
     * @var string
     */
    private $card6No = '';

    /**
     * @var string
     */
    private $card4No = '';

    /**
     * @var string
     */
    private $bank = '';

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var bool|null
     */
    private $status = '0';

    /**
     * @var string
     */
    private $paymentId;


    public function __construct()
    {
        $this->setCreatedAt(new \DateTime());
    }
    
    /**
     * Set userId.
     *
     * @param string $userId
     *
     * @return PayPay2goToken
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId.
     *
     * @return string
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set token.
     *
     * @param string $token
     *
     * @return PayPay2goToken
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token.
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set tokenLife.
     *
     * @param string|null $tokenLife
     *
     * @return PayPay2goToken
     */
    public function setTokenLife($tokenLife = null)
    {
        $this->tokenLife = $tokenLife;

        return $this;
    }

    /**
     * Get tokenLife.
     *
     * @return string|null
     */
    public function getTokenLife()
    {
        return $this->tokenLife;
    }

    /**
     * Set card6No.
     *
     * @param string $card6No
     *
     * @return PayPay2goToken
     */
    public function setCard6No($card6No)
    {
        $this->card6No = $card6No;

        return $this;
    }

    /**
     * Get card6No.
     *
     * @return string
     */
    public function getCard6No()
    {
        return $this->card6No;
    }

    /**
     * Set card4No.
     *
     * @param string $card4No
     *
     * @return PayPay2goToken
     */
    public function setCard4No($card4No)
    {
        $this->card4No = $card4No;

        return $this;
    }

    /**
     * Get card4No.
     *
     * @return string
     */
    public function getCard4No()
    {
        return $this->card4No;
    }

    /**
     * Set bank.
     *
     * @param string $bank
     *
     * @return PayPay2goToken
     */
    public function setBank($bank)
    {
        $this->bank = $bank;

        return $this;
    }

    /**
     * Get bank.
     *
     * @return string
     */
    public function getBank()
    {
        return $this->bank;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return PayPay2goToken
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set status.
     *
     * @param integer|null $status
     *
     * @return PayPay2goToken
     */
    public function setStatus($status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return integer|null
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get paymentId.
     *
     * @return string
     */
    public function getPaymentId()
    {
        return $this->paymentId;
    }
    
    public function getMaskedCardNo()
    {
        $pad = str_pad('', 6, '*');
        if ( $this->getCard6No() != null ) {
            $maskedCardNo = $this->getCard6No().$pad.$this->getCard4No();
            return $maskedCardNo;
        }
        return '';
    }
}
