<?php

namespace EvBundle\Entity;

/**
 * PayPaymentType
 */
class PayPaymentType
{
    /**
     * @var string|null
     */
    private $paymentName;

    /**
     * @var string|null
     */
    private $tableName;

    /**
     * @var int
     */
    private $paymentTypeId;


    /**
     * Set paymentName.
     *
     * @param string|null $paymentName
     *
     * @return PayPaymentType
     */
    public function setPaymentName($paymentName = null)
    {
        $this->paymentName = $paymentName;

        return $this;
    }

    /**
     * Get paymentName.
     *
     * @return string|null
     */
    public function getPaymentName()
    {
        return $this->paymentName;
    }

    /**
     * Set tableName.
     *
     * @param string|null $tableName
     *
     * @return PayPaymentType
     */
    public function setTableName($tableName = null)
    {
        $this->tableName = $tableName;

        return $this;
    }

    /**
     * Get tableName.
     *
     * @return string|null
     */
    public function getTableName()
    {
        return $this->tableName;
    }

    /**
     * Get paymentTypeId.
     *
     * @return int
     */
    public function getPaymentTypeId()
    {
        return $this->paymentTypeId;
    }
    /**
     * @var int|null
     */
    private $status;


    /**
     * Set status.
     *
     * @param int|null $status
     *
     * @return PayPaymentType
     */
    public function setStatus($status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return int|null
     */
    public function getStatus()
    {
        return $this->status;
    }
}
