<?php

namespace EvBundle\Entity;

/**
 * PayUserPaymentMapping
 */
class PayUserPaymentMapping
{
    /**
     * @var string|null
     */
    private $userId;

    /**
     * @var string
     */
    private $paymentId;

    /**
     * @var int|null
     */
    private $statusId;

    /**
     * @var \DateTime|null
     */
    private $date;

    /**
     * @var string
     */
    private $id;

    /**
     * @var \EvBundle\Entity\PayPaymentType
     */
    private $paymentType;


    /**
     * Set userId.
     *
     * @param string|null $userId
     *
     * @return PayUserPaymentMapping
     */
    public function setUserId($userId = null)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId.
     *
     * @return string|null
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set paymentId.
     *
     * @param string $paymentId
     *
     * @return PayUserPaymentMapping
     */
    public function setPaymentId($paymentId)
    {
        $this->paymentId = $paymentId;

        return $this;
    }

    /**
     * Get paymentId.
     *
     * @return string
     */
    public function getPaymentId()
    {
        return $this->paymentId;
    }

    /**
     * Set statusId.
     *
     * @param int|null $statusId
     *
     * @return PayUserPaymentMapping
     */
    public function setStatusId($statusId = null)
    {
        $this->statusId = $statusId;

        return $this;
    }

    /**
     * Get statusId.
     *
     * @return int|null
     */
    public function getStatusId()
    {
        return $this->statusId;
    }

    /**
     * Set date.
     *
     * @param \DateTime|null $date
     *
     * @return PayUserPaymentMapping
     */
    public function setDate($date = null)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date.
     *
     * @return \DateTime|null
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set paymentType.
     *
     * @param \EvBundle\Entity\PayPaymentType|null $paymentType
     *
     * @return PayUserPaymentMapping
     */
    public function setPaymentType(\EvBundle\Entity\PayPaymentType $paymentType = null)
    {
        $this->paymentType = $paymentType;

        return $this;
    }

    /**
     * Get paymentType.
     *
     * @return \EvBundle\Entity\PayPaymentType|null
     */
    public function getPaymentType()
    {
        return $this->paymentType;
    }
}
