<?php

namespace EvBundle\Entity;

/**
 * PushNotification
 */
class PushNotification
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $userId;

    /**
     * @var string
     */
    private $content;
    
    /**
     * @var json
     */
    private $data;

    /**
     * @var int
     */
    private $status;

    /**
     * @var int
     */
    private $inBackground;
    
    /**
     * @var int
     */
    private $syncMode;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;

    const SEND_READY = 0;
    const SEND_SUCCESS = 1;
    const SEND_FAILED = 2;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
        $this->status = 0;
        $this->syncMode = 0;
        $this->inBackground = 0;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param string $userId
     *
     * @return PushNotification
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return string
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return PushNotification
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set data
     *
     * @param json $data
     *
     * @return PushNotification
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return json
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return PushNotification
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }
    
    /**
     * Set syncMode
     *
     * @param integer $syncMode
     *
     * @return PushNotification
     */
    public function setSyncMode($syncMode)
    {
        $this->syncMode = (int)$syncMode;

        return $this;
    }

    /**
     * Get syncMode
     *
     * @return int
     */
    public function getSyncMode()
    {
        return $this->syncMode;
    }
    
    /**
     * Set inBackground
     *
     * @param integer $inBackground
     *
     * @return PushNotification
     */
    public function setInBackground($inBackground)
    {
        $this->inBackground = (int)$inBackground;

        return $this;
    }

    /**
     * Get inBackground
     *
     * @return int
     */
    public function getInBackground()
    {
        return $this->inBackground;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return PushNotification
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return PushNotification
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}

