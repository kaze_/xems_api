<?php

namespace EvBundle\Entity;

/**
 * Sms
 */
class Sms
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $phone;
    
    /**
     * @var string
     */
    private $content;

    /**
     * @var int
     */
    private $pincode;

    /**
     * @var int
     */
    private $sendStatus;

    /**
     * @var int
     */
    private $valiStatus;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;

    const SEND_READY = 0;
    const SEND_SUCCESS = 1;
    const SEND_FAILED = 2;
    
    const VALI_NONE = 0;
    const VALI_OK = 1;
    const VALI_EXPIRED = 2;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
        $this->sendStatus = 0;
        $this->valiStatus = 0;
    }
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Sms
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }
    
    /**
     * Set content
     *
     * @param string $content
     *
     * @return Sms
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set pincode
     *
     * @param integer $pincode
     *
     * @return Sms
     */
    public function setPincode($pincode)
    {
        $this->pincode = $pincode;

        return $this;
    }

    /**
     * Get pincode
     *
     * @return int
     */
    public function getPincode()
    {
        return $this->pincode;
    }

    /**
     * Set sendStatus
     *
     * @param integer $sendStatus
     *
     * @return Sms
     */
    public function setSendStatus($sendStatus)
    {
        $this->sendStatus = $sendStatus;

        return $this;
    }

    /**
     * Get sendStatus
     *
     * @return int
     */
    public function getSendStatus()
    {
        return $this->sendStatus;
    }

    /**
     * Set valiStatus
     *
     * @param integer $valiStatus
     *
     * @return Sms
     */
    public function setValiStatus($valiStatus)
    {
        $this->valiStatus = $valiStatus;

        return $this;
    }

    /**
     * Get valiStatus
     *
     * @return int
     */
    public function getValiStatus()
    {
        return $this->valiStatus;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Sms
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Sms
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}

