<?php
/**
 * Created by PhpStorm.
 * User: chiehyingpan
 * Date: 2018/2/21
 * Time: 下午2:57
 */

namespace EvBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Base;

class EvChargingSessionLogRepository Extends EntityRepository
{
    static $defaultLimit = 20;

    public function findNewestSessionByStation($station)
    {
        $res = $this->findBy(array('chargingStation' => $station), array('startAt' => 'DESC'), 1);
        if ( !isset($res[0])) {
            // something is wrong, no session record
            return null;
        }
        return $res[0];
    }

    public function findByStation($condition, $ordering, $limit = null, $offset = null )
    {
        $station = isset($condition['station'])? $condition['station'] : null;
        $from = isset($condition['from'])? $condition['from']: null;
        $to = isset($condition['to'])? $condition['to']: null;
        $orderType = isset($ordering['startAt'])? $ordering['startAt']: 'DESC';
        if ( !$limit ) $limit = $this::$defaultLimit;
        if ( !$offset ) $offset = 0;

        $qb = $this->createQueryBuilder('q');
        $qb->where('q.chargingStation = :station')
            ->andWhere('q.endAt is not null')
            ->orderBy('q.startAt', $orderType)
            ->setMaxResults($limit)
            ->setFirstResult($offset)
            ->setParameter('station', $station, 'uuid');
        if ( $from ) {
            $qb->andWhere('q.startAt > :from')->setParameter('from', $from );
        }
        if ( $to ) {
            $qb->andWhere('q.startAt < :to')->setParameter('to', $to);
        }

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    public function findByUser($condition, $ordering, $limit = null, $offset = null )
    {
        $user = isset($condition['user'])? $condition['user'] : null;
        $from = isset($condition['from'])? $condition['from']: null;
        $to = isset($condition['to'])? $condition['to']: null;
        $orderType = isset($ordering['startAt'])? $ordering['startAt']: 'DESC';
        if ( !$limit ) $limit = $this::$defaultLimit;
        if ( !$offset ) $offset = 0;

        $qb = $this->createQueryBuilder('q');
        $qb->where('q.userId = :userId')
            ->andWhere('q.endAt is not null')
            ->orderBy('q.startAt', $orderType)
            ->setMaxResults($limit)
            ->setFirstResult($offset)
            ->setParameter('userId', $user, 'uuid');
        if ( $from ) {
            $qb->andWhere('q.startAt > :from')->setParameter('from', $from );
        }
        if ( $to ) {
            $qb->andWhere('q.startAt < :to')->setParameter('to', $to);
        }

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    public function findDataByStation($fields, $condition, $ordering, $limit = null, $offset = null)
    {
        $station = isset($condition['chargingStation'])? $condition['chargingStation'] : null;
        $from = isset($condition['from'])? $condition['from']: null;
        $to = isset($condition['to'])? $condition['to']: null;

        if ( !$limit ) $limit = $this::$defaultLimit;
        if ( !$offset ) $offset = 0;

        $qb = $this->createQueryBuilder('p');
        $qb->select('p.'.$fields[0]);
        for ($i = 1; $i < count($fields); $i++) {
            $qb->addSelect('p.' . $fields[$i]);
        }
        $qb->where('p.chargingStation = :station')
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->setParameter('station', $station, 'uuid');

        if ( $ordering != null ) {
            foreach($ordering as $key => $order) {
                $qb->orderBy('p.'.$key, $order);
            }
        }

        if ( $from ) {
            $qb->andWhere('p.startAt > :from')->setParameter('from', $from );
        }
        if ( $to ) {
            $qb->andWhere('p.startAt < :to')->setParameter('to', $to);
        }

        $result = $qb->getQuery()->getResult();
        return $result;
    }


}
