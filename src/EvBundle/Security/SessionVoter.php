<?php
/**
 * Created by PhpStorm.
 * User: chiehyingpan
 * Date: 2017/7/3
 * Time: 下午3:58
 */

namespace EvBundle\Security;


use EvBundle\Entity\EvChargingSessionLog;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use AppBundle\Entity\User;


class SessionVoter extends Voter
{
    private $decisionManager;
    private $entityManager;

    const VIEW = 'view';
    const EDIT = 'edit';

    public function __construct(AccessDecisionManagerInterface $decisionManager, EntityManager $entityManager)
    {
        $this->decisionManager = $decisionManager;
        $this->entityManager = $entityManager;
    }

    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, array(self::VIEW, self::EDIT))) {
            return false;
        }
        if (!$subject instanceof EvChargingSessionLog) {
            return false;
        }
        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        if (!$user instanceof User) {
            return false;
        }

        if ( $this->decisionManager->decide($token, array('ROLE_ADMIN'))) {
            return true;
        }

        switch ($attribute) {
            case self::VIEW:
                return $this->canView($subject, $user);
            case self::EDIT:
                return $this->canEdit($subject, $user, $token);
        }
        throw new \LogicException('This code should not be reached!');
    }

    private function isSessionBelongsToCustomer($circuitId, $customerId)
    {
        if ( $circuitId == null || $circuitId == '' || $customerId == null || $customerId == '' ) {
            return false;
        }
        $cmMappingRepository = $this->entityManager->getRepository('AppBundle:EmsCustomerLocationMapping');
        $qb = $cmMappingRepository->createQueryBuilder('cl');
        $qb->innerJoin('AppBundle:EmsLocationGatewayMapping', 'lg', 'WITH', 'lg.locationId = cl.locationId')
            ->innerJoin('AppBundle:EmsGatewayDeviceMapping', 'gd', 'WITH', 'gd.gatewayId = lg.gatewayId')
            ->innerJoin('AppBundle:EmsMeterCircuitMapping', 'mc', 'WITH', 'mc.device = gd.deviceId')
            ->where('mc.circuit = :circuit')
            ->andWhere('cl.customerId = :customer')
            ->setParameter('circuit', $circuitId)
            ->setParameter('customer', $customerId, 'uuid');
        $result = $qb->getQuery()->getResult();
        if ( count($result) != 0 ) {
            return true;
        }
        return false;
    }

    private function canView($session, $user)
    {
        $payUser = $session->getOwner();
        $chargingUser = $session->getUserId();
        if ( $payUser ) {
            if ( $payUser->getId() == $user->getId() ) {
                return true;
            }
        } 
        if ( $chargingUser ) {
            if ( $chargingUser->getId() == $user->getId() ) {
                return true;
            }
        }
        // not user, manager?
        $circuit = $session->getCircuit();
        if ( $circuit ) {
            return $this->isSessionBelongsToCustomer($circuit->getCircuitId(), $user->getCustomerId());
        }
        return false;
    }

    private function canEdit($subject, $user, $token)
    {

        if (!$this->canView($subject, $user)) {
            return false;
        }

        if ($this->decisionManager->decide($token, array('ROLE_CUSTOMER_ADMIN'))) {
            return true;
        }
        return false;

    }
}