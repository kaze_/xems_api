<?php
/**
 * Created by PhpStorm.
 * User: chiehyingpan
 * Date: 2018/2/26
 * Time: 下午4:30
 */

namespace EvBundle\Services;


use Doctrine\ORM\EntityManager;

class Calculators
{
    private $em;
    private $vehicleMetadataService;
    private $mileage;
    private $mileageV2;
    private $kwCo2 = 0.529;
    private $mileCo2 = 0.28352;
    private $avgMileage = 5;

    public function __construct(EntityManager $em, $vehicleMetadata)
    {
        $this->em = $em;
        $this->vehicleMetadataService = $vehicleMetadata;
        $this->reloadMilageTable();
    }

    public function reloadMilageTable()
    {
        // load v1
        $repo = $this->em->getRepository('EvBundle:EvMileage');
        $res = $repo->findAll();

        $mileageList = array();
        foreach($res as $one) {
            $modelId = $one->getEvModel()->getEvModelId();
            $batteryId = $one->getEvBattery()->getEvBatteryId();
            $batteryCapacity = $one->getEvBattery()->getEvBatterySize();
            $mileage = $one->getEvMileage();

            $mileageList[$modelId][$batteryId] = $mileage / $batteryCapacity;
        }
        $this->mileage = $mileageList;

        // load v2
        $mileageV2 = [];
        $vehicleMetadata = $this->vehicleMetadataService->getVehicleMetadata();
        $totalMilage = 0;
        foreach($vehicleMetadata['data'] as $model) {
            $mileage = $model->getMileage();
            $mileageV2[$model->getId()] = $mileage;
            // $totalMilage += $mileage;
        }
        /*
        if ( $totalMilage ) {
            $this->avgMileage = $totalMilage / count($vehicleMetadata['data']);
        }*/
        $this->mileageV2 = $mileageV2;
    }

    public function getMileageMetadata()
    {
        return $this->mileage;
    }

    public function getDistance($kwh, $modelId, $batteryId=null)
    {
        return $kwh * $this->getMileage($modelId, $batteryId);
    }

    public function getMileage($modelId, $batteryId=null)
    {
        if ( $modelId ) {
            return $this->avgMileage;
        }
        // old version
        if ( $batteryId ) {
            if ( !isset($this->mileage[$modelId][$batteryId] )) {
                return $this->avgMileage;
            }
            return $this->mileage[$modelId][$batteryId];
        }
        if ( !isset($this->mileageV2[$modelId])) {
            return $this->avgMileage;
        }
        return $this->mileageV2[$modelId];        
    }

    public function getCarbonReduction($kwh, $modelId, $batteryId=null)
    {
        $mileage = $this->getMileage($modelId, $batteryId);
        $kgCo2e = $kwh * ( $mileage * $this->mileCo2 - $this->kwCo2);
        return $kgCo2e;
    }
}
