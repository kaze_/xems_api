<?php

namespace EvBundle\Services;


use Doctrine\ORM\EntityManager;
use AppBundle\Entity\EvChargingStationMaster as Station;
use EvBundle\Entity\EvChargingSessionLog;
use EvBundle\Entity\EvLocationChargingGuest;
use EvBundle\Entity\EvUserOwnVehicle;
use EvBundle\Services\ChargingPolicy\ChargingPolicy;
use EvBundle\Services\ChargingPolicy\ChengGongChargingPolicy;
use EvBundle\Services\ChargingPolicy\FareastChargingPolicy;
use Monolog\Logger;

class ChargingControll
{
    private $_em;
    private $_locatinSetting;
    private $_logger;
    private $_payPoints;
    private $_vehicle;
    private $_energyFee;
    
    private const GUEST_UNIT = "#GUEST";

    public function __construct(EntityManager $em, $locationSetting,  logger $logger, $payPoints, $vehicle, $energyFee)
    {
        $this->_em = $em;
        $this->_locatinSetting = $locationSetting;
        $this->_logger = $logger;
        $this->_payPoints = $payPoints;
        $this->_vehicle = $vehicle;
        $this->_energyFee = $energyFee;
    }

    /**
     * @param string charging policy
     * @return EvBundle\Services\ChargingPolicy\ChargingPolicy $policyObject
     */
    public function getChargingPolicyByPolicy($policy)
    {
        switch($policy) {
            case 'fareast':
                return new FareastChargingPolicy($this->_em, $this->_locatinSetting, $this->_payPoints, $this->_energyFee);
            break;
            case 'chenggong':
                return new ChengGongChargingPolicy($this->_em, $this->_locatinSetting, $this->_payPoints, $this->_energyFee);
            break;
        }
        return null;
    }

    /**
     * 
     */
    public function validateCharging($user, $evPlate, $station, $guest=0)
    {
        $location = $this->_em->getRepository('AppBundle:EmsLocationMaster')
        ->findByChargingStation($station->getChargingStationId());

        if ( !$location ) {
            // ???
            return ChargingPolicy::VALIDATE_OK;
        }
        
        // 1. get stations charging fee type and decide charging or not        
        // 2. check user billing status
        $policy = $station->getChargingFeePolicy();
        // location/vehicle checking
        $chargingPolicy = $this->getChargingPolicyByPolicy($policy);
        if ( $chargingPolicy ) {
            // TODO:
            // move  guest checking and location vehicle checking into station policy?
            $guest = $this->checkLocationGuest($user, $location->getLocationId());
            // if not a guest, check if car plate is active in the location
            // location/vehicle checking
            if ( $guest == 0 && ($policy == 'fareast' || $policy == 'chenggong')) {
                $checkLocationVehicle = $this->_vehicle->checkVehicleByLocationUsers($location, $user, $evPlate);
                if ( $checkLocationVehicle == false ) {
                    return ChargingPolicy::VALIDATE_BAD_USER;
                }
            }
            return $chargingPolicy->validateCharging($user, $evPlate, $location, $guest);
        }
        // 其他的就放行？
        return ChargingPolicy::VALIDATE_OK;
    }

    public function getValidateData($user, $evPlate, $station)
    {
        $location = $this->_em->getRepository('AppBundle:EmsLocationMaster')
            ->findByChargingStation($station->getChargingStationId());

        $userId = null;
        $guest = 0;
        if ( $user === "00000000-0000-0000-0000-000000000000" ) {
            // plate recognition
            $locationUser = $this->_vehicle->getLocationUserByVehicle($location, $evPlate);
            if ( $locationUser ) {
                $userId = $locationUser->getUserId();
            } else {
                // can't find user, check if user a guest
                $user = $this->getLocationChargeableGuestByVehicle($location, $evPlate);
                if ( $user ) {
                    $guest = $user->getId();
                    $userId = $user->getUserId();
                } else {
                    // non-chargeable guest
                    $userId = null;
                }
            }
        } else {
            $userId = $user;
        }
        return ['userId' => $userId, 'evPlate' => $evPlate, 'guestId' => $guest];
    }

    public function calculateChargingFeeBySession($session, $guest=false)
    {
        $station = $session->getChargingStation();
        $policy = $station->getChargingFeePolicy();
        $chargingPolicy = $this->getChargingPolicyByPolicy($policy);
        if ( $chargingPolicy ) {
            return $chargingPolicy->calculateChargingFeeBySession($session, $guest);
        }
        return true;
    }

    public function calculateChargingFeeByFeeData($feeData, $station, $guest=false)
    {
        if ( is_string($station)) {
            $station = $this->_em->getRepository('EvBundle:EvChargingStationMaster')->find($station);
            if ( !$station ) return false;
        }
        $policy = $station->getChargingFeePolicy();
        $chargingPolicy = $this->getChargingPolicyByPolicy($policy);
        if ( $chargingPolicy ) {
            return $chargingPolicy->calculateChargingFeeByFeeData($feeData, $station, $guest);
        }
        return true;
    }

    public function collectChargingFee($session, $guest=false)
    {
        // check if guest
        $user = $session->getUserId();
        if ( $user == null ) {
            return false;
        }
        $station = $session->getChargingStation();
        if ( !$station ) {
            return false;
        }
        $location = $this->_em->getRepository('AppBundle:EmsLocationMaster')
            ->findByChargingStation($station->getChargingStationId());
        if ( !$location ) {
            return false;
        }
        $guest = $this->checkLocationGuest($user->getId(), $location->getLocationId());
        $policy = $station->getChargingFeePolicy();
        $chargingPolicy = $this->getChargingPolicyByPolicy($policy);
        if ( $chargingPolicy ) {
            return $chargingPolicy->collectChargingFee($session, $guest);
        }
        return true;
    }

    /**
     * @param EvBundles\Entity\EvChargingStationMaster $staion
     * @param int status 1: on, 0: off
     * 
     * @return bool 
     */
    public function setStationVipStatus(Station $station, $status)
    {
        if ( $status != 0 && $status != 1 ) {
            return false;
        }
        if ( $status == null ) {
            return false;
        }
        $station->setPriorityCharging($status);
        $this->_em->persist($station);
        $this->_em->flush();
        return true;
    }

    /* location guest users controll */
    public function checkLocationGuest($userId, $locationId)
    {
        $res = $this->_em->getRepository('EvBundle:EvUserLocationMapping') 
            ->findOneBy(['userId' => $userId, 'locationId' => $locationId, 'unitNumber' => self::GUEST_UNIT]) ;
        if ( $res == null ) {
            return false;
        }
        return true;
    }

    public function ListLocationActiveGuest($locationId)
    {
        $res = $this->_em->getRepository('EvBundle:EvLocationChargingGuest')
            ->findBy(['locationId' => $locationId, 'checked' => 0 ], ['id' => 'ASC']);

        return $res;
    }

    public function getLocationGuest($guestId, $locationId)
    {
        $res = $this->_em->getRepository('EvBundle:EvLocationChargingGuest')
            ->findOneBy(['id' => $guestId, 'locationId' => $locationId]);
        return $res;
    }

    public function getLocationChargeableGuestByVehicle($locationId, $evPlate)
    {
        $res = $this->_em->getRepository('EvBundle:EvLocationChargingGuest')
            ->findOneBy([
                'evPlate' => $evPlate, 
                'locationId' => $locationId, 
                'checked' => 0,
                'sessionId' => null ]);
        return $res;
    }

    public function addLocationGuest($locationId, $phone, $evPlate, $unit, $preCharge)
    {
        $guestLocationUser = $this->_em->getRepository('EvBundle:EvUserLocationMapping')
            ->findOneBy(['locationId' => $locationId, 'unitNumber' => self::GUEST_UNIT]);
        if ( $guestLocationUser ) {
            $userId = $guestLocationUser->getUserId();
        } else {
            return false;
        }
        $guest = new EvLocationChargingGuest();
        $evPlateCanonical = EvUserOwnVehicle::canonicalizeEvPlate($evPlate);
        $guest->setLocationId($locationId)
            ->setUserId($userId)
            ->setPhone($phone)
            ->setEvPlate($evPlateCanonical)
            ->setVisitUnit($unit)
            ->setPreCharge($preCharge)
            ->setChecked(0)
            ->setCreatedAt(new \DateTime())
            ->setUpdatedAt(new \DateTime());

        try {
            $this->_em->persist($guest);
            $this->_em->flush();
            $this->_em->refresh($guest);
        } catch( \Exception $e) {
            return false;
        }
        $this->_payPoints->modifyLocationPoints(
            $userId, $locationId, $preCharge, 'PRECHARGE', json_encode(['guest_id' => $guest->getId()]), null );
        return true;
    }

    public function checkoutLocationGuest($locationId, $guestId, $refund)
    {
        $guest = $this->getLocationGuest($guestId, $locationId);
        if ( $guest->getChecked() == 1 ) {
            return false;
        }
        $ret = $this->_payPoints->modifyLocationPoints(
            $guest->getUserId(), $locationId, -$refund, 'REFUND', json_encode(['guest_id' => $guest->getId()]), null );
        if ( isset($ret['result']) && $ret['result']  == true ) {
            $guest->setChecked(1);
        } else {
            return false;
        }
        try {
            $this->_em->persist($guest);
            $this->_em->flush();
            $this->_em->refresh($guest);
        } catch( \Exception $e) {
            return false;
        }
        return $ret;
    }

    public function setSessionToLocationGuest($session, $guestId)
    {
        $guest = $this->_em->getRepository('EvBundle:EvLocationChargingGuest')
            ->findOneBy(['id' => $guestId, 'sessionId' => null, 'checked' => 0 ]);
        if ( $guest == null ) return false;
        $guest->setSessionId($session->getSessionId()) ;
        try {
            $this->_em->persist($guest);
            $this->_em->flush();
        } catch( \Exception $e) {
            return false;
        }
        return true;
    }
}
