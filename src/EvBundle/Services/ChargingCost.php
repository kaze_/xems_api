<?php
namespace EvBundle\Services;

use EvBundle\Entity\EvChargingLogMonthly;
use Doctrine\ORM\EntityManager;
use PDOException;
use PDO;
use AppBundle\Entity\EvChargingStationMaster;

class ChargingCost
{
    private $_em;
    private $_credit;
    
    public function __construct(EntityManager $em, Credit $credit)
    {
        $this->_em = $em;
        $this->_credit = $credit;
    }
    
    public function stationMonthlyCost($startDate)
    {
        $stations = $this->_em->getRepository('AppBundle:EvChargingStationMaster')->findAll();
        $monthlyFeeRepo = $this->_em->getRepository('EspBundle:EspElectricityUsageMonthly');
        $sessionRepo = $this->_em->getRepository('EvBundle:EvChargingSessionLog');
        $evMonthlyRepo = $this->_em->getRepository('EvBundle:EvChargingLogMonthly');
        
        foreach($stations as $station) {
//             if ( $station->getChargeMonthly() == 0 ) {
//                 continue;
//             }
            $circuits = $station->getCircuit();
            if ( $circuits == null || count($circuits) == 0 ) {
                continue;
            }
            $circuit = $circuits[0];
            
            $fee = $monthlyFeeRepo->findOneBy(['circuit' => $circuit, 'date' => $startDate]);
            if ( !$fee ) {
                continue;
            }
            // get calculated monthly fee
            $stationMonthly = $evMonthlyRepo->findOneBy(['chargingStationId' => $station, 'date' => $startDate]) ;
            if ( $stationMonthly == null ) {
                $stationMonthly = new EvChargingLogMonthly();
                $stationMonthly->setChargingStationId($station->getChargingStationId());
                $stationMonthly->setDate($startDate);
            }
            $stationMonthly->setTotalKwh($fee->getTotalUsage());
            $stationMonthly->setBasicCost(0);
            $stationMonthly->setAmountCost($fee->getEnergyFee());
            // get summary of sessions
            $endDate = clone $startDate;
            $endDate->modify('+1 months');
             
            if ( $endDate > new \DateTime() || $station->getChargeMonthly() == 0 ) {
                $stationMonthly->setOrderReady(0);
            } else {
                $stationMonthly->setOrderReady(1);
            }
            
            $qb = $sessionRepo->createQueryBuilder('q');
            $qb->select('sum(q.totalKwh) as kwh, sum(q.amountCost) as cost, sum(q.carbonReduction) as reduction')
                ->where('q.chargingStation = :station')
                ->andWhere('q.startAt between :start and :end')
                ->setParameter('station', $station)
                ->setParameter('start', $startDate)
                ->setParameter('end', $endDate);
    
            $result = $qb->getQuery()->getResult();
            print_r($result);
            print_r($stationMonthly);
            $this->_em->persist($stationMonthly);
            $this->_em->flush();
        }
    }
    
    /*
     * 
     */
    public function singleStationMonthlyOrder($startDate=null, $chargingStation=null, EvChargingLogMonthly $stationMonthly=null)
    {
        if ( $stationMonthly == null ) {
            $monthlyRepo = $this->_em->getRepository('EvBundle:EvChargingLogMonthly');
            $stationMonthly = $monthlyRepo->findOneBy(['date' => $startDate, 'chargingStationId' => $chargingStation]);
            if ( $stationMonthly == null ) {
                return null;
            }
        } else {
            $chargingStation = $stationMonthly->getChargingStationId();
        }
        
        $mappingRepo = $this->_em->getRepository('EvBundle:EvUserStationMapping');
        $mapping = $mappingRepo->findOneBy(['chargingStation' => $chargingStation, 'owner' => 1, 'statusId' => 1 ]);
        if ( $mapping == null ) {
            echo("no user related to charging station ".$chargingStation.PHP_EOL);
            return null;
        }
        $user = $mapping->getUser();
        
        $amt = $stationMonthly->getAmountCost() + $mapping->getUsageFee();
        $pdesc = "分攤電費、使用費";
        
        $order = $this->_credit->placeOrder($user, $amt, $pdesc);
        $stationMonthly->setOrder($order);
        $this->_em->flush($stationMonthly);
    }
    
    public function stationMonthlyOrder($startDate)
    {
        $monthlyRepo = $this->_em->getRepository('EvBundle:EvChargingLogMonthly');
        $stationsMonthly = $monthlyRepo->findBy(['date' => $startDate]);
        foreach($stationsMonthly as $stationMonthly) {
            $this->singleStationMonthlyOrder(null, null, $stationMonthly);
        }
    }
    
    public function stationMonthlyTrade($startDate)
    {
        
    }
    
    public function userMonthlyCost($startDate)
    {
        $em = $this->_em;
        /*
        SQL command: 
        insert into ev_user_charging_log_monthly (user_id, date, total_kwh, basic_cost, amount_cost, carbon_reduction) 
	        select sb.user_id, '2020-02-01', sb.sum_kwh, sb.sum_basic, sb.sum_amount, sb.sum_carbon 
	        from 
	        (select user_id, sum(total_kwh) as sum_kwh, sum(basic_cost) as sum_basic, sum(amount_cost) as sum_amount, sum(carbon_reduction) as sum_carbon
	        	from ev_charging_session_log as s
                where subdate(date(end_at), dayofmonth(end_at)-1) = '2020-02-01' and user_id is not null 
                group by user_id) as sb
	        on duplicate key update total_kwh = sb.sum_kwh, basic_cost = sb.sum_basic,  amount_cost = sb.sum_amount, carbon_reduction = sb.sum_carbon;
         */

        $RAW_QUERY = <<<EOQ
        insert into ev_user_charging_log_monthly (user_id, date, total_kwh, basic_cost, amount_cost, carbon_reduction) 
	        select sb.user_id, :startDate, sb.sum_kwh, sb.sum_basic, sb.sum_amount, sb.sum_carbon 
	        from 
	        (select user_id, sum(total_kwh) as sum_kwh, sum(basic_cost) as sum_basic, sum(amount_cost) as sum_amount, sum(carbon_reduction) as sum_carbon
	        	from ev_charging_session_log as s
                where subdate(date(end_at), dayofmonth(end_at)-1) = :startDate2 and user_id is not null 
                group by user_id) as sb
	        on duplicate key update total_kwh = sb.sum_kwh, basic_cost = sb.sum_basic,  amount_cost = sb.sum_amount, carbon_reduction = sb.sum_carbon;
EOQ;

        $result = null;
        try {
            $statement = $em->getConnection()->prepare($RAW_QUERY);
	    $dateStr = date_format($startDate, 'Y-m-d');
	    $statement->bindParam(':startDate', $dateStr, PDO::PARAM_STR);
	    $statement->bindParam(':startDate2', $dateStr, PDO::PARAM_STR);
            $statement->execute();
            // $result = $statement->fetchAll(PDO::FETCH_ASSOC);
            // print_r($result);
        } catch (PDOException $e) {
            echo($e->getMessage()) ;
        }
    }
    
    /*
     * 
     */
    public function singleUserMonthlyOrder($startDate=null, $chargingStation=null, EvChargingLogMonthly $stationMonthly=null)
    {
        if ( $stationMonthly == null ) {
            $monthlyRepo = $this->_em->getRepository('EvBundle:EvChargingLogMonthly');
            $stationMonthly = $monthlyRepo->findOneBy(['date' => $startDate, 'chargingStationId' => $chargingStation]);
            if ( $stationMonthly == null ) {
                return null;
            }
        } else {
            $chargingStation = $stationMonthly->getChargingStationId();
        }
        
        $mappingRepo = $this->_em->getRepository('EvBundle:EvUserStationMapping');
        $mapping = $mappingRepo->findOneBy(['chargingStation' => $chargingStation, 'owner' => 1, 'statusId' => 1 ]);
        if ( $mapping == null ) {
            echo("no user related to charging station ".$chargingStation.PHP_EOL);
            return null;
        }
        $user = $mapping->getUser();
        
        $amt = $stationMonthly->getAmountCost() + $mapping->getUsageFee();
        $pdesc = "分攤電費、使用費";
        
        $order = $this->_credit->placeOrder($user, $amt, $pdesc);
        $stationMonthly->setOrder($order);
        $this->_em->flush($stationMonthly);
    }

}

