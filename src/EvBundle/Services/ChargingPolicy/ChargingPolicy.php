<?php

namespace EvBundle\Services\ChargingPolicy;

abstract class ChargingPolicy
{
    public CONST VALIDATE_OK = 0;
    public CONST VALIDATE_BAD_USER = 1; // 不是合法使用者
    public CONST VALIDATE_NO_BALANCE = 2; // 餘額不足

    abstract function validateCharging($user, $evPlate, $station, $guest=false);
    abstract function calculateChargingFeeByFeeData($feeData, $station, $guest=false);
    abstract function calculateChargingFeeBySession($session, $guest=false);
    abstract function collectChargingFee($session, $guest=false);
}