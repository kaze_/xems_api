<?php

namespace EvBundle\Services\ChargingPolicy;

use DateTime;
use EvBundle\Services\ChargingPolicy\ChargingPolicy;
use Doctrine\ORM\EntityManager;
use EvBundle\Entity\EvChargingSessionLog;

class FareastChargingPolicy extends ChargingPolicy
{

    private CONST MINIMUM_CHARGING_POINT = 0;
    private CONST MININUM_CHARGING_KWH = 30;

    private $_em;
    private $_locationSetting;
    private $_payPoints;
    private $sessionKwh = [];
    private $_energyFee;

    private CONST TOU_TYPE = ["peak", "semi_peak", "saturday_semi_peak", "off_peak"];
    private const GUEST_UNIT = "#GUEST";

    // don't collect charging fee if power consumption of the session is under MIN_KWH
    private CONST MIN_KWH = 1;

    public function __construct(EntityManager $em, $locationSetting, $payPoints, $energyFee)
    {
        $this->_em = $em;
        $this->_locationSetting = $locationSetting;
        $this->_payPoints = $payPoints;
        $this->_energyFee = $energyFee;
    }

    /* guest user control... */

    /* location guest users controll */

    public function validateCharging($user, $evPlate, $location, $guest=false)
    {
        if ( $user == null ) {
            return self::VALIDATE_BAD_USER;
        }
        // find user in this location 
        $locationUserRepo = $this->_em->getRepository('EvBundle:EvUserLocationMapping');
        $locationUser = $locationUserRepo->findOneBy(['userId' => $user, 'locationId' => $location]) ;
        if ( !$locationUser ) {
            return self::VALIDATE_BAD_USER;
        }
        // not an active user
        if ( $locationUser->getStatusId() == 0 ) {
            return self::VALIDATE_BAD_USER;
        }
        // check user points
        $locationUserPoint = $this->_em->getRepository('EvBundle:EvLocationPoints')
            ->findOneBy(['userId' => $user, 'locationId' => $location]);
        $points = 0;
        if ( $locationUserPoint ) {
            $points = $locationUserPoint->getPoints();
        }

        // calculate kwh
        // TODO: guest???
        if ( self::MININUM_CHARGING_KWH ) {
            $estimateKwh = $this->estimateChargingKwh($location, $points, $guest);
            if ( $estimateKwh < self::MININUM_CHARGING_KWH ) {
                return self::VALIDATE_NO_BALANCE;
            }
        }
        // not enough points
        if ( $points < self::MINIMUM_CHARGING_POINT || $points <= 0 ) {
            return self::VALIDATE_NO_BALANCE;
        }
        return self::VALIDATE_OK;
    }

    public function estimateChargingKwh($location, $points, $guest=false)
    {
        $feeSetting = $this->_locationSetting->getCurrentEvChargingRate($location);
        if ( !$feeSetting ) {
            return 0;
        }
        $rateType = isset($feeSetting['rate_type'])? $feeSetting['rate_type']: 0;
        switch ($rateType) {
            case 1:
            case 2:
            case 3:
                // let them pass
                return self::MININUM_CHARGING_KWH;
            break;
            case 4:
                // get current tou
                $tou = $this->_energyFee->getCurrentTouByLocation($location);
                if ( $tou === 0 || $tou === null ) {
                    return 0;
                }
                $strTou = $tou;
                if ( $guest == true ) {
                    $strTou = 'guest_'. $tou;
                } 
                if ( isset($feeSetting['rate_parameter'][$strTou])) {
                    $estimateKwh = $points / $feeSetting['rate_parameter'][$strTou];
                    return $estimateKwh;
                }
                return 0;
        }
    }

    /**
     * Note, log demand usage
     *         "fee": {
     *              "peak": {
     *              "amount": 31.536765000000432,
     *              "kwh": 9.47050000000013,
     *              "usage": 10.843452736767
     *              }
     *          },
     *          "startKwh": ...,
     *          "amount": ...,
     *          "endKwh": ...,
     *          "totalKwh": ...,
     */
    public function calculateChargingFeeByFeeData($feeData, $station, $guest=false)
    {
        if  ( !$feeData ) {
            return 0;
        }
        $location = $this->_em->getRepository('AppBundle:EmsLocationMaster')->findByChargingStation($station->getChargingStationId());
        // get location charging fee type
        $feeSetting = $this->_locationSetting->getCurrentEvChargingRate($location);
        if ( !$feeSetting ) {
            return 0;
        }
        $rateType = isset($feeSetting['rate_type'])? $feeSetting['rate_type']: 0;
        $realKwh = $feeData['totalKwh'];
        if ( $realKwh < self::MIN_KWH ) {
            return 0;
        }
        switch($rateType) {
            case 1: 
                return 0;
            break;
            case 2: 
                return $feeData["amount"];
            break;
            case 3:
                $percentage = isset($feeSetting['rate_parameter']) ? $feeSetting['rate_parameter']: 0;
                return ($feeData["amount"] * (1 + $percentage/100));
            break;
            case 4:
                $rate = $feeSetting['rate_parameter'];
                $totalFee = 0;
                $kwhArr = [];
                $totalKwh = 0;
                if ( $feeData["fee"]) {
                    foreach($feeData['fee'] as $tou => $value) {
                        if(isset($value["kwh"])) {
                            $kwh = $value["kwh"];
                        } else {
                            continue;
                        }
                        $touStr = $tou;
                        if ( $guest == true ) {
                            $touStr = "guest_".$tou;
                        }
                        if ( isset($rate[$touStr])) {
                            $fee = $rate[$touStr];
                        } else {
                            $fee = 0;
                            // log
                        }
                        $calcKwh = ceil($kwh);
                        $calcFee = $calcKwh * $fee;
                        $totalFee += $calcFee;
                        $kwhArr[$touStr."_kwh"] = $calcKwh;
                        $kwhArr[$touStr."_fee"] = $calcFee;
                        $totalKwh += $calcKwh;
                    }
                }
                $kwhArr['total_kwh'] = $totalKwh;
                return $totalFee;
            break;
            case 5:
                // TBD
            break;
        }   
        return 0;
    }

    public function getChargingDetailBySession($session)
    {
        $sessionId = $session->getSessionId()->toString();
        if ( isset($this->sessionKwh[$sessionId])) {
            return $this->sessionKwh[$sessionId];
        }
    }

    /**
     * 檢查 $start 和 $end 之間是否跨過 $time ('HH:ii:ss') 這個時間點
     * @param string: time ('HH:ii:ss')
     * @param DateTime: start
     * @param DateTime: end
     * @return boolean
     */
    public static function checkTimeBetweenDatetimes($time, $start, $end)
    {
        // kinda stupid, find a better solution some other time
        $between = false;
        $check = DateTime::createFromFormat('Y-m-d H:i:s', $start->format('Y-m-d').' '.$time);
        // if check > end, means checktime is not between start and end.
        // check --- start --- end
        // start --- check --- end (break, and addBonus = true)
        // start --- end --- check (end loop)
        while( $check < $end ) {
            if ( $start < $check ) {
                $between = true;
                break;
            }
            $check->modify('+1 days');
        }
        return $between;
    }

    /**
     * 檢查 $start 和 $end 之間是否重疊 $time_start $time_end ('HH:ii:ss') 這個時段
     * @param string: time_start ('HH:ii:ss')
     * @param string: time_end ('HH:ii:ss')
     * @param DateTime: start
     * @param DateTime: end
     * @return boolean
     */
    public static function checkTimeBetweenTimePeriod($time_start, $time_end, $start, $end)
    {
        $start_check = DateTime::createFromFormat('H:i:s', $time_start);
        $end_check = DateTime::createFromFormat('H:i:s', $time_end);

        // 跨日的情況
        if ($end_check < $start_check) {
            $end_check = DateTime::createFromFormat('Y-m-d H:i:s', $start->format('Y-m-d').' '.$time_end);
            $end_check->modify('+1 days');
        }
        $end_check = $end_check->getTimestamp();
        $start_check = DateTime::createFromFormat('Y-m-d H:i:s', $start->format('Y-m-d').' '.$time_start);
        $start_check = $start_check->getTimestamp();

        $session_start_time = $start->getTimestamp();
        $session_end_time = $end->getTimestamp();

        if ($session_end_time < $session_start_time) {
            return false;
        }

        return $session_start_time >= $start_check || $session_end_time >= $start_check;
    }

    public function calculateChargingFeeBySession($session, $guest=false)
    {
        $station = $session->getChargingStation();
        $location = $this->_em->getRepository('AppBundle:EmsLocationMaster')->findByChargingStation($station->getChargingStationId());
        // get location charging fee type
        $feeSetting = $this->_locationSetting->getCurrentEvChargingRate($location);
        if ( !$feeSetting ) {
            return 0;
        }
        $rateType = isset($feeSetting['rate_type'])? $feeSetting['rate_type']: 0;
        switch($rateType) {
            case 1: 
                return 0;
            break;
            case 2: 
                return $session->getAmountCost();
            break;
            case 3:
                $percentage = isset($feeSetting['rate_parameter']) ? $feeSetting['rate_parameter']: 0;
                return ($session->getAmountCost() * (1 + $percentage/100));
            break;
            case 4:
                $rate = $feeSetting['rate_parameter'];
                $totalFee = 0;
                $kwhArr = [];
                $totalKwh = 0;
                foreach(self::TOU_TYPE as $tou) {
                    // $touFuncStr = str_replace('_', '', ucwords($tou, '_'));
                    $touStr = $tou;
                    if ( $guest == true ) {
                        $touStr = "guest_".$tou;
                    }
                    $kwh = $session->{'get'.str_replace('_', '', ucwords($tou, '_')).'Kwh'}();
                    if ( isset($rate[$touStr])) {
                        $fee = $rate[$touStr];
                    } else {
                        $fee = 0;
                        // log
                    }
                    $calcKwh = ceil($kwh);
                    $calcFee = ceil($calcKwh * $fee);
                    $totalFee += $calcFee;
                    if ( $calcKwh != 0 ) {
                        $kwhArr[$touStr."_kwh"] = $calcKwh;
                    } 
                    if ( $calcFee != 0 ) {
                        $kwhArr[$touStr."_fee"] = $calcFee;
                    } 
                    $totalKwh += $calcKwh;
                }
                // calculate off_peak time bonus;
                if ( isset($rate['off_peak_bonus'])) {
                    $bonusTime = $rate['off_peak_bonus'];
                    if ( $bonusTime && isset( $bonusTime['time']) && isset( $bonusTime['time_end']) && isset($bonusTime['kwh'])) {
                        $time_start = $bonusTime['time'];
                        $time_end = $bonusTime['time_end'];
                        $bonusKwh = $bonusTime['kwh'];
                        $start = $session->getStartAt();
                        $end = $session->getEndAt();

                        $is_time_overlap = $this->checkTimeBetweenTimePeriod($time_start, $time_end, $start, $end);
                        if ( $totalKwh < $bonusKwh && $is_time_overlap) {
                            if ( $guest == true ) {
                                $off_peak_rate = isset($rate['guest_off_peak']) ? $rate['guest_off_peak'] : 0;
                            } else {
                                $off_peak_rate = isset($rate['off_peak']) ? $rate['off_peak'] : 0;
                            }
                            // 未滿規定的度數，就以該規定度數計算
                            $bonusFee = $off_peak_rate * $bonusKwh;
                            $totalFee = $bonusFee;
                            $kwhArr['off_peak_bonus_fee'] = $bonusFee;
                        }
                    }
                }
                $kwhArr['total_kwh'] = $totalKwh;
                $sessionId = $session->getSessionId()->toString();
                $this->sessionKwh[$sessionId] = $kwhArr;
                return $totalFee;
            break;
            case 5:
                // TBD
            break;
        }   
        return 0;
    }

    public function collectChargingFee($session, $guest=false)
    {
        $station = $session->getChargingStation();
        $location = $this->_em->getRepository('AppBundle:EmsLocationMaster')->findByChargingStation($station->getChargingStationId());
        $sessionId = $session->getSessionId();
        // check if location exists
        if (!$location) {
            return "session ".$sessionId.".location not found";
        }
        // check if session is already paid
        $sessionPayLog = $this->_payPoints->getPointsLogBySession($sessionId);
        if($sessionPayLog) {
            return "session ".$sessionId.".already paid";
        }
        $fee = $this->calculateChargingFeeBySession($session, $guest);
        // if ( $fee ) {
        // log even if $fee = 0; especially if user is a
            if ( $session->getUserId() == null ) {
                return "session ". $sessionId.": no user found";
            }
            $kwhStr = "";
            $kwh = $this->getChargingDetailBySession($session, $guest);
            if ( $kwh ) {
                $kwhStr = json_encode($kwh);
                if ( $kwhStr == null ) $kwhStr = "";
            }
            return $this->_payPoints->modifyLocationPoints(
                $session->getUserId()->getId(), $location->getLocationId(), (-1)*ceil($fee), 'CHARGING', 
                $kwhStr, $session->getSessionId() );
        // }
        return "session " .$sessionId. ": no session fee collected";
    }
}
