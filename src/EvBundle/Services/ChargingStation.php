<?php

namespace EvBundle\Services;

use AppBundle\Entity\EvChargingStationMaster;
use Doctrine\ORM\EntityManager;

class ChargingStation
{
    private $_em;
    
    public function __construct(EntityManager $em)
    {
        $this->_em = $em;
    }

    public function vipOn(EvChargingStationMaster $station)
    {
        $station->setPriorityCharging(1);
        $this->_em->persist($station);
        $this->_em->flush();
        return true;
    }
    public function vipOff(EvChargingStationMaster $station)
    {
        $station->setPriorityCharging(0);
        $this->_em->persist($station);
        $this->_em->flush();
        return true;
    }
}