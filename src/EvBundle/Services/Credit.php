<?php

namespace EvBundle\Services;


use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use EvBundle\Entity\CreditCard;
use EvBundle\Entity\PayHistory;
use EvBundle\Services\Credit\CreditInterface;
use Predis\Client;
use Symfony\Bridge\Monolog\Logger;
use Exception;
use Doctrine\DBAL\LockMode;
use EvBundle\Services\Credit\MockPayment;
use EvBundle\Services\Credit\Pay2goToken;

class Credit
{

    private $_em;
    private $_credit;
    private $_logger;
    private $_env;
    
    public function __construct(CreditInterface $credit, EntityManager $em, logger $logger, $env) {
        $this->_em = $em;
        $this->_credit = $credit;
        $this->_logger = $logger;
        $this->_env = $env;
    }

    public function add(User $user, CreditCard $creditCard, $paymentType=null) {
        $credit = null;
        if ( MockPayment::isTestCreditCard($creditCard)) {
            $credit = new MockPayment($this->_em);
        } else {
            $credit = $this->_credit;
        }
        return $credit->add($user, $creditCard);
    }

    public function delete($paymentId) {
        return $this->_credit->delete($paymentId);
    }
    
    public function check($user) {
        $repo = $this->_em->getRepository('EvBundle:PayPay2goToken');
        $payment = $repo->findOneBy(['userId' => $user->getId()->toString(), 'status' => CreditCard::STATUS_OK]);
        if ( $payment != null ) {
            return CreditCard::STATUS_OK;
        } 
        return null;
    }
    
    public function get($user) {
        $result = $this->_credit->get($user);
        return $result;
    }
    
    public function getDefault($user) {
        $result = $this->_credit->get($user);
        if ( $result != null && count($result) != 0 ) {
            return $result[0];
        }
        return null;
    }
    
    public function verify(User $user, CreditCard $creditCard) {
        //get test order no
        $payHistory = new PayHistory();
        $payHistory->setUserId($user->getId()->toString());
        $payHistory->setAmount(1);
        $payHistory->setPdesc('verify');
        $payHistory->setIsVerify(true);
        $payHistory->setTradeAt(new \DateTime());
        $payHistory->setCreditCard($creditCard);
        try {
            $this->_em->persist($payHistory);
            $this->_em->flush($payHistory);
        } catch (Exception $ex) {
            $this->_logger->error('[error before trade] '.$ex->getMessage());
            return false;
        }

        $redis = new Client();

        $ps = 0;
        //check service on
        exec('ps aux | grep -i \'bin/console pay:trade\' ', $ps);
        //sync mode
        if(count($ps) < 3){
            return $this->trade($payHistory->getId());
//            $cmd = 'pay:trade '.$payHistory->getId();
//            exec('php ../bin/console '.$cmd.' --env='.$this->_env, $output);
//            $re = json_decode(@$output[0]);
//            return $re->success;
        }
        //async mode
        return $redis->publish('paytrade', $payHistory->getId());
    }


    public function doTrade($orderId)
    {
        set_time_limit(0);
        ini_set('memory_limit','1024M');

        $em = $this->_em;
        
        $em->getConnection()->beginTransaction();
        //trade
        try {
            $payHistory = $em->getRepository('EvBundle:PayHistory')->find($orderId, LockMode::PESSIMISTIC_WRITE);
            $status = $payHistory->getStatus();
            
            if ( $status != PayHistory::STATUS_TRADING && $status != PayHistory::STATUS_FAILED) { 
                throw new Exception("Order Status: Canceled or Success already. Cannot trade again.");
            }
            $user = $em->find('AppBundle:User', $payHistory->getUserId());
            $payment = $em->find('EvBundle:PayPay2goToken', $payHistory->getPaymentId());
            
            $re = $this->_credit->send($user, $payment, $payHistory);
            $em->persist($re);
            $em->flush();
            $em->commit();
        } catch (Exception $ex) {
            $em->rollback();
            $this->_log('[Error after trade] '.$ex->getMessage());
            return false;
        }
            
        return boolval($payHistory->getStatus());
    }
    
    public function placeOrder($user, $amt, $pDesc, $paymentType=null, $creditCard=null)
    {
        $userId = $user->getId()->toString();
        $order = new PayHistory();
        $order->setUserId($userId);
        $order->setAmount($amt);
        $order->setPdesc($pDesc);
        $order->setIsVerify(false);
        $order->setTradeAt(new \DateTime());
        
        if ( $paymentType === null ) {
            $repo = $this->_em->getRepository('EvBundle:PayPay2goToken');
            $payment = $repo->findOneBy(['userId' => $userId, 'status' => CreditCard::STATUS_OK]);
            
            if ( $payment == null ) {
                return null;
            }
            $order->setPaymentTypeId(2);
            $order->setPaymentId($payment->getPaymentId());
        }
        $this->_em->persist($order);
        $this->_em->flush();
        return $order;
    }
    
    public function trade($order)
    {
        if ( $order instanceof PayHistory) {
            $orderId = $order->getId();
        } else {
            $orderId = $order;
        }
        $ps = 0;
        $redis = new Client();
        exec('ps aux | grep -i \'bin/console pay:trade\' ', $ps);
        //sync mode
        if(count($ps) < 3){
            return $this->doTrade($orderId);
        }
        //async mode
        return $redis->publish('paytrade', $orderId);
    }
    
    public function cancelOrder($order)
    {
        $this->_credit->deauthorize($order);
    }

    public function pay(CreditCard $creditCard) {
        
    }
    
    public function getInfo(CreditCard $creditCard) {
        //decode
    }
    
    private function _log($msg)
    {
        $this->_logger->info($msg);
//        echo $msg;
    }
}
