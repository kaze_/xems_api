<?php
/**
 * Created by PhpStorm.
 * User: wild
 * Date: 2018/1/19
 * Time: 下午3:09
 */

namespace EvBundle\Services\Credit;


use AppBundle\Entity\User;
use EvBundle\Entity\CreditCard;
use EvBundle\Entity\PayHistory;

interface CreditInterface
{

    // public function encrypt($str);
    public function send(User $user, $payment, PayHistory $payHistory);
    public function add(User $user,  $paymentData);
    public function deauthorize(PayHistory $order);
    
}