<?php

namespace EvBundle\Services\Credit;


use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use EvBundle\Entity\CreditCard;
use EvBundle\Entity\PayHistory;
use Exception;
use EvBundle\Entity\PayAuthHistory;
use EvBundle\Entity\PayPay2goToken;

class MockPayment implements CreditInterface
{

    private $_em;

    const VERSION = '1.0';
    const TEST_CREDIT_CARD = [
        '1111111111111111', 
        '4000111111111111', 
        '4000111111111112', 
        '4000111111111113'
    ];

    public function __construct(EntityManager $em) {
        $this->_em = $em;
    }

    public static function isTestCreditCard(CreditCard $creditCard) {
        if ( in_array($creditCard->getCardNo(), self::TEST_CREDIT_CARD) ) {
            return true;
        }
        return false;
    }

    public function send(User $user,  $creditCard , PayHistory $order, $timeToLive=3) { 
        
        $em = $this->_em;
        if($order->getStatus() == PayHistory::STATUS_SUCCESS){
            return $order;
        }
        $log = new PayAuthHistory();
        $log->setOrderId($order->getId());
        $log->setAction("MockAuth");
        $log->setStatus(PayHistory::STATUS_SUCCESS);
        $em->persist($log);

        if ($creditCard instanceof CreditCard ) {
            $token = new PayPay2goToken();
            $cardNo = $creditCard->getCardNo();
            $token->setCard6No(substr($cardNo, 6));
            $token->setCard4No(substr($cardNo, -4));
            $token->setStatus(1);
            $token->setUserId($user->getId()->toString());
            
            $em->persist($token);
            
            $order->setPaymentId($token->getPaymentId());
        }
        
        $em->flush();
        
        $order->setPaymentTypeId(0);
        $order->setPaymentId($token->getPaymentId());
        $order->setStatus(PayHistory::STATUS_SUCCESS);
        
        return $order;
    }
    
    /**
     * @param PayHistory $order : origin order
     * @return $order : new deauthorize order
     */
    public function deauthorize(PayHistory $order, $timeToLive=3)
    {
        if ( $order == null ) {
            return null;
        }
        
        $em = $this->_em;
        
        $log = new PayAuthHistory();
        $log->setOrderId($order->getId());
        $log->setAction("MockCancel");
        $log->setStatus(PayHistory::STATUS_CANCELED);
        $em->persist($log);
        $em->flush();

        $order->setStatus(PayHistory::STATUS_CANCELED);
        
        return $order;
        
    }
    
    public function add(User $user,  $creditCard) 
    {
        $result = false;
        
        $payHistory = new PayHistory();
        $payHistory->setUserId($user->getId()->toString());
        $payHistory->setAmount(1);
        $payHistory->setPdesc('Verify and get Token');
        $payHistory->setIsVerify(true);
        $payHistory->setTradeAt(new \DateTime());
        
        // $order = $this->send($user, $creditCard, $payHistory);
        try {
            $this->_em->persist($payHistory);
            $this->_em->flush();
        } catch (Exception $ex) {
            $this->_log('[Error before trade] '.$ex->getMessage());
            return false;
        }
        
        $order = $this->send($user, $creditCard, $payHistory);
        if ( $order->getStatus() == PayHistory::STATUS_SUCCESS) {
            $order = $this->deauthorize($order);
        }
        try {
            $this->_em->persist($order);
            $this->_em->flush();
            $result = true;
        } catch (Exception $ex) {
            $this->_log('[Error after trade] '.$ex->getMessage());
            return false;
        }
        return $result;
    }
    
    public function delete($paymentId)
    {
        $repo = $this->_em->getRepository('EvBundle:PayPay2goToken');
        $token = $repo->findOneBy(['paymentId' => $paymentId]);
        if ( $token == null ) {
            return false;
        }
        
        try {
            $token->setStatus(CreditCard::STATUS_DELETE);
            $this->_em->persist($token);
            $this->_em->flush();
        } catch(Exception $ex) {
            echo($ex->getMessage());
            return false;
        }
        return true;
    }
}

