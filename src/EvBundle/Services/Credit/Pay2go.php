<?php

namespace EvBundle\Services\Credit;


use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use EvBundle\Entity\CreditCard;
use EvBundle\Entity\PayHistory;
use function intval;
use function openssl_encrypt;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Validation;
use function time;
use function trim;
use function var_dump;

class Pay2go implements CreditInterface
{

    private $_em;
    private $_hashKey;
    private $_hashIv;
    private $_merchantID;
    private $_error;

    const URL = 'https://core.spgateway.com/API/CreditCard';
    const VERSION = '1.0';
    
    public function __construct($hashKey, $hashIv, $merchantID, EntityManager $em) {
        $this->_em = $em;
        $this->_hashKey = $hashKey;
        $this->_hashIv = $hashIv;
        $this->_merchantID = $merchantID;
//        $this->_hashKey = '12345678901234567890123456789012';
//        $this->_hashIv = '1234567890123456';
    }

    private function _addpadding($string, $blocksize = 32) {
        $len = strlen($string);
        $pad = $blocksize - ($len % $blocksize);
        $string .= str_repeat(chr($pad), $pad);

        return $string;
    }

    public function encrypt ($str) {
        return trim(bin2hex(openssl_encrypt(
            $this->_addpadding($str), 
            'aes-256-cbc', 
            $this->_hashKey, 
            OPENSSL_RAW_DATA|OPENSSL_ZERO_PADDING, 
            $this->_hashIv)));
    }

    public function send(User $user, CreditCard $creditCard, PayHistory $payHistory) {
        //check status
        if($payHistory->getStatus() == PayHistory::STATUS_SUCCESS){
            return $payHistory;
        }
        
        $postData = array(
            'TimeStamp' => time(),
            'Version' => self::VERSION,
            'MerchantOrderNo' => trim($payHistory->getId()),
            'Amt' => intval($payHistory->getAmount()),
            'ProdDesc' => trim($payHistory->getPdesc()),
            'PayerEmail' => trim($user->getEmail()),
            'CareAE' => intval($creditCard->getIsAe()),
            'CardNo' => trim($creditCard->getCardno()),
            'Exp' =>  trim($creditCard->getExpY()).trim($creditCard->getExpM()),
            'CVC' => trim($creditCard->getCvc())
        );
        
        if( ! empty($inst)){
            $postData['Inst'] = trim($inst);
        }
        
        $validator = Validation::createValidator();
        $errors = $validator->validate($postData,
            new Collection(array(
                'MerchantOrderNo' => [new NotNull(), new Length(array('max' => 20))],
                'ProdDesc' => [new NotNull(), new Length(array('max' => 50))],
                'PayerEmail' => [new NotNull(), new Email()],
                'CardNo' => [new NotNull(), new Length(array('max' => 16))],
                'Exp' => [new NotNull(), new Length(array('min' => 4, 'max' => 4))],
                'CVC' => [new NotNull(), new Length(array('max' => 4))],
                'TimeStamp' => [],
                'Version' => [],
                'Amt' => [],
                'CareAE' => [],
                
            ))
        );
        if(isset($errors[0])){
            return $errors->get(0)->getPropertyPath().' '.$errors->get(0)->getMessage();
        }
        
        
        $str = http_build_query($postData);
        $res = $this->encrypt($str);
        $data = array(
            'MerchantID_' => $this->_merchantID,
            'PostData_' => $res,
            'Pos_' => 'JSON'
        );
        
        $ch = curl_init(self::URL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $result = curl_exec($ch);
        curl_close($ch);

        if ($result === FALSE) {
            $payHistory->setStatus(PayHistory::STATUS_FAILED);
        }else{
            $result = json_decode($result);
        }

        $payHistory->setResult($result);
        $payHistory->setStatus(($result->Status == 'SUCCESS') ? PayHistory::STATUS_SUCCESS : PayHistory::STATUS_FAILED);
        $payHistory->setErrorCode($result->Status);
        
            
        return $payHistory;
    }
    public function add(User $user, $paymentData)
    {}
    public function delete($paymentId)
    {}
    public function deauthorize(PayHistory $order)
    {}
}
