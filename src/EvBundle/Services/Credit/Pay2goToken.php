<?php

namespace EvBundle\Services\Credit;


use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use EvBundle\Entity\CreditCard;
use EvBundle\Entity\PayHistory;
use function intval;
use function openssl_encrypt;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Validation;
use Exception;
use function time;
use function trim;
use function var_dump;
use EvBundle\Entity\PayPay2goToken;
use EvBundle\Entity\PayAuthHistory;

class Pay2goToken implements CreditInterface
{

    private $_em;
    private $_hashKey;
    private $_hashIv;
    private $_merchantID;
    private $_error;
    private $_url;

    const VERSION = '1.0';
    
    public function __construct($url, $hashKey, $hashIv, $merchantID,  EntityManager $em) {
        $this->_em = $em;
        $this->_hashKey = $hashKey;
        $this->_hashIv = $hashIv;
        $this->_merchantID = $merchantID;
        $this->_url = $url;
    }

    private function _addpadding($string, $blocksize = 32) {
        $len = strlen($string);
        $pad = $blocksize - ($len % $blocksize);
        $string .= str_repeat(chr($pad), $pad);

        return $string;
    }

    function pay2go_encrypt($str) {
        $str = trim(bin2hex(
            mcrypt_encrypt(
                MCRYPT_RIJNDAEL_128, 
                $this->_hashKey, 
                $this->_addpadding($str), 
                MCRYPT_MODE_CBC, 
                $this->_hashIv)));
        return $str;
    }
    
    public function encrypt ($str) {
        return trim(bin2hex(openssl_encrypt(
            $this->_addpadding($str), 
            'aes-256-cbc', 
            $this->_hashKey, 
            OPENSSL_RAW_DATA|OPENSSL_ZERO_PADDING, 
            $this->_hashIv)));
    }

    function get_checkvalue($amt, $timestamp, $merchantOrderNo)
    {
        
        $timestamp = time();
        $merchantOrderNo = strval($timestamp);
        $mer_array = array(
            'MerChantID' => $this->_merchantID,
            'Timestamp' => $timestamp,
            'MerchantOrderNo' => $merchantOrderNo,
            'Version' => self::VERSION,
            'Amt' => $amt
        );
        ksort($mer_array);
        $check_merstr = http_build_query($mer_array);
        $checkvalue_str = "HashKey=$this->_hashKey&$check_merstr&HashIV=$this->_hashIv";
        $check_value = strtoupper(hash("sha256", $checkvalue_str));
        return $check_value;
    }
    
    function get_checkcode($amt, $merchantOrderNo, $tradeNo)
    {
        $code_array = array(
            'Amt' => $amt,
            'MerchantID' => $this->_merchantID,
            'MerchantOrderNo' => $merchantOrderNo,
            'TradeNo' => $tradeNo
        );
        ksort($code_array);
        $code_merstr = http_build_query($code_array);
        $check_code_str = "HashIV=$this->_hashIv&$code_merstr&HashKey=$this->_hashKey";
        $check_code = strtoupper(hash("sha256", $check_code_str));
        return $check_code;
    }
 
    public function send(User $user,  $creditCard , PayHistory $order, $timeToLive=3) { 
        
        $em = $this->_em;
        
        $timestamp = time();
        
        if($order->getStatus() == PayHistory::STATUS_SUCCESS){
            return $order;
        }
        
        $postData = array(
            'TimeStamp' => $timestamp,
            'Version' => self::VERSION,
            'MerchantOrderNo' => trim($order->getId()),
            'Amt' => intval($order->getAmount()),
            'ProdDesc' => trim($order->getPdesc()),
            'PayerEmail' => trim($user->getEmail())
        );
        
        $log = new PayAuthHistory();
        $log->setOrderId($order->getId());
        $log->setAction("Auth");
        $em->persist($log);
        
        if (  $creditCard instanceof CreditCard ) {
            $postData['CardNo'] = trim($creditCard->getCardNo());
            $postData['Exp'] = trim($creditCard->getExpY()).trim($creditCard->getExpM());
            $postData['CVC'] = trim($creditCard->getCvc());
            $postData['TokenSwitch'] = "get";
            $postData['TokenLife'] = $postData['Exp'];
            $postData['TokenTerm'] = trim($user->getId());
        } else if ( $creditCard instanceof PayPay2goToken) {
            $postData['TokenValue'] = $creditCard->getToken();
            $postData['TokenSwitch'] = "on";
            $postData['TokenTerm'] = trim($user->getId());
        }
        
//         $validator = Validation::createValidator();
//         $errors = $validator->validate($postData,
//             new Collection(array(
//                 'MerchantOrderNo' => [new NotNull(), new Length(array('max' => 20))],
//                 'ProdDesc' => [new NotNull(), new Length(array('max' => 50))],
//                 'PayerEmail' => [new NotNull(), new Email()],
//                 'CardNo' => [new NotNull(), new Length(array('max' => 16))],
//                 'Exp' => [new NotNull(), new Length(array('min' => 4, 'max' => 4))],
//                 'CVC' => [new NotNull(), new Length(array('max' => 4))],
//                 'TimeStamp' => [],
//                 'Version' => [],
//                 'Amt' => [],
//             ))
//             );
//         if(isset($errors[0])){
//             return $errors->get(0)->getPropertyPath().' '.$errors->get(0)->getMessage();
//         }
        
        
        $str = http_build_query($postData);
        $res = $this->pay2go_encrypt($str);
        // $res = $this->encrypt($str);
        $data = array(
            'MerchantID_' => $this->_merchantID,
            'PostData_' => $res,
            'Pos_' => 'JSON'
        );
        
        $ch = curl_init($this->_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $res = curl_exec($ch);
        curl_close($ch);
        
        if ($res === FALSE) {
            $order->setStatus(PayHistory::STATUS_FAILED);
            $log->setStatus(PayHistory::STATUS_FAILED);
            $em->persist($log);
            $em->flush();
            return $order;
        } else {
            $result = json_decode($res, true);
        }

        $log->setResult($res);
        if ($result['Status'] == 'SUCCESS') {
            if ($this->get_checkcode($order->getAmount(), $order->getId(), $result['Result']['TradeNo']) != $result['Result']['CheckCode']) {
                $order->setStatus(PayHistory::STATUS_FAILED);
                $order->setErrorCode($result->Status . ' but check code does not match');
            } else {
                if ($creditCard instanceof CreditCard ) {
                    $token = new PayPay2goToken();
                    $token->setCard6No($result['Result']['Card6No']);
                    $token->setCard4No($result['Result']['Card4No']);
                    $token->setStatus(1);
                    $token->setToken($result['Result']['TokenValue']);
                    $token->setTokenLife($result['Result']['TokenLife']);
                    $token->setUserId($user->getId()->toString());
                    
                    $em->persist($token);
                    $em->flush($token);
                    
                    $order->setPaymentId($token->getPaymentId());
                    $order->setPaymentTypeId(2);
                }
                $order->setAuth($result['Result']['Auth']);
                $order->setErrorCode($result['Status']);
                $order->setStatus(PayHistory::STATUS_SUCCESS);
                $log->setStatus(PayHistory::STATUS_SUCCESS);
            }
        } else if ($result['Status'] == 'TRA10067' && (--$timeToLive) > 0) { 
            // wrong IP address, for Pay2Go only accept 3 IP address, while Azure Web App have 4 outbound IP
            $order = $this->send($user,  $creditCard , $order, $timeToLive);
            $log->setStatus($order->getStatus());
            $em->persist($log);
            $em->flush();
            return $order;
        } else {
            $order->setErrorCode($result['Status']);
            $order->setStatus(PayHistory::STATUS_FAILED);
            $log->setStatus(PayHistory::STATUS_FAILED);
        }
        $em->persist($log);
        $em->flush();
        $order->setResult($result);
        
        return $order;
    }
    
    /**
     * @param PayHistory $order : origin order
     * @return $order : new deauthorize order
     */
    public function deauthorize(PayHistory $order, $timeToLive=3)
    {
        if ( $order == null ) {
            return null;
        }
        
        $em = $this->_em;
        
        $log = new PayAuthHistory();
        $log->setOrderId($order->getId());
        $log->setAction("Cancel");
        $em->persist($log);
        
        $postData = array(
            'RespondType' => 'JSON',
            'Version' => '1.0',
            'Amt' => $order->getAmount(),
            'MerchantOrderNo'=> $order->getOrderId(),
            'IndexType' => 1,
            'TimeStamp' => time()
        );
        $str = http_build_query($postData);
        $res = $this->pay2go_encrypt($str);
        // $res = $this->encrypt($str);
        $data = array(
            'MerchantID_' => $this->_merchantID,
            'PostData_' => $res,
            'Pos_' => 'JSON'
        );
        
        $ch = curl_init($this->_url."/Cancel");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $res = curl_exec($ch);
        curl_close($ch);
        
        if ($res === FALSE) {
            $log->setStatus(PayHistory::STATUS_FAILED);
            $em->persist($log);
            $em->flush();
            return null;
        } else {
            $result = json_decode($res, true);
        }
        $log->setResult($res);
        if ($result['Status'] == 'SUCCESS') {
            if ($this->get_checkcode($order->getAmount(), $order->getId(), $result['Result']['TradeNo']) != $result['Result']['CheckCode']) {
                $order->setStatus(PayHistory::STATUS_FAILED);
                $log->setStatus(PayHistory::STATUS_FAILED);
                $order->setErrorCode($result->Status . ' but check code does not match');
            } else {
                $order->setErrorCode($result['Status']);
                $order->setStatus(PayHistory::STATUS_CANCELED);
                $log->setStatus(PayHistory::STATUS_CANCELED);
            }
        } else if ( $result['Status'] == 'TRA10067' && (--$timeToLive) > 0) {
            // wrong IP address, for Pay2Go only accept 3 IP address, while Azure Web App have 4 outbound IP
            $order = $this->deauthorize($order, $timeToLive);
            $log->setStatus($order->getStatus());
            $em->persist($log);
            $em->flush();
            return $order;
        } else {
            $log->setStatus(PayHistory::STATUS_FAILED);
        }
        $em->persist($log);
        $em->flush();
        $order->setResult($result);
        
        return $order;
        
    }
    
    public function add(User $user,  $creditCard) 
    {
        $result = false;
        
        $payHistory = new PayHistory();
        $payHistory->setUserId($user->getId()->toString());
        $payHistory->setAmount(1);
        $payHistory->setPdesc('Verify and get Token');
        $payHistory->setIsVerify(true);
        $payHistory->setTradeAt(new \DateTime());
        
        // $order = $this->send($user, $creditCard, $payHistory);
        try {
            $this->_em->persist($payHistory);
            $this->_em->flush();
        } catch (Exception $ex) {
            $this->_log('[Error before trade] '.$ex->getMessage());
            return false;
        }
        
        $order = $this->send($user, $creditCard, $payHistory);
        if ( $order->getStatus() == PayHistory::STATUS_SUCCESS) {
            $order = $this->deauthorize($order);
        }
        try {
            $this->_em->persist($order);
            $this->_em->flush();
            $result = true;
        } catch (Exception $ex) {
            $this->_log('[Error after trade] '.$ex->getMessage());
            return false;
        }
        return $result;
    }
    
    public function delete($paymentId)
    {
        $repo = $this->_em->getRepository('EvBundle:PayPay2goToken');
        $token = $repo->findOneBy(['paymentId' => $paymentId]);
        if ( $token == null ) {
            return false;
        }
        
        try {
            $token->setStatus(CreditCard::STATUS_DELETE);
            $this->_em->persist($token);
            $this->_em->flush();
        } catch(Exception $ex) {
            echo($ex->getMessage());
            return false;
        }
        return true;
    }
    
    public function get($key) 
    {
        $repo = $this->_em->getRepository('EvBundle:PayPay2goToken');
        if ( $key instanceof User) {
            $result = $repo->findBy(['userId' => $key->getId()->toString(), 'status' => CreditCard::STATUS_OK]);
        } else {
            // paymentId
            $result = $repo->findBy(['paymentId' => $key]) ;
        }
        return $result;
    }

}

