<?php

namespace EvBundle\Services;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;

class FAQ
{
    private const VERSION_NAME = "FAQ";
    private $_FAQData;
    private $_FAQVersion;
    private $_em;

    public function __construct(EntityManager $em)
    {
        $this->_em = $em;
        $this->cacheFAQMetadata();
    }

    public function cacheFAQMetadata()
    {
        // get FAQ
        $faq = $this->_em->getRepository('EvBundle:Qa')
            ->findBy(['enabled' => 1 ]);
        //$faq = $this->_em->getRepository('EvBundle:Qa')
        //    ->createQueryBuilder('c')->getQuery()->getResult(Query::HYDRATE_ARRAY);
        //$this->_FAQData = $faq;
        
        if( ! empty($faq)){
            foreach ($faq as $q) {
                $list[] = [
                    'title' => $q->getTitle(),
                    'content' => $q->getContent(),
                    'update_at' => $q->getUpdatedAt()->format('Y-m-d H:i:s')
                ];
            } 
            $this->_FAQData = $list;
        }
        
        // get data version
        $vRepo = $this->_em->getRepository('EvBundle:EvDataVersion');
        $vResult = $vRepo->findOneBy(['evDataName' => $this::VERSION_NAME]);
        $this->_FAQVersion = $vResult->getVersion();
    }
    
    public function getFAQData()
    {
        return [
            "version" => [
                "data_name" => $this::VERSION_NAME, 
                "version" => $this->_FAQVersion
            ],
            "data" => $this->_FAQData
        ];
    }
}