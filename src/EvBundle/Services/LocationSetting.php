<?php

namespace EvBundle\Services;

use DateTime;
use Doctrine\ORM\EntityManager;
use EvBundle\Entity\EvLocationChargingRateSetting;
use Exception;
use Monolog\Logger;

class LocationSetting
{
    private $_em;
    private $_logger;
    
    public function __construct(EntityManager $em, logger $logger) {
        $this->_em = $em;
        $this->_logger = $logger;
    }

    public function getEvChargingSetting($location)
    {
        $evRateRepo = $this->_em->getRepository('EvBundle:EvLocationChargingManagement');
        $evRate = $evRateRepo->findOneBy(['emsLocationId' => $location ]) ;
        if ( $evRate ) {
            return [
                'ev_shared_normal_contract' => $evRate->getEvNormalContract(),
                'ev_shared_semi_peak_contract' => $evRate->getEvSemiContract(),
                'ev_shared_saturday_semi_peak_contract' => $evRate->getEvSaturdaySemiContract(),
                'ev_shared_off_peak_contract' => $evRate->getEvOffPeakContract(),
                'charging_way' => $evRate->getEvChargingStrategyId()
            ];
        } else {
            return [];
        }
    }

    public function getCurrentEvChargingRate($location, $object=false) 
    {
        $rateQB = $this->_em->getRepository('EvBundle:EvLocationChargingRateSetting')->createQueryBuilder('q');
        $rateQB->where('q.evLocationId = :location')
            ->andWhere('q.effectiveAt <= :now')
            ->andWhere('q.enabled = 1')
            ->setParameter('location', $location)
            ->setParameter('now', new DateTime())
            ->orderBy('q.effectiveAt', 'DESC')
            ->setMaxResults(1);

        $result = $rateQB->getQuery()->getOneOrNullResult();
        if ( $object == true ) {
            return $result;
        }
        $ret = [];
        if ( $result ) {
            $ret['rate_type'] = $result->getEvRateTypeId();
            switch($ret['rate_type']) {
                case 3:
                case 5:
                    $ret['rate_parameter'] = $result->getEvRateParameter();
                break;
                case 4:
                    $ret['rate_parameter'] = json_decode($result->getEvRateParameter(), true);
                break;
            }
            $ret['effective_at'] = $result->getEffectiveAt();
        }
        return $ret;
    }

    public function getScheduledEvChargingRate($location, $object=false)
    {
        $rateQB = $this->_em->getRepository('EvBundle:EvLocationChargingRateSetting')->createQueryBuilder('q');
        $rateQB->where('q.evLocationId = :location')
            ->andWhere('q.effectiveAt > :now')
            ->andWhere('q.enabled = 1')
            ->setParameter('location', $location)
            ->setParameter('now', new DateTime())
            ->orderBy('q.effectiveAt', 'ASC')
            ->setMaxResults(1);

        $result = $rateQB->getQuery()->getOneOrNullResult();
        if ( $object == true ) {
            return $result;
        }
        $ret = [];
        if ( $result ) {
            $ret['rate_type'] = $result->getEvRateTypeId();
            switch($ret['rate_type']) {
                case 3:
                case 5:
                    $ret['rate_parameter'] = $result->getEvRateParameter();
                break;
                case 4:
                    $ret['rate_parameter'] = json_decode($result->getEvRateParameter(), true);
                break;
            }
            $ret['effective_at'] = $result->getEffectiveAt();
        }
        return $ret;
    }

    public function setScheduledEvChargingRate($location, $rateType, $rateParameter, $effectiveDate)
    {
        $scheduledRate = $this->getScheduledEvChargingRate($location->getLocationId(), true);
        if ( !$scheduledRate ) {
            $scheduledRate = new EvLocationChargingRateSetting();
            $scheduledRate->setEvLocationId($location->getLocationId())
                ->setEnabled(1)
                ->setCreatedAt(new DateTime());
        }
        $scheduledRate->setUpdatedAt(new DateTime());
        $scheduledRate->setEvRateTypeId($rateType);
        $scheduledRate->setEffectiveAt($effectiveDate);
        switch($rateType) {
            case 3:
            case 5:
                $scheduledRate->setEvRateParameter($rateParameter);
            break;
            case 4:
                $scheduledRate->setEvRateParameter(json_encode($rateParameter));
            break;
            default:
            // no parameter, pass
            break;
        }
        try {
            $this->_em->persist($scheduledRate);
            $this->_em->flush();
        } catch (Exception $ex) {
            return [false, $ex->getMessage()];
        }
        return [true];
    }
}