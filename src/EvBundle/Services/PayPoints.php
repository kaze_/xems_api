<?php

namespace EvBundle\Services;

use Doctrine\ORM\EntityManager;
use EvBundle\Entity\EvLocationPoints;
use EvBundle\Entity\EvLocationPointsLog;
use AppBundle\Entity\EmsLocationMaster;
use Monolog\Logger as logger;
use Doctrine\DBAL\LockMode;
use Exception;
use PDO;

class PayPoints
{

    private $_em;
    private $_logger;
    private CONST MINIMUM_CHARGING_POINT = 50;
    
    public function __construct(EntityManager $em, logger $logger) {
        $this->_em = $em;
        $this->_logger = $logger;
    }

    /**
     * @param string $userId
     * @param string $location
     * @param int $points
     * @param string $reason
     * 
     * @return int  
     * @throws 
     */
    public function modifyLocationPoints($userId, $locationId, $points, $action, $reason="", $sessionId=null)
    {
        $pointRepo = $this->_em->getRepository('EvBundle:EvLocationPoints');   
        $userPoints = $pointRepo->findOneBy(['userId' => $userId, 'locationId' => $locationId]);
        if ( $userPoints ) {
            $pointId = $userPoints->getId();
        } else {
            $pointId = null;
        }

        $pointLog = new EvLocationPointsLog();
        $pointLog->setUserId($userId);
        $pointLog->setLocationId($locationId);
        $pointLog->setPoints($points);
        $pointLog->setAction($action);
        $pointLog->setComment($reason);
        $pointLog->setSessionId($sessionId);

        try {
            $this->_em->getConnection()->beginTransaction();
            if ( $pointId ) {
                $pointData = $pointRepo->find($pointId, LockMode::PESSIMISTIC_READ);
                $pointData->setPoints($pointData->getPoints()+$points);
            } else {
                $pointData = new EvLocationPoints();
                $pointData->setUserId($userId);
                $pointData->setLocationId($locationId);
                $pointData->setPoints($points);
            }
            $pointLog->setPointsLeft($pointData->getPoints());
            $this->_em->persist($pointData);
            $this->_em->persist($pointLog);
            $this->_em->flush();
            $this->_em->commit();
        } catch (Exception $e) {
            $this->_em->getConnection()->rollBack();
            $this->_logger->info($e->getMessage());
            return ["result" => false, "message" => $e->getMessage()];
        } 
        return ["result" => true, "data" => $pointLog ];
    }

    public function getLocationPointStatusByUser($location, $user, $start=null, $end=null)
    {
        $evPointsLogRepo = $this->_em->getRepository('EvBundle:EvLocationPointsLog');
        $qb = $evPointsLogRepo->createQueryBuilder('q')->select('q.action, sum(q.points) as points')
            ->where('q.locationId = :location')
            ->andWhere('q.userId = :user')
            ->setParameter('location', $location)
            ->setParameter('user', $user);
        if ( $start != null ) {
            $qb->andWhere('q.loggedAt > :start')
            ->setParameter('start', $start);
        }
        $points = 0;
        if ( $end != null ) {
            $qb->andWhere('q.loggedAt < :end')
            ->setParameter('end', $end);

            $qb3 = $evPointsLogRepo->createQueryBuilder('p')
                ->where('p.locationId = :location')
                ->andWhere('p.userId = :user') 
                ->andWhere('p.loggedAt < :end')
                ->setParameter('location', $location)
                ->setParameter('user', $user)
                ->setParameter('end', $end)
                ->orderBy('p.loggedAt', 'DESC')
                ->addOrderBy('p.id', 'DESC')
                ->setMaxResults(1);        
            $pointsThisMonth = $qb3->getQuery()->getOneOrNullResult();
            if ( $pointsThisMonth) {
                $points = $pointsThisMonth->getPointsLeft();
            }
        } else {
            $evPoints = $this->_em->getRepository('EvBundle:EvLocationPoints')
                ->findOneBy(['locationId' => $location, 'userId' => $user]);
            if ( $evPoints ) {
                $points = $evPoints->getPoints();
            }
        }
            
        $evPointsLog = $qb->groupby('q.action')
            ->getQuery()->getResult();

        // points last month
        $qb2 = $evPointsLogRepo->createQueryBuilder('q')
            ->where('q.locationId = :location')
            ->andWhere('q.userId = :user')
            ->andWhere('q.loggedAt < :start')
            ->setParameter('location', $location)
            ->setParameter('user', $user)
            ->setParameter('start', $start)
            ->orderBy('q.loggedAt', 'DESC')
            ->addOrderBy('q.id', 'DESC')
            ->setMaxResults(1);
        $pointsLastMonth = $qb2->getQuery()->getOneOrNullResult();
        if ( $pointsLastMonth ) {
            $pointsLast = $pointsLastMonth->getPointsLeft();
        } else {
            $pointsLast = 0;
        }
            
        $pointsDeposit = 0;
        $pointsUsed = 0;
        if ( $evPointsLog ) {
            foreach($evPointsLog as $log){
                switch($log["action"]) {
                    case 'DEPOSIT':
                    case 'PRECHARGE':
                    case 'REFUND':
                        $pointsDeposit += $log["points"];
                    break;
                    case 'USED':
                    case 'CHARGING':
                        $pointsUsed += $log["points"];
                    break;
                }
            }
        }

        return [
            'points' => $points,
            'pointsUsed' => $pointsUsed,
            'pointsDeposit' => $pointsDeposit,
            'pointsLastMonth' => $pointsLast
        ];
    }
    
    public function getLocationUsersWithPointsLeftAt($location, $before)
    {
        // get total points left now
        $conn = $this->_em->getConnection();
        $rawQuery = "SELECT t1.* FROM ems.ev_location_points_log as t1
	        join ( 
		        select user_id, points_left, max(id) id 
                FROM ems.ev_location_points_log 
                where location_id = :locationId 
                and logged_at < :before
		        group by user_id
	        ) t2
        on t1.user_id = t2.user_id and t1.id = t2.id;" ;
        $result = null;
        try {
            $stmt = $conn->prepare($rawQuery);
            $param = [
                'locationId' => $location->getLocationId(),
                'before' => $before->format('Y-m-d H:i:s')
            ];
            $stmt->execute($param);
            // $result = $stmt->fetchAll(PDO::FETCH_OBJ);
            $result = $stmt->fetchAll();
        } catch (Exception $e ) { 
            // echo($e->getMessage());
        }
        return $result;
    }

    public function getLocationPointsLeftAt($location, $before)
    {
        $pointsLeft = 0;
        $results = $this->getLocationUsersWithPointsLeftAt($location, $before);
        if ( $results != null ) {
            foreach($results as $user) {
                if ( isset($user['points_left'])) {
                    $pointsLeft += $user['points_left'];
                }
            }
        }
        return $pointsLeft;
    }

    public function getLocationPointStatus($location, $start=null, $end=null)
    {
        // get total points used this period
        $evPointsLogRepo = $this->_em->getRepository('EvBundle:EvLocationPointsLog');
        $qb = $evPointsLogRepo->createQueryBuilder('q')->select('q.action, sum(q.points) as points')
            ->where('q.locationId = :location')
            ->setParameter('location', $location);

        if ( $start != null ) {
            $qb->andWhere('q.loggedAt > :start')
            ->setParameter('start', $start);
        }
        if ( $end != null ) {
            $qb->andWhere('q.loggedAt < :end')
            ->setParameter('end', $end);
        }
            
        $evPointsLog = $qb->groupby('q.action')
            ->getQuery()->getResult();

        // $pointsUsed = $evPointsLog['points'];

        // get total points left now
        $pointsLeft = $this->getLocationPointsLeftAt($location, $end);
        $pointsLastMonth = $this->getLocationPointsLeftAt($location, $start);
        // echo('points left: '. $pointsLeft.' points left last month: '. $pointsLastMonth);

        $pointsDeposit = 0;
        $pointsUsed = 0;
        if ( $evPointsLog ) {
            foreach($evPointsLog as $log){
                switch($log["action"]) {
                    case 'DEPOSIT':
                    case 'PRECHARGE':
                    case 'REFUND':
                        $pointsDeposit += $log["points"];
                    break;
                    case 'USED':
                    case 'CHARGING':
                        $pointsUsed += $log["points"];
                    break;
                }
            }
        }

        return [
            'points' => $pointsLeft,
            'pointsUsed' => $pointsUsed,
            'pointsDeposit' => $pointsDeposit,
            'pointsLastMonth' => $pointsLastMonth
        ];
    }

    /**
     * @param string $locationId
     * @param string $userId
     * 
     * @return 
     */
    public function checkLocationPoints($locationId, $userId)
    {
        $pointRepo = $this->_em->getRepository('EvBundle:EvLocationPoints');   
        $userPoints = $pointRepo->findOneBy(['userId' => $userId, 'locationId' => $locationId]);
        if ( $userPoints ) {
            $pointId = $userPoints->getId();
        } else {
            $pointId = null;
        } 
        if ( $userPoints->getPoints() < $this::MINIMUM_CHARGING_POINT ) {

        }
    }

    /**
     * @param string $locationId
     * @param string $userId
     * 
     * @return 
     */
    public function getLocationPoints($locationId, $userId)
    {
        $pointRepo = $this->_em->getRepository('EvBundle:EvLocationPoints');   
        $userPoints = $pointRepo->findOneBy(['userId' => $userId, 'locationId' => $locationId]);
        if ( !$userPoints ) {
            return null;
        }
        return $userPoints->getPoints();
    }

    // get logs
    public function getPointsLogBySession($sessionId) {
        $pointsLogRepo = $this->_em->getRepository('EvBundle:EvLocationPointsLog');   
        $log = $pointsLogRepo->findOneBy(['sessionId' => $sessionId]); 
        return $log;
    }

    public function getUserPointStatus($userId)
    {
        $pointsLogRepo = $this->_em->getRepository('EvBundle:EvLocationPoints');
        $points = $pointsLogRepo->findBy(['userId' => $userId]);
        return $points;
    }

    public function getLocationUserPointsLogs($locationId, $userId, $start, $end)
    {
        $qb = $this->_em->getRepository('EvBundle:EvLocationPointsLog')
            ->createQueryBuilder('q')
            ->where('q.locationId = :locationId')
            ->andWhere('q.userId = :userId')
            ->andWhere('q.loggedAt between :start and :end')
            ->setParameter('locationId', $locationId)
            ->setParameter('userId', $userId)
            ->setParameter('start', $start)
            ->setParameter('end', $end)
            ->orderBy('q.loggedAt', 'DESC');
        $logs = $qb->getQuery()->getResult();
        return $logs;
    }

    public function getUserPointsLogs($userId, $start=null, $end=null, $max=0, $action=null)
    {
        $qb = $this->_em->getRepository('EvBundle:EvLocationPointsLog')
            ->createQueryBuilder('q')
            ->where('q.userId = :userId')
            ->andWhere('q.points != 0')
            ->setParameter('userId', $userId)
            ->orderBy('q.loggedAt', 'DESC');

            if ( $max != 0 ) {
                $qb->setMaxResults($max);
            }
    
            if ( $start != null ) {
                $qb->andWhere('q.loggedAt > :start')
                    ->setParameter('start', $start);
            }
            if ( $end != null ) {
                $qb->andWhere('q.loggedAt < :end') 
                    ->setParameter('end', $end);
            }

            if ( $action != null ) {
                $qb->andWhere('q.action = :action')
                    ->setParameter('action', $action);
            }
        $logs = $qb->getQuery()->getResult();
        return $logs;
    }

    // statistics
    // need to put it into daily/monthly (and hourly?) routine
    public function getUserPointsMonthly($user, $start=null, $end=null, $max=0, $action=null)
    {
        $qb = $this->_em->getRepository('EvBundle:EvLocationPointsLog')
            ->createQueryBuilder('q')
            ->select( 'CONCAT(DATE_FORMAT(q.loggedAt, \'%Y-%m-\'), \'01 00:00:00\') as date, sum(q.points) as points')
            ->where('q.userId = :user')
            ->setParameter('user', $user)
            ->orderBy('date', 'DESC')
            ->groupBy('date');

        if ( $max != 0 ) {
            $qb->setMaxResults($max);
        }

        if ( $start != null ) {
            $qb->andWhere('q.loggedAt > :start')
                ->setParameter('start', $start);
        }
        if ( $end != null ) {
            $qb->andWhere('q.loggedAt < :end') 
                ->setParameter('end', $end);
        }

        if ( $action != null ) {
            $qb->andWhere('q.action = :action')
                ->setParameter('action', $action);
        }
        $res = $qb->getQuery()->getResult();
        return $res;
    }
}