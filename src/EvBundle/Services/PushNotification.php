<?php

namespace EvBundle\Services;


use AppBundle\Entity\LoginToken;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Exception;
use Predis\Client;
// use RMS\PushNotificationsBundle\Message\AndroidMessage;
use RMS\PushNotificationsBundle\Message\iOSMessage;


class PushNotification
{

    private $_em;
    private $_rms;
    private $_fcm;
    private $_logger;
    private $_error;

    public function __construct(EntityManager $em,  $rmsPushnotifications, $logger) {
        $this->_em = $em;
        $this->_rms = $rmsPushnotifications;
        $this->_logger = $logger;
    }
    
    public function add(User $user, $msg, $data = [], $inBackground = false)
    {
        $pn = new \EvBundle\Entity\PushNotification();
        $pn->setUserId($user->getId()->toString());
//        $pn->setUser($user);
        $pn->setContent($msg);
        $pn->setInBackground($inBackground);
        $pn->setData(json_encode($data));

        try {
            $this->_em->persist($pn);
            $this->_em->flush();
        } catch (Exception $ex) {
            $this->_error = 'create push notification task failed.';
            return false;
        }
        
        $redis = new Client();

        //check service on
        $symfonyCmd = 'bin/console push:notification';
        exec('ps aux | grep -i \''.$symfonyCmd.'\' ', $ps);
        //sync mode
        if(count($ps) < 3){
            try {
                $pn->setSyncMode(true);
                $this->_em->persist($pn);
                $this->_em->flush();
            } catch (Exception $ex) {
                $this->_error = 'update push notification sync_mode failed.';
            }
            return $this->push($pn->getId());
        }
        //async mode
        return $redis->publish('push_notification', $pn->getId());
    }
    
    public function FCMPush($title,$content,$token) {
        //hotelB2B
        define( 'API_ACCESS_KEY', 'AAAAc-RLEAE:APA91bHvTxw6INDzNAZDZXoGlYgE4IR5TVTensDNhmv6MWIEycxIqlCUussxB7wFzjAv2wKjWYRm6MadEPQ4-ULZkjO-Ay73IP9dtlQvHMmpJ3CQwhF-LCth1buW7RWAxiCxEU8DAm8a' );
        
        
        $msg = array(
            'body'  =>  $content,
            'title' =>  $title,
            'icon'  => 'myicon',/*Default Icon*/
            'sound' => 'mySound'/*Default sound*/
        );
        
        $fields = array(
            'to' => $token,
            'data' => $msg);
        $data = json_encode($fields);
        
        $headers = array(
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json',
            'Content-Length:' . strlen($data)
        );
        
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, $data );
        $result = curl_exec($ch );
        $this->_error = $result;
        $this->_logger->error($result);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close( $ch );
        return $httpcode;
    }
    
    public function push($id)
    {
        try {
            $pn = $this->_em->find('EvBundle:PushNotification', $id);
            
            $user = $this->_em->find('AppBundle:User', $pn->getUserId());
            $tokenRepo = $this->_em->getRepository('AppBundle:LoginToken');
            $token = $tokenRepo->findOneBy(array('user' => $user));
            
            switch ($token->getOS()){
                case LoginToken::OS_ANDROID:
                    $len = 64;
                    break;
                case LoginToken::OS_IOS:
                default:
                    $len = 64;
                    break;
            }
            if(strlen($token->getDeviceToken()) < $len ){
                $this->_error = 'token length is wrong. (len='.strlen($token->getDeviceToken());
                $this->_em->clear();
                return false;
            }
        } catch (Exception $ex) {
            $this->_error = 'Get token failed.'.$ex->getMessage();
            $this->_em->clear();
            return false;
        } 
       
        try {
            $this->_logger->info($token->getDeviceToken());
            switch ($token->getOS()){
                case LoginToken::OS_IOS:
                    $message = new iOSMessage();
                    if( ! $pn->getInBackground()) $message->setMessage($pn->getContent());
                    $message->setAPSContentAvailable('1');
                    $message->setAPSBadge('1');
                    if( ! empty($pn->getData())){
                        $message->setData(json_decode($pn->getData(), true));
                    }
                    $message->setDeviceIdentifier($token->getDeviceToken());
                    $re = $this->_rms->send($message);
                    $pn->setStatus($re ? \EvBundle\Entity\PushNotification::SEND_SUCCESS : \EvBundle\Entity\PushNotification::SEND_FAILED);
                    break;
                case LoginToken::OS_ANDROID:
                    $result = $this->FCMPush($pn->getContent(),$pn->getData(),$token->getDeviceToken());
                    $pn->setStatus( ($result == 200 ) ? \EvBundle\Entity\PushNotification::SEND_SUCCESS : \EvBundle\Entity\PushNotification::SEND_FAILED);
                    // $pn->setStatus($result);
//                     $message = new AndroidMessage();
//                     $message->setMessage($pn->getContent());
//                     $message->setGCM(true);
//                     $message->setDeviceIdentifier($token->getDeviceToken());
                    break;
            }
    
            // $re = $this->_rms->send($message);
            
            // $pn->setStatus($re ? \EvBundle\Entity\PushNotification::SEND_SUCCESS : \EvBundle\Entity\PushNotification::SEND_FAILED);

            try {
                $this->_em->persist($pn);
                $this->_em->flush();
            } catch (Exception $ex) {
                $this->_error = 'update push notification status failed.';
                $this->_em->clear();
                return false;
            }
            $this->_em->clear();
            return true;
        } catch (Exception $ex) {
            $pn->setStatus(\EvBundle\Entity\PushNotification::SEND_FAILED);

            try {
                $this->_em->persist($pn);
                $this->_em->flush();
            } catch (Exception $ex) {
                $this->_error = 'update push notification status failed.';
                $this->_em->clear();
                return false;
            }
            $this->_error = 'send failed.'.$ex->getMessage();
            $this->_em->clear();
            return false;
        }
    }
    
    public function error()
    {
        return $this->_error;
    }
}
