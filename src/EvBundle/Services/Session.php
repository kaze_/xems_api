<?php
/**
 * Created by PhpStorm.
 * User: chiehyingpan
 * Date: 2018/4/8
 * Time: 上午1:48
 */

namespace EvBundle\Services;


use AppBundle\Entity\EmsCircuitMaster;
use AppBundle\Entity\EmsPowerReportLog;
use AppBundle\Entity\EmsSmartRelayTransactionLog;
use Doctrine\ORM\EntityManager;
use EvBundle\Entity\EvChargingSessionLog;
use EvBundle\Entity\EvUserStationMapping;
use Monolog\Logger;

class Session
{
    private $_em;
    private $push;
    private $powerReport;
    private $fee;
    private $calculator;
    private $logger;
    private $chargingControll;

    public function __construct(EntityManager $em, $push, $fee, $calculator, $logger, $chargingControll)
    {
        $this->_em = $em;
        $this->push = $push;
        //$this->powerReport = $powerReport;
        $this->fee = $fee;
        $this->calculator = $calculator;
        $this->logger = $logger;
        $this->chargingControll = $chargingControll;
    }

    // Todo : move to repository class
    public function getLastOnUserId($circuit)
    {

        if ( $circuit instanceof EmsCircuitMaster ) {
            $circuit_id = $circuit->getCircuitId();
        } else {
            // assume string
            $circuit_id = $circuit;
        }

        $conn = $this->_em->getConnection();
        $stmt = $conn->prepare('
            SELECT id as ID, source_id as SOURCE_ID, ev_plate as EV_PLATE, transaction_time, session_status,status_id,comment
            FROM ems_smart_relay_transaction_log as r
            WHERE circuit_id = :circuit_id
            AND r.source = :source 
            AND r.status_id = :status_id
            AND r.transaction_time + interval 5 minute > now()
            AND r.session_id is null
            ORDER BY r.transaction_time
            DESC
            LIMIT 1
        ');

        $stmt->bindValue(':circuit_id',$circuit_id, \PDO::PARAM_STR);
        $stmt->bindValue(':source', EmsSmartRelayTransactionLog::SOURCE_USER, \PDO::PARAM_STR);
        $stmt->bindValue(':status_id', EmsSmartRelayTransactionLog::ACT_METERON, \PDO::PARAM_INT);

        $stmt->execute();
        $result = $stmt->fetchAll( \PDO::FETCH_ASSOC );

        if ( isset($result[0]['SOURCE_ID']) ) {
            //$this->logger->info("getLastOnUserId id ", [ $result[0]['SOURCE_ID'] ] );
            // { "ID" => , "SOURCE_ID" => 
            return $result[0];
            // return $result[0]['SOURCE_ID'];
        }
        //$this->logger->info("getLastOnUserId result ", [$result]);
        return null;
    }


    public function getPreviousInsertSessionOid($station)
    {
        $conn = $this->_em->getConnection();
        $stmt = $conn->prepare('
            SELECT id as OID FROM ev_charging_session_log
            WHERE charging_station_id = :station_id
            ORDER BY id
            DESC
            LIMIT 1
        ');

        $stmt->bindValue(':station_id', $station->getChargingStationId(), \PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetchAll( \PDO::FETCH_ASSOC );
        // $this->logger->info( __FILE__ . " getPreviousInsertSessionOid", [ $result ] ) ;
        if ( $result && isset( $result[0]['OID'] )) {
            return (int) $result[0]['OID'];
        }
        return null;

    }

    public function markRelayTransactionAsUsed($relaylog_oid, $session_oid)
    {
        // $relayRepo = $this->em->getRepository('AppBundle:EmsSmartRelayTransactionLog');

        $this->logger->info("markRelayTransactionAsUsed", [ "relaylog" => $relaylog_oid, "session" => $session_oid ] );
        $conn = $this->_em->getConnection();
        $stmt = $conn->prepare('
            UPDATE ems_smart_relay_transaction_log SET session_id = :session_oid
            WHERE id = :id
        ');

        $stmt->bindValue(':session_oid', $session_oid, \PDO::PARAM_INT);
        $stmt->bindValue(':id', $relaylog_oid, \PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll( \PDO::FETCH_ASSOC );
        return ;
    }


    //
    // json_args format : {"kwh":188367.2}
    //                    {"kwh":501.46,"event_time":"2020-04-13 13:28:38"}
    //
    public function startSession($circuit, $station, $json_args=null)
    {

        $newSession = new EvChargingSessionLog();
        $newSession->setCircuit($circuit)
            ->setChargingStation($station)
            ->setStartAt(new \DateTime());
        // get owner id
        $mappingRepo = $this->_em->getRepository('EvBundle:EvUserStationMapping');
        $ownerMapping = $mappingRepo->findOneBy(array('chargingStation' => $station, 'owner' => 1));
        if ($ownerMapping) {
            $newSession->setOwner($ownerMapping->getUser());
        }

        // get user id
        $result = $this->getLastOnUserId( $circuit );
        $relaylog_oid = null;
        $guest_id =null;
        if ( $result ) {
            $userId = $result['SOURCE_ID'];
            $relaylog_oid = (int) $result['ID'];
            $this->logger->info("startSession getLastOnUserId ", [ $result ] );
            $userOb = $this->_em->find('AppBundle:User', $userId);
            $newSession->setUserId($userOb);
            if ( isset($result['EV_PLATE']) )
                $newSession->setEvPlate( $result['EV_PLATE'] );
            $comment = json_decode($result['comment'], true);
            if ( $comment && isset($comment['guestId']) ) {
                $guest_id = $comment['guestId'];
            }
        }
        $kwh = 0;
        if ( $json_args != null ) {
            if ( isset( $json_args['event_time'] ) ) {
                $now = new \DateTime($json_args['event_time']);
                $newSession->setStartAt( $now);
            }
            if ( isset( $json_args['kwh'] ) ) {
                $kwh = $json_args['kwh'];
                $newSession->setStartKwh($kwh);

                // if kwh from log data, mark the line
                // if kwh from client, unmark the line
                $kwh = 0;
            }
        } 

        if ( $kwh == 0 ) {
            // get start kwh
            $logReo = $this->_em->getRepository('AppBundle:EmsPowerReportLog');
            $startDate = (new \DateTime())->modify('-10 minutes');
            $log = $logReo->findDataByCircuit(['kwhTotal'],
                ['endDate' => new \DateTime(), 'startDate' => $startDate, 'circuit' => $circuit],
                ['reportDatetime' => 'DESC'], 1);
            if ( $log && count($log) != 0 ) {
                $newSession->setStartKwh($log[0]['kwhTotal']);
            } else {
                //$this->logger->error("startSession: cannot find start kwh", [ $circuit->getCircuitId(); ]  );
                $newSession->setStartKwh(0);
            }
        }


        try {
            // no session id now
            $this->_em->persist($newSession);
            $this->_em->flush();
            $this->_em->refresh($newSession);
            // fail
            //$oooo = $this->_em->getConnection()->lastInsertId();
            //$this->logger->info("get last Insert Id : ", [ "lastInsertId" => $oooo ] );

            // fail again
            // $repo = $this->_em->getRepository('EvBundle:EvChargingSessionLog');
            // $saved_session = $repo->findNewestSessionByStation( $station );
            // $this->logger->info("get last session oid",[ $saved_session->getSessionId(),"oid"=>$saved_session->getOId()]);

            // $oid = $this->getPreviousInsertSessionOid( $station );
            $oid = $newSession->getOId();
            $this->logger->info( __FILE__ . " getPreviousInsertSessionOid", [ $oid ] ) ;
            if ( $relaylog_oid != null && $oid != null ) 
                $this->markRelayTransactionAsUsed($relaylog_oid, $oid );

        } catch(\Exception $e) {
            $this->logger->info( __FILE__ . "::startSession ", [$e->getMessage()] );
        }

        // mark guest session
        if ( $guest_id != null ) {
            $ret = $this->chargingControll->setSessionToLocationGuest($newSession, $guest_id);
        }
    }

    /* return calculated session object, but don't really collect charging fee and end session */
    public function endSessionDryRun($circuit, $station, $json_args=null)
    {
        $this->logger->info("endSession Dry-Run enter");

        $repo = $this->_em->getRepository('EvBundle:EvChargingSessionLog');
        $sess = $repo->findOneBy(array('chargingStation' => $station), array('startAt' => 'DESC'));
        // don't update this object to database.
        $this->_em->detach($sess);

        if ( $sess && $sess->getEndAt() == null ) {
            if ( isset( $json_args['event_time'] ) ) {
                $now = new \Datetime( $json_args['event_time']);
                $sess->setEndAt($now);
            } else {
                $now = new \Datetime();
                $sess->setEndAt($now);
            }
            /**
             * Note, log demand usage
             *         "fee": {
             *              "peak": {
             *              "amount": 31.536765000000432,
             *              "kwh": 9.47050000000013,
             *              "usage": 10.843452736767
             *              }
             *          },
             */
            $fee = $this->fee->deviceFee($sess->getStartAt(), $sess->getEndAt(), $circuit);
            $sess->setBasicCost(0);
            if ( $fee != null ) {
                if ( $sess->getStartKwh() == null || $sess->getStartKwh() == 0 ) {
                    if ( isset($fee['startKwh'])) {
                        $sess->setStartKwh($fee['startKwh']);
                    }
                }
                if ( isset($fee['amount']) ) $sess->setAmountCost($fee['amount']);
                if ( isset($fee['totalKwh'])) $sess->setTotalKwh($fee['totalKwh']);
                if ( isset($fee['endKwh'])) $sess->setEndKwh($fee['endKwh']);
                if ( isset($fee['fee'])) {
                    foreach($fee['fee'] as $tou => $value) {
                        if ($tou == 'normal') {
                            $touPhrase = 'Peak';
                        } else {
                            $touPhrase = str_replace('_', '', ucwords($tou, '_'));
                        }
                        $demandFunctionName = "setMax".$touPhrase."Demand";
                        $kwhFunctionName = "set".$touPhrase."Kwh";
                        if ( isset($value['usage'])) {
                            $sess->{$demandFunctionName}($value['usage']);
                        } 
                        if ( isset($value['kwh'])) {
                            $sess->{$kwhFunctionName}($value['kwh']);
                        }
                    }
                }
            } else {
                $sess->setEndKwh($sess->getStartKwh());
                $sess->setTotalKwh(0);
                $sess->setAmountCost(0);
            }
            
            // get carbon reduction
            $evPlate = $sess->getEvPlate();
            $totalKwh = $sess->getTotalKwh();
            $modelId = null;
            if ($evPlate != null && $evPlate != "") {
                $evRepo = $this->_em->getRepository('EvBundle:EvUserOwnVehicle');
                $vehicle = $evRepo->findOneBy(['evPlate' => $evPlate]);
                if ( $vehicle ) {
                    $modelId = $vehicle->getEvModelId();
                } 
            }
            $sess->setCarbonReduction($this->calculator->getCarbonReduction($totalKwh, $modelId));
        }
        return $sess;
    }

    // json_args format : {"kwh":188367.2}
    //                    {"kwh":501.46,"event_time":"2020-04-13 13:28:38"}
    public function endSession($circuit, $station, $json_args=null)
    {
        $this->logger->info("endSession enter");

        $repo = $this->_em->getRepository('EvBundle:EvChargingSessionLog');
        $sess = $repo->findOneBy(array('chargingStation' => $station), array('startAt' => 'DESC'));

        if ( $sess && $sess->getEndAt() == null ) {

            if ( isset( $json_args['event_time'] ) ) {
                $now = new \Datetime( $json_args['event_time']);
                $sess->setEndAt($now);
            } else {
                $now = new \Datetime();
                $sess->setEndAt($now);
            }
            // endKwh
            
            // $fee = $this->powerReport->getCircuitFee($circuit, $sess->getStartAt(), $sess->getEndAt());
            /**
             * Note, log demand usage
             *         "fee": {
             *              "peak": {
             *              "amount": 31.536765000000432,
             *              "kwh": 9.47050000000013,
             *              "usage": 10.843452736767
             *              }
             *          },
             */
            $fee = $this->fee->deviceFee($sess->getStartAt(), $sess->getEndAt(), $circuit);
            $sess->setBasicCost(0);
            if ( $fee != null ) {
                if ( $sess->getStartKwh() == null || $sess->getStartKwh() == 0 ) {
                    if ( isset($fee['startKwh'])) {
                        $sess->setStartKwh($fee['startKwh']);
                    }
                }
                if ( isset($fee['amount']) ) $sess->setAmountCost($fee['amount']);
                if ( isset($fee['totalKwh'])) $sess->setTotalKwh($fee['totalKwh']);
                if ( isset($fee['endKwh'])) $sess->setEndKwh($fee['endKwh']);
                if ( isset($fee['fee'])) {
                    foreach($fee['fee'] as $tou => $value) {
                        if ($tou == 'normal') {
                            $touPhrase = 'Peak';
                        } else {
                            $touPhrase = str_replace('_', '', ucwords($tou, '_'));
                        }
                        $demandFunctionName = "setMax".$touPhrase."Demand";
                        $kwhFunctionName = "set".$touPhrase."Kwh";
                        if ( isset($value['usage'])) {
                            $sess->{$demandFunctionName}($value['usage']);
                        } 
                        if ( isset($value['kwh'])) {
                            $sess->{$kwhFunctionName}($value['kwh']);
                        }
                    }
                }
            } else {
                $sess->setEndKwh($sess->getStartKwh());
                $sess->setTotalKwh(0);
                $sess->setAmountCost(0);
            }
            $ret = $this->chargingControll->collectChargingFee($sess);
            if ($ret) $this->logger->INFO(json_encode($ret));
            
                // get carbon reduction
                $evPlate = $sess->getEvPlate();
                $totalKwh = $sess->getTotalKwh();
                $modelId = null;
                if ($evPlate != null && $evPlate != "") {
                    $evRepo = $this->_em->getRepository('EvBundle:EvUserOwnVehicle');
                    $vehicle = $evRepo->findOneBy(['evPlate' => $evPlate]);
                    if ( $vehicle ) {
                        $modelId = $vehicle->getEvModelId();
                    } 
                }
                $sess->setCarbonReduction($this->calculator->getCarbonReduction($totalKwh, $modelId));
            try {
                $this->_em->persist($sess);
                $this->_em->flush();
            } catch(\Exception $e) {
                $this->logger->info($e->getMessage());
            }
        }
    }

    /**
     * get current session by station
     * param $station charging station(object)
     * param $active get active session only or not
     * 
     * return $session(object)
     */
    public function getLatestSessionByStation($station, $active=false)
    {
        // find newest session
        $repo = $this->_em->getRepository('EvBundle:EvChargingSessionLog');
        $session = $repo->findOneBy(array('chargingStation' => $station), array('startAt' => 'DESC'));
        if ( $session == null ) {
            return null;
        }
        if ( $active == true  && $session->getEndAt() != null ) {
            return null;
        }
        return $session;
    }


    public function getActiveSessionByUser($user)
    {
        $em = $this->_em;

        $repo = $em->getRepository('EvBundle:EvChargingSessionLog');
        $res = $repo->findBy([
            'userId' => $user->getId(),
            'endAt' => null
        ]);
        return $res; 
    }

    private function notify($station, $status, $msg, $function)
    {
        $repo = $this->_em->getRepository('EvBundle:EvUserStationMapping');
        $mappings = $repo->findBy(array('chargingStation' => $station));
        $data = array(
            'time' => (new \DateTime())->format('Y-m-d H:i:s'),
            'chargingStation' => array(
                'id' => $station->getChargingStationId(),
                'status' => $status,
            ),
            'title' => $msg,
            'msg' => $msg
        );
        // 開始收集需要 push notification 的 user
        // 有沒有人對應到這個 charging station
        $pushUsers = [];
        foreach($mappings as $mapping) {
            $user = $mapping->getUser();
            $send = $mapping->$function();
            if ( $user ) {
                $userId = $user->getId()->toString();
                $pushUsers[$userId] = [$user, $send];
            }
        }
        // 有沒有人正在用這個 charging station？找到該 station 正在進行的 session (最新的，並且沒有 ended_at 的 session)，取它的 user
        // 不用 active 是因為如果通知 session 結束的話，該 session 的 ended_at 應該已經有值... (該怎麼處理比較好？)
        $session = $this->getLatestSessionByStation($station) ;
        if ( $session ) {
            $user = $session->getUserId();
            if ( $user ) {
                $userId = $user->getId()->toString();
                // 已經放過的就不要推第二次了
                if ( !isset($pushUsers[$userId])) {
                    $pushUsers[$userId] = [$user, true];
                }
            }
        }

        $users = array_keys($pushUsers);
        $this->logger->info(json_encode($users));
        foreach($users as $user) {
            $push = $pushUsers[$user];
            $show = $push[1];
            $userObj = $push[0];
            if ( $show == false ) {
                // dont' show - inBackgroud = true
                $re = $this->push->add($userObj, $msg, $data, true);
            } else {
                $re = $this->push->add($userObj, $msg, $data);
            }
        }
        return ;
    }

    public function gatewayEvent(EmsCircuitMaster $circuit, $status, $json_args)
    {
        $stations = $circuit->getChargingStation();
        if ( !$stations || empty($stations)) {
            return null;
        }
        $station = $stations[0];

        $msg = null;
        switch($status) {
            case EmsSmartRelayTransactionLog::ACT_STARTED:
                $this->startSession($circuit, $station, $json_args);
                $msg = "開始充電";
                $this->notify($station, $status, $msg, "getIsNotiChargingStart");
                break;
            case EmsSmartRelayTransactionLog::ACT_ENDING:
                $msg = "即將結束充電";
                $this->notify($station, $status, $msg, "getIsNotiChargingAlmost");
                break;
            case EmsSmartRelayTransactionLog::ACT_ENDED:
                $this->endSession($circuit, $station, $json_args);
                $msg = "結束充電";
                $this->notify($station, $status, $msg, "getIsNotiChargingFinish");
                break;
            case EmsSmartRelayTransactionLog::ACT_UNPLUGGED:
                $this->endSession($circuit, $station, $json_args);
                $msg = "充電槍已拔除";
                $this->notify($station, $status, $msg, "getIsNotiChargingRemoval");
                break;
            case EmsSmartRelayTransactionLog::ACT_METEROFF:
                $this->endSession($circuit, $station);
                $msg = "充電樁已關閉";
                $this->notify($station, $status, $msg, "getIsNotiStationAutoOff");
                break;
            case EmsSmartRelayTransactionLog::ACT_METERON:
                $msg = "充電樁已開啟";
                $this->notify($station, $status, $msg, "getIsNotiStationAutoOn");
                break;
            default:
                return ;
        }
        return ;
    }

    public function getSessionData($in_session)
    {
        if ( is_string($in_session )) {
            $session = $this->_em->getRepository('EvBundle:EvChargingSessionLog')->find($in_session);
            if ( !$session ) {
                return null;
            }
        } else {
            $session = $in_session;
        }

        $startAt = $session->getStartAt();
        $circuit = $session->getCircuit()->getCircuitId();

        $data = array(
            'sessionId' => $session->getSessionId(),
            'startAt' => $startAt->format('Y-m-d H:i:s'),
        );

        $payUser = $session->getOwner();
        // ?????????
        $chargingUser = $session->getUserId();

        if ( $payUser ) {
            $data['payUser'] = $payUser->getUsername();
        } else {
            $data['payUser'] = '';
        }
        if ( $chargingUser ) {
            $data['chargingUser'] = $chargingUser->getUsername();
        } else {
            $data['chargingUser'] = '';
        }

        $chargingStation = $session->getChargingStation();
        $data['chargingStationName'] = $chargingStation? $chargingStation->getName() : '';
        $data['chargingStationId'] = $chargingStation? $chargingStation->getChargingStationId() : '';
        $endAt = $session->getEndAt();
        $evPlate = $session->getEvPlate();
        $data['evPlate'] = $evPlate ? $evPlate : "";
        if ( $endAt == null  ) {
            $data['ended'] = false;
            $endAt = new \DateTime();

            // get charging fee of taipower
            // 1. cost (for web) - basicCost, energy cost
            // 2. income (for both web and app) amountCost(totalCost), peakCost, semiPeakCost, saturdaySemiPeakCost, offPeakCost
            $fee = $this->fee->deviceFee($startAt, $endAt, $circuit);
            $data['endAt'] = $endAt->format('Y-m-d H:i:s');

            if ( $fee ) {
                // 1. cost (for web) - basicCost, energy cost   
                $data['basicCost'] = 0;
                $data['energyCost'] = $fee['amount'];
                $data['feeKwh'] = $fee['totalKwh'];
                $data['totalKwh'] = ceil($fee['totalKwh']);
                // 2. get income cost from points
                $guest = false;
                if ( $chargingUser && $chargingStation) {
                    $locationRepo = $this->_em->getRepository('AppBundle:EmsLocationMaster');
                    $location = $locationRepo->findByChargingStation($chargingStation->getChargingStationId());
                    if( $location != null ) {
                        $guest = $this->chargingControll->checkLocationGuest($chargingUser->getId(), $location->getLocationId());
                    } 
                    $amountCost = $this->chargingControll->calculateChargingFeeByFeeData($fee, $chargingStation, $guest);
                }
                $data['amountCost'] = $amountCost;
            } else {
                $data['feeKwh'] = 0;
                $data['totalKwh'] = 0;
                $data['energyCost'] = 0;
                $data['amountCost'] = 0;
            }

            // get distance and carbon reduction
            $evPlate = $session->getEvPlate();
            // $evCalculator = $this->get('ev.calculator');
            $modelId = null;
            if ($evPlate != null && $evPlate != "") {
                $evRepo = $this->_em->getRepository('EvBundle:EvUserOwnVehicle');
                $vehicle = $evRepo->findOneBy(['evPlate' => $evPlate]);
                if ( $vehicle ) {
                    $modelId = $vehicle->getEvModelId();
                } 
            }
            $data['distance'] = $this->calculator->getDistance($data['totalKwh'], $modelId);
            $data['carbonReduction'] = $this->calculator->getCarbonReduction($data['totalKwh'], $modelId);

            // data for active session page
            // search for shutdown message
            
            $relay = $this->_em->getRepository('AppBundle:EmsSmartRelayTransactionLog');
            $relayStatus = $relay->createQueryBuilder('c')
            ->where('c.circuit = :circuit')
            ->andWhere('c.transactionTime > :start')
            ->andWhere('c.source = 2')
            ->andWhere('c.statusId = 5') 
            ->orderBy('c.transactionTime', 'DESC')
            ->setMaxResults(1)
            ->setParameter('circuit', $circuit)
            ->setParameter('start', $startAt)
            ->getQuery()->getOneOrNullResult();
            if ( $relayStatus ) {
                $data['status'] = 3;
            } else {
                $data['status'] = 2;
            }

            $data['switch'] = [
                EvUserStationMapping::SW_ST_AUTO_ONOFF => false,
                EvUserStationMapping::SW_CH_OFF_PEAK => false
            ];

            $data['reason'] = "尚未結束";

        } else {
            $data['ended'] = true;
            $data['totalKwh'] = $session->getEndKwh() - $session->getStartKwh();
            $data['feeKwh'] = $data['totalKwh'];
            $data['endAt'] = $endAt->format('Y-m-d H:i:s');
            $data['basicCost'] = $session->getBasicCost();
            $data['energyCost'] = $session->getAmountCost();
            $data['carbonReduction'] = $session->getCarbonReduction();
            $data['status'] = 1;
            $data['reason'] = "正常結束";
            // find charged fee
            $pointLog = $this->_em->getRepository('EvBundle:EvLocationPointsLog')->findOneBy(['sessionId' => $data['sessionId']]);
            if ( $pointLog ) {
                $data['amountCost'] = abs($pointLog->getPoints());
                $strDetail = $pointLog->getComment();
                $detail = json_decode($strDetail);
                if ( $detail ) {
                    $costDetail = [];
                    forEach($detail as $tou => $value) {
                        // snake case to camel case first
                        if ( $value == 0 ) {
                            continue;
                        }
                        switch ($tou) {
                            case 'guest_peak_fee':
                            case 'peak_fee':
                                $costDetail['peakCost'] = $value;
                            break;
                            case 'guest_semi_peak_fee':
                            case 'semi_peak_fee':
                                $costDetail['semiPeakCost'] = $value;
                            break;
                            case 'guest_saturday_semi_peak_fee':
                            case 'saturday_semi_peak_fee':
                                $costDetail['saturdaySemiPeakCost'] = $value;
                            break;
                            case 'guest_off_peak_fee':
                            case 'off_peak_fee':
                                $costDetail['offPeakCost'] = $value;
                            break;
                            case 'total_kwh':
                            case 'totalKwh':
                                $data['totalKwh'] = $value;
                            break;
                        }
                    }
                    if ( count($costDetail) != 0) {
                        $data['costDetail'] = $costDetail;
                    }
                }
            }
        }
        return $data;
    }
}
