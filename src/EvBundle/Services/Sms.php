<?php

namespace EvBundle\Services;


use Doctrine\ORM\EntityManager;
use Exception;
use Predis\Client;
use function var_dump;
use EvBundle\Entity\Sms as SmsEntity;

class Sms
{
    private $_em;
    private $_smsUser;
    private $_smsPassword;
    private $_env;
    private $_pincode;
    private $_error;

    public function __construct($smsUser, $smsPassword, EntityManager $em, $env) {
        $this->_em = $em;
        $this->_smsUser = $smsUser;
        $this->_smsPassword = $smsPassword;
        $this->_env = $env;
        $this->_pincode = random_int(100000, 999999);
    }
    
    public function getPincode()
    {
        return $this->_pincode;
    }
    
    public function add($phone, $content, $periodSec = 28, $pincode = '')
    {
        $sms = new \EvBundle\Entity\Sms();

        $sms->setPhone($phone);
        $sms->setPincode($pincode);
        $sms->setContent($content);
        $sms->setSendStatus(SmsEntity::SEND_READY);

        try {
            $this->_em->persist($sms);
            $this->_em->flush();
        } catch (Exception $ex) {
            $this->_error = 'add sms failed.';
            return false;
        }
        
        $redis = new Client();

        //check service on
        $symfonyCmd = 'bin/console sms:send';
        exec('ps aux | grep -i \''.$symfonyCmd.'\' ', $ps);
        //sync mode
        if(count($ps) < 3){
            return $this->send($sms->getId());
        }
        //async mode
        return $redis->publish('sms_send', $sms->getId());
    }
    
    public function addVerify($phone, $periodSec = 28)
    {
        $query = $this->_em->createQueryBuilder();

        //Check last send Datetime (createdAt)
        $query->select( 'count(s.id) c' )
            ->from( 'EvBundle:Sms',  's' )
            ->where('s.pincode > 0')
            ->where('s.phone = '. $phone)
            ->andWhere('s.createdAt > :now')
            ->setParameter('now', new \DateTime('-'.$periodSec.' seconds'), \Doctrine\DBAL\Types\Type::DATETIME)
        ;
        $count = $query->getQuery()->getSingleScalarResult();
        if($count > 0){
            $this->_error = 'Resend in '.$periodSec.' seconds, try later.';
            return false;
        }

        //expired other
        $query = $this->_em->createQueryBuilder();
        $q = $query->update('EvBundle:Sms', 's')
            ->where('s.phone = '. $phone)
            ->where('s.pincode > 0')
            ->where('s.valiStatus = '.SmsEntity::VALI_NONE)
            ->set('s.valiStatus', '?2')
            ->set('s.updatedAt', '?3')
            ->setParameter(2, SmsEntity::VALI_EXPIRED)
            ->setParameter(3, new \DateTime())
            ->getQuery();
        $p = $q->execute();
        
        return $this->add($phone, 'Your PinCode is '.$this->_pincode, $periodSec, $this->_pincode);
    }

    public function send($id)
    {
        $sms = $this->_em->getRepository('EvBundle:Sms')->find($id);
        
        if($sms->getValiStatus() == SmsEntity::VALI_EXPIRED){
            return true;
        }
        
        $re = file_get_contents('http://smexpress.mitake.com.tw:9600/SmSendGet.asp?'.
            'username='.$this->_smsUser.
            '&password='.$this->_smsPassword.
            '&dstaddr='.$sms->getPhone().
            '&smbody='.$sms->getContent());
        
        //$str = 'msgid=1184715281
        //statuscode=1
        //AccountPoint=97';
        preg_match('/msgid=(\d*)[^\d]+statuscode=(\d).+AccountPoint=([\d]*)/is', $re, $match);

        if( ! isset($match[2])){
            $this->_error = 'Cant identity mistake sms api result, please check mistake status.';
            return false;            
        }
        
        //FIXME:須改寫，用 callback
        $success = intval(@$match[2]) == 1;
        $sms->setSendStatus($success);
        $this->_em->persist($sms);
        $this->_em->flush();


        if( ! boolval($success)){
            $this->_error = 'Send sms failed.';
            return false;
        }
        
        return true;
    }

    public function validation($phone, $pinCode, $expireMin = 5)
    {
        $query = $this->_em->createQueryBuilder();

        $query->select( 's.id, s.phone, s.pincode' )
            ->from( 'EvBundle:Sms',  's' )
            ->where('s.phone = '. $phone)
            ->andWhere('s.sendStatus = '.SmsEntity::SEND_SUCCESS)
            ->andWhere('s.valiStatus = '.SmsEntity::VALI_NONE)
            ->andWhere('s.createdAt > :now')
            ->setParameter('now', new \DateTime('-'.$expireMin.' minutes'), \Doctrine\DBAL\Types\Type::DATETIME)
            ->setMaxResults( 1 )
            ->setFirstResult( 0 )
//            ->orderBy('date(s.created_at) DESC');//this is the trick that works for me

            ->orderBy('s.createdAt', 'DESC');
            ;
        $result = $query->getQuery()->getOneOrNullResult();
        if( ! empty($result)){
            if($result['pincode'] == $pinCode){
                $obj = $this->_em->getReference('EvBundle:Sms', $result['id']);
                $obj->setValiStatus(1);
                $obj->setUpdatedAt(new \DateTime());
                $this->_em->persist($obj);
                $this->_em->flush($obj);
            
                return true;
            }else{
                $this->_error = 'pinCode is not currect.';
            }
        }else{
            $this->_error = 'pinCode is expired.';
        }
        
        return false;
    }
    
    public function error()
    {
        return $this->_error;
    }
    
    public function isValid($phone)
    {
        
    }
    
}
