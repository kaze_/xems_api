<?php

namespace EvBundle\Services;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;
use EvBundle\Entity\EvLocationUserVehicle;
use EvBundle\Entity\EvUserOwnVehicle;
use Exception;

class Vehicle
{

    private $_vehicleMetadata;
    private $_em;


    public function __construct(EntityManager $em, $vehicleMetadata)
    {
        $this->_em = $em;
        $this->_vehicleMetadata = $vehicleMetadata;
    }

    /**
     * @param object $user
     * @param string $evPlate
     * @param string modelId
     * 
     * @return object|array true|false
     */
    public function addVehicle($user, $evPlate, $modelId)
    {
        $model = $this->_vehicleMetadata->getModelDataByModelId($modelId);
        if ( !$model ) {
            return ["result" => false, "message" => "model not exist"];
        } 

        $evPlateCanonical = EvUserOwnVehicle::canonicalizeEvPlate($evPlate);
        $vehicle = $this->_em->getRepository('EvBundle:EvUserOwnVehicle')
        ->findOneBy(['user' => $user, 'evPlateCanonical' => $evPlateCanonical ]);

        if ( $vehicle ) {
            if ( $vehicle->getEnabled() == 1 ) {
                return ["result" => false, "message" => 'Duplicate entry'];
            } else if ( $vehicle->getEnabled() == 0 ) {
                $vehicle->setEnabled(1);
                $vehicle->setEvModelId($model->getId());
                $vehicle->setUpdatedAt(new \DateTime());
            }
        } else {
            $vehicle = new EvUserOwnVehicle();
            $vehicle->setUser($user);
            $vehicle->setEvPlate($evPlate);
            $vehicle->setEvModelId($model->getId());
            $vehicle->setEnabled(1);
            $vehicle->setUpdatedAt(new \DateTime());
            $vehicle->setCreatedAt(new \DateTime());
        }

        try {
            $this->_em->persist($vehicle);
            $this->_em->flush();
            $this->_em->refresh($vehicle);
        } catch (Exception $ex) {
            return ["result" => false, "message" => $ex->getMessage()];
        }
        return ["result" => true];
    }

    public function getvehicle($user, $evPlate)
    {
        $evPlateCanonical = EvUserOwnVehicle::canonicalizeEvPlate($evPlate);
        try {
            $vehicle = $this->_em->getRepository('EvBundle:EvUserOwnVehicle')
                ->findOneBy(['user' => $user, 'evPlateCanonical' => $evPlateCanonical, 'enabled' => 1 ]);
        } catch(Exception $e) {
            return null;
        }
        return $vehicle;
    }

    public function deleteVehicle($user, $evPlate) 
    {
        $vehicle = $this->getVehicle($user, $evPlate);
        
        if ( !$vehicle ) {
            return ["result" => false, "message" => "vehicle not found"];
        } 

        $vehicle->setEnabled(0);
        $vehicle->setUpdatedAt(new \DateTime());

        try {
            $this->_em->persist($vehicle);
            $this->_em->flush();
        } catch (Exception $ex) {
            return ["result" => false, "message" => $ex->getMessage()];
        }
        return ["result" => true];
    }

    public function editVehicle($user, $evPlate, $modelId)
    {
        $model = $this->_vehicleMetadata->getModelDataByModelId($modelId);
        if ( !$model ) {
            return ["result" => false, "message" => "model not exist"];
        } 

        $vehicle = $this->getVehicle($user, $evPlate);
        
        if ( !$vehicle ) {
            return ["result" => false, "message" => "vehicle not found"];
        } 

        $vehicle->setEvModelId($model->getId());
        $vehicle->setUpdatedAt(new \DateTime());

        try {
            $this->_em->persist($vehicle);
            $this->_em->flush();
        } catch (Exception $ex) {
            return ["result" => false, "message" => $ex->getMessage()];
        }
        return ["result" => true];
    }

    /**
     * @param object user objet 
     * @return array vehicle objects
     */
    public function getVehiclesByUser($user)
    {
        $vehicles = $this->_em->getRepository('EvBundle:EvUserOwnVehicle')
            ->findBy(['user' => $user, 'enabled' => 1 ]);
        return $vehicles;
    }

    /** 
     * Location & Vehicle
     */

    public function checkVehicleByLocationUsers($location, $user, $evPlate) 
    {
        $vehicleRepo = $this->_em->getRepository('EvBundle:EvUserOwnVehicle');
        $locationVehicle = $vehicleRepo->createQueryBuilder('q')
            // ->join('EvBundle:EvVehicleModelMaster', 'v', 'WITH', 'q.evModelId = v.id')
            ->leftJoin('EvBundle:EvLocationUserVehicle', 'l', 'WITH', 'q.id = l.userVehicleId and l.locationId = :location')
            ->select('l.enabled')
            ->where('q.user = :user')
            ->andWhere('q.enabled = 1')
            ->andWhere('q.evPlateCanonical = :plate')
            ->setParameter('user', $user)
            ->setParameter('location', $location)
            ->setParameter('plate', $evPlate)
            ->setMaxResults(1)
            ->getQuery()->getOneOrNullResult();

        if ( !$locationVehicle ) {
            return false;
        }
        if ( $locationVehicle['enabled'] === null ) {
            return true;
        } 
        return $locationVehicle['enabled'];
    }

    public function getVehiclesByLocationUsers($location, $user)
    {
        $vehicleRepo = $this->_em->getRepository('EvBundle:EvUserOwnVehicle');
        $locationVehicles = $vehicleRepo->createQueryBuilder('q')
            ->join('EvBundle:EvVehicleModelMaster', 'v', 'WITH', 'q.evModelId = v.id')
            ->leftJoin('EvBundle:EvLocationUserVehicle', 'l', 'WITH', 'q.id = l.userVehicleId and l.locationId = :location')
            ->select('q.evPlate, q.vinNumber, v.evModelName', 'l.enabled')
            ->where('q.user = :user')
            // ->andWhere('l.locationId = :location')
            ->andWhere('q.enabled = 1')
            ->setParameter('user', $user)
            ->setParameter('location', $location)
            ->getQuery()->getResult();
        
        $newVehicles = [];
        foreach($locationVehicles as $vehicle) {
            if ( $vehicle['enabled'] === null ) {
                $vehicle['enabled'] = true;
            }
            $newVehicles[] = $vehicle;
        }
         return $newVehicles;
    }

    public function enableVehecleByLocationUsers($location, $user, $evPlate)
    {
        $vehicle = $this->getVehicle($user, $evPlate);
        if ( !$vehicle ) {
            return false;
        }
        // print_r($vehicle);
        $locationVehicle = $this->_em->getRepository('EvBundle:EvLocationUserVehicle')
            ->findOneBy(['locationId' => $location->getLocationId(), 'userVehicleId' => $vehicle->getId()]);
        if ( !$locationVehicle ) {
            $locationVehicle = new EvLocationUserVehicle();
            $locationVehicle->setUserVehicleId($vehicle->getId());
            $locationVehicle->setLocationId($location->getLocationId());
            $locationVehicle->setCreatedAt(new \DateTime());
            $locationVehicle->setUpdatedAt(new \DateTime());
        }
        $locationVehicle->setEnabled(1);
        try {
            $this->_em->persist($locationVehicle);
            $this->_em->flush();
        } catch (Exception $ex) {
            // echo($ex->getMessage());
            return false;
        }
        return true;
    }

    public function disableVehicleByLocationUsers($location, $user, $evPlate)
    {
        $vehicle = $this->getVehicle($user, $evPlate);
        if ( !$vehicle ) {
            return false;
        }
        $locationVehicle = $this->_em->getRepository('EvBundle:EvLocationUserVehicle')
            ->findOneBy(['locationId' => $location->getLocationId(), 'userVehicleId' => $vehicle->getId()]);
        if ( !$locationVehicle ) {
            // 不太可能？
            $locationVehicle = new EvLocationUserVehicle();
            $locationVehicle->setUserVehicleId($vehicle->getId());
            $locationVehicle->setLocationId($location->getLocationId());
            $locationVehicle->setCreatedAt(new \DateTime());
        }
        $locationVehicle->setUpdatedAt(new \DateTime());
        $locationVehicle->setEnabled(0);
        try {
            $this->_em->persist($locationVehicle);
            $this->_em->flush();
        } catch (Exception $ex) {
            return false;
        }
        return true;
    }

    // add vehicle and enable at the same time
    public function addVehicleByLocationUsers($location, $user, $evPlate, $modelId)
    {
        $result = $this->addVehicle($user, $evPlate, $modelId);
        if ( is_array($result) && $result["result"] == false ) {
            return $result;
        }
        // $this->_em->clear();
        $result2 = $this->enableVehecleByLocationUsers($location, $user, $evPlate);
        if ( $result2 == false ) {
            return ['result' => false, 'message' => 'enable vehicle failed'];
        }
        return ['result' => true];
    }

    public function getLocationUserByVehicle($location, $evPlate) {
        $locationUserRepo = $this->_em->getRepository('EvBundle:EvUserLocationMapping');
        $locationUser = $locationUserRepo->createQueryBuilder('q')
            ->innerJoin('EvBundle:EvUserOwnVehicle', 'o', 'WITH', 'q.userId = o.user and o.enabled=1')
            ->leftJoin('EvBundle:EvLocationUserVehicle', 'v', 'WITH', 'q.userId = v.userVehicleId and v.enabled=1')
            // ->select('l.userId, v.enabled, l.unitNumber')
            ->where('o.evPlateCanonical = :evPlate')
            ->andWhere('q.statusId = 1 ')
            ->andWhere('q.locationId = :location')
            ->setParameter('evPlate', $evPlate)
            ->setParameter('location', $location)
            ->getQuery()->getOneOrNullResult();

        // print_r($locationUser);
        return $locationUser;
    }

    public function getUserVehicleByLocation($location)
    {
        // still, find location user first => find plate => find points
        $userLocationRepo = $this->_em->getRepository('EvBundle:EvUserLocationMapping');
        
        $plates = $userLocationRepo->createQueryBuilder('ul')
            ->join('EvBundle:EvUserOwnVehicle', 'uv', 'WITH', 'ul.userId = uv.user')
            ->leftJoin('EvBundle:EvLocationUserVehicle', 'luv', 'WITH', 'luv.userVehicleId = uv.id')
            ->join('EvBundle:EvUserDetail', 'ud', 'WITH', 'ud.userId = ul.userId')
            ->join('EvBundle:EvLocationPoints', 'lp', 'WITH', 'lp.userId = ul.userId and lp.locationId = ul.locationId')
            ->select('uv.evPlateCanonical as plate, ud.name, ud.cellPhone, lp.points')
            ->where('uv.enabled = 1')
            ->andWhere('ul.statusId = 1')
            ->andWhere('luv.enabled = 1 or luv.enabled is null')
            ->andWhere('ul.locationId = :location')
            ->setParameter('location', $location)

            // if car plates are duplicated, select newest one
            ->orderBy('ul.createdAt', 'DESC')
            ->groupBy('plate')

            ->getQuery()->getResult();

        return $plates;
    }
}