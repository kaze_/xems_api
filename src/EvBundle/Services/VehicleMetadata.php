<?php

namespace EvBundle\Services;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;

class VehicleMetadata
{
    private const version_data_name = "vehicle_model";
    private $_vehicleMetadata;
    private $_modelIdIndex;
    private $_index;
    private $_vehicleMetadataVersion;
    private $_em;

    public function __construct(EntityManager $em)
    {
        $this->_em = $em;
        $this->cacheVehicleMetadata();
    }

    private function cacheVehicleMetadata()
    {
        // get vehicle model
        $repo = $this->_em->getRepository('EvBundle:EvVehicleModelMaster');
        // $result = $repo->createQueryBuilder('c')->getQuery()->getResult(Query::HYDRATE_ARRAY);
        $result = $repo->findAll();
        $this->_vehicleMetadata = $result;
        
        // make index
        $index = [];
        $modelIdIndex = [];
        foreach($result as $model) {
            $index[$model->getId()] = $model;
            $modelIdIndex[$model->getEvModelId()] = $model;
        }
        $this->_index = $index;
        $this->_modelIdIndex = $modelIdIndex;

        // get data version
        $vRepo = $this->_em->getRepository('EvBundle:EvDataVersion');
        $vResult = $vRepo->findOneBy(['evDataName' => $this::version_data_name]);
        $this->_vehicleMetadataVersion = $vResult->getVersion();
    }

    public function getVehicleMetadata()
    {
        return [
            "version" => [
                "data_name" => $this::version_data_name, 
                "version" => $this->_vehicleMetadataVersion
            ],
            "data" => $this->_vehicleMetadata
        ];
    }

    /**
     * @param int id
     * @return object|null
     */
    public function getModelDataById($id) 
    {
        if ( isset ($this->_index[$id]))
            return $this->_index[$id];
        return null;
    }

    /**
     * @param string id
     * @return object|null
     */
    public function getModelDataByModelId($id) 
    {
        if ( isset($this->_modelIdIndex[$id]))
            return $this->_modelIdIndex[$id];
        return null;
    }

}