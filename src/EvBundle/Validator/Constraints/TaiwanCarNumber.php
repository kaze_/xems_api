<?php
/**
 * Created by PhpStorm.
 * User: wild
 * Date: 2018/1/16
 * Time: 上午11:30
 */

namespace EvBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class TaiwanCarNumber extends Constraint
{
    public $message = 'It can only contain 00-0000, 0000-00, 000-000, 000-0000, 0000000 and always uppercase.';
}
