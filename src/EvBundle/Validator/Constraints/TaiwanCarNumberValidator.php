<?php
/**
 * Created by PhpStorm.
 * User: wild
 * Date: 2018/1/16
 * Time: 上午11:31
 */

namespace EvBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class TaiwanCarNumberValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if ( ! (
             preg_match(
                '/^([0-9A-Z]{2}-[0-9A-Z]{4}$)|'.
                '(^[0-9A-Z]{4}-[0-9A-Z]{2})$|'.
                '(^[0-9A-Z]{3}-[0-9A-Z]{3,4})$|'.
		        '(^[0-9A-Z]+$)'.
                '/', $value, $matches)
        )
        ) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ string }}', $value)
                ->addViolation();
        }
    }
}
