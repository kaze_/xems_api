<?php
/**
 * Created by PhpStorm.
 * User: wild
 * Date: 2018/1/16
 * Time: 上午11:30
 */

namespace EvBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class TaiwanPhone extends Constraint
{
    public $message = 'It can only contain 09xxxxxxxx, 10 numbers.';
}