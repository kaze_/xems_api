<?php

namespace IothubBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

class DataTransCommand extends ContainerAwareCommand
{
    protected function configure()
    {
    	$this->setName('iothub:datatransfer')
            ->setDescription('Transfer data from iothub to ems')
            ->setHelp('This command allow you to transfer data from iothub to ems')
	;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $trans = $this->getContainer()->get('iothub.iothub2ems');
        $trans->iothub2Ems();
    
        echo('Data transfered\n');
    }
}

