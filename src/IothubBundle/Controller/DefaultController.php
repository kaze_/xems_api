<?php

namespace IothubBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('IothubBundle:Default:index.html.twig');
    }

    public function testAction()
    {
        $tran = $this->get('iothub.iothub2ems');
        $tran->iothub2Ems();
/*        $repository = $this->getDoctrine()->getManager('iothub_lab')->getRepository('IothubBundle:PowerReport');
        $data = $repository->findBy(array('circuitId' => 'Module1'));
        $data = $repository->findAll();
        return $data;
*/
    }
}
