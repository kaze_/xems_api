<?php

namespace IothubBundle\Entity;

/**
 * PowerReport
 */
class PowerReport
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $circuitId;

    /**
     * @var \DateTime
     */
    private $reportDatetime;

    /**
     * @var int
     */
    private $modbusId;

    /**
     * @var float
     */
    private $vAverage;

    /**
     * @var float
     */
    private $aAverage;

    /**
     * @var float
     */
    private $kWTotal;

    /**
     * @var float
     */
    private $kWhTotal;

    /**
     * @var float
     */
    private $PFTotal;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set circuitId.
     *
     * @param string $circuitId
     *
     * @return PowerReport
     */
    public function setCircuitId($circuitId)
    {
        $this->circuitId = $circuitId;

        return $this;
    }

    /**
     * Get circuitId.
     *
     * @return string
     */
    public function getCircuitId()
    {
        return $this->circuitId;
    }

    /**
     * Set reportDatetime.
     *
     * @param \DateTime $reportDatetime
     *
     * @return PowerReport
     */
    public function setReportDatetime($reportDatetime)
    {
        $this->reportDatetime = $reportDatetime;

        return $this;
    }

    /**
     * Get reportDatetime.
     *
     * @return \DateTime
     */
    public function getReportDatetime()
    {
        return $this->reportDatetime;
    }

    /**
     * Set modbusId.
     *
     * @param int $modbusId
     *
     * @return PowerReport
     */
    public function setModbusId($modbusId)
    {
        $this->modbusId = $modbusId;

        return $this;
    }

    /**
     * Get modbusId.
     *
     * @return int
     */
    public function getModbusId()
    {
        return $this->modbusId;
    }

    /**
     * Set vAverage.
     *
     * @param float $vAverage
     *
     * @return PowerReport
     */
    public function setVAverage($vAverage)
    {
        $this->vAverage = $vAverage;

        return $this;
    }

    /**
     * Get vAverage.
     *
     * @return float
     */
    public function getVAverage()
    {
        return $this->vAverage;
    }

    /**
     * Set aAverage.
     *
     * @param float $aAverage
     *
     * @return PowerReport
     */
    public function setAAverage($aAverage)
    {
        $this->aAverage = $aAverage;

        return $this;
    }

    /**
     * Get aAverage.
     *
     * @return float
     */
    public function getAAverage()
    {
        return $this->aAverage;
    }

    /**
     * Set kWTotal.
     *
     * @param float $kWTotal
     *
     * @return PowerReport
     */
    public function setKWTotal($kWTotal)
    {
        $this->kWTotal = $kWTotal;

        return $this;
    }

    /**
     * Get kWTotal.
     *
     * @return float
     */
    public function getKWTotal()
    {
        return $this->kWTotal;
    }

    /**
     * Set kWhTotal.
     *
     * @param float $kWhTotal
     *
     * @return PowerReport
     */
    public function setKWhTotal($kWhTotal)
    {
        $this->kWhTotal = $kWhTotal;

        return $this;
    }

    /**
     * Get kWhTotal.
     *
     * @return float
     */
    public function getKWhTotal()
    {
        return $this->kWhTotal;
    }

    /**
     * Set pFTotal.
     *
     * @param float $pFTotal
     *
     * @return PowerReport
     */
    public function setPFTotal($pFTotal)
    {
        $this->PFTotal = $pFTotal;

        return $this;
    }

    /**
     * Get pFTotal.
     *
     * @return float
     */
    public function getPFTotal()
    {
        return $this->PFTotal;
    }
}
