<?php

namespace IothubBundle\Services;

use Doctrine\ORM\EntityManager;
use AppBundle\Entity\EmsPowerReportLog;

class IothubDataTransfer
{
    private $doctrine;
    private $limit=100;

    public function __construct($doctrine)
    {
        $this->doctrine = $doctrine;
    }
    public function iothub2Ems()
    {
        // $doctrine = $this->container->get('doctrine');
        $connections = $this->doctrine->getConnectionNames();
        $keys = array_keys($connections);

        $em = $this->doctrine->getManager();

        $mapRepo = $em->getRepository('AppBundle:EmsIothubMapping');
        foreach($keys as $conn) { 
            $maps = $mapRepo->findBy(array('connection' => $conn));
            if ( empty($maps) ) {
                continue;
            }
//            echo ("processing connection: ".$conn.PHP_EOL);
            foreach($maps as $map) {
                $repo = $this->doctrine->getManager($conn)->getRepository($map->getEntity());
                $qb = $repo->createQueryBuilder('q');
                $qb->where('q.id > :id')
                    ->andWhere('q.circuitId = :circuit')
                    ->setParameter('circuit', $map->getIothubCircuitId())
                    ->setParameter('id', $map->getMaxId())
                    ->orderBy('q.id', 'ASC')
                    ->setMaxResults($this->limit);
                $result = $qb->getQuery()->getResult();
                $max_id = $map->getMaxId();
//                echo("circuit ".$map->getCircuitId()." from data id: ".$max_id.PHP_EOL);
                if ( $conn == 'default' ) {
                    foreach($result as $report) {
                        $powerReport = clone $report;
                        $powerReport->setCircuitId($map->getCircuitId());
                        $em->persist($powerReport);
                        $max_id = $report->getId();
                    }
                } else {
                    foreach($result as $report) {
                        $powerReport = new EmsPowerReportLog();
                        $powerReport->setCircuitId($map->getCircuitId());
                        $powerReport->setAAverage($report->getAAverage());
                        $powerReport->setVAverage($report->getVAverage());
                        $powerReport->setReportDatetime($report->getReportDatetime());
                        $powerReport->setModbusId($report->getModbusId());
                        $powerReport->setKwTotal($report->getKWTotal());
                        $powerReport->setKwhTotal($report->getKWhTotal());
                        $powerReport->setPfTotal($report->getPFTotal());
                    
                        $em->persist($powerReport);
                        $max_id = $report->getId();
                    }
                }
                $map->setMaxId($max_id);
                $em->persist($map);
                $em->flush();
            }
        }
        // get mapping from EmsIothubMapping
 /*       
        $mapRepo = $em->getRepository('AppBundle:EmsIothubMapping');
        $circuitsMap = $mapRepo->findAll();
   */     

        // process each mapping
        
        // safe max_id to each mapping
    }
}

