<?php

namespace tests\AppBundle\Controller\GatewayControllerTest;

use PHPUnit\Framework\TestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class GatewayControllerTest extends WebTestCase
{
    // use ChengGong Gateway and charging circuits;
    private $gatewayId = "b1292434-0085-4ba0-8e4d-504fe30c0e99";
    private $circuits = [
        '9edc4fef-31ed-4a59-88aa-0bb7be50a6c8',
        '9a73f656-655e-4e65-8262-fdfc411b18f6',
        '0dcbcb7f-a72a-4dda-bf25-c441ceeb887c',
        '880014ef-ef55-4f3b-8d06-8793dc5d96a4',
        '60b81619-8959-4b2a-987d-e791f18b9313',
        '8d436dec-dfa3-465d-94bb-c5c6f5e5f805'
    ];

    // preserved:
    // private plugStatus = ['U', 'C', 'P', 'N', 'D', 'F'];
    private $plugStatus = ['U', 'C', 'P'];
    // stored in ~xmight/Desktop/storage0/.env in gateways
    private $gatewayAuth = [
        // use bill's old account in database
        // "user" => "c6078be0-054e-4032-b5ff-8d00b66d44d9", 
        // "token" => "d84fc0af89f65fa126736d554324aa4600b8dbbdde2ef1208fc3849cf48fa78d"
        // or, onyx's old account in database
        'user' => "cdfb009a-f231-4e57-9ce0-0d0fa008dcb0",
        'token' => "01b24aa5f431ccd8adc3af6a2dbbe226c7b82db92c99c8f63f250ef6b8b23131"
    ];

    private function generateGatewayStatus()
    {
        $gatewayData = [];
        forEach($this->circuits as $circuitId) {
            // circuit loop 
            $count = rand(1, 3);
            $circuitData = [];
            for($i = 0; $i < $count; $i++ ) {
                $newData = [
                    'update_time' => (new \DateTime())->format('Y-m-d H:i:s'),
                    'last_plug_status_time' => (new \DateTime())->format('Y-m-d H:i:s'),
                    'plug_status' => $this->plugStatus[rand(0,2)],
                    'acc_power' => rand(0,500)/100,
                    'circuit_power' => rand(0,1000/100),
                ];
                $circuitData[] = $newData;
            }
            $gatewayData[$circuitId] = $circuitData;
        }
        return $gatewayData;
    }

    public function testSetStatus()
    {
        $client = static::createClient();
        $gwData = $this->generateGatewayStatus();

        $crawler = $client->request(
            'POST', 
            '/api/v1/gateways/'.$this->gatewayId.'/status',
            array(),
            array(),
            array(
                'CONTENT_TYPE' => 'application/json', 
                // add "HTTP_" before customed header
                'HTTP_X-XM-USER' => $this->gatewayAuth['user'],
                'HTTP_X-XM-Auth-Token' => $this->gatewayAuth['token']
            ),
            json_encode($gwData),
        );
        echo(json_encode($gwData));
        echo($client->getResponse()->getContent());
        $this->assertTrue($client->getResponse()->isSuccessful());
    }
}