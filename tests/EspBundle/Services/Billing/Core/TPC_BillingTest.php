<?php
/**
 * Created by PhpStorm.
 * User: chiehyingpan
 * Date: 2018/5/7
 * Time: 下午3:44
 */

namespace tests\EspBundle\Services\Billing\Core;

use EspBundle\Services\Billing\Core\TPC_Billing;
use PHPUnit\Framework\TestCase;

class TPC_BillingTest extends TestCase
{
    private $rateTable;

    public function __construct()
    {
        parent::__construct();
        //$path = $this->get('kernel')->getRootDir() . '/../src/EspBundle/Resources/config/tpc20180401.json';
        //$path = $fileLocator->locate('@EspBundle/Resources/config/tpc20160401.json');
        $path = __DIR__.'/../../../../../src/EspBundle/Resources/config/tpc20180401.json';
        $this->rateTable = json_decode(file_get_contents($path));
        // print_r($this->rateTable);
    }
/*
    public function testPricing1()
    {
        $testdata = [
            ["2017-03-16 00:00",26882.52],
            ["2017-03-16 00:05",28894.56],
            ["2017-03-16 00:10",28895.64],
            ["2017-03-16 00:15",28896.6],
            ["2017-03-16 00:20",28897.68],
            ["2017-03-16 00:25",28898.52],
            ["2017-03-16 07:35",28899.99]
        ];
        $r = new TPC_Billing(2, $this->rateTable);
        print_r($r->pricing($testdata));

        $r = new TPC_Billing(7, $this->rateTable);
        $contract = ["normal"=>60, 'semi_peak'=>0, "saturday_semi_peak"=>20, "off_peak"=>10];
        $r->setDemandContract($contract);
        print_r($r->pricing($testdata));

        $contract = ["normal"=>78, "semi_peak"=>0, "saturday_semi_peak"=>0, "off_peak"=>0];
        $max_dmd1 = ["normal"=>71, "semi_peak"=>0, "saturday_semi_peak"=>73, "off_peak"=>76];
        $energy = ["energy"=>["peak"=>8480, "semi_peak"=>0, "saturday_semi_peak"=>2080, "off_peak"=>4400], "date"=>"2017-03-30"];
        $r = new TPC_Billing(5, $this->rateTable);
        $r->setDemandContract($contract);
        $r->setMaxDemand($max_dmd1);
        $r->setPhase(3);
        $r->setEnergy($energy);
        print_r($r->pricing());
    }

    private function doTest($st,$dt,$contract=null,$max_dmd=null,$phase=3){
        $data1 = [];
        $t = date_create_from_format("Y-m-d H:i", $dt);
        $pot = $t->format("Ym");
        $kwh = mt_rand(100,1000);
        do{
            $kwh += mt_rand(1,100)/100.0;
            array_push($data1,[$t->format("Y-m-d H:i"), $kwh]);
            $t->modify("+5 minutes");
        } while($pot == $t->format("Ym"));
        //
        //$r = new TPC_Billing($st);
        $r = new TPC_Billing($st, $this->rateTable);
        $r->setPhase($phase);
        // echo "<hr>".$r->service_rate->desc."(".$data1[0][0].") ed:".$r->service_rate->effective_date;
        if ($contract != null) $r->setDemandContract($contract);
        if ($max_dmd != null) $r->setMaxDemand($max_dmd);
        print_r($r->pricing($data1));
    }

    public function testPricing2()
    {
        $this->doTest(1, "2017-07-01 00:00");
        $this->doTest(2, "2017-10-01 00:00");
        $this->doTest(3, "2017-10-01 00:00");
        $this->doTest(4, "2017-07-01 00:00");

        $contract = ["normal" => 60, "semi_peak" => 10, "saturday_semi_peak" => 6, "off_peak" => 5];
        $max_dmd1 = ["normal" => 69, "semi_peak" => 0, "saturday_semi_peak" => 85, "off_peak" => 90];
        $this->doTest(5, "2017-07-01 00:00", $contract, $max_dmd1);

        $contract = ["normal" => 50, "semi_peak" => 0, "saturday_semi_peak" => 0, "off_peak" => 0];
        $this->doTest(6, "2017-07-01 00:00", $contract);
        $this->doTest(6, "2017-10-01 00:00", $contract);
        
        $contract = ["normal" => 60, "semi_peak" => 10, "saturday_semi_peak" => 6, "off_peak" => 5];
        $max_dmd1 = ["normal" => 69, "semi_peak" => 0, "saturday_semi_peak" => 85, "off_peak" => 90];
        $this->doTest(7, "2017-07-01 00:00", $contract, $max_dmd1);
        $max_dmd2 = ["normal" => 70, "semi_peak" => 0, "saturday_semi_peak" => 77, "off_peak" => 95];
        $this->doTest(7, "2017-10-01 00:00", $contract, $max_dmd2);

        $contract = ["normal" => 100, "semi_peak" => 20, "saturday_semi_peak" => 10, "off_peak" => 5];
        $max_dmd1 = ["normal" => 110, "semi_peak" => 0, "saturday_semi_peak" => 146, "off_peak" => 140];
        $this->doTest(8, "2017-07-01 00:00", $contract, $max_dmd1);
        $max_dmd2 = ["normal" => 124, "semi_peak" => 0, "saturday_semi_peak" => 139, "off_peak" => 159];
        $this->doTest(8, "2017-10-01 00:00", $contract, $max_dmd2);

        $contract = ["normal" => 200, "semi_peak" => 20, "saturday_semi_peak" => 10, "off_peak" => 5];
        $max_dmd1 = ["normal" => 201, "semi_peak" => 223, "saturday_semi_peak" => 236, "off_peak" => 245];
        $this->doTest(9, "2017-07-01 00:00", $contract, $max_dmd1);
        $max_dmd2 = ["normal" => 0, "semi_peak" => 223, "saturday_semi_peak" => 236, "off_peak" => 268];
        $this->doTest(9, "2017-10-01 00:00", $contract, $max_dmd2);

        $contract = ["normal" => 78, "semi_peak" => 0, "saturday_semi_peak" => 0, "off_peak" => 0];
        $max_dmd1 = ["normal" => 86, "semi_peak" => 0, "saturday_semi_peak" => 86, "off_peak" => 91];
        $this->doTest(5, "2017-04-01 00:00", $contract, $max_dmd1);
    }

    public function testPricing3()
    {
        // testing 3...
        $path = __DIR__.'/../../../../../src/EspBundle/Resources/config/tpc20180401.json';
        $rtable = json_decode(file_get_contents($path));

        $r = new TPC_Billing(5, $rtable);
        $contract = ["normal"=>78, "semi_peak"=>0, "saturday_semi_peak"=>0, "off_peak"=>0];
        $mx_dmd = ["normal"=>71, "semi_peak"=>0, "saturday_semi_peak"=>73, "off_peak"=>76];
        $energy = ["energy"=>["peak"=>8480, "semi_peak"=>0, "saturday_semi_peak"=>2080, "off_peak"=>4400], "date"=>"2017-03-30"];
        $r->setDemandContract($contract);
        $r->setMaxDemand($mx_dmd);
        $r->setPhase(3);
        $r->setEnergy($energy);
        print_r($r->pricing());
    }
*/
}
