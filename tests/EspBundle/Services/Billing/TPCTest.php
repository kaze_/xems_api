<?php
/**
 * Created by PhpStorm.
 * User: chiehyingpan
 * Date: 2018/5/9
 * Time: 下午12:20
 */

namespace tests\EspBundle\Services\Billing;

use EspBundle\Services\Billing\TPC;
use PHPUnit\Framework\TestCase;

class TPCTest extends TestCase
{
    private $rateTable;

    public function __construct()
    {
        parent::__construct();
        //$path = $this->get('kernel')->getRootDir() . '/../src/EspBundle/Resources/config/tpc20180401.json';
        //$path = $fileLocator->locate('@EspBundle/Resources/config/tpc20160401.json');
        $path = __DIR__.'/../../../../src/EspBundle/Resources/config/tpc20180401.json';
        $this->rateTable = json_decode(file_get_contents($path));
        // print_r($this->rateTable);
    }
/*
    public function testIsSummer()
    {

    }

    public function testReduce_monthly_ntou()
    {

    }

    public function testReduce_monthly()
    {
        $testdata = [
            ["2017-03-16 00:00",26882.52],
            ["2017-03-16 00:05",28894.56],
            ["2017-03-16 00:10",28895.64],
            ["2017-03-16 00:15",28896.6],
            ["2017-03-16 00:20",28897.68],
            ["2017-03-16 00:25",28898.52],
            ["2017-03-16 07:35",28899.99],
            ["2017-03-16 08:00",28900.22]
        ];
        for($i = 1; $i <= 11; $i++) {
            $tpc = new TPC($i, $this->rateTable);
            print_r($tpc->reduce_monthly_to_power($testdata));
        }
    }

    public function testGetPeriodFromCode()
    {
        $tpc = new TPC(1, $this->rateTable);
        print_r($tpc->getPeriodFromCode('op'));
        print_r($tpc->getPeriodFromCode('pp'));
        print_r($tpc->getPeriodFromCode('spp'));
        print_r($tpc->getPeriodFromCode('p'));
    }

    public function testGetRateInfo()
    {

    }

    public function testReduce_time_period()
    {

    }

    public function testGetTpList()
    {
        print_r(TPC::getTpList());
        print_r(TPC::getTpList(true));
    } 
    */
}
