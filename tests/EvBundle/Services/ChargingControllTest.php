<?php
/**
 * Created by PhpStorm.
 * User: chiehyingpan
 * Date: 2018/5/9
 * Time: 下午12:20
 */

namespace tests\EvBundle\Services\ChargingControll;

use EvBundle\Services\ChargingControll;
use PHPUnit\Framework\TestCase;
use EvBundle\Services\ChargingPolicy\ChargingPolicy;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ChargingControllTest extends WebTestCase
{
    private $chargingControll = null;
    private $_em;
    private $_locationSetting;
    private $_logger;
    private $_payPoints;
    private $_vehicle;
    private $_energyFee;
    /*  public function __construct(
        EntityManager $em, $locationSetting,  logger $logger, $payPoints, 
            $vehicle, $energyFee)
        arguments: ["@doctrine.orm.entity_manager", "@ev.location_setting", 
            "@monolog.logger", "@pay.points", "@ev.vehicle", "@esp.fee"]
        */

    public function __construct()
    {
        $client = static::createClient();
        $container = $client->getContainer();
        $this->_em = $container->get('doctrine.orm.entity_manager');
        $this->_locationSetting = $container->get('ev.location_setting');
        $this->_logger = $container->get('monolog.logger.php');
        $this->_payPoints = $container->get('pay.points');
        $this->_vehicle = $container->get('ev.vehicle');
        $this->_energyFee = $container->get('esp.fee');

        $this->chargingControll = new ChargingControll($this->_em, $this->_locationSetting, 
            $this->_logger, $this->_payPoints, $this->_vehicle, $this->_energyFee);
        parent::__construct();
    }
    
    //     public function validateCharging($user, $evPlate, $station, $guest=0)
    public function doTestValidateCharging($result, $user, $evPlate, $station, $guest)
    {
        $stationObj = $this->_em->find('AppBundle:EvChargingStationMaster', $station);
        $this->assertEquals($result, 
            $this->chargingControll->validateCharging($user, $evPlate, $stationObj, $guest));
    }
    
    public function testValidateCharging()
    {
        //=== TEST DATA ===/
        // TODO - 建立 test data，不要散在程式裡面
        // test ChengGong Apartment Charging
        $user = 'a837cba3-314b-4583-aeee-7bb53777fa47';
        $evPlate = 'AAAACC';
        $station = 'b5d55e94-cb97-4c73-bbe2-b706100d231a';
        $guest = 0;
        $this->doTestValidateCharging(ChargingPolicy::VALIDATE_OK,
            $user, $evPlate, $station, $guest);

        // test ChengGong Apartment Charging and plate with dash (AA-CCCC)
        $user = 'a837cba3-314b-4583-aeee-7bb53777fa47';
        $evPlate = 'AACCCC';
        $station = 'b5d55e94-cb97-4c73-bbe2-b706100d231a';
        $guest = 0;
        $this->doTestValidateCharging(ChargingPolicy::VALIDATE_OK,
            $user, $evPlate, $station, $guest);

        // test FarEast Charging
        $user = 'a837cba3-314b-4583-aeee-7bb53777fa47';
        $evPlate = 'AAAACC';
        $station = '375bd311-512b-4e09-bc36-b8adcae9578b';
        $this->doTestValidateCharging(ChargingPolicy::VALIDATE_OK,
            $user, $evPlate, $station, $guest);
        
        // test FarEast Charging and plate with dash (AA-CCCC)
        $user = 'a837cba3-314b-4583-aeee-7bb53777fa47';
        $evPlate = 'AACCCC';
        $station = '375bd311-512b-4e09-bc36-b8adcae9578b';
        $this->doTestValidateCharging(ChargingPolicy::VALIDATE_OK,
            $user, $evPlate, $station, $guest);

        // test ChengGong Apartment and account with no points
        $user = '4143e277-33c3-464f-9dca-0ecf1050f0a6';
        $evPlate = 'EAA2151';
        $station = 'b5d55e94-cb97-4c73-bbe2-b706100d231a';
        $this->doTestValidateCharging(ChargingPolicy::VALIDATE_NO_BALANCE,
            $user, $evPlate, $station, $guest);

        // test OCPP test site with OCPP test user
        $user = '058a5a31-1bb4-48b3-9755-7eb557d7be34';
        $evPlate = null;
        $station = '8391a920-c597-11ea-a203-17ee8d4f6ddc';
        $this->doTestValidateCharging(ChargingPolicy::VALIDATE_OK,
            $user, $evPlate, $station, $guest);

        //=== REAL DATA ===/
        // test REAL FarEast station and account with invalid User
        $user = '178b1552-e4f8-4abc-9fe4-227483966e44';
        $evPlate = 'REA3633';
        $station = '7f5fe398-3b50-40c0-9083-3b93eced6fe2';
        $this->doTestValidateCharging(ChargingPolicy::VALIDATE_BAD_USER,
            $user, $evPlate, $station, $guest);

        // test READ FarEast station and account with valid User
        $user = '001626b2-0a13-4fe5-b21f-29fdb6ce7cb3';
        $evPlate = 'EAB2799';
        $station = '7f5fe398-3b50-40c0-9083-3b93eced6fe2';
        $this->doTestValidateCharging(ChargingPolicy::VALIDATE_OK,
            $user, $evPlate, $station, $guest);
    }
    /*
    public function testCalculateChargingFeeBySession()
    {
	
    }
*/
}
