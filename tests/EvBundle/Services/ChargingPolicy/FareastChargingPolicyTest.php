<?php

namespace tests\EvBundle\Services\ChargingPolicy\FareastChargingPolicy;

use DateTime;
use PHPUnit\Framework\TestCase;
use EvBundle\Services\ChargingPolicy\FareastChargingPolicy as FareastPolicy;
use EvBundle\Services\ChargingPolicy\ChengGongChargingPolicy;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use EvBundle\Services\ChargingControll;

class FareastChargingPolicyTest extends TestCase
{
    public function __construct()
    {
        parent::__construct();
    }

    public function testCheckTimeBetweenTimePeriod() {
        // 設定時段為跨日
        // session 沒跨日的狀況
        $this->doTestCheckTimeBetweenTimePeriod(true, '22:30:00', '07:30:00', 
            '2020-09-25 21:23:00', '2020-09-25 23:00:00');
        $this->doTestCheckTimeBetweenTimePeriod(true, '22:30:00', '07:30:00', 
            '2020-09-25 22:40:00', '2020-09-25 23:00:00');
        // session 跨日的狀況
        $this->doTestCheckTimeBetweenTimePeriod(true, '22:30:00', '07:30:00', 
            '2020-09-25 22:40:00', '2020-09-26 06:00:00');
        $this->doTestCheckTimeBetweenTimePeriod(true, '22:30:00', '07:30:00', 
            '2020-09-25 22:40:00', '2020-09-26 08:00:00');
        $this->doTestCheckTimeBetweenTimePeriod(true, '22:30:00', '07:30:00', 
            '2020-09-25 21:00:00', '2020-09-26 08:00:00');
        $this->doTestCheckTimeBetweenTimePeriod(true, '22:30:00', '07:30:00', 
            '2020-09-25 08:00:00', '2020-09-26 08:00:00');
        $this->doTestCheckTimeBetweenTimePeriod(true, '22:30:00', '07:30:00', 
            '2020-09-25 09:00:00', '2020-09-26 10:00:00');
        // 沒跨到設定時段的狀況
        $this->doTestCheckTimeBetweenTimePeriod(false, '22:30:00', '07:30:00', 
            '2020-09-25 08:00:00', '2020-09-25 21:00:00');
        // 特殊情況
        $this->doTestCheckTimeBetweenTimePeriod(true, '22:30:00', '07:30:00', 
            '2020-09-25 22:30:00', '2020-09-26 07:30:00');
        $this->doTestCheckTimeBetweenTimePeriod(false, '22:30:00', '07:30:00', 
            '2020-09-25 22:30:00', '2020-09-25 07:30:00');
        $this->doTestCheckTimeBetweenTimePeriod(false, '22:30:00', '07:30:00', 
            '2020-09-25 22:30:00', '2020-09-24 07:30:00');

        // 設定時段為非跨日
        // session 沒跨日的狀況
        $this->doTestCheckTimeBetweenTimePeriod(true, '22:30:00', '23:30:00', 
            '2020-09-25 21:23:00', '2020-09-25 23:30:00');
        $this->doTestCheckTimeBetweenTimePeriod(true, '22:30:00', '23:30:00', 
            '2020-09-25 22:40:00', '2020-09-25 23:30:00');
        // session 跨日的狀況
        $this->doTestCheckTimeBetweenTimePeriod(true, '22:30:00', '23:30:00', 
            '2020-09-25 22:40:00', '2020-09-26 06:00:00');
        $this->doTestCheckTimeBetweenTimePeriod(true, '22:30:00', '23:30:00', 
            '2020-09-25 22:40:00', '2020-09-26 08:00:00');
        $this->doTestCheckTimeBetweenTimePeriod(true, '22:30:00', '23:30:00', 
            '2020-09-25 21:00:00', '2020-09-26 08:00:00');
        $this->doTestCheckTimeBetweenTimePeriod(true, '22:30:00', '23:30:00', 
            '2020-09-25 08:00:00', '2020-09-26 08:00:00');
        $this->doTestCheckTimeBetweenTimePeriod(true, '22:30:00', '23:30:00', 
            '2020-09-25 09:00:00', '2020-09-26 10:00:00');
        // 沒跨到設定時段的狀況
        $this->doTestCheckTimeBetweenTimePeriod(false, '22:30:00', '23:30:00', 
            '2020-09-25 08:00:00', '2020-09-25 09:00:00');
        // 特殊情況
        $this->doTestCheckTimeBetweenTimePeriod(true, '22:30:00', '23:30:00', 
            '2020-09-25 22:30:00', '2020-09-25 23:30:00');
        $this->doTestCheckTimeBetweenTimePeriod(false, '22:30:00', '23:30:00', 
            '2020-09-25 22:30:00', '2020-09-25 21:30:00');
        $this->doTestCheckTimeBetweenTimePeriod(false, '22:30:00', '23:30:00', 
            '2020-09-25 22:30:00', '2020-09-24 21:30:00');
    }

    private function doTestCheckTimeBetweenTimePeriod($expect, $timeStr, $timeStr2, $startStr, $endStr) {
        $this->assertEquals($expect, FareastPolicy::checkTimeBetweenTimePeriod($timeStr, $timeStr2,
            DateTime::createFromFormat('Y-m-d H:i:s', $startStr),
            DateTime::createFromFormat('Y-m-d H:i:s', $endStr) )
        );
    }
}


class FareastChargingFeeTest extends WebTestCase {
    private $chargingControll = null;
    private $_em;
    private $_locationSetting;
    private $_logger;
    private $_payPoints;
    private $_vehicle;
    private $_energyFee;

    public function __construct()
    {
        $client = static::createClient();
        $container = $client->getContainer();
        $this->_em = $container->get('doctrine.orm.entity_manager');
        $this->_locationSetting = $container->get('ev.location_setting');
        $this->_logger = $container->get('monolog.logger.php');
        $this->_payPoints = $container->get('pay.points');
        $this->_vehicle = $container->get('ev.vehicle');
        $this->_energyFee = $container->get('esp.fee');

        $this->chargingControll = new ChargingControll($this->_em, $this->_locationSetting, 
            $this->_logger, $this->_payPoints, $this->_vehicle, $this->_energyFee);
        parent::__construct();
    }

    public function testCalculateChargingFeeBySession() {
        // 跨到設定的時段，且未滿30度電
        $session_id = '7fd0cc75-6de1-4c5f-929e-8cfd5d10f3cc';
        $price = $this->getPrice($session_id);
        $this->assertEquals(60, $price);

        // 跨到設定的時段，且超過30度電
        $session_id = 'cba32d8a-0d6a-4d5a-b12a-80f265d4cce9';
        $price = $this->getPrice($session_id);
        $this->assertEquals(138, $price);

        // 未跨到設定的時段
        $session_id = '129152eb-efe4-4a8a-91c2-dd0241dbfe2a';
        $price = $this->getPrice($session_id);
        $this->assertEquals(82, $price);
    }

    private function getPrice($session_id) {
        $session = $this->_em->getRepository('EvBundle:EvChargingSessionLog')->findOneBy(['sessionId' => $session_id]);
        return $this->chargingControll->calculateChargingFeeBySession($session);
    }
}