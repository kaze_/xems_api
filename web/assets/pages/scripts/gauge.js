     document.addEventListener("DOMContentLoaded", function(event) {

      var g1 = new JustGage({
        id: 'g1',
        value: 45,
        min: 0,
        max: 100,
        symbol: '%',
        pointer: true,
        pointerOptions: {
          toplength: -15,
          bottomlength: 10,
          bottomwidth: 12,
          color: '#8e8e93',
          stroke: '#ffffff',
          stroke_width: 3,
          stroke_linecap: 'round'
        },
        gaugeWidthScale: 0.6,
        counter: true,
        onAnimationEnd: function() {
          console.log('animation ended');
          var log = document.getElementById('log');
          log.innerHTML = log.innerHTML + 'Animation just ended.<br/>';
        }
      });

      document.getElementById('gauge_refresh').addEventListener('click', function() {
        g1.refresh(getRandomInt(0, 100));
      });
    });
