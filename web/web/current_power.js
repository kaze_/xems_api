// Load the Visualization API and the piechart package.
google.charts.load('current', {'packages':['corechart', 'gauge']});
// Set a callback to run when the Google Visualization API is loaded.

google.charts.setOnLoadCallback(loadCurrentPowerData);


var api_site = "http://ems2.xmight.com/xems/api/v1";
var locationID = "FC0673E4B6F04043857D8B8B87E67B77";

var powerConsumptionData;



// power consumption gauge
function loadCurrentPowerData()
{
	var url = api_site + "/locations/" + locationID + "/demand/current"
	var interval = 300000; // refresh every 5 minutes
	
	var jsonData = $.ajax(
		{
		async: true,
		url: url,
		dataType: "json",
		async: true,
		success: function(data) {
			if (data) {
				powerConsumptionData = data["data"]["currDmdKw"];
				// drawPowerConsumptionChart(data["data"]["currDmdKw"]);
				$('#current_demand').html(Math.round(data["data"]["currDmdKw"]*100)/100);
				$('#max_demand_today').html(Math.round(data["data"]["max_today"]['currDmdKw']*100)/100);
				$('#max_demand_time_today').html(data["data"]["max_today"]['time']);
				$('#max_demand_this_month').html(Math.round(data["data"]["max_this_month"]['currDmdKw']*100)/100);
				$('#max_demand_time_this_month').html(data["data"]["max_this_month"]['time']);

                drawPowerConsumptionChart1();

            }
			setTimeout(loadCurrentPowerData, interval);
		}
	}).responseText;
	//document.write(jsonData);
}


function drawPowerConsumptionChart1()
{
    var chart = new google.visualization.Gauge(document.getElementById('power_consumption_div1'));

    var data = google.visualization.arrayToDataTable([
        ['Label', 'Value'],
        ['瞬時功耗', powerConsumptionData]
    ]);
    var options = {
        chartArea: {
            'width': '100%',
            'height': '100%'
        },
        redFrom: 90, redTo: 100,
        yellowFrom:75, yellowTo: 90,
        minorTicks: 5
    };

    chart.draw(data, options);
}

$(window).resize(function() {
	drawPowerConsumptionChart1();
});
