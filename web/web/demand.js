// Load the Visualization API and the piechart package.
google.charts.load('current', {'packages':['corechart', 'gauge']});
// Set a callback to run when the Google Visualization API is loaded.
google.charts.setOnLoadCallback(loadDemandData);
google.charts.setOnLoadCallback(loadPowerConsumptionData);
google.charts.setOnLoadCallback(loadCurrentConsumptionData);

var max_demand = 50;
var api_site = "http://ems2.xmight.com/xems/api/v1";
var locationID = "FC0673E4B6F04043857D8B8B87E67B77";
var demanChartData;
var powerConsumptionData;
var currentConsumptionData;
var basic_fee = 11810;

function loadFeeData() {
	var endDate = new Date();
	var startDate = new Date(endDate.getFullYear(), endDate.getMonth(), 1);
	var url = api_site + "/locations/" + locationID + "/kWh?from=" + startDate.toISOString() + "&to=" + endDate.toISOString();
	var interval = 300000;

	var jsonData = $.ajax({
		url: url,
		dataTupe: "json",
		// async: false,
		success: function(result) {
			if(result) {
				var data = result['data'];
				var fee = Math.round(data['kWh'] * 3.24) ;
				$('#fee_start_time').html(startDate.toLocaleString());
				$('#fee_end_time').html(endDate.toLocaleString());
				$('#fee').html(fee);
				$('#total_fee').html(fee+basic_fee);
				$('#basic_fee').html(basic_fee);
            		}
			setTimeout(loadFeeData, interval);
		}
	}).responseText;
	//document.write(jsonData);
}

function loadDemandData () {
	// the following variables are just for test
	var endDate = new Date();
	var startDate = new Date();
	startDate.setDate(startDate.getDate() - 1);
	var url = api_site + "/locations/" + locationID + "/demand?from=" + startDate.toISOString() +
		"&to=" + endDate.toISOString();
	var interval = 300000; // refresh every 5 minutes

	$.ajax({
		url: url,
		dataType: "json",
		// async: false,
		success: function(result) {
			if (result) {
				var data = result['data'];
				var drawData = new Array();
				for ( i = 0; i < data.length; i++ ) {
					drawData.push([new Date(data[i]["time"]), data[i]["demand"], max_demand]);
				}
				demandChartData = drawData;
				drawDemandChart();
			}
			setTimeout(loadDemandData, interval);
		}
	});
}

function drawDemandChart() {
	// Create our data table out of JSON data loaded from server.
	var data = new google.visualization.DataTable();
	data.addColumn('datetime', 'Time');
	data.addColumn('number', 'demand');
	data.addColumn('number', 'max demand');
	data.addRows(demandChartData);

	var options = {
		legend: 'none',
		hAxis: {
			title: 'Time',
		},
		vAxis: {
			title: 'Demand (kw)'
		},
		chartArea: {
			'left' : 40,
			'top' : 10,
			'right': 30,
			'width' : '100%', 
			'height' : '90%' 
		},
		colors: ['#37b7f3', '#d12610'],
	}
	// Instantiate and draw our chart, passing in some options.
	var chart = new google.visualization.LineChart(document.getElementById('demand_chart_div'));
	//var chart = new google.charts.Line(document.getElementById('demand_chart_div'));
	chart.draw(data, options);
	//chart.setSelection({row: 5, column:100})
}

// power consumption gauge
function loadPowerConsumptionData()
{
	var url = api_site + "/locations/" + locationID + "/demand/current"
	var interval = 300000; // refresh every 5 minutes
	
	var jsonData = $.ajax({
		url: url,
		dataType: "json",
		// async: false,
		success: function(data) {
			if (data) {
				powerConsumptionData = data["data"]["currDmdKw"];
				// drawPowerConsumptionChart(data["data"]["currDmdKw"]);
				$('#current_demand').html(Math.round(data["data"]["currDmdKw"]*100)/100);
				$('#max_demand_today').html(Math.round(data["data"]["max_today"]['currDmdKw']*100)/100);
				$('#max_demand_time_today').html(data["data"]["max_today"]['time']);
				$('#max_demand_this_month').html(Math.round(data["data"]["max_this_month"]['currDmdKw']*100)/100);
				$('#max_demand_time_this_month').html(data["data"]["max_this_month"]['time']);
				drawPowerConsumptionChart();
            		}
			setTimeout(loadPowerConsumptionData, interval);
		}
	}).responseText;
	//document.write(jsonData);
}

function drawPowerConsumptionChart()
{
    var chart = new google.visualization.Gauge(document.getElementById('power_consumption_div'));
    
    var data = google.visualization.arrayToDataTable([
          ['Label', 'Value'],
          ['瞬時功耗', powerConsumptionData]
    ]);
    var options = {
	chartArea: {
		'width': '100%',
		'height': '100%'
	},
        redFrom: 90, redTo: 100,
        yellowFrom:75, yellowTo: 90,
        minorTicks: 5
    };

    chart.draw(data, options);
}


// power consumption gauge
function loadCurrentConsumptionData()
{
	var url = api_site + "/locations/" + locationID + "/current/now"
	var interval = 300000; // refresh every 5 minutes
	
	var jsonData = $.ajax({
		url: url,
		dataType: "json",
		// async: false,
		success: function(data) {
			if (data) {
				$('#current_a').html(Math.round(data["data"]["aA"]*100)/100);
				$('#current_b').html(Math.round(data["data"]["aB"]*100)/100);
				$('#current_c').html(Math.round(data["data"]["aC"]*100)/100);
				
				currentConsumptionData = (Math.round(data["data"]["aSummary"])*100)/200;
				$('#current_average').html(currentConsumptionData);
				$('#max_current_today').html(Math.round(data["data"]["max_today"]['aSummary']*100)/200);
				$('#max_current_time_today').html(data["data"]["max_today"]['time']);
				$('#max_current_this_month').html(Math.round(data["data"]["max_this_month"]['aSummary']*100)/200);
				$('#max_current_time_this_month').html(data["data"]["max_this_month"]['time']);
				drawCurrentConsumptionChart();
            		}
			setTimeout(loadCurrentConsumptionData, interval);
		}
	}).responseText;
	//document.write(jsonData);
}

function drawCurrentConsumptionChart()
{
    var chart = new google.visualization.Gauge(document.getElementById('current_consumption_div'));
    
    var data = google.visualization.arrayToDataTable([
          ['Label', 'Value'],
          ['瞬時電流', currentConsumptionData]
    ]);
    var options = {
	chartArea: {
		'width': '100%',
		'height': '100%'
	},
	max: 120,
        redFrom: 100, redTo: 120,
        yellowFrom:75, yellowTo: 100,
        minorTicks: 5
    };

    chart.draw(data, options);
}

loadFeeData();
$(window).resize(function() {
	drawCurrentConsumptionChart();
	drawPowerConsumptionChart();
	drawDemandChart();
});
